<div class="vehicleTypes index">
	<h2><?php echo __('Vehicle Types'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('vehicle_type_name'); ?></th>
			<th><?php echo $this->Paginator->sort('property_id'); ?></th>
			<th><?php echo $this->Paginator->sort('deposit'); ?></th>
			<th><?php echo $this->Paginator->sort('pass_cost'); ?></th>
			<th><?php echo $this->Paginator->sort('pass_cost_after_1_year'); ?></th>
			<th><?php echo $this->Paginator->sort('recurring_cost_duration'); ?></th>
			<th><?php echo $this->Paginator->sort('recurring_cost'); ?></th>
			<th><?php echo $this->Paginator->sort('vehicle_password'); ?></th>
			<th><?php echo $this->Paginator->sort('is_recurring'); ?></th>
			<th><?php echo $this->Paginator->sort('total_number_of_spaces'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($vehicleTypes as $vehicleType): ?>
	<tr>
		<td><?php echo h($vehicleType['VehicleType']['id']); ?>&nbsp;</td>
		<td><?php echo h($vehicleType['VehicleType']['vehicle_type_name']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($vehicleType['Property']['name'], array('controller' => 'properties', 'action' => 'view', $vehicleType['Property']['id'])); ?>
		</td>
		<td><?php echo h($vehicleType['VehicleType']['deposit']); ?>&nbsp;</td>
		<td><?php echo h($vehicleType['VehicleType']['pass_cost']); ?>&nbsp;</td>
		<td><?php echo h($vehicleType['VehicleType']['pass_cost_after_1_year']); ?>&nbsp;</td>
		<td><?php echo h($vehicleType['VehicleType']['recurring_cost_duration']); ?>&nbsp;</td>
		<td><?php echo h($vehicleType['VehicleType']['recurring_cost']); ?>&nbsp;</td>
		<td><?php echo h($vehicleType['VehicleType']['vehicle_password']); ?>&nbsp;</td>
		<td><?php echo h($vehicleType['VehicleType']['is_recurring']); ?>&nbsp;</td>
		<td><?php echo h($vehicleType['VehicleType']['total_number_of_spaces']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $vehicleType['VehicleType']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $vehicleType['VehicleType']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $vehicleType['VehicleType']['id']), array(), __('Are you sure you want to delete # %s?', $vehicleType['VehicleType']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Vehicle Type'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Properties'), array('controller' => 'properties', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Property'), array('controller' => 'properties', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Vehicles'), array('controller' => 'vehicles', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Vehicle'), array('controller' => 'vehicles', 'action' => 'add')); ?> </li>
	</ul>
</div>
