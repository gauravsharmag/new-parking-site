<!DOCTYPE html>

<html lang="en">
    <head>
        
        <!-- Basic Page Needs -->
        <meta charset="utf-8">
        <title> Unlearn Kids</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Mobile Specific Metas -->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta name="description" content=" Thor HTML5 Template">
        <meta name="author" content="mokawed">
        <!-- Google Fonts -->
        <link href='https://fonts.googleapis.com/css?family=Yellowtail' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Bitter' rel='stylesheet' type='text/css'>
        <!-- Font Awesome -->
        <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
    
    </head>

    <body style=" background: #e9e9e9;">
        <div style="max-width: 700px; margin-left: auto ; margin-right: auto;">
            <div style="border: 12px solid #ffff32; overflow-x: auto;">
                
                
                <table style="background-color: #fff; margin-top: 6px; width: 100%;">
                    <tr align="center">
                        <td>
                            <?php $protocol = stripos($_SERVER['SERVER_PROTOCOL'], 'https') === true ? 'https://' : 'http://'; ?>
                            <img src="<?php echo $protocol . $_SERVER['SERVER_NAME']; ?>/img/onlineparking-logo.png" style="display: block; max-width: 100%; height: auto;"></img>      
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p style=" text-align: center; font-weight: bold; font-size: 20px;"> <i class="fa fa-angle-left" aria-hidden="true"></i>Ticket Reply<i class="fa fa-angle-right" aria-hidden="true"></i></p>
                        </td>
                    </tr>
                    
                    <tr>
                        <td style="font-size: 16px; padding-left: 10px; padding-right: 10px; word-wrap: break-word; min-width: 160px; max-width: 160px;">
                            <p > 
                                <?php echo $msg_line_1; ?>
                            </p>

                            <p> 
                                <?php echo $msg_line_2; ?>
                            </p>
                            <hr>
                            <?php if(!$managerEmail){ ?>
                            <p> 
                                <a href="<?php echo $url ?>">CLICK HERE TO VIEW TICKET</a>
                            </p>
                            <hr>
                            <?php } ?>
						</td>
                    </tr>
                    
                    <tr>
                        <td style="font-size: 16px; padding-left: 10px;"> 
                            <p> Thanks & Regards, </p>
                            <p> <?php echo $propertyName; ?></p>
                        </td>
                    </tr>
            </div>
        </div>
    </body>
</html>





	  
















