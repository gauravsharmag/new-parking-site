
<h3>Hello, <?php echo ucfirst($name); ?></h3>
<p>
    This is a friendly reminder from <?php echo $site; ?> that you have not assigned any vehicle to your Parking Pass <b>
	"<?php echo ucfirst($pass); ?>"</b> with Property <b>"<?php echo ucfirst($property); ?>"</b>. Please login to you account and assign a vehicle.<br/>
	<br>
	<b>NOTE: ANY VEHICLE NOT ASSOCIATED WITH A PASS WILL BE TOWED.</b>
	<br>
	<br>
    You can login <a href="http://<?php echo $subdomain; ?>.<?php echo $domain; ?>">here-</a><br/>
    If you feel this is a mistake, please contact us through our support ticket in your dashboard - 
    <a href="https://<?php echo $subdomain; ?>.<?php echo $domain; ?>/tickets/contact_us" >Contact Form</a><br/>
    <br/>
    &nbsp;Sincerely,<br/>
    <?php echo $property; ?> Support Team
</p>
