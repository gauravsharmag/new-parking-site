
<h3>Hello, <?php echo ucfirst($cp['User']['first_name']); ?></h3>
<p>
    This is a friendly reminder from <?php echo Configure::read('SITE'); ?> that your Parking Pass <b>
	"<?php echo ucfirst($cp['Pass']['Pass_name']); ?>"</b> with Property <b>"<?php echo ucfirst($cp['Property']['Property_name']); ?>"</b> 
	will be expiring with in 7 days. To prevent your pass from expiring please login to your account and renew your pass.<br/>
    You can login <a href="http://<?php echo $cp['Property']['sub_domain']; ?>.<?php echo Configure::read('SITE_URL'); ?>">here-</a><br/>
    If you feel this is a mistake, please contact us through our support ticket in your dashboard - 
    <a href="https://<?php echo $cp['Property']['sub_domain']; ?>.<?php echo Configure::read('SITE_URL'); ?>/tickets/contact_us" >Contact Form</a><br/>
    &nbsp;Sincerely,<br/>
    <?php echo $cp['Property']['Property_name']; ?> Support Team
</p>
