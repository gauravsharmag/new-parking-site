<div class="passes view">
<h2><?php echo __('Pass'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($pass['Pass']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Property'); ?></dt>
		<dd>
			<?php echo $this->Html->link($pass['Property']['name'], array('controller' => 'properties', 'action' => 'view', $pass['Property']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Cost 1st Year'); ?></dt>
		<dd>
			<?php echo h($pass['Pass']['cost_1st_year']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Cost After 1st Year'); ?></dt>
		<dd>
			<?php echo h($pass['Pass']['cost_after_1st_year']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Pass Duration'); ?></dt>
		<dd>
			<?php echo h($pass['Pass']['pass_duration']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Duration Type'); ?></dt>
		<dd>
			<?php echo h($pass['Pass']['duration_type']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Deposit'); ?></dt>
		<dd>
			<?php echo h($pass['Pass']['deposit']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Expiration Date'); ?></dt>
		<dd>
			<?php echo h($pass['Pass']['expiration_date']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Sort Order'); ?></dt>
		<dd>
			<?php echo h($pass['Pass']['sort_order']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Pass'), array('action' => 'edit', $pass['Pass']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Pass'), array('action' => 'delete', $pass['Pass']['id']), array(), __('Are you sure you want to delete # %s?', $pass['Pass']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Passes'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Pass'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Properties'), array('controller' => 'properties', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Property'), array('controller' => 'properties', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Transactions'), array('controller' => 'transactions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Transaction'), array('controller' => 'transactions', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Transactions'); ?></h3>
	<?php if (!empty($pass['Transaction'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th><?php echo __('Date Time'); ?></th>
		<th><?php echo __('Amount'); ?></th>
		<th><?php echo __('Result'); ?></th>
		<th><?php echo __('Pass Id'); ?></th>
		<th><?php echo __('Message'); ?></th>
		<th><?php echo __('Credits Used'); ?></th>
		<th><?php echo __('Payment Method Used'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($pass['Transaction'] as $transaction): ?>
		<tr>
			<td><?php echo $transaction['id']; ?></td>
			<td><?php echo $transaction['user_id']; ?></td>
			<td><?php echo $transaction['date_time']; ?></td>
			<td><?php echo $transaction['amount']; ?></td>
			<td><?php echo $transaction['result']; ?></td>
			<td><?php echo $transaction['pass_id']; ?></td>
			<td><?php echo $transaction['message']; ?></td>
			<td><?php echo $transaction['credits_used']; ?></td>
			<td><?php echo $transaction['payment_method_used']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'transactions', 'action' => 'view', $transaction['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'transactions', 'action' => 'edit', $transaction['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'transactions', 'action' => 'delete', $transaction['id']), array(), __('Are you sure you want to delete # %s?', $transaction['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Transaction'), array('controller' => 'transactions', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
