<head>
<?php echo $this->Html->script('jquery.min'); ?>

<script>
$(document).ready(function(){
    if($("#PassExpirationDate").val()=="01-01-1970"){
             $("#PassExpirationDate").attr("value","" );
        }


    if ($("#PassIsFixedDuration").is(':checked'))
          {
             $("#panelDate").show("slow");
             $("#panelDuration").hide("slow");
             $("#PassExpirationDate").attr("required",true );

             $("#PassDuration").attr("required",false);
             $("#PassDurationType").attr("required",false);

          }
          else
         {
           $("#panelDate").hide("slow");
           $("#panelDuration").show("slow");
           $("#PassExpirationDate").attr("required",false );
           $("#PassExpirationDate").attr("value","" );
           $("#PassDuration").attr("required",true );
           $("#PassDurationType").attr("required",true );

         }

   $("#PassIsFixedDuration").change(function()
       {
           if ($(this).is(':checked'))
           {
               $("#panelDate").show("slow");
               $("#PassExpirationDate").attr("required",true );
               $("#panelDuration").hide("slow");
               $("#PassDuration").attr("required",false);
               $("#PassDurationType").attr("required",false);

           }
           else
           {
                $("#panelDate").hide("slow");
                $("#PassExpirationDate").attr("required",false );
                $("#panelDuration").show("slow");
                $("#PassDuration").attr("required",true );
                $("#PassDurationType").attr("required",true );
           }
       });
});
</script>

</head>


<body>




<div class="page-content-wrapper">
		<div class="page-content">
<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
				<h3 class="page-title">Property <small><?php echo $propertyName;?></small>
                					</h3>
                					<ul class="page-breadcrumb breadcrumb">
                                         <li class="btn-group">
                  							<button type="button" class="btn blue dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true">
                  							<span><?php $srNo=0; echo __('Actions'); ?></span><i class="fa fa-angle-down"></i>
                  							</button>
                  							<ul class="dropdown-menu pull-right" role="menu">
                  								<li>
													<a href="#"><?php echo $this->Html->link(__('View Property'), array('controller'=>'Properties','action' => 'view',$propertyId,$propertyName)); ?> </a>
												</li>
                  								<li>
                  									<a href="#"><?php echo $this->Html->link(__('Edit Property'), array('controller'=>'Properties','action' => 'edit',$propertyId,$propertyName)); ?> </a>
                  								</li>
                  								<li>
                  									<a href="#"><?php echo $this->Html->link(__('Manage Parking Location'), array('controller' => 'Packages', 'action' => 'add',$propertyId,$propertyName)); ?></a>
                  								</li>

                  								<li>
                  									<a href="#"><?php echo $this->Html->link(__('Manage Passes'), array('controller' => 'Passes', 'action' => 'add',$propertyId,$propertyName)); ?> </a>
                  								</li>

                  							</ul>
                  				        </li>
                  						<li>
                							<i class="fa fa-home"></i>
                							<?php echo $this->Html->link('Properties',array('controller'=>'properties','action'=>'index')); ?>
                							<i class="fa fa-angle-right"></i>
                						</li>
                						<li>
                							<?php echo __('Edit Pass'); ?>

                						</li>

                					</ul>
					<div class="tabbable tabbable-custom boxless tabbable-reversed">


							<div class="tab-pane active" id="tab_0">
								<div class="portlet box green">
									<div class="portlet-title">
										<div class="caption">
											<i class="fa fa-gift"></i><?php  echo __('Passes '.' '); ?>
										</div>
										<div class="actions">
											<a href="/admin/Passes/add/<?php echo $propertyId?>/<?php echo $propertyName;?>" class="btn btn-default btn-sm">
												<i class="fa fa-pencil"></i> Existing Passes </a>
										</div>
									</div>

										<!-- BEGIN FORM-->

									<div class="portlet-body form">

									<?php echo $this->Form->create('Pass',array('class'=>'form-horizontal'));
                                         //echo $this->Html->link(__('List Existing Packages'), array('action' => 'index',$propertyId,$propertyName));
									?>
                                    <div class="form-body">
                                              <h3><?php echo $this->Session->flash();?></h3>
                                         <?php
                                                     echo $this->Form->input('id');
                                                  	 echo $this->Form->hidden('property_id');
                                                  	  echo $this->Form->input('name',array('required'=>true,'div'=>array('class'=>'form-group'),
                                                                                                                                     'label'=>array('class'=>'col-md-3 control-label','text'=>'Name'),
                                                                                                                                     'between'=>'<div class="col-md-4">',
                                                                                                                                     'after'=>'</div>',
                                                                                                                                     'class'=>'form-control'));
                                                     echo $this->Form->input('deposit',array('div'=>array('class'=>'form-group'),
                                                                                                            'label'=>array('class'=>'col-md-3 control-label','text'=>'Deposit'),
                                                                                                             'between'=>'<div class="col-md-4">',
                                                                                                             'after'=>'</div>',
                                                   	                                                         'class'=>'form-control'));
                                                     echo $this->Form->input('cost_1st_year',array('required'=>true,'div'=>array('class'=>'form-group'),
                                                								'label'=>array('class'=>'col-md-3 control-label','text'=>'Cost For One Year'),
                                                								'between'=>'<div class="col-md-4">',
                                                								'after'=>'</div>',
                                                								'class'=>'form-control'));
                                                     echo $this->Form->input('cost_after_1st_year',array('required'=>true,'div'=>array('class'=>'form-group'),
                                                                                 'label'=>array('class'=>'col-md-3 control-label','text'=>'Cost After One Year'),
                                                                                 'between'=>'<div class="col-md-4">',
                                                                                 'after'=>'</div>',
                                                                                 'class'=>'form-control'));                                                        echo"<div id='panelDuration'>";

                                                                echo $this->Form->input('duration',array('required'=>true,'div'=>array('class'=>'form-group'),
                                                                                'label'=>array('class'=>'col-md-3 control-label','text'=>'Pass Duration'),
                                                                                'between'=>'<div class="col-md-4">',
                                                                                'after'=>'</div>',
                                                                                'class'=>'form-control'));

                                                                echo $this->Form->input('duration_type',array('type'=>'select','options'=>array('Hour'=>'Hour','Day'=>'Day','Month'=>'Month','Year'=>'Year'),'empty'=>'Select One','required'=>true,'div'=>array('class'=>'form-group'),
                                                                                 'label'=>array('class'=>'col-md-3 control-label','text'=>'Duration Type'),
                                                                                 'between'=>'<div class="col-md-4">',
                                                                                 'after'=>'</div>',
                                                                                 'class'=>'form-control'));
                                                        echo"</div>";
                                                        echo $this->Form->input('is_fixed_duration',array('type'=>'checkbox','div'=>array('class'=>'form-group'),
                                                                                'label'=>false,
                                                                                 'before'=>'<label class="col-md-3 control-label">Is Fixed Duration?</label><div class="col-md-4">',
                                                                                'after'=>'</div>'
                                                                                 ));
                                                        echo"<div id='panelDate' style='display:none'>";
                                                		            echo $this->Form->input('expiration_date',array('type'=>'text','div'=>array('class'=>'form-group'),
                                                								'label'=>array('class'=>'col-md-3 control-label','text'=>'Expiration Date'),
                                                								'between'=>'<div class="col-md-4"><div class="input-group date date-picker" data-date-format="mm/dd/yyyy" data-date-start-date="+0d">',
                                                								'after'=>'<span class="input-group-btn"><button type="button" class="btn default"><i class="fa fa-calendar"></i></button></span></div></div>',
                                                								'class'=>'form-control'));
                                                		 echo"</div>";
                                                		//echo $this->Form->hidden('sort_order');
                                                  		?>
                                           <div class="form-actions fluid">
												<div class="col-md-offset-3 col-md-9">
													<button type="submit" class="btn blue">Submit</button>
													<!--<button type="button" class="btn default">Cancel</button>-->
												</div>
											</div>
                                         <?php echo $this->Form->end(); ?>
                                     </div>

										<!-- END FORM-->
									</div>
								</div>

							</div>



					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->

</body>
<?php echo $this->Html->script('//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js'); ?> 
<?php echo $this->Html->script('/assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js'); ?>
<?php echo $this->Html->script('/assets/admin/pages/scripts/components-pickers.js');   ?>
<script>
    jQuery(document).ready(function() {     
      ComponentsPickers.init(); 
    });
</script>
