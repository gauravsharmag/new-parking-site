
<script>
$(document).ready(function(){
	$("#NewPass").hide();
	$("#AddNew").click(function(){
		$("#NewPass").slideToggle();
	});
	
    $("#panelDate").hide("slow");
    $("#PassDuration").attr("required",true );
    $("#PassDurationType").attr("required",true );
    $("#PassExpirationDate").attr("required",false );
    $("#panelDuration").show("slow");
    $("#PassIsFixedDuration").change(function()
    {
        if ($(this).is(':checked'))
        {
            $("#panelDate").show("slow");
            $("#PassExpirationDate").attr("required",true );
            $("#panelDuration").hide("slow");
            $("#PassDuration").attr("required",false);
            $("#PassDurationType").attr("required",false);

        }
        else
        {
             $("#panelDate").hide("slow");
             $("#PassExpirationDate").attr("required",false );
             $("#panelDuration").show("slow");
             $("#PassDuration").attr("required",true );
             $("#PassDurationType").attr("required",true );
        }
    });
});
</script>

<div class="page-content-wrapper">
	<div class="page-content" style="min-height:1089px">
		<!-- BEGIN PAGE HEADER-->
		<div class="row">
			<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
									<?php echo __(' Add/View Passes in ').$propertyName; ?> <small></small>
					 </h3>
					 <ul class="page-breadcrumb breadcrumb">
							<li class="btn-group">
								<button type="button" class="btn blue dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true">
								<span><?php $srNo=0; echo __('Actions'); ?></span><i class="fa fa-angle-down"></i>
								</button>
								<ul class="dropdown-menu pull-right" role="menu">
									<li>
										<a href="#"><?php echo $this->Html->link(__('View Property'), array('controller'=>'Properties','action' => 'view',$id,$propertyName)); ?> </a>
									</li>
									<li>
										<a href="#"><?php echo $this->Html->link(__('Edit Property'), array('controller'=>'Properties','action' => 'edit',$id,$propertyName)); ?> </a>
									</li>
									<li>
										<a href="#"><?php echo $this->Html->link(__('Manage Parking Locations'), array('controller' => 'Packages', 'action' => 'add',$id,$propertyName)); ?></a>
									</li>

									<li>
										<a href="#"><?php echo $this->Html->link(__('Manage Passes'), array('controller' => 'Passes', 'action' => 'add',$id,$propertyName)); ?> </a>
									</li>

								</ul>
							</li>
						<li>
							<i class="fa fa-home"></i>
								 <?php echo $this->Html->link('Properties',array('controller'=>'properties','action'=>'index')); ?>
							<i class="fa fa-angle-right"></i>
						</li>
						 <li>
							  <?php echo $this->Html->link($propertyName,array('controller'=>'properties','action'=>'view',$id)); ?>
							 <i class="fa fa-angle-right"></i>
						 </li>
						 <li>
								<?php echo __('Passes Details'); ?>
						 </li>                              
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
					<div id="flashMessages">
							<h2><?php  echo $this->Session->flash();?></h2>
					</div>
					<div class="nav-pills">
						<button class="btn btn-primary" id="AddNew">Add new Pass</button>
					</div>
				   <!-- 1 Portlet-->
					<div class="portlet box red" id="NewPass" style="display:none;">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-gift"></i><?php echo 'Add New Pass For '.$propertyName; ?>
							</div>
							<div class="tools">
								<a class="collapse" href="javascript:;">
								</a>
							</div>
						</div>
						<div class="portlet-body">
								<div class="form">
								<?php 
									  if($new_pass_allowed==true)
									  {
											echo $this->Form->create('Pass',array('class'=>'form-horizontal'));
											echo $this->Form->hidden('property_id',array('default'=>$id));
											echo $this->Form->input('name',array('required'=>true,'div'=>array('class'=>'form-group'),
																	'label'=>array('class'=>'col-md-3 control-label','text'=>'Name'),
																	'error'=>array('attributes'=>array('class'=>'model-error')),
																	'between'=>'<div class="col-md-4">',
																	'after'=>'</div>',
																	'class'=>'form-control'));

											echo $this->Form->input('deposit',array('required'=>true,'div'=>array('class'=>'form-group'),
																	'label'=>array('class'=>'col-md-3 control-label','text'=>'Deposit'),
																	'error'=>array('attributes'=>array('class'=>'model-error')),
																	'between'=>'<div class="col-md-4">',
																	'after'=>'</div>',
																	'class'=>'form-control'));
											echo $this->Form->input('cost_1st_year',array('required'=>true,'div'=>array('class'=>'form-group'),
																	'label'=>array('class'=>'col-md-3 control-label','text'=>'Cost For One Year'),
																	'error'=>array('attributes'=>array('class'=>'model-error')),
																	'between'=>'<div class="col-md-4">',
																	'after'=>'</div>',
																	'class'=>'form-control'));
											echo $this->Form->input('cost_after_1st_year',array('required'=>true,'div'=>array('class'=>'form-group'),
																	 'label'=>array('class'=>'col-md-3 control-label','text'=>'Cost After One Year'),
																	 'error'=>array('attributes'=>array('class'=>'model-error')),
																	 'between'=>'<div class="col-md-4">',
																	 'after'=>'</div>',
																	 'class'=>'form-control'));
											echo"<div id='panelDuration'>";

													echo $this->Form->input('duration',array('required'=>true,'div'=>array('class'=>'form-group'),
																	'label'=>array('class'=>'col-md-3 control-label','text'=>'Pass Duration'),
																	'error'=>array('attributes'=>array('class'=>'model-error')),
																	'between'=>'<div class="col-md-4">',
																	'after'=>'</div>',
																	'class'=>'form-control'));

													echo $this->Form->input('duration_type',array('type'=>'select','options'=>array('Hour'=>'Hour','Day'=>'Day','Month'=>'Month','Year'=>'Year'),'empty'=>'Select One','required'=>true,'div'=>array('class'=>'form-group'),
																	 'label'=>array('class'=>'col-md-3 control-label','text'=>'Duration Type'),
																	 'error'=>array('attributes'=>array('class'=>'model-error')),
																	 'between'=>'<div class="col-md-4">',
																	 'after'=>'</div>',
																	 'class'=>'form-control'));
											echo"</div>";
											echo $this->Form->input('is_fixed_duration',array('type'=>'checkbox','div'=>array('class'=>'form-group'),
																	'label'=>false,
																	'error'=>array('attributes'=>array('class'=>'model-error')),
																	 'before'=>'<label class="col-md-3 control-label">Is Fixed Duration?</label><div class="col-md-4">',
																	'after'=>'</div>'
																	 ));
											echo"<div id='panelDate'>";
														echo $this->Form->input('expiration_date',array('required'=>true,'type'=>'text','div'=>array('class'=>'form-group'),
																	'label'=>array('class'=>'col-md-3 control-label','text'=>'Expiration Date'),
																	'error'=>array('attributes'=>array('class'=>'model-error')),
																	'between'=>'<div class="col-md-4"><div class="input-group date date-picker" data-date-format="mm/dd/yyyy" data-date-start-date="+0d">',
																	'after'=>'<span class="input-group-btn"><button type="button" class="btn default"><i class="fa fa-calendar"></i></button></span></div></div>',
																	'class'=>'form-control'));
											 echo"</div>";
											echo $this->Form->hidden('sort_order');
									   }
									   else
									   {
										echo "<h3>Sorry you cannot add new passes as the limit of allowed passes has been reached<h3>";
									   }
								?>
								<?php if($new_pass_allowed==true){?>
								 <div class="form-actions fluid">
									<div class="col-md-offset-3 col-md-9">
										<button type="submit" class="btn blue">Submit</button> 
									</div>
								</div>
								<?php }?>
							  <?php if($new_pass_allowed==true){echo $this->Form->end();}?>
							  </div>
						</div>
					</div>
					 <!-- 1 Portlet-->
					 <!-- 2 Portlet-->
					<div class="portlet box green">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-gift"></i><?php echo __('Existing Passes '.$propertyName); ?>
							</div>
							<div class="tools">
								<a class="collapse" href="javascript:;">
								</a>
							</div>
						</div>
						<div class="portlet-body">
							<?php if(!empty($passes))
							  {
							?>
							   <div class="table-responsive">
								  <table class="table table-striped table-bordered table-hover">
								   <thead>
										<tr>
											<th><?php echo $this->Paginator->sort('sort_order','Pass Order'); ?></th>
											<th><?php echo $this->Paginator->sort('name'); ?></th>
											<th><?php echo $this->Paginator->sort('deposit'); ?></th>
											<th><?php echo $this->Paginator->sort('cost_1st_year'); ?></th>
											<th><?php echo $this->Paginator->sort('cost_after_1st_year'); ?></th>
											<th><?php echo $this->Paginator->sort('pass_duration'); ?></th>
											<th><?php echo $this->Paginator->sort('duration_type'); ?></th>
											<th><?php echo $this->Paginator->sort('is_fixed_duration'); ?></th>
											<th><?php echo $this->Paginator->sort('expiration_date'); ?></th>
											<th class="actions"><?php echo __('Actions'); ?></th>
										</tr>
									</thead>
									<tbody>
									<?php foreach ($passes as $pass): ?>
										<tr>
											<td><?php //echo h($pass['Pass']['id']);
												   echo h($pass['Pass']['sort_order']); ?>&nbsp;
											</td>
											<td>
												<?php echo h($pass['Pass']['name']); ?>
											</td>
											<td><?php echo h($pass['Pass']['deposit']); ?>&nbsp;</td>
											<td><?php echo h($pass['Pass']['cost_1st_year']); ?>&nbsp;</td>
											<td><?php echo h($pass['Pass']['cost_after_1st_year']); ?>&nbsp;</td>
											<td><?php echo h($pass['Pass']['duration']); ?>&nbsp;</td>
											<td><?php echo h($pass['Pass']['duration_type']); ?>&nbsp;</td>
											<td><?php if($pass['Pass']['is_fixed_duration']==1){echo h('Yes');}else{echo h('No');} ?>
													&nbsp;
											</td>
											<td><?php  if($pass['Pass']['expiration_date']==null){ echo "NA";}else{
														$pass['Pass']['expiration_date']= date("m/d/Y", strtotime($pass['Pass']['expiration_date']));
														 echo h($pass['Pass']['expiration_date']);
														 }
												?>
											</td>    
											<td class="actions">
											<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $pass['Pass']['id'],$id,$propertyName)); ?>
												<?php //echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $pass['Pass']['id']), array(), __('Are you sure you want to delete # %s?', $pass['Pass']['id'])); ?>
											</td>
										 </tr>
									<?php endforeach; ?>
									</tbody>
								</table>
									<p>
										<?php
											echo $this->Paginator->counter(array(
											'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
											));
										?>  
									</p>
									<div class="paging">
										<?php
											echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
											echo $this->Paginator->numbers(array('separator' => ''));
											echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
										?>
									</div>
							<?php }?>
						</div>           
					</div>
				</div>
				 <!-- 2 Portlet-->
			</div>
		</div>
		<!--End Row-->
	</div>
</div>
<?php echo $this->Html->script('//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js'); ?> 
<?php echo $this->Html->script('/assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js'); ?>
<?php echo $this->Html->script('/assets/admin/pages/scripts/components-pickers.js');   ?>
<script>
    jQuery(document).ready(function() {     
      ComponentsPickers.init(); 
    });
</script>
<!--End Page -->
