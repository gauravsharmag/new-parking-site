<div class="passes index">
	<h2><?php echo __('Passes'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('user_id'); ?></th>
			<th><?php echo $this->Paginator->sort('vehicle_id'); ?></th>
			<th><?php echo $this->Paginator->sort('property_id'); ?></th>
			<th><?php echo $this->Paginator->sort('transaction_id'); ?></th>
			<th><?php echo $this->Paginator->sort('status'); ?></th>
			<th><?php echo $this->Paginator->sort('membership_vaild_upto'); ?></th>
			<th><?php echo $this->Paginator->sort('pass_valid_upto'); ?></th>
			<th><?php echo $this->Paginator->sort('RFID_tag_number'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($passes as $pass): ?>
	<tr>
		<td><?php echo h($pass['Pass']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($pass['User']['id'], array('controller' => 'users', 'action' => 'view', $pass['User']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($pass['Vehicle']['id'], array('controller' => 'vehicles', 'action' => 'view', $pass['Vehicle']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($pass['Property']['name'], array('controller' => 'properties', 'action' => 'view', $pass['Property']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($pass['Transaction']['id'], array('controller' => 'transactions', 'action' => 'view', $pass['Transaction']['id'])); ?>
		</td>
		<td><?php echo h($pass['Pass']['status']); ?>&nbsp;</td>
		<td><?php echo h($pass['Pass']['membership_vaild_upto']); ?>&nbsp;</td>
		<td><?php echo h($pass['Pass']['pass_valid_upto']); ?>&nbsp;</td>
		<td><?php echo h($pass['Pass']['RFID_tag_number']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $pass['Pass']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $pass['Pass']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $pass['Pass']['id']), array(), __('Are you sure you want to delete # %s?', $pass['Pass']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Pass'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Vehicles'), array('controller' => 'vehicles', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Vehicle'), array('controller' => 'vehicles', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Properties'), array('controller' => 'properties', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Property'), array('controller' => 'properties', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Transactions'), array('controller' => 'transactions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Transaction'), array('controller' => 'transactions', 'action' => 'add')); ?> </li>
	</ul>
</div>
