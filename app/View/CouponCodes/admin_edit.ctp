<div class="couponCodes form">
<?php echo $this->Form->create('CouponCode'); ?>
	<fieldset>
		<legend><?php echo __('Admin Edit Coupon Code'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('code');
		echo $this->Form->input('coupon_package_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('CouponCode.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('CouponCode.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Coupon Codes'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Coupon Packages'), array('controller' => 'coupon_packages', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Coupon Package'), array('controller' => 'coupon_packages', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Coupon Code Users'), array('controller' => 'coupon_code_users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Coupon Code User'), array('controller' => 'coupon_code_users', 'action' => 'add')); ?> </li>
	</ul>
</div>
