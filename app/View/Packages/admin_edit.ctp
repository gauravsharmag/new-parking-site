
<script> 
$(document).ready(function(){
     if ($('#PackageIsGuest').is(':checked')){
             $("#panelGuest").show("slow");
             $("#PackageFreeGuestCredits").attr("required",true );
           }
     else{
             $("#panelGuest").hide("slow");
             $("#PackageFreeGuestCredits").attr("required",false );
           }

      if ($("#PackageIsFixedDuration").is(':checked'))
      {
         $("#panelDate").show("slow");
         $("#panelTime").hide("slow");
         $("#PackageStartDate").attr("required",true );
         $("#PackageExpirationDate").attr("required",true );
         $("#PackageDuration").attr("required",false);
         $("#PackageDurationType").attr("required",false);


      }
      else
     {
       $("#panelDate").hide("slow");
       $("#panelTime").show("slow");
       $("#PackageStartDate").attr("required",false );
       $("#PackageExpirationDate").attr("required",false );
       $("#PackageStartDate").attr("value"," ");
       $("#PackageExpirationDate").attr("value"," " );
       $("#PackageDuration").attr("required",true );
       $("#PackageDurationType").attr("required",true );

     }



    $("#PackageIsFixedDuration").change(function()
    {
     if ($(this).is(':checked')){
        $("#panelDate").show("slow");
        $("#PackageStartDate").attr("required",true );
        $("#PackageExpirationDate").attr("required",true );
        $("#panelTime").hide("slow");
        $("#PackageDuration").attr("required",false);
        $("#PackageDurationType").attr("required",false);
    }
    else{

        $("#panelDate").hide("slow");
        $("#PackageStartDate").attr("required",false );
        $("#PackageExpirationDate").attr("required",false );
        $("#panelTime").show("slow");
        $("#PackageDuration").attr("required",true );
        $("#PackageDurationType").attr("required",true );
    }

  });
});
$(document).ready(function(){
if($("#PackageStartDate").val()=="01-01-1970"){
          $("#panelDate").hide("slow");
          $("#panelTime").show("slow");
    }

  $('#PackageIsGuest').click(function ()
  {
           if ($(this).is(':checked')){
             $("#panelGuest").show("slow");
             $("#PackageFreeGuestCredits").attr("required",true );
           }
           else
           {
             $("#panelGuest").hide("slow");
            $("#PackageFreeGuestCredits").attr("required",false );
           }
    });

  });


</script>



<body>
<div class="page-content-wrapper">
		<div class="page-content">
<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
				<h3 class="page-title">Property <small><?php echo $propertyName;?></small>
                					</h3>
                					<ul class="page-breadcrumb breadcrumb">
                                        <li class="btn-group">
            							    <button type="button" class="btn blue dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true">
            							    <span><?php $srNo=0; echo __('Actions'); ?></span><i class="fa fa-angle-down"></i>
            							    </button>
            							    <ul class="dropdown-menu pull-right" role="menu">
												<li>
													<a href="#"><?php echo $this->Html->link(__('View Property'), array('controller'=>'Properties','action' => 'view',$propertyId,$propertyName)); ?> </a>
												</li>
            								    <li>
            									    <a href="#"><?php echo $this->Html->link(__('Edit Property'), array('controller'=>'Properties','action' => 'edit',$propertyId,$propertyName)); ?> </a>
            								    </li>
            								    <li>
            									    <a href="#"><?php echo $this->Html->link(__('Parking Locations'), array('controller' => 'Packages', 'action' => 'add',$propertyId,$propertyName)); ?></a>
            								    </li>
            								    <li>
            									    <a href="#"><?php echo $this->Html->link(__('Manage Passes'), array('controller' => 'Passes', 'action' => 'add',$propertyId,$propertyName)); ?> </a>
            								    </li>
            						        </ul>
            				            </li>
                                        <li>
                                            <i class="fa fa-home"></i>
                                                <?php echo $this->Html->link('Properties',array('controller'=>'properties','action'=>'index')); ?>
                                            <i class="fa fa-angle-right"></i>
                                        </li>
                                         <li>
                                            <?php echo $this->Html->link($propertyName,array('controller'=>'properties','action'=>'view',$propertyId)); ?>
                                            <i class="fa fa-angle-right"></i>
                                          </li>
                                         <li>
                                                <?php echo __('Edit Parking Package'); ?>
                                          </li>
                					</ul>
					<div class="tabbable tabbable-custom boxless tabbable-reversed">


							<div class="tab-pane active" id="tab_0">
								<div class="portlet box green">
									<div class="portlet-title">
										<div class="caption">
											<i class="fa fa-gift"></i><?php  echo __('Parking Packages '.' '); ?>
										</div>
										<div class="actions">
												<a href="/admin/Packages/add/<?php echo $propertyId?>/<?php echo $propertyName;?>" class="btn btn-default btn-sm">
													<i class="fa fa-pencil"></i> Existing Parking Locations </a>
											</div>
									</div>

										<!-- BEGIN FORM-->

									<div class="portlet-body form">
									<?php echo $this->Form->create('Package',array('class'=>'form-horizontal'));
                                        // echo $this->Html->link(__('List Existing Permits'), array('action' => 'index',$propertyId,$propertyName));
									?>
                                    <div class="form-body">
                                              <h3><?php echo $this->Session->flash();?></h3>
                                         <?php
                                                        echo $this->Form->input('id');
                                                //echo $this->Form->input('property_id');
                                                  echo $this->Form->hidden('property_id',array('default'=>$propertyId));

                                                  echo $this->Form->input('name',array('div'=>array('class'=>'form-group'),
                                                          'label'=>array('class'=>'col-md-3 control-label','text'=>'Name'),
                                                           'between'=>'<div class="col-md-4">',
                                                           'after'=>'</div>',
                                                           'class'=>'form-control'));

                                                    $getPassName=$this->requestAction(array('controller'=>'Passes','action'=>'getPassName',$this->request->data['Package']['pass_id']));
                                                    echo $this->Form->input('pass_id',array('div'=>array('class'=>'form-group'),
                                                                                                                      'empty'=>'Select',
                                                                                                                      'type'=>'select','options'=>$passes,
                                                                                                                      'label'=>array('class'=>'col-md-3 control-label','text'=>'Assigned Pass'),
                                                                                                                      'between'=>'<div class="col-md-4">',
                                                                                                                      'after'=>'<span class="help-block">Currently Assigned Pass:  '. $getPassName.'</span></div>',
                                                                                                                      'class'=>'form-control'));
                                                    echo $this->Form->input('remove_assigned_pass',array('type'=>'checkbox','div'=>array('class'=>'form-group'),
                                                              'label'=>false,
                                                               'before'=>'<label class="col-md-3 control-label">Remove Assigned Pass?</label><div class="col-md-4">',
                                                               'after'=>'</div>'
                                                               ));
                                                   echo $this->Form->input('cost',array('div'=>array('class'=>'form-group'),'label'=>array('class'=>'col-md-3 control-label','text'=>'Cost'),'between'=>'<div class="col-md-4">','after'=>'</div>','class'=>'form-control'));
                                             echo"<div id='panelTime'>";
                                                    echo $this->Form->input('duration',array('div'=>array('class'=>'form-group'),
                                                                'label'=>array('class'=>'col-md-3 control-label','text'=>'Duration'),
                                                                'between'=>'<div class="col-md-4">',
                                                                'after'=>'</div>',
                                                                'class'=>'form-control'));

                                                    echo $this->Form->input('duration_type',array('type'=>'select','options'=>array('Day'=>'Day','Month'=>'Month','Year'=>'Year'),'empty'=>'Select One','div'=>array('class'=>'form-group'),
                                                                'label'=>array('class'=>'col-md-3 control-label','text'=>'Duration Type'),
                                                                 'between'=>'<div class="col-md-4">',
                                                                  'after'=>'</div>',
                                                                  'class'=>'form-control'));
                                             echo"</div>";
                                                    echo $this->Form->input('is_fixed_duration',array('type'=>'checkbox','div'=>array('class'=>'form-group'),
                                                             'label'=>false,
                                                              'before'=>'<label class="col-md-3 control-label">Is Fixed Duration?</label><div class="col-md-4">',
                                                              'after'=>'</div>'
                                                              ));
                                             echo"<div id='panelDate'>";
                                        	         echo $this->Form->input('start_date',array('type'=>'text','div'=>array('class'=>'form-group'),
                                                          'label'=>array('class'=>'col-md-3 control-label','text'=>'Start Date'),
                                                          'between'=>'<div class="col-md-4"><div class="input-group date date-picker" data-date-format="mm/dd/yyyy" data-date-start-date="+0d">',
                                                           'after'=>'<span class="input-group-btn"><button type="button" class="btn default"><i class="fa fa-calendar"></i></button></span></div></div>',
                                                           'class'=>'form-control'));

                                                    echo $this->Form->input('expiration_date',array('type'=>'text','div'=>array('class'=>'form-group'),
                                                            'label'=>array('class'=>'col-md-3 control-label','text'=>'Valid Upto'),
                                                             'between'=>'<div class="col-md-4"><div class="input-group date date-picker" data-date-format="mm/dd/yyyy" data-date-start-date="+0d">',
                                                             'after'=>'<span class="input-group-btn"><button type="button" class="btn default"><i class="fa fa-calendar"></i></button></span></div></div>',
                                                             'class'=>'form-control'));
                                             echo"</div>";

                                                   echo $this->Form->input('total_spaces_each_package',array('required'=>true,'div'=>array('class'=>'form-group'),
                                                              'label'=>array('class'=>'col-md-3 control-label','text'=>'Allocate space'),
                                                              'between'=>'<div class="col-md-4">',
                                                               'after'=>'</div>',
                                                               'class'=>'form-control'));

                                                    echo $this->Form->input('is_recurring',array('type'=>'checkbox','div'=>array('class'=>'form-group'),
                                                                  'label'=>false,
                                                                  'before'=>'<label class="col-md-3 control-label">Is Recurring ?</label><div class="col-md-4">',
                                                                   'after'=>'</div>'
                                                                   ));
                                                    echo $this->Form->input('is_guest',array('type'=>'checkbox','div'=>array('class'=>'form-group'),
                                                                    'label'=>false,
                                                                    'before'=>'<label class="col-md-3 control-label">Is Guest ?</label><div class="col-md-4">',
                                                                     'after'=>'</div>'
                                                                     ));
                                                   echo"<div id='panelGuest'>";
                                                   echo $this->Form->input('free_guest_credits',array('div'=>array('class'=>'form-group'),
                                                                                     'label'=>array('class'=>'col-md-3 control-label','text'=>'Free Guest Credits'),
                                                                                     'between'=>'<div class="col-md-4">',
                                                                                     'after'=>'</div>',
                                                                                     'class'=>'form-control'));
                                                     echo"</div>";
                                                     echo $this->Form->input('is_required',array('type'=>'checkbox','div'=>array('class'=>'form-group'),
                                                                     'label'=>false,
                                                                     'before'=>'<label class="col-md-3 control-label">Is Required ? </label><div class="col-md-4">',
                                                                     'after'=>'</div>'
                                                                      ));


                                           ?>
                                           <div class="form-actions fluid">
												<div class="col-md-offset-3 col-md-9">
													<button type="submit" class="btn blue">Submit</button>
													<!--<button type="button" class="btn default">Cancel</button>-->
												</div>
											</div>
                                         <?php echo $this->Form->end(); ?>
                                     </div>

										<!-- END FORM-->
									</div>
								</div>

							</div>



					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->

</body>
<?php echo $this->Html->script('//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js'); ?> 
<?php echo $this->Html->script('/assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js'); ?>
<?php echo $this->Html->script('/assets/admin/pages/scripts/components-pickers.js');   ?>
<script>
    jQuery(document).ready(function() {     
      ComponentsPickers.init(); 
    });
</script>
