<div class="packages view">
<h2><?php echo __('Package'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($package['Package']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Property'); ?></dt>
		<dd>
			<?php echo $this->Html->link($package['Property']['name'], array('controller' => 'properties', 'action' => 'view', $package['Property']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($package['Package']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Cost'); ?></dt>
		<dd>
			<?php echo h($package['Package']['cost']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Start Date'); ?></dt>
		<dd>
			<?php echo h($package['Package']['start_date']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Expiration Date'); ?></dt>
		<dd>
			<?php echo h($package['Package']['expiration_date']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Password'); ?></dt>
		<dd>
			<?php echo h($package['Package']['password']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Is Fixed Duration'); ?></dt>
		<dd>
			<?php echo h($package['Package']['is_fixed_duration']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Total Spaces Each Package'); ?></dt>
		<dd>
			<?php echo h($package['Package']['total_spaces_each_package']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Duration'); ?></dt>
		<dd>
			<?php echo h($package['Package']['duration']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Duration Type'); ?></dt>
		<dd>
			<?php echo h($package['Package']['duration_type']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Is Recurring'); ?></dt>
		<dd>
			<?php echo h($package['Package']['is_recurring']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Is Guest'); ?></dt>
		<dd>
			<?php echo h($package['Package']['is_guest']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Is Required'); ?></dt>
		<dd>
			<?php echo h($package['Package']['is_required']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Package'), array('action' => 'edit', $package['Package']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Package'), array('action' => 'delete', $package['Package']['id']), array(), __('Are you sure you want to delete # %s?', $package['Package']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Packages'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Package'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Properties'), array('controller' => 'properties', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Property'), array('controller' => 'properties', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Customer Passes'), array('controller' => 'customer_passes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer Pass'), array('controller' => 'customer_passes', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Vehicles'), array('controller' => 'vehicles', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Vehicle'), array('controller' => 'vehicles', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Customer Passes'); ?></h3>
	<?php if (!empty($package['CustomerPass'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th><?php echo __('Vehicle Id'); ?></th>
		<th><?php echo __('Property Id'); ?></th>
		<th><?php echo __('Transaction Id'); ?></th>
		<th><?php echo __('Status'); ?></th>
		<th><?php echo __('Membership Vaild Upto'); ?></th>
		<th><?php echo __('Pass Valid Upto'); ?></th>
		<th><?php echo __('RFID Tag Number'); ?></th>
		<th><?php echo __('Assigned Location'); ?></th>
		<th><?php echo __('Package Id'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($package['CustomerPass'] as $customerPass): ?>
		<tr>
			<td><?php echo $customerPass['id']; ?></td>
			<td><?php echo $customerPass['user_id']; ?></td>
			<td><?php echo $customerPass['vehicle_id']; ?></td>
			<td><?php echo $customerPass['property_id']; ?></td>
			<td><?php echo $customerPass['transaction_id']; ?></td>
			<td><?php echo $customerPass['status']; ?></td>
			<td><?php echo $customerPass['membership_vaild_upto']; ?></td>
			<td><?php echo $customerPass['pass_valid_upto']; ?></td>
			<td><?php echo $customerPass['RFID_tag_number']; ?></td>
			<td><?php echo $customerPass['assigned_location']; ?></td>
			<td><?php echo $customerPass['package_id']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'customer_passes', 'action' => 'view', $customerPass['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'customer_passes', 'action' => 'edit', $customerPass['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'customer_passes', 'action' => 'delete', $customerPass['id']), array(), __('Are you sure you want to delete # %s?', $customerPass['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Customer Pass'), array('controller' => 'customer_passes', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Vehicles'); ?></h3>
	<?php if (!empty($package['Vehicle'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Property Id'); ?></th>
		<th><?php echo __('Package Id'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th><?php echo __('Make'); ?></th>
		<th><?php echo __('Model'); ?></th>
		<th><?php echo __('Color'); ?></th>
		<th><?php echo __('License Plate Number'); ?></th>
		<th><?php echo __('License Plate State'); ?></th>
		<th><?php echo __('Reserved Space'); ?></th>
		<th><?php echo __('Last 4 Digital Of Vin'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($package['Vehicle'] as $vehicle): ?>
		<tr>
			<td><?php echo $vehicle['id']; ?></td>
			<td><?php echo $vehicle['property_id']; ?></td>
			<td><?php echo $vehicle['package_id']; ?></td>
			<td><?php echo $vehicle['user_id']; ?></td>
			<td><?php echo $vehicle['make']; ?></td>
			<td><?php echo $vehicle['model']; ?></td>
			<td><?php echo $vehicle['color']; ?></td>
			<td><?php echo $vehicle['license_plate_number']; ?></td>
			<td><?php echo $vehicle['license_plate_state']; ?></td>
			<td><?php echo $vehicle['reserved_space']; ?></td>
			<td><?php echo $vehicle['last_4_digital_of_vin']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'vehicles', 'action' => 'view', $vehicle['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'vehicles', 'action' => 'edit', $vehicle['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'vehicles', 'action' => 'delete', $vehicle['id']), array(), __('Are you sure you want to delete # %s?', $vehicle['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Vehicle'), array('controller' => 'vehicles', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
