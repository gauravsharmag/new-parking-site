<div class="packages form">
<?php echo $this->Form->create('Package'); ?>
	<fieldset>
		<legend><?php echo __('Edit Package'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('property_id');
		echo $this->Form->input('name');
		echo $this->Form->input('cost');
		echo $this->Form->input('start_date');
		echo $this->Form->input('expiration_date');
		echo $this->Form->input('password');
		echo $this->Form->input('is_fixed_duration');
		echo $this->Form->input('total_spaces_each_package');
		echo $this->Form->input('duration');
		echo $this->Form->input('duration_type');
		echo $this->Form->input('is_recurring');
		echo $this->Form->input('is_guest');
		echo $this->Form->input('is_required');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Package.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('Package.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Packages'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Properties'), array('controller' => 'properties', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Property'), array('controller' => 'properties', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Customer Passes'), array('controller' => 'customer_passes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer Pass'), array('controller' => 'customer_passes', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Vehicles'), array('controller' => 'vehicles', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Vehicle'), array('controller' => 'vehicles', 'action' => 'add')); ?> </li>
	</ul>
</div>
