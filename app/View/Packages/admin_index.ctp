

<?php echo $this->Html->script('jquery.min'); ?>
<script>

$(document).ready(function(){

if($("#PackageStartDate").val()=="1970-01-01 00:00:00")
    {
         $("#PackageStartDate").attr("value", "");
          $("#PackageExpirationDate").attr("value", "");

    }
  });


</script>

<div class="page-content-wrapper">
		<div class="page-content">
<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">Property <small><?php echo $propertyName;?></small>
					</h3>
					<ul class="page-breadcrumb breadcrumb">
                                <li class="btn-group">
            							<button type="button" class="btn blue dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true">
            							<span><?php $srNo=0; echo __('Actions'); ?></span><i class="fa fa-angle-down"></i>
            							</button>
            							<ul class="dropdown-menu pull-right" role="menu">
            								<li>
            									<a href="#"><?php echo $this->Html->link(__('Edit Property'), array('controller'=>'Properties','action' => 'edit',$propertyId,$propertyName)); ?> </a>
            								</li>


            								<li>
            									<a href="#"><?php echo $this->Html->link(__('Manage  Parking Locations'), array('controller' => 'Packages', 'action' => 'add',$propertyId,$propertyName)); ?></a>
            								</li>

            								<li>
            									<a href="#"><?php echo $this->Html->link(__('Manage Passes'), array('controller' => 'Passes', 'action' => 'add',$propertyId,$propertyName)); ?> </a>
            								</li>

            							</ul>
            				    </li>
                                <li>
                                    <i class="fa fa-home"></i>
                                         <?php echo $this->Html->link('Properties',array('controller'=>'properties','action'=>'index')); ?>
                                    <i class="fa fa-angle-right"></i>
                                </li>
                                 <li>
                                      <?php echo $this->Html->link($propertyName,array('controller'=>'properties','action'=>'view',$propertyId)); ?>
                                     <i class="fa fa-angle-right"></i>
                                 </li>
                                 <li>
                                        <?php echo __('All Parking Packages'); ?>
                                 </li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
            <div class="row">
				<div class="col-md-12">
                            <div id="flashMessages">
                               <?php echo $this->Session->flash();?>
                            </div>
					<!-- BEGIN EXAMPLE TABLE PORTLET-->
					<div class="portlet box grey-cascade">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-users"></i>Existing Parking Locations in <?php echo $propertyName;?>
							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								<?php /*?><a href="#portlet-config" data-toggle="modal" class="config">
								</a>
								<a href="javascript:;" class="reload">
								</a>
								<a href="javascript:;" class="remove">
								</a><?php */?>
							</div>
						</div>
						<div class="portlet-body">
							<div class="table-toolbar">
								<div class="btn-group">
									<?php echo $this->Html->link('Add New <i class="fa fa-plus"></i>',array('controller'=>'packages','action'=>'add',$propertyId,$propertyName),array('escape'=>false,'class'=>'btn green')); ?>
								</div>

							</div>

                            <?php $srNo=0;  ?>
                          <div class="table-responsive">
							<table class="table table-striped table-bordered table-hover">
							<thead>
							    <tr>
                                       <th><?php echo "Sr.No."; ?></th>
                                       <th><?php echo h('Name'); ?></th>
                                       <th><?php echo h('Cost'); ?></th>
                                       <th>From - To</th>
                                       <th><?php echo h('Pass Assigned'); ?></th>
                                       <th><?php echo h('Fixed Duration'); ?></th>
                                       <th><?php echo h('Duration'); ?></th>
                                       <th><?php echo h('Recurring'); ?></th>
                                       <th><?php echo h('Guest'); ?></th>
                                       <th><?php echo h('Free Guest Credits'); ?></th>
                                       <th><?php echo h('Required'); ?></th>
                                       <th><?php echo h('Total Space'); ?></th>
                                       <th><?php echo __('Actions'); ?></th>
                                 </tr>
							</thead>
							<tbody>
                           <?php //debug($packages);
                                        foreach ($packages as $package): ?>
                           	<tr>
<td><?php echo h(++$srNo); ?>&nbsp;</td>

                                                                                <td><?php echo h($package['Package']['name']); ?>&nbsp;</td>
                                                                           		<td><?php echo h($package['Package']['cost']); ?>&nbsp;</td>
                                                                           		<td><?php     
                                                                           		if($package['Package']['start_date']==null){ echo "NA";}else{
                                                                           		echo date("m/d/Y", strtotime($package['Package']['start_date']))."<br>"."To"."<br>".date("m/d/Y", strtotime($package['Package']['expiration_date']));
                                                                           		} ?></td>         		
                                                                                <td><?php
                                                                                          echo $this->requestAction(array('controller'=>'Passes','action'=>'getPassName',$package['Package']['pass_id']));
                                                                                            //echo h($package['Package']['password']); ?>&nbsp;</td>
                                                                           		<td><?php if($package['Package']['is_fixed_duration']==1){echo h('Yes');}else{echo h('No');} ?>&nbsp;</td>

                                                                           		<td><?php echo $package['Package']['duration'].' '.$package['Package']['duration_type']; ?>&nbsp;</td>
                                                                           	

                                                                           		<td><?php if($package['Package']['is_recurring']==1){echo h('Yes');}else{echo h('No');} ?>&nbsp;</td>

                                                                           		<td><?php if($package['Package']['is_guest']==1){echo h('Yes');}else{echo h('No');} ?>&nbsp;</td>
                                                                           		<td><?php echo h($package['Package']['free_guest_credits']); ?>&nbsp;</td>

                                                                           		<td><?php if($package['Package']['is_required']==1){echo h('Yes');}else{echo h('No');} ?>&nbsp;</td>
                                                                           		<td><?php echo h($package['Package']['total_spaces_each_package']); ?>&nbsp;</td>
                                                                           		<td class="actions">

                                                                           			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $package['Package']['id'],$propertyName,$propertyId),array('class' => 'btn green btn-xs green-stripe')); ?>
																					<?php
																							echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $package['Package']['id'],$propertyName,$propertyId), array('class' => 'btn red btn-xs red-stripe'), __(' Are you sure to delete this package ?', $package['Package']['id']));
																								//echo $this->Html->link(__('Delete'), array('action' => 'delete', $package['Package']['id'],$propertyName,$propertyId));
																						?>

                                                                           		</td>                           		
                           	</tr>
                           <?php endforeach; ?>

							</tbody>
							</table>
							<p>
                            	<?php
                            	echo $this->Paginator->counter(array(
                            	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
                            	));
                            	?>	</p>
                            	<div class="paging">
                            	    <?php
                            		    echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
                            		    echo $this->Paginator->numbers(array('separator' => ''));
                            		    echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
                            	    ?>
                                </div>
                          </div>
						</div>
					</div>
					<!-- END EXAMPLE TABLE PORTLET-->
				</div>
			</div>

