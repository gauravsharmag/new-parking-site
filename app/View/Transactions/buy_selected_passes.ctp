<style>
.error {
    color: red;
}
</style>
<script>
netTotalAmount=parseInt(<?php echo $totalAmount; ?>);
</script>
<div class="page-content-wrapper">
		<div class="page-content" style="min-height:996px">
			
			<div id="flashMessages">
									<h2><?php  echo $this->Session->flash();?></h2>
			</div>
			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					<?php //debug($this->Session->params);
					//debug($this->Session->read('Auth'));
					//debug($this->Session->read('PropertyId')['Property']['id']);
					//debug($this->Session->read('PropertyName'));
					?>
					<?php echo $this->Session->read('PropertyName')." ";?>Passes <small></small>
					</h3>
					<ul class="page-breadcrumb breadcrumb">

						<li>
							<i class="fa fa-home"></i>
							<?php echo $this->Html->link(
                             'Home',
                              array(
                                    'controller' => 'Users',
                                    'action' => 'myAccount',
                                    'full_base' => true
                                                                                              )
                              );?>
							<i class="fa fa-angle-right"></i>
						</li>

						<li>
							<a href="#">Buy Passes</a>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>

			<!-- END PAGE HEADER-->
			<div id="flashMessages">
									<h2><?php  echo $this->Session->flash();?></h2>
			</div>
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- Begin: life time stats -->
					<div class="portlet">
						<div class="portlet-title">
							<div class="table-toolbar">
									<a href="/Transactions/select_passes"><button class="btn green"><i class="m-icon-swapleft m-icon-white"></i>  Back</button></a>
							</div>
						</div>
						<div class="portlet-body">
							<div class="tabbable">
								<ul class="nav nav-tabs nav-tabs-lg">
									<li class="active">
										<a data-toggle="tab" href="#tab_1">
										   Passes
                                        </a>
									</li>


								</ul>
								<div class="tab-content">
									<div id="tab_1" class="tab-pane active">
											<div class="portlet box yellow">
												<div class="portlet-title">
													<div class="caption">
														<i class="fa fa-building"></i> Passes
													</div>
													<div class="tools">
														<a class="collapse" href="javascript:;">
														</a>

													</div>

												</div>
												<div class="portlet-body">
													<div class="table-responsive">
														<table class="table table-striped table-bordered table-hover">
															<thead>
																 <tr>
																	<th>#</th>
																	<th>Pass Name</th>
																	<th>Required</th>
																	<th>Guest Pass</th>
																	<th>Deposit</th>
																	<th>Cost</th>
																	<th>Expiry Date</th>
																	<th>Parking Location Name</th>
																	<th>Parking Location Cost</th>
																	<th>Total Amount</th>
																 </tr>
															</thead>
															<tbody>
																<?php $i=1;foreach($returnArray as $pass){?>
																	<tr>
																		<td>
																		  <?php echo $i++;?>
																		</td>
																		<td>
																			<?php echo $pass['Pass']['passName'];?>
																		</td>
																		<td>
																			<?php echo $pass['Pass']['required'];?>
																		</td>
																		 <td>
																			<?php 
																				  if($pass['Pass']['isGuest']=="Yes"){
																					echo '<span class="label label-sm label-danger">Guest Pass</span>';
																				  }else{
																					echo $pass['Pass']['isGuest'];
																				  }
																			?>
																		</td>
																		<td>
																			<?php echo '$ '.$pass['Pass']['passDeposit'];?>
																		</td>
																		<td>
																			<?php echo '$ '.$pass['Pass']['cost'];?>
																		</td>
																		<td>
																			<?php echo $pass['Pass']['expiryDate']; ?>
																		</td>
																		 <td>
																			<?php echo $pass['Pass']['packageName']; ?>
																		</td>
																		 <td>
																			<?php 
																				  if($pass['Pass']['packageCost']!=="NA"){
																					echo $pass['Pass']['packageCost'];
																				  }else{
																					echo $pass['Pass']['packageCost']; 
																				  }
																			?>
																		</td>
																		 <td>
																			<?php echo '$ '.$pass['Pass']['passCost']; ?>
																		</td>
																	</tr>
																<?php }?>
																	<tr>
																		<td></td>
																		<td></td>
																		<td></td>
																		<td></td>
																		<td></td>
																		<td></td>
																		<td></td>
																		<td></td>
																		<th>Total Amount</th>
																		<th id='orgAmt'><?php echo '$ '.$totalAmount;?></th>
																	</tr>
																	<tr>
																		<td></td>
																		<td></td>
																		<td></td>
																		<td></td>
																		<td></td>
																		<td></td>
																		<td></td>
																		<td></td>
																		<th>Discount</th>
																		<th id='discountAmt'><?php echo '$ 0';?></th>
																	</tr>
																	<tr>
																		<td></td>
																		<td></td>
																		<td></td>
																		<td></td>
																		<td></td>
																		<td></td>
																		<td></td>
																		<td></td>
																		<th>Net Amount Payable</th>
																		<th id='totalAmt'><?php echo '$ '.$totalAmount;?></th> 
																	</tr>
															</tbody>
														</table>
													</div>
												</div>
											</div>

									             <?php echo $this->Form->create('Transaction',array('class'=>'form-horizontal'));?>
														  <div class="row">
																	<div class="col-md-6 col-sm-6">
																		<div class="portlet box green">
																			<div class="portlet-title">
																				<div class="caption">
																					<i class="fa fa-gift"></i>Billing Address
																				</div>
																				<div class="tools">
																					<a href="javascript:;" class="collapse">
																					</a>

																				</div>
																			</div>
																			<div class="portlet-body form">
																				<div class="form-body">
																							
																					<div class="form-group">
																						<label class="col-md-4 control-label">First Name</label>
																						<div class="col-md-6">
																						 <?php
																							echo $this->Form->input('first_name',array('label'=>false,
																																		'errorMessage' => false,
																																		'value'=>AuthComponent::user('first_name'),
																																		'class'=>'form-control'));
																							if ($this->Form->isFieldError('first_name')) {
																										 echo $this->Form->error('first_name',array('div'=>false,'attributes'=>array('wrap' => 'span', 'class' => 'model-error')));

																						 }?>
																						 </div>
																					</div>
																					<div class="form-group">
																						<label class="col-md-4 control-label">Last Name</label>
																						<div class="col-md-6">
																						 <?php
																							echo $this->Form->input('last_name',array('label'=>false,
																																		'errorMessage' => false,
																																		 'value'=>AuthComponent::user('last_name'),
																																		'class'=>'form-control'));
																							if ($this->Form->isFieldError('last_name')) {
																										 echo $this->Form->error('last_name',array('div'=>false,'attributes'=>array('wrap' => 'span', 'class' => 'model-error')));

																						 }?>
																						 </div>
																					</div>
																					<div class="form-group">
																						  <label class="col-md-4 control-label">Email</label>
																						  <div class="col-md-6">
																						   <?php
																							echo $this->Form->input('email',array('label'=>false,
																																		'errorMessage' => false,
																																		'value'=>AuthComponent::user('email'),
																																		'class'=>'form-control'));
																							  if ($this->Form->isFieldError('email')) {
																										   echo $this->Form->error('email',array('div'=>false,'attributes'=>array('wrap' => 'span', 'class' => 'model-error')));

																						   }?>
																						  </div>
																					  </div>
																					  <div class="form-group">
																						  <label class="col-md-4 control-label">Address Line 1</label>
																						  <div class="col-md-6">
																						   <?php	
																							echo $this->Form->input('address_line_1',array('label'=>false,
																																		'errorMessage' => false,
																																		'value'=>AuthComponent::user('address_line_1'),
																																		'class'=>'form-control'));
																							  if ($this->Form->isFieldError('address_line_1')) {
																										   echo $this->Form->error('address_line_1',array('div'=>false,'attributes'=>array('wrap' => 'span', 'class' => 'model-error')));

																						   }?>
																						  </div>
																					  </div>
																					  <div class="form-group">
																						  <label class="col-md-4 control-label">Apartment\Suite #</label>
																						  <div class="col-md-6">
																						   <?php
																							echo $this->Form->input('address_line_2',array('label'=>false,
																																		'errorMessage' => false,
																																		'value'=>AuthComponent::user('address_line_2'),
																																		'class'=>'form-control'));
																							  if ($this->Form->isFieldError('address_line_2')) {
																										   echo $this->Form->error('address_line_2',array('div'=>false,'attributes'=>array('wrap' => 'span', 'class' => 'model-error')));

																						   }?>
																						  </div>
																					  </div>
																					  <div class="form-group">
																						  <label class="col-md-4 control-label">City</label>
																						  <div class="col-md-6">
																						   <?php
																							echo $this->Form->input('city',array('label'=>false,
																																		'errorMessage' => false,
																																		'value'=>AuthComponent::user('city'),
																																		'class'=>'form-control'));
																							  if ($this->Form->isFieldError('city')) {
																										   echo $this->Form->error('city',array('div'=>false,'attributes'=>array('wrap' => 'span', 'class' => 'model-error')));

																						   }?>
																						  </div>
																					  </div>
																					  <div class="form-group">
																						  <label class="col-md-4 control-label">State</label>
																						   <div class="col-md-6">
																							<?php
																								 echo $this->Form->input('state',array('type' => 'select',
																																		'label'=>false,
																																		'options' => $states,
																																		'value'=>AuthComponent::user('state'),
																																		'empty'=>'State',
																																		'errorMessage' => false,
																																		'class'=>'form-control'));
																								if ($this->Form->isFieldError('state')) {
																										   echo $this->Form->error('state',array('div'=>false,'attributes'=>array('wrap' => 'span', 'class' => 'model-error')));

																							}?>
																						   </div>
																					  </div>
																					  <div class="form-group">
																						  <label class="col-md-4 control-label">Zip</label>
																						  <div class="col-md-6">
																						   <?php
																							echo $this->Form->input('zip',array('label'=>false,
																																		'errorMessage' => false,
																																		'value'=>AuthComponent::user('zip'),
																																		'class'=>'form-control'));
																							  if ($this->Form->isFieldError('zip')) {
																										   echo $this->Form->error('zip',array('div'=>false,'attributes'=>array('wrap' => 'span', 'class' => 'model-error')));

																						   }?>
																						  </div>
																					  </div>
																					  <div class="form-group">
																						  <label class="col-md-4 control-label">Phone</label>
																						  <div class="col-md-6">
																						   <?php
																							echo $this->Form->input('phone',array('label'=>false,
																																		'errorMessage' => false,
																																		'value'=>AuthComponent::user('phone'),
																																		'class'=>'form-control'));
																							  if ($this->Form->isFieldError('phone')) {
																										   echo $this->Form->error('phone',array('div'=>false,'attributes'=>array('wrap' => 'span', 'class' => 'model-error')));

																						   }?>
																						  </div>
																					  </div>
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="col-md-6 col-sm-6">
																	<?php if($totalAmount>0){?>
																		<div class="portlet box blue">
																		<div class="portlet-title">
																			<div class="caption">
																				<i class="fa fa-gift"></i>Credit Card Details
																			</div>
																			<div class="tools">
																				<a href="javascript:;" class="collapse">
																				</a>

																			</div>
																		</div>
																		<div class="portlet-body form">
																			<div class="form-body">

																			<?php
																				  for($i=1;$i<=12;$i++){
																					  if($i<10){
																						$month['0'.$i]='0'.$i;
																					  }else{
																						$month[$i]=$i;
																					  }
																				  }
																				  $dt = new DateTime();
																				  $currentYear= (int)$dt->format('Y');
																				  for($i=$currentYear;$i<=$currentYear+20;$i++){$year[$i]=$i;}
																			 ?>
																					<div class="form-group">
																						<label class="col-md-4 control-label">Coupon</label> 
																						<div class="col-md-4">
																						 <?php
		
																							echo $this->Form->input('coupon',array('label'=>false,
																																		'errorMessage' => false,
																																		'placeholder'=>'Enter Coupon Code',
																																		'class'=>'form-control'));
																							if ($this->Form->isFieldError('coupon')) {
																										 echo $this->Form->error('coupon',array('div'=>false,'attributes'=>array('wrap' => 'span', 'class' => 'model-error')));

																						 }?>
																						 <span id="couponBlock" class="help-block"></span>
																						 </div>
																						 <div class="col-md-3">
																							 <a id="applyCoupon" class="btn btn-primary" href='' >Apply</a>
																						 </div>
																					</div>
																					
																					<div class="form-group">
																						<label class="col-md-4 control-label">Card Number</label>
																						<div class="col-md-6">
																						 <?php
																							echo $this->Form->input('card_number',array('label'=>false,
																																		'errorMessage' => false,
																																		'class'=>'form-control'));
																							if ($this->Form->isFieldError('card_number')) {
																										 echo $this->Form->error('card_number',array('div'=>false,'attributes'=>array('wrap' => 'span', 'class' => 'model-error')));

																						 }?>
																						 </div>
																					</div>
																					<div class="form-group">
																						<label class="col-md-4 control-label">CVV</label>
																						<div class="col-md-6">
																							<?php
																							echo $this->Form->input('cvv',array('label'=>false,
																																		'errorMessage' => false,
																																		'class'=>'form-control'));
																							if ($this->Form->isFieldError('cvv')) {
																										 echo $this->Form->error('cvv',array('div'=>false,'attributes'=>array('wrap' => 'span', 'class' => 'model-error')));

																							}?>
																						</div>
																					</div>
																					<div class="form-group">

																						 <table>
																							<?php
																									  echo "<tr>";
																										 ?>
																										<label class="col-md-4 control-label">Expiry Date</label>
																										 <?php
																										 echo "<td>";
																											echo $this->Form->input('month',array('type' => 'select',
																																	'label'=>array('class'=>'col-md-1 control-label','text'=>''),
																																	'options' => $month,
																																	 'empty'=>'Month',
																																	 ));
																										 echo "</td>";
																										 echo "<td>";
																											echo $this->Form->input('year',array('type' => 'select',
																																	 'label'=>array('class'=>'col-md-1 control-label','text'=>''),
																																	 'options' => $year,
																																	 'empty'=>'Year',
																																	 ));
																										 echo "</td>";
																									   echo "</tr>";
																									  ?>
																						  </table>
																					</div>
																					<div class="form-group">
																						<label class="col-md-4 control-label">First Name</label>
																						<div class="col-md-6">
																							<?php
																							echo $this->Form->input('first_name_cc',array('label'=>false,
																																		'errorMessage' => false,
																																		'class'=>'form-control'));
																							if ($this->Form->isFieldError('first_name_cc')) {
																										 echo $this->Form->error('first_name_cc',array('div'=>false,'attributes'=>array('wrap' => 'span', 'class' => 'model-error')));

																							}?>
																						</div>
																					</div>
																					<div class="form-group">
																						<label class="col-md-4 control-label">Last Name</label>
																						<div class="col-md-6">
																							<?php
																							echo $this->Form->input('last_name_cc',array('label'=>false,
																																		'errorMessage' => false,
																																		'class'=>'form-control'));
																							if ($this->Form->isFieldError('last_name_cc')) {
																										 echo $this->Form->error('last_name_cc',array('div'=>false,'attributes'=>array('wrap' => 'span', 'class' => 'model-error')));

																							}?>
																						</div>
																					</div>
																				</div>
																			</div>
																		</div>

																<?php }?>
																<div class="well">
																	<div class="row">
																		<div class="col-md-6">
																			 <div class="col-md-offset-3 col-md-9">
																				  <button class="btn green" type="submit">Check Out</button>
																			 </div>

																		</div>
																	</div>
																</div>
													  </div>

												<?php echo $this->Form->end(); ?>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- End: life time stats -->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
<script>
	$( document ).ready(function() {
		if($('#TransactionCoupon').val()){
			$("#discountAmt").html('$ 0');
		    $("#totalAmt").html('$ '+netTotalAmount);
		    $("#orgAmt").html('$ '+netTotalAmount);
		    $("#couponBlock").html('<font color="red">Applying Coupon, Please Wait</font>');
			$.ajax({
              url: '/TableCoupons/getCouponDetails',
              type:'POST',
              data:{'code':$('#TransactionCoupon').val()},
              success: function (r) {
				if(r=='Invalid Coupon'){
					$("#couponBlock").html('<font color="red">Invalid Coupon</font>');
				}else if(r=='No Coupon Exist'){
					$("#couponBlock").html('<font color="red">No Such Coupon Exists</font>');
				}else if(r=='false'){
					$("#couponBlock").html('<font color="red">Please Enter Coupon Code</font>');
				}else{
					discount= parseFloat((parseInt(r)/100)*netTotalAmount);
					discount=discount.toFixed(2);
					newAmount= netTotalAmount-discount;
					$("#totalAmt").html('$ '+newAmount);
					$("#discountAmt").html('$ '+discount);
					$("#couponBlock").html('<font color="red">Coupon Applied</font>');
				}
            }
         });
		}
	});
	$("#applyCoupon").click(function(event){
		event.preventDefault();
		$("#discountAmt").html('$ 0');
		$("#totalAmt").html('$ '+netTotalAmount);
		$("#couponBlock").html('<font color="red">Applying Coupon, Please Wait</font>');
		if($('#TransactionCoupon').val()){
			$.ajax({
              url: '/TableCoupons/getCouponDetails',
              type:'POST',
              data:{'code':$('#TransactionCoupon').val()},
              success: function (r) {
				if(r=='Invalid Coupon'){
					$("#couponBlock").html('<font color="red">Invalid Coupon</font>');
				}else if(r=='No Coupon Exist'){
					$("#couponBlock").html('<font color="red">No Such Coupon Exists</font>');
				}else if(r=='false'){
					$("#couponBlock").html('<font color="red">Please Enter Coupon Code</font>');
				}else if(r=='Coupon Expired 1'){
					$("#couponBlock").html('<font color="red">Coupon Expired</font>');
				}else if(r=='Coupon Expired 2'){
					$("#couponBlock").html('<font color="red">Coupon Expired</font>');
				}else if(r=='Coupon Expired 3'){
					$("#couponBlock").html('<font color="red">Coupon Expired</font>');
				}else{
					discount= parseFloat((parseInt(r)/100)*netTotalAmount);
					discount=discount.toFixed(2);
					newAmount= netTotalAmount-discount;
					$("#totalAmt").html('$ '+newAmount);
					$("#discountAmt").html('$ '+discount);
					$("#couponBlock").html('<font color="red">Coupon Applied</font>');
				}
            }
         });
       }else{
		$("#couponBlock").html('<font color="red">Please Enter Coupon Code</font>');
	   }
});
 $('#TransactionCoupon').change(function(){
			if($(this).val()=='') { 
				$("#discountAmt").html('$ 0');
				$("#totalAmt").html('$ '+netTotalAmount);
				$("#couponBlock").html('');
			}	
		}); 
</script>
<?php 
	echo $this->Html->script('block.js'); 
	echo $this->Html->script('jquery.validate.min.js'); 
?>
<script>
	function postForm(){
		$('#TransactionPassRenewForm').submit();
	}
	$("#TransactionBuySelectedPassesForm").validate({
				rules: {
				"data[Transaction][first_name]": {
					required: true,
				},
				"data[Transaction][last_name]": {
					required: true,
				},
				"data[Transaction][email]":{
					required: true,
					email:true
				},
				"data[Transaction][address_line_1]": {
					required: true,
				},
				"data[Transaction][city]": {
					required: true,
				},
				"data[Transaction][state]":{
					required: true,
				},
				"data[Transaction][zip]":{
					required: true,
				},
				"data[Transaction][phone]":{
					required: true,
				},
				"data[Transaction][card_number]":{
					required: true,
					minlength: 15,
					number:true
				},
				"data[Transaction][cvv]":{
					required: true,
					number:true,
					//detectCardType:true
				},
				"data[Transaction][month]":{
					required: true,
				},
				"data[Transaction][year]":{
					required: true,
				},
				"data[Transaction][first_name_cc]": {
					required: true,
				},
				"data[Transaction][last_name_cc]": {
					required: true,
				}
			},
		   
			messages: {
				"data[Transaction][first_name]": { required: "Please enter first name.",
				},
				"data[Transaction][last_name]": { required: "Please enter last name.",
				},
				"data[Transaction][email]": { required:  "Please enter valid email.",email:  "Please enter valid email."
				},
				"data[Transaction][address_line_1]": { required: "Address line is required.",
				},
				"data[Transaction][city]": { required: "City is required.",
				},
				"data[Transaction][state]": { required:  "State is required.",
				},
				"data[Transaction][zip]": { required:  "Zip is required.",
				},
				"data[Transaction][phone]": { required:  "Phone is required.",
				},
				"data[Transaction][card_number]": { required:  "Card number is required.",
				},
				"data[Transaction][cvv]": { required:  "Cvv is required.",//detectCardType:"Please add a valid cvv"
				},
				"data[Transaction][month]": { required:  "Month is required.",
				},
				"data[Transaction][year]": { required:  "Year is required.",
				},
				"data[Transaction][first_name_cc]": { required: "Please enter first name.",
				},
				"data[Transaction][last_name_cc]": { required: "Please enter last name.",
				}
			}, submitHandler: function (form) {
					$.blockUI({
						message: 'Processing payment. Please wait',
						css: {
							border: 'none',
							padding: '15px',
							backgroundColor: '#000',
							'-webkit-border-radius': '10px',
							'-moz-border-radius': '10px',
							opacity: .5,
							color: '#fff'
					}});
					form.submit();
			}
		});
		 jQuery.validator.addMethod('detectCardType', function (value, element) {

				// The two password inputs
				var number = $("#TransactionCardNumber").val();
				var cvv = $("#TransactionCvv").val();
				var cards = {
					visa: /^4[0-9]{12}(?:[0-9]{3})?$/,
					mastercard: /^5[1-5][0-9]{14}$/,
					amex: /^3[47][0-9]{13}$/,
					diners: /^3(?:0[0-5]|[68][0-9])[0-9]{11}$/,
					discover: /^6(?:011|5[0-9]{2})[0-9]{12}$/,
					jcb: /^(?:2131|1800|35\d{3})\d{11}$/,
					dankort:/^5019\d{12}$/,
					switch:/^6759\d{12}(\d{2,3})?$/,
					solo: /^6767\d{12}(\d{2,3})?$/,
					forbrugsforeningen:/^600722\d{10}$/,
					laser:/^(6304|6706|6709|6771(?!89))\d{8}(\d{4}|\d{6,7})?$/
				};
				for (var card in cards) {
					if (cards[card].test(number)) {
					   if(card === "amex"){
						  if($("#TransactionCvv").val().length == 4 ){
							  return true;
						  }else{
							 return false; 
						  }                  
					   }else{
						if($("#TransactionCvv").val().length == 3 ){
							   return true;
						  }else{
							 return false; 
						  }             
					   }                 
					}
				}
				 if($("#TransactionCvv").val().length == 3 ){
                       return true;
                  }else{
                     return false; 
                  } 
    });
</script>
