<div class="table-responsive">
			  <table id="view_rfid_list" class="table table-striped table-bordered table-hover">
				 <thead>
					<tr>
					   <th>Sr. No.</th>
					   <th>Pass Name</th>
					   <th>Total Renewal</th>
					   <th>Free Renewal</th>
					   <th>Paid Renewal</th>
					   <th>Amount</th>
					</tr>
				  </thead>
				  <tbody>
					  <?php $array=$finalArr;
							$sr=1;
							$totalRenewal=$paidRenewal=$freeRenewal=$totalCost=0;
							for($i=0;$i<count($array);$i++){ ?>
						   <tr>
								<td align="left">
									<?php echo $sr++; ?>
								</td>
								<td>
									<?php echo $array[$i]['PassName']; ?>
								</td>
								<td align="left">
									<?php $totalRenewal=$totalRenewal+($array[$i]['FreePassCount']+$array[$i]['PaidPassCount']);
										echo $array[$i]['FreePassCount']+$array[$i]['PaidPassCount']; ?>
								</td>
								<td align="left">
									<?php  $freeRenewal=$freeRenewal+$array[$i]['FreePassCount'];
											echo $array[$i]['FreePassCount']; ?>
								</td>
								<td align="left">
									<?php  $paidRenewal=$paidRenewal+$array[$i]['PaidPassCount'];
											echo $array[$i]['PaidPassCount']; ?>
								</td>
								<td align="left">
									<?php echo '$ '.$array[$i]['PaidPassAmount']; 
										  $totalCost=$totalCost+$array[$i]['PaidPassAmount'];
									?>
								</td>
							</tr>
						<?php } ?>
						<tr>
							<td></td>
							<td><b>TOTAL</b></td>
							<td><b><?php echo $totalRenewal; ?></b></td>
							<td><b><?php echo $freeRenewal; ?></b></td>
							<td><b><?php echo $paidRenewal; ?></b></td>
							<td><b><?php echo '$ '.$totalCost; ?></b></td>
						</tr>
				   </tbody>
				</table>
				
		</div>
