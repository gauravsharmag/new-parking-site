<style>
.error {
    color: red;
}
</style>
<div class="page-content-wrapper">
		<div class="page-content" style="min-height:996px">	
			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					Order View <small>view order details</small>
					</h3>
					<ul class="page-breadcrumb breadcrumb">

						<li>
							<i class="fa fa-home"></i>
							<?php echo $this->Html->link(
                                                         'Home',
                                                          array(
                                                                'controller' => 'Users',
                                                                'action' => 'myAccount',
                                                                'full_base' => true
                                                                                                                          )
                                                          );?>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">Pass View</a>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
			<div id="flashMessages">
									<h2><?php  echo $this->Session->flash();?></h2>
			</div>
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- Begin: life time stats -->
					<div class="portlet">
						<div class="portlet-title">

						</div>
						<div class="portlet-body">
							<div class="tabbable">
								<ul class="nav nav-tabs nav-tabs-lg">
									<li class="active">
										<a data-toggle="tab" href="#tab_1">
										Details </a>
									</li>
								</ul>
<!-----------------------------TAB Start------------------------------------------------------------------------------------------>								
								<div class="tab-content">
									<div id="tab_1" class="tab-pane active">					
<!-----------------------------TAB Content Start------------------------------------------------------------------------------------------>		
										<div class="row">
											
											 <?php if($renewAllowed){ 
														echo $this->Form->create('Transaction',array('class'=>'form-horizontal'));
													?>
											  <div class="col-md-6 col-sm-6">
															<div class="portlet box green">
                                            					<div class="portlet-title">
                                            						<div class="caption">
                                            					    	<i class="fa fa-gift"></i>Billing Address
                                            						</div>
                                            						<div class="tools">
																		<a href="javascript:;" class="collapse"></a>
																	</div>
																</div>
                                            					<div class="portlet-body form">
																	<div class="form-body">
																	<?php
																	echo $this->Form->input('first_name',array('div'=>array('class'=>'form-group'),
                                                                                            'value'=>$billingAddress['BillingAddress']['first_name'],
                                                                                            'label'=>array('class'=>'col-md-4 control-label','text'=>'First Name'),
                                                                                            'between'=>'<div class="col-md-6">',
                                                                                            'after'=>'</div>',
                                                                                            'error'=>array('attributes'=>array('class'=>'model-error')),
                                                                                            'class'=>'form-control'));
                                                                    
                                                                    echo $this->Form->input('last_name',array('div'=>array('class'=>'form-group'),
                                                                                            'value'=>$billingAddress['BillingAddress']['last_name'],
                                                                                            'label'=>array('class'=>'col-md-4 control-label','text'=>'Last Name'),
                                                                                            'between'=>'<div class="col-md-6">',
                                                                                            'after'=>'</div>',
                                                                                            'error'=>array('attributes'=>array('class'=>'model-error')),
                                                                                            'class'=>'form-control'));
																	echo $this->Form->input('email',array('type'=>'email','div'=>array('class'=>'form-group'),
                                                                                              'value'=>$billingAddress['BillingAddress']['email'],
                                                                                              'label'=>array('class'=>'col-md-4 control-label','text'=>'Email'),
                                                                                              'between'=>'<div class="col-md-6">',
                                                                                              'after'=>'</div>',
                                                                                              'error'=>array('attributes'=>array('class'=>'model-error')),
                                                                                              'class'=>'form-control'));
                                                                                              
                                                                    echo $this->Form->input('address_line_1',array('div'=>array('class'=>'form-group'),
                                                                                           'value'=>$billingAddress['BillingAddress']['address_line_1'],
                                                                                           'label'=>array('class'=>'col-md-4 control-label','text'=>'Address Line 1'),
                                                                                           'between'=>'<div class="col-md-6">',
                                                                                           'after'=>'</div>',
                                                                                           'error'=>array('attributes'=>array('class'=>'model-error')),
                                                                                           'class'=>'form-control'));
                                                                    echo $this->Form->input('address_line_2',array('div'=>array('class'=>'form-group'),
                                                                                          'value'=>$billingAddress['BillingAddress']['address_line_2'],
                                                                                          'label'=>array('class'=>'col-md-4 control-label','text'=>'Apartment\Suite #'),
                                                                                          'between'=>'<div class="col-md-6">',
                                                                                          'after'=>'</div>',
                                                                                          'error'=>array('attributes'=>array('class'=>'model-error')),
                                                                                          'class'=>'form-control'));

																	echo $this->Form->input('city',array('div'=>array('class'=>'form-group'),
                                                                                            'value'=>$billingAddress['BillingAddress']['city'],
                                                                                            'label'=>array('class'=>'col-md-4 control-label','text'=>'City'),
																							'value'=>$billingAddress['BillingAddress']['city'],
                                                                                            'between'=>'<div class="col-md-6">',
                                                                                            'error'=>array('attributes'=>array('class'=>'model-error')),
                                                                                            'after'=>'</div>',
                                                                                            'class'=>'form-control'));																								                                                                                                  
																	echo $this->Form->input('state',array('type' => 'select',
                                                                                            'div'=>array('class'=>'form-group'),
                                                                                            'label'=>array('class'=>'col-md-4 control-label','text'=>'State'),
                                                                                            'value'=>$billingAddress['BillingAddress']['state'],
                                                                                            'options' => $states,
                                                                                            'empty'=>'State',
                                                                                            'between'=>'<div class="col-md-6">',
                                                                                            'after'=>'</div>',
                                                                                            'error'=>array('attributes'=>array('class'=>'model-error')),
                                                                                            'class'=>'form-control'));

                                                                     echo $this->Form->input('zip',array('div'=>array('class'=>'form-group'),
                                                                                             'value'=>$billingAddress['BillingAddress']['zip'],
                                                                                             'label'=>array('class'=>'col-md-4 control-label','text'=>'Zip'),
                                                                                             'between'=>'<div class="col-md-6">',
                                                                                             'after'=>'</div>',
                                                                                             'error'=>array('attributes'=>array('class'=>'model-error')),
                                                                                             'class'=>'form-control'));
                                                                      echo $this->Form->input('phone',array('div'=>array('class'=>'form-group'),
                                                                                              'value'=>$billingAddress['BillingAddress']['phone'],
                                                                                              'label'=>array('class'=>'col-md-4 control-label','text'=>'Phone'),
                                                                                              'between'=>'<div class="col-md-6">',
                                                                                              'after'=>'</div>',
                                                                                              'error'=>array('attributes'=>array('class'=>'model-error')),
                                                                                              'class'=>'form-control'));
																	?>	
																	 </div>		
																</div>
															</div>
											  </div>
											  <div class="col-md-6 col-sm-6">
												<div class="portlet blue-hoki box">
													<div class="portlet-title">
														<div class="caption">
															<i class="fa fa-cogs"></i>Pass Details
														</div>
													</div>
													<div class="portlet-body">
														<div class="row static-info">
														<div class="col-md-5 name">
																Name:
															</div>
															<div class="col-md-7 value">
																<?php echo $passName;?>
															</div>
														</div>
														<div class="row static-info">
															<div class="col-md-5 name">
																Renewal Cost:
															</div>
															<div class="col-md-7 value">
																<?php echo "$ ".$passRenewalcost?>
															</div>
														</div>
														<div class="row static-info">
															<div class="col-md-5 name">
																Expiry date:
															</div>
															<div class="col-md-7 value">
																<?php echo $passExpiryDate;?>
															</div>
														</div>
													</div>
												</div>
												<div class="portlet box blue">
                                            	  <div class="portlet-title">
                                            			<div class="caption">
                                            				<i class="fa fa-gift"></i>Credit Card Details
                                            			</div>
                                            			<div class="tools">
                                            				<a href="javascript:;" class="collapse"></a>
                                            			</div>
                                            		</div>
                                            		<div class="portlet-body form">
															<div class="form-body">	
																<?php
                                            						for($i=1;$i<=12;$i++){$month[$i]=$i;}
                                            						$dt = new DateTime();
                                                                    $currentYear= (int)$dt->format('Y');
                                                                    for($i=$currentYear;$i<=$currentYear+20;$i++){$year[$i]=$i;}
                                            						echo $this->Form->input('card_number',array('div'=>array('class'=>'form-group'),
                                                                                            'label'=>array('class'=>'col-md-4 control-label','text'=>'Card Number'),
                                                                                            'between'=>'<div class="col-md-6">',
                                                                                             'error'=>array('attributes'=>array('class'=>'model-error')),
                                                                                            'after'=>'</div>',
                                                                                            'class'=>'form-control'));

                                                                    echo $this->Form->input('cvv',array('div'=>array('class'=>'form-group'),
                                                                                            'label'=>array('class'=>'col-md-4 control-label','text'=>'CVV'),
                                                                                            'between'=>'<div class="col-md-6">',
                                                                                            'error'=>array('attributes'=>array('class'=>'model-error')),
                                                                                            'after'=>'</div>',
                                                                                            'class'=>'form-control'));
                                                                 ?>
																 <div class="form-group">
                                                                      <table>
                                                                      <?php
                                                                          echo "<tr>";
                                                                      ?>
                                                                      <label class="col-md-4 control-label">Expiry Date</label>
                                                                      <?php
                                                                           echo "<td>";
                                                                           echo $this->Form->input('month',array('type' => 'select',
                                                                                                   'label'=>array('class'=>'col-md-1 control-label','text'=>''),
                                                                                             	   'options' => $month,
                                                                                             	    'error'=>array('attributes'=>array('class'=>'model-error')),
                                                                                                   'empty'=>'Month',
                                                                                                   ));
                                                                           echo "</td>";
                                                                           echo "<td>";
                                                                           echo $this->Form->input('year',array('type' => 'select',
                                                                                                   'label'=>array('class'=>'col-md-1 control-label','text'=>''),
                                                                                                   'options' => $year,
                                                                                                    'error'=>array('attributes'=>array('class'=>'model-error')),
                                                                                                   'empty'=>'Year',
                                                                                                   ));
                                                                          echo "</td>";
                                                                          echo "</tr>";
                                                                        ?>
                                                                        </table>
                                                                  </div>
                                                                  <?php
                                                                       echo $this->Form->input('first_name_cc',array('div'=>array('class'=>'form-group'),
                                                                                                'label'=>array('class'=>'col-md-4 control-label','text'=>'First Name'),
                                                                                                'between'=>'<div class="col-md-6">',
                                                                                                 'error'=>array('attributes'=>array('class'=>'model-error')),
                                                                                                 'after'=>'</div>',
                                                                                                 'class'=>'form-control'));
                                                                       echo $this->Form->input('last_name_cc',array('div'=>array('class'=>'form-group'),
                                                                                               'label'=>array('class'=>'col-md-4 control-label','text'=>'Last Name'),
                                                                                                'between'=>'<div class="col-md-6">',
                                                                                                 'error'=>array('attributes'=>array('class'=>'model-error')),
                                                                                                'after'=>'</div>',
                                                                                                'class'=>'form-control'));
                                                                 ?>
                                                                 <div class="row">
                                                                       <div class="col-md-6">
                                                                           <div class="col-md-offset-3 col-md-9">
                                                                              <button class="btn green" type="submit"  >Check Out</button>
                                                                           </div>
                                                                           <?php echo $this->Form->end(); }?>
                                                                       </div>
                                                                 </div>
															</div>
														</div>
												</div>												
											  </div> 
										</div>
<!-----------------------------TAB Content End------------------------------------------------------------------------------------------>												
									</div>
								</div>
<!-----------------------------TAB Finish------------------------------------------------------------------------------------------>
							</div>
						</div>
					</div>
<!-----------------------------Portlet Finish------------------------------------------------------------------------------------------>
				</div>
			</div>
<!------------------------------- END Row------------------------------------------------------------------------>
		</div>
	</div>
<?php 
	echo $this->Html->script('block.js'); 
	echo $this->Html->script('jquery.validate.min.js'); 
?>
<script>
	function postForm(){
		$('#TransactionPassRenewForm').submit();
	}
	$("#TransactionPassRenewForm").validate({
				rules: {
				"data[Transaction][first_name]": {
					required: true,
				},
				"data[Transaction][last_name]": {
					required: true,
				},
				"data[Transaction][email]":{
					required: true,
					email:true
				},
				"data[Transaction][address_line_1]": {
					required: true,
				},
				"data[Transaction][city]": {
					required: true,
				},
				"data[Transaction][state]":{
					required: true,
				},
				"data[Transaction][zip]":{
					required: true,
				},
				"data[Transaction][phone]":{
					required: true,
				},
				"data[Transaction][card_number]":{
					required: true,
					minlength: 15,
					number:true
				},
				"data[Transaction][cvv]":{
					required: true,
					number:true,
					//detectCardType:true
				},
				"data[Transaction][month]":{
					required: true,
				},
				"data[Transaction][year]":{
					required: true,
				},
				"data[Transaction][first_name_cc]": {
					required: true,
				},
				"data[Transaction][last_name_cc]": {
					required: true,
				}
			},
		   
			messages: {
				"data[Transaction][first_name]": { required: "Please enter first name.",
				},
				"data[Transaction][last_name]": { required: "Please enter last name.",
				},
				"data[Transaction][email]": { required:  "Please enter valid email.",email:  "Please enter valid email."
				},
				"data[Transaction][address_line_1]": { required: "Address line is required.",
				},
				"data[Transaction][city]": { required: "City is required.",
				},
				"data[Transaction][state]": { required:  "State is required.",
				},
				"data[Transaction][zip]": { required:  "Zip is required.",
				},
				"data[Transaction][phone]": { required:  "Phone is required.",
				},
				"data[Transaction][card_number]": { required:  "Card number is required.",
				},
				"data[Transaction][cvv]": { required:  "Cvv is required.",//detectCardType:"Please add a valid cvv"
				},
				"data[Transaction][month]": { required:  "Month is required.",
				},
				"data[Transaction][year]": { required:  "Year is required.",
				},
				"data[Transaction][first_name_cc]": { required: "Please enter first name.",
				},
				"data[Transaction][last_name_cc]": { required: "Please enter last name.",
				}
			}, submitHandler: function (form) {
					$.blockUI({
						message: 'Processing payment. Please wait',
						css: {
							border: 'none',
							padding: '15px',
							backgroundColor: '#000',
							'-webkit-border-radius': '10px',
							'-moz-border-radius': '10px',
							opacity: .5,
							color: '#fff'
					}});
					form.submit();
			}
		});
		 jQuery.validator.addMethod('detectCardType', function (value, element) {

				// The two password inputs
				var number = $("#TransactionCardNumber").val();
				var cvv = $("#TransactionCvv").val();
				var cards = {
					visa: /^4[0-9]{12}(?:[0-9]{3})?$/,
					mastercard: /^5[1-5][0-9]{14}$/,
					amex: /^3[47][0-9]{13}$/,
					diners: /^3(?:0[0-5]|[68][0-9])[0-9]{11}$/,
					discover: /^6(?:011|5[0-9]{2})[0-9]{12}$/,
					jcb: /^(?:2131|1800|35\d{3})\d{11}$/,
					dankort:/^5019\d{12}$/,
					switch:/^6759\d{12}(\d{2,3})?$/,
					solo: /^6767\d{12}(\d{2,3})?$/,
					forbrugsforeningen:/^600722\d{10}$/,
					laser:/^(6304|6706|6709|6771(?!89))\d{8}(\d{4}|\d{6,7})?$/
				};
				for (var card in cards) {
					if (cards[card].test(number)) {
					   if(card === "amex"){
						  if($("#TransactionCvv").val().length == 4 ){
							  return true;
						  }else{
							 return false; 
						  }                  
					   }else{
						if($("#TransactionCvv").val().length == 3 ){
							   return true;
						  }else{
							 return false; 
						  }             
					   }                 
					}
				}
				 if($("#TransactionCvv").val().length == 3 ){
                       return true;
                  }else{
                     return false; 
                  } 
    });
</script>
