
<div class="page-content-wrapper">
		<div class="page-content" style="min-height:996px">
			
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					<?php //debug($this->Session->params);
					//debug($this->Session->read('Auth'));
					//debug($this->Session->read('PropertyId')['Property']['id']);
					//debug($this->Session->read('PropertyName'));
					?>
					<?php echo $this->Session->read('PropertyName')." ";?> Remaining Passes <small></small>
					</h3>
					<ul class="page-breadcrumb breadcrumb">

						<li>
							<i class="fa fa-home"></i>
							<?php echo $this->Html->link(
                             'Home',
                              array(
                                    'controller' => 'Users',
                                    'action' => 'myAccount',
                                    'full_base' => true
                                                                                              )
                              );?>
							<i class="fa fa-angle-right"></i>
						</li>

						<li>
							<a href="#">Compulsory Passes</a>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			
			<!-- END PAGE HEADER-->
			<div id="flashMessages">
									<h2><?php  echo $this->Session->flash();?></h2>
			</div>
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- Begin: life time stats -->
					<div class="portlet">
						<div class="portlet-title">

						</div>
						<div class="portlet-body">
							<div class="tabbable">
								<ul class="nav nav-tabs nav-tabs-lg">
									<li class="active">
										<a data-toggle="tab" href="#tab_1">
										  New Pass
                                        </a>
									</li>


								</ul>
								<div class="tab-content">
									<div id="tab_1" class="tab-pane active">
										<div class="row">
											<div class="col-md-12 col-sm-12">
												<div class="alert alert-danger">
													  <strong> You already have a pass, are you sure you want to purchase another one ?</strong> 
												 </div>
											 </div>
										  </div>
                                          <div class="portlet box yellow">
													<div class="portlet-title">
														<div class="caption">
															<i class="fa fa-building"></i>Buy New Pass
														</div>
														<div class="tools">
															<a class="collapse" href="javascript:;">
															</a>

														</div>

													</div>
													<div class="portlet-body">
														<div class="table-responsive">
															<table class="table table-striped table-bordered table-hover">
																<thead>
																	 <tr>
																		<th>#</th>
																		<th>Pass Name</th>
																		<th>Deposit</th>
																		<th>Cost</th>
																		<th>Expiry Date</th>
																		<?php if($package){?>
																		<th>Parking Location Name</th>
																		<th>Parking Location Cost</th>
																		<?php }?>
																		<th>Total Amount </th>
																	 </tr>
																</thead>
																<tbody>
																	<?php $i=1;$pass=$remainingPass; ?>
																		<tr>
																			<td>
																			  <?php echo $i++;?>
																			</td>
																			<td>
																				<?php echo $pass['Pass']['name'];?>
																			</td>
																			<td>
																				<?php echo '$ '.$pass['Pass']['deposit'];?>
																			</td>
																			<td>
																				<?php echo '$ '.$pass['Pass']['cost_1st_year'];?>
																			</td>
																			<td>
																				<?php if($pass['Pass']['is_fixed_duration']==1){
																						echo date("m/d/Y",strtotime($pass['Pass']['expiration_date']));
																					}else{
																						echo date("m/d/Y",strtotime($pass['Pass']['expiration_date']));
																					}
																				?>
																			</td>
																			<?php if($package){?>
																			<td><?php echo $package['Package']['name']; ?></td>
																			<td><?php echo '$ '.$package['Package']['cost']; ?></td>
																			<?php }?>
																			<td>
																				<?php echo '$ '.$totalAmount;?>
																			</td>
																		</tr>
																	
																		<tr>
																			<td></td>
																			<td></td>
																			<td></td>
																			<td></td>
																			<?php if($package){?>
																			<td></td>
																			<td></td>
																			<?php }?>
																			<th>Net Amount Payable</th>
																			<th><?php echo '$ '.$totalAmount;?></th>
																		</tr>
																</tbody>
															</table>
														</div>
													</div>
                                                </div>

									             <?php echo $this->Form->create('Transaction',array('class'=>'form-horizontal'));?>
												  <div class="row">
														<div class="col-md-6 col-sm-6">
															<div class="portlet box green">
																	<div class="portlet-title">
																		<div class="caption">
																			<i class="fa fa-gift"></i>Billing Address
																		</div>
																		<div class="tools">
																			<a href="javascript:;" class="collapse">
																			</a>

																		</div>
																	</div>
																	<div class="portlet-body form">
																		<div class="form-body">
																		  <?php if(!empty($billingAddress)){?>
																					<div class="form-group">
																						<label class="col-md-4 control-label">First Name</label>
																						<div class="col-md-6">
																						 <?php
																
																							echo $this->Form->input('first_name',array('label'=>false,
																																		'errorMessage' => false,
																																		'value'=>$billingAddress['BillingAddress']['first_name'],
																																		
																																		'class'=>'form-control'));
																							if ($this->Form->isFieldError('first_name')) {
																										 echo $this->Form->error('first_name',array('div'=>false,'attributes'=>array('wrap' => 'span', 'class' => 'model-error')));

																						 }?>
																						 </div>
																					</div>
																					<div class="form-group">
																						<label class="col-md-4 control-label">Last Name</label>
																						<div class="col-md-6">
																						 <?php
																							echo $this->Form->input('last_name',array('label'=>false,
																																		'errorMessage' => false,
																																		'value'=>$billingAddress['BillingAddress']['last_name'],
																																		'class'=>'form-control'));
																							if ($this->Form->isFieldError('last_name')) {
																										 echo $this->Form->error('last_name',array('div'=>false,'attributes'=>array('wrap' => 'span', 'class' => 'model-error')));

																						 }?>
																						 </div>
																					</div>
																					<div class="form-group">
																						  <label class="col-md-4 control-label">Email</label>
																						  <div class="col-md-6">
																						   <?php
																							echo $this->Form->input('email',array('label'=>false,
																																		'errorMessage' => false,
																																		'value'=>$billingAddress['BillingAddress']['email'],
																																		'class'=>'form-control'));
																							  if ($this->Form->isFieldError('email')) {
																										   echo $this->Form->error('email',array('div'=>false,'attributes'=>array('wrap' => 'span', 'class' => 'model-error')));

																						   }?>
																						  </div>
																					  </div>
																					  <div class="form-group">
																						  <label class="col-md-4 control-label">Address Line 1</label>
																						  <div class="col-md-6">
																						   <?php
																							echo $this->Form->input('address_line_1',array('label'=>false,
																																		'errorMessage' => false,
																																		'value'=>$billingAddress['BillingAddress']['address_line_1'],
																																		'class'=>'form-control'));
																							  if ($this->Form->isFieldError('address_line_1')) {
																										   echo $this->Form->error('address_line_1',array('div'=>false,'attributes'=>array('wrap' => 'span', 'class' => 'model-error')));

																						   }?>
																						  </div>
																					  </div>
																					  <div class="form-group">
																						  <label class="col-md-4 control-label">Apartment\Suite #</label>
																						  <div class="col-md-6">
																						   <?php
																							echo $this->Form->input('address_line_2',array('label'=>false,
																																		'errorMessage' => false,
																																		'value'=>$billingAddress['BillingAddress']['address_line_2'],
																																		'class'=>'form-control'));
																							  if ($this->Form->isFieldError('address_line_2')) {
																										   echo $this->Form->error('address_line_2',array('div'=>false,'attributes'=>array('wrap' => 'span', 'class' => 'model-error')));

																						   }?>
																						  </div>
																					  </div>
																					  <div class="form-group">
																						  <label class="col-md-4 control-label">City</label>
																						  <div class="col-md-6">
																						   <?php
																							echo $this->Form->input('city',array('label'=>false,
																																		'errorMessage' => false,
																																		'value'=>$billingAddress['BillingAddress']['city'],
																																		'class'=>'form-control'));
																							  if ($this->Form->isFieldError('city')) {
																										   echo $this->Form->error('city',array('div'=>false,'attributes'=>array('wrap' => 'span', 'class' => 'model-error')));

																						   }?>
																						  </div>
																					  </div>
																					  <div class="form-group">
																						  <label class="col-md-4 control-label">State</label>
																						   <div class="col-md-6">
																							<?php
																								 echo $this->Form->input('state',array('type' => 'select',
																																		'label'=>false,
																																		'options' => $states,
																																		'value'=>$billingAddress['BillingAddress']['state'],
																																		'empty'=>'State',
																																		'errorMessage' => false,
																																		'class'=>'form-control'));
																								if ($this->Form->isFieldError('state')) {
																										   echo $this->Form->error('state',array('div'=>false,'attributes'=>array('wrap' => 'span', 'class' => 'model-error')));

																							}?>
																						   </div>
																					  </div>
																					  <div class="form-group">
																						  <label class="col-md-4 control-label">Zip</label>
																						  <div class="col-md-6">
																						   <?php
																							echo $this->Form->input('zip',array('label'=>false,
																																		'errorMessage' => false,
																																		'value'=>$billingAddress['BillingAddress']['zip'],
																																		'class'=>'form-control'));
																							  if ($this->Form->isFieldError('zip')) {
																										   echo $this->Form->error('zip',array('div'=>false,'attributes'=>array('wrap' => 'span', 'class' => 'model-error')));

																						   }?>
																						  </div>
																					  </div>
																					  <div class="form-group">
																						  <label class="col-md-4 control-label">Phone</label>
																						  <div class="col-md-6">
																						   <?php
																							echo $this->Form->input('phone',array('label'=>false,
																																		'errorMessage' => false,
																																		'value'=>$billingAddress['BillingAddress']['phone'],
																																		'class'=>'form-control'));
																							  if ($this->Form->isFieldError('phone')) {
																										   echo $this->Form->error('phone',array('div'=>false,'attributes'=>array('wrap' => 'span', 'class' => 'model-error')));

																						   }?>
																						  </div>
																					  </div>

																			  <?php }else{?>
																					<div class="form-group">
																						<label class="col-md-4 control-label">First Name</label>
																						<div class="col-md-6">
																						 <?php
																							echo $this->Form->input('first_name',array('label'=>false,
																																		'errorMessage' => false,
																																		'class'=>'form-control'));
																							if ($this->Form->isFieldError('first_name')) {
																										 echo $this->Form->error('first_name',array('div'=>false,'attributes'=>array('wrap' => 'span', 'class' => 'model-error')));

																						 }?>
																						 </div>
																					</div>
																					<div class="form-group">
																						<label class="col-md-4 control-label">Last Name</label>
																						<div class="col-md-6">
																						 <?php
																							echo $this->Form->input('last_name',array('label'=>false,
																																		'errorMessage' => false,
																																		'class'=>'form-control'));
																							if ($this->Form->isFieldError('last_name')) {
																										 echo $this->Form->error('last_name',array('div'=>false,'attributes'=>array('wrap' => 'span', 'class' => 'model-error')));

																						 }?>
																						 </div>
																					</div>
																					<div class="form-group">
																						  <label class="col-md-4 control-label">Email</label>
																						  <div class="col-md-6">
																						   <?php
																							echo $this->Form->input('email',array('label'=>false,
																																		'errorMessage' => false,
																																		'class'=>'form-control'));
																							  if ($this->Form->isFieldError('email')) {
																										   echo $this->Form->error('email',array('div'=>false,'attributes'=>array('wrap' => 'span', 'class' => 'model-error')));

																						   }?>
																						  </div>
																					  </div>
																					  <div class="form-group">
																						  <label class="col-md-4 control-label">Address Line 1</label>
																						  <div class="col-md-6">
																						   <?php
																							echo $this->Form->input('address_line_1',array('label'=>false,
																																		'errorMessage' => false,
																																		'class'=>'form-control'));
																							  if ($this->Form->isFieldError('address_line_1')) {
																										   echo $this->Form->error('address_line_1',array('div'=>false,'attributes'=>array('wrap' => 'span', 'class' => 'model-error')));

																						   }?>
																						  </div>
																					  </div>
																					  <div class="form-group">
																						  <label class="col-md-4 control-label">Apartment\Suite #</label>
																						  <div class="col-md-6">
																						   <?php
																							echo $this->Form->input('address_line_2',array('label'=>false,
																																		'errorMessage' => false,
																																		'class'=>'form-control'));
																							  if ($this->Form->isFieldError('address_line_2')) {
																										   echo $this->Form->error('address_line_2',array('div'=>false,'attributes'=>array('wrap' => 'span', 'class' => 'model-error')));

																						   }?>
																						  </div>
																					  </div>
																					  <div class="form-group">
																						  <label class="col-md-4 control-label">City</label>
																						  <div class="col-md-6">
																						   <?php
																							echo $this->Form->input('city',array('label'=>false,
																																		'errorMessage' => false,
																																		'class'=>'form-control'));
																							  if ($this->Form->isFieldError('city')) {
																										   echo $this->Form->error('city',array('div'=>false,'attributes'=>array('wrap' => 'span', 'class' => 'model-error')));

																						   }?>
																						  </div>
																					  </div>
																					  <div class="form-group">
																						  <label class="col-md-4 control-label">State</label>
																						   <div class="col-md-6">
																							<?php
																								 echo $this->Form->input('state',array('type' => 'select',
																																		'label'=>false,
																																		'options' => $states,
																																		'empty'=>'State',
																																		'errorMessage' => false,
																																		'class'=>'form-control'));
																								if ($this->Form->isFieldError('state')) {
																										   echo $this->Form->error('state',array('div'=>false,'attributes'=>array('wrap' => 'span', 'class' => 'model-error')));

																							}?>
																						   </div>
																					  </div>
																					  <div class="form-group">
																						  <label class="col-md-4 control-label">Zip</label>
																						  <div class="col-md-6">
																						   <?php
																							echo $this->Form->input('zip',array('label'=>false,
																																		'errorMessage' => false,
																																		'class'=>'form-control'));
																							  if ($this->Form->isFieldError('zip')) {
																										   echo $this->Form->error('zip',array('div'=>false,'attributes'=>array('wrap' => 'span', 'class' => 'model-error')));

																						   }?>
																						  </div>
																					  </div>
																					  <div class="form-group">
																						  <label class="col-md-4 control-label">Phone</label>
																						  <div class="col-md-6">
																						   <?php
																							echo $this->Form->input('phone',array('label'=>false,
																																		'errorMessage' => false,
																																		'class'=>'form-control'));
																							  if ($this->Form->isFieldError('phone')) {
																										   echo $this->Form->error('phone',array('div'=>false,'attributes'=>array('wrap' => 'span', 'class' => 'model-error')));

																						   }?>
																						  </div>
																					  </div>
																			  
																			  
																			  
																			  <?php }?>
																		</div>


																	</div>
																</div>

															</div>
															<div class="col-md-6 col-sm-6">
																 <?php if($totalAmount>0){?>
																		<div class="portlet box blue">
																					<div class="portlet-title">
																						<div class="caption">
																							<i class="fa fa-gift"></i>Credit Card Details
																						</div>
																						<div class="tools">
																							<a href="javascript:;" class="collapse">
																							</a>

																						</div>
																					</div>
																					<div class="portlet-body form">
																							<div class="form-body">

																								<?php
																									  for($i=1;$i<=12;$i++){$month[$i]=$i;}
																									  $dt = new DateTime();
																									  $currentYear= (int)$dt->format('Y');
																									  for($i=$currentYear;$i<=$currentYear+20;$i++){$year[$i]=$i;}
																								 ?>
																										<div class="form-group">
																											<label class="col-md-4 control-label">Card Number</label>
																											<div class="col-md-6">
																											 <?php
																												echo $this->Form->input('card_number',array('label'=>false,
																																							'errorMessage' => false,
																																							'class'=>'form-control'));
																												if ($this->Form->isFieldError('card_number')) {
																															 echo $this->Form->error('card_number',array('div'=>false,'attributes'=>array('wrap' => 'span', 'class' => 'model-error')));

																											 }?>
																											 </div>
																										</div>
																										<div class="form-group">
																											<label class="col-md-4 control-label">CVV</label>
																											<div class="col-md-6">
																												<?php
																												echo $this->Form->input('cvv',array('label'=>false,
																																							'errorMessage' => false,
																																							'class'=>'form-control'));
																												if ($this->Form->isFieldError('cvv')) {
																															 echo $this->Form->error('cvv',array('div'=>false,'attributes'=>array('wrap' => 'span', 'class' => 'model-error')));

																												}?>
																											</div>
																										</div>
																										<div class="form-group">

																											 <table>
																												<?php
																														  echo "<tr>";
																															 ?>
																															<label class="col-md-4 control-label">Expiry Date</label>
																															 <?php
																															 echo "<td>";
																																echo $this->Form->input('month',array('type' => 'select',
																																						'label'=>array('class'=>'col-md-1 control-label','text'=>''),
																																						'options' => $month,
																																						 'empty'=>'Month',
																																						 ));
																															 echo "</td>";
																															 echo "<td>";
																																echo $this->Form->input('year',array('type' => 'select',
																																						 'label'=>array('class'=>'col-md-1 control-label','text'=>''),
																																						 'options' => $year,
																																						 'empty'=>'Year',
																																						 ));
																															 echo "</td>";
																														   echo "</tr>";
																														  ?>
																											  </table>
																										</div>
																										<div class="form-group">
																											<label class="col-md-4 control-label">First Name</label>
																											<div class="col-md-6">
																												<?php
																												echo $this->Form->input('first_name_cc',array('label'=>false,
																																							'errorMessage' => false,
																																							'class'=>'form-control'));
																												if ($this->Form->isFieldError('first_name_cc')) {
																															 echo $this->Form->error('first_name_cc',array('div'=>false,'attributes'=>array('wrap' => 'span', 'class' => 'model-error')));

																												}?>
																											</div>
																										</div>
																										<div class="form-group">
																											<label class="col-md-4 control-label">Last Name</label>
																											<div class="col-md-6">
																												<?php
																												echo $this->Form->input('last_name_cc',array('label'=>false,
																																							'errorMessage' => false,
																																							'class'=>'form-control'));
																												if ($this->Form->isFieldError('last_name_cc')) {
																															 echo $this->Form->error('last_name_cc',array('div'=>false,'attributes'=>array('wrap' => 'span', 'class' => 'model-error')));

																												}?>
																											</div>
																										</div>
																							</div>																													
																					</div>
																		</div>
																		<?php }?>
																		<div class="well">

																			<div class="row">
																				<div class="col-md-6">
																					 <div class="col-md-offset-3 col-md-9">
																						  <button class="btn green" type="submit">Check Out</button>
																					 </div>
																					 <?php echo $this->Form->end(); ?>
																				</div>
																			</div>
																		</div>
                                    								</div>


									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- End: life time stats -->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
<?php 
	echo $this->Html->script('block.js'); 
	echo $this->Html->script('jquery.validate.min.js'); 
?>
<style>
.error {
    color: red;
}
</style>
<script>
	
	$("#TransactionBuyRemainingPassForm").validate({
				rules: {
				"data[Transaction][first_name]": {
					required: true,
				},
				"data[Transaction][last_name]": {
					required: true,
				},
				"data[Transaction][email]":{
					required: true,
					email:true
				},
				"data[Transaction][address_line_1]": {
					required: true,
				},
				"data[Transaction][city]": {
					required: true,
				},
				"data[Transaction][state]":{
					required: true,
				},
				"data[Transaction][zip]":{
					required: true,
				},
				"data[Transaction][phone]":{
					required: true,
				},
				"data[Transaction][card_number]":{
					required: true,
					minlength: 15,
					number:true
				},
				"data[Transaction][cvv]":{
					required: true,
					number:true,
					//detectCardType:true
				},
				"data[Transaction][month]":{
					required: true,
				},
				"data[Transaction][year]":{
					required: true,
				},
				"data[Transaction][first_name_cc]": {
					required: true,
				},
				"data[Transaction][last_name_cc]": {
					required: true,
				}
			},
		   
			messages: {
				"data[Transaction][first_name]": { required: "Please enter first name.",
				},
				"data[Transaction][last_name]": { required: "Please enter last name.",
				},
				"data[Transaction][email]": { required:  "Please enter valid email.",email:  "Please enter valid email."
				},
				"data[Transaction][address_line_1]": { required: "Address line is required.",
				},
				"data[Transaction][city]": { required: "City is required.",
				},
				"data[Transaction][state]": { required:  "State is required.",
				},
				"data[Transaction][zip]": { required:  "Zip is required.",
				},
				"data[Transaction][phone]": { required:  "Phone is required.",
				},
				"data[Transaction][card_number]": { required:  "Card number is required.",
				},
				"data[Transaction][cvv]": { required:  "Cvv is required.",//detectCardType:"Please add a valid cvv"
				},
				"data[Transaction][month]": { required:  "Month is required.",
				},
				"data[Transaction][year]": { required:  "Year is required.",
				},
				"data[Transaction][first_name_cc]": { required: "Please enter first name.",
				},
				"data[Transaction][last_name_cc]": { required: "Please enter last name.",
				}
			}, submitHandler: function (form) {
					$.blockUI({
						message: 'Processing payment. Please wait',
						css: {
							border: 'none',
							padding: '15px',
							backgroundColor: '#000',
							'-webkit-border-radius': '10px',
							'-moz-border-radius': '10px',
							opacity: .5,
							color: '#fff'
					}});
					form.submit();
			}
		});
		 jQuery.validator.addMethod('detectCardType', function (value, element) {

				// The two password inputs
				var number = $("#TransactionCardNumber").val();
				var cvv = $("#TransactionCvv").val();
				var cards = {
					visa: /^4[0-9]{12}(?:[0-9]{3})?$/,
					mastercard: /^5[1-5][0-9]{14}$/,
					amex: /^3[47][0-9]{13}$/,
					diners: /^3(?:0[0-5]|[68][0-9])[0-9]{11}$/,
					discover: /^6(?:011|5[0-9]{2})[0-9]{12}$/,
					jcb: /^(?:2131|1800|35\d{3})\d{11}$/,
					dankort:/^5019\d{12}$/,
					switch:/^6759\d{12}(\d{2,3})?$/,
					solo: /^6767\d{12}(\d{2,3})?$/,
					forbrugsforeningen:/^600722\d{10}$/,
					laser:/^(6304|6706|6709|6771(?!89))\d{8}(\d{4}|\d{6,7})?$/
				};
				for (var card in cards) {
					if (cards[card].test(number)) {
					   if(card === "amex"){
						  if($("#TransactionCvv").val().length == 4 ){
							  return true;
						  }else{
							 return false; 
						  }                  
					   }else{
						if($("#TransactionCvv").val().length == 3 ){
							   return true;
						  }else{
							 return false; 
						  }             
					   }                 
					}
				}
				 if($("#TransactionCvv").val().length == 3 ){
                       return true;
                  }else{
                     return false; 
                  } 
    });
</script>

