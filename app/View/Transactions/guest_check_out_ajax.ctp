<!-- BEGIN PAGE CONTENT-->
<div class="row">
	 
	 <?php echo $this->Form->create('Transaction',array('class'=>'form-horizontal','id'=>'TRANSACTION_FORM'));?>
	  <div class="col-md-6 col-sm-6">
					<div class="portlet box green">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-gift"></i>Billing Address
							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse"></a>
							</div>
						</div>
						<div class="portlet-body form">
							<div class="form-body">
							<?php
							echo $this->Form->input('first_name',array('div'=>array('class'=>'form-group'),
													'value'=>$billingAddress['BillingAddress']['first_name'],
													'label'=>array('class'=>'col-md-4 control-label','text'=>'First Name'),
													'between'=>'<div class="col-md-6">',
													'after'=>'</div>',
													'error'=>array('attributes'=>array('class'=>'model-error')),
													'class'=>'form-control'));
							
							echo $this->Form->input('last_name',array('div'=>array('class'=>'form-group'),
													'value'=>$billingAddress['BillingAddress']['last_name'],
													'label'=>array('class'=>'col-md-4 control-label','text'=>'Last Name'),
													'between'=>'<div class="col-md-6">',
													'after'=>'</div>',
													'error'=>array('attributes'=>array('class'=>'model-error')),
													'class'=>'form-control'));
							echo $this->Form->input('email',array('type'=>'email','div'=>array('class'=>'form-group'),
													  'value'=>$billingAddress['BillingAddress']['email'],
													  'label'=>array('class'=>'col-md-4 control-label','text'=>'Email'),
													  'between'=>'<div class="col-md-6">',
													  'after'=>'</div>',
													  'error'=>array('attributes'=>array('class'=>'model-error')),
													  'class'=>'form-control'));
													  
							echo $this->Form->input('address_line_1',array('div'=>array('class'=>'form-group'),
												   'value'=>$billingAddress['BillingAddress']['address_line_1'],
												   'label'=>array('class'=>'col-md-4 control-label','text'=>'Address Line 1'),
												   'between'=>'<div class="col-md-6">',
												   'after'=>'</div>',
												   'error'=>array('attributes'=>array('class'=>'model-error')),
												   'class'=>'form-control'));
							echo $this->Form->input('address_line_2',array('div'=>array('class'=>'form-group'),
												  'value'=>$billingAddress['BillingAddress']['address_line_2'],
												  'label'=>array('class'=>'col-md-4 control-label','text'=>'Apartment\Suite #'),
												  'between'=>'<div class="col-md-6">',
												  'after'=>'</div>',
												  'error'=>array('attributes'=>array('class'=>'model-error')),
												  'class'=>'form-control'));

							echo $this->Form->input('city',array('div'=>array('class'=>'form-group'),
													'value'=>$billingAddress['BillingAddress']['city'],
													'label'=>array('class'=>'col-md-4 control-label','text'=>'City'),
													'value'=>$billingAddress['BillingAddress']['city'],
													'between'=>'<div class="col-md-6">',
													'error'=>array('attributes'=>array('class'=>'model-error')),
													'after'=>'</div>',
													'class'=>'form-control'));																								                                                                                                  
							echo $this->Form->input('state',array('type' => 'select',
													'div'=>array('class'=>'form-group'),
													'label'=>array('class'=>'col-md-4 control-label','text'=>'State'),
													'value'=>$billingAddress['BillingAddress']['state'],
													'options' => $states,
													'empty'=>'State',
													'between'=>'<div class="col-md-6">',
													'after'=>'</div>',
													'error'=>array('attributes'=>array('class'=>'model-error')),
													'class'=>'form-control'));

							 echo $this->Form->input('zip',array('div'=>array('class'=>'form-group'),
													 'value'=>$billingAddress['BillingAddress']['zip'],
													 'label'=>array('class'=>'col-md-4 control-label','text'=>'Zip'),
													 'between'=>'<div class="col-md-6">',
													 'after'=>'</div>',
													 'error'=>array('attributes'=>array('class'=>'model-error')),
													 'class'=>'form-control'));
							  echo $this->Form->input('phone',array('div'=>array('class'=>'form-group'),
													  'value'=>$billingAddress['BillingAddress']['phone'],
													  'label'=>array('class'=>'col-md-4 control-label','text'=>'Phone'),
													  'between'=>'<div class="col-md-6">',
													  'after'=>'</div>',
													  'error'=>array('attributes'=>array('class'=>'model-error')),
													  'class'=>'form-control'));
							?>	
							 </div>		
						</div>
					</div>
	  </div>
	  <div class="col-md-6 col-sm-6">
		<div class="portlet blue-hoki box">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-cogs"></i>Guest Credit Details
				</div>
			</div>
			<div class="portlet-body">
				<div class="row static-info">
				<div class="col-md-5 name">
						Number Of Days :
					</div>
					<div class="col-md-7 value">
						<?php echo $currentCreditUsed;?>
					</div>
				</div>
				<div class="row static-info">
					<div class="col-md-5 name">
						Amount Payable
					</div>
					<div class="col-md-7 value">
						<?php echo $amount;?>
					</div>
				</div>
			</div>
		</div>
		<div class="portlet box blue">
		  <div class="portlet-title">
				<div class="caption">
					<i class="fa fa-gift"></i>Credit Card Details
				</div>
				<div class="tools">
					<a href="javascript:;" class="collapse"></a>
				</div>
			</div>
			<div class="portlet-body form">
				 <!--Error Block Start-->
				<div class="note note-danger" id="paymentErrorDiv" style="display:none;">
					<h4 class="block">Error !</h4>
					<p id="paymentErrorMsg"></p>
				</div>
				<!--Error Block End-->
					<div class="form-body">	
						<?php
							for($i=1;$i<=12;$i++){$month[$i]=$i;}
							$dt = new DateTime();
							$currentYear= (int)$dt->format('Y');
							for($i=$currentYear;$i<=$currentYear+20;$i++){$year[$i]=$i;}
							echo $this->Form->input('card_number',array('div'=>array('class'=>'form-group'),
													'label'=>array('class'=>'col-md-4 control-label','text'=>'Card Number'),
													'between'=>'<div class="col-md-6">',
													 'error'=>array('attributes'=>array('class'=>'model-error')),
													'after'=>'</div>',
													'class'=>'form-control',
													'pattern'=>".{14,16}"
													));

							echo $this->Form->input('cvv',array('div'=>array('class'=>'form-group'),
													'label'=>array('class'=>'col-md-4 control-label','text'=>'CVV'),
													'between'=>'<div class="col-md-6">',
													'error'=>array('attributes'=>array('class'=>'model-error')),
													'after'=>'</div>',
													'class'=>'form-control',
													'pattern'=>".{3,4}"
													
													));
						 ?>
						 <div class="form-group">
							  <table>
							  <?php
								  echo "<tr>";
							  ?>
							  <label class="col-md-4 control-label">Expiry Date</label>
							  <?php
								   echo "<td>";
								   echo $this->Form->input('month',array('type' => 'select',
														   'label'=>array('class'=>'col-md-1 control-label','text'=>''),
														   'options' => $month,
														   'empty'=>'Month',
														   ));
								   echo "</td>";
								   echo "<td>";
								   echo $this->Form->input('year',array('type' => 'select',
														   'label'=>array('class'=>'col-md-1 control-label','text'=>''),
														   'options' => $year,
														   'empty'=>'Year',
														   ));
								  echo "</td>";
								  echo "</tr>";
								?>
								</table>
						  </div>
						  <?php
							   echo $this->Form->input('first_name_cc',array('div'=>array('class'=>'form-group'),
														'label'=>array('class'=>'col-md-4 control-label','text'=>'First Name'),
														'between'=>'<div class="col-md-6">',
														 'error'=>array('attributes'=>array('class'=>'model-error')),
														 'after'=>'</div>',
														 'class'=>'form-control'));
							   echo $this->Form->input('last_name_cc',array('div'=>array('class'=>'form-group'),
													   'label'=>array('class'=>'col-md-4 control-label','text'=>'Last Name'),
														'between'=>'<div class="col-md-6">',
														 'error'=>array('attributes'=>array('class'=>'model-error')),
														'after'=>'</div>',
														'class'=>'form-control'));
						 ?>
						 <div class="row">
							   <div class="col-md-6">
								   <div class="col-md-offset-3 col-md-9">
									  <button class="btn green" type="submit">Check Out</button>
								   </div>
								   <?php echo $this->Form->end(); ?>
							   </div>
						 </div>
					</div>
				</div>
		</div>												
	  </div> 
</div>
<script>
	
	$("#TRANSACTION_FORM").validate({
				rules: {
				"data[Transaction][first_name]": {
					required: true,
				},
				"data[Transaction][last_name]": {
					required: true,
				},
				"data[Transaction][email]":{
					required: true,
					email:true
				},
				"data[Transaction][address_line_1]": {
					required: true,
				},
				"data[Transaction][city]": {
					required: true,
				},
				"data[Transaction][state]":{
					required: true,
				},
				"data[Transaction][zip]":{
					required: true,
				},
				"data[Transaction][phone]":{
					required: true,
				},
				"data[Transaction][card_number]":{
					required: true,
					number:true
				},
				"data[Transaction][cvv]":{
					required: true,
					number:true
				},
				"data[Transaction][month]":{
					required: true,
				},
				"data[Transaction][year]":{
					required: true,
				},
				"data[Transaction][first_name_cc]": {
					required: true,
				},
				"data[Transaction][last_name_cc]": {
					required: true,
				}
			},
		   
			messages: {
				"data[Transaction][first_name]": { required: "Please enter first name.",
				},
				"data[Transaction][last_name]": { required: "Please enter last name.",
				},
				"data[Transaction][email]": { required:  "Please enter valid email.",email:  "Please enter valid email."
				},
				"data[Transaction][address_line_1]": { required: "Address line is required.",
				},
				"data[Transaction][city]": { required: "City is required.",
				},
				"data[Transaction][state]": { required:  "State is required.",
				},
				"data[Transaction][zip]": { required:  "Zip is required.",
				},
				"data[Transaction][phone]": { required:  "Phone is required.",
				},
				"data[Transaction][card_number]": { required:  "Card number is required.",
				},
				"data[Transaction][cvv]": { required:  "Cvv is required.",
				},
				"data[Transaction][month]": { required:  "Month is required.",
				},
				"data[Transaction][year]": { required:  "Year is required.",
				},
				"data[Transaction][first_name_cc]": { required: "Please enter first name.",
				},
				"data[Transaction][last_name_cc]": { required: "Please enter last name.",
				}
			},
			submitHandler: function(form) {
				$.blockUI({ css: { 
							border: 'none', 
							padding: '15px', 
							backgroundColor: '#000', 
							'-webkit-border-radius': '10px', 
							'-moz-border-radius': '10px', 
							opacity: .5, 
							color: '#fff' 
				} });
				
				 $('#paymentErrorDiv').hide();
				 $.ajax({
				   type: "POST",
				   url: "/Transactions/processPayment/<?php echo $customerPassId.'/'.$passId.'/'.$permitId?>?vehicle_id="+$('#VehicleVehicleId').val()+"&owner="+$('#VehicleOwner').val()+"&make="+$('#VehicleMake').val()+"&model="+$('#VehicleMake').val()+"&model="+$('#VehicleModel').val()+"&color="+$('#VehicleColor').val()+"&platenum="+$('#VehicleLicensePlateNumber').val()+"&state="+$('#VehicleLicensePlateState').val()+"&vin="+$('#VehicleLast4DigitalOfVin').val()+"&location="+$('#VehicleAssignedLocation').val(),
				   data: $("#TRANSACTION_FORM").serialize(),
				   
				   success: function(data){ 
					   data=JSON.parse(data);
					   if(data.success==false){
						    $('#PaymentPanelTitle').css('background-color', 'rgba(255, 0, 0, 0.5)');
							$('#paymentErrorDiv').show();
							$('#paymentErrorMsg').html(data.message);
							$.unblockUI();
					   }else{
						    $('#daySelectionDiv').empty();
						    $('#paymentDiv').empty();
						    $('#PaymentPanelTitle').css('background-color', 'rgba(0, 128, 0, 0.5)');
						    window.location.href = "//<?php echo $_SERVER['HTTP_HOST']?>"; 
					   }
					   
				   }
				 });
			}
			
		});
</script>
