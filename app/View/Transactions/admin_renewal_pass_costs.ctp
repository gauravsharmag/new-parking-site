<div class="page-content-wrapper">
		<div class="page-content" style="min-height:1027px">
			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					Passes <small>Property Wide Renewed Passes Costs</small>
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li>
							<i class="fa fa-home"></i>
							<a href="/admin/Users/superAdminHome">Home</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">Property Renewed Wide Pass Cost</a>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
			<div id="flashMessages">
									<h2><?php  echo $this->Session->flash();?></h2>
			</div>
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					
					<!-- Begin: life time stats -->
					<div class="portlet">
						<div class="portlet-title">
							<div class="row">
								<!--<div class="col-md-3">
									<?php //echo $this->Form->input('type_transaction',array('class'=>'form-control','type'=>'select','label'=>false,'empty'=>'All Transaction','options'=>array('passRenewed'=>'Renewal'))); ?>
								</div>-->
									<div class="col-md-3">
											<div data-date-format="mm/dd/yyyy" data-date="08/15/2014" class="input-group input-medium date-picker input-daterange col-md-12">
												<input type="text" name="from" id="from" class="form-control">
												<span class="input-group-addon">
												to </span>
												<input type="text" name="to" id="to" class="form-control">
											</div>
											<!-- /input-group -->
											<span class="help-block" id="helpBlock">
											Select Date </span>
									</div>
									<div class="col-md-1"> 
											<input type="submit" class="btn yellow" value="Go" name="submit" id="gobtn">
									</div>
							</div>
						</div>
						<div class="portlet-body" id='portletBody'>
							
						</div>
					</div>
					<!-- End: life time stats -->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
<?php echo $this->Html->script('block.js'); ?>
<script type="text/javascript">
jQuery(document).ready(function ($) {
	
	$("#gobtn").click(function () {
				if($('#from').val()){
					$.blockUI({ css: { 
						border: 'none', 
						padding: '15px', 
						backgroundColor: '#000', 
						'-webkit-border-radius': '10px', 
						'-moz-border-radius': '10px', 
						opacity: .5, 
						color: '#fff' 
					} });
					$('#helpBlock').html("Select Date");
					$('#helpBlock').css('color','grey');
					var toDate="";
					if($('#to').val()){
						toDate=$('#to').val();
					}else{
						var dt = new Date();
						$('#to').val((dt.getMonth()+1) + '/' + (dt.getDate()-1) + '/' +  dt.getFullYear());
						toDate=(dt.getMonth()+1) + '/' + (dt.getDate()-1) + '/' +  dt.getFullYear();
					}
					var data = {};
					data[0]=$('#from').val();
					data[1]=toDate;
					var dataSend = JSON.stringify(data);
					$.ajax({
							//url: "/admin/Transactions/pass_cost_listing?toDate="+toDate+"&fromDate="+$('#from').val()+"&type="+$('#type_transaction').val(),
							url: "/admin/Transactions/renewal_pass_cost_listing?toDate="+toDate+"&fromDate="+$('#from').val(),
							type: "get",
							//data: dataSend, 
							dataType: "json",
							success: function (rsp) {
								response=rsp.PropertyData;
								document.getElementById("portletBody").innerHTML='';
								var htmlString='<div class="note note-warning"><p>Pass Amount</p></div><div class="row static-info"><div class="col-md-4 value">PROPERTY</div><div class="col-md-2 value" align="right">AMOUNT($)</div></div>';
								for(var i=0;i<(response.length);i++){
									if(response[i].Property.cost==null){
										response[i].Property.cost=0;
									}
									htmlString=htmlString+'<div class="row static-info"><div class="col-md-4 name">'+response[i].Property.name+'</div><div class="col-md-2 value" align="right">  '+response[i].Property.cost+'</div></div>';
								//	console.log(response[i]);
								}
								if(rsp.TotalAmount==null){
									rsp.TotalAmount=0;
								}
								htmlString=htmlString+'<div class="row static-info"><div class="col-md-4 name"><b>TOTAL AMOUNT</b></div><div class="col-md-2 value" align="right"> '+rsp.TotalAmount+'</div></div>';

								document.getElementById("portletBody").innerHTML=htmlString;
								//console.log(response[0]);
								$.unblockUI();
							}
					});
				}else{
					$('#helpBlock').html("Select From date");
					$('#helpBlock').css('color','red')
				}
	});
	  
});
</script>
