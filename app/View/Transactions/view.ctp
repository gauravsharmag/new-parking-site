<div class="transactions view">
<h2><?php echo __('Transaction'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($transaction['Transaction']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('User'); ?></dt>
		<dd>
			<?php echo $this->Html->link($transaction['User']['id'], array('controller' => 'users', 'action' => 'view', $transaction['User']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Date Time'); ?></dt>
		<dd>
			<?php echo h($transaction['Transaction']['date_time']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Amount'); ?></dt>
		<dd>
			<?php echo h($transaction['Transaction']['amount']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Result'); ?></dt>
		<dd>
			<?php echo h($transaction['Transaction']['result']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Pass'); ?></dt>
		<dd>
			<?php echo $this->Html->link($transaction['Pass']['id'], array('controller' => 'passes', 'action' => 'view', $transaction['Pass']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Message'); ?></dt>
		<dd>
			<?php echo h($transaction['Transaction']['message']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Credits Used'); ?></dt>
		<dd>
			<?php echo h($transaction['Transaction']['credits_used']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Payment Method Used'); ?></dt>
		<dd>
			<?php echo h($transaction['Transaction']['payment_method_used']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Transaction'), array('action' => 'edit', $transaction['Transaction']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Transaction'), array('action' => 'delete', $transaction['Transaction']['id']), array(), __('Are you sure you want to delete # %s?', $transaction['Transaction']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Transactions'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Transaction'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Passes'), array('controller' => 'passes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Pass'), array('controller' => 'passes', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Passes'); ?></h3>
	<?php if (!empty($transaction['Pass'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th><?php echo __('Vehicle Id'); ?></th>
		<th><?php echo __('Property Id'); ?></th>
		<th><?php echo __('Transaction Id'); ?></th>
		<th><?php echo __('Status'); ?></th>
		<th><?php echo __('Membership Vaild Upto'); ?></th>
		<th><?php echo __('Pass Valid Upto'); ?></th>
		<th><?php echo __('RFID Tag Number'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($transaction['Pass'] as $pass): ?>
		<tr>
			<td><?php echo $pass['id']; ?></td>
			<td><?php echo $pass['user_id']; ?></td>
			<td><?php echo $pass['vehicle_id']; ?></td>
			<td><?php echo $pass['property_id']; ?></td>
			<td><?php echo $pass['transaction_id']; ?></td>
			<td><?php echo $pass['status']; ?></td>
			<td><?php echo $pass['membership_vaild_upto']; ?></td>
			<td><?php echo $pass['pass_valid_upto']; ?></td>
			<td><?php echo $pass['RFID_tag_number']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'passes', 'action' => 'view', $pass['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'passes', 'action' => 'edit', $pass['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'passes', 'action' => 'delete', $pass['id']), array(), __('Are you sure you want to delete # %s?', $pass['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Pass'), array('controller' => 'passes', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
