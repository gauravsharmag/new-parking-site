
		<div class="table-responsive">
			  <table id="view_rfid_list" class="table table-striped table-bordered table-hover">
				 <thead>
					<tr>
					   <th>Sr. No.</th>
					   <th>Property</th>
					   <th>Total Pass Renewed</th>
					   <th>Free</th>
					   <th>Paid</th>
					   <th>Amount</th>
					</tr>
				  </thead>
				  <tbody>
					  <?php 
							$sr=1;
							$totalRenewal=$paidRenewal=$freeRenewal=$totalCost=0;
							for($i=0;$i<count($array);$i++){ ?>
						   <tr>
								<td align="left">
									<?php echo $sr++; ?>
								</td>
								<td>
									<?php echo $array[$i]['PropertyName']; ?>
								</td>
								<td align="left">
									<?php $totalRenewal=$totalRenewal+($array[$i]['FreePassCount']+$array[$i]['PaidPassCount']);
										echo '<a href="javascript:;" onclick="getPassesData(\''.$array[$i]['PropertyName'].'\',\''.$array[$i]['PropertyID'].'\',\''.$toDate.'\',\''.$fromDate.'\');">'.($array[$i]['FreePassCount']+$array[$i]['PaidPassCount']).'</a>'; ?>
								</td>
								<td align="left">
									<?php  $freeRenewal=$freeRenewal+$array[$i]['FreePassCount'];
											echo $array[$i]['FreePassCount']; ?>
								</td>
								<td align="left">
									<?php  $paidRenewal=$paidRenewal+$array[$i]['PaidPassCount'];
											echo $array[$i]['PaidPassCount']; ?>
								</td>
								<td align="left">
									<?php echo '$ '.$array[$i]['PaidPassAmount']; 
										  $totalCost=$totalCost+$array[$i]['PaidPassAmount'];
									?>
								</td>
							</tr>
						<?php } ?>
						<tr>
							<td></td>
							<td><b>TOTAL</b></td>
							<td><b><?php echo $totalRenewal; ?></b></td>
							<td><b><?php echo $freeRenewal; ?></b></td>
							<td><b><?php echo $paidRenewal; ?></b></td>
							<td><b><?php echo '$ '.$totalCost; ?></b></td>
						</tr>
				   </tbody>
				</table>
				
		</div>
<!--------------------MODAL START--------------------->
<div id="propertyModal" class="modal fade" aria-hidden="true" role="dialog" tabindex="-1" >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
				<h4 id='pName'></h4>
                <button class="close" aria-hidden="true" data-dismiss="modal" type="button"><i class="fa  fa-times "></i></button>
            </div>
            <div class="modal-body">
                <div id="pass_details">
				
                </div>
            </div>
        </div>
    </div>
</div>
<!--------------------MODAL END----------------------->
<script type="text/javascript">
	function getPassesData(pName,property,to,from){
			$.blockUI({    
							message: '<h4>Loading..</h4>',  
							css: { 
									border: 'none', 
									padding: '8px', 
									backgroundColor: '#000', 
									'-webkit-border-radius': '10px', 
									'-moz-border-radius': '10px', 
									opacity: .5, 
                                    width:'35%',
									color: '#fff' 
						}
				  });
		$('#pName').text(pName); 
		$.ajax({
				url: "/admin/Transactions/get_renewal_passwise?toDate="+to+"&fromDate="+from+"&propertyId="+property,
				type: "get",
				success: function (rsp) {
					$('#pass_details').html(rsp); 
					$('#propertyModal').modal('show'); 
					$.unblockUI(); 
				}
		});
	}
	  

</script>
