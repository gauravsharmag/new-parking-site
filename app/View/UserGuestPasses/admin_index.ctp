<div class="page-content-wrapper">
	<div class="page-content" style="min-height:1027px">
		<!-- BEGIN PAGE HEADER-->
		<div class="row">
			<div class="col-md-12">
				<!-- BEGIN PAGE TITLE & BREADCRUMB-->
				<h3 class="page-title">
				Passes <small>Guest Passes</small>
				</h3>
				<ul class="page-breadcrumb breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<a href="/admin/Users/superAdminHome">Home</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="#">Guest Pass Activation</a>
					</li>
				</ul>
				<!-- END PAGE TITLE & BREADCRUMB-->
			</div>
		</div>
		<!-- END PAGE HEADER-->
		<div id="flashMessages">
			<h2><?php  echo $this->Session->flash();?></h2>
		</div>
		<!-- BEGIN PAGE CONTENT-->
		<div class="row">
			<div class="col-md-12">
				<form class="form-inline" role="form">
					<div class="form-group">
						<?php
							echo $this->Form->input('current_property',array( 
																			'type'=>'select',
																			'options' => $property_list,
																			'label'=>false,
																			
																			'class'=>'form-control' 
																		));
						?>
					</div>
					<div class="form-group">
						<div data-date-format="mm/dd/yyyy" data-date="12/05/2015" class="input-group input-medium date-picker input-daterange col-md-12">
							<input type="text" name="from" id="from" class="form-control">
							<span class="input-group-addon">
							to </span>
							<input type="text" name="to" id="to" class="form-control">
						</div>
					</div>
					<button type="button" onclick="getReport();" class="btn btn-red">Get Report</button>
				</form>
				<hr>
				<h3 id="totalDays"></h3>
				<br>
				<div class="table-scrollable">
					<table id="userGuestPass" class="table table-striped table-bordered table-hover" >
						<thead>
							<tr>
								<th>Customer</th>
								<th>By User</th>
								<th>Property</th>
								<th>Vehicle</th>
								<th>Days</th>
								<th>Paid</th>
								<th>Amount</th>
								<th>From</th> 
								<th>To</th> 
							</tr>
						</thead>
						<tbody>
							<tr>
								<td colspan="8" class="dataTables_empty">.....SELECT OPTIONS......</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<?php 
	echo $this->Html->script(array('block.js','moment.min.js')); 
?>
<?php echo $this->Html->script('//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js'); ?> 
<?php echo $this->Html->script('/assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js'); ?>
<?php echo $this->Html->script('/assets/admin/pages/scripts/components-pickers.js');   ?>
<?php echo $this->Html->css('/assets/global/plugins/bootstrap-datepicker/css/datepicker3.css'); ?>
<script>
    jQuery(document).ready(function() {     
      ComponentsPickers.init(); 
    });
</script>
<?php echo $this->Html->script('//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js'); ?>
<?php echo $this->Html->css('//cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css'); ?>	

<script>
	function getReport(){
		var toDate="";
		if($('#to').val()){
			toDate=$('#to').val();
		}else{
			var dt = new Date();
			$('#to').val((dt.getMonth()+1) + '/' + (dt.getDate()-1) + '/' +  dt.getFullYear());
			toDate=(dt.getMonth()+1) + '/' + (dt.getDate()-1) + '/' +  dt.getFullYear();
		}
		var data = {};
		var fromDate=$('#from').val();
		data[0]=$('#from').val();
		data[1]=toDate;
		var dataSend = JSON.stringify(data);
		$('.dataTables_empty').html('LOADING');
		$.ajax({
				url: "/admin/UserGuestPasses/cost?property_id="+$('#current_property').val()+"&from="+fromDate+"&toDate="+toDate, 
				dataType: "json",
				success: function (response) {
					if(response){
						$('#totalDays').html('TOTAL DAYS : '+response);
					}else{
						$('#totalDays').html('TOTAL DAYS : 0');
					}
				}
		});
		$('#userGuestPass').dataTable({ 
			 "bProcessing": false,
			 "bServerSide": true,
			 "bDestroy": true,
			 "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
			 "order": [[ 7, "desc" ]],
			 "columnDefs": [{"className": "dt-center", "targets": "_all"},
							{
								"targets": 0,
								"data": function ( row, type, val, meta ) {
											return row.first_name+' '+row.last_name;
								}
						   },
							{
								"targets": 7,
								"data": function ( row, type, val, meta ) {
											var date = moment(row.created);
											var newDate = date.format(" MMM DD YY HH:mm:ss"); 
											return newDate;
								}
						   },
							{
								"targets": 8,
								"data": function ( row, type, val, meta ) {
											var date = moment(row.to_date);
											var newDate = date.format("MMM DD YY HH:mm:ss"); 
											return newDate;
								}
						   }
						],
			"sAjaxSource":"/admin/UserGuestPasses/getPasses?property_id="+$('#current_property').val()+"&from="+fromDate+"&toDate="+toDate,
			 "language": {
				"zeroRecords": "No records to display"
			  }
		});
	}
</script>
