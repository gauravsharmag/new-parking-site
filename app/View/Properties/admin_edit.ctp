
<?php echo $this->Html->script(array('/assets/global/plugins/ckeditor/ckeditor'))?>
<body>
<div class="page-content-wrapper">
		<div class="page-content">
			<div id="flashMessages">
                	<h2><?php  echo $this->Session->flash();?></h2>
            </div>
<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
				<h3 class="page-title">Property <small><?php echo $this->request->data['Property']['name'] ;?></small>
                					</h3>
                					<ul class="page-breadcrumb breadcrumb">
                						<li>
                							<i class="fa fa-home"></i>
                							<?php echo $this->Html->link('Properties',array('controller'=>'properties','action'=>'index')); ?>
                							<i class="fa fa-angle-right"></i>
                						</li>
                						<li>
                							<?php echo __('Edit Property Details'); ?>

                						</li>

                					</ul>
					<div class="tabbable tabbable-custom boxless tabbable-reversed">


							<div class="tab-pane active" id="tab_0">
								<div class="portlet box green">
									<div class="portlet-title">
										<div class="caption">
											<i class="fa fa-gift"></i><?php  echo __('Edit Property '.' '.$this->request->data['Property']['name']); ?>
										</div>

									</div>

										<!-- BEGIN FORM-->

									<div class="portlet-body form">

									<?php echo $this->Form->create('Property',array('type'=>'file','class'=>'form-horizontal')); //debug($this->request->data);?>
                                    <div class="form-body">
                                              <h3><?php echo $this->Session->flash();?></h3>
                                       <?php
												echo $this->Form->input('id');
												 echo $this->Form->input('name',array('div'=>array('class'=>'form-group'),
                                                                 'label'=>array('class'=>'col-md-3 control-label','text'=>'Name'),
                                                                 'between'=>'<div class="col-md-4">',
                                                                 'error'=>array('attributes'=>array('class'=>'model-error')),
                                                                 'after'=>'</div>',
                                                                 'class'=>'form-control'));
         	                                    echo $this->Form->input('sub_domain',array('div'=>array('class'=>'form-group'),
                                                                'label'=>array('class'=>'col-md-3 control-label','text'=>'Sub Domain'),
                                                                'between'=>'<div class="col-md-4">',
                                                                'error'=>array('attributes'=>array('class'=>'model-error')),
                                                                'after'=>'</div>',
                                                                'class'=>'form-control'));
		                                       // echo "<b>".$this->Form->label('logo')."</b>";
		                                       // echo $this->Html->image('/img/logo/'.$this->request->data['Property']['logo']);echo"<br>";
		                               ?>


                                                    <div class="form-group">
                                                	<label class="control-label col-md-3">Property logo:</label>
                                                	  <div class="col-md-9">
                                                		<p class="form-control-static">
                                                			<img style="max-height:200px; max-width:400px;" src=<?php echo $this->webroot."img/logo/".h($this->request->data['Property']['logo']);?> alt='property logo'>

                                                		</p>
                                                		 <!-- <label id="flip" class="control-label col-md-3">Click To Change Logo</label>-->

                                                		</div>
                                                	</div>
                                                	<?php echo $this->Form->input('logo',array('type'=>'file','label'=>array('class'=>'col-md-4 control-label','text'=>'Click To Change Logo'),
                                                                                                    		                                                              'div'=>array('class'=>'form-group'),
                                                                                                    		                                                              'between'=>'<div class="col-md-4">',
                                                                                                                                                                           'after'=>'</div>',
                                                                                                                                                                           'class'=>'form-control'
                                                                                   ));
	                                            echo $this->Form->input('password',array('div'=>array('class'=>'form-group'),
                                                                'label'=>array('class'=>'col-md-3 control-label','text'=>'Password'),
                                                                'between'=>'<div class="col-md-4">',
                                                                'error'=>array('attributes'=>array('class'=>'model-error')),
                                                                'error'=>array('attributes'=>array('class'=>'model-error')),
                                                                'after'=>'</div>',
                                                                'class'=>'form-control'));
            		                            echo $this->Form->input('terms_and_conditions',array('div'=>array('class'=>'form-group'),
                                                                'label'=>array('class'=>'col-md-3 control-label','text'=>'Terms And Conditions'),
                                                                'between'=>'<div class="col-md-9">',
                                                                'error'=>array('attributes'=>array('class'=>'model-error')),
                                                                'error'=>array('attributes'=>array('class'=>'model-error')),
                                                                'after'=>'</div>',
                                                                'class'=>'ckeditor form-control'));
                                                 echo $this->Form->input('free_guest_credits',array('div'=>array('class'=>'form-group'),
                                                                 'label'=>array('class'=>'col-md-3 control-label','text'=>'Free Guest Credits'),
                                                                 'between'=>'<div class="col-md-4">',
                                                                 'error'=>array('attributes'=>array('class'=>'model-error')),
                                                                 'error'=>array('attributes'=>array('class'=>'model-error')),
                                                                 'after'=>'</div>',
                                                                 'class'=>'form-control'));               
            		                             echo $this->Form->input('total_passes_per_user',array('div'=>array('class'=>'form-group'),
                                                                 'label'=>array('class'=>'col-md-3 control-label','text'=>'Passes Per User'),
                                                                 'between'=>'<div class="col-md-4">',
                                                                 'error'=>array('attributes'=>array('class'=>'model-error')),
                                                                 'error'=>array('attributes'=>array('class'=>'model-error')),
                                                                 'after'=>'</div>',
                                                                 'class'=>'form-control'));
            		                             echo $this->Form->input('total_space',array('div'=>array('class'=>'form-group'),
                                                                 'label'=>array('class'=>'col-md-3 control-label','text'=>'Total Space'),
                                                                 'error'=>array('attributes'=>array('class'=>'model-error')),
                                                                 'error'=>array('attributes'=>array('class'=>'model-error')),
                                                                 'between'=>'<div class="col-md-4">',
                                                                 'after'=>'</div>',
                                                                 'class'=>'form-control'));
														/*for($i=0;$i<count($adminArray);$i++){
															echo $this->Form->input('current_admin',array('value'=>$adminArray[$i],
																											'disabled'=>true,
																											'div'=>array('class'=>'form-group'),
																											'label'=>array('class'=>'col-md-3 control-label','text'=>'Current Admin'),
																											'between'=>'<div class="col-md-4"><div class="input-group">',
																											'after'=>'<span class="input-group-addon"><i class="fa fa-user"></i></span></div></div>',
																											'class'=>'form-control',
																											'placeholder'=>'Password'));
														}*/
														echo $this->Form->input('property_admin',array( 'type'=>'select',
																										'options' => $propertyAdmin,
																										'selected'=>$ids,
																										'empty'=>'Select Administrator',
																										'div'=>array('class'=>'form-group'),
																										'multiple' => true,
																										'error'=>array('attributes'=>array('class'=>'model-error')),
																										'label'=>array('class'=>'col-md-3 control-label',
																										'text'=>'Property Admin'),'between'=>'<div class="col-md-4">',
																										'after'=>'</div>',
																										'class'=>'form-control' ));
													echo $this->Form->input('coupon_selection',array( 'type'=>'select',
																										'options' => array('0'=>'NO','1'=>'YES'),
																										'div'=>array('class'=>'form-group'),
																										'error'=>array('attributes'=>array('class'=>'model-error')),
																										'label'=>array('class'=>'col-md-3 control-label',
																										'text'=>'Coupon Registration Required ?'),'between'=>'<div class="col-md-4">',
																										'after'=>'</div>',
																										'class'=>'form-control' ));
														/*echo $this->Form->input('change_admin',array('type'=>'checkbox','div'=>array('class'=>'form-group'),
                                                                                'label'=>false,
                                                                                 'before'=>'<label class="col-md-3 control-label">Change Administrator ?</label><div class="col-md-4">',
                                                                                'after'=>'</div>',
                                                                                 'class'=>'form-control'));*/
												

	                                    ?>
                                           <div class="form-actions fluid">
												<div class="col-md-offset-3 col-md-9">
													<button type="submit" class="btn blue">Submit</button>
													<!--<button type="button" class="btn default">Cancel</button>-->
												</div>
											</div>
                                         <?php echo $this->Form->end(); ?>
                                     </div>

										<!-- END FORM-->
									</div>
								</div>

							</div>



					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->

</body>
