<?php echo $this->Html->script(array('//cdnjs.cloudflare.com/ajax/libs/ckeditor/4.4.1/ckeditor.js'))?>
<div class="page-content-wrapper">
		<div class="page-content">
<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
				<div id="flashMessages">
                	<h2><?php  echo $this->Session->flash();?></h2>
                </div>
					<div class="tabbable tabbable-custom boxless tabbable-reversed">
						
						
							<div class="tab-pane active" id="tab_0">
								<div class="portlet box green">
									<div class="portlet-title">
										<div class="caption">
											<i class="fa fa-gift"></i><?php echo __('Add Property'); ?>
										</div>
										
									</div>
                                   
										<!-- BEGIN FORM-->
										
									<div class="portlet-body form">

									<?php echo $this->Form->create('Property',array('type'=>'file','class'=>'form-horizontal'));
									// debug($roles);
								//pr($propertyMng);
									?>
                                    <div class="form-body">
												
                                         
                                            <?php 
                                       			echo $this->Form->input('name',array('required'=>true,'div'=>array('class'=>'form-group'),'error'=>array('attributes'=>array('class'=>'model-error')),'label'=>array('class'=>'col-md-3 control-label','text'=>'Name'),'between'=>'<div class="col-md-4">','after'=>'</div>','class'=>'form-control'));						 
									  			echo $this->Form->input('sub_domain',array('required'=>true,'div'=>array('class'=>'form-group'),'error'=>array('attributes'=>array('class'=>'model-error')),'label'=>array('class'=>'col-md-3 control-label','text'=>'Sub Domain'),'between'=>'<div class="col-md-4">','after'=>'</div>','class'=>'form-control'));
												echo $this->Form->input('property_admin',array( 'type'    => 'select','options' => $propertyAdmin,'empty'=>'Choose One','required'=>false,'div'=>array('class'=>'form-group'),'error'=>array('attributes'=>array('class'=>'model-error')),'label'=>array('class'=>'col-md-3 control-label','text'=>'Property Admin'),'between'=>'<div class="col-md-4">','after'=>'</div>','class'=>'form-control' ));?>
											<div class="panel panel-default">
                                            <div class="panel-heading"> Property Manager </div>
                                            <div class="panel-body">

                                             <?php
                                                echo $this->Form->input('first_name',array('required'=>true,'div'=>array('class'=>'form-group'),'error'=>array('attributes'=>array('class'=>'model-error')),'label'=>array('class'=>'col-md-3 control-label','text'=>'First Name'),'between'=>'<div class="col-md-4">','after'=>'</div>','class'=>'form-control','placeholder'=>'First Name'));
												echo $this->Form->input('last_name',array('required'=>true,'div'=>array('class'=>'form-group'),'error'=>array('attributes'=>array('class'=>'model-error')),'label'=>array('class'=>'col-md-3 control-label','text'=>'Last Name'),'between'=>'<div class="col-md-4">','after'=>'</div>','class'=>'form-control','placeholder'=>'Last Name'));
                                       			echo $this->Form->input('username',array('required'=>true,'div'=>array('class'=>'form-group'),'error'=>array('attributes'=>array('class'=>'model-error')),'label'=>array('class'=>'col-md-3 control-label','text'=>'Username'),'between'=>'<div class="col-md-4">','after'=>'</div>','class'=>'form-control','placeholder'=>'Username'));

												echo $this->Form->input('email',array('required'=>true,'div'=>array('class'=>'form-group'),'error'=>array('attributes'=>array('class'=>'model-error')),'label'=>array('class'=>'col-md-3 control-label','text'=>'Email Address'),'between'=>'<div class="col-md-4"><div class="input-group"><span class="input-group-addon"><i class="fa fa-envelope"></i>														</span>','after'=>'</div></div>','class'=>'form-control','placeholder'=>'Email Address','type'=>'email'));

												echo $this->Form->input('manager_password',array('type'=>'password','required'=>true,'div'=>array('class'=>'form-group'),'error'=>array('attributes'=>array('class'=>'model-error')),'label'=>array('class'=>'col-md-3 control-label','text'=>'Password'),'between'=>'<div class="col-md-4"><div class="input-group">','after'=>'<span class="input-group-addon"><i class="fa fa-user"></i>													</span></div></div>','class'=>'form-control','placeholder'=>'Password'));

												echo $this->Form->input('password_confirmation',array('required'=>true,'div'=>array('class'=>'form-group'),'error'=>array('attributes'=>array('class'=>'model-error')),'label'=>array('class'=>'col-md-3 control-label','text'=>'Password Confirmation'),'between'=>'<div class="col-md-4"><div class="input-group">','after'=>'<span class="input-group-addon"><i class="fa fa-user"></i>													</span></div></div>','class'=>'form-control','placeholder'=>'Password Confirmation','type'=>'password'));

												echo $this->Form->hidden('role_id',array('default'=>'3'));

                                             ?>
                                            </div>
                                          </div>
											<?php
												echo $this->Form->input('logo',array('required'=>true,'type'=>'file','div'=>array('class'=>'form-group'),'error'=>array('attributes'=>array('class'=>'model-error')),'label'=>array('class'=>'col-md-3 control-label','text'=>'Logo'),'between'=>'<div class="col-md-4">','after'=>'</div>','class'=>'form-control'));
												
												echo $this->Form->input('password',array('required'=>true,'div'=>array('class'=>'form-group'),'error'=>array('attributes'=>array('class'=>'model-error')),'label'=>array('class'=>'col-md-3 control-label','text'=>'Property Password'),'between'=>'<div class="col-md-4">','after'=>'</div>','class'=>'form-control'));
												
												echo $this->Form->input('terms_and_conditions',array('required'=>true,'div'=>array('class'=>' form-group'),'error'=>array('attributes'=>array('class'=>'model-error')),'label'=>array('class'=>'col-md-3 control-label','text'=>'Terms And Conditions'),'between'=>'<div class="col-md-4">','after'=>'</div>','class'=>'ckeditor form-control', 'type'=>'textarea' ));
		        								echo $this->Form->input('free_guest_credits',array('required'=>true,'div'=>array('class'=>'form-group'),'error'=>array('attributes'=>array('class'=>'model-error')),'label'=>array('class'=>'col-md-3 control-label','text'=>'Free Guest Credits'),'between'=>'<div class="col-md-4">','after'=>'</div>','class'=>'form-control'));
		        							    echo $this->Form->input('total_passes_per_user',array('required'=>true,'div'=>array('class'=>'form-group'),'error'=>array('attributes'=>array('class'=>'model-error')),'label'=>array('class'=>'col-md-3 control-label','text'=>'Total Passes Per User'),'between'=>'<div class="col-md-4">','after'=>'</div>','class'=>'form-control'));
		        																	
												echo $this->Form->input('total_space',array('div'=>array('class'=>'form-group'),'error'=>array('attributes'=>array('class'=>'model-error')),'label'=>array('class'=>'col-md-3 control-label','text'=>'Total Space'),'between'=>'<div class="col-md-4">','after'=>'</div>','class'=>'form-control'));
												echo $this->Form->input('coupon_selection',array( 'type'    => 'select','options' => array('0'=>'NO','1'=>'YES'),'div'=>array('class'=>'form-group'),'error'=>array('attributes'=>array('class'=>'model-error')),'label'=>array('class'=>'col-md-3 control-label','text'=>'Coupon Registration Required ?'),'between'=>'<div class="col-md-4">','after'=>'</div>','class'=>'form-control' ));
											  ?>
                                          <div class="form-actions fluid">
												<div class="col-md-offset-3 col-md-9">
													<button type="submit" class="btn blue">Submit</button>
													<!--<button type="button" class="btn default">Cancel</button>-->
												</div>
											</div>
                                         <?php echo $this->Form->end(); ?>   
                                     </div>   
                                   
										<!-- END FORM-->
									</div>
								</div>							
								
							</div>
							
							
						
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
  

   </div>
 </div>
