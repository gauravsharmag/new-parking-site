<div class="properties view">
<h2><?php echo __('Property'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($property['Property']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($property['Property']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Sub Domain'); ?></dt>
		<dd>
			<?php echo h($property['Property']['sub_domain']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Logo'); ?></dt>
		<dd>
			<?php echo h($property['Property']['logo']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Password'); ?></dt>
		<dd>
			<?php echo h($property['Property']['password']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Terms And Conditions'); ?></dt>
		<dd>
			<?php echo h($property['Property']['terms_and_conditions']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Total Passes Per User'); ?></dt>
		<dd>
			<?php echo h($property['Property']['total_passes_per_user']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Total Space'); ?></dt>
		<dd>
			<?php echo h($property['Property']['total_space']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Free Guest Credits'); ?></dt>
		<dd>
			<?php echo h($property['Property']['free_guest_credits']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Property'), array('action' => 'edit', $property['Property']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Property'), array('action' => 'delete', $property['Property']['id']), array(), __('Are you sure you want to delete # %s?', $property['Property']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Properties'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Property'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Customer Passes'), array('controller' => 'customer_passes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer Pass'), array('controller' => 'customer_passes', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Packages'), array('controller' => 'packages', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Package'), array('controller' => 'packages', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Passes'), array('controller' => 'passes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Pass'), array('controller' => 'passes', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Vehicles'), array('controller' => 'vehicles', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Vehicle'), array('controller' => 'vehicles', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Property Users'), array('controller' => 'property_users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Property User'), array('controller' => 'property_users', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Customer Passes'); ?></h3>
	<?php if (!empty($property['CustomerPass'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th><?php echo __('Vehicle Id'); ?></th>
		<th><?php echo __('Property Id'); ?></th>
		<th><?php echo __('Transaction Id'); ?></th>
		<th><?php echo __('Status'); ?></th>
		<th><?php echo __('Membership Vaild Upto'); ?></th>
		<th><?php echo __('Pass Valid Upto'); ?></th>
		<th><?php echo __('RFID Tag Number'); ?></th>
		<th><?php echo __('Assigned Location'); ?></th>
		<th><?php echo __('Package Id'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($property['CustomerPass'] as $customerPass): ?>
		<tr>
			<td><?php echo $customerPass['id']; ?></td>
			<td><?php echo $customerPass['user_id']; ?></td>
			<td><?php echo $customerPass['vehicle_id']; ?></td>
			<td><?php echo $customerPass['property_id']; ?></td>
			<td><?php echo $customerPass['transaction_id']; ?></td>
			<td><?php echo $customerPass['status']; ?></td>
			<td><?php echo $customerPass['membership_vaild_upto']; ?></td>
			<td><?php echo $customerPass['pass_valid_upto']; ?></td>
			<td><?php echo $customerPass['RFID_tag_number']; ?></td>
			<td><?php echo $customerPass['assigned_location']; ?></td>
			<td><?php echo $customerPass['package_id']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'customer_passes', 'action' => 'view', $customerPass['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'customer_passes', 'action' => 'edit', $customerPass['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'customer_passes', 'action' => 'delete', $customerPass['id']), array(), __('Are you sure you want to delete # %s?', $customerPass['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Customer Pass'), array('controller' => 'customer_passes', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Packages'); ?></h3>
	<?php if (!empty($property['Package'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Property Id'); ?></th>
		<th><?php echo __('Name'); ?></th>
		<th><?php echo __('Cost'); ?></th>
		<th><?php echo __('Start Date'); ?></th>
		<th><?php echo __('Expiration Date'); ?></th>
		<th><?php echo __('Password'); ?></th>
		<th><?php echo __('Is Fixed Duration'); ?></th>
		<th><?php echo __('Total Spaces Each Package'); ?></th>
		<th><?php echo __('Duration'); ?></th>
		<th><?php echo __('Duration Type'); ?></th>
		<th><?php echo __('Is Recurring'); ?></th>
		<th><?php echo __('Is Guest'); ?></th>
		<th><?php echo __('Is Required'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($property['Package'] as $package): ?>
		<tr>
			<td><?php echo $package['id']; ?></td>
			<td><?php echo $package['property_id']; ?></td>
			<td><?php echo $package['name']; ?></td>
			<td><?php echo $package['cost']; ?></td>
			<td><?php echo $package['start_date']; ?></td>
			<td><?php echo $package['expiration_date']; ?></td>
			<td><?php echo $package['password']; ?></td>
			<td><?php echo $package['is_fixed_duration']; ?></td>
			<td><?php echo $package['total_spaces_each_package']; ?></td>
			<td><?php echo $package['duration']; ?></td>
			<td><?php echo $package['duration_type']; ?></td>
			<td><?php echo $package['is_recurring']; ?></td>
			<td><?php echo $package['is_guest']; ?></td>
			<td><?php echo $package['is_required']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'packages', 'action' => 'view', $package['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'packages', 'action' => 'edit', $package['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'packages', 'action' => 'delete', $package['id']), array(), __('Are you sure you want to delete # %s?', $package['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Package'), array('controller' => 'packages', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Passes'); ?></h3>
	<?php if (!empty($property['Pass'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Property Id'); ?></th>
		<th><?php echo __('Cost 1st Year'); ?></th>
		<th><?php echo __('Cost After 1st Year'); ?></th>
		<th><?php echo __('Pass Duration'); ?></th>
		<th><?php echo __('Duration Type'); ?></th>
		<th><?php echo __('Deposit'); ?></th>
		<th><?php echo __('Expiration Date'); ?></th>
		<th><?php echo __('Sort Order'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($property['Pass'] as $pass): ?>
		<tr>
			<td><?php echo $pass['id']; ?></td>
			<td><?php echo $pass['property_id']; ?></td>
			<td><?php echo $pass['cost_1st_year']; ?></td>
			<td><?php echo $pass['cost_after_1st_year']; ?></td>
			<td><?php echo $pass['pass_duration']; ?></td>
			<td><?php echo $pass['duration_type']; ?></td>
			<td><?php echo $pass['deposit']; ?></td>
			<td><?php echo $pass['expiration_date']; ?></td>
			<td><?php echo $pass['sort_order']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'passes', 'action' => 'view', $pass['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'passes', 'action' => 'edit', $pass['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'passes', 'action' => 'delete', $pass['id']), array(), __('Are you sure you want to delete # %s?', $pass['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Pass'), array('controller' => 'passes', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Vehicles'); ?></h3>
	<?php if (!empty($property['Vehicle'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Property Id'); ?></th>
		<th><?php echo __('Package Id'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th><?php echo __('Make'); ?></th>
		<th><?php echo __('Model'); ?></th>
		<th><?php echo __('Color'); ?></th>
		<th><?php echo __('License Plate Number'); ?></th>
		<th><?php echo __('License Plate State'); ?></th>
		<th><?php echo __('Reserved Space'); ?></th>
		<th><?php echo __('Last 4 Digital Of Vin'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($property['Vehicle'] as $vehicle): ?>
		<tr>
			<td><?php echo $vehicle['id']; ?></td>
			<td><?php echo $vehicle['property_id']; ?></td>
			<td><?php echo $vehicle['package_id']; ?></td>
			<td><?php echo $vehicle['user_id']; ?></td>
			<td><?php echo $vehicle['make']; ?></td>
			<td><?php echo $vehicle['model']; ?></td>
			<td><?php echo $vehicle['color']; ?></td>
			<td><?php echo $vehicle['license_plate_number']; ?></td>
			<td><?php echo $vehicle['license_plate_state']; ?></td>
			<td><?php echo $vehicle['reserved_space']; ?></td>
			<td><?php echo $vehicle['last_4_digital_of_vin']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'vehicles', 'action' => 'view', $vehicle['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'vehicles', 'action' => 'edit', $vehicle['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'vehicles', 'action' => 'delete', $vehicle['id']), array(), __('Are you sure you want to delete # %s?', $vehicle['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Vehicle'), array('controller' => 'vehicles', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Property Users'); ?></h3>
	<?php if (!empty($property['PropertyUser'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th><?php echo __('Property Id'); ?></th>
		<th><?php echo __('Role Id'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($property['PropertyUser'] as $propertyUser): ?>
		<tr>
			<td><?php echo $propertyUser['id']; ?></td>
			<td><?php echo $propertyUser['user_id']; ?></td>
			<td><?php echo $propertyUser['property_id']; ?></td>
			<td><?php echo $propertyUser['role_id']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'property_users', 'action' => 'view', $propertyUser['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'property_users', 'action' => 'edit', $propertyUser['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'property_users', 'action' => 'delete', $propertyUser['id']), array(), __('Are you sure you want to delete # %s?', $propertyUser['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Property User'), array('controller' => 'property_users', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
