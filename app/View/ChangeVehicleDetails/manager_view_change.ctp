<div class="page-content-wrapper">
    <div class="page-content">
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                   <?php echo CakeSession::read('PropertyName'); ?><small> View Change Requests</small>
                </h3>
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <?php echo $this->Html->link('Home',array('controller'=>'users','action'=>'manager_home_page')); ?>
                       
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="">View Change Requests</a>
                    </li>
                    <li class="pull-right">
                        <div id="dashboard-report-range" class="dashboard-date-range tooltips" data-placement="top" data-original-title="Change dashboard date range">
                            <i class="icon-calendar"></i>
                            <span></span>
                            <i class="fa fa-angle-down"></i>
                        </div>
                    </li>
                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <div id="flashMessages">
            <h2><?php echo $this->Session->flash(); ?></h2>
        </div>
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN SAMPLE TABLE PORTLET-->
                <div class="portlet box purple">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-cogs"></i>All Change Requests ( <?php echo CakeSession::read('PropertyName'); ?>)
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse">
                            </a>

                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="table-scrollable">
                            <table class="table table-hover">
                                <thead>
                                    <tr>

                                        <th><?php echo $this->Paginator->sort('Change Details Request'); ?></th>
                                        <th><?php echo $this->Paginator->sort( 'Vehicle Owner'); ?></th>
                                        <th><?php echo $this->Paginator->sort('created', 'Added On'); ?></th>
                                        <th><?php echo $this->Paginator->sort('changed', 'Status'); ?></th>
                                        
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($viewQueries as $view) { ?>

                                        <tr>
                                            <td><?php if($view['ChangeVehicleDetail']['changed']==0){ ?>
                                                <strong>For Vehicle Number:   <?php echo $this->Html->link($view['ChangeVehicleDetail']['state']." ".$view['ChangeVehicleDetail']['plate_number'], array('controller' => 'ChangeVehicleDetails', 'action' => 'manager_change', $view['ChangeVehicleDetail']['id'])); ?>
                                                </strong>
                                            <?php }else{ ?>
                                                <strong>For Vehicle Number:   <?php echo $this->Html->link($view['ChangeVehicleDetail']['state']." ".$view['ChangeVehicleDetail']['plate_number'], array('controller' => 'ChangeVehicleDetails', 'action' => 'manager_done', $view['ChangeVehicleDetail']['id'])); ?>
                                                </strong>
                                            <?php } ?>

                                            </td>
                                            <td>
                                                <?php echo $view['User']['username']; ?>
                                            </td>
                                            <td>
                                                <?php 
                                                 echo $this->Time->format($view['ChangeVehicleDetail']['created'], '%B %e, %Y %H:%M %p');
                                              ?>
                                            </td>
                                            
                                            <td>

                                                <?php if ($view['ChangeVehicleDetail']['changed'] == 0) { ?>
                                                    <span class="label label-warning">
                                                    <?php } else { ?>
                                                        <span class="label label-success">
                                                        <?php } ?>
                                                        <?php
                                                        if ($view['ChangeVehicleDetail']['changed'] == 0) {
                                                            echo "Pending";
                                                        } else {
                                                            echo "Done by ".$view['ChangeVehicleDetail']['done_by'];
                                                        }
                                                        ?> </span>
                                            </td>
                                            
                                            

                                        </tr>

                                    <?php } ?>

                                </tbody>
                            </table>
                        </div>
                        <p>
                            <?php
                            echo $this->Paginator->counter(array(
                                'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
                            ));
                            ?>  
                        </p>
                        <div class="paging">
                            <?php
                            echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
                            echo " ";
                            echo $this->Paginator->numbers(array('separator' => ''));
                            echo " ";
                            echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
                            ?>
                        </div>
                    </div>
                </div>
                <!-- END SAMPLE TABLE PORTLET-->
            </div>
        </div>
    </div>
</div>