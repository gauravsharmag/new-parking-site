
<div class="page-content-wrapper">
    <div class="page-content" style="min-height:1027px">

        <!-- BEGIN PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">

                    Vehicles <small> Changed Vehicle Details</small>
                </h3>
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <?php echo $this->Html->link('Home',array('controller'=>'users','action'=>'customerHomePage')); ?>
                       <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="#">Changed Vehicle Details</a>
                    </li>
                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <!-- END PAGE HEADER-->
        <div id="flashMessages">
            <h2><?php echo $this->Session->flash(); ?></h2>
        </div>
        <!-- BEGIN PAGE CONTENT-->
        <div class="row">
            <div class="col-md-12">
                    <!-- BEGIN Portlet PORTLET-->
                    <div class="portlet box yellow">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-gift"></i>All Unapproved Requests
                            </div>
                        </div>
                        <div class="portlet-body">
							<div class="well well-lg">
                                 <h4 class="block">Delete Change Vehicle Requests</h4>
                                        <p> 
											If you want to delete the request to change vehicle please click on delete. Thanks.
                                        </p>
                              </div>
							<table class="table table-striped table-bordered table-hover">
								<thead>
									<tr>
										<th>Current Details</th>
										<th>New Details For Updating</th>
										<th>Actions</th>
									</tr>
								</thead>
								<tbody>
										<?php if($unapprovedRequest){
												foreach ($unapprovedRequest as $details): ?>
													<tr>
														<td>
															<ul class="list-group">
																<li class="list-group-item"> <b>Name : </b>
																	<span> <?php echo $details['Vehicle']['owner']?></span>
																</li>
																<li class="list-group-item"> <b>Make : </b>
																	<span> <?php echo $details['Vehicle']['make']?> </span>
																</li>
																<li class="list-group-item"> <b>Model : </b>
																	<span> <?php echo $details['Vehicle']['model']?> </span>
																</li>
																<li class="list-group-item"> <b>Color : </b>
																	<span> <?php echo $details['Vehicle']['color']?> </span>
																</li>
																<li class="list-group-item"> <b>Plate Number : </b>
																	<span> <?php echo $details['Vehicle']['license_plate_number']?></span>
																</li>
																<li class="list-group-item"> <b>State : </b>
																	<span> <?php echo $details['Vehicle']['license_plate_state']?> </span>
																</li>
																<li class="list-group-item"> <b>Vin : </b>
																	<span> <?php echo $details['Vehicle']['last_4_digital_of_vin']?> </span>
																</li>
															</ul>
														</td>
														<td>
															<ul class="list-group">
																<li class="list-group-item"> <b>Name : </b>
																	<span> <?php echo $details['ChangeVehicleDetail']['vehicle_name']?></span>
																</li>
																<li class="list-group-item"> <b>Make : </b>
																	<span> <?php echo $details['ChangeVehicleDetail']['make']?> </span>
																</li>
																<li class="list-group-item"> <b>Model : </b>
																	<span> <?php echo $details['ChangeVehicleDetail']['model']?> </span>
																</li>
																<li class="list-group-item"> <b>Color : </b>
																	<span> <?php echo $details['ChangeVehicleDetail']['color']?> </span>
																</li>
																<li class="list-group-item"> <b>Plate Number : </b>
																	<span> <?php echo $details['ChangeVehicleDetail']['plate_number']?></span>
																</li>
																<li class="list-group-item"> <b>State : </b>
																	<span> <?php echo $details['ChangeVehicleDetail']['state']?> </span>
																</li>
																<li class="list-group-item"> <b>Vin : </b>
																	<span> <?php echo $details['ChangeVehicleDetail']['vin']?> </span>
																</li>
																<li class="list-group-item"> <b>Reason : </b>
																	<span> <?php echo $details['ChangeVehicleDetail']['reason']?> </span>
																</li>
															</ul>
														</td>
														<td>
															 <?php if($details['ChangeVehicleDetail']['changed']==0){
																	echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $details['ChangeVehicleDetail']['id']), array('class'=>'btn btn-danger '), __('Are you sure you want to remove this request?')); 
																   }elseif($details['ChangeVehicleDetail']['changed']==1){
																		echo '<span class="label label-info"> Approved </span>';
																   }elseif($details['ChangeVehicleDetail']['changed']==9){
																	echo '<span class="label label-danger"> Rejected </span><br>
																			<b>REASON: </b>'.$details['ChangeVehicleDetail']['rejection_reason'].'
																	';
																   }
																	
															  ?>
																  
														</td>
													</tr>
										<?php endforeach; }
															else{
										?>
											<tr>
												<td></td>
												<td>No Un-approved Request Found</td>
												<td></td>
											</tr>
										<?php } ?>
								</tbody>
							</table>
                        </div>
                    </div>
       
            
                
                
             
            </div>

        </div>
    </div>
    <!-- END PAGE CONTENT-->
</div>


