
<div class="page-content-wrapper">
    <div class="page-content" style="min-height:1027px">

        <!-- BEGIN PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">

                    Vehicles <small>Change Vehicle Details</small>
                </h3>
                <ul class="page-breadcrumb breadcrumb">
                    
                    <li>
                        <i class="fa fa-home"></i>
                        <?php echo $this->Html->link('Home',array('controller'=>'users','action'=>'superAdminHome')); ?>
                       <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="#">Change Vehicle Details</a>
                    </li>
                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <!-- END PAGE HEADER-->
        <div id="flashMessages">
            <h2><?php echo $this->Session->flash(); ?></h2>
        </div>
        <!-- BEGIN PAGE CONTENT-->
						 <!--<div class="changeVehicles">
							 <div class="changeVehicleTitle">
                                  <p>User Details</p>
                             </div>
                             <div class="row static-info">
								<div class="col-md-1 name">
									Property :
								</div>
								<div class="col-md-11 value">
									<?php 
											echo $propertyName;
									?>
								</div>
							</div>
                             <div class="row static-info">
								<div class="col-md-1 name">
									User Name :
								</div>
								<div class="col-md-11 value">
									<?php 
											echo '<a href="/admin/users/view_user_details/'.$changeDetail['User']['id'].'" >'.$changeDetail['User']['username'].'</a>';
									?>
								</div>
							</div>
							 <div class="row static-info">
								<div class="col-md-1 name">
									Name :
								</div>
								<div class="col-md-11 value">
									<?php 
											echo $changeDetail['User']['first_name'].' '.$changeDetail['User']['last_name'];
									?>
								</div>
							</div>
							<div class="row static-info">
								<div class="col-md-1 name">  
									Address :
								</div>
								<div class="col-md-11 value">
									<?php 
											echo $changeDetail['User']['address_line_1'].' '.$changeDetail['User']['address_line_2'].'<br>'.
												 $changeDetail['User']['city'].' '.$changeDetail['User']['state'].'<br>'.
												 $changeDetail['User']['zip'].'<br> Email : '.
												 $changeDetail['User']['email'].'<br> Phone : '.
												 $changeDetail['User']['phone'].'';
									?>
								</div>
							</div>
							<div class="row static-info">
								<div class="col-md-1 name">
									Vehicle Details :
								</div>
								<div class="col-md-11 value">
									<?php 
											echo '<a href="/admin/Vehicles/complete_vehicle_detail/'.$changeDetail['Vehicle']['id'].'" >View Vehicle Details</a>';
									?>
								</div>
							</div>
						</div>-->

                        <div class="changeVehicles">
                             <div class="changeVehicleTitle">
                                  <p><i class="fa fa-user" aria-hidden="true"></i> User Details</p>
                             </div>

                            <table class="changeVehicleTable">

                                <tr>  
                                    <td class="titleclass"> Property : </td>
                                    <td> <?php echo $propertyName;?> </td>
                                </tr>

                                <tr>  
                                    <td class="titleclass"> Username : </td>
                                    <td> <?php echo '<a href="/admin/users/view_user_details/'.$changeDetail['User']['id'].'" >'.$changeDetail['User']['username'].'</a>'; ?></td>
                                </tr>

                                <tr>  
                                    <td class="titleclass"> Name : </td>
                                    <td> <?php echo $changeDetail['User']['first_name'].' '.$changeDetail['User']['last_name']; ?></td>
                                </tr>

                                <tr>  
                                    <td class="titleclass"> Address : </td>
                                    <td> <?php 
                                            echo $changeDetail['User']['address_line_1'].' '.$changeDetail['User']['address_line_2'].'<br>'.
                                                 $changeDetail['User']['city'].' '.$changeDetail['User']['state'].'<br>'.
                                                 $changeDetail['User']['zip'].'<br> Email : '.
                                                 $changeDetail['User']['email'].'<br> Phone : '.
                                                 $changeDetail['User']['phone'].'';
                                        ?>
                                    </td>
                                </tr>

                                <tr>  
                                    <td class="titleclass"> Vehicle Details : </td>
                                    <td> <?php echo '<a href="/admin/Vehicles/complete_vehicle_detail/'.$changeDetail['Vehicle']['id'].'" >View Vehicle Details</a>'; ?></td>
                                </tr>
                            </table>

                        </div>



        <div class="row">
            <div class="col-md-12">

                <div class="col-md-6">
                    <!-- BEGIN Portlet PORTLET-->
                    <div class="portlet box green-meadow">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-gift"></i>Current Vehicle Details
                            </div>
                            <div class="tools">
                               
                            </div>
                        </div>
                        <div class="portlet-body"> 
							
                            <?php echo $this->Form->create('ChangeVehicleDetail', array('url'=>array('action'=>'x'),'class' => 'form-horizontal', 'novalidate' => 'novalidate')) ?>
	
                            <div class="form-body">

                                <div class="form-group">
                                    <label class="col-md-4 control-label">Vehicle Name :</label>
                                    <div class="col-md-6">
                                        <?php
                                        echo $this->Form->input('vehiffcle_name', array('label' => false,
                                            'required' => true,
                                            'errorMessage' => false,
                                            'value' => $changeDetail['Vehicle']['owner'],
                                            'disabled' => TRUE,
                                            'class' => 'form-control'));
                                        
                                        ?>
                                    </div>

                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Make :</label>
                                    <div class="col-md-6">


                                        <?php
                                        echo $this->Form->input('maffke', array('label' => false,
                                            'required' => true,
                                            'errorMessage' => false,
                                            'value' => $changeDetail['Vehicle']['make'],
                                            'disabled' => TRUE,
                                            'class' => 'form-control'));
                                      
                                        ?>

                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Model :</label>
                                    <div class="col-md-6">
                                        <?php
                                        echo $this->Form->input('mofdel', array('label' => false,
                                            'errorMessage' => false,
                                            'value' => $changeDetail['Vehicle']['model'],
                                            'disabled' => TRUE,
                                            'class' => 'form-control'));
                                       
                                        ?>
                                    </div>

                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Color :</label>
                                    <div class="col-md-6">
                                        <?php
                                        echo $this->Form->input('colfffor', array('label' => false,
                                            'required' => true,
                                            'errorMessage' => false,
                                            'value' => $changeDetail['Vehicle']['color'],
                                            'disabled' => TRUE,
                                            'class' => 'form-control'));
                                       
                                        ?>
                                    </div>

                                </div>

                                <div class="form-group">
                                    <label class="col-md-4 control-label">Plate Number :</label>
                                    <div class="col-md-6">
                                        <?php
                                        echo $this->Form->input('platdfe_number', array('label' => false,
                                            'required' => true,
                                            'errorMessage' => false,
                                            'value' => $changeDetail['Vehicle']['license_plate_number'],
                                            'disabled' => TRUE,
                                            'class' => 'form-control'));
                                      
                                        ?>
                                    </div>

                                </div>

                                <div class="form-group">
                                    <label class="col-md-4 control-label">State :</label>
                                    <div class="col-md-6">
                                         <?php
                                        echo $this->Form->input('stadsate', array('type' => 'select',
                                            'options' => $states,
                                            'empty' => 'Select One',
                                            'selected'=>$changeDetail['Vehicle']['license_plate_state'],
                                            'label' => false,
                                             'disabled' => TRUE,
                                            'class' => 'form-control'));
                                       
                                        ?>
                                    </div>

                                </div>


                                <div class="form-group">
                                    <label class="col-md-4 control-label">Vin :</label>
                                    <div class="col-md-6">
                                        <?php
                                        echo $this->Form->input('vinass', array('label' => false, 
                                            'required' => true,
                                            'errorMessage' => false,
                                            'value' => $changeDetail['Vehicle']['last_4_digital_of_vin'],
                                            'disabled' => TRUE,
                                            'class' => 'form-control'));
                                        
                                        ?>
                                    </div>

                                </div>
                               
                                <div class="form-group">

                                    <div class="col-md-6">
                                        <?php
                                        echo $this->Form->hidden('vi66n', array('label' => false,
                                        ));
                                       
                                        ?>
                                    </div>

                                </div>
                                <div class="form-group">

                                    <div class="col-md-6">
                                        <?php
                                        echo $this->Form->hidden('vinr56', array('label' => false,
                                        ));
                                       
                                        ?>
                                    </div>

                                </div>
                                <div class="form-group">

                                    <div class="col-md-6">
                                        <?php
                                        echo $this->Form->hidden('vincgh', array('label' => false,
                                        ));
                                        
                                        ?>
                                    </div>

                                </div>


                            </div>


                            <?php echo $this->Form->end(); ?>
                            <!-- END FORM-->
                        </div>   </div>
                    <!-- END Portlet PORTLET-->
                </div>

                <div class="col-md-6">
                    <!-- BEGIN Portlet PORTLET-->
                    <div class="portlet box yellow">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-gift"></i>Submitted Details
                            </div>
                            <div class="tools">

                            </div>
                        </div>
                        <div class="portlet-body">

                            <?php echo $this->Form->create('ChangeVehicleDetail', array('url'=>array('action'=>'change/'.$changeDetail['ChangeVehicleDetail']['id']),'class' => 'form-horizontal')) ?>


                            <div class="form-body">
                                
                                 <div class="form-group">
                                    <label class="col-md-4 control-label">Reason :</label>
                                    <div class="col-md-6">
                                        <?php
                                        echo $this->Form->input('reason', array('label' => false,
                                            'required' => true,
                                            'type'=>'textarea',
                                            'errorMessage' => false,
                                            'value' =>   $changeDetail['ChangeVehicleDetail']['reason'],
                                            'disabled'=>true,
                                            'class' => 'form-control'));
                                        if ($this->Form->isFieldError('reason')) {
                                            echo $this->Form->error('reason', array('div' => false, 'attributes' => array('wrap' => 'span', 'class' => 'model-error')));
                                        }
                                        ?>
                                    </div>

                                </div>

                                <div class="form-group">
                                    <label class="col-md-4 control-label">Vehicle Name :</label>
                                    <div class="col-md-6">
                                        <?php
                                        echo $this->Form->input('vehicle_name', array('label' => false,
                                            'required' => true,
                                            'errorMessage' => false,
                                            'value' => $changeDetail['ChangeVehicleDetail']['vehicle_name'],
                                            'class' => 'form-control'));
                                        if ($this->Form->isFieldError('vehicle_name')) {
                                            echo $this->Form->error('vehicle_name', array('div' => false, 'attributes' => array('wrap' => 'span', 'class' => 'model-error')));
                                        }
                                        ?>
                                    </div>

                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Make :</label>
                                    <div class="col-md-6">

                                        <?php
                                        echo $this->Form->input('make', array('label' => false,
                                            'required' => true,
                                            'errorMessage' => false,
                                            'value' => $changeDetail['ChangeVehicleDetail']['make'],
                                            'class' => 'form-control'));
                                        if ($this->Form->isFieldError('make')) {
                                            echo $this->Form->error('make', array('div' => false, 'attributes' => array('wrap' => 'span', 'class' => 'model-error')));
                                        }
                                        ?>

                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Model :</label>
                                    <div class="col-md-6">
                                        <?php
                                        echo $this->Form->input('model', array('label' => false,
                                            'errorMessage' => false,
                                            'value' => $changeDetail['ChangeVehicleDetail']['model'],
                                            'class' => 'form-control'));
                                        if ($this->Form->isFieldError('model')) {
                                            echo $this->Form->error('model', array('div' => false, 'attributes' => array('wrap' => 'span', 'class' => 'model-error')));
                                        }
                                        ?>
                                    </div>

                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Color :</label>
                                    <div class="col-md-6">
                                        <?php
                                        echo $this->Form->input('color', array('label' => false,
                                            'required' => true,
                                            'errorMessage' => false,
                                            'value' => $changeDetail['ChangeVehicleDetail']['color'],
                                            'class' => 'form-control'));
                                        if ($this->Form->isFieldError('color')) {
                                            echo $this->Form->error('color', array('div' => false, 'attributes' => array('wrap' => 'span', 'class' => 'model-error')));
                                        }
                                        ?>
                                    </div>

                                </div>

                                <div class="form-group">
                                    <label class="col-md-4 control-label">Plate Number :</label>
                                    <div class="col-md-6">
                                        <?php
                                        echo $this->Form->input('plate_number', array('label' => false,
                                            'required' => true,
                                            'errorMessage' => false,
                                            'value' => $changeDetail['ChangeVehicleDetail']['plate_number'],
                                            'class' => 'form-control'));
                                        if ($this->Form->isFieldError('plate_number')) {
                                            echo $this->Form->error('plate_number', array('div' => false, 'attributes' => array('wrap' => 'span', 'class' => 'model-error')));
                                        }
                                        ?>
                                    </div>

                                </div>

                                <div class="form-group">
                                    <label class="col-md-4 control-label">State :</label>
                                    <div class="col-md-6">
                                         <?php
                                        echo $this->Form->input('state', array('type' => 'select',
                                            'options' => $states,
                                            'empty' => 'Select One',
                                            'selected'=>$changeDetail['ChangeVehicleDetail']['state'],
                                            'label' => false,
                                            
                                            'class' => 'form-control'));
                                        if ($this->Form->isFieldError('state')) {
                                            echo $this->Form->error('state', array('div' => false, 'attributes' => array('wrap' => 'span', 'class' => 'model-error')));
                                        }
                                        ?>


                                    </div>

                                </div>


                                <div class="form-group">
                                    <label class="col-md-4 control-label">Vin :</label>
                                    <div class="col-md-6">
                                        <?php
                                        echo $this->Form->input('vin', array('label' => false,
                                            'required' => true,
                                            'errorMessage' => false,
                                            'value' => $changeDetail['ChangeVehicleDetail']['vin'],
                                            'class' => 'form-control'));
                                        if ($this->Form->isFieldError('vin')) {
                                            echo $this->Form->error('vin', array('div' => false, 'attributes' => array('wrap' => 'span', 'class' => 'model-error')));
                                        }
                                        ?>
                                    </div>

                                </div>
								 <div class="form-group">
									<label class="col-md-4 control-label"></label>
									<div class="col-md-8">
										<div class="checkbox-list">
											<label class="checkbox-inline">
												<div class="checker" id="uniform-inlineCheckbox21">
													<span class="">
															<?php  echo $this->Form->input('rejected', array("type"=>"checkbox",'div'=>false,'label' => false,)); ?>
												</div> REJECT THIS REQUEST 
											</label>
																					
										</div>
									</div>
								</div>
								<div class="form-group" id="RejectDiv" style="display:none;">
                                    <label class="col-md-4 control-label">Reason For Rejection:</label>
                                    <div class="col-md-6">
                                        <?php
                                        echo $this->Form->input('rejection_reason', array('label' => false,
                                            'required' => false,
                                            'type'=>'textarea',
                                            'errorMessage' => false,
                                            'class' => 'form-control'));
                                        if ($this->Form->isFieldError('rejection_reason')) {
                                            echo $this->Form->error('rejection_reason', array('div' => false, 'attributes' => array('wrap' => 'span', 'class' => 'model-error')));
                                        }
                                        ?>
                                    </div>

                                </div>
                                <div class="form-group">

                                    <div class="col-md-offset-4 col-md-9">
                                        <button type="submit" class="btn green">Update Details</button>
                                    </div>
                                </div>
                            </div>
						

                            <?php echo $this->Form->end(); ?>
                            <!-- END FORM-->
                        </div>
   



                    </div>
                </div>
                <!-- END Portlet PORTLET-->
            </div>

        </div>
    </div>
    <!-- END PAGE CONTENT-->
</div>
<script>
 $( document ).ready(function() {
	$('#ChangeVehicleDetailRejected').change(function(){
		if($("#ChangeVehicleDetailRejected").is(':checked')){
			$('#RejectDiv').show();
			$('#ChangeVehicleDetailRejectionReason').attr('required','required');
		}else{
			$('#ChangeVehicleDetailRejectionReason').removeAttr( "required" );
			$('#RejectDiv').hide(); 
		}
	});
});
</script>

