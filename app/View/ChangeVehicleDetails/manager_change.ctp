
<div class="page-content-wrapper">
    <div class="page-content" style="min-height:1027px">

        <!-- BEGIN PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">

                    Vehicles <small>Change Vehicle Details</small>
                </h3>
                <ul class="page-breadcrumb breadcrumb">
                   
                    <li>
                        <i class="fa fa-home"></i>
                        <?php echo $this->Html->link('Home',array('controller'=>'users','action'=>'manager_home_page')); ?>
                       
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="#">Change Vehicle Details</a>
                    </li>
                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <!-- END PAGE HEADER-->
        <div id="flashMessages">
            <h2><?php echo $this->Session->flash(); ?></h2>
        </div>
        <!-- BEGIN PAGE CONTENT-->
        <div class="row">
            <div class="col-md-12">

                <div class="col-md-6"> 
                    <!-- BEGIN Portlet PORTLET-->
                    <div class="portlet box green-meadow">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-gift"></i>Current Vehicle Details
                            </div>
                            <div class="tools">
                                
                            </div>
                        </div>
                        <div class="portlet-body"> 

                            <?php echo $this->Form->create('ChangeVehicleDetail', array('class' => 'form-horizontal', 'novalidate' => 'novalidate')) ?>


                            <div class="form-body">

                                <div class="form-group">
                                    <label class="col-md-4 control-label">Vehicle Name :</label>
                                    <div class="col-md-6">
                                        <?php
                                        echo $this->Form->input('vehiffcle_name', array('label' => false,
                                            'required' => true,
                                            'errorMessage' => false,
                                            'value' => $changeDetail['Vehicle']['owner'],
                                            'disabled' => TRUE,
                                            'class' => 'form-control'));
                                        
                                        ?>
                                    </div>

                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Make :</label>
                                    <div class="col-md-6">


                                        <?php
                                        echo $this->Form->input('maffke', array('label' => false,
                                            'required' => true,
                                            'errorMessage' => false,
                                            'value' => $changeDetail['Vehicle']['make'],
                                            'disabled' => TRUE,
                                            'class' => 'form-control'));
                                      
                                        ?>

                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Model :</label>
                                    <div class="col-md-6">
                                        <?php
                                        echo $this->Form->input('mofdel', array('label' => false,
                                            'errorMessage' => false,
                                            'value' => $changeDetail['Vehicle']['model'],
                                            'disabled' => TRUE,
                                            'class' => 'form-control'));
                                       
                                        ?>
                                    </div>

                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Color :</label>
                                    <div class="col-md-6">
                                        <?php
                                        echo $this->Form->input('colfffor', array('label' => false,
                                            'required' => true,
                                            'errorMessage' => false,
                                            'value' => $changeDetail['Vehicle']['color'],
                                            'disabled' => TRUE,
                                            'class' => 'form-control'));
                                       
                                        ?>
                                    </div>

                                </div>

                                <div class="form-group">
                                    <label class="col-md-4 control-label">Plate Number :</label>
                                    <div class="col-md-6">
                                        <?php
                                        echo $this->Form->input('platdfe_number', array('label' => false,
                                            'required' => true,
                                            'errorMessage' => false,
                                            'value' => $changeDetail['Vehicle']['license_plate_number'],
                                            'disabled' => TRUE,
                                            'class' => 'form-control'));
                                      
                                        ?>
                                    </div>

                                </div>

                                <div class="form-group">
                                    <label class="col-md-4 control-label">State :</label>
                                    <div class="col-md-6">
                                         <?php
                                        echo $this->Form->input('statedsf', array('type' => 'select',
                                            'options' => $states,
                                            'empty' => 'Select One',
                                            'selected'=>$changeDetail['Vehicle']['license_plate_state'],
                                            'label' => false,
                                            'disabled' => TRUE,
                                            'class' => 'form-control'));
                                   
                                        ?>
                                    </div>

                                </div>


                                <div class="form-group">
                                    <label class="col-md-4 control-label">Vin :</label>
                                    <div class="col-md-6">
                                        <?php
                                        echo $this->Form->input('vinass', array('label' => false,
                                            'required' => true,
                                            'errorMessage' => false,
                                            'value' => $changeDetail['Vehicle']['last_4_digital_of_vin'],
                                            'disabled' => TRUE,
                                            'class' => 'form-control'));
                                        
                                        ?>
                                    </div>

                                </div>
                                <div class="form-group">

                                    <div class="col-md-6">
                                        <?php
                                        echo $this->Form->hidden('vi66n', array('label' => false,
                                        ));
                                       
                                        ?>
                                    </div>

                                </div>
                                <div class="form-group">

                                    <div class="col-md-6">
                                        <?php
                                        echo $this->Form->hidden('vinr56', array('label' => false,
                                        ));
                                       
                                        ?>
                                    </div>

                                </div>
                                <div class="form-group">

                                    <div class="col-md-6">
                                        <?php
                                        echo $this->Form->hidden('vincgh', array('label' => false,
                                        ));
                                        
                                        ?>
                                    </div>

                                </div>


                            </div>


                            <?php echo $this->Form->end(); ?>
                            <!-- END FORM-->
                        </div>   </div>
                    <!-- END Portlet PORTLET-->
                </div>

                <div class="col-md-6">
                    <!-- BEGIN Portlet PORTLET-->
                    <div class="portlet box yellow">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-gift"></i>Submitted Details
                            </div>
                            <div class="tools">

                            </div>
                        </div>
                        <div class="portlet-body">

                            <?php echo $this->Form->create('ChangeVehicleDetail', array('url'=>array('action'=>'manager_change/'.$changeDetail['ChangeVehicleDetail']['id']),'class' => 'form-horizontal', 'novalidate' => 'novalidate')) ?>


                            <div class="form-body">
                                
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Reason :</label>
                                    <div class="col-md-6">
                                        <?php
                                        echo $this->Form->input('reason', array('label' => false,
                                            'required' => true,
                                            'type'=>'textarea',
                                            'errorMessage' => false,
                                            'value' =>   $changeDetail['ChangeVehicleDetail']['reason'],
                                            'disabled'=>true,
                                            'class' => 'form-control'));
                                        if ($this->Form->isFieldError('reason')) {
                                            echo $this->Form->error('reason', array('div' => false, 'attributes' => array('wrap' => 'span', 'class' => 'model-error')));
                                        }
                                        ?>
                                    </div>

                                </div>

                                <div class="form-group">
                                    <label class="col-md-4 control-label">Vehicle Name :</label>
                                    <div class="col-md-6">
                                        <?php
                                        echo $this->Form->input('vehicle_name', array('label' => false,
                                            'required' => true,
                                            'errorMessage' => false,
                                            'value' => $changeDetail['ChangeVehicleDetail']['vehicle_name'],
                                            'class' => 'form-control'));
                                        if ($this->Form->isFieldError('vehicle_name')) {
                                            echo $this->Form->error('vehicle_name', array('div' => false, 'attributes' => array('wrap' => 'span', 'class' => 'model-error')));
                                        }
                                        ?>
                                    </div>

                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Make :</label>
                                    <div class="col-md-6">

                                        <?php
                                        echo $this->Form->input('make', array('label' => false,
                                            'required' => true,
                                            'errorMessage' => false,
                                            'value' => $changeDetail['ChangeVehicleDetail']['make'],
                                            'class' => 'form-control'));
                                        if ($this->Form->isFieldError('make')) {
                                            echo $this->Form->error('make', array('div' => false, 'attributes' => array('wrap' => 'span', 'class' => 'model-error')));
                                        }
                                        ?>

                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Model :</label>
                                    <div class="col-md-6">
                                        <?php
                                        echo $this->Form->input('model', array('label' => false,
                                            'errorMessage' => false,
                                            'value' => $changeDetail['ChangeVehicleDetail']['model'],
                                            'class' => 'form-control'));
                                        if ($this->Form->isFieldError('model')) {
                                            echo $this->Form->error('model', array('div' => false, 'attributes' => array('wrap' => 'span', 'class' => 'model-error')));
                                        }
                                        ?>
                                    </div>

                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Color :</label>
                                    <div class="col-md-6">
                                        <?php
                                        echo $this->Form->input('color', array('label' => false,
                                            'required' => true,
                                            'errorMessage' => false,
                                            'value' => $changeDetail['ChangeVehicleDetail']['color'],
                                            'class' => 'form-control'));
                                        if ($this->Form->isFieldError('color')) {
                                            echo $this->Form->error('color', array('div' => false, 'attributes' => array('wrap' => 'span', 'class' => 'model-error')));
                                        }
                                        ?>
                                    </div>

                                </div>

                                <div class="form-group">
                                    <label class="col-md-4 control-label">Plate Number :</label>
                                    <div class="col-md-6">
                                        <?php
                                        echo $this->Form->input('plate_number', array('label' => false,
                                            'required' => true,
                                            'errorMessage' => false,
                                            'value' => $changeDetail['ChangeVehicleDetail']['plate_number'],
                                            'class' => 'form-control'));
                                        if ($this->Form->isFieldError('plate_number')) {
                                            echo $this->Form->error('plate_number', array('div' => false, 'attributes' => array('wrap' => 'span', 'class' => 'model-error')));
                                        }
                                        ?>
                                    </div>

                                </div>

                                <div class="form-group">
                                    <label class="col-md-4 control-label">State :</label>
                                    <div class="col-md-6">
                                        <?php
                                        echo $this->Form->input('state', array('type' => 'select',
                                            'options' => $states,
                                            'empty' => 'Select One',
                                            'selected'=>$changeDetail['ChangeVehicleDetail']['state'],
                                            'label' => false,
                                            
                                            'class' => 'form-control'));
                                        if ($this->Form->isFieldError('state')) {
                                            echo $this->Form->error('state', array('div' => false, 'attributes' => array('wrap' => 'span', 'class' => 'model-error')));
                                        }
                                        ?>

                                    </div>

                                </div>


                                <div class="form-group">
                                    <label class="col-md-4 control-label">Vin :</label>
                                    <div class="col-md-6">
                                        <?php
                                        echo $this->Form->input('vin', array('label' => false,
                                            'required' => true,
                                            'errorMessage' => false,
                                            'value' => $changeDetail['ChangeVehicleDetail']['vin'],
                                            'class' => 'form-control'));
                                        if ($this->Form->isFieldError('vin')) {
                                            echo $this->Form->error('vin', array('div' => false, 'attributes' => array('wrap' => 'span', 'class' => 'model-error')));
                                        }
                                        ?>
                                    </div>

                                </div>


                                <div class="form-group">

                                    <div class="col-md-offset-4 col-md-9">
                                        <button type="submit" class="btn green">Update Details</button>
                                    </div>
                                </div>
                            </div>


                            <?php echo $this->Form->end(); ?>
                            <!-- END FORM-->
                        </div>




                    </div>
                </div>
                <!-- END Portlet PORTLET-->
            </div>

        </div>
    </div>
    <!-- END PAGE CONTENT-->
</div>


