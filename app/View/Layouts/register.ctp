<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" >
	<!--<![endif]-->
	<!-- BEGIN HEAD -->
	<head>
	<meta charset="utf-8"/>
	<title>My Parking Pass | Powered by Digital6</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
	<meta content="" name="description"/>
	<meta content="" name="author"/>
	<!-- BEGIN GLOBAL MANDATORY STYLES -->
	<link href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
	<?php echo $this->Html->charset();
	  echo $this->Html->css('/assets/global/plugins/bootstrap/css/bootstrap.min.css');
	  echo $this->Html->script('/assets/global/plugins/jquery-1.11.0.min.js'); 
	  echo $this->Html->script('/assets/global/plugins/bootstrap/js/bootstrap.min.js');
	  ?>
	<body>
		
		   <?php 
					if(isset($property)){
						echo $this->Html->link($this->Html->image('logo/'.$property['logo'].'',array('height'=>'121','width'=>'300')),'',array('escape'=>false)); 
					}else{
						echo $this->Html->link($this->Html->image('onlineparking-logo.png',array('height'=>'121','width'=>'300')),'',array('escape'=>false)); 
					}
			?>
			
		<?php echo $this->fetch('content'); ?>
		
		
		<?php echo date('Y'); ?> &copy; <a href="http://digital6technologies.com" target="_blank" style="color:black;">Powered by Digital6 Technologies</a>
		
	</body>
<!-- END BODY -->
</html>
