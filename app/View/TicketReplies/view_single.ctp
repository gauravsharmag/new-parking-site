<script>
    $(document).ready(function ()
    {
        $('#message-badge').hide();
        $('#submitB').click(function () {

            $('#submitB').attr("disabled", true);
            $('#TicketReplyAddForm').submit();
        });

        $('#chatbox').scrollTop($('#chatbox').prop('scrollHeight'));
        $('#checkbox').change(function () {
            if ($(this).prop('checked')) {

                $("#but1").hide(500);
                $("#but2").show(500);
            } else {
                $("#but2").hide(500);
                $("#but1").show(500);
            }
        });
    });
</script>
<div class="page-content-wrapper">
    <div class="page-content">

        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                    <?php echo CakeSession::read('PropertyName'); ?><small> View Messages</small>
                </h3>
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <?php echo $this->Html->link('Home', array('controller' => 'users', 'action' => 'customerHomePage')); ?>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="">View Messages</a>
                    </li>
                    <li class="pull-right">
                        <div id="dashboard-report-range" class="dashboard-date-range tooltips" data-placement="top" data-original-title="Change dashboard date range">
                            <i class="icon-calendar"></i>
                            <span></span>
                            <i class="fa fa-angle-down"></i>
                        </div>
                    </li>
                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <div id="flashMessages">
            <h2><?php echo $this->Session->flash(); ?></h2>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-gift"></i>Subject: <?php echo $ticket['Ticket']['subject']; ?>
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse">
                            </a>
                        </div>
                    </div>

                    <div class="portlet-body form" id="chats123">
                        <!-- BEGIN FORM-->
                        <?php if ($ticket['Ticket']['status'] == 0) { ?>
                            <div id="chatbox" style="width:100%; height: 300px; overflow-y: scroll;">
                            <?php } else { ?>
                                <div id="chatbox" style="width:100%; height: 400px; overflow-y: scroll;">
                                <?php } ?>

                                <ul class="chats">
                                    <?php if ($ticket['Ticket']['user_id'] == AuthComponent::user('id')) {
                                        ?>
                                        <li class="out">
                                            <div class="avatar" >
                                                <img class="avatar" alt="" src="/assets/admin/layout/img/avatar3.jpg">  </div>
                                            <?php
                                        } else {
                                            ?>
                                        <li class="in">
                                            <div class="avatar" >
                                                <img class="avatar" alt="" src="/assets/admin/layout/img/avatar2.jpg">  </div>
                                        <?php }
                                        ?>


                                        <div class="message">
                                            <span class="arrow">
                                            </span> 
                                            <?php
                                            if ($ticket['Ticket']['user_id'] == AuthComponent::user('id')) {
                                                echo $ticket['User']['username'];
                                            } else {
                                                echo "Admin:";
                                            }
                                            ?>
                                            <span class="datetime">
                                                at <?php echo $this->Time->niceShort($ticket['Ticket']['created']); ?> </span>
                                            <span class="body">
                                                <?php echo $ticket['Ticket']['message']; ?>	</span>						
                                        </div>
                                    </li>

                                    <?php
//debug($ticketReply); die();
                                    foreach ($ticketReply as $ticketRep) {
                                        if (AuthComponent::user('id') == $ticketRep['TicketReply']['user_id']) {
                                            ?>
                                            <li class="out">
                                                <div class="avatar" >
                                                    <img class="avatar" alt="" src="/assets/admin/layout/img/avatar3.jpg">  </div>
                                                <?php
                                            } else {
                                                ?>
                                            <li class="in">
                                                <div class="avatar" >
                                                    <img class="avatar" alt="" src="/assets/admin/layout/img/avatar2.jpg">  </div>
                                            <?php }
                                            ?>


                                            <div class="message">
                                                <span class="arrow">
                                                </span>

                                                <?php
                                                if (AuthComponent::user('id') != $ticketRep['TicketReply']['user_id']) {
                                                    echo "Admin:";
                                                } else {
                                                    echo "You";
                                                }
                                                ?>
                                                <span class="datetime">
                                                    at <?php echo $this->Time->niceShort($ticketRep['TicketReply']['created']); ?> </span>
                                                <span class="body">
    <?php echo $ticketRep['TicketReply']['reply_body']; ?>	</span>       
                                            </div>
                                        </li>

<?php } //die;
?>


                                </ul>
                            </div>
<?php if ($ticket['Ticket']['status'] == 0) { ?>
                                <?php echo $this->Form->create('TicketReply', array('url'=>array('action' => 'add'), 'class' => 'chat-form')) ?>

                                <div class="row">
                                    <div class="input-cont">
    <?php
    echo $this->Form->input('user_id', array('label' => false,
        'errorMessage' => false,
        'value' => AuthComponent::user('id'), 'type' => 'hidden',
        'class' => 'form-control'));
    ?>
                                        <?php
                                        echo $this->Form->input('ticket_id', array('label' => false,
                                            'errorMessage' => false,
                                            'value' => $ticket['Ticket']['id'], 'type' => 'hidden',
                                            'class' => 'form-control'));
                                        ?>

                                        <?php
                                        echo $this->Form->input('reply_body', array('label' => false,
                                            'errorMessage' => false,
                                            'type' => 'textarea',
                                            'rows' => 2,
                                            'placeholder' => 'Type a message here...',
                                            'class' => 'ckeditor'));
                                        if ($this->Form->isFieldError('reply_body')) {
                                            echo $this->Form->error('reply_body', array('div' => false, 'attributes' => array('wrap' => 'span', 'class' => 'model-error')));
                                        }
                                        ?>
                                    </div>
                                </div>
                                <div class="row">
									 <div class=" col-md-2">
										<?php
											echo $this->Form->input('status', array(
												'type'=>'checkbox',
												'div'=>false,
												'label' => 'Tick to close the ticket',
												//'class' => 'form-control pull-left',
												'id' => 'checkbox'
												));
                                        ?>
                                    </div>
                                    <div class=" col-md-4">
                                        <button type="submit"  class="btn green" id="submitB"  >
                                            <p id="but1">Send Message</p>
                                            <p id="but2" style="display:none" id="submitB">Send and close ticket</p>
                                        </button>
                                    </div>

                                </div>						
    <?php echo $this->Form->end(); ?>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
