<div class="page-header navbar navbar-fixed-top">
	 <!-- BEGIN HEADER INNER -->
	 <div class="page-header-inner">
		   <!-- BEGIN LOGO -->
			<div class="page-logo">
				<?php
				if (CakeSession::check('PropertyLogo')) {
					echo $this->Html->link($this->Html->image('logo/' . CakeSession::read('PropertyLogo') . '', array('height' => '45', 'width' => '130')), '', array('escape' => false));
				} else {
					if (isset($this->params['Property'])) {
						echo $this->Html->link($this->Html->image('logo/' . $this->params['Property']['logo'] . '', array('height' => '45', 'width' => '130')), '', array('escape' => false));
					} else {
						echo $this->Html->link($this->Html->image('onlineparking-logo.png', array('height' => '45', 'width' => '130')), '', array('escape' => false));
					}
				}
				?>
				<div class="menu-toggler sidebar-toggler hide">
					<!-- DOC: Remove the above "hide" to enable the sidebar toggler button on header -->
				</div>
			</div>
        <!-- END LOGO -->
        <!-- BEGIN RESPONSIVE MENU TOGGLER -->
        <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
        </a>
        <!-- END RESPONSIVE MENU TOGGLER -->
        <div class="top-menu">
			 <ul class="nav navbar-nav pull-right">
				 <li> 
                    <div id="google_translate_element"></div>
                    <script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
                    <script type="text/javascript">
						function googleTranslateElementInit() {  new google.translate.TranslateElement({pageLanguage: 'en', includedLanguages: 'es', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
						}   
                    </script>
                </li>
                 
                 <li>
                    <?php
                    if (AuthComponent::user('role_id') == 2) { 
                        if (CakeSession::read('PropertySelection')) {
                            echo $this->Form->create('PropertyUser', array('url'=>array('controller' => 'PropertyUsers', 'action' => 'pa_reset_property'),'class' => 'form-horizontal'));
                            ?>
                            <div class="row">       
                                <div class="col-md-12">
                                    <label class="col-md-5 control-label">Select Property</label>
                                    <div class="col-md-5"> <?php
                                        echo $this->Form->hidden('page_url', array('default' => $_SERVER['REQUEST_URI']));
                                        echo $this->Form->input('property_user_id', array('label' => false,
                                            'options' => CakeSession::read('AdminProperty'),
                                            'selected' => CakeSession::read('CurrentProperty'),
                                            'class' => 'form-control'));
                                        ?>
                                    </div>  
                                    <div class="col-md-2">
                                        <button type="submit" class="btn blue">Go</button>
                                    </div>
                                    <?php echo $this->Form->end(); ?>
                                </div>
                            </div>

                            <?php
                        }
                    }
                    ?>
                </li>
                	<li class="dropdown dropdown-user">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
						<span class="username"> Welcome 
							<?php
							if (AuthComponent::user('first_name') != null) {
								echo AuthComponent::user('first_name');
							} else {
								echo AuthComponent::user('username');
							}
							?> </span>
						<i class="fa fa-angle-down"></i> 
					</a>
					<ul class="dropdown-menu">
						<li>
							<?php echo $this->Html->link('<i class="fa fa-user" aria-hidden="true"></i> My Profile', array('controller' => 'users', 'action' => 'myAccount'), array('escape' => false)); ?>
						</li>
						 <li>
						    <?php if (AuthComponent::user('role_id') == 1) { ?>
						        <a href="http://support.digital6technologies.com/" target="blank"><i class="fa fa-ticket" aria-hidden="true"></i> Submit Support Ticket</a> 
						    <?php } ?>
						</li>
						<li>
							<?php
								$authComp = $this->Session->read('Auth');
								if ($authComp['User']['role_id'] == 1 || $authComp['User']['role_id'] == 2) {

									echo $this->Html->link('<i class="fa fa-sign-out" aria-hidden="true"></i> Log Out', array('controller' => 'users', 'action' => 'admin_logout'), array('escape' => false));
								} else {
									echo $this->Html->link('<i class="fa fa-sign-out" aria-hidden="true"></i> Log Out', array('controller' => 'users', 'action' => 'logout'), array('escape' => false));
								}
							?>
						</li>
					</ul>
				</li>
			 </ul>
		</div>
	 </div>
	 <!-- END HEADER INNER -->
</div>
