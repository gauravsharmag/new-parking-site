<div class="page-sidebar-wrapper">
		<!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
		<!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
		<div class="page-sidebar navbar-collapse collapse">
			<!-- BEGIN SIDEBAR MENU -->
			<ul class="page-sidebar-menu" data-auto-scroll="true" data-slide-speed="200">
				<!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
				<li class="sidebar-toggler-wrapper">
					<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
					<div class="sidebar-toggler">
					</div>
					<!-- END SIDEBAR TOGGLER BUTTON -->
				</li>
				<!-- DOC: To remove the search box from the sidebar you just need to completely remove the below "sidebar-search-wrapper" LI element -->
				<li class="sidebar-search-wrapper hidden-xs">
					<!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
					<!-- DOC: Apply "sidebar-search-bordered" class the below search form to have bordered search box -->
					<!-- DOC: Apply "sidebar-search-bordered sidebar-search-solid" class the below search form to have bordered & solid search box -->
				<?php /*?>	<form class="sidebar-search" action="extra_search.html" method="POST">
						<a href="javascript:;" class="remove">
						<i class="icon-close"></i>
						</a>
						<div class="input-group">
							<input type="text" class="form-control" placeholder="Search...">
							<span class="input-group-btn">
							<a href="javascript:;" class="btn submit"><i class="icon-magnifier"></i></a>
							</span>
						</div>
					</form><?php */?>
					<!-- END RESPONSIVE QUICK SEARCH FORM -->
				</li>
				<li class="start active ">
             
				  <?php
						if(CakeSession::read('Allowed')==true){ 
							echo $this->Html->link('<i class="icon-home"></i><span class="title">Dashboard</span><span class="selected"></span>',array('controller'=>'Vehicles','action'=>'add_vehicles'),array('escape'=>false)); 

						}	?>
				</li>
				
				<li>

					<a href="javascript:;">
					<i class="icon-user"></i>
					<span class="title">User Profile</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub-menu">
						<li>
                            <?php 
									echo $this->Html->link('<i class="icon-users"></i>View Profile',array('controller'=>'users','action'=>'myAccount'),array('escape'=>false)); 
								?>
                        </li>
					</ul>
				</li>
				<?php if($this->Session->read('Allowed')==false){?>
				<!--<li>
					<a href="javascript:;">
					<i class="fa fa-sitemap"></i>
					<span class="title">Passes</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub-menu">
						<li>
							<?php
							//echo $this->Html->link('<i class="fa fa-plus-square-o"></i>Buy Passes',array('controller'=>'Transactions','action'=>'compulsoryPasses'),array('escape'=>false)); ?>
						</li>

					</ul>
				</li>-->
				<?php }?>
				<?php if($this->Session->read('Allowed')){?>
				<!--<li>
					<a href="javascript:;">
					<i class="fa fa-sitemap"></i>
					<span class="title">Packages</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub-menu">
						<li>-->
							<?php
							//echo $this->Html->link('<i class="fa fa-plus-square-o"></i>View Packages',array('controller'=>'Packages','action'=>'index'),array('escape'=>false)); ?>
						<!--</li>

					</ul>
				</li>-->
				<!-- BEGIN FRONTEND THEME LINKS -->
				<li>
					<a href="javascript:;">
					<i class="fa  fa-building "></i>
					<span class="title">
					Passes </span>
					<span class="arrow">
					</span>
					</a>
					<ul class="sub-menu">
						<li>
							<?php
							echo $this->Html->link('<i class="fa fa-plus-square-o"></i>View Passes',array('controller'=>'CustomerPasses','action'=>'view'),array('escape'=>false)); ?>
						</li>
                        <li>
							<?php
							//echo $this->Html->link('<i class="fa fa-plus-square-o"></i>Buy new Pass',array('controller'=>'','action'=>''),array('escape'=>false)); ?>
						</li>

					</ul>
				</li>
				<!-- END FRONTEND THEME LINKS -->
				<li>
					<a href="javascript:;">
					<i class="fa fa-cab"></i>
					<span class="title">Vehicles</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub-menu">

						<li>
							 <?php
							echo $this->Html->link('<i class="fa fa-plus-square-o"></i>My Vehicles',array('controller'=>'Vehicles','action'=>'index'),array('escape'=>false)); ?>
						</li>
						<li>
							<?php
							//echo $this->Html->link('<i class="fa fa-plus-square-o"></i>Guest Vehicles',array('controller'=>'','action'=>''),array('escape'=>false)); ?>
						</li>
					</ul>
				</li>
				<li>
					<a href="javascript:;">
					<i class="fa  fa-building"></i>
					<span class="title">Guest Pass</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub-menu">

						<li>
							 <?php
							echo $this->Html->link('<i class="fa fa-plus-square-o"></i>Guest Passes',array('controller'=>'CustomerPasses','action'=>'guestPass'),array('escape'=>false)); ?>
						</li>
					</ul>
				</li>
				<li>
					<a href="javascript:;">
					<i class="fa  fa-building"></i>
					<span class="title">Contact Us</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub-menu">

						<li>
							 <?php
							echo $this->Html->link('<i class="fa fa-plus-square-o"></i>Contact Us',array('controller'=>'tickets','action'=>'contact_us'),array('escape'=>false)); ?>
						</li>
						<li>
							 <?php
							echo $this->Html->link('<i class="fa fa-plus-square-o"></i>My Queries',array('controller'=>'tickets','action'=>'view'),array('escape'=>false)); ?>
						</li>
					</ul>
				</li>
				<li>
					<a href="/ChangeVehicleDetails/all_requests">
					<i class="fa fa-cab"></i>
					<span class="title">Change Vehicle Requests</span>
					</a>
				</li>
				
               <?php }?>			
			</ul>
			<!-- END SIDEBAR MENU -->
		</div>
	</div>
