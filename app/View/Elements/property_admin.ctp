<div class="page-sidebar-wrapper">
		<!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
		<!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
		<div class="page-sidebar navbar-collapse collapse">
			<!-- BEGIN SIDEBAR MENU -->
			<ul class="page-sidebar-menu" data-auto-scroll="true" data-slide-speed="200">
				<!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
				<li class="sidebar-toggler-wrapper">
					<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
					<div class="sidebar-toggler">
					</div>
					<!-- END SIDEBAR TOGGLER BUTTON -->
				</li>
				<!-- DOC: To remove the search box from the sidebar you just need to completely remove the below "sidebar-search-wrapper" LI element -->
				<li class="sidebar-search-wrapper hidden-xs">
					<!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
					<!-- DOC: Apply "sidebar-search-bordered" class the below search form to have bordered search box -->
					<!-- DOC: Apply "sidebar-search-bordered sidebar-search-solid" class the below search form to have bordered & solid search box -->
				<?php echo $this->Form->create('Vehicle',array('url'=>array('controller'=>'vehicles','action'=>'pa_search'),'class'=>'sidebar-search'))?>
						<a href="javascript:;" class="remove">
						<i class="icon-close"></i>
						</a>
						<div class="input-group">
						<?php echo $this->Form->input('search_key',array('placeholder'=>'Search...','div'=>false,'class'=>'form-control','label'=>false));?>
							<!--<input type="text" class="form-control" placeholder="Search...">-->
							<span class="input-group-btn">
							<button class="btn submit yellow"><i class="icon-magnifier"></i></button>
							</span>
						</div>
					<?php echo $this->Form->end();?>
					<!-- END RESPONSIVE QUICK SEARCH FORM -->
				</li>
				<li class="start active ">
                <?php 
				echo $this->Html->link('<i class="icon-home"></i><span class="title">Dashboard</span><span class="selected"></span>',array('controller'=>'users','action'=>'pa_home_page'),array('escape'=>false)); ?>
					
				</li>
				<li>
					<a href="javascript:;">
					<i class="fa  fa-building "></i>
					<span class="title">
					Property </span>
					<span class="arrow">
					</span>
					</a>
					<ul class="sub-menu">
						<li>
							 <?php 
							echo $this->Html->link('<i class="fa fa-building-o"></i>Property Details',array('controller'=>'properties','action'=>'pa_property_view'),array('escape'=>false)); ?>
						</li>
					
					</ul>
				</li>
				<li>
					<a href="javascript:;">
					<i class="fa  fa-building "></i>
					<span class="title">
					Users </span>
					<span class="arrow">
					</span>
					</a>
					<ul class="sub-menu">
						<li>
							 <?php 
							echo $this->Html->link('<i class="icon-users"></i>List Users',array('controller'=>'users','action'=>'pa_user_details'),array('escape'=>false)); ?>
						</li>
					
					</ul>
				</li>
				<!--
				<li>
					<a href="javascript:;">
					<i class="fa  fa-building "></i>
					<span class="title">
					Messages </span>
					<span class="arrow">
					</span>
					</a>
					<ul class="sub-menu">
						<li>
							 <?php 
							//echo $this->Html->link('<i class="icon-users"></i>View All Messages',array('controller'=>'tickets','action'=>'view'),array('escape'=>false)); ?>
						</li>
                                                <li>
							 <?php 
							//echo $this->Html->link('<i class="icon-users"></i>Contact Super Admin',array('controller'=>'tickets','action'=>'contact_super_admin'),array('escape'=>false)); ?>
						</li>
					
					</ul>
				</li>-->
				
				
				<li>
					<a href="javascript:;">
					<i class="fa fa-cab"></i>
					<span class="title">Vehicle</span>
					<span class="arrow "></span>
					</a> 
					<ul class="sub-menu">
                  		<li>
							 <?php 
							echo $this->Html->link('<i class="fa fa-taxi"></i>List Vehicles',array('controller'=>'Vehicles','action'=>'all_vehicles'),array('escape'=>false)); ?>
						</li>
						<li>
							 <?php 
							echo $this->Html->link('<i class="fa fa-taxi"></i>Active Vehicles',array('controller'=>'CustomerPasses','action'=>'active_vehicles'),array('escape'=>false)); ?>
						</li>
						<li>
							 <?php 
							echo $this->Html->link('<i class="fa fa-taxi"></i>Inactive Vehicles',array('controller'=>'CustomerPasses','action'=>'inactive_vehicles'),array('escape'=>false)); ?>
						</li>
						<li>
							 <?php 
							echo $this->Html->link('<i class="fa fa-taxi"></i>Guest Vehicles',array('controller'=>'CustomerPasses','action'=>'guest_vehicles'),array('escape'=>false)); ?>
						</li>
						<li>
							 <?php 
							echo $this->Html->link('<i class="fa fa-taxi"></i>Active Guest Vehicles',array('controller'=>'CustomerPasses','action'=>'active_guest_vehicles'),array('escape'=>false)); ?>
						</li>
						<li>
							 <?php 
							echo $this->Html->link('<i class="fa fa-taxi"></i>Other Vehicles',array('controller'=>'Vehicles','action'=>'other_vehicles'),array('escape'=>false)); ?>
						</li>								
					</ul>
				</li>
				<!--<li>
					<a href="javascript:;">
					<i class="fa fa-credit-card"></i>
					<span class="title">Passes Creation Details</span>
					<span class="arrow "></span>
					</a> 
					<ul class="sub-menu">
                  		<li>
							 <?php 
							echo $this->Html->link('<i class="fa fa-credit-card"></i>View Created Trends',array('controller'=>'CustomerPasses','action'=>'admin_view_passes_created'),array('escape'=>false)); ?>
						</li>
														
					</ul>
				</li>-->
				<li>
					<a href="javascript:;">
					<i class="fa fa-credit-card"></i>
					<span class="title">Passes Expiration Details</span>
					<span class="arrow "></span>
					</a> 
					<ul class="sub-menu">
                  		<li>
							 <?php 
							echo $this->Html->link('<i class="fa fa-credit-card"></i>Expiring Today',array('controller'=>'CustomerPasses','action'=>'admin_view_expirations_today'),array('escape'=>false)); ?>
						</li>
						<li>
							 <?php 
							echo $this->Html->link('<i class="fa fa-credit-card"></i>Expiring In Next 30 Days',array('controller'=>'CustomerPasses','action'=>'admin_view_expirations_next'),array('escape'=>false)); ?>
						</li>
						<li>
							 <?php 
							echo $this->Html->link('<i class="fa fa-credit-card"></i>Expired In Last 30 Days',array('controller'=>'CustomerPasses','action'=>'admin_view_expirations_last'),array('escape'=>false)); ?>
						</li>
						
														
					</ul>
				</li>
				<li>
					<a href="javascript:;">
					<i class="fa fa-sitemap"></i>
					<span class="title">Reports</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub-menu"> 
						<li>  
							 <?php 
							 echo $this->Html->link('<i class="fa fa-sitemap"></i> Property Wide Vehicles',array('controller'=>'Vehicles','action'=>'list_vehicles_pa'),array('escape'=>false)); ?>
						</li>
						<li>
							 <?php 
							 echo $this->Html->link('<i class="fa fa-users"></i> Property Wide Users',array('controller'=>'Users','action'=>'property_wise_users_pa'),array('escape'=>false)); ?>
						</li>
						<li>
							 <?php 
								//echo $this->Html->link('<i class="fa fa-building"></i> Property Wide Passes',array('controller'=>'CustomerPasses','action'=>'property_wise_passes'),array('escape'=>false)); 
							?> 
						</li>
						<li>
							 <?php 
								 echo $this->Html->link('<i class="fa fa-building"></i>Pass Details',array('controller'=>'CustomerPasses','action'=>'property_wise_pass_details_pa'),array('escape'=>false)); 
							?>
						</li>
						<li>
							 <?php 
								 //echo $this->Html->link('<i class="fa fa-sitemap"></i>Pass Cost Details',array('controller'=>'Transactions','action'=>'pass_costs_pa'),array('escape'=>false)); 
							?>
						</li>
						<li>
							 <?php 
								//echo $this->Html->link('<i class="fa fa-sitemap"></i>Guest Pass Details',array('controller'=>'Transactions','action'=>'guest_pass_costs'),array('escape'=>false)); 
							?>
						</li>
						<li>
							 <?php 
								//echo $this->Html->link('<i class="fa fa-sitemap"></i>New Guest Pass Details (valid from 5th Dec, 2015)',array('controller'=>'Transactions','action'=>'new_guest_pass_costs'),array('escape'=>false)); 
							?>
						</li>
						<li>
							 <?php 
								//echo $this->Html->link('<i class="fa fa-building"></i> Property Wide Expiring Passes',array('controller'=>'CustomerPasses','action'=>'pass_expiry'),array('escape'=>false)); 
							?>
						</li>
						<li>
							<?php
							 echo $this->Html->link('<i class="fa fa-building"></i> ' . (__('Last Seen Reports')), array('controller' => 'CustomerPasses', 'action' => 'last_seen'), array('escape' => false));
							?>
						</li>
						<li>
							 <?php 
								//echo $this->Html->link('<i class="fa fa-sitemap"></i>Pass Cost Details (RENEWAL)',array('controller'=>'Transactions','action'=>'renewal_passes'),array('escape'=>false)); 
							?>
						</li> 
						<li>
							 <?php 
								echo $this->Html->link('<i class="fa fa-sitemap"></i>No Vehicle',array('controller'=>'CustomerPasses','action'=>'no_vehicle'),array('escape'=>false)); 
							?>
						</li> 
						<li>
							 <?php 
								echo $this->Html->link('<i class="fa fa-sitemap"></i>Download Csv',array('controller'=>'PropertyUsers','action'=>'create_csv',CakeSession::read('PropertyId'),'admin'=>true),array('escape'=>false,'target'=>'_blank;')); 
							?>
						</li> 
					</ul>
				</li>
				
				<!--<li>
					<a href="javascript:;">
					<i class="fa  fa-building"></i>
					<span class="title">View  All Queries</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub-menu">

						<li>
							 <?php
							echo $this->Html->link('<i class="fa fa-plus-square-o"></i>View  All Queries',array('controller'=>'tickets','action'=>'view'),array('escape'=>false)); ?>
						</li>
						
					</ul>
				</li>-->
				<!--<li>
					<a href="javascript:;">
					<i class="fa  fa-building"></i>
					<span class="title">Passes Status</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub-menu">
                                            <?php if(isset($passStatus)){ 
												foreach ($passStatus as $pStatus) { ?>
						<li>
							 <?php
							echo $this->Html->link('<i class="fa fa-plus-square-o"></i>'.$pStatus['Pass']['name'],array('controller'=>'transactions','action'=>'pass_status', $pStatus['Pass']['id']),array('escape'=>false)); ?>
						</li>
                                            <?php } }?>
					</ul>
				</li>-->
				
			</ul>
			<!-- END SIDEBAR MENU -->
		</div>
	</div>
