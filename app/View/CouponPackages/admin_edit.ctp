<?php echo $this->Html->css('chosen.css'); ?>
<?php echo $this->Html->script('chosen.jquery.js'); ?>
	<div class="page-content-wrapper">
		<div class="page-content" style="min-height:1027px">
			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					Coupons <small>Edit Coupon Package</small>
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li>
							<i class="fa fa-home"></i>
							<a href="#">Home</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#"></a>Edit Package</a>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
			<div id="flashMessages">
								<?php  echo $this->Session->flash();?>
			</div>
			
			<div class="row">
				<div class="col-md-12">
					<div class="portlet box green">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-gift"></i><?php echo __('Edit Package'); ?>
							</div>			
						</div>	
						<div class="portlet-body">
							<?php echo $this->Form->create('CouponPackage',array('class'=>'form-horizontal'));?>
								 
								<?php echo $this->Form->hidden('id');
                                      echo $this->Form->input('name',array('required'=>true,'div'=>array('class'=>'form-group'),'error'=>array('attributes'=>array('class'=>'model-error')),'label'=>array('class'=>'col-md-3 control-label','text'=>'Name'),'between'=>'<div class="col-md-4">','after'=>'</div>','class'=>'form-control','required'=>true));						 
									  echo $this->Form->input('property_id',array( 'selected'=>$selectedId,'type'    => 'select','options' => $property_list,'empty'=>'Choose One','required'=>false,'div'=>array('class'=>'form-group'),'error'=>array('attributes'=>array('class'=>'model-error')),'label'=>array('class'=>'col-md-3 control-label','text'=>'Property'),'between'=>'<div class="col-md-4">','after'=>'</div>','class'=>'form-control' ));
									  echo $this->Form->input('current_pass',array('options' => $passes_list,'selected'=>$selectedPassId ,'id'=>'current_pass','type'    => 'select','multiple' => true,'required'=>false,'div'=>array('class'=>'form-group'),'error'=>array('attributes'=>array('class'=>'model-error')),'label'=>array('class'=>'col-md-3 control-label','text'=>'Passes'),'between'=>'<div class="col-md-4">','after'=>'</div>','class'=>'form-control'));
									//  echo $this->Form->input('coupon_codes',array('required'=>true,'div'=>array('class'=>'form-group'),'error'=>array('attributes'=>array('class'=>'model-error')),'label'=>array('class'=>'col-md-3 control-label','text'=>'Coupon Codes'),'between'=>'<div class="col-md-4">','after'=>'</div>','class'=>'form-control','required'=>true));						 
								?>
                                 <div class="form-group">
												<div class="col-md-offset-3 col-md-9">
													<button type="submit" class="btn blue">Submit</button>
													<!--<button type="button" class="btn default">Cancel</button>-->
												</div>
											</div>	
								<?php echo $this->Form->end(); ?> 
								<br>
								<div class="note note-warning">
                                                						<p>
                                                							 Related Coupons
                                                						</p>
                                            </div>
                                  <div class="clearfix">								
										<a href="javascript:;" class="btn btn-primary" onclick="addNewCoupon()">New Coupons</a>
								</div>
								<br>
								 <div class="portlet box blue" style='display:none;' id="addNewCouponForm">
									<div class="portlet-title">
										<div class="caption">
											<i class="fa fa-gift"></i>Add New Coupon
										</div>
										<div class="tools">
											<a href="javascript:;" class="collapse" data-original-title="" title="">
											</a>
										</div>
									</div>
									<div class="portlet-body ">
										
											<?php echo $this->Form->create('CouponCode',array('url'=>array('controller'=>'CouponCodes','action'=>'add'),'class' => 'form-horizontal')); 
													echo $this->Form->hidden('coupon_package_id',array('value'=>$this->request->data['CouponPackage']['id'])); 
											?>
											<div class="form-group">
												<label for="CouponPackageName" class="col-md-3 control-label">Code</label>
													<div class="col-md-4">
														<input name="data[CouponCode][code]" required="required" class="form-control" maxlength="100" type="text" id="CouponCodeCode" placeholder='Enter Comma Seperated Codes'>
													</div>
											</div>
											<div class="form-actions" >
												<div class="row">
													<div class="col-md-offset-3 col-md-9">
														<button type="submit" class="btn btn-circle blue">Add</button>
													</div>
												</div>
											</div>
											<?php echo $this->Form->end(); ?>
										
									</div>
								</div>
								<table id='codeTable' class="table table-striped table-bordered table-hover">
								<thead>
								<tr>
									<th>
										 Coupon Code
									</th>
									<th>
										 Action
									</th>
								</tr>
								</thead>
								<tbody>
									<?php 
											for($i=0;$i<count($this->request->data['CouponCode']);$i++){
												echo "<tr>";
													
													echo '<td>';
														echo $this->request->data['CouponCode'][$i]['code'];
													echo '</td>';
													echo '<td>';
														echo $this->Form->postLink(__('Delete'), array('controller'=>'CouponCodes','action' => 'delete', $this->request->data['CouponCode'][$i]['id']), array(), __('Are you sure you want to delete this coupon ?'));
													echo '</td>';
												echo "</tr>";
												
											}
									
									?>
								</tbody>
								</table>
						</div>
					</div>
				</div>
					 
			</div>
		</div>
	</div>

<?php echo $this->Html->script('block.js'); 
	  //echo $this->Html->script('pgwmodal.min.js'); 
?>
<?php echo $this->Html->script('//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js'); ?>
<?php echo $this->Html->css('//cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css'); ?>
<script type="text/javascript">
jQuery(document).ready(function ($) { 
		$('#codeTable').dataTable();
		 //jQuery('#CouponPackageCurrentPass').empty();
		// $("#current_pass").chosen();
	    $('#CouponPackagePropertyId').change(function () {  
			$('#current_pass').empty();
			$.blockUI({ css: { 
						border: 'none', 
						padding: '15px', 
						backgroundColor: '#000', 
						'-webkit-border-radius': '10px', 
						'-moz-border-radius': '10px', 
						opacity: .5, 
						color: '#fff' 
			} }); 
			$.ajax({
                type: "GET",
                url: "/admin/Passes/get_passes_list/"+jQuery(this).val()+"",
                dataType: "HTML",
                success: function (response) {
					//jQuery('#current_pass').append("<option value=''>Select Pass</option>");
                    var arr=$.parseJSON(response);
                    jQuery.each(arr, function (i, text) {
                        jQuery('#current_pass').append(jQuery('<option></option>').val(i).html(text));
                    });
                    // $("#current_pass").chosen();
                    $.unblockUI();
                }
            }); 
        });
	
});
function addNewCoupon(){
		$('#addNewCouponForm').toggle('slow'); 
		
	}
</script>

