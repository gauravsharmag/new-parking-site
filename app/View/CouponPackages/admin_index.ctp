
	<div class="page-content-wrapper">
		<div class="page-content" style="min-height:1027px">
			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					Coupons <small>All Coupons</small>
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li>
							<i class="fa fa-home"></i>
							<a href="#">Home</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#"></a>Coupons</a>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
			<div id="flashMessages">
								<?php  echo $this->Session->flash();?>
			</div>
			
			<div class="row">
				<div class="col-md-12">
					 <div class="clearfix">								
										<a href="/admin/CouponPackages/add" class="btn btn-primary" onclick="addNewCoupon()">Add Property Permit Package And Coupon</a>
								</div>
					<br>
					<div class="portlet box red">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-cogs"></i>All Property Permit Packages
							</div>
						</div>
						<div class="portlet-body">
							
						<div class="table-responsive">
								<table id='codeTable' class="table table-striped table-bordered table-hover">
								<thead>
								<tr>
									<th>
										 Name
									</th>
									<th>
										 Property
									</th>
									<th>
										 Passes
									</th>
									<th>
										Codes
									</th>
									<th>
										 Action
									</th>
								</tr>
								</thead>
								<tbody>
								
									<?php $SrNo=1;
											for($i=0;$i<count($allCoupons);$i++){
												echo "<tr>";
													
													echo '<td>';
														echo $allCoupons[$i]['CouponPackage']['name']?$allCoupons[$i]['CouponPackage']['name']:'No Name';
													echo '</td>';
													echo '<td>';
														echo $allCoupons[$i]['Property']['name'];
													echo '</td>';
													echo '<td>';
														$passes=explode(',',$allCoupons[$i]['CouponPackage']['passes']);
														$passStr='';
														for($j=0;$j<count($passes);$j++){
															$passName=$this->requestAction('/admin/Passes/getPassName/'.$passes[$j]);
															if($passName){
																$passStr=$passStr.$passName;
																if($j<(count($passes)-1)){
																	$passStr=$passStr.',';
																}
															}
														}
														echo $passStr;
													echo '</td>';
													echo '<td>';
														$codeStr='';
														for($k=0;$k<count($allCoupons[$i]['CouponCode']);$k++){
															$codeStr=$codeStr.$allCoupons[$i]['CouponCode'][$k]['code'];
																if($k<(count($allCoupons[$i]['CouponCode'])-1)){
																	$codeStr=$codeStr.',';
																}
														}
														echo $codeStr;
													echo '</td>';
													echo '<td>';
														echo '<a href="/admin/CouponPackages/edit/'.$allCoupons[$i]['CouponPackage']['id'].'">Edit</a>';
													echo '</td>';
												echo "</tr>";
												$SrNo++;
											}
									
									?>
								
								</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
					 
			</div>
		</div>
	</div>
<?php echo $this->Html->script('//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js'); ?>
<?php echo $this->Html->css('//cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css'); ?> 

<script type="text/javascript">
$(function(){
	$('#codeTable').dataTable();
});

</script>
