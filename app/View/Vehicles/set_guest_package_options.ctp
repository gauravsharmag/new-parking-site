<?php echo $this->Html->script('jquery.min'); ?>
<script>
$(document).ready(function(){
	
	$("#VehicleTo").change(function(){
			var oneday = 24*60*60*1000;
			var start_date_array = $('#VehicleFrom').val().split('/');
			var end_date_array = $(this).val().split('/');
			var end_date = new Date(parseInt(end_date_array[2], 10),
									   parseInt(end_date_array[0], 10) - 1,
									   parseInt(end_date_array[1], 10));
			var start_date = new Date(parseInt(start_date_array[2], 10),
									   parseInt(start_date_array[0], 10) - 1,
									   parseInt(start_date_array[1], 10));
			var days = Math.floor((end_date.getTime() - start_date.getTime())/(oneday));
			console.log(days); 
			var permitCost=<?php echo $permitCost;?>;
			$('#VehicleDays').val(days);
			$('#VehicleAmount').val(days*permitCost); 
			var remainingCredits=<?php echo $remainingCredits;?>;
			
			if(days>=remainingCredits){
				$('#VehicleCreditsUsed').val(remainingCredits);
				$('#VehicleDiscount').val(remainingCredits*permitCost);
				$('#VehicleNetAmmount').val($('#VehicleAmount').val()-$('#VehicleDiscount').val());
			}else{
				var creditsUsed=remainingCredits-days;
				$('#VehicleCreditsUsed').val(days);
				$('#VehicleDiscount').val(days*permitCost);
				var amountPayable=$('#VehicleAmount').val()-$('#VehicleDiscount').val();
				if(amountPayable<=0){
					$('#VehicleNetAmmount').val('0');
				}else{
					$('#VehicleNetAmmount').val(amountPayable);
				}
				
			}
			console.log(remainingCredits); 
	});
	if($("#VehicleTo").val()!=""){
			var oneday = 24*60*60*1000;
			var start_date_array = $('#VehicleFrom').val().split('/');
			var end_date_array = $(this).val().split('/');
			var end_date = new Date(parseInt(end_date_array[2], 10),
									   parseInt(end_date_array[0], 10) - 1,
									   parseInt(end_date_array[1], 10));
			var start_date = new Date(parseInt(start_date_array[2], 10),
									   parseInt(start_date_array[0], 10) - 1,
									   parseInt(start_date_array[1], 10));
			var days = Math.floor((end_date.getTime() - start_date.getTime())/(oneday));
			console.log(days); 
			var permitCost=<?php echo $permitCost;?>;
			$('#VehicleDays').val(days);
			$('#VehicleAmount').val(days*permitCost); 
			var remainingCredits=<?php echo $remainingCredits;?>;
			
			console.log(remainingCredits);
			if(days>=remainingCredits){
				$('#VehicleCreditsUsed').val(remainingCredits);
				$('#VehicleDiscount').val(remainingCredits*permitCost);
				$('#VehicleNetAmmount').val($('#VehicleAmount').val()-$('#VehicleDiscount').val());
			}else{
				var creditsUsed=remainingCredits-days;
				$('#VehicleCreditsUsed').val(days);
				$('#VehicleDiscount').val(days*permitCost);
				var amountPayable=$('#VehicleAmount').val()-$('#VehicleDiscount').val();
				if(amountPayable<=0){
					$('#VehicleNetAmmount').val('0');
				}else{
					$('#VehicleNetAmmount').val(amountPayable);
				}	
			}
	}
	 
});
function parseDate(str) {
			var d = str.split('/');
			return new Date(parseInt(d[2]),parseInt(d[0]), parseInt(d[1]));
		}
</script>
<div class="page-content-wrapper">
	<div class="page-content" style="min-height:996px">	
		<!-- BEGIN PAGE HEADER-->
		<div class="row">
			<div class="col-md-12">
				<!-- BEGIN PAGE TITLE & BREADCRUMB-->
				<h3 class="page-title">
				Order View <small>view order details</small>
				</h3>
				<ul class="page-breadcrumb breadcrumb">

					<li>
						<i class="fa fa-home"></i> 
						<?php echo $this->Html->link(
													 'Home',
													  array(
															'controller' => 'Users',
															'action' => 'myAccount',
															'full_base' => true
																													  )
													  );?>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="#">Pass View</a>
					</li>
				</ul>
				<!-- END PAGE TITLE & BREADCRUMB-->
			</div>
		</div>
		<!-- END PAGE HEADER-->
		<div id="flashMessages">
								<h2><?php  echo $this->Session->flash();?></h2>
		</div>
		<!-- BEGIN PAGE CONTENT-->
		<div class="row">
			<div class="col-md-12">
				
		<!-- BEGIN PORTLET-->
				<div class="portlet box blue-hoki">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-gift"></i>SELECT PERMIT DETAILS
					    </div>
					</div>
					<div class="portlet-body">
						<div class="alert alert-success">
                                        <strong>Free Guest Credits Details </strong>
                         </div>
						<div class="row static-info">
							<div class="col-md-2 name">
								Total Credits :
							</div>
							<div class="col-md-7 value">
								<?php echo $freeGuestCredits;?>
							</div>
						</div>
						<div class="row static-info">
							<div class="col-md-2 name">
								Remaining :
							</div>
							<div class="col-md-7 value">
								<?php echo $remainingCredits;?>
							</div>
						</div>
						<div class="alert alert-danger">
                                        <strong>Select Number Of Days  </strong>
                         </div>
                         <?php echo $this->Form->create('Vehicle',array('class'=>''));?>
							<div class="form-body">	
								<div class="row static-info">
									<div class="col-md-2 name">
											From:
									</div>
									<div class="col-md-3 value">
										<?php   $dt = new DateTime();
												$currentDate= $dt->format('m/d/Y');
												echo $this->Form->input('from',array('required'=>true,'type'=>'text','div'=>false,
														'label'=>false,
														'disabled' => 'disabled',
														'value'=>$currentDate,                                               					                                               								
														'class'=>'form-control'));
										?>
									</div>
								</div>
								<div class="row static-info">
									<div class="col-md-2 name">
										To
									</div>
									<div class="col-md-3 value">
										<?php $tomorrow = new DateTime('tomorrow');
										
										  echo $this->Form->input('to',array('required'=>true,'type'=>'text','div'=>false,
														'label'=>false,
														'between'=>'<div class="input-group date date-picker" data-date-format="mm/dd/yyyy" data-date-start-date="'.$tomorrow->format('m/d/Y').'">',
														'after'=>'<span class="input-group-btn"><button type="button" class="btn default"><i class="fa fa-calendar"></i></button></span></div>',
														'class'=>'form-control'));
										?>
									</div>
								</div>
								<div class="row static-info">
									<div class="col-md-4 name">
										 Number Of Days:<br>
										<?php
											echo $this->Form->input('days',array('required'=>true,'type'=>'text','div'=>false,
														'label'=>false,
														'disabled' => 'disabled',
														'value'=>'',                                               					                                               								
														'class'=>'form-control'));
										 ?>
									</div>
									<div class="col-md-4 name">
										 Cost Per Day ($) :<br>
										 <?php
											echo $this->Form->input('cost',array('required'=>true,'type'=>'text','div'=>false,
														'label'=>false,
														'disabled' => 'disabled',
														'value'=>$permitCost,                                               					                                               								
														'class'=>'form-control'));
										 ?>
									</div>
									<div class="col-md-4 name">
										 Amount ($) :<br>
										  <?php
											echo $this->Form->input('amount',array('required'=>true,'type'=>'text','div'=>false,
														'label'=>false,
														'disabled' => 'disabled',
														'value'=>'',                                               					                                               								
														'class'=>'form-control'));
										 ?>
									</div>
								</div>
								<div class="row static-info">
									<div class="col-md-4 name">
										 Using Credits:<br>
										<?php
											echo $this->Form->input('creditsUsed',array('required'=>true,'type'=>'text','div'=>false,
														'label'=>false,
														'disabled' => 'disabled',
														'value'=>'',                                               					                                               								
														'class'=>'form-control'));
										 ?>
									</div>
									<div class="col-md-4 name">
										 Cost Per Day ($) :<br>
										 <?php
											echo $this->Form->input('cost',array('required'=>true,'type'=>'text','div'=>false,
														'label'=>false,
														'disabled' => 'disabled',
														'value'=>$permitCost,                                               					                                               								
														'class'=>'form-control'));
										 ?>
									</div>
									<div class="col-md-4 name">
										Discount ($) :<br>
										  <?php
											echo $this->Form->input('discount',array('required'=>true,'type'=>'text','div'=>false,
														'label'=>false,
														'disabled' => 'disabled',
														'value'=>'',                                               					                                               								
														'class'=>'form-control'));
										 ?>
									</div>
								</div>
								<div class="row static-info">
									<div class="col-md-2 name">
										Net Amount Payable ($) :
									</div>
									<div class="col-md-6 value">
										 <?php 
											echo $this->Form->input('netAmmount',array('required'=>true,'type'=>'text','div'=>false,
														'label'=>false,
														'disabled' => 'disabled',
														'value'=>'',                                               					                                               								
														'class'=>'form-control'));
										 ?>
									</div>
								</div>
							</div>
							<hr>
							<div class="form-actions">
								<button type="submit" class="btn blue">Submit</button>
								<a href="/Vehicles/add_vehicles" class="btn default">Cancel</a>
							</div>
						<?php echo $this->Form->end(); ?>
					</div>
				</div>
				<!-- END PORTLET-->
			</div>
		</div>
		<!-- END PAGE CONTENT-->
	</div>
</div>
