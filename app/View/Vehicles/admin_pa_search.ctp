<div class="page-content-wrapper">
		<div class="page-content" style="min-height:1027px">
			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					Search <small>Search Results</small>
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li>
							<i class="fa fa-home"></i>
							<?php 
								if(AuthComponent::user('role_id')==1){
										echo $this->Html->link('Home',array(
                                          'controller' => 'Users',
                                          'action' => 'superAdminHome',
                                          'full_base' => true
                                      ));
                                    }elseif(AuthComponent::user('role_id')==2){
										echo $this->Html->link('Home',array(
                                          'controller' => 'Users',
                                          'action' => 'pa_home_page',
                                          'full_base' => true
                                      ));
                                    }
                                  
                               ?>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">Search Results</a>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
			<div id="flashMessages">
									<h2><?php  echo $this->Session->flash();?></h2>
			</div>
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					
					<!-- Begin: life time stats -->
					<div class="portlet">
						<div class="portlet-title">
							<div class="caption">
								Search
							</div>
						</div>
						<div class="portlet-body">
							<div class="nav-pills"> 
								<?php echo $this->Form->create('Vehicle',array('class'=>'form-horizontal','url'=>array('action'=>'pa_search')))?>				 
									  <div class="input-group col-md-3">
										<?php echo $this->Form->input('search_key',array('placeholder'=>'Search...','div'=>false,'class'=>'form-control','label'=>false));?>
											<span class="input-group-btn">
												<button class="btn submit yellow"><i class="icon-magnifier"></i></button>
											</span>
									</div>
								<?php echo $this->Form->end();?>
							</div>
							<div class="table-container">		
								<div class="table-responsive">
                                              <table id="search" class="table table-striped table-bordered table-hover">
                                            	 <thead>
                                            	   	<tr>
													   <th>User</th>
                                                       <th>Make/Model/Color</th>
                                                       <th>Plate Number</th>
                                                       <th>VIN</th>
                                                       <th>RFID</th> 
                                                       <th>Pass Status</th>
                                                       <th>Permit Status</th>
                                                    </tr>
                        						  </thead>
                                            	  <tbody>
													   <?php if(!empty($searchArray)){
																	$dt = new DateTime();
																	$currentDateTime= $dt->format('Y-m-d H:i:s');
																foreach($searchArray as $arr){
																	$class='';
																	if($arr['Vehicle']['status']=='unknown')
																	{
																		if($arr['Vehicle']['pass_valid_upto']>=$currentDateTime){
																			$class='success';
																		}elseif($arr['Vehicle']['pass_valid_upto']<$currentDateTime){
																			$class='danger';
																		}
																	}elseif($arr['Vehicle']['status']=='noRFID'||$arr['Vehicle']['status']=='noVehicle'){
																		$class='warning';
																	}
														   ?>
															<tr class="<?php echo $class;?>">
																<td>
																	<?php echo $arr['Vehicle']['nameOfUser']."<br>".$arr['Vehicle']['username']."<br>".$arr['Vehicle']['contact'];//Changes Done on 11th Nov to Add User DATA?>
																</td>
																<td>
																	<?php echo $arr['Vehicle']['make']."/".$arr['Vehicle']['model']."/".$arr['Vehicle']['color'];?>
																</td>
																<td>
																	<?php if($arr['Vehicle']['license_plate_number']=='No Vehicle'){
																				echo "No Vehicle";
																			}else{
																				echo $arr['Vehicle']['license_plate_number'].' '.
																					 $arr['Vehicle']['license_plate_state'];
																			}
																	?>
																</td>
																<td>
																	<?php if($arr['Vehicle']['vin']=='No Vehicle'){
																				echo "No Vehicle";
																			}else{
																				echo $arr['Vehicle']['vin'];
																			}
																	?>
																</td>
																<td>
																	<?php echo $arr['Vehicle']['RFID_tag_number']?>
																</td>
																<td>
																	<?php $dt = new DateTime();
																		  $currentDateTime= $dt->format('Y-m-d H:i:s');
																		  if($arr['Vehicle']['status']!='noRFID'){
																				if($arr['Vehicle']['pass_valid_upto']>=$currentDateTime){
																					echo date("m/d/Y H:i:s", strtotime($arr['Vehicle']['pass_valid_upto'])).' '.
																							'<span class="label label-sm label-danger">Active</span>';
																				}else{
																					echo date("m/d/Y H:i:s", strtotime($arr['Vehicle']['pass_valid_upto'])).' '.
																							'<span class="label label-sm label-warning">Expired</span>';
																				}
																			}else{
																				echo $arr['Vehicle']['pass_valid_upto'];
																			}	
																	?>
																</td>
																<td>
																	<?php  
																			if(is_null($arr['Vehicle']['membership_vaild_upto'])){
																				echo "No Permit";
																			}elseif($arr['Vehicle']['membership_vaild_upto']>=$currentDateTime){
																				echo date("m/d/Y H:i:s", strtotime($arr['Vehicle']['membership_vaild_upto'])).' '.
																						'<span class="label label-sm label-danger">Active</span>';
																			}elseif($arr['Vehicle']['membership_vaild_upto']<$currentDateTime){
																				echo date("m/d/Y H:i:s", strtotime($arr['Vehicle']['membership_vaild_upto'])).' '.
																						'<span class="label label-sm label-warning">Expired</span>';
																			}
																	?>
																</td>
															</tr>
														<?php }}else{?> 
															 <tr>
																	<td colspan="7"> No Results Found</td>
															</tr>
															
														<?php	}?>
                                            	   </tbody>
                                            	</table>
												
                                        </div>
							</div>
						</div>
					</div>
					<!-- End: life time stats -->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
<?php if(!empty($searchArray)){?>
<script src="//cdn.datatables.net/1.10.2/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function() {
	$('#search').dataTable();
});
</script>
<?php }?>
