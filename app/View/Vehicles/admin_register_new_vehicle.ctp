<div class="vehicles form">
<?php echo $this->Form->create('Vehicle',array('url'=>array('controller'=>'vehicles','action'=>'add')));  //$userdata = $this->session->read('Auth.User');
                                                                                               // debug($userdata);


      ?>
	<fieldset>
		<legend><?php echo __('Register New Vehicle'); ?></legend>
	<?php
        echo $this->Form->hidden('Vehicle.user_id',array('value'=>$this->session->read('Auth.User.id')));
		echo $this->Form->input('property_id',array( 'label'=>'Property Name',
                                                                              'type'    => 'select',
                                                                              'options' => $propertyName,
                                                                              'empty'=>'Choose One'
                                                                                 ));
		echo $this->Form->input('vehicle_type_name',array('options'=>array('General'=>'General','VIP'=>'VIP','Commercial'=>'Commercial'),'empty'=>'Select'));
		echo $this->Form->input('make',array('required'=>true,'placeholder'=>'Brand name'));
		echo $this->Form->input('model',array('required'=>true,'placeholder'=>'Model Name'));
		echo $this->Form->input('color',array('required'=>true,'placeholder'=>'Color'));
		echo $this->Form->input('license_plate_number',array('required'=>true,'placeholder'=>'Licence Number'));
		echo $this->Form->input('license_plate_state',array('required'=>true,'placeholder'=>'???'));
		//echo $this->Form->input('reserved_space');
		//echo $this->Form->input('last_4_digital_of_vin');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
   <div>
       <?php echo $this->element('homePage');?>
   </div>

<div class="actions">
	<h3><?php echo __(''); ?></h3>
	<ul>
         <li><?php echo $this->Html->link(__('My Account'), array('controller'=>'users','action' => 'myAccount')); ?></li>
         <li><?php echo $this->Html->link(__('My Vehicle'), array('controller' => 'Vehicles', 'action' => 'myVehicle')); ?> </li>
         <li><?php echo $this->Html->link(__('Guest Vehicle'), array('controller' => 'Vehicles', 'action' => 'registerGuestVehicle')); ?> </li>

	</ul>
</div>

