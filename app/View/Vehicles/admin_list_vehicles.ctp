<div class="page-content-wrapper">
		<div class="page-content" style="min-height:1027px">
			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					Vehicles <small>Property Wide Vehicle Results</small>
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li>
							<i class="fa fa-home"></i>
							<?php 
								if(AuthComponent::user('role_id')==1){
										echo $this->Html->link('Home',array(
                                          'controller' => 'Users',
                                          'action' => 'superAdminHome',
                                          'full_base' => true
                                      ));
                                    }
                                  
                               ?>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">Vehicle Listing</a>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
			<div id="flashMessages">
									<h2><?php  echo $this->Session->flash();?></h2>
			</div>
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					
					<!-- Begin: life time stats -->
					<div class="portlet box grey-cascade">
						<div class="portlet-title ">
							<div class="caption">
								<i class="fa fa-users"></i>Vehicle List
							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								
							</div>
						</div>	
						<div class="portlet-body">
							<div class="row">
										<?php  
												echo $this->Form->input('current_property',array( 'type'=>'select',
																										'options' => $property_list,
																										'selected'=>$selectedId,
																										'label'=>false,
																										'between'=>'<div class="col-md-3">',
																										'after'=>'</div>',
																										'class'=>'form-control' 
																									));
										?>
							</div>
							<hr>
							<div class="table-container">		
								<div class="table-scrollable">
                                     <table id="vehicles_listing" class="table table-striped table-bordered table-hover">
                                    	 <thead>
                                    	   	<tr>
                                               <th>User Name</th>
                                               <th>Make</th>
                                               <th>Model</th>
                                               <th>Color</th>
                                               <th>Number</th>
                                               <th>State</th>
                                               <th>Details</th>
                                            </tr>
                						  </thead>
                                    	  <tbody>
                                               <tr>
													<td colspan="7" class="dataTables_empty">Loading Vehicles...</td>
												</tr>
                                    	   </tbody>
                                    	</table>
										
                                </div>
							</div>
						</div>
					</div>
					<!-- End: life time stats -->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>

<?php echo $this->Html->script('//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js'); ?>
<?php echo $this->Html->css('//cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css'); ?>
<script type="text/javascript">
$(document).ready(function() {
	$('select').on('change', function() {
			request="/admin/vehicles/get_property_vehicle_details/"+this.value+"";
			$('#vehicles_listing').dataTable({
				"bProcessing": false,
				"bServerSide": true,
				"bDestroy":true,
				"sAjaxSource": request
        
			});
		
	});
	$(function(){
		$('#vehicles_listing').dataTable({
			"bProcessing": false,
			"bServerSide": true,
			//"sAjaxSource": "<?php echo $this->Html->Url(array('controller' => 'vehicles', 'action' => 'get_property_vehicle_details',$selectedId)); ?>"   
			"sAjaxSource": request="/admin/vehicles/get_property_vehicle_details/"+$('#current_property').val()+""
		});
	});
});

</script>

