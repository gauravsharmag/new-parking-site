
<div class="page-content-wrapper">
		<div class="page-content">
			
<!-- BEGIN PAGE CONTENT-->
<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					<?php echo $this->requestAction(array('controller'=>'Properties','action'=>'getPropertyNameRFIF',$this->request->data['Vehicle']['property_id']));?> <small>Vehicles Details</small>
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li>
							<i class="fa fa-home"></i>
							<?php echo $this->Html->link(
                                              'Home',
                                              array(
                                                 'controller' => 'Users',
                                                 'action' => 'superAdminHome',
                                                 'full_base' => true
                                                                  )
                                              );?>
							<i class="fa fa-angle-right"></i>
						</li>

						<li>
							<a href="#">Vehicle Detail</a>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<div id="flashMessages">
											<h2><?php  echo $this->Session->flash();?></h2>
			</div>
			<!-- END PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
				<h3 class="page-title">Vehicle Details <small></small></h3>
					<div class="tabbable tabbable-custom boxless tabbable-reversed">


							<div class="tab-pane active" id="tab_0">
								<div class="portlet box green">
									<div class="portlet-title">
										<div class="caption">
											<i class="fa fa-gift"></i><?php  echo 'Vehicle Details';?>
										</div>

									</div>
									
										<!-- BEGIN FORM-->

									<div class="portlet-body form">
                                     <div class="form-body">
                                              <h3><?php echo $this->Session->flash();?></h3>
                                              <div class="note note-success">
                                                						<p>
                                                							User Details
                                                						</p>
                                             </div>
											  <div class="row static-info">
												<div class="col-md-5 name">
														Property :
												</div>
												<div class="col-md-7 value">
													<?php echo $this->requestAction(array('controller'=>'Properties','action'=>'getPropertyNameRFIF',$this->request->data['Vehicle']['property_id']));?>
												</div>
											</div>
											 <div class="row static-info">
												<div class="col-md-5 name">
														User Name :
												</div>
												<div class="col-md-7 value">
													<?php echo "<a href='/admin/users/view_user_details/".$this->request->data['User']['id']."'>".$this->request->data['User']['username']."</a>";?>
												</div>
											</div>
											<div class="row static-info">
												<div class="col-md-5 name">
														Address :
												</div>
												<div class="col-md-7 value">
													<?php echo $this->request->data['User']['first_name'].'  '.$this->request->data['User']['last_name']."<br>".
															   $this->request->data['User']['address_line_1'].'  '.$this->request->data['User']['address_line_2'].' '.
															   $this->request->data['User']['city'].'  '.$this->request->data['User']['state'].'  '."<br>".
															   $this->request->data['User']['zip'];
													?>
												</div>
											</div>
											<div class="row static-info">
												<div class="col-md-5 name">
														Email :
												</div>
												<div class="col-md-7 value">
													<?php echo $this->request->data['User']['email'];?>
												</div>
											</div>
											<div class="row static-info">
												<div class="col-md-5 name">
														Phone :
												</div>
												<div class="col-md-7 value">	
													<?php echo $this->request->data['User']['phone'];?>
												</div>
											</div>
											       
                                             <div class="note note-success">
                                                						<p>
                                                							 Vehicle Details
                                                						</p>
                                             </div>
											<div class="row static-info">
												<div class="col-md-5 name">
														 Name For Vehicle :
												</div>
												<div class="col-md-7 value">
													<?php echo $this->request->data['Vehicle']['owner'];?>
												</div>
											</div>
											<div class="row static-info">
												<div class="col-md-5 name">
														Make :
												</div>
												<div class="col-md-7 value">
													<?php echo $this->request->data['Vehicle']['make'];?>
												</div>
											</div>
											<div class="row static-info">
												<div class="col-md-5 name">
														Make :
												</div>
												<div class="col-md-7 value">
													<?php echo $this->request->data['Vehicle']['model'];?>
												</div>
											</div>
											<div class="row static-info">
												<div class="col-md-5 name">
														Color :
												</div>
												<div class="col-md-7 value">
													<?php echo $this->request->data['Vehicle']['color'];?>
												</div>
											</div>
											<div class="row static-info">
												<div class="col-md-5 name">
														Plate Number :
												</div>
												<div class="col-md-7 value">
													<?php echo $this->request->data['Vehicle']['license_plate_number'].' '.
															   $this->request->data['Vehicle']['license_plate_state'];
													?>
												</div>
											</div>
											<div class="row static-info">
												<div class="col-md-5 name">
														VIN :
												</div>
												<div class="col-md-7 value">
													<?php echo $this->request->data['Vehicle']['last_4_digital_of_vin'];?>
												</div>
											</div>
											<?php if(!empty($this->request->data['CustomerPass'])){
													 foreach($this->request->data['CustomerPass'] as $pass){
											?>
											<div class="row static-info">
												<div class="col-md-5 name">
														Pass:
												</div>
												<div class="col-md-7 value">
													<?php 
													      if(!is_null($pass['pass_id'])){
															   $dt = new DateTime();
															   $currentDateTime= $dt->format('Y-m-d H:m:s');
															   if($pass['pass_valid_upto']>=$currentDateTime){
																$tag="<span class='label label-sm label-success'>Active Pass</span>";
															   }else{
																	$tag="<span class='label label-sm label-warning'>Expired Pass</span>";
															   }
															echo $this->requestAction(array('controller'=>'Passes','action'=>'getPassName',$pass['pass_id'])).' '.
																 date("m/d/Y H:i:s", strtotime($pass['pass_valid_upto'])).' '.
																 $tag;
														  }else{
															echo "Pass Not Bought";
														  }
													?>
												</div>
											</div>
											<div class="row static-info">
												<div class="col-md-5 name">
														Parking Package:
												</div>
												<div class="col-md-7 value">
													<?php 
													      if(!is_null($pass['package_id'])){
															   $dt = new DateTime();
															   $currentDateTime= $dt->format('Y-m-d H:m:s');
															   if($pass['membership_vaild_upto']>=$currentDateTime){
																$tag="<span class='label label-sm label-success'>Active Package</span>";
															   }else{
																	$tag="<span class='label label-sm label-warning'>Expired Package</span>";
															   }
															echo $this->requestAction(array('controller'=>'Packages','action'=>'getPackageName',$pass['package_id'])).' '.
																 date("m/d/Y H:i:s", strtotime($pass['membership_vaild_upto'])).' '.
																 $tag;
														  }else{
															echo "Permit Not Bought";
														  }
													?>
												</div>
											</div>
											<div class="row static-info">
												<div class="col-md-5 name">
														Guest Vehicle:
												</div>
												<div class="col-md-7 value">
													<?php 
													      if($pass['is_guest_pass']==1){
																echo "Yes";
														  }else{
															echo "No";
														  }
													?>
												</div>
											</div>
											<div class="row static-info">
												<div class="col-md-5 name">
														RFID TAG:
												</div>
												<div class="col-md-7 value">
													<?php 
													      if(is_null($pass['RFID_tag_number'])){
																echo "No RFID Assigned"."<br>".
																 "<a href='/admin/CustomerPasses/edit/".$pass['id']."'>Add Now</a>";
														  }else{
															 echo  "<a href='/admin/CustomerPasses/edit/".$pass['id']."'>".$pass['RFID_tag_number']."</a>";
														  }
													?>
												</div>
											</div>
											
											<?php }
											}
											?>
                                     </div>

										<!-- END FORM-->
									</div>
								</div>

							</div>



					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->

