
<div class="page-content-wrapper">
		<div class="page-content">
			<div id="flashMessages">
											<h2><?php  echo $this->Session->flash();?></h2>
			</div>
<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
				<h3 class="page-title">Vehicles <small> Edit Vehicle Details</small></h3>
					<div class="tabbable tabbable-custom boxless tabbable-reversed">


							<div class="tab-pane active" id="tab_0">
								<div class="portlet box green">
									<div class="portlet-title">
										<div class="caption">
											<i class="fa fa-gift"></i><?php  echo 'Vehicle Details';?>
										</div>

									</div>
									
										<!-- BEGIN FORM-->

									<div class="portlet-body form">

									<?php echo $this->Form->create('Vehicle',array('class'=>'form-horizontal'));
                                         //echo $this->Html->link(__('List Existing Packages'), array('action' => 'index',$propertyId,$propertyName));
									?>
                                    <div class="form-body">
                                              <h3><?php echo $this->Session->flash();?></h3>
                                              <div class="note note-success">
                                                						<p>
                                                							 Pass Details
                                                						</p>
                                             </div>
											  <div class="row static-info">
												<div class="col-md-5 name">
														Property :
												</div>
												<div class="col-md-7 value">
													<?php echo $this->request->data['Property']['name'];?>
												</div>
											</div>
											 <div class="row static-info">
												<div class="col-md-5 name">
														Pass :
												</div>
												<div class="col-md-7 value">
													<?php 
															echo "No Pass Assigned";
													?>
												</div>
											</div>
											<div class="row static-info">
												<div class="col-md-5 name">
														User Name :
												</div>
												<div class="col-md-7 value">
													<?php 
														 echo $this->Html->link( $this->request->data['User']['username'],array('controller'=>'users','action'=>'view_user_details',$this->request->data['User']['id']));
													?>
												</div>
											</div>
											<div class="row static-info">
												<div class="col-md-5 name">
														Customer :
												</div>
												<div class="col-md-7 value">
													<?php echo $this->request->data['User']['first_name'].' '.$this->request->data['User']['last_name'].'<br>'.
															   $this->request->data['User']['address_line_1'].' '.$this->request->data['User']['address_line_2'].' '.$this->request->data['User']['city'].' '.$this->request->data['User']['state'].'<br>'.
															   $this->request->data['User']['zip'].'<br>'.
															   'Phone: '.$this->request->data['User']['phone'].'<br>'.
															   'Email: '.$this->request->data['User']['email'];
													?>
												</div>
											</div>
                                             <div class="note note-success">
                                                						<p>
                                                							 Vehicle Details
                                                						</p>
                                             </div>
                                            <?php        
													echo $this->Form->input('id'); 
													echo $this->Form->input('owner',array('div'=>array('class'=>'form-group'),
                                                                                         'label'=>array('class'=>'col-md-3 control-label','text'=>'Name For Vehicle'),
                                                                                         'between'=>'<div class="col-md-4">',
                                                                                         //'value'=>$this->request->data['Vehicle']['owner'],
                                                                                         'error'=>array('attributes'=>array('class'=>'model-error')),
                                                                                         'after'=>'</div>',
                                                                                         'class'=>'form-control'));
                                                    echo $this->Form->input('make',array('div'=>array('class'=>'form-group'),
                                                                                         'label'=>array('class'=>'col-md-3 control-label','text'=>'Make'),
                                                                                         'between'=>'<div class="col-md-4">',
                                                                                        // 'value'=>$this->request->data['Vehicle']['make'],
                                                                                         'error'=>array('attributes'=>array('class'=>'model-error')),
                                                                                         'after'=>'</div>',
                                                                                         'class'=>'form-control'));
                                                    echo $this->Form->input('model',array('div'=>array('class'=>'form-group'),
                                                                                         'label'=>array('class'=>'col-md-3 control-label','text'=>'Model'),
                                                                                         'between'=>'<div class="col-md-4">',
                                                                                         //'value'=>$this->request->data['Vehicle']['model'],
                                                                                         'error'=>array('attributes'=>array('class'=>'model-error')),
                                                                                         'after'=>'</div>',
                                                                                         'class'=>'form-control'));
                                                     echo $this->Form->input('color',array('div'=>array('class'=>'form-group'),
                                                                                         'label'=>array('class'=>'col-md-3 control-label','text'=>'Color'),
                                                                                         'between'=>'<div class="col-md-4">',
                                                                                        // 'value'=>$this->request->data['Vehicle']['color'],
                                                                                         'error'=>array('attributes'=>array('class'=>'model-error')),
                                                                                         'after'=>'</div>',
                                                                                         'class'=>'form-control'));
                                                     echo $this->Form->input('license_plate_number',array('div'=>array('class'=>'form-group'),
                                                                                         'label'=>array('class'=>'col-md-3 control-label','text'=>'License Plate Number'),
                                                                                         'between'=>'<div class="col-md-4">',
                                                                                         //'value'=>$this->request->data['Vehicle']['license_plate_number'],
                                                                                         'error'=>array('attributes'=>array('class'=>'model-error')),
                                                                                         'after'=>'</div>',
                                                                                         'class'=>'form-control'));
                                                     echo $this->Form->input('license_plate_state',array('type'=>'select','div'=>array('class'=>'form-group'),
                                                                                         'label'=>array('class'=>'col-md-3 control-label','text'=>'License Plate State'),
                                                                                         'between'=>'<div class="col-md-4">',
                                                                                         'options'=>$states,
                                                                                         'empty'=>'Select One',
                                                                                         //'value'=>$this->request->data['Vehicle']['license_plate_state'],
                                                                                         'error'=>array('attributes'=>array('class'=>'model-error')),
                                                                                         'after'=>'</div>',
                                                                                         'class'=>'form-control'));
                                                     echo $this->Form->input('last_4_digital_of_vin',array('div'=>array('class'=>'form-group'),
                                                                                         'label'=>array('class'=>'col-md-3 control-label','text'=>'Vin Last 4 Digits'),
                                                                                         'between'=>'<div class="col-md-4">',
                                                                                         //'value'=>$this->request->data['Vehicle']['last_4_digital_of_vin'],
                                                                                         'error'=>array('attributes'=>array('class'=>'model-error')),
                                                                                         'after'=>'</div>',
                                                                                         'class'=>'form-control'));
                                          ?>
                                           <div class="form-actions fluid">
												<div class="col-md-offset-3 col-md-9">
													<button type="submit" class="btn blue">Submit</button>
													<!--<button type="button" class="btn default">Cancel</button>-->
												</div>
											</div>
                                         <?php echo $this->Form->end(); ?>
                                     </div>

										<!-- END FORM-->
									</div>
								</div>

							</div>



					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.3.1/jquery.maskedinput.min.js" type="text/javascript"></script>
<script type="text/javascript">
  $("#CustomerPassPassValidUpto").mask("99/99/9999 99:99:99");
   $("#CustomerPassMembershipVaildUpto").mask("99/99/9999 99:99:99");
    </script>
