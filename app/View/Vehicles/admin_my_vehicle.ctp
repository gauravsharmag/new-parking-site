<div class="vehicles view">
<h2><?php $srNo=0;
//debug($vehicles);
?>
<h2>Registered Vehicle</h2>
<div>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo h('Sr.No.'); ?></th>
			<th><?php echo h('Property'); ?></th>
            <th><?php echo h('Type'); ?></th>
            <th><?php echo h('Make'); ?></th>
			<th><?php echo h('Model'); ?></th>
			<th><?php echo h('Color'); ?></th>
			<th><?php echo h('Plate Number'); ?></th>
			<th><?php echo h('license_plate_state'); ?></th>
			<th><?php echo h('reserved_space'); ?></th>
			<th><?php echo h('last_4_digital_of_vin'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($vehicles as $vehicle): $srNo++?>

	<tr>
		<td><?php echo $srNo; ?>&nbsp;</td>
		<td>
			<?php echo h($vehicle['Property']['name']); ?>
		</td>
        <td><?php echo h($vehicle['Vehicle']['vehicle_type_name']); ?>&nbsp;</td>
		<td><?php echo h($vehicle['Vehicle']['make']); ?>&nbsp;</td>
		<td><?php echo h($vehicle['Vehicle']['model']); ?>&nbsp;</td>
		<td><?php echo h($vehicle['Vehicle']['color']); ?>&nbsp;</td>
		<td><?php echo h($vehicle['Vehicle']['license_plate_number']); ?>&nbsp;</td>
		<td><?php echo h($vehicle['Vehicle']['license_plate_state']); ?>&nbsp;</td>
		<td><?php echo h($vehicle['Vehicle']['reserved_space']); ?>&nbsp;</td>
		<td><?php echo h($vehicle['Vehicle']['last_4_digital_of_vin']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'viewMyVehicle', $vehicle['Vehicle']['id'])); ?>
			<?php /*echo $this->Html->link(__('Edit'), array('action' => 'edit_my_vehicle', $vehicle['Vehicle']['id']));*/ ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'deleteMyVehicle', $vehicle['Vehicle']['id']), array(), __('Are you sure you want to delete # %s?', $vehicle['Vehicle']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>

	</div>
</div>
<div>
    <?php echo $this->element('homePage');?>
</div>
<div class="actions">
	<h3><?php echo __(''); ?></h3>
	<ul>
         <li><?php echo $this->Html->link(__('My Account'), array('controller'=>'users','action' => 'myAccount')); ?></li>
         <li><?php echo $this->Html->link(__('New Vehicle'), array('controller' => 'Vehicles', 'action' => 'registerNewVehicle')); ?> </li>
         <li><?php echo $this->Html->link(__('Guest Vehicle'), array('controller' => 'Vehicles', 'action' => 'registerGuestVehicle')); ?> </li>

	</ul>
</div>

