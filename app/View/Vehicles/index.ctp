
<div class="page-content-wrapper">
		<div class="page-content" style="min-height:1027px">

			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					
					My Vehicles <small>Vehicle Details</small>
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<!--<li class="btn-group">
							<a href="/Vehicles/register"><button class="btn btn-primary" id="AddNew">New Vehicle
							
							</button></a>
						</li>-->
						<li>
							<i class="fa fa-home"></i>
							<a href="/users/customerHomePage">Home</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">Vehicles</a>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
			<div id="flashMessages">
                	<h2><?php  echo $this->Session->flash();?></h2>
            </div>
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
				
					<!-- BEGIN SAMPLE TABLE PORTLET-->
					<div id="main" class="portlet box green">
						
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-cogs"></i>My Vehicles
							</div>
							<div class="tools">
								<a class="collapse" href="javascript:;">
								</a>
							</div>
						</div>
						<div class="portlet-body flip-scroll">
						<?php if(empty($activeVehicles) && empty($customerOtherVehicles))
								echo "<h3>No Vehicle Data Present. Click <b>New Vehicle</b> to register new vehicle.</h3>";
						?>
						<?php if(!empty($activeVehicles)){?>
							<div class="portlet box yellow">
								<div class="portlet-title">
									<div class="caption">
										<i class="fa fa-cogs"></i>Active Vehicles
									</div>
									<div class="tools">
										<a class="collapse" href="javascript:;">
										</a>
									</div>
								</div>
						   <div class="portlet-body flip-scroll">
							<table class="table table-bordered table-striped table-condensed flip-content">
							<thead class="flip-content">
							<tr>
								<th width="5%">
									 #
								</th>
								<th>
									Vehicle Details
								</th>
								<th>
									Pass Details
								</th>
							</tr>
							</thead>
							<tbody>
							<?php $i=1; foreach($activeVehicles as $activeVehicle){?>
                                                								<tr>
																					<td>
																						<?php echo $i++;?>
																					</td>
                                                									<td>																						 
																						<table>	
																							<tr><td>Vehicle Name :</td><td><?php echo $activeVehicle['Vehicle']['owner'];?></td></tr>																			
																							<tr><td>Status :</td><td><span class="label label-sm label-danger">Active Vehicle</span></td></tr>
																							<tr><td>Make :</td><td><?php echo $activeVehicle['Vehicle']['make'];?></td></tr>
																							<tr><td>Model :</td><td><?php echo $activeVehicle['Vehicle']['model'];?></td></tr>
																							<tr><td>Color :</td><td><?php echo $activeVehicle['Vehicle']['color'];?></td></tr>
																							<tr><td>Plate Number :</td><td><?php echo $activeVehicle['Vehicle']['license_plate_number'];?></td></tr>
																							<tr><td>State :</td><td><?php echo $activeVehicle['Vehicle']['license_plate_state'];?></td></tr>
																							<tr><td>VIN :</td><td><?php echo $activeVehicle['Vehicle']['last_4_digital_of_vin'];?></td></tr>
																							<?php if(AuthComponent::user('role_id')==4){?>
																							<tr><td>
																									<a href="/ChangeVehicleDetails/change/<?php echo $activeVehicle['Vehicle']['id'] ?>">   
																										<button class="btn btn-primary" >Request Admin to change details</button>
																									</a>
																								</td>
																							</tr>
																							<?php }?>
                                                										</table>
                                                									</td>
																					<td>
																						<table>
																							<tr><td>Status :</td><td><span class="label label-sm label-danger">Active Pass</span></td></tr>
																							<tr><td>Pass :</td><td><?php if(AuthComponent::user('role_id')==4){
																															echo $this->requestAction(array('controller'=>'Passes','action'=>'getPassName',$activeVehicle['Vehicle']['customer_pass_id']));
																														}else{
																															echo $this->requestAction(array('controller'=>'Passes','action'=>'getPName',$activeVehicle['Vehicle']['customer_pass_id']));
																														}
																							
																												?></td>
																							</tr>
																							<tr><td>Pass Expiration Date :</td><td><?php echo date("m/d/Y H:i:s", strtotime($activeVehicle['CustomerPass']['pass_valid_upto']));?></td></tr>
																							<tr><td>Parking Location :</td><td><?php echo $this->requestAction(array('controller'=>'Packages','action'=>'getPackageName',$activeVehicle['CustomerPass']['package_id']));
																														  //echo $rslt;?></td>
																							</tr>
																							<!--<tr><td>Parking Package Expiration Date :</td><td><?php echo date("m/d/Y H:i:s", strtotime($activeVehicle['CustomerPass']['membership_vaild_upto']));?></td></tr>-->
																							<tr><td>RFID Tag Number :</td><td><?php echo is_null($activeVehicle['CustomerPass']['RFID_tag_number'])?"NA":$activeVehicle['CustomerPass']['RFID_tag_number'];?>
																								</td>
																							</tr>
                                                										</table>
																					</td>																					
																				
                                                									
                                                								</tr>
							<?php }?>
							
							</tbody>
							</table>
							</div>
							</div>
							<?php }?>
							<?php if(!empty($customerOtherVehicles)){?>
							<div class="portlet box blue">
								<div class="portlet-title">
									<div class="caption">
										<i class="fa fa-cogs"></i>Registered Vehicles
									</div>
									<div class="tools">
										<a class="collapse" href="javascript:;">
										</a>
									</div>
								</div>
						   <div class="portlet-body flip-scroll">
							<table class="table table-bordered table-striped table-condensed flip-content">
							 <?php if($customerOtherVehicles!=null){?>
								<?php $j=0;for($i=0;$i<(round(count($customerOtherVehicles)/2,0,PHP_ROUND_HALF_DOWN)+1)*2;$i=$i+2){?>
								     <tr>
								         <?php if(array_key_exists($i,$customerOtherVehicles)){?>
											 <td>
												<table>		
													<tr><td>Vehicle Name :</td><td><?php echo $customerOtherVehicles[$i]['Vehicle']['owner'];?></td></tr>		
													<tr><td>Status :</td><td><span class="label label-sm label-success">Registered Vehicle</span></td></tr>
													<tr><td>Make :</td><td><?php echo $customerOtherVehicles[$i]['Vehicle']['make'];?></td></tr>
													<tr><td>Model :</td><td><?php echo $customerOtherVehicles[$i]['Vehicle']['model'];?></td></tr>
													<tr><td>Color :</td><td><?php echo $customerOtherVehicles[$i]['Vehicle']['color'];?></td></tr>
													<tr><td>Plate Number :</td><td><?php echo $customerOtherVehicles[$i]['Vehicle']['license_plate_number'];?></td></tr>
													<tr><td>State :</td><td><?php echo $customerOtherVehicles[$i]['Vehicle']['license_plate_state'];?></td></tr>
													<tr><td>VIN :</td><td><?php echo $customerOtherVehicles[$i]['Vehicle']['last_4_digital_of_vin'];?></td></tr>
													<tr><td><?php echo $this->Html->link('Edit Details',array('controller'=>'vehicles','action'=>'edit',$customerOtherVehicles[$i]['Vehicle']['id']));?></td></tr>
												</table>
											  </td>
										<?php }?>
										<?php if(array_key_exists($i+1,$customerOtherVehicles)){?>
												<td>
												<table>
													<tr><td>Vehicle Name :</td><td><?php echo $customerOtherVehicles[$i+1]['Vehicle']['owner'];?></td></tr>	
													<tr><td>Status :</td><td><span class="label label-sm label-success">Registered Vehicle</span></td></tr>
													<tr><td>Make :</td><td><?php echo $customerOtherVehicles[$i+1]['Vehicle']['make'];?></td></tr>
													<tr><td>Model :</td><td><?php echo $customerOtherVehicles[$i+1]['Vehicle']['model'];?></td></tr>
													<tr><td>Color :</td><td><?php echo $customerOtherVehicles[$i+1]['Vehicle']['color'];?></td></tr>
													<tr><td>Plate Number :</td><td><?php echo $customerOtherVehicles[$i+1]['Vehicle']['license_plate_number'];?></td></tr>
													<tr><td>State :</td><td><?php echo $customerOtherVehicles[$i+1]['Vehicle']['license_plate_state'];?></td></tr>
													<tr><td>VIN :</td><td><?php echo $customerOtherVehicles[$i+1]['Vehicle']['last_4_digital_of_vin'];?></td></tr>
													<tr><td><?php echo $this->Html->link('Edit Details',array('controller'=>'vehicles','action'=>'edit',$customerOtherVehicles[$i+1]['Vehicle']['id']));?></td></tr>
												</table>
											  </td>
										<?php }?>
									 </tr>
								<?php }?>
							<?php }?>
							
							</table>
							</div>
							</div>
							<?php }?>
						</div>
					</div>
					<!-- END SAMPLE TABLE PORTLET-->

				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>

