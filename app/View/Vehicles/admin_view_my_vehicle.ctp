<div class="vehicles view">
<h2><?php echo __('Registered Vehicle Details'); //debug($vehicle);?></h2>
	<dl>

		<dt><?php echo __('Vehicle Type'); ?></dt>
		<dd>
			<?php echo h($vehicle['Vehicle']['vehicle_type_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Make'); ?></dt>
		<dd>
			<?php echo h($vehicle['Vehicle']['make']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Model'); ?></dt>
		<dd>
			<?php echo h($vehicle['Vehicle']['model']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Color'); ?></dt>
		<dd>
			<?php echo h($vehicle['Vehicle']['color']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Plate Number'); ?></dt>
		<dd>
			<?php echo h($vehicle['Vehicle']['license_plate_number']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Plate State'); ?></dt>
		<dd>
			<?php echo h($vehicle['Vehicle']['license_plate_state']); ?>
			&nbsp;
		</dd>

        			<?php echo $this->Html->link(__('Edit'), array('action' => 'editMyVehicle', $vehicle['Vehicle']['id']));?>
                    &nbsp;&nbsp;&nbsp;
                    <?php echo @$this->Html->link(__('Apply for Pass'), array('controller'=>'Passes','action' => 'applyPass', array($vehicle['Vehicle']['id']),
                                                                                                                                                        $vehicle['User']['id'],
                                                                                                                                                         $vehicle['Property']['id'],
                                                                                                                                                         $vehicle['Vehicle']['vehicle_type_name'],
                                                                                                                                                          $vehicle['Property']['name']


                    ));?>


	</dl>
</div>
<div>
    <?php echo $this->element('homePage');?>
</div>

<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		  <li><?php echo $this->Html->link(__('My Account'), array('controller'=>'users','action' => 'myAccount')); ?></li>
                 <li><?php echo $this->Html->link(__('My Vehicle'), array('controller' => 'Vehicles', 'action' => 'myVehicle')); ?> </li>
                 <li><?php echo $this->Html->link(__('Guest Vehicle'), array('controller' => 'Vehicles', 'action' => 'registerGuestVehicle')); ?> </li>
	</ul>
</div>

