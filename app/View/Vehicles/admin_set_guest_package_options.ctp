<?php echo $this->Html->script('jquery.min'); ?>
<script>
$(document).ready(function(){
	$("#VehicleVehicleId").change(function() {
		if($("#VehicleVehicleId option:selected").val()!=""){
			$("#VehicleOwner").attr("required",false);
			$("#VehicleMake").attr("required",false);
			$("#VehicleModel").attr("required",false);
			$("#VehicleColor").attr("required",false);
			$("#VehicleLicensePlateNumber").attr("required",false);
			$("#VehicleLicensePlateState").attr("required",false);
			$("#VehicleLast4DigitalOfVin").attr("required",false);
		}
		else{
			console.log($("#VehicleVehicleId option:selected").val());
			$("#VehicleOwner").attr("required",true);
			$("#VehicleMake").attr("required",true);
			$("#VehicleModel").attr("required",true);
			$("#VehicleColor").attr("required",true);
			$("#VehicleLicensePlateNumber").attr("required",true);
			$("#VehicleLicensePlateState").attr("required",true);
			$("#VehicleLast4DigitalOfVin").attr("required",true);
		}
			 
	}); 
	if($("#VehicleVehicleId option:selected").val()!=""){
			$("#VehicleOwner").attr("required",false);
			$("#VehicleMake").attr("required",false);
			$("#VehicleModel").attr("required",false);
			$("#VehicleColor").attr("required",false);
			$("#VehicleLicensePlateNumber").attr("required",false);
			$("#VehicleLicensePlateState").attr("required",false);
			$("#VehicleLast4DigitalOfVin").attr("required",false);
		}
		else{
			console.log($("#VehicleVehicleId option:selected").val());
			$("#VehicleOwner").attr("required",true);
			$("#VehicleMake").attr("required",true);
			$("#VehicleModel").attr("required",true);
			$("#VehicleColor").attr("required",true);
			$("#VehicleLicensePlateNumber").attr("required",true);
			$("#VehicleLicensePlateState").attr("required",true);
			$("#VehicleLast4DigitalOfVin").attr("required",true);
		}
		function parseDate(str) {
			var d = str.split('/');
			return new Date(parseInt(d[2]),parseInt(d[0]), parseInt(d[1]));
		}
	$("#to").change(function(){
			var oneday = 24*60*60*1000;
			var start_date_array = $('#from').val().split('/');
			var end_date_array = $(this).val().split('/');
			var end_date = new Date(parseInt(end_date_array[2], 10),
									   parseInt(end_date_array[0], 10) - 1,
									   parseInt(end_date_array[1], 10));
			var start_date = new Date(parseInt(start_date_array[2], 10),
									   parseInt(start_date_array[0], 10) - 1,
									   parseInt(start_date_array[1], 10));
			var days = Math.floor((end_date.getTime() - start_date.getTime())/(oneday));
			console.log(days); 
			var permitCost=<?php echo $permitCost;?>;
			$('#days').val(days);
			$('#amount').val(days*permitCost); 
			var remainingCredits=<?php echo $remainingCredits;?>;
			
			if(days>=remainingCredits){
				$('#creditsUsed').val(remainingCredits);
				$('#discount').val(remainingCredits*permitCost);
				$('#netAmmount').val($('#amount').val()-$('#discount').val());
			}else{
				var creditsUsed=remainingCredits-days;
				$('#creditsUsed').val(days);
				$('#discount').val(days*permitCost);
				var amountPayable=$('#amount').val()-$('#discount').val();
				if(amountPayable<=0){
					$('#netAmmount').val('0');
				}else{
					$('#netAmmount').val(amountPayable);
				}
				
			}
			console.log(remainingCredits); 
	});
	if($("#to").val()!=""){
			var oneday = 24*60*60*1000;
			var start_date_array = $('#from').val().split('/');
			var end_date_array = $(this).val().split('/');
			var end_date = new Date(parseInt(end_date_array[2], 10),
									   parseInt(end_date_array[0], 10) - 1,
									   parseInt(end_date_array[1], 10));
			var start_date = new Date(parseInt(start_date_array[2], 10),
									   parseInt(start_date_array[0], 10) - 1,
									   parseInt(start_date_array[1], 10));
			var days = Math.floor((end_date.getTime() - start_date.getTime())/(oneday));
			console.log(days); 
			var permitCost=<?php echo $permitCost;?>;
			$('#days').val(days);
			$('#amount').val(days*permitCost); 
			var remainingCredits=<?php echo $remainingCredits;?>;
			
			console.log(remainingCredits);
			if(days>=remainingCredits){
				$('#creditsUsed').val(remainingCredits);
				$('#discount').val(remainingCredits*permitCost);
				$('#netAmmount').val($('#amount').val()-$('#discount').val());
			}else{
				var creditsUsed=remainingCredits-days;
				$('#creditsUsed').val(days);
				$('#discount').val(days*permitCost);
				var amountPayable=$('#amount').val()-$('#discount').val();
				if(amountPayable<=0){
					$('#netAmmount').val('0');
				}else{
					$('#netAmmount').val(amountPayable);
				}	
			}
	}
	 
});

</script>
<div class="page-content-wrapper">
		<div class="page-content" style="min-height:996px">
			
			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					Order View <small>view order details</small>
					</h3>
					<ul class="page-breadcrumb breadcrumb">

						<li>
							<i class="fa fa-home"></i> 
							<?php echo $this->Html->link(
                                                         'Home',
                                                          array(
                                                                'controller' => 'Users',
                                                                'action' => 'myAccount',
                                                                'full_base' => true
                                                                                                                          )
                                                          );?>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">Pass View</a>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
			<div id="flashMessages">
									<h2><?php  echo $this->Session->flash();?></h2>
			</div>
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- Begin: life time stats -->
					<div class="portlet">
						<div class="portlet-title">

						</div>
						<div class="portlet-body">
							<div class="tabbable">
								<ul class="nav nav-tabs nav-tabs-lg">
									<li class="active">
										<a data-toggle="tab" href="#tab_1">
										Details </a>
									</li>
								</ul>
								
								<div class="tab-content">
								
									<div id="tab_1" class="tab-pane active">
				<?php echo $this->Form->create('Vehicle',array('class'=>'form-horizontal'));?>
										<div class="row">
											<div class="col-md-6 col-sm-6">
											                       <div id="AllVehicle" class="portlet box yellow">
																			<div class="portlet-title">
																						<div class="caption">
																							<i class="fa fa-gift"></i>Guest Vehicles
																						</div>
																						<div class="tools">
																							<a class="collapse" href="javascript:;">
																							</a>
																						</div>
																			</div>
																			<div class="portlet-body form">
										
																	
											<div class="form-body">
                                                <?php
													 echo $this->Form->input('vehicle_id',array('type' => 'select',
                                                                            'div'=>array('class'=>'form-group'),
                                                                            'label'=>array('class'=>'col-md-3 control-label','text'=>'Select Existing Vehicle'),
																			'error'=>array('attributes'=>array('class'=>'model-error')),
                                                                            'options' => $customerOtherVehicles,
                                                                            'empty'=>'Select One',
                                                                            'between'=>'<div class="col-md-4">',
                                                                            'after'=>'</div>',
                                                                            'class'=>'form-control'));?>
													
													 <div class="note note-success">
                                                						<p>
                                                							 Add New Guest Vehicle
                                                						</p>
                                                   </div>
													<?php
													echo $this->Form->input('owner',array('div'=>array('class'=>'form-group'),'error'=>array('attributes'=>array('class'=>'model-error')),'label'=>array('class'=>'col-md-3 control-label','text'=>'Vehicle Name'),'between'=>'<div class="col-md-4">','after'=>'</div>','class'=>'form-control','placeholder'=>'Vehicle Name'));
												    
												    echo $this->Form->input('make',array('div'=>array('class'=>'form-group'),'error'=>array('attributes'=>array('class'=>'model-error')),'label'=>array('class'=>'col-md-3 control-label','text'=>'Make'),'between'=>'<div class="col-md-4">','after'=>'</div>','class'=>'form-control','placeholder'=>'Make'));

												    echo $this->Form->input('model',array('div'=>array('class'=>'form-group'),'error'=>array('attributes'=>array('class'=>'model-error')),'label'=>array('class'=>'col-md-3 control-label','text'=>'Model'),'between'=>'<div class="col-md-4">','after'=>'</div>','class'=>'form-control','placeholder'=>'Model'));

												    echo $this->Form->input('color',array('div'=>array('class'=>'form-group'),'error'=>array('attributes'=>array('class'=>'model-error')),'label'=>array('class'=>'col-md-3 control-label','text'=>'Color'),'between'=>'<div class="col-md-4">','after'=>'</div>','class'=>'form-control','placeholder'=>'Color'));

												    echo $this->Form->input('license_plate_number',array('div'=>array('class'=>'form-group'),'error'=>array('attributes'=>array('class'=>'model-error')),'label'=>array('class'=>'col-md-3 control-label','text'=>'License Plate Number'),'between'=>'<div class="col-md-4">','after'=>'</div>','class'=>'form-control','placeholder'=>'License Plate Number'));

												    //echo $this->Form->input('license_plate_state',array('div'=>array('class'=>'form-group'),'label'=>array('class'=>'col-md-3 control-label','text'=>'License Plate State'),'between'=>'<div class="col-md-4">','after'=>'</div>','class'=>'form-control','form-control','placeholder'=>'License Plate State'));
                                                    echo $this->Form->input('license_plate_state',array('type' => 'select',
                                                                            'div'=>array('class'=>'form-group'),
                                                                            'label'=>array('class'=>'col-md-3 control-label','text'=>'License Plate State'),
																			'error'=>array('attributes'=>array('class'=>'model-error')),
                                                                            'options' => $states,
                                                                            'empty'=>'State',
                                                                            'between'=>'<div class="col-md-4">',
                                                                            'after'=>'</div>',
                                                                            'class'=>'form-control'));

												    //echo $this->Form->input('reserved_space',array('div'=>array('class'=>'form-group'),'label'=>array('class'=>'col-md-3 control-label','text'=>'Reserved Space'),'between'=>'<div class="col-md-4">','after'=>'</div>','class'=>'form-control','placeholder'=>'Reserved Space'));

												    echo $this->Form->input('last_4_digital_of_vin',array('div'=>array('class'=>'form-group'),'error'=>array('attributes'=>array('class'=>'model-error')),'label'=>array('class'=>'col-md-3 control-label','text'=>'Last 4 Digital Of Vin'),'between'=>'<div class="col-md-4">','after'=>'</div>','class'=>'form-control','placeholder'=>'Last 4 Digital Of Vin'));
										        ?>
															
																			</div>
																		</div>
                                                                                                  
                                          											    </div>
																												<div class="well">
                                                                                                                    <div class="row">
                                                                                                                        <div class="col-md-6">
                                                                                                                             <div class="col-md-offset-3 col-md-9">
                                                                                                                                  <button class="btn green" type="submit">Proceed</button>
                                                                                                                             </div>
                                                                                                                             <?php echo $this->Form->end(); ?>
                                                                                                                        </div>
                                                                                                                    </div>
																												</div>

                                            </div>
											<div class="col-md-6 col-sm-12">
												<div class="portlet blue-hoki box">
													<div class="portlet-title">
														<div class="caption">
															<i class="fa fa-cogs"></i>New Guest Permit Details
														</div>

													</div>
													<div class="portlet-body">
														<div class="row static-info">
															<div class="col-md-5 name">
																From:
															</div>
															<div class="col-md-7 value">
																<?php   $dt = new DateTime();
																		$currentDate= $dt->format('m/d/Y');
																		echo $this->Form->input('from',array('required'=>true,'type'=>'text','div'=>false,
                                                								'label'=>false,
                                                								'disabled' => 'disabled',
                                                								'value'=>$currentDate,                                               					                                               								
                                                								'class'=>'form-control'));
																?>
															</div>
														</div>
														<div class="row static-info">
															<div class="col-md-5 name">
																To
															</div>
															<div class="col-md-7 value">
																<?php $tomorrow = new DateTime('tomorrow');
																
																  echo $this->Form->input('to',array('required'=>true,'type'=>'text','div'=>false,
                                                								'label'=>false,
                                                								'between'=>'<div class="input-group date date-picker" data-date-format="mm/dd/yyyy" data-date-start-date="'.$tomorrow->format('m/d/Y').'">',
                                                								'after'=>'<span class="input-group-btn"><button type="button" class="btn default"><i class="fa fa-calendar"></i></button></span></div>',
                                                								'class'=>'form-control'));
																?>
															</div>
														</div>
														<div class="row static-info">
															<div class="col-md-4 name">
																 Number Of Days:<br>
																<?php
																	echo $this->Form->input('days',array('required'=>true,'type'=>'text','div'=>false,
                                                								'label'=>false,
                                                								'disabled' => 'disabled',
                                                								'value'=>'',                                               					                                               								
                                                								'class'=>'form-control'));
																 ?>
															</div>
															<div class="col-md-4 name">
																 Cost Per Day:<br>
																 <?php
																	echo $this->Form->input('cost',array('required'=>true,'type'=>'text','div'=>false,
                                                								'label'=>false,
                                                								'disabled' => 'disabled',
                                                								'value'=>$permitCost,                                               					                                               								
                                                								'class'=>'form-control'));
																 ?>
															</div>
															<div class="col-md-4 name">
																 Amount:<br>
																  <?php
																	echo $this->Form->input('amount',array('required'=>true,'type'=>'text','div'=>false,
                                                								'label'=>false,
                                                								'disabled' => 'disabled',
                                                								'value'=>'',                                               					                                               								
                                                								'class'=>'form-control'));
																 ?>
															</div>
															
														</div>
														<div class="row static-info">
															<div class="col-md-4 name">
																 Using Credits:<br>
																<?php
																	echo $this->Form->input('creditsUsed',array('required'=>true,'type'=>'text','div'=>false,
                                                								'label'=>false,
                                                								'disabled' => 'disabled',
                                                								'value'=>'',                                               					                                               								
                                                								'class'=>'form-control'));
																 ?>
															</div>
															<div class="col-md-4 name">
																 Cost Per Day:<br>
																 <?php
																	echo $this->Form->input('cost',array('required'=>true,'type'=>'text','div'=>false,
                                                								'label'=>false,
                                                								'disabled' => 'disabled',
                                                								'value'=>$permitCost,                                               					                                               								
                                                								'class'=>'form-control'));
																 ?>
															</div>
															<div class="col-md-4 name">
																Discount:<br>
																  <?php
																	echo $this->Form->input('discount',array('required'=>true,'type'=>'text','div'=>false,
                                                								'label'=>false,
                                                								'disabled' => 'disabled',
                                                								'value'=>'',                                               					                                               								
                                                								'class'=>'form-control'));
																 ?>
															</div>
														</div>
														<div class="row static-info">
															<div class="col-md-5 name">
																Net Amount Payable:
															</div>
															<div class="col-md-7 value">
																 <?php 
																	echo $this->Form->input('netAmmount',array('required'=>true,'type'=>'text','div'=>false,
                                                								'label'=>false,
                                                								'disabled' => 'disabled',
                                                								'value'=>'',                                               					                                               								
                                                								'class'=>'form-control'));
																 ?>
															</div>
														</div>
													</div>
												</div>
												
												  <div class="portlet yellow-crusta box">
													<div class="portlet-title">
														<div class="caption">
															<i class="fa fa-cogs"></i>Guest Credit Details
														</div>
					
													</div>
													<div class="portlet-body">
														<div class="row static-info">
															<div class="col-md-5 name">
																Total Credits :
															</div>
															<div class="col-md-7 value">
																<?php echo $freeGuestCredits;?>
															</div>
														</div>
														<div class="row static-info">
															<div class="col-md-5 name">
																Remaining :
															</div>
															<div class="col-md-7 value">
																<?php echo $remainingCredits;?>
															</div>
														</div>
												       
													</div>
												
												</div>
												
											</div>
										</div>
                                         
										        <div class="row">
														<div class="col-md-6 col-sm-12">
														</div>
											
												</div>



                                                                											
											  </div>
									       </div>
										</div>


										</div>
									</div>
			<!-----------------------------TAB Finish------------------------------------------------------------------------------------------>
								</div>
							</div>
						</div>
					</div>
					<!-- End: life time stats -->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
