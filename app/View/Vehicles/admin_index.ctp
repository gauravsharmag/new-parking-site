<div class="page-content-wrapper">  
		<div class="page-content" style="min-height:1027px">
			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					Vehicles <small>All Vehicles</small>
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li>
							<i class="fa fa-home"></i>
							<a href="#">Home</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">Vehicles</a>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
			<div id="flashMessages">
									<h2><?php  echo $this->Session->flash();?></h2>
			</div>
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					
					<!-- Begin: life time stats -->
					<div class="portlet">
						<div class="portlet-title">
							<div class="caption">
								All Vehicles
							</div>
						</div>
						<div class="portlet-body">
							<div class="table-container">		
								<div class="table-scrollable">
                                              <table id="vehicle_list" class="table table-striped table-bordered table-hover">
                                            	 <thead>
                                            	   	<tr>
                                                       <th>Name</th>
                                                       <th>Property</th>
                                                       <th>Owner</th>
                                                       <th>Make</th>
                                                       <th>Model</th>
                                                       <th>Color</th>
                                                       <th>Plate Number</th>
                                                       <th>State</th> 
                                                       <th>Vin</th>
                                                       <th>Edit Details</th>
                                                    </tr>
                        						  </thead>
                                            	  <tbody>
                                                       <tr>
															<td colspan="10" class="dataTables_empty">Loading Data...</td>
														</tr>
                                            	   </tbody>
                                            	</table>
												
                                        </div>
							</div>
						</div>
					</div>
					<!-- End: life time stats -->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>

<?php echo $this->Html->script('//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js'); ?>
<?php echo $this->Html->css('//cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css'); ?>
<script type="text/javascript"> 
$(function(){
	$('#vehicle_list').dataTable({
		"bProcessing": false,
        "bServerSide": true,
        "sAjaxSource": "<?php echo $this->Html->Url(array('controller' => 'Vehicles', 'action' => 'get_vehicles')); ?>"
	});
	
});
</script>
