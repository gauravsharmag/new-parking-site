<div class="alert alert-danger">
				<strong>Selected Vehicle Details </strong>
 </div>

	<table class="table table-bordered table-hover table-responsive">		
		<tbody>
			<tr>
				<td> Vehicle Name : </td>
				<td> <?php echo $vehicleDetails['Vehicle']['owner'] ?> </td>
			</tr>
			<tr>
				<td> Make : </td>
				<td> <?php echo $vehicleDetails['Vehicle']['make'] ?> </td>
			</tr>
			<tr>
				<td> Model : </td>
				<td> <?php echo $vehicleDetails['Vehicle']['model'] ?> </td>
			</tr>
			<tr>
				<td> Color : </td>
				<td> <?php echo $vehicleDetails['Vehicle']['color'] ?> </td>
			</tr>
			<tr>
				<td> Plate Number : </td>
				<td> <?php echo $vehicleDetails['Vehicle']['license_plate_number'] ?> </td>
			</tr>
			<tr>
				<td> State : </td>
				<td> <?php echo $vehicleDetails['Vehicle']['license_plate_state'] ?> </td>
			</tr>
			<tr>
				<td> VIN : </td>
				<td> <?php echo $vehicleDetails['Vehicle']['last_4_digital_of_vin'] ?> </td>
			</tr>
		</tbody>
	</table>

