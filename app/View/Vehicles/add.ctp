<?php echo $this->Html->script('jquery.min'); ?>
<script>
$(document).ready(function(){
 //$("#AddNew").show();
 $("#AllVehicle").hide();

  $("#ListAll").click(function(){
    $("#AllVehicle").slideToggle();
  });
});
</script>
<div class="page-content-wrapper">
		<div class="page-content" style="min-height:1089px">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="portlet-config" class="modal fade">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button aria-hidden="true" data-dismiss="modal" class="close" type="button"></button>
							<h4 class="modal-title">Modal title</h4>
						</div>
						<div class="modal-body">
							 Widget settings form goes here
						</div>
						<div class="modal-footer">
							<button class="btn blue" type="button">Save changes</button>
							<button data-dismiss="modal" class="btn default" type="button">Close</button>
						</div>
					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
			<!-- /.modal -->
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN STYLE CUSTOMIZER -->

			<!-- END STYLE CUSTOMIZER -->
			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					Add Vehicle <small></small>
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li class="btn-group">
							<button class="btn btn-primary" id="ListAll">List Vehicles
							</button>
						</li>
						<li>
							<i class="fa fa-home"></i>
							<a href="index.html">Home</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">Add Vehicle</a>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">

				<div class="col-md-12">
				     <div id="flashMessages">
						<h2><?php  echo $this->Session->flash();?></h2>
					</div>
					<div class="tabbable tabbable-custom boxless tabbable-reversed">

						<div class="tab-content">
							<div id="tab_0" class="tab-pane active">
								<div id="AllVehicle" class="portlet box yellow" style="display:none;">
									<div class="portlet-title">
										<div class="caption">
											<i class="fa fa-gift"></i>Registered Vehicles
										</div>
										<div class="tools">
											<a class="collapse" href="javascript:;">
											</a>
										</div>
									</div>
									<div class="portlet-body form">
										
										<table class="table table-bordered table-striped table-condensed flip-content">
											<thead>
												<tr>
													<th>#</th>
													<th>Name For Vehicle</th>
													<th>Make</th>
													<th>Model</th>
													<th>Color</th>
													<th>Number</th>
													<th>State</th>
													<th>VIN</th>
													<th>Select</th>
												</tr>
											</thead>
											<tbody>
												<?php $i=1;foreach($customerOtherVehicles as $vehicle){?>
													<tr>
														<td><?php echo $i++;?></td>
														<td><?php echo $vehicle['Vehicle']['owner'];?></td>
														<td><?php echo $vehicle['Vehicle']['make'];?></td>
														<td><?php echo $vehicle['Vehicle']['model'];?></td>
														<td><?php echo $vehicle['Vehicle']['color'];?></td>
														<td><?php echo $vehicle['Vehicle']['license_plate_number'];?></td>
														<td><?php echo $vehicle['Vehicle']['license_plate_state'];?></td>
														<td><?php echo $vehicle['Vehicle']['last_4_digital_of_vin'];?></td>
														<td><?php echo $this->Html->link('Select',array('action'=>'addSelectedVehicle',$customerPass,$vehicle['Vehicle']['id']));?></td>
													</tr>
												<?php }?>
											</tbody>
										</table>
										</form>
									</div>
								</div>

								<div id="AddNew" class="portlet box green">
									<div class="portlet-title">
										<div class="caption">
											<i class="fa fa-gift"></i>Add Vehicle Details
										</div>
										<div class="tools">
											<a class="collapse" href="javascript:;">
											</a>

										</div>
									</div>
									<div class="portlet-body form">

										<!-- BEGIN FORM-->
										<?php echo $this->Form->create('Vehicle',array('class'=>'form-horizontal'));?>
											<div class="form-body">
                                                <?php
													echo $this->Form->input('owner',array('div'=>array('class'=>'form-group'),'error'=>array('attributes'=>array('class'=>'model-error')),'label'=>array('class'=>'col-md-3 control-label','text'=>'Name For Vehicle'),'between'=>'<div class="col-md-4">','after'=>'</div>','class'=>'form-control','placeholder'=>'Vehicle Name'));
												    echo $this->Form->input('make',array('div'=>array('class'=>'form-group'),'error'=>array('attributes'=>array('class'=>'model-error')),'label'=>array('class'=>'col-md-3 control-label','text'=>'Make'),'between'=>'<div class="col-md-4">','after'=>'</div>','class'=>'form-control','placeholder'=>'Make'));

												    echo $this->Form->input('model',array('div'=>array('class'=>'form-group'),'error'=>array('attributes'=>array('class'=>'model-error')),'label'=>array('class'=>'col-md-3 control-label','text'=>'Model'),'between'=>'<div class="col-md-4">','after'=>'</div>','class'=>'form-control','placeholder'=>'Model'));

												    echo $this->Form->input('color',array('div'=>array('class'=>'form-group'),'error'=>array('attributes'=>array('class'=>'model-error')),'label'=>array('class'=>'col-md-3 control-label','text'=>'Color'),'between'=>'<div class="col-md-4">','after'=>'</div>','class'=>'form-control','placeholder'=>'Color'));

												    echo $this->Form->input('license_plate_number',array('div'=>array('class'=>'form-group'),'error'=>array('attributes'=>array('class'=>'model-error')),'label'=>array('class'=>'col-md-3 control-label','text'=>'License Plate Number'),'between'=>'<div class="col-md-4">','after'=>'</div>','class'=>'form-control','placeholder'=>'License Plate Number'));

												   
                                                    echo $this->Form->input('license_plate_state',array('type' => 'select',
                                                                            'div'=>array('class'=>'form-group'),
                                                                            'label'=>array('class'=>'col-md-3 control-label','text'=>'License Plate State'),
																			'error'=>array('attributes'=>array('class'=>'model-error')),
                                                                            'options' => $states,
                                                                            'empty'=>'State',
                                                                            'between'=>'<div class="col-md-4">',
                                                                            'after'=>'</div>',
                                                                            'class'=>'form-control'));

												    echo $this->Form->input('last_4_digital_of_vin',array('div'=>array('class'=>'form-group'),'error'=>array('attributes'=>array('class'=>'model-error')),'label'=>array('class'=>'col-md-3 control-label','text'=>'Last 4 Digital Of Vin'),'between'=>'<div class="col-md-4">','after'=>'</div>','class'=>'form-control','placeholder'=>'Last 4 Digital Of Vin'));
										       
													echo $this->Form->input('assigned_location',array('div'=>array('class'=>'form-group'),'error'=>array('attributes'=>array('class'=>'model-error')),'label'=>array('class'=>'col-md-3 control-label','text'=>'Assigned Location'),'between'=>'<div class="col-md-4">','after'=>'</div>','class'=>'form-control','placeholder'=>'Assigned Location'));
										        ?>
											</div>
											<div class="form-actions fluid">
												<div class="col-md-offset-3 col-md-9">
													<button class="btn blue" type="submit">Submit</button>
												</div>
												 <?php echo $this->Form->end(); ?>
											</div>
										</form>
										<!-- END FORM-->
									</div>
								</div>

							</div>

						</div>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
