    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                    <h3 class="page-title">
					<?php echo $this->Session->read('PropertyName');?><small> Pass Expiry Details</small>
                    </h3>
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <i class="fa fa-home"></i>
                           <?php
										echo $this->Html->link('Home',array(
                                          'controller' => 'Users',
                                          'action' => 'manager_home_page',
                                          'full_base' => true
                                      ));                                
                               ?>
                            <i class="fa fa-angle-right"></i>
                        </li>
                        <li>
                           Pass Expirations
                        </li>
                        <li class="pull-right">
                            <div id="dashboard-report-range" class="dashboard-date-range tooltips" data-placement="top" data-original-title="Change dashboard date range">
                                <i class="icon-calendar"></i>
                                <span></span>
                                <i class="fa fa-angle-down"></i>
                            </div>
                        </li>
                    </ul>
                    <!-- END PAGE TITLE & BREADCRUMB-->
                </div>
            </div> 
            <div class="row">
                    <div class="portlet box red">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-coffee"></i>Passes Expiring in Next 30 Days (
                                <?php 
                                    echo $expiringNextMonth;
                                ?> )
                            </div>
                            <div class="tools">
                                <a href="javascript:;" class="collapse">
                                </a>
                                
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="table-scrollable">
                             <table  id="result" class="table table-bordered table-hover">
                                <thead>
                                    <tr>                
                                       <th>User Name</th>
                                       <th>Property</th>
                                       <th>Pass</th>
                                       <th>Parking Package</th>
                                       <th>Vehicle Details</th>     
                                        <th>RFID</th>                  
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php  if(empty($expiringNextMonthDetails)){
												echo	"<tr><td colspan='7'> No Results Found</td></tr>";
											}
											else{
												foreach($expiringNextMonthDetails as $pass){ ?>
												<tr>
													<td>
														<a href="/users/manager_view_user_details/<?php echo $pass['User']['id']?>">
															<?php echo $pass['User']['username']?>
														</a>
													</td>
													<td><?php echo $pass['CustomerPass']['property_id']?></td>
													<td><?php echo $pass['CustomerPass']['pass_id']."<br>".
																   date('m/d/Y H:i:s',strtotime($pass['CustomerPass']['pass_valid_upto']));
														?>
													</td>
													<td><?php echo $pass['CustomerPass']['package_id']."<br>";
															  if($pass['CustomerPass']['membership_vaild_upto']!="NA"){
																echo date('m/d/Y H:i:s',strtotime($pass['CustomerPass']['membership_vaild_upto']));
															  }else{echo $pass['CustomerPass']['membership_vaild_upto'];}
													
														?>
													</td>
													<td><?php if(!is_null($pass['Vehicle']['id'])){
																echo "Make: ".$pass['Vehicle']['make']." Model: ".$pass['Vehicle']['model']."<br>".
																	 "Plate No.: ".$pass['Vehicle']['license_plate_number']." ".$pass['Vehicle']['license_plate_state'];
															  }else{
																echo "No Vehicle Assigned";
															  }
														?>
													</td>	
													<td><?php 
															echo is_null($pass['CustomerPass']['RFID_tag_number'])? "No RFID": $pass['CustomerPass']['RFID_tag_number'];
														?>
													</td>
												</tr>
                                    <?php 
                                    }} 
                                    ?>
                                </tbody>
                            </table>
                       </div>
                    </div>    
                </div>
            </div>
        </div>
    </div>
<?php if(!empty($expiringNextMonthDetails)){?>
<script src="//cdn.datatables.net/1.10.2/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function() {
	$('#result').dataTable();
});
</script>
<?php }?>
