<div class="row">
	<?php echo $this->Form->create('CustomerPass',array('class'=>'form-horizontal'));// debug($roles);?>
		<div class="form-body">
		<?php
			echo $this->Form->hidden('id',array('value'=>$customerPass['CustomerPass']['id']));
			echo $this->Form->input('pass_valid_upto',array(
													'error'=>array(
																	'attributes'=>array('class'=>'model-error')
																	),
													 'required'=>true,
													 'div'=>array('class'=>'form-group'),
													 'label'=>array('class'=>'col-md-3 control-label','text'=>'Pass Validity'),
													 'between'=>'<div class="col-md-4">',
													 'after'=>'</div>',
													 'type'=>'text',
													 
													 'class'=>'form-control date-picker',
													 'value'=>date('m/d/Y',strtotime($customerPass['CustomerPass']['pass_valid_upto'])),
													 'placeholder'=>'Pass Validity')
													);
			if($customerPass['CustomerPass']['membership_vaild_upto']){
				echo $this->Form->input('membership_vaild_upto',array(
													'error'=>array(
																	'attributes'=>array('class'=>'model-error')
																	),
													 'required'=>true,
													 'div'=>array('class'=>'form-group'),
													 'label'=>array('class'=>'col-md-3 control-label','text'=>'Permit Validity'),
													 'between'=>'<div class="col-md-4">',
													 'after'=>'</div>',
													 'type'=>'text',
													 
													 'class'=>'form-control date-picker',
													 'value'=>date('m/d/Y',strtotime($customerPass['CustomerPass']['membership_vaild_upto'])),
													 'placeholder'=>'Pass Validity')
													);
		   }else{
			 echo $this->Form->hidden('membership_vaild_upto',array('value'=>$customerPass['CustomerPass']['membership_vaild_upto']));
		   }
		?>
		</div>
		 <div class="form-actions fluid">   
			<div class="col-md-offset-3 col-md-9">
				<button type="submit" class="btn blue">Approve Pass</button>
				<!--<button type="button" class="btn default">Cancel</button>-->
			</div>
		</div>
	 <?php echo $this->Form->end(); ?>   
</div>
<script type="text/javascript">
  //$("#CustomerPassPassValidUpto").mask("99/99/9999 99:99:99");
  $("#CustomerPassPassValidUpto").datepicker({format: 'mm/dd/yyyy'});
  <?php if($customerPass['CustomerPass']['membership_vaild_upto']){ ?>
	//$("#CustomerPassMembershipVaildUpto").mask("99/99/9999 99:99:99");
	$("#CustomerPassMembershipVaildUpto").datepicker({format: 'mm/dd/yyyy'});
 <?php } ?>
</script>
