<div class="page-content-wrapper">
		<div class="page-content" style="min-height:1027px">
			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					Passes <small>Property Wide Passes Status</small>
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li>
							<i class="fa fa-home"></i>
							<a href="#">Home</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">Property Wide Pass Status</a>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
			<div id="flashMessages">
									<h2><?php  echo $this->Session->flash();?></h2>
			</div>
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					
					<!-- Begin: life time stats -->
					<div class="portlet">
						<div class="portlet-title">
							
						</div>
						<div class="portlet-body">
						<div class="row">
                             <div class="col-md-12">
                                <?php echo $this->Form->create('',array('class'=>'form-inline'));?>
                                    <div class="form-group">
                                         <?php
                                            echo $this->Form->input('current_property',array( 'type'=>'select',
																										'options' => $property_list,
																										'selected'=>$selectedId,
																										'div'=>false,
																										'label'=>false,
																										'class'=>'form-control',
																										'id'=>'current_property' 
																									));
                                         ?>
                                    </div>
                                     <div class="form-group">
                                         <?php
                                           echo $this->Form->input('current_pass',array( 'type'=>'select',
																										'options' => $passes_list,
																										'empty'=>'Select Pass',
																										'div'=>false,
																										'label'=>false,
																										'placeholder'=>"",
																										
																										'class'=>'form-control',
																										'id'=>'current_pass' 
																									));
                                         ?>
                                    </div>
                                    <div class="form-group">
                                        <div data-date-format="mm/dd/yyyy" data-date="08/15/2014" class="input-group input-large date-picker input-daterange col-md-12">
                                            <input type="text" name="from" id="from" class="form-control">
                                            <span class="input-group-addon">
                                                <?php echo (__('To')); ?> </span>
                                            <input type="text" name="to" id="to" class="form-control">
                                        </div>
                                    </div>
                                    <input type="button" class="btn red" value="Go" name="submit" id="gobtn">
                                <?php echo $this->Form->end();?>
                            </div>
                        </div>
						<hr>
							<div class="note note-warning" id="pass_intro" style="display: none">
									<p id="pass_name">Select Pass To View Details</p><br>
									Total Passes Purchased In Given Interval: <label id="totalPasses"></label>
							</div>
							<div class="table-container" id="table_list" style="display: none">		
								<div class="table-responsive">
                                              <table id="view_rfid_list" class="table table-striped table-bordered table-hover">
                                            	 <thead>
                                            	   	<tr>
                                                       <th>User</th>
                                                       <th>Property</th>
                                                       <th>Vehicle</th>
                                                       <th>Vehicle Number</th>
                                                       <th>Pass Validity</th>
                                                       <th>Permit Validity</th>
                                                       <th>Assigned RFID</th> 
                                                       <?php if(AuthComponent::user('role_id')==1) { ?>
                                                       <th>Edit RFID</th> 
														<?php }?>
                                                    </tr>
                        						  </thead>
                                            	  <tbody>
                                                       <tr>
															<td colspan="8" class="dataTables_empty">Loading Data......</td>
														</tr>
                                            	   </tbody>
                                            	</table>
												<?php //echo $this->Js->writeBuffer(); ?>
                                        </div>
							</div>
						</div>
					</div>
					<!-- End: life time stats -->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
<?php echo $this->Html->script('block.js'); ?>
<?php echo $this->Html->script('//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js'); ?>
<?php echo $this->Html->css('//cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css'); ?>
<?php echo $this->Html->script('//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js'); ?> 
<?php echo $this->Html->script('/assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js'); ?>
<?php echo $this->Html->script('/assets/admin/pages/scripts/components-pickers.js');   ?>
<?php echo $this->Html->css('/assets/global/plugins/bootstrap-datepicker/css/datepicker3.css'); ?>
<script>
    jQuery(document).ready(function() {     
      ComponentsPickers.init(); 
    });
</script>

<script type="text/javascript">
jQuery(document).ready(function ($) {
	$('#current_property').change(function () {
			$('#current_pass').empty();
			 $.blockUI({    
							message: '<h4>Loading..</h4>',  
							css: { 
									border: 'none', 
									padding: '8px', 
									backgroundColor: '#000', 
									'-webkit-border-radius': '10px', 
									'-moz-border-radius': '10px', 
									opacity: .5, 
                                    width:'35%',
									color: '#fff' 
						}
				  });
			$.ajax({
                type: "GET",
                url: "/admin/Passes/get_passes_list/"+jQuery(this).val()+"",
                dataType: "HTML",
                success: function (response) {
					jQuery('#current_pass').append("<option value=''>Select Pass</option>");
                    var arr=$.parseJSON(response);
                    jQuery.each(arr, function (i, text) {
                        jQuery('#current_pass').append(jQuery('<option></option>').val(i).html(text));
                    });
                    $.unblockUI();
                }
            }); 
        });
	$("#gobtn").click(function () {
		var currentPass = $('#current_pass').val();
		if(currentPass){
				if($('#from').val()){
					$.blockUI({    
							message: '<h4>Loading..</h4>',  
							css: { 
									border: 'none', 
									padding: '8px', 
									backgroundColor: '#000', 
									'-webkit-border-radius': '10px', 
									'-moz-border-radius': '10px', 
									opacity: .5, 
                                    width:'35%',
									color: '#fff' 
						}
				  });
					$('#helpBlock').html("Select Date");
					$('#helpBlock').css('color','grey');
					var toDate="";
					if($('#to').val()){
						toDate=$('#to').val();
					}else{
						var dt = new Date();
						$('#to').val((dt.getMonth()+1) + '/' + (dt.getDate()-1) + '/' +  dt.getFullYear());
						toDate=(dt.getMonth()+1) + '/' + (dt.getDate()-1) + '/' +  dt.getFullYear();
					}
					var data = {};
					data[0]=$('#from').val();
					data[1]=toDate;
					data[2]=$('#current_pass').val();
					var dataSend = JSON.stringify(data);
					$.ajax({
							url: "/admin/CustomerPasses/get_passes_detail_interval?fromDate="+$('#from').val()+"&toDate="+toDate+"&pass="+$('#current_pass').val(),
							//type: "POST",
							//data: dataSend,  
							dataType: "json",
							success: function (response) {
								$('#totalPasses').html(response.totalPass);
								$('#pass_name').html(response.passName);
								dataToSend= JSON.stringify(response.condition);
								$('#table_list').show();
								$(function(){
									$('#view_rfid_list').dataTable({
											"bProcessing": false,
											"bServerSide": true,
											"bDestroy": true,
											"sAjaxSource": "/admin/CustomerPasses/passes_interval/"+dataToSend+""
									});
								});
								$('#pass_intro').show();
								
								$.unblockUI();
							}
					});
				}else{
					$('#helpBlock').html("Select From date");
					$('#helpBlock').css('color','red')
				}	
		}else{
			alert('Select Pass');
		}
	
	});
	  
});
</script>
