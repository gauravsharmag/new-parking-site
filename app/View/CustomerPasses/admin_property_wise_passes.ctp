
<div class="page-content-wrapper">
		<div class="page-content" style="min-height:1027px">
			
			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					Passes <small>Property Wide Passes Status</small>
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li>
							<i class="fa fa-home"></i>
							<a href="#">Home</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">Property Wide Pass Status</a>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
			<div id="flashMessages">
									<h2><?php  echo $this->Session->flash();?></h2>
			</div>
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					
					<!-- Begin: life time stats -->
					<div class="portlet">
						<div class="portlet-title">
							
						</div>
						<div class="portlet-body">
							<div class="row">
									<?php  
												echo $this->Form->input('current_property',array( 'type'=>'select',
																										'options' => $property_list,
																										'selected'=>$selectedId,
																										//'div'=>array('class'=>'form-group'),
																										'label'=>false,
																										'between'=>'<div class="col-md-3">',
																										'after'=>'</div>',
																										'class'=>'form-control' 
																									));
												echo $this->Form->input('current_pass',array( 'type'=>'select',
																										'options' => $passes_list,
																										//'selected'=>$selectedPassId,
																										'empty'=>'Select Pass',
																										//'div'=>array('class'=>'form-group'),
																										'label'=>false,
																										'placeholder'=>"",
																										'between'=>'<div class="col-md-3">',
																										'after'=>'</div>',
																										'class'=>'form-control' 
																									));
									?>
							</div>
							<hr>
							<div class="note note-warning">
									<p id="pass_name">Select Pass To View Details</p>
							</div>
							<div class="table-container" id="pass_details" style="display: none">		
								<div class="table-responsive">
                                              <table class="table table-striped table-bordered table-hover">
                                            	 <thead>
                                            	   	<tr>
                                                       <th>Pass And Package Both Active</th>
                                                       <th>Valid Pass</th>
                                                       <th>Expired Pass</th>
                                                       <th>Active Vehicles</th>
                                                       <th>Active Pass But Vehicle Not assigned</th>
                                                       <th>Total Transaction Amounts</th>
                                                    </tr>
                        						  </thead>
                                            	  <tbody>
                                                       <tr> 
															<td><label id="active"></label></td>
															<td>Total Passes Purchased: <label id="totalPasses"></label><br>
																Package Expired: <label id="validPackageExpired"></label><br>
																Package Not Bought: <label id="validPackageNotBought"></label>
															</td>
															<td>Total Expired: <label id="expired"></label><br>
																Expiring/Expired Today: <label id="exp_today"></label><br>
																Expired Last Month: <label id="exp_last_month"></label><br>
																Expiring In Upcoming Month: <label id="exp_next_month"></label>
															</td>
															<td><label id="active_vehicles"></label></td>
															<td><label id="unassigned_vehicle"></label></td>
															<td> 
																<!--<b>Total Transaction Amount Till Friday, 21 November 2014= $ <label id="totalAmountTransaction"></label></b>-->
																<br>
																
																 Pass Cost Amount = $ <label id="totalPassCost"></label><br>
																 Pass Deposit Amount = $ <label id="totalPassDeposit"></label><br>
																 Pass Renewal Amount = $ <label id="totalPassRenewal"></label><br>
																 <br>
																   <b>Total Pass Amount = $ <label id="totalPassAmount"></label></b><br>
																   <b>Total Package Cost Amount = $ <label id="totalPackageAmount"></label></b><br>
																   <b>Total Amount = $ <label id="totalAmount"></label></b>
																   
																   
															</td>
														</tr>
                                            	   </tbody>
                                            	</table>
												<?php //echo $this->Js->writeBuffer(); ?>
                                        </div>
							</div>
							
							<div class="table-container" id="table_list" style="display: none">	
								<div class="note note-success">
									<p id="pass_name">Pass Listing</p>
								</div>	
								<div class="table-responsive">
                                              <table id="view_rfid_list" >
                                            	 <thead>
                                            	   	<tr>
                                                       <th>User</th>
                                                       <th>Property</th>
                                                       <th>Vehicle</th>
                                                       <th>Vehicle Number</th>
                                                       <th>Pass Validity</th>
                                                       <th>Permit Validity</th>
                                                       <th>Assigned RFID</th> 
                                                       <th>Edit RFID</th> 
                                                       <th>View Guest Pass Detail</th> 
                                                    </tr>
                        						  </thead>
                                            	  <tbody>
                                                       <tr>
															<td colspan="9" class="dataTables_empty">Loading Data......</td>
														</tr>
                                            	   </tbody>
                                            	</table>
												<?php //echo $this->Js->writeBuffer(); ?>
                                        </div>
							</div>
						</div>
					</div>
					<!-- End: life time stats -->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>

<?php echo $this->Html->script('block.js'); 
	  //echo $this->Html->script('pgwmodal.min.js'); 
?>

<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Guest Pass Activation List</h4>
      </div>
      <div class="modal-body">
			<div class="note note-warning">
				<p id="pass_name">Guest Pass Details</p>
			</div>
			<div class="table-scrollable">
				<table id="userGuestPass" class="table table-striped table-bordered table-hover" >
					<thead>
						<tr>
							<th>By User</th>
							<th>Property</th>
							<th>Vehicle</th>
							<th>Days</th>
							<th>Paid</th>
							
							<th>Amount</th>
							<th>From</th> 
							<th>To</th> 
						</tr>
					</thead>
					<tbody>
						<tr>
							<td colspan="5" class="dataTables_empty">Loading Data......</td>
						</tr>
					</tbody>
				</table>
			</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<?php echo $this->Html->script('//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js'); ?>
<?php echo $this->Html->css('//cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css'); ?>
<script type="text/javascript">

	    $('#current_property').change(function () {
			$('#current_pass').empty();
			$.blockUI({    
							message: '<h4>Loading..</h4>',  
							css: { 
									border: 'none', 
									padding: '8px', 
									backgroundColor: '#000', 
									'-webkit-border-radius': '10px', 
									'-moz-border-radius': '10px', 
									opacity: .5, 
                                    width:'35%',
									color: '#fff' 
						}
				  });
			$.ajax({
                type: "GET",
                url: "/admin/Passes/get_passes_list/"+jQuery(this).val()+"",
                dataType: "HTML",
                success: function (response) {
					jQuery('#current_pass').append("<option value=''>Select Pass</option>");
                    var arr=$.parseJSON(response);
                    jQuery.each(arr, function (i, text) {
                        jQuery('#current_pass').append(jQuery('<option></option>').val(i).html(text));
                    });
                    $.unblockUI();
                }
            }); 
        });
         $('#current_pass').change(function () {
			if($(this).val()!=''){
			 var property_id=$('#current_property').val();
			 $.blockUI({    
							message: '<h4>Loading..</h4>',  
							css: { 
									border: 'none', 
									padding: '8px', 
									backgroundColor: '#000', 
									'-webkit-border-radius': '10px', 
									'-moz-border-radius': '10px', 
									opacity: .5, 
                                    width:'35%',
									color: '#fff' 
						}
				  });
			$.ajax({
                type: "GET",
                url: "/admin/CustomerPasses/get_passes_detail/"+jQuery(this).val()+"/"+$('#current_property').val()+"",
                dataType: "HTML",
                success: function (response) {
					var arr=$.parseJSON(response);
					$('#totalPasses').html(arr.totalPass);
					$('#active').html(arr.activePass);
					$('#validPackageExpired').html(arr.validPassPackageExpired);
					$('#validPackageNotBought').html(arr.validPassPackageNotBought);
					$('#expired').html(arr.expiredPass);
					$('#active_vehicles').html(arr.activeVehicles);
					$('#unassigned_vehicle').html(arr.vehicleNotAssigned);
					$('#pass_name').html(arr.passName);
					$('#exp_today').html(arr.expiringToday);
					$('#exp_last_month').html(arr.expiredLastMonth);
					$('#exp_next_month').html(arr.expiringNextMonth);
					$('#totalPackageAmount').html(arr.totalPackageAmount);
					$('#totalPassCost').html(arr.totalPassCost);
					$('#totalPassDeposit').html(arr.totalPassDeposit);
					$('#totalPassRenewal').html(arr.totalPassRenewal);
					$('#totalPassAmount').html(arr.totalPassAmount);
					$('#totalAmountTransaction').html(arr.totalAmountTransaction);
					$('#totalAmount').html(parseInt(arr.totalPackageAmount)+parseInt(arr.totalPassAmount));
					 $('#guestContent').hide();
							var table=	$('#view_rfid_list').dataTable({
											"bProcessing": false,
											"bServerSide": true,
											"bDestroy": true,
											"sAjaxSource": "/admin/CustomerPasses/passes_property_wise/"+$('#current_pass').val()+""
								});
	
					
					
					if(arr.isGuest){
							 table.fnSetColumnVis( 8, true );
					}else{
						$('#guestContent').hide();
						table.fnSetColumnVis( 8, false );
					}
					$('#pass_details').show();
					$('#table_list').show();
					 $.unblockUI(); 
                }
            }); 
           }
		});
   

function showModal(e){
	  
	   $.blockUI({    
							message: '<h4>Loading..</h4>',  
							css: { 
									border: 'none', 
									padding: '8px', 
									backgroundColor: '#000', 
									'-webkit-border-radius': '10px', 
									'-moz-border-radius': '10px', 
									opacity: .5, 
                                    width:'35%',
									color: '#fff' 
						}
				  }); 
			$('#userGuestPass').dataTable({
												"bProcessing": false,
												"bServerSide": true,
												"bDestroy": true,
												"sAjaxSource": "/admin/UserGuestPasses/user_guest_pass/"+e.id+"",
												//"order": [[ 7, "desc" ]],
												"fnInitComplete":function(settings, json){
													 $.unblockUI();
													 $('#myModal').modal('show');
												}
			});
	}
</script>

