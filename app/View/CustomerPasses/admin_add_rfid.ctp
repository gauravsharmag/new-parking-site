<div class="page-content-wrapper">
		<div class="page-content" style="min-height:1027px">
			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					RFID <small>Add RFID Tags</small>
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li>
							<i class="fa fa-home"></i>
							<a href="#">Home</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">RFID Tags</a>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
			<div id="flashMessages">
									<h2><?php  echo $this->Session->flash();?></h2>
			</div>
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					
					<!-- Begin: life time stats -->
					<div class="portlet box red">
						<div class="portlet-title">
							<div class="caption">
								Assign RFID Tags
							</div>
							
						</div>
						<div class="portlet-body">
							<div class="row">
								<div class="col-md-12">	
								 <?php echo $this->Form->create('',array('class'=>'form-inline'));?>
                                    <div class="form-group">
                                         <?php
                                            echo $this->Form->input('current_property', array('type' => 'select',
                                                'options' => $property_list,
                                                'empty' => 'All Property',
                                                'div' => false,
                                                'label' => "",
                                                'id'=>'current_property',
                                                'placeholder' => "",
                                                'class' => 'form-control'
                                            ));
                                         ?>
                                    </div>
                                    <div class="form-group">
                                         <?php
                                   			 echo $this->Form->input('manager_approved',array( 'type'=>'select',
																										'options' => array('Not Approved','Approved'),
																										'empty'=>'All Type',
																										'div'=>false,
																										'label'=>false,
																										'id'=>'manager_approved',
																										'class'=>'form-control' 
																									));
                                   		?>
                                    </div>
                                   
                                    <input type="button" class="btn red" value="Go" name="submit" id="gobtn">
                               		<?php echo $this->Form->end();?>
                               	</div>
							</div>
							<hr>
							
							<div class="row">	
								<div class="col-md-12">	
									<div class="table-scrollable">
										<table id="rfid_list" class="table table-striped table-bordered table-hover">
										  <thead>
											<tr>
											   <th>User</th>
											   <th>Property</th>
											   <th>Pass</th>
											   <th>Vehicle</th>
											   <th>Vehicle Number</th>
											   <th>Pass Validity</th>
											   <th>Permit Validity</th>
											   <th>Approved</th> 
											   <th>Action</th> 
											</tr>
										  </thead>
										  <tbody>
											   <tr>
													<td colspan="9" class="dataTables_empty">...LOADING....</td> 
												</tr>
										   </tbody>
										</table>
									</div> 
								</div>
							</div>
						</div>
					</div>
					<!-- End: life time stats -->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
<?php echo $this->Html->script('/assets/global/plugins/bootstrap-daterangepicker/moment.min.js'); ?>   
<?php echo $this->Html->script('//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js'); ?>
<?php echo $this->Html->css('//cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css'); ?>
<script type="text/javascript">
function createTable(){
	$('#rfid_list').dataTable({
		 "bDestroy": true,
		"columnDefs": [
			{	"targets": 0,
				"render": function ( data, type, row ) {
					return "<b>Username: </b> <a href='/admin/users/view_user_details/"+row.user_id+"'>"+row.username+"</a><br> <b>Name: </b>"+row.first_name+" "+row.last_name+"<br> <b>Address: </b>"+row.address_line_1+" "+row.address_line_2+"<br> "+row.city+" "+row.state+"<br> "+row.zip+"<br> <b>Mobile: </b>"+row.phone+"<br> <b>Email: </b>"+row.email;
				}
			},
			{
				"targets": 3,
				"data": function ( row, type, val, meta ) {
							if(row.last_4_digital_of_vin){
								return "<b>Owner: </b>"+row.owner+"<br> <b>Make: </b>"+row.make+"<br> <b>Model: </b>"+row.model+"<br><b>Color: </b>"+row.color+"<br><b>VIN: </b> "+row.last_4_digital_of_vin;
							}
							return "";
				}
			},
			{
				"targets": 4,
				"data": function ( row, type, val, meta ) {
							if(row.last_4_digital_of_vin){
								return "<b>Plate Number: </b>"+row.license_plate_number+"<br> <b>State: </b>"+row.license_plate_state;
							}
							return "";
				}
			},
			{
				"targets": 5,
				"data": function ( row, type, val, meta ) {
							var date = moment(row.pass_valid_upto);
							var newDate = date.format("DD MMM YYYY HH:mm:ss"); 
							return newDate;	
				}
			},
			{
				"targets": 6,
				"data": function ( row, type, val, meta ) {
							if (row.membership_vaild_upto) {
								var date = moment(row.membership_vaild_upto);
								var newDate = date.format("DD MMM YYYY HH:mm:ss"); 
								return newDate;
							}else{
								return "";
							}
				}
			},
			{
				"targets": 7,
				"data": function ( row, type, val, meta ) {
							if(row.approved==1){
								return '<span class="label label-sm label-info">YES</span>';
							}
							return '<span class="label label-sm label-danger">NO</span>';
						}
			},
			{   "targets":8,
				"render": function ( data, type, row ) {
						  returnStr="<a class='btn green btn-xs green-stripe' href='/admin/CustomerPasses/edit/"+row.id+"'>Assign RFID</a>";
						  returnStr+="<a class='btn red btn-xs red-stripe' href='/admin/CustomerPasses/user_passes/"+row.user_id+"'>All Passes</a>";
						  return returnStr;
				}
			},
			
			
		],
        "bServerSide": true,
        "sAjaxSource": "<?php echo $this->Html->Url(array('controller' => 'CustomerPasses', 'action' => 'pass_approval')); ?>?property="+$('#current_property').val()+"&type="+$('#manager_approved').val()
	});
	
}
$("#gobtn").click(function () {
	createTable();
});
jQuery(document).ready(function ($) {
	createTable();
});
</script>
