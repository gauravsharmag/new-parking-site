<style>
.error {
    color: red;
}
</style>
<div class="page-content-wrapper">
	<div class="page-content" style="min-height:996px">	
		<!-- BEGIN PAGE HEADER-->
		<div class="row">
			<div class="col-md-12">
				<!-- BEGIN PAGE TITLE & BREADCRUMB-->
				<h3 class="page-title">
				GUEST PASS 
				</h3>
				<!-- END PAGE TITLE & BREADCRUMB-->
			</div>
		</div>
		<!-- END PAGE HEADER-->
		<div id="flashMessages">
			<h2><?php  echo $this->Session->flash();?></h2>
		</div>
		<!-- BEGIN PAGE CONTENT-->
		<div class="row">
			<div class="col-md-12">
				<!--ACCORDIAN START-->
					 <!--Accordian Start-->
						<div class="panel-group accordion" id="accordion3">
							<div class="panel panel-default">
								<div class="panel-heading" id="DayPanelTitle">
									<h4 class="panel-title">
                                         <a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_1" aria-expanded="false"> SELECT DAYS AND VEHICLE<i class="fa fa-check" aria-hidden="true" style="margin-left: 50%; display:none;" id="1div"> </i></a>
									</h4>
								</div>
								<div id="collapse_3_1" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
									<div class="panel-body" id="daySelectionDiv">
										 <?php echo $this->Form->create('Vehicle',array('class'=>'form-horizontal','id'=>'DAYS_SECTION'));?>
											<div class="row">
												<div class="col-md-6">
													<!----SECTION 1 START-->
													<div class="portlet box blue-hoki">
														<div class="portlet-title">
															<div class="caption">
																<i class="fa fa-gift"></i>Days 
															</div>
														</div>
														<div class="portlet-body">
																<div class="note note-danger">
																	<h4 class="block">IMPORTANT !</h4>
																	<p> 
																		Please note that it is important to add the vehicle to the pass. Please select the vehicle from the dropdown or add new vehicle details.
																		Without vehicle the pass will not be considered valid. Thanks.  
																	</p>
																</div>
																<div class="row static-info">
																	<div class="col-md-4 name">
																		Total Credits : <?php echo $freeGuestCredits;?>
																	</div>
																	
																</div>
																<div class="row static-info">
																	<div class="col-md-4 name">
																		Remaining : <?php echo $remainingCredits;?>
																	</div>
																	
																</div>
																<div class="row static-info">
																	<div class="col-md-2 name">
																			From:
																	</div>
																	<div class="col-md-10 value">
																		<?php   $dt = new DateTime();
																				$currentDate= $dt->format('m/d/Y');
																				echo $this->Form->input('from',array('required'=>true,'type'=>'text','div'=>false,
																						'label'=>false,
																						'disabled' => 'disabled',
																						'value'=>$currentDate,                                               					                                               								
																						'class'=>'form-control'));
																		?>
																	</div>
																</div>
																<div class="row static-info">
																	<div class="col-md-2 name">
																		To
																	</div>
																	<div class="col-md-10 value">
																		<?php $tomorrow = new DateTime('tomorrow');
																		
																		  echo $this->Form->input('to',array('required'=>true,'type'=>'text','div'=>false,
																						'label'=>false,
																						'between'=>'<div class="input-group date date-picker" data-date-format="mm/dd/yyyy" data-date-start-date="'.$tomorrow->format('m/d/Y').'">',
																						'after'=>'<span class="input-group-btn"><button type="button" class="btn default"><i class="fa fa-calendar"></i></button></span></div>',
																						'class'=>'form-control'));
																		?>
																		
																	</div>
																</div>
																<div class="row static-info">
																	<div class="col-md-4 name">
																		 Number Of Days:<br>
																		<?php
																			echo $this->Form->input('days',array('required'=>true,'type'=>'text','div'=>false,
																						'label'=>false,
																						'disabled' => 'disabled',
																						'value'=>'',                                               					                                               								
																						'class'=>'form-control'));
																		 ?>
																	</div>
																	<div class="col-md-4 name">
																		 Cost Per Day ($) :<br>
																		 <?php
																			echo $this->Form->input('cost',array('required'=>true,'type'=>'text','div'=>false,
																						'label'=>false,
																						'disabled' => 'disabled',
																						'value'=>$permitCost,                                               					                                               								
																						'class'=>'form-control'));
																		 ?>
																	</div>
																	<div class="col-md-4 name">
																		 Amount ($) :<br>
																		  <?php
																			echo $this->Form->input('amount',array('required'=>true,'type'=>'text','div'=>false,
																						'label'=>false,
																						'disabled' => 'disabled',
																						'value'=>'',                                               					                                               								
																						'class'=>'form-control'));
																		 ?>
																	</div>
																</div>
																<div class="row static-info">
																	<div class="col-md-4 name">
																		 Using Credits:<br>
																		<?php
																			echo $this->Form->input('creditsUsed',array('required'=>true,'type'=>'text','div'=>false,
																						'label'=>false,
																						'disabled' => 'disabled',
																						'value'=>'',                                               					                                               								
																						'class'=>'form-control'));
																		 ?>
																	</div>
																	<div class="col-md-4 name">
																		 Cost Per Day ($) :<br>
																		 <?php
																			echo $this->Form->input('cost',array('required'=>true,'type'=>'text','div'=>false,
																						'label'=>false,
																						'disabled' => 'disabled',
																						'value'=>$permitCost,                                               					                                               								
																						'class'=>'form-control'));
																		 ?>
																	</div>
																	<div class="col-md-4 name">
																		Discount ($) :<br>
																		  <?php
																			echo $this->Form->input('discount',array('required'=>true,'type'=>'text','div'=>false,
																						'label'=>false,
																						'disabled' => 'disabled',
																						'value'=>'',                                               					                                               								
																						'class'=>'form-control'));
																		 ?>
																	</div>
																</div>
																<div class="row static-info">
																	<div class="col-md-6 name">
																		Net Amount Payable ($) :
																	</div>
																	<div class="col-md-6 value">
																		 <?php 
																			echo $this->Form->input('netAmmount',array('required'=>true,'type'=>'text','div'=>false,
																						'label'=>false,
																						'disabled' => 'disabled',
																						'value'=>'',                                               					                                               								
																						'class'=>'form-control'));
																		 ?>
																	</div>
																</div>
														</div>
													</div>
													
													<!----SECTION 1 END-->
												</div>
												<div class="col-md-6">
													<!----SECTION 2 START-->
													<div class="portlet box red">
														<div class="portlet-title">
															<div class="caption">
																<i class="fa fa-gift"></i>Vehicle 
															</div>
														</div>
														<div class="portlet-body">
															<div class="form-body">	
																
																<div class="alert alert-info">
																			<strong>Select Existing Vehicle  </strong>
																</div>
																<?php
																	echo $this->Form->input('vehicle_id',array('type' => 'select',
																					'div'=>array('class'=>'form-group'),
																					'label'=>array('class'=>'col-md-6 control-label','text'=>'Select Existing Vehicle'),
																					'error'=>array('attributes'=>array('class'=>'model-error')),
																					'options' => $customerOtherVehicles,
																					'empty'=>'New Vehicle',
																					'between'=>'<div class="col-md-5">',
																					'after'=>'</div>',
																					'onChange'=>'getVehicleDetails()',
																					'class'=>'form-control'));
																?>
																	
																
																<div id="addNewVehicle" >					
																	<div class="note note-success">
																		<p>
																			OR Add New Guest Vehicle
																		</p>
																	</div>
							
																	 <?php
																		echo $this->Form->input('owner',array('div'=>array('class'=>'form-group'),'error'=>array('attributes'=>array('class'=>'model-error')),'label'=>array('class'=>'col-md-6 control-label','text'=>'Vehicle Name'),'between'=>'<div class="col-md-5">','after'=>'</div>','class'=>'form-control','placeholder'=>'Vehicle Name'));
																		
																		echo $this->Form->input('make',array('div'=>array('class'=>'form-group'),'error'=>array('attributes'=>array('class'=>'model-error')),'label'=>array('class'=>'col-md-6 control-label','text'=>'Make'),'between'=>'<div class="col-md-5">','after'=>'</div>','class'=>'form-control','placeholder'=>'Make'));

																		echo $this->Form->input('model',array('div'=>array('class'=>'form-group'),'error'=>array('attributes'=>array('class'=>'model-error')),'label'=>array('class'=>'col-md-6 control-label','text'=>'Model'),'between'=>'<div class="col-md-5">','after'=>'</div>','class'=>'form-control','placeholder'=>'Model'));

																		echo $this->Form->input('color',array('div'=>array('class'=>'form-group'),'error'=>array('attributes'=>array('class'=>'model-error')),'label'=>array('class'=>'col-md-6 control-label','text'=>'Color'),'between'=>'<div class="col-md-5">','after'=>'</div>','class'=>'form-control','placeholder'=>'Color'));

																		echo $this->Form->input('license_plate_number',array('div'=>array('class'=>'form-group'),'error'=>array('attributes'=>array('class'=>'model-error')),'label'=>array('class'=>'col-md-6 control-label','text'=>'License Plate Number'),'between'=>'<div class="col-md-5">','after'=>'</div>','class'=>'form-control','placeholder'=>'License Plate Number'));

																		echo $this->Form->input('license_plate_state',array('type' => 'select',
																								'div'=>array('class'=>'form-group'),
																								'label'=>array('class'=>'col-md-6 control-label','text'=>'License Plate State'),
																								'error'=>array('attributes'=>array('class'=>'model-error')),
																								'options' => $states,																
																								'between'=>'<div class="col-md-5">',
																								'after'=>'</div>',
																								'class'=>'form-control'));

																		echo $this->Form->input('last_4_digital_of_vin',array('div'=>array('class'=>'form-group'),'error'=>array('attributes'=>array('class'=>'model-error')),'label'=>array('class'=>'col-md-6 control-label','text'=>'Last 4 Digital Of Vin'),'between'=>'<div class="col-md-5">','after'=>'</div>','class'=>'form-control','placeholder'=>'Last 4 Digital Of Vin'));
																		
																		echo $this->Form->input('assigned_location',array('div'=>array('class'=>'form-group'),'error'=>array('attributes'=>array('class'=>'model-error')),'label'=>array('class'=>'col-md-6 control-label','text'=>'Assigned Location'),'between'=>'<div class="col-md-5">','after'=>'</div>','class'=>'form-control','placeholder'=>'Assigned Location'));
																		
																	?>
																</div>
																<div id="ExistingVehicleDiv" >
														
																</div>
																<!--Error Block Start-->
																<div class="note note-danger" id="daysErrorDiv" style="display:none;">
																	<h4 class="block">Error !</h4>
																	<p id="daysErrorMsg"></p>
																</div>
																<!--Error Block End-->
																<div class="form-actions">
																	<a href="/Vehicles/add_vehicles" class="btn default">Cancel</a>
																	<button type="submit" class="btn blue">Submit</button>
																	
																</div>
															</div>
														</div>
													</div>
													<!----SECTION 3 END-->
												</div>
											</div>
										 <?php echo $this->Form->end(); ?>
									</div>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title" id="PaymentPanelTitle">
										<a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_2" aria-expanded="false"> MAKE PAYMENT <i class="fa fa-check" aria-hidden="true" style="margin-left: 50%; display:none;" id="seconddiv"> </i></a>
									</h4>
								</div>
								<div id="collapse_3_2" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
									<div class="panel-body" id="paymentDiv">
										
										
										<!--Property Data Start-->
											
										<!--Property Data End-->
										
										
									</div>
								</div>
							</div>
							
							
						</div>
						 <!--Accordian End-->
				
				<!--ACCORDIAN END-->
			</div>
		</div>
	</div>
</div>
<?php 
		//echo $this->Html->script('jquery.min'); 
		echo $this->Html->script('jquery.validate.min.js'); 
		echo $this->Html->script('block.js'); 
?>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.3.1/jquery.maskedinput.min.js" type="text/javascript"></script>
<script>
$("#VehicleLast4DigitalOfVin").mask("9999");
var CUSTOMER_PASS=<?php echo $customerPassId; ?>;
var USER_GUEST_PASS='';
$(document).ready(function(){
	$("#1div").click();
	
	$("#VehicleTo").change(function(){
			var oneday = 24*60*60*1000;
			var start_date_array = $('#VehicleFrom').val().split('/');
			var end_date_array = $(this).val().split('/');
			var end_date = new Date(parseInt(end_date_array[2], 10),
									   parseInt(end_date_array[0], 10) - 1,
									   parseInt(end_date_array[1], 10));
			var start_date = new Date(parseInt(start_date_array[2], 10),
									   parseInt(start_date_array[0], 10) - 1,
									   parseInt(start_date_array[1], 10));
			var days = Math.floor((end_date.getTime() - start_date.getTime())/(oneday));
			//console.log(days); 
			var permitCost=<?php echo $permitCost;?>;
			$('#VehicleDays').val(days);
			$('#VehicleAmount').val(days*permitCost); 
			var remainingCredits=<?php echo $remainingCredits;?>;
			
			if(days>=remainingCredits){
				$('#VehicleCreditsUsed').val(remainingCredits);
				$('#VehicleDiscount').val(remainingCredits*permitCost);
				$('#VehicleNetAmmount').val($('#VehicleAmount').val()-$('#VehicleDiscount').val());
			}else{
				var creditsUsed=remainingCredits-days;
				$('#VehicleCreditsUsed').val(days);
				$('#VehicleDiscount').val(days*permitCost);
				var amountPayable=$('#VehicleAmount').val()-$('#VehicleDiscount').val();
				if(amountPayable<=0){
					$('#VehicleNetAmmount').val('0');
				}else{
					$('#VehicleNetAmmount').val(amountPayable);
				}
				
			}
			 jQuery("#paymentDiv").empty();
			$('#PaymentPanelTitle').css('background-color', '');
	});
	if($("#VehicleTo").val()!=""){
			var oneday = 24*60*60*1000;
			var start_date_array = $('#VehicleFrom').val().split('/');
			var end_date_array = $(this).val().split('/');
			var end_date = new Date(parseInt(end_date_array[2], 10),
									   parseInt(end_date_array[0], 10) - 1,
									   parseInt(end_date_array[1], 10));
			var start_date = new Date(parseInt(start_date_array[2], 10),
									   parseInt(start_date_array[0], 10) - 1,
									   parseInt(start_date_array[1], 10));
			var days = Math.floor((end_date.getTime() - start_date.getTime())/(oneday));
			//console.log(days); 
			var permitCost=<?php echo $permitCost;?>;
			$('#VehicleDays').val(days);
			$('#VehicleAmount').val(days*permitCost); 
			var remainingCredits=<?php echo $remainingCredits;?>;
			
			console.log(remainingCredits);
			if(days>=remainingCredits){
				$('#VehicleCreditsUsed').val(remainingCredits);
				$('#VehicleDiscount').val(remainingCredits*permitCost);
				$('#VehicleNetAmmount').val($('#VehicleAmount').val()-$('#VehicleDiscount').val());
			}else{
				var creditsUsed=remainingCredits-days;
				$('#VehicleCreditsUsed').val(days);
				$('#VehicleDiscount').val(days*permitCost);
				var amountPayable=$('#VehicleAmount').val()-$('#VehicleDiscount').val();
				if(amountPayable<=0){
					$('#VehicleNetAmmount').val('0');
				}else{
					$('#VehicleNetAmmount').val(amountPayable);
				}	
			}
	}
	  var url = "/Vehicles/get_guest_vehicle_ajax/"+CUSTOMER_PASS+"/"+USER_GUEST_PASS; 
	  jQuery("#vehicleDiv").load(url); 
	 
});

function parseDate(str) {
			var d = str.split('/');
			return new Date(parseInt(d[2]),parseInt(d[0]), parseInt(d[1]));
		}
		 $("#DAYS_SECTION").validate({
			rules: {
				"data[Vehicle][to]": {
					required: true,
				},
				"data[Vehicle][owner]": {
					required: true,
				},
				"data[Vehicle][make]": {
					required: true,
				},
				"data[Vehicle][model]": {
					required: true,
				},
				"data[Vehicle][color]": {
					required: true,
				},
				"data[Vehicle][license_plate_number]": {
					required: true,
				},
				"data[Vehicle][license_plate_state]": {
					required: true,
				},
				"data[Vehicle][last_4_digital_of_vin]": {
					required: true
				}
			},
		   
			messages: {
				"data[Vehicle][to]": { required: "Please select the date.",
				},
				"data[Vehicle][owner]": { required: "Please enter vehicle name.",
				},
				"data[Vehicle][make]": { required: "Please enter vehicle make.",
				},
				"data[Vehicle][model]": {
					required: "Please enter vehicle model.",
				},
				"data[Vehicle][color]": {
					required: "Please enter vehicle color.",
				},
				"data[Vehicle][license_plate_number]": {
					required: "Please enter vehicle plate number.",
				},
				"data[Vehicle][license_plate_state]": {
					required: "Please enter vehicle plate state.",
				},
				"data[Vehicle][last_4_digital_of_vin]": {
					required: "Please enter 4 digits of VIN.",
				}
			},
			submitHandler: function(form) {
				$.blockUI({ 
						message: '<h4>Processing....Please Wait</h4>',  
						css: { 
							border: 'none', 
							padding: '15px', 
							backgroundColor: '#000', 
							'-webkit-border-radius': '10px', 
							'-moz-border-radius': '10px', 
							opacity: .5, 
							color: '#fff' 
				} });
				 $('#daysErrorDiv').hide();
				 $('#paymentDiv').empty();
				 $.ajax({
				   type: "POST",
				   url: "/CustomerPasses/checkForPayment/<?php echo $customerPassId.'/'.$passId.'/'.$permitId?>",
				   data: $("#DAYS_SECTION").serialize(),
				   success: function(rsp){ 
					   rsp=JSON.parse(rsp);
					 // console.log(data);
					   if(rsp.success==false){
							 $('#DayPanelTitle').css('rgba(255, 0, 0, 0.5)');
							 //alert(data.message);
							 $('#daysErrorDiv').show();
						     $('#daysErrorMsg').html(rsp.message);
						     $.unblockUI();
					   }else{
						  if(rsp.beyond==false){ 
							   $('#DayPanelTitle').css('background-color', 'rgba(0, 128, 0, 0.5)');
							  if(rsp.payment==false){
								 jQuery("#paymentDiv").empty();
								 jQuery("#paymentDiv").load("/Transactions/guestCheckOutAjax/<?php echo $customerPassId.'/'.$passId.'/'.$permitId?>"); 
								 $("#seconddiv").click();
								 $.unblockUI();
							  }else{
								 window.location.href = "//<?php echo $_SERVER['HTTP_HOST']?>";
							  }
						  }else{
							 $('#DayPanelTitle').css('background-color', 'rgba(255, 0, 0, 0.5)');
							 //$('#daysErrorDiv').show();
						     //$('#daysErrorMsg').html(data.message);
						     alert(rsp.message);
						     $.unblockUI();
						  }
						  
					   } 
				   }
				 });
			}
			
		});
</script>

<script>
	$("#VehicleVehicleId").change(function() {
		if($("#VehicleVehicleId option:selected").val()!=""){
			$("#VehicleOwner").attr("required",false);
			$("#VehicleMake").attr("required",false);
			$("#VehicleModel").attr("required",false);
			$("#VehicleColor").attr("required",false);
			$("#VehicleLicensePlateNumber").attr("required",false);
			$("#VehicleLicensePlateState").attr("required",false);
			$("#VehicleLast4DigitalOfVin").attr("required",false);
		}else{
			console.log($("#VehicleVehicleId option:selected").val());
			$("#VehicleOwner").attr("required",true);
			$("#VehicleMake").attr("required",true);
			$("#VehicleModel").attr("required",true);
			$("#VehicleColor").attr("required",true);
			$("#VehicleLicensePlateNumber").attr("required",true);
			$("#VehicleLicensePlateState").attr("required",true);
			$("#VehicleLast4DigitalOfVin").attr("required",true);
		}
			 
	}); 
	if($("#VehicleVehicleId option:selected").val()!=""){
		$("#VehicleOwner").attr("required",false);
		$("#VehicleMake").attr("required",false);
		$("#VehicleModel").attr("required",false);
		$("#VehicleColor").attr("required",false);
		$("#VehicleLicensePlateNumber").attr("required",false);
		$("#VehicleLicensePlateState").attr("required",false);
		$("#VehicleLast4DigitalOfVin").attr("required",false);
	}else{
		console.log($("#VehicleVehicleId option:selected").val());
		$("#VehicleOwner").attr("required",true);
		$("#VehicleMake").attr("required",true);
		$("#VehicleModel").attr("required",true);
		$("#VehicleColor").attr("required",true);
		$("#VehicleLicensePlateNumber").attr("required",true);
		$("#VehicleLicensePlateState").attr("required",true);
		$("#VehicleLast4DigitalOfVin").attr("required",true);
	}
	 

function parseDate(str) {
			var d = str.split('/');
			return new Date(parseInt(d[2]),parseInt(d[0]), parseInt(d[1]));
		}
</script>
<script>
	function getVehicleDetails(){ 
		$('#daysErrorDiv').hide();
		if($('#VehicleVehicleId').val()){
			$('#addNewVehicle').hide('slow');
			$.blockUI({ css: { 
						border: 'none', 
						padding: '15px', 
						backgroundColor: '#000', 
						'-webkit-border-radius': '10px', 
						'-moz-border-radius': '10px', 
						opacity: .5, 
						color: '#fff' 
			} }); 
			
			$.ajax({
					url: "/Vehicles/my_vehicle_details/"+$('#VehicleVehicleId').val(), 
					success: function(result){
						$("#ExistingVehicleDiv").html(result);
						$('#ExistingVehicleDiv').show('slow');
						$.unblockUI();
					}
				});
		}else{
			$('#addNewVehicle').show('slow');
			$('#ExistingVehicleDiv').hide('slow');
		}
		
	}
	
</script>
