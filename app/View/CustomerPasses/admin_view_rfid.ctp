<div class="page-content-wrapper">
		<div class="page-content" style="min-height:1027px">
			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					RFID <small>View/Edit RFID Tags</small>
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li>
							<i class="fa fa-home"></i>
							<a href="#">Home</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">RFID Tags</a>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
			<div id="flashMessages">
									<h2><?php  echo $this->Session->flash();?></h2>
			</div>
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					
					<!-- Begin: life time stats -->
					<div class="portlet">
						<div class="portlet-title">
							<div class="caption">
								Assigned RFID Tags
							</div>
							
						</div>
						<div class="portlet-body">
							<div class="table-container">	
								
								<div class="row">
										<?php  
													echo $this->Form->input('current_property',array( 'type'=>'select',
																											'options' => $property_list,
																											'selected'=>$selectedId,
																											'label'=>false,
																											'between'=>'<div class="col-md-3">',
																											'after'=>'</div>',
																											'class'=>'form-control' 
																										));         
										?>
								</div>	
								<hr>
								<div class="table-scrollable">
                                              <table id="view_rfid_list" class="table table-striped table-bordered table-hover">
                                            	 <thead>
                                            	   	<tr>
                                                       <th>User</th>
                                                       <th>Property</th>
                                                       <th>Vehicle</th>
                                                       <th>Vehicle Number</th>
                                                       <th>Pass Validity</th>
                                                       <th>Permit Validity</th>
                                                       <th>Assigned RFID</th> 
                                                       <th>Edit RFID</th> 
                                                    </tr>
                        						  </thead>
                                            	  <tbody>
                                                       <tr>
															<td colspan="8" class="dataTables_empty">Loading Data...</td>
														</tr>
                                            	   </tbody>
                                            	</table>
												<?php echo $this->Js->writeBuffer(); ?>
                                        </div>
							</div>
						</div>
					</div>
					<!-- End: life time stats -->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
<?php echo $this->Html->script('//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js'); ?>
<?php echo $this->Html->css('//cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css'); ?>

<script type="text/javascript">
jQuery(document).ready(function ($) {
$(function(){
	$('#view_rfid_list').dataTable({
		"bProcessing": false,
        "bServerSide": true,
        "bDestroy":true,
        "sAjaxSource": "/admin/CustomerPasses/view_passes/"+$('#current_property').val()+""
	});

});
	
    $('#current_property').change(function () {
		 $('#view_rfid_list').dataTable({
		"bProcessing": false,
        "bServerSide": true,
        "bDestroy":true,
        "sAjaxSource": "/admin/CustomerPasses/view_passes/"+$('#current_property').val()+""
		});
	 
	 });
	
});
</script>
