<?php //debug($array);        ?>
<div class="page-content-wrapper">
    <div class="page-content" style="min-height:1027px">

        <!-- BEGIN PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                    <?php echo (__('Add RFID')); ?>  <small><?php echo (__('RFID Tags??')); ?></small>
                </h3>
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <?php echo $this->Html->link(__('Home'), array('controller' => 'Users', 'action' => 'superAdminHome', 'full_base' => true)); ?>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="#"><?php echo (__('RFID Tags')); ?></a>
                    </li>
                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <!-- END PAGE HEADER-->
        <div id="flashMessages">
            <h2><?php echo $this->Session->flash(); ?></h2>
        </div>
        <!-- BEGIN PAGE CONTENT-->
        <div class="row">
            <div class="col-md-12">
                <div class="note note-danger">
                    <p style="font-family:arial;color:red;font-size:20px;">
                        <?php echo (__('All RFID Tag values should not contain spaces and special characters')); ?>
                    </p>
                </div>
                <!-- BEGIN SAMPLE TABLE PORTLET-->
                <div id="main" class="portlet box green">

                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-cogs"></i><?php echo (__('RFID Tags')); ?>
                        </div>
                        <div class="tools">
                            <a class="collapse" href="javascript:;">
                            </a>
                        </div>
                    </div>
                    <div class="portlet-body flip-scroll">
                        <div class="note note-warning">
                            <p>
                                <?php echo (__('Customer Details')); ?>
                            </p>
                        </div>
                        <div class="row static-info">
                            <div class="col-md-2 name">
                                <?php echo (__('Name :')); ?>
                            </div>
                            <div class="col-md-7 value">
                                <?php echo $userDetails['User']['first_name'] . ' ' . $userDetails['User']['last_name']; ?>
                            </div>
                        </div>
                        <div class="row static-info">
                            <div class="col-md-2 name">
                                <?php echo (__('User Name :')); ?>
                            </div>
                            <div class="col-md-7 value">
                                <?php echo $userDetails['User']['username']; ?>
                            </div>
                        </div>
                        <div class="row static-info">
                            <div class="col-md-2 name">
                                <?php echo (__('Address :')); ?>
                            </div>
                            <div class="col-md-7 value">
                                <?php
                                echo $userDetails['User']['address_line_1'] . ' ' . $userDetails['User']['address_line_2'] . '<br>' .
                                $userDetails['User']['city'] . ' ' . $userDetails['User']['state'] . '<br>' .
                                $userDetails['User']['zip'];
                                ?>
                            </div>
                        </div>
                        <div class="row static-info">
                            <div class="col-md-2 name">
                                <?php echo (__('Email :')); ?>
                            </div>
                            <div class="col-md-7 value">
                                <?php echo $userDetails['User']['email']; ?>
                            </div>
                        </div>
                        <div class="row static-info">
                            <div class="col-md-2 name">
                                <?php echo (__('Phone :')); ?>
                            </div>
                            <div class="col-md-7 value">
                                <?php echo $userDetails['User']['phone']; ?>
                            </div>
                        </div>
                        <div class="row">
                            <?php echo $this->Form->create('CustomerPass', array('class' => 'form-horizontal')); ?>
                       <!-- <table class="table table-bordered table-striped table-condensed flip-content">-->
                            <?php for ($i = 0; $i < count($array); $i++) { ?>
                                                                                    <!--<tr>-->
                                <div class="col-md-<?php echo (count($array)==1)?12:6 ?>">
                                    <?php
                                    if ($array[$i]['CustomerPass']['RFID_tag_number'] != NULL) {
                                        $value = $array[$i]['CustomerPass']['RFID_tag_number'];
                                        $location_value = $array[$i]['CustomerPass']['assigned_location'];
                                        $disabled = true;
                                        $rd = 'disabled';
                                    } else {
                                        $value = '';
                                        $disabled = false;
                                        $rd = '';
                                    }
                                    if ($array[$i]['CustomerPass']['assigned_location'] != NULL) {
                                        $value = $array[$i]['CustomerPass']['RFID_tag_number'];
                                        $location_value = $array[$i]['CustomerPass']['assigned_location'];
                                        $loc_disabled = true;
                                        $ld = 'disabled';
                                    } else {
                                        $location_value = '';
                                        $loc_disabled = false;
                                        $ld = '';
                                    }
                                    // echo $this->Form->hidden('id/' . $i . '', array('default' => $array[$i]['CustomerPass']['id'], 'disabled' => $disabled));
                                    ?>
    <!--<td>-->	 
                                    <div class="note note-success">
                                        <p>
                                            <?php echo $array[$i]['CustomerPass']['pass_id'] . ' ' . 'Details'; ?>
                                        </p>
                                    </div>
                                    
                                    <div class="row static-info">
                                        <div class="col-md-5 name">
                                            <?php echo (__('Property :')); ?>
                                        </div>
                                        <div class="col-md-7 value">
                                            <?php echo $array[$i]['CustomerPass']['property_id']; ?>
                                        </div>
                                    </div>
                                     <div class="row static-info">
                                        <div class="col-md-5 name">
                                            <?php echo (__('Approved :')); ?>
                                        </div>
                                        <div class="col-md-7 value">
                                           <?php 
												if($array[$i]['CustomerPass']['approved']){
													echo '<span class="label label-sm label-info">YES</span>';
												}else{
													echo '<span class="label label-sm label-danger">NO</span>';
												}
											?>
                                        </div>
                                    </div>
                                    <div class="row static-info">
                                        <div class="col-md-5 name">
                                            <?php echo (__('Pass :')); ?>
                                        </div>
                                        <div class="col-md-7 value">
                                            <?php echo $array[$i]['CustomerPass']['pass_id']; ?>
                                        </div>
                                    </div>
                                    <div class="row static-info">
                                        <div class="col-md-5 name">
                                            <?php echo (__('Parking Location :')); ?>
                                        </div>
                                        <div class="col-md-7 value">
                                            <?php echo $array[$i]['CustomerPass']['package_id']; ?>
                                        </div>
                                    </div>
                                    <div class="row static-info">
                                        <div class="col-md-5 name">
                                            <?php echo (__('Pass Valid Upto :')); ?>
                                        </div>
                                        <div class="col-md-7 value">
                                            <?php echo $array[$i]['CustomerPass']['pass_valid_upto']; ?>
                                        </div>
                                    </div>
                                    <div class="row static-info">
                                        <div class="col-md-5 name">
                                            <?php echo (__('Parking Location Valid Upto :')); ?>
                                        </div>
                                        <div class="col-md-7 value">	
                                            <?php echo $array[$i]['CustomerPass']['membership_vaild_upto']; ?>
                                        </div>
                                    </div>
                                    <div class="row static-info">
                                        <div class="col-md-5 name">
                                            <?php echo (__('Vehicle Details :')); ?>
                                        </div>
                                        <div class="col-md-7 value">	
                                            <?php if($array[$i]['Vehicle']['id']){
													?>
													<table class="table">
														<tbody>
															<tr>
																<td>Make</td>
																<td><?php echo $array[$i]['Vehicle']['make']; ?></td>
															</tr>
															<tr>
																<td>Model</td>
																<td><?php echo $array[$i]['Vehicle']['model']; ?></td>
															</tr>
															<tr>
																<td>Color</td>
																<td><?php echo $array[$i]['Vehicle']['color']; ?></td>
															</tr>
															<tr>
																<td>Plate Number</td>
																<td><?php echo $array[$i]['Vehicle']['license_plate_number']; ?></td>
															</tr>
															<tr>
																<td>State</td>
																<td><?php echo $array[$i]['Vehicle']['license_plate_state']; ?></td>
															</tr>
															<tr>
																<td>VIN</td>
																<td><?php echo $array[$i]['Vehicle']['last_4_digital_of_vin']; ?></td>
															</tr>
														</tbody>
													</table>
											<?php	
												 }else{
													echo "No Vehicle";
												 }
											?>
                                        </div>
                                    </div>
                                    <div class="row static-info">
                                        <div class="col-md-5 name">
                                            <?php echo (__('RFID Tag :')); ?>
                                        </div>
                                        <div class="col-md-7 value">	
                                            <?php
                                            if (!empty($this->validationErrors['CustomerPass'])) {
                                                if (array_key_exists('RFID_tag_number/' . $array[$i]['CustomerPass']['id'] . '/', $this->validationErrors['CustomerPass'])) {
                                                    $str = $this->validationErrors['CustomerPass']['RFID_tag_number/' . $array[$i]['CustomerPass']['id'] . '/'];
                                                    $valueToDisplay = explode(' ', $str[0]);
                                                    $value = $valueToDisplay[2];
                                                }
                                            }
                                            echo $this->Form->input('RFID_tag_number/' . $array[$i]['CustomerPass']['id'] . '/' . $rd . '', array('required' => true,
                                                'label' => false,
                                                'div' => false,
                                                'readonly' => $disabled ? 'readonly' : '',
                                                'value' => $value,
                                                'error' => array('attributes' => array('class' => 'model-error')),
                                                'class' => 'form-control'
                                            ));
                                            ?>
                                        </div>
                                    </div>
                                    <div class="row static-info">
                                        <div class="col-md-5 name">
                                            <?php echo (__('Assigned Location :')); ?>
                                        </div>
                                        <div class="col-md-7 value">	
                                            <?php
                                            if (!empty($this->validationErrors['CustomerPass'])) {
                                                if (array_key_exists('assigned_location/' . $array[$i]['CustomerPass']['id'] . '/', $this->validationErrors['CustomerPass'])) {
                                                    $loc_str = $this->validationErrors['CustomerPass']['assigned_location/' . $array[$i]['CustomerPass']['id'] . '/'];
                                                    $loc_valueToDisplay = explode(' ', $loc_str[0]);
                                                    $location_value = $loc_valueToDisplay[2];
                                                }
                                            }
                                            echo $this->Form->input('assigned_location/' . $array[$i]['CustomerPass']['id'] . '/'.$ld, array('required' => false,
                                                'label' => false,
                                                'div' => false,
                                                'readonly' => $loc_disabled ? 'readonly' : '',
                                                'value' => $location_value,
                                                'error' => array('attributes' => array('class' => 'model-error')),
                                                'class' => 'form-control'
                                            ));
                                            ?>
                                        </div>
                                    </div>
                                    <!--</td>-->
                                </div>
                                <!--</tr>-->
                            <?php } ?>
                            <!--</table>-->
                        </div>
                        <div class="form-actions">
                            <button type="submit" class="btn blue"><?php echo (__('Submit')); ?></button>
                        </div>
                        <?php echo $this->Form->end(); ?>
                    </div>

                </div>
            </div>
        </div>
        <!-- END SAMPLE TABLE PORTLET-->

    </div>
</div>
<!-- END PAGE CONTENT-->
</div>
</div>

