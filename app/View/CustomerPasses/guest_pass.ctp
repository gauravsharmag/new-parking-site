<div class="page-content-wrapper">
		<div class="page-content" style="min-height:1027px">
			
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					
					My Passes <small>Guest Passes Details</small>
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li>
							<i class="fa fa-home"></i>
							<?php 
										echo $this->Html->link('Home',array(
                                          'controller' => 'Users',
                                          'action' => 'customerHomePage',
                                          'full_base' => true
                                      ));
                                 
                               ?>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">Guest Passes</a>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
			<div id="flashMessages">
                	<h2><?php  echo $this->Session->flash();?></h2>
                </div>
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					
					<!-- BEGIN SAMPLE TABLE PORTLET-->
					<div class="portlet box green">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-cogs"></i>Guest Passes
							</div>
							<div class="tools">
								<a class="collapse" href="javascript:;">
								</a>
							</div>
						</div>
						<div class="portlet-body flip-scroll">
							<?php if(!empty($guestPass)){?>
							<table class="table table-bordered table-striped table-condensed flip-content">
							<thead class="flip-content">
							<tr>
								<th width="5%">
									 #
								</th>
								<th>
									Parking Location
								</th>
								<th>
									Pass Name
								</th>
								<th>
									 Vehicle Plate Number
								</th>
								<th class="numeric">
									 Status
								</th>
								
								<th class="numeric">
									 Permit Validity
								</th>
								<th class="numeric">
									Expiration Date
								</th>

							</tr>
							</thead>
							<tbody>
							<!------Active Passes Start------>
							<?php  
									$srNo=1;foreach($guestPass as $pass){
									
							?>
							<tr>
								<td>
									<?php echo $srNo++; ?>
								</td>
								<td>
									<?php if($pass['CustomerPass']['original_pass']==1){
										echo $this->requestAction(array('controller'=>'Packages','action'=>'getPackageNamePassBought',$pass['CustomerPass']['id']));
									}else{
										echo "Guest Pass";	
									}
									?>
								</td>
								<td>
									<?php  if($pass['CustomerPass']['original_pass']==1){
											echo $this->requestAction(array('controller'=>'Passes','action'=>'getPassName',$pass['CustomerPass']['id']));
										}else{
											echo "Guest Package";	
										}
									?>
								</td>
								<td>
									 <?php
									    if($pass['CustomerPass']['vehicle_id']==null){
                                            echo "No Vehicle Registered"."<br>";
                                            //echo $this->Html->link('Register Now', array('controller' => 'Vehicles', 'action' => 'add',$pass['CustomerPass']['id']));
                                        }else{
                                             $vehiclePlateNumber=$this->requestAction(array('controller'=>'vehicles','action'=>'getVehiclePlateNumber',$pass['CustomerPass']['vehicle_id']));
                                             echo $vehiclePlateNumber['Vehicle']['license_plate_number'];
                                        }
									 ?>
								</td>
								<td class="numeric">
									<?php $dt = new DateTime();
										  $currentDateTime= $dt->format('Y-m-d H:i:s');
										  if($pass['CustomerPass']['status']=='active'){
											echo '<span class="label label-sm label-danger">Active Pass</span>';
										 }elseif($pass['CustomerPass']['status']=='valid'){
											echo ' <span class="label label-sm label-success">Valid Pass</span>';
										}elseif($pass['CustomerPass']['status']=='expired'){
											echo '<span class="label label-sm label-warning">Expired Pass</span>';
										}
									?>
                                	
                                </td>
								<td class="numeric">
									
									
									<?php 
										if($pass['CustomerPass']['status']=='active'){
											echo date("m/d/Y H:i:s", strtotime($pass['CustomerPass']['membership_vaild_upto']));
											 if (!is_null($pass['CustomerPass']['membership_vaild_upto'])) {
                                                    $date1 =strtotime($pass['CustomerPass']['membership_vaild_upto']);
                                                    $date2 = strtotime($pass['CustomerPass']['pass_valid_upto']);
                                                    $datediff =$date2-$date1;
                                                    $daysLeft=floor($datediff/(60*60*24));
                                                    //echo (__('Valid Upto : ')) . date("m/d/Y H:i:s", strtotime($pass['CustomerPass']['membership_vaild_upto']));
                                                    echo "<br>";
                                                    echo "<br>";
                                                    if ($daysLeft > 0) {
                                                        echo '<button id="' . $pass['CustomerPass']['id'] . '" class="btn blue btn-xs" type="button" onclick="showExtendDiv();">Extend Pass</button>';
                                                    }
                                                }
                                                echo '<div id="extendDiv" style="display:none">
															<br>
														<span class="help-block"> NOTE: The Vehicle Will Remain Same If You Extend the Pass. </span>';
												echo $this->Form->create('CustomerPass',array('url'=>array('action'=>'extendGuestPass'),'class'=>'form-inline'));	
												   echo '<div class="form-group">';
														echo $this->Form->hidden('customerPassId',array('value'=>$pass['CustomerPass']['id']));
									
															 echo '<label for="days">Days:</label>';
															 echo $this->Form->input('days',array('div'=>false,'label'=>false,'id'=>'days','type'=>'number','min'=>1,'class'=>'form-control','max'=>$daysLeft));
															
															
														echo '</div>
														<button class="btn btn-default">Submit</button>';
													echo $this->Form->end();
													echo '</div>';
										}elseif($pass['CustomerPass']['status']=='valid'){
											if($pass['CustomerPass']['membership_vaild_upto']==null){
												echo $this->Html->link('Register Guest Vehicle', array('controller' => 'CustomerPasses', 'action' => 'getCustomerPassDetails',$pass['CustomerPass']['id']));
											}else{ 
												echo date("m/d/Y H:i:s", strtotime($pass['CustomerPass']['membership_vaild_upto']));
												echo "<br>";
												echo $this->Html->link('Register Guest Vehicle', array('controller' => 'CustomerPasses', 'action' => 'getCustomerPassDetails',$pass['CustomerPass']['id']));
											}
										}elseif($pass['CustomerPass']['status']=='expired'){
											echo date("m/d/Y H:i:s", strtotime($pass['CustomerPass']['membership_vaild_upto']));
										}
										
									?>
								</td>
								<td class="numeric">
									 <?php echo date("m/d/Y H:i:s", strtotime($pass['CustomerPass']['pass_valid_upto']));
											$currentDateTime= time();
										    $pass_date = strtotime($pass['CustomerPass']['pass_valid_upto']);
										    $datediff =$pass_date-$currentDateTime;
										    $daysLeft=floor($datediff/(60*60*24));
						
										   // if($daysLeft<=7){
												echo "<br>DAYS LEFT FOR EXPIRATION: ".$daysLeft;
												echo '<br>'.$this->Html->link(__('RENEW NOW'), array('controller' => 'CustomerPasses', 'action' => 'renewPass', $pass['CustomerPass']['id'],true));
											//}
									 
									 ?>
								</td>
							</tr>
							<?php }
									
							?>
							
							</tbody>
							</table>
							<?php }
									else{?>
											<h3>No Pass Exists</h3>
							<?php		}
							?>
						</div>
					</div>
					<!-- END SAMPLE TABLE PORTLET-->

				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>

<script>
	function showExtendDiv() {
        $('#extendDiv').toggle('slow');
    }
</script>
