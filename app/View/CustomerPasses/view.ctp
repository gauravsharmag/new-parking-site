<div class="page-content-wrapper">
		<div class="page-content" style="min-height:1027px">
			
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					
					My Passes <small>Passes Details</small>
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li>
							<i class="fa fa-home"></i>
							<?php 
										echo $this->Html->link('Home',array(
                                          'controller' => 'Users',
                                          'action' => 'customerHomePage',
                                          'full_base' => true
                                      ));
                                 
                               ?>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">Passes</a>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
			<div id="flashMessages">
                	<h2><?php  echo $this->Session->flash();?></h2>
                </div>
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<?php if(Configure::read('SMS_NOTIFICATION')){ if($currentUser['User']['phone_verified']==0){ ?>
							<div class="alert alert-danger">
                                  <strong>You need to verify phone number in order to receive text message alerts. <a href="javascript:;" onclick="sendOTP();">Click here to verify.</a></strong> 
                             </div>
					<?php } } ?>
					<!-- BEGIN SAMPLE TABLE PORTLET-->
					<div class="portlet box green">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-cogs"></i>My Passes
							</div>
							<div class="tools">
								<a class="collapse" href="javascript:;">
								</a>
							</div>
						</div>
						<div class="portlet-body flip-scroll">
							<table class="table table-bordered table-striped table-condensed flip-content">
							<thead class="flip-content">
							<tr>
								<th width="5%">
									 #
								</th>
								<!--<th>
									Parking Location Name
								</th>-->
								<th>
									Pass Name
								</th>
								<th>
									 Vehicle Plate Number
								</th>
								<th class="numeric">
									 Status
								</th>
								<th class="numeric">
									Expiration Date
								</th>
								<th class="numeric">
									Recurring Profile
								</th>
								<th class="numeric">
									Assigned Location
								</th>
							</tr>
							</thead>
							<tbody>
							<!------Active Passes Start------>
							<?php $srNo=1;foreach($customerActivePass as $activePass){?>
							<tr>
								<td>
									<?php echo $srNo++; ?>
								</td>
								<!--<td>
									<?php //echo $this->requestAction(array('controller'=>'Packages','action'=>'getPackageNamePassBought',$activePass['CustomerPass']['id']));?>
								</td>-->
								<td>
									<?php echo $this->requestAction(array('controller'=>'Passes','action'=>'getPassName',$activePass['CustomerPass']['id']));?>
								</td>
								<td>
									 <?php
									    if($activePass['CustomerPass']['vehicle_id']==null){
                                            echo "No Vehicle Registered"."<br>";
											if($activePass['CustomerPass']['is_guest_pass']){ 
												echo $this->Html->link('Add Vehicle', array('controller' => 'Vehicles', 'action' => 'add_guest_vehicle',$activePass['CustomerPass']['id']));
											}else{
												echo $this->Html->link('Add Vehicle', array('controller' => 'Vehicles', 'action' => 'add',$activePass['CustomerPass']['id']));
											}
                                        }else{
                                           // echo $activePass['CustomerPass']['vehicle_id'];
                                             $vehiclePlateNumber=$this->requestAction(array('controller'=>'vehicles','action'=>'getVehiclePlateNumber',$activePass['CustomerPass']['vehicle_id']));
                                             echo $vehiclePlateNumber['Vehicle']['license_plate_number'];
                                        }
									 ?>
								</td>
								<td class="numeric"> 
                                	 <span class="label label-sm label-danger">Active Pass</span>
                                	<?php   
                                            if ($activePass['CustomerPass']['is_guest_pass'] == 1) {
                                                if (!is_null($activePass['CustomerPass']['membership_vaild_upto'])) {
													
														$date1 =strtotime($activePass['CustomerPass']['membership_vaild_upto']);
														$date2 = strtotime($activePass['CustomerPass']['pass_valid_upto']);
														$datediff =$date2-$date1;
														$daysLeft=floor($datediff/(60*60*24));
														echo (__('Valid Upto : ')) . date("m/d/Y H:i:s", strtotime($activePass['CustomerPass']['membership_vaild_upto']));
														if($activePass['CustomerPass']['vehicle_id']!=null){
															echo "<br>";
															echo "<br>";
															
															if ($daysLeft > 0) {
																$uDID=uniqid();
																echo '<button id="' . $activePass['CustomerPass']['id'] . '" class="btn blue btn-xs" type="button" onclick="showExtendDiv(\''.$uDID.'\');">Extend Pass</button>';
																 echo '<div id="extendDiv'.$uDID.'" style="display:none">
																	<br>
																<span class="help-block"> NOTE: The Vehicle Will Remain Same If You Extend the Pass. </span>';
																echo $this->Form->create('CustomerPass',array('url'=>array('action'=>'extendGuestPass'),'class'=>'form-inline'));	
																   echo '<div class="form-group">';
																		echo $this->Form->hidden('customerPassId',array('value'=>$activePass['CustomerPass']['id']));
													
																			 echo '<label for="days">Days:</label>';
																			 echo $this->Form->input('days',array('div'=>false,'label'=>false,'id'=>'days','type'=>'number','min'=>1,'class'=>'form-control','max'=>$daysLeft));
																			
																			
																		echo '</div>
																		<button class="btn btn-default">Submit</button>';
																	echo $this->Form->end();
																	echo '</div>';
															}
														}
                                                }
                                               
                                            } else {
                                                echo 'Valid Upto : ' . date("m/d/Y H:i:s", strtotime($activePass['CustomerPass']['membership_vaild_upto']));
                                            }
                                            
									?>
                                </td>
								<!--<td class="numeric">
									<?php 
										//echo date("m/d/Y H:i:s", strtotime($activePass['CustomerPass']['membership_vaild_upto']));
									?>
								</td>-->
								<td class="numeric">
									 <?php echo date("m/d/Y H:i:s", strtotime($activePass['CustomerPass']['pass_valid_upto']));
											$currentDateTime= time();
										    $pass_date = strtotime($activePass['CustomerPass']['pass_valid_upto']);
										    $datediff =$pass_date-$currentDateTime;
										    $daysLeft=floor($datediff/(60*60*24));
						
										   // if($daysLeft<=7){
												echo "<br>DAYS LEFT FOR EXPIRATION: ".$daysLeft;
												echo '<br>'.$this->Html->link(__('RENEW NOW'), array('controller' => 'CustomerPasses', 'action' => 'renewPass', $activePass['CustomerPass']['id'],true));
											//}
									 
									 ?>
								</td>
								<td>
									<?php if($activePass['CustomerPass']['is_guest_pass']==0){
												if($activePass['CustomerPass']['recurring_profile_id']){
													echo '<button id="'.$activePass['CustomerPass']['id'].'" class="btn yellow btn-xs" type="button" onclick="showModal(this);">View Profiles</button><br> 
														 Current Profile : '.$activePass['CustomerPass']['recurring_profile_id'].'<br>
														 Current Status : '.$activePass['CustomerPass']['recurring_profile_status'];
												}
										  }else{
												echo "NA";
										  }
									?>
								</td>
								<td>
									<?php if($activePass['CustomerPass']['assigned_location']){
												echo $activePass['CustomerPass']['assigned_location'];
										  }else{
												echo "NA";
										  }
									?>
								</td>
							</tr>
							<?php }?>
							<!------Active Passes End------>
							<?php foreach($remainingPasses as $remainingPass){?>
										<tr>
											<td>
												<?php echo $srNo++;?>
											</td>
											<!--<td>
												<?php //echo $this->requestAction(array('controller'=>'Packages','action'=>'remainingPassPackageName',$remainingPass['Pass']['id']));?>
											</td>-->																					
											<td>
											<?php echo $remainingPass['Pass']['name'];?>
											</td>
											<td>
												 <?php echo "No Vehicle Registered"?>
											</td>
											<td><?php if($noCurrentPass){ ?>
														<span class="label label-sm label-info">Remaining</span>
												<?php }else{
														echo "<span class='label label-sm label-danger'>New Pass</span>";
													}
												 ?><br>
												 <?php echo $this->Html->link('Buy Pass', array('controller' => 'Transactions', 'action' => 'buyRemainingPass',$remainingPass['Pass']['id']));
													  echo "<br>";
													  $totalCost=$remainingPass['Pass']['deposit']+$remainingPass['Pass']['cost_1st_year'];
													  echo  "Total Cost : $ ".$totalCost." (Including depost and pass cost)";
													
												?>
												 
											</td>
											<td>
												<?php	echo date("m/d/Y H:i:s", strtotime($remainingPass['Pass']['expiration_date']));?>
											</td>
											<td>NA</td>
											<td>NA</td>
										</tr>
								<?php }?>
							<!------Valid Passes Start------>
                            <?php foreach($customerValidPass as $validPass){?>
							<tr>
								<td>
									<?php echo $srNo++; ?>
								</td>
								<!--<td>
									<?php //echo $this->requestAction(array('controller'=>'Packages','action'=>'getPackageNamePassBought',$validPass['CustomerPass']['id']));?>
								</td>-->
								<td>
									<?php echo $this->requestAction(array('controller'=>'Passes','action'=>'getPassName',$validPass['CustomerPass']['id']));?>
								</td>
								<td>
									 <?php
									    if($validPass['CustomerPass']['vehicle_id']==null){
                                            echo "No Vehicle Registered"."<br>";
                                           // echo $this->Html->link('Add Vehicle', array('controller' => 'Vehicles', 'action' => 'add',$validPass['CustomerPass']['id']));
                                        }else{
                                            $vehiclePlateNumber=$this->requestAction(array('controller'=>'vehicles','action'=>'getVehiclePlateNumber',$validPass['CustomerPass']['vehicle_id']));
                                            echo $vehiclePlateNumber['Vehicle']['license_plate_number'];
                                           // debug($vehiclePlateNumber);
                                        }
									 ?>
								</td>
								<td class="numeric">
                                	 <span class="label label-sm label-success">Valid Pass</span><br>
                                	 <?php if($validPass['CustomerPass']['membership_vaild_upto']==null){
											echo $this->Html->link('Activate Pass', array('controller' => 'CustomerPasses', 'action' => 'getCustomerPassDetails',$validPass['CustomerPass']['id']));
									}else{ 
										if($validPass['CustomerPass']['is_guest_pass']==1){
											if(!is_null($validPass['CustomerPass']['membership_vaild_upto'])){
												echo "Last Used Upto: ".date("m/d/Y H:i:s", strtotime($validPass['CustomerPass']['membership_vaild_upto']));
												echo "<br>";
											}
										}
										echo $this->Html->link('Register Vehicle', array('controller' => 'CustomerPasses', 'action' => 'getCustomerPassDetails',$validPass['CustomerPass']['id']));
									}
									?>
                                </td>
								<td class="numeric">
									 <?php echo date("m/d/Y H:i:s", strtotime($validPass['CustomerPass']['pass_valid_upto']));
										   $currentDateTime= time();
										   $pass_date = strtotime($validPass['CustomerPass']['pass_valid_upto']);
										   $datediff =$pass_date-$currentDateTime;
									       $daysLeft=floor($datediff/(60*60*24));
					
									   // if($daysLeft<=7){
											echo "<br>DAYS LEFT FOR EXPIRATION: ".$daysLeft;
											echo '<br>'.$this->Html->link(__('RENEW NOW'), array('controller' => 'CustomerPasses', 'action' => 'renewPass', $validPass['CustomerPass']['id'],true));
										//}
									 ?>
								</td>
								<td>
									<?php if($validPass['CustomerPass']['is_guest_pass']==0){
												if($validPass['CustomerPass']['recurring_profile_id']){
													echo '<button id="'.$validPass['CustomerPass']['id'].'" class="btn yellow btn-xs" type="button" onclick="showModal(this);">View Profiles</button><br> 
														 Current Profile : '.$validPass['CustomerPass']['recurring_profile_id'].'<br>
														 Current Status : '.$validPass['CustomerPass']['recurring_profile_status'];
												}else{
													echo "NA";
												}
										  }else{
												echo "NA";
										  }
									?>
								</td>
								<td>
									<?php if($validPass['CustomerPass']['assigned_location']){
												echo $validPass['CustomerPass']['assigned_location'];
										  }else{
												echo "NA";
										  }
									?>
								</td>
							</tr>
							<?php }?>
							<!------Valid Passes End------>
                            <!------Expired Passes Renewable Start------>
                            <?php foreach($customerRenewableExpiredPass as $expiredPass){?>
							<tr>
								<td>
									<?php echo $srNo++; ?>
								</td>
								<!--<td>
									<?php //echo $this->requestAction(array('controller'=>'Packages','action'=>'getPackageNamePassBought',$expiredPass['CustomerPass']['id']));?>
								</td>-->
								<td>
									<?php echo $this->requestAction(array('controller'=>'Passes','action'=>'getPassName',$expiredPass['CustomerPass']['id']));?>
								</td>
								<td>
									 <?php
									    if($expiredPass['CustomerPass']['vehicle_id']==null){
                                            echo "No Vehicle Registered"."<br>";
                                            //echo $this->Html->link('Register Now', array('controller' => '', 'action' => ''));
                                        }else{
                                           //echo $expiredPass['CustomerPass']['vehicle_id'];
                                           $vehiclePlateNumber=$this->requestAction(array('controller'=>'vehicles','action'=>'getVehiclePlateNumber',$expiredPass['CustomerPass']['vehicle_id']));
                                           echo $vehiclePlateNumber['Vehicle']['license_plate_number'];
                                        }
									 ?>
								</td>
								<td class="numeric">
                                	<span class="label label-sm label-warning">Expired Renewable Pass</span><br>
                                	<?php  if(!is_null($expiredPass['CustomerPass']['membership_vaild_upto'])){
												//echo date("m/d/Y H:i:s", strtotime($expiredPass['CustomerPass']['membership_vaild_upto'])); 
												//echo "<br>";
											}else{
												echo "Permit Expired";
											}
											
										  echo $this->Html->link('Renew', array('controller' => 'CustomerPasses', 'action' => 'renewPass',$expiredPass['CustomerPass']['id']));
									?>
                                </td>
								<td class="numeric">
									 <?php 
										echo date("m/d/Y H:i:s", strtotime($expiredPass['CustomerPass']['pass_valid_upto']));
										
									 ?>
								</td>
								<td>
									<?php if($expiredPass['CustomerPass']['is_guest_pass']==0){
												if($expiredPass['CustomerPass']['recurring_profile_id']){
													echo '<button id="'.$expiredPass['CustomerPass']['id'].'" class="btn yellow btn-xs" type="button" onclick="showModal(this);">View Profiles</button><br> 
														 Current Profile : '.$expiredPass['CustomerPass']['recurring_profile_id'].'<br>
														 Current Status : '.$expiredPass['CustomerPass']['recurring_profile_status'];
												}
										  }else{
												echo "NA";
										  }
									?>
								</td>
								<td>
									<?php if($expiredPass['CustomerPass']['assigned_location']){
												echo $expiredPass['CustomerPass']['assigned_location'];
										  }else{
												echo "NA";
										  }
									?>
								</td>
							</tr>
							<?php }?>
							<!------Expired Passes Renewable End------>
                            <!------Expired Passes Non Renewable Start------>
                            <?php foreach($customerNonRenewableExpiredPass as $expiredPass1){?>
							<tr>
								<td>
									<?php echo $srNo++; ?>
								</td>
								<!--<td>
									<?php //echo $this->requestAction(array('controller'=>'Packages','action'=>'getPackageNamePassBought',$expiredPass1['CustomerPass']['id']));?>
								</td>-->
								<td>
									<?php echo $this->requestAction(array('controller'=>'Passes','action'=>'getPassName',$expiredPass1['CustomerPass']['id']));?>
								</td>
								<td>
									 <?php
									    if($expiredPass1['CustomerPass']['vehicle_id']==null){
                                            echo "No Vehicle Registered"."<br>";
                                            //echo $this->Html->link('Register Now', array('controller' => '', 'action' => ''));
                                        }else{
                                           $vehiclePlateNumber=$this->requestAction(array('controller'=>'vehicles','action'=>'getVehiclePlateNumber',$expiredPass1['CustomerPass']['vehicle_id']));
                                           echo $vehiclePlateNumber['Vehicle']['license_plate_number'];
                                        }
									 ?>
								</td>
								<td class="numeric">
                                	 <span class="label label-sm label-success">Expired Non-Renewable Pass</span><br>
                                	 <?php if(!is_null($expiredPass1['CustomerPass']['membership_vaild_upto'])){
											echo date("m/d/Y H:i:s", strtotime($expiredPass1['CustomerPass']['membership_vaild_upto']));
										}else{
											echo "Permit Expired";
										}
									?>
                                </td>
								<td class="numeric">
									 <?php echo date("m/d/Y H:i:s", strtotime($expiredPass1['CustomerPass']['pass_valid_upto']));?>
								</td>
								<td>
									<?php if($expiredPass1['CustomerPass']['is_guest_pass']==0){
												if($expiredPass1['CustomerPass']['recurring_profile_id']){
													echo '<button id="'.$expiredPass1['CustomerPass']['id'].'" class="btn yellow btn-xs" type="button" onclick="showModal(this);">View Profiles</button><br> 
														 Current Profile : '.$expiredPass1['CustomerPass']['recurring_profile_id'].'<br>
														 Current Status : '.$expiredPass1['CustomerPass']['recurring_profile_status'];
												}
										  }else{
												echo "NA";
										  }
									?>
								</td>
								<td>
									<?php if($expiredPass1['CustomerPass']['assigned_location']){
												echo $expiredPass1['CustomerPass']['assigned_location'];
										  }else{
												echo "NA";
										  }
									?>
								</td>
							</tr>
							<?php }?>
							<!------Expired Passes Non Renewable End------>
							</tbody>
							</table>
						</div>
					    
					</div>
					<!-- END SAMPLE TABLE PORTLET-->
					<div class="portlet box red" style="display: none;" id="RecurringDiv">
						  <div class="portlet-title">
							 <div class="caption">
								<i class="fa fa-cogs"></i>Recurring Profiles
							</div>
							<div class="tools">
								<a class="collapse" href="javascript:;">
								</a>
							</div>
						</div>
						<div class="portlet-body flip-scroll">
							<div class="table-responsive">
								<table id="recurringProfileTable" class="table table-striped table-bordered table-hover">
                                          <thead>
                                            	 <tr>
                                                       <th>Profile ID</th>
                                                       <th>Status</th>
                                                       <th>Date/Time</th>
                                                 </tr>
										</thead>
										<tbody>
											<tr>
												<td colspan="5" class="dataTables_empty">Loading Data......</td>
											</tr>
										</tbody>
								</table>
							</div>
						</div>
					</div>
					<!--Document Start-->
                                <div class="portlet box blue">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-gift"></i>Property Documents
                                        </div>
                                        <div class="tools">
                                            <a class="collapse" href="javascript:;">
                                            </a>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
										<!-- BEGIN FORM-->
										<div class="table-container">       
											<div class="table-responsive">
												<table class="table table-striped table-bordered table-hover" id="documents">
													<thead>
														<tr>
															<th>Document Type</th>
															<th>Action</th>
														</tr>
													</thead>
													<tbody>
													   <tr>
															<td colspan="2" align="center">No Data Found</td>
														</tr>
													</tbody>
												</table>
											</div>
										</div>
										<!-- END FORM-->
                                </div>
                            </div>
                            <!--Document End-->
					<div class="portlet box yellow" style="display: none;" id="RecurringProfileDiv">
						  <div class="portlet-title">
							 <div class="caption">
								<i class="fa fa-cogs"></i>Recurring Profile History
							</div>
							<div class="tools">
								<a class="collapse" href="javascript:;">
								</a>
							</div>
						</div>
						<div class="portlet-body flip-scroll">
							<div class="table-responsive">
								<table id="recurringProfileDetailTable" class="table table-striped table-bordered table-hover">
                                          <thead>
                                            	 <tr>
                                                       <th>Profile ID</th>
                                                       <th>Transaction ID</th>
                                                       <th>History</th>
                                                       <th>Date/Time</th>
                                                       <th>Status</th>
                                                 </tr>
										</thead>
										<tbody>
											<tr>
												<td colspan="5" class="dataTables_empty">Loading Data......</td>
											</tr>
										</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
<!-- Modal -->
<div id="resultModal" class="modal fade" role="dialog">
  <div class="modal-dialog" >
    <!-- Modal content-->
    <div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4 class="modal-title">Online Parking Pass Verification</h4>
		  </div>
		  <div class="modal-body">
			  <div class="alert alert-success">
                      <strong>A verification code has been sent to <?php echo $currentUser['User']['phone']; ?>.</strong>
               </div>
			<?php echo $this->Form->create('Notification',array('url'=>array('controller'=>'Notifications','action'=>'verifyOtp'))); ?>
				<div class="form-body">
					<div class="form-group">
						<?php  echo $this->Form->input('otp',array('label'=>array('text'=>'Please enter the verification code you received in the field below'),'required'=>true,'class'=>'form-control','error'=>array('attributes'=>array('class'=>'model-error'))));?>
					</div>
				</div>
				<div class="form-actions">
					 <button type="submit" class="btn green">Verify</button>
				</div>
			<?php echo $this->Form->end();?>
		  </div>
		
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<?php echo $this->Html->script('//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js'); ?>
<?php echo $this->Html->css('//cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css'); ?>
<script type="text/javascript">
function showModal(e){
	$('#RecurringDiv').hide();
	$('#RecurringProfileDiv').hide();
		$('#recurringProfileTable').dataTable({
											"bProcessing": false,
											"bServerSide": true,
											"bDestroy": true,
											"sAjaxSource": "/CustomerPasses/get_recurring_profiles/"+e.id+"",
											"fnInitComplete":$('#RecurringDiv').show()
		});
	}
function getRecurringProfileDetail(e){
	$('#recurringProfileDetailTable').dataTable({
											"bProcessing": false,
											"bServerSide": true,
											"bDestroy": true,
											"sAjaxSource": "/CustomerPasses/recurring_profile_details/"+e.id+"",
											"fnInitComplete":$('#RecurringProfileDiv').show()
		});
}	
  function showExtendDiv(UID) {
        $('#extendDiv'+UID).toggle('slow');
    }
 
function sendOTP(){
	$.ajax({
		  url: '/Notifications/sendOpt',
		  type:'POST',
		  success: function (res) {
			  res=jQuery.parseJSON(res);
			  if(res.success==true || res.success=='true'){
				$('#resultModal').modal('show');
			 }else{
				alert(res.message);
			 }
			console.log(res);
		}
	 });
}
$('#documents').DataTable({
	"bProcessing": false,
	"bServerSide": true,
	"bDestroy": true,
	"sAjaxSource": "/admin/Documents/get_documents/<?php echo $this->Session->read('PropertyId'); ?>",
	"initComplete": function(settings, json) {
						if(json.iTotalDisplayRecords==0){
							$( "#documents" ).find( ".dataTables_empty" ).attr('colspan',$('#documents thead th').length);
						}
	},
	"columnDefs": [
		{
			"targets": 1,
			"data": function (row, type, val, meta) {
				str='<a class= "label label-sm label-danger" target="_blank;" href="/files/docs/'+row.file_name+'">Download</a>';
				return str;
			}
		}
	]
});
</script>
