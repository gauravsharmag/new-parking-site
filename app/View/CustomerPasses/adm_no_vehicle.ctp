<div class="page-content-wrapper">
		<div class="page-content" style="min-height:1027px">
			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					Passes <small>Active Passes With No Vehicles</small>
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li>
							<i class="fa fa-home"></i>
							<?php 
								if(AuthComponent::user('role_id')==1){
										echo $this->Html->link('Home',array(
                                          'controller' => 'Users',
                                          'action' => 'superAdminHome',
                                          'full_base' => true
                                      ));
                                    }
                                  
                               ?>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">No Vehicles Passes</a>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
			<div id="flashMessages">
									<h2><?php  echo $this->Session->flash();?></h2>
			</div>
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					
					<!-- Begin: life time stats -->
					<div class="portlet">
						<div class="portlet-title">
							<!--<div class="caption">
								Search
							</div>-->
						
						<div class="nav-pills">
										<?php  
												echo $this->Form->input('current_property',array( 'type'=>'select',
																										'options' => $property_list,
																										
																										'div'=>array('class'=>'form-group'),
																										'label'=>array('class'=>'col-md-1 control-label',
																										'text'=>'Property'),
																										'between'=>'<div class="col-md-3">',
																										'after'=>'</div>',
																										'class'=>'form-control' 
																									));
										?>
							</div>
							<br>
						</div>	
						<div class="portlet-body">
							<div class="clear-fix" style="display:none;" id="contactBtn" >	
									<input type="checkbox"  id="bulkDelete"/> <button id="deleteTriger" class="btn btn-danger" >Send Email To Selected Customers</button>
									<span class="help-block">Mark to send mail to all customers present on table </span>
							</div>
								<br>
							<div class="table-container">		
								<div class="table-responsive">
                                            <table id="customers_listing" class="table table-striped table-bordered table-hover">
                                            	 <thead>
                                            	   <tr>
														<th>
															First Name
														</th>
														<th>
															Last Name
														</th>
														<th>
															 Email
														</th>
														<th>
															 Phone
														</th>
														<th>
															 Pass
														</th>
														<th>
															 RFID
														</th>
														<th>
															Validity
														</th>
														<th>
															Manage User
														</th>
														<!--<th>
															Action
														</th>-->
													</tr>
                        						  </thead>
                                            	  <tbody>
                                                       <tr>
															<td colspan="10" class="dataTables_empty">Loading customers...</td>
														</tr>
                                            	   </tbody>
                                            	</table>
                                        </div>
							</div>
						</div>
					</div>
					<!-- End: life time stats -->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
<div id="myModalForError" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h3 class="modal-title">Send Email</h3>
         <h4 id='returnMessage' style=''></h3>
      </div>
      <div class="modal-body">
		  <?php echo $this->Form->create('Ticket',array('class'=>'form-horizontal','id'=>'reportErrorForm'));?>
			<?php 
					echo $this->Form->hidden('uri',array('value'=>$_SERVER['REQUEST_URI']));
					echo $this->Form->hidden('to_users');
			?>
			<div class="form-group" >   
				<label class="col-md-3 control-label">Subject<font>*</font></label>
				<div class="col-md-9">
					<?php echo $this->Form->input('subject', array('required' => true,'type'=>'text','label' => false, 'div' => false, 'class' => 'form-control')); ?>    
				</div> 
			</div>
			<div class="form-group" >   
				<label class="col-md-3 control-label">Message<font>*</font></label>
				<div class="col-md-9">
					<?php echo $this->Form->input('message', array('required' => true,'type'=>'textarea','label' => false, 'div' => false, 'class' => 'form-control')); ?>    
				</div> 
			</div>
			<div class="form-group">
					<label class="col-md-4 control-label"></label>
					<div class="col-md-8">
						<div class="checkbox-list">
							<label class="checkbox-inline">
								<div class="checker" id="uniform-inlineCheckbox21">
									<span>
										<?php echo $this->Form->input('generate_ticket', array('type'=>'checkbox','label' => false, 'div' => false)); ?>    
									</span>
								</div>Send Message Too
							</label>
						</div>
					</div>
			</div>
			<div class="form-actions">
				<div class="row">
					<div class="col-md-offset-3 col-md-9">
						<button type="submit" class="btn green">Submit</button>
					</div>
				</div>
			</div>
		<?php echo $this->Form->end(); ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<?php echo $this->Html->script('../DATATABLE_BUTTON/jquery.dataTables.min.js'); ?>

<?php echo $this->Html->css('../DATATABLE_BUTTON/jquery.dataTables.min.css'); ?>

<?php echo $this->Html->script('block.js'); ?>
<?php echo $this->Html->script('date.js'); ?>
<script type="text/javascript">
$(document).ready(function() {
	$('select').on('change', function() {
			createTable();
	});
	createTable();
	
});
function createTable(){
	$('#contactBtn').show();
	/*$.blockUI({ css: { 
						border: 'none', 
						padding: '15px', 
						backgroundColor: '#000', 
						'-webkit-border-radius': '10px', 
						'-moz-border-radius': '10px', 
						opacity: .5, 
						color: '#fff' 
			} }); */
		$('#customers_listing').dataTable({
			"bProcessing": false,
			"bServerSide": true,
			 "bDestroy": true,
			 "initComplete": function(settings, json) {
												$.unblockUI();
											 },
			
		   "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
			"sAjaxSource": request="/admin/CustomerPasses/get_no_vehicle/"+$('#current_property').val()+"",
			"columnDefs": [
				{
					
					"render": function ( data, type, row ) {
								if(row.RFID_tag_number==null){
									row.RFID_tag_number="<span class='label label-sm label-danger'>NO TAG ASSIGNED</span>"
								}
								str=row.RFID_tag_number+'<br><a class="btn green btn-xs green-stripe" href="/admin/CustomerPasses/edit/'+row.id+'">Edit Pass</a>';
								return str;
					},
					"targets": 5
				},
				{
					
					"render": function ( data, type, row ) {
								var date = new Date(row.pass_valid_upto);
								var newDate = date.toString('MM/dd/yyyy HH:mm:ss');
								str="PASS : "+newDate;
								if(row.membership_vaild_upto!=null){
									var date = new Date(row.membership_vaild_upto);
									var newDate = date.toString('MM/dd/yyyy HH:mm:ss');
									str+="<br>PERMIT : "+newDate;
								}else{
									str+="<br>PERMIT : NOT BOUGHT";
								}
								return str;
					},
					"targets": 6
				},
				{
					
					"render": function ( data, type, row ) {
								str='<a class="btn default btn-xs " href="/admin/Users/pa_view_user_details/'+row.user_id+'">View</a>';
								str+='<a class="btn yellow btn-xs yellow-stripe" target="_blank" href="/admin/users/print_details/'+row.user_id+'">Print</a>';
								str+="<input type='checkbox' class='deleteRow' value='"+row.user_id+"*"+$('#current_property option:selected').text()+"*"+row.email+"*"+row.phone+"'><span class='label label-sm label-success'>Send Email</span></input>" ;
								return str;
					},
					"targets": 7
				}
			]
		});
	}

$("#bulkDelete").on('click', function() { // bulk checked
        var status = this.checked;

        $(".deleteRow").each(function() {
            $(this).prop("checked", status);
        });
    });
$('#deleteTriger').on("click", function(event) {// triggering delete one by one
       
		
        if ($('.deleteRow:checked').length > 0) {  // at-least one checkbox checked
            var ids = [];
            $('.deleteRow').each(function() {
                if ($(this).is(':checked')) {
                    ids.push($(this).val());
                }
            });
			ids_string = ids.toString(); 
			$("#returnMessage").html('');
			$("#TicketSubject").val('');
			$("#TicketMessage").val('');
			$('#TicketToUsers').val(ids_string);
			$('#myModalForError').modal();

        } else {
			alert('Select Customers');
        }
    });
$("#reportErrorForm").submit(function(e)
	{
		e.preventDefault(); //STOP default action
		$.blockUI({
            css: {
                border: 'none',
                padding: '15px',
                margin: 0,
                width: '100%',
                top: '200px',
                left: '0px',
                textAlign: 'center',
                color: '#B35900',
                backgroundColor: 'transparent',
                fontWeight: 'bold',
            },
            overlayCSS: {
                backgroundColor: '#2F2F2F',
                opacity: '0.5',
            },
            message: '<h2><img src="/../../app/webroot/img/busy2.gif", />Loading</h2>'});
		var postData = $(this).serializeArray();
		//console.log(postData);
		$.ajax(
		{
			url : '/admin/Tickets/send_email_bulk',
			type: "POST",
			data : postData,
			success:function(data, textStatus, jqXHR) 
			{
				if(data=='true'){
					$("#returnMessage").html('Message Sent Successfully');
					$("#returnMessage").attr('style','color:green;');
				}else{
					$("#returnMessage").html('Message Cannot Be Sent, Please Try Later');
					$("#returnMessage").attr('style','color:red;');
				}
				  $.unblockUI();
			},
			error: function(jqXHR, textStatus, errorThrown) 
			{
				//if fails      
			}
		});
		
	});
</script>

