<div class="page-content-wrapper">
	<div class="page-content" style="min-height:1027px">
		<!-- BEGIN PAGE HEADER-->
		<div class="row">
			<div class="col-md-12">
				<!-- BEGIN PAGE TITLE & BREADCRUMB-->
				<h3 class="page-title">
				Passes <small>Approve Passes</small>
				</h3>
				<ul class="page-breadcrumb breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<a href="#">Home</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="#">Approve Passes</a>
					</li>
				</ul>
				<!-- END PAGE TITLE & BREADCRUMB-->
			</div>
		</div>
		<!-- END PAGE HEADER-->
		<div id="flashMessages">
								<h2><?php  echo $this->Session->flash();?></h2>
		</div>
		<!-- BEGIN PAGE CONTENT-->
		<div class="row">
			<div class="col-md-12">
				
				<!-- Begin: life time stats -->
				<div class="portlet">
					<div class="portlet-title">
					</div>
					<div class="portlet-body">
						<div class="row">
								<?php  echo "<div class='col-md-4'>";
											echo $this->Form->input('manager_approved',array( 'type'=>'select',
																									'options' => array('Not Approved','Approved'),
																									//'empty'=>'All Type',
																									'div'=>false,
																									'label'=>false,
																									'between'=>'<div class="col-md-12">',
																									'after'=>'</div>',
																									'class'=>'form-control' 
																								));
										echo "</div>";
										
										
								?>
								<div class="col-md-1"> 
										<input type="submit" class="btn yellow" value="Go" name="submit" id="gobtn">
								</div>
						</div>
						<hr>
						
						<div class="table-container">		
							<div class="table-responsive">
								<table id="rfid_list" class="table table-striped table-bordered table-hover">
								  <thead>
									<tr>
									   <th>User</th>
									   <th>Property</th>
									   <th>Pass</th>
									   <th>Vehicle</th>
									   <th>Vehicle Number</th>
									   <th>Pass Validity</th>
									   <th>Permit Validity</th>
									   <th>Approved</th> 
									   <th>Action</th> 
									</tr>
								  </thead>
								  <tbody>
									   <tr>
											<td colspan="9" class="dataTables_empty">...LOADING....</td> 
										</tr>
								   </tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
				<!-- End: life time stats -->
			</div>
		</div>
		<!-- END PAGE CONTENT-->
	</div>
</div>
<div id="modelPass" class="modal fade" aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" style="display: none;">
    <div class="modal-dialog" >
        <div class="modal-content">
            <div class="modal-header">
                <button class="close" aria-hidden="true" data-dismiss="modal" type="button"><i class="fa  fa-times "></i></button>
            </div>
            <div class="modal-body">
                <div id="model_details">

                </div>
            </div>
        </div>
    </div>
</div>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.3.1/jquery.maskedinput.min.js" type="text/javascript"></script>
<?php echo $this->Html->script(['moment-with-locales.js','bootstrap_picker.js']); ?>
<?php echo $this->Html->css(['bootstrap_date']); ?>
<script type="text/javascript">
function createTable(){
	$('#rfid_list').dataTable({
		 "bDestroy": true,
		"columnDefs": [
			{	"targets": 0,
				"render": function ( data, type, row ) {
					return "<b>Username: </b> <a href='/users/manager_view_user_details/"+row.user_id+"'>"+row.username+"</a><br> <b>Name: </b>"+row.first_name+" "+row.last_name+"<br> <b>Address: </b>"+row.address_line_1+" "+row.address_line_2+"<br> "+row.city+" "+row.state+"<br> "+row.zip+"<br> <b>Mobile: </b>"+row.phone+"<br> <b>Email: </b>"+row.email;
				}
			},
			{
				"targets": 3,
				"data": function ( row, type, val, meta ) {
							if(row.last_4_digital_of_vin){
								return "<b>Owner: </b>"+row.owner+"<br> <b>Make: </b>"+row.make+"<br> <b>Model: </b>"+row.model+"<br><b>Color: </b>"+row.color+"<br><b>VIN: </b> "+row.last_4_digital_of_vin;
							}
							return "";
				}
			},
			{
				"targets": 4,
				"data": function ( row, type, val, meta ) {
							if(row.last_4_digital_of_vin){
								return "<b>Plate Number: </b>"+row.license_plate_number+"<br> <b>State: </b>"+row.license_plate_state;
							}
							return "";
				}
			},
			{
				"targets": 5,
				"data": function ( row, type, val, meta ) {
							var date = moment(row.pass_valid_upto);
							var newDate = date.format("DD MMM YYYY HH:mm:ss"); 
							return newDate;	
				}
			},
			{
				"targets": 6,
				"data": function ( row, type, val, meta ) {
							if (row.membership_vaild_upto) {
								var date = moment(row.membership_vaild_upto);
								var newDate = date.format("DD MMM YYYY HH:mm:ss"); 
								return newDate;
							}else{
								return ""; 
							}
				}
			},
			{
				"targets": 7,
				"data": function ( row, type, val, meta ) {
							if(row.approved==1){
								return '<span class="label label-sm label-info">YES</span>';
							}
							return '<span class="label label-sm label-danger">NO</span>';
						}
			},
			{   "targets":8,
				"render": function ( data, type, row ) {
						 if(row.approved==1){
							var token = Math.floor((Math.random() * 100000000111111) + 1);
							return '<form name="post_'+token+'" style="display:none;" method="post" action="/Users/approve_user/'+row.id+'/0"><input  type="hidden" name="_method" value="POST"></form><a href="#" class="label label-sm label-danger" onclick="if (confirm(&quot;Are you sure you want to deny this pass? Denying the pass will delete this pass.&quot;)) { document.post_'+token+'.submit(); } event.returnValue = false; return false;">Deny</a>';
						}else{
							var token = Math.floor((Math.random() * 100000000111111) + 1);
							str= '<a href="#" class="label label-sm label-info" onclick="if (confirm(&quot;Are you sure you want to approve this pass ?&quot;)) { getPass('+row.id+')} event.returnValue = false; return false;">Approve</a>';
							var token = Math.floor((Math.random() * 100000000111111) + 1);
							str+='&nbsp;<form name="post_'+token+'" style="display:none;" method="post" action="/Users/approve_user/'+row.id+'/0"><input  type="hidden" name="_method" value="POST"></form><a href="#" class="label label-sm label-danger" onclick="if (confirm(&quot;Are you sure you want to deny this pass? Denying the pass will delete this pass.&quot;)) { document.post_'+token+'.submit(); } event.returnValue = false; return false;">Deny</a>';
							return str;
						}
				}
			},
			
			
		],
        "bServerSide": true,
        "sAjaxSource": "<?php echo $this->Html->Url(array('controller' => 'CustomerPasses', 'action' => 'pass_approval','admin'=>true)); ?>?type="+$('#manager_approved').val()
	});
	
}
$("#gobtn").click(function () {
	createTable();
});
jQuery(document).ready(function ($) {
	createTable();
});
function getPass(passID){
	
	$.ajax({
		type: "GET",
		url: "/CustomerPasses/approve/"+passID+"",
		success: function (response) {
			console.log(response);
			 $("#model_details").empty();
             $("#model_details").append(response);
             $('#modelPass').modal('show');
			//$.unblockUI();
		}
	});
}
</script>
