<div class="page-content-wrapper">
		<div class="page-content" style="min-height:1027px">
			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					RFID <small>Add RFID Tags</small>
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li>
							<i class="fa fa-home"></i>
							<a href="#">Home</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">RFID Tags</a>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
			<div id="flashMessages">
									<h2><?php  echo $this->Session->flash();?></h2>
			</div>
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					
					<!-- Begin: life time stats -->
					<div class="portlet">
						<div class="portlet-title">
							<div class="caption">
								Assign RFID Tags
							</div>
							
						</div>
						<div class="portlet-body">
							<div class="row">
									<?php  echo "<div class='col-md-4'>";
												echo $this->Form->input('current_property',array( 'type'=>'select',
																										'options' => $property_list,
																										'empty'=>'All Property',
																										'div'=>false,
																										'label'=>false,
																										'between'=>'<div class="col-md-12">',
																										'after'=>'</div>',
																										'class'=>'form-control' 
																									));
											echo "</div>";
											
											
									?>
									<?php  echo "<div class='col-md-4'>";
												echo $this->Form->input('manager_approved',array( 'type'=>'select',
																										'options' => array('Not Approved','Approved'),
																										'empty'=>'All Type',
																										'div'=>false,
																										'label'=>false,
																										'between'=>'<div class="col-md-12">',
																										'after'=>'</div>',
																										'class'=>'form-control' 
																									));
											echo "</div>";
											
											
									?>
									<div class="col-md-1"> 
											<input type="submit" class="btn yellow" value="Go" name="submit" id="gobtn">
									</div>
							</div>
							<hr>
							
							<div class="table-container">		
								<div class="table-responsive">
                                              <table id="rfid_list" class="table table-striped table-bordered table-hover">
                                            	 <thead>
                                            	   	<tr>
                                                       <th>User</th>
                                                       <th>Approved</th>
                                                       <th>Property</th>
                                                       <th>Number Of Passes</th>
                                                       <th>Assign RFID</th> 
                                                    </tr>
                        						  </thead>
                                            	  <tbody>
                                                       <tr>
															<td colspan="5" class="dataTables_empty">Loading Data...</td>
														</tr>
                                            	   </tbody>
                                            	</table>
												<?php echo $this->Js->writeBuffer(); ?>
                                        </div>
							</div>
						</div>
					</div>
					<!-- End: life time stats -->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
<script type="text/javascript">
function createTable(){
	$('#rfid_list').dataTable({
		 "bDestroy": true,
		 "columnDefs": [
			{ "searchable": false, "targets": 3 },
			{
				"render": function ( data, type, row ) {
					return "<b>Username: </b> "+row[0]+"<br> <b>Name: </b>"+row[5]+" "+row[6]+"<br> <b>Address: </b>"+row[7]+" "+row[8]+"<br> "+row[9]+" "+row[10]+"<br> "+row[11]+"<br> <b>Mobile: </b>"+row[12]+"<br> <b>Email: </b>"+row[13];
				},
				"targets": 0
			},
			{
				"render": function ( data, type, row ) {
					if(row[1]==1){
						return '<span class="label label-sm label-info">YES</span>';
					}
					return '<span class="label label-sm label-danger">NO</span>';
				},
				"targets": 1
			}
		  ],
        "bServerSide": true,
        "sAjaxSource": "<?php echo $this->Html->Url(array('controller' => 'CustomerPasses', 'action' => 'get_passes')); ?>?property="+$('#current_property').val()+"&type="+$('#manager_approved').val()
	});
	
}
$("#gobtn").click(function () {
	createTable();
});
jQuery(document).ready(function ($) {
	createTable();
});
</script>
