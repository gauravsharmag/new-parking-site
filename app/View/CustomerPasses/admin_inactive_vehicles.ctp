<div class="page-content-wrapper">
		<div class="page-content" style="min-height:1089px">
			
			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					<?php echo CakeSession::read('PropertyName');?> <small>In-Active Vehicles</small>
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li>
							<i class="fa fa-home"></i>
							<?php echo $this->Html->link(
                                              'Home',
                                              array(
                                                 'controller' => 'Users',
                                                 'action' => 'pa_home_page',
                                                 'full_base' => true
                                                                  )
                                              );?>
							<i class="fa fa-angle-right"></i>
						</li>

						<li>
							<a href="#">All In-Active Vehicles Listing</a>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<div class="tabbable tabbable-custom boxless tabbable-reversed">
						<ul class="nav nav-tabs">
							<li class="active">
								<a data-toggle="tab" href="#tab_0">
								Vehicles Listing </a>
							</li>
							
						</ul>
						<div class="tab-content">
						  <!----------------------------------------TAB Start------------------------->
							<div id="tab_0" class="tab-pane active">
								<div class="portlet box blue">
									<div class="portlet-title">
										<div class="caption">
											<i class="fa fa-gift"></i><?php echo CakeSession::read('PropertyName');?> In-Active Vehicle Details
										</div>
										<div class="tools">
											<a class="collapse" href="javascript:;">
											</a>
										</div>
									</div>
									<div class="portlet-body">
										<div class="table-container">		
											<div class="table-responsive">
                                              <table id="inactive_vehicle_list" class="table table-striped table-bordered table-hover">
                                            	 <thead>
                                            	   	<tr>
                                                       <th>Customer</th>
                                                       <th>Contact Details</th>
                                                       <th>Make</th>
                                                       <th>Model</th>
                                                       <th>Color</th>
                                                       <th>Plate Number</th>
                                                       <th>State</th> 
                                                       <th>Vin</th>
                                                       <th>Pass Expiry Date</th>
                                                       <th>Parking Package Expiry Date</th>
                                                       <th>RFID</th>
                                                    </tr>
                        						  </thead>
                                            	  <tbody>
                                                       <tr>
															<td colspan="11" class="dataTables_empty">Loading Data...</td>
														</tr>
                                            	   </tbody>
                                            	</table>
											</div>
										</div>
									</div>
								</div>
							</div>
			             <!----------------------------------------TAB END------------------------->
						</div>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
<?php echo $this->Html->script('//code.jquery.com/jquery-1.12.0.min.js'); ?>
<?php echo $this->Html->script('//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js'); ?>
<?php echo $this->Html->script('//cdn.datatables.net/buttons/1.1.2/js/dataTables.buttons.min.js'); ?>
<?php echo $this->Html->script('//cdn.datatables.net/buttons/1.1.2/js/buttons.print.min.js'); ?>
<?php echo $this->Html->script('//cdn.datatables.net/buttons/1.1.2/js/buttons.flash.min.js'); ?>
<?php echo $this->Html->script('//cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js'); ?>
<?php echo $this->Html->script('//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js'); ?>
<?php echo $this->Html->script('//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js'); ?>
<?php echo $this->Html->script('//cdn.datatables.net/buttons/1.1.2/js/buttons.html5.min.js'); ?>
<?php echo $this->Html->script('//cdn.datatables.net/buttons/1.1.2/js/buttons.colVis.min.js'); ?>
<?php echo $this->Html->css('//cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css'); ?>
 <?php echo $this->Html->css('//cdn.datatables.net/buttons/1.1.2/css/buttons.dataTables.min.css'); ?> 
<script type="text/javascript">
 $(function(){
	$('#inactive_vehicle_list').dataTable({
		"bProcessing": false,
        "bServerSide": true,
        "sAjaxSource": "<?php echo $this->Html->Url(array('controller' => 'CustomerPasses', 'action' => 'admin_get_inactive_vehicles')); ?>",
         "dom": 'lBfrtip',
         "columnDefs": [
            {
                "render": function ( data, type, row ) {
                    return data +' '+row[11];
                },
                "targets": 0
            },
             {
                "render": function ( data, type, row ) {
                    return data +' '+row[12]+' <br>'+row[13]+' '+row[14]+" <br>"+row[15]+" <br>"+row[16];
                },
                "targets": 1
            },
           
        ],
        buttons: [
            {
                extend: 'print',
                customize: function ( win ) {
                    $(win.document.body)
                        .css( 'font-size', '10pt' )
                        .prepend(
                            '<img src="<?php echo "https://netparkingpass.com/img/logo/".CakeSession::read('PropertyLogo')?>" alt="" style="position:absolute; opacity:0.3; top:40%; left:30%;" />'
                        );
 
                    $(win.document.body).find( 'table' )
                        .addClass( 'compact' )
                        .css( 'font-size', 'inherit' ); 
                }
            },
            {
				extend: 'csv'
			},
			{
				extend: 'pdf'
			},
			{
				extend: 'excel'
			},
			 {
				extend: 'colvis',
				 text: "Select Columns"
			}
        ],
       oLanguage: {
			sInfoFiltered: "",
			sLengthMenu: "Show_MENU_entries.&nbsp;&nbsp;<b>Download Current Table As :</b>&nbsp;",
		},
	});
	
});
</script>
