<div class="page-content-wrapper">
        <div class="page-content" style="min-height:1089px">
            
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                    <h3 class="page-title">
                    <?php echo CakeSession::read('PropertyName');?> <small>User Details</small>
                    </h3>
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <i class="fa fa-home"></i>
                            <?php echo $this->Html->link(
                                              'Home',
                                              array(
                                                 'controller' => 'Users',
                                                 'action' => 'pa_home_page',
                                                 'full_base' => true
                                                                  )
                                              );?>
                            <i class="fa fa-angle-right"></i>
                        </li>
                        <li>
                            <a href="#">User Details</a>
                        </li>
                        <li class="btn-group">
							<a href="/admin/users/print_details/<?php echo $userDetails['User']['id']?>" target="_blank"><button type="button" class="btn blue">
							<span>Print</span>
							</button></a>
						</li>
                    </ul>
                    
                    <!-- END PAGE TITLE & BREADCRUMB-->
                </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
            <div class="row">
                <div class="col-md-12">
                    <div class="tabbable tabbable-custom boxless tabbable-reversed">
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a data-toggle="tab" href="#tab_0">
                                General Info </a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div id="tab_0" class="tab-pane active">
                                <div class="portlet box green">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-gift"></i>Account Details
                                        </div>
                                        <div class="tools">
                                            <a class="collapse" href="javascript:;">
                                            </a>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <!-- BEGIN FORM-->
                                      <div class="table-responsive">
                                                <table class="table table-striped table-bordered table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th>Full Name</th>
                                                            <th>User Name</th>
                                                            <th>Email</th>
                                                            <th>Phone</th>
                                                            <th>Address</th>
                                                            <th>Guest Credits Left</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td><?php echo $userDetails['User']['first_name'].' '.
                                                                           $userDetails['User']['last_name'];
                                                                ?>
                                                            </td>
                                                            <td>
                                                                <?php echo $userDetails['User']['username'];?>
                                                            </td>
                                                            <td>
                                                                <?php echo $userDetails['User']['email'];?>
                                                            </td>
                                                             <td>
                                                                <?php echo $userDetails['User']['phone'];?>
                                                            </td>
                                                             <td>
                                                                <?php echo $userDetails['User']['address_line_1'].' '.$userDetails['User']['address_line_2']."<br>".$userDetails['User']['city'].' '.$userDetails['User']['state'].' '.$userDetails['User']['zip'];

                                                                ?>
                                                            </td>
                                                            <td>
                                                                <?php echo $userDetails['User']['guest_credits'];?>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        <!-- END FORM-->
                                    </div>
                                </div>
                                <div class="portlet box blue">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-gift"></i>Vehicle Details
                                        </div>
                                        <div class="tools">
                                            <a class="collapse" href="javascript:;">
                                            </a>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <!-- BEGIN FORM-->
                                        <div class="table-container">       
                                            <div class="table-responsive">
                                                <table id="customer_list" class="table table-striped table-bordered table-hover">
                                                    <thead>
                                                        <tr>
                                                             <th width="5%">#</th>
                                                             <th>Name</th>
                                                             <th>Make</th>
                                                             <th>Model</th>
                                                             <th>Color</th>
                                                             <th>Plate Number</th>
                                                             <th>State</th>
                                                             <th>Vin</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                      <?php $srNo=1;foreach($userDetails['Vehicle'] as $vehicle){?>
                                                        <tr>
                                                            <td>
                                                                <?php echo $srNo++; ?>
                                                            </td>
                                                            <td>
                                                                <?php echo $vehicle['owner'];?>
                                                            </td>
                                                            <td>
                                                                <?php echo $vehicle['make'];?>
                                                            </td>
                                                            <td>
                                                                <?php echo $vehicle['model'];?>
                                                            </td>
                                                            <td>
                                                                <?php echo $vehicle['color'];?>
                                                            </td>
                                                            <td>
                                                                <?php echo $vehicle['license_plate_number'];?>
                                                            </td>
                                                            <td>
                                                                <?php echo $vehicle['license_plate_state'];?>
                                                            </td>
                                                            <td>
                                                                <?php echo $vehicle['last_4_digital_of_vin'];?>
                                                            </td>
                                                        </tr>
                                                    <?php }?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <!-- END FORM-->
                                    </div>
                                </div>
                                <div class="portlet box grey-cascade">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-gift"></i>Passes
                                        </div>
                                        <div class="tools">
                                            <a class="collapse" href="javascript:;">
                                            </a>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <!-- BEGIN FORM-->
                                        <div class="table-container">       
                                            <div class="table-responsive">
                                                <table id="customer_list" class="table table-striped table-bordered table-hover">
                                                    <thead>
                                                        <tr>
                                                             <th width="5%">#</th>
                                                             <th>Parking Package Name</th>
                                                             <th>Pass Name</th>
                                                             <th>Vehicle Plate Number</th>
                                                             <th>Status</th>
                                                             <th>Permit Expiration Date</th>
                                                             <th>Pass Expiration Date</th>
                                                             <th>RFID Tag Number</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                      <?php $srNo=1;foreach($userDetails['CustomerPass'] as $pass){?>
                                                        <tr>
                                                            <td>
                                                                <?php echo $srNo++; ?>
                                                            </td>
                                                            <td>
                                                                <?php echo $this->requestAction(array('controller'=>'Packages','action'=>'getPackageNamePassBought',$pass['id']));?>
                                                            </td>
                                                            <td>
                                                                <?php echo $this->requestAction(array('controller'=>'Passes','action'=>'getPName',$pass['id']));?>
                                                            </td>
                                                            <td>
                                                                  <?php
                                                                    if($pass['vehicle_id']==null){
                                                                        echo "No Vehicle Registered"."<br>";
                                                                    }else{
                                                                        $vehiclePlateNumber=$this->requestAction(array('controller'=>'vehicles','action'=>'getVehiclePlateNumber',$pass['vehicle_id']));
                                                                        echo $vehiclePlateNumber['Vehicle']['license_plate_number'];
                                                                    }
                                                                   ?>
                                                            </td>
                                                            <td>
                                                                <?php
                                                                    $dt = new DateTime();
                                                                    $currentDateTime= $dt->format('Y-m-d H:i:s');
                                                                    if(($pass['membership_vaild_upto']==null||$pass['membership_vaild_upto']<$currentDateTime) && $pass['pass_valid_upto']>$currentDateTime){
                                                                        echo "<span class='label label-sm label-info'>Valid Pass</span>";
                                                                    }elseif($pass['pass_valid_upto']<$currentDateTime){
                                                                        echo "<span class='label label-sm label-warning'>Expired Pass</span>";
                                                                    }elseif($pass['membership_vaild_upto']>$currentDateTime && $pass['pass_valid_upto']>$currentDateTime){
                                                                        echo "<span class='label label-sm label-success'>Active Pass</span>";
                                                                    }
                                                                   ?>
                                                            </td>
                                                            <td class="numeric">
                                                                <?php 
                                                                    echo is_null($pass['membership_vaild_upto'])?"Permit Not Bought":date("m/d/Y H:i:s", strtotime($pass['membership_vaild_upto']));
                                                                ?>
                                                            </td>
                                                            <td class="numeric">
                                                                <?php echo date("m/d/Y H:i:s", strtotime($pass['pass_valid_upto']));?>
                                                            </td>
                                                            <td>
                                                                  <?php
                                                                    if($pass['RFID_tag_number']==null){
                                                                        echo "No Tag Assigned"."<br>";
                                                                    }else{
                                                                        echo $pass['RFID_tag_number'];
                                                                    }
                                                                   ?>
                                                            </td>
                                                        </tr>
                                                    <?php }?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <!-- END FORM-->
                                    </div>
                                </div>
                                <div class="portlet box yellow">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-gift"></i>Transactions
                                        </div>
                                        <div class="tools">
                                            <a class="collapse" href="javascript:;">
                                            </a>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <!-- BEGIN FORM-->
                                        <div class="table-container">       
                                            <div class="table-responsive">
                                                <table id="transaction_list" class="table table-striped table-bordered table-hover">
                                                    <thead>
                                                        <tr>
                                                             <th>Pay Pal Id</th>
                                                             <th>Date</th>
                                                             <th>Amount</th>
                                                             <th>Result</th>
                                                             <th>Pass</th>
                                                             <th>First Name</th>
                                                             <th>Last Name</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                       <tr>
                                                            <td colspan="5" class="dataTables_empty">Loading Data...</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <!-- END FORM-->
                                    </div>
                                </div>
                                <?php if(isset($userDetails['BillingAddress'][0])){?>
                                <div class="portlet box red">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-gift"></i>Billing Address
                                        </div>
                                        <div class="tools">
                                            <a class="collapse" href="javascript:;">
                                            </a>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <!-- BEGIN FORM-->
                                         <div class="table-container">       
                                            <div class="table-responsive">
                                                <table class="table table-striped table-bordered table-hover">
                                                    <thead>
                                                        <tr>
                                                             <th>Name</th>
                                                             <th>Email</th>
                                                             <th>Phone</th>
                                                             <th>Address</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                       <tr>
                                                            <td>
                                                                <?php
                                                                    echo $userDetails['BillingAddress'][0]['first_name'].' '.
                                                                         $userDetails['BillingAddress'][0]['last_name'];
                                                                ?>
                                                            </td>
                                                             <td>
                                                                <?php
                                                                    echo $userDetails['BillingAddress'][0]['email'];
                                                                ?>
                                                            </td>
                                                            <td>
                                                                <?php
                                                                    echo $userDetails['BillingAddress'][0]['phone'];
                                                                ?>
                                                            </td>
                                                            <td>
                                                                <?php
                                                                    echo $userDetails['BillingAddress'][0]['address_line_1'].' '.$userDetails['BillingAddress'][0]['address_line_2'].'<br>'.
                                                                         $userDetails['BillingAddress'][0]['city'].' '.$userDetails['BillingAddress'][0]['state'].'<br>'.
                                                                            $userDetails['BillingAddress'][0]['zip'];
                                                                ?>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <!-- END FORM-->
                                    </div>
                            </div>
                             <?php }?>
                            <!------Tickets Start---------->
                                <div class="portlet box red">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-gift"></i>Communication History
                                        </div>
                                        <div class="tools">
                                            <a class="collapse" href="javascript:;">
                                            </a>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <!-- BEGIN FORM-->
                                        <div class="table-container">       
                                            <div class="table-responsive">
                                                <table class="table table-striped table-bordered table-hover" id="tickets">
                                                    <thead>
														<tr>

															<th>Subject</th>
															<th>From</th>
															<th>To</th>
															<th>Sent On</th>
															<th>Status</th>
															<th>Action</th>
														</tr>
													</thead>
                                                    <tbody>
                                                       <tr>
                                                            <td colspan="6" align="center">No Data Found</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <!-- END FORM-->
                                </div>
                            </div>
                            <!------Tickets End------------>
                        
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PAGE CONTENT-->
        </div>
    </div>
<script type="text/javascript">
$(function(){
    $('#transaction_list').dataTable({
        "bProcessing": false,
        "bServerSide": true,
        "sAjaxSource": "<?php echo $this->Html->Url(array('controller' => 'Transactions', 'action' => 'admin_get_transactions',$userDetails['User']['id'])); ?>"
    });
    $('#tickets').dataTable({
		"bProcessing": false,
		"bServerSide": true,
		"bDestroy": true,
		"aoColumns": [
			{"mData":"Ticket.subject"},
			{"mData":"User.first_name"},
			{"mData":"Recipient.first_name"},
			{"mData":"Ticket.created"},
			{"mData":"Ticket.status"},
			{"mData":"Ticket.id"},
		],
		"sAjaxSource":"/admin/Tickets/get_tickets_user/<?php echo $userDetails['User']['id']; ?>"
		//"sAjaxSource":"<?php echo $this->Html->Url(array('controller' => 'SubscriptionPayments', 'action' => 'sub_revenew_table',1)); ?>"
	});
});
</script>
