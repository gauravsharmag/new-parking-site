	<?php  //echo $this->Html->script('/assets/admin/pages/scripts/charts.js');
		   //echo $this->Html->script('/assets/global/plugins/flot/jquery.flot.min.js');  
		   //echo $this->Html->script('/assets/global/plugins/flot/jquery.flot.resize.min.js');
		   //echo $this->Html->script('/assets/global/plugins/flot/jquery.flot.pie.min.js');
		  // echo $this->Html->script('/assets/global/plugins/flot/jquery.flot.stack.min.js');
	      // echo $this->Html->script('/assets/global/plugins/flot/jquery.flot.crosshair.min.js');
	?>
	<div class="page-content-wrapper">
		<div class="page-content">
        
        	<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					Welcome To <?php echo CakeSession::read('PropertyName');?><small>Dashboard</small>
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li>
							<i class="fa fa-home"></i>
							<a href="javascript;">Home</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">Dashboard</a>
						</li>
						<li class="pull-right">
							<div id="dashboard-report-range" class="dashboard-date-range tooltips" data-placement="top" data-original-title="Change dashboard date range">
								<i class="icon-calendar"></i>
								<span></span>
								<i class="fa fa-angle-down"></i>
							</div>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
            <div class="row">
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<div class="dashboard-stat blue-madison">
						<div class="visual">
							<i class="fa fa-building-o"></i>
						</div>
						<div class="details">
							<div class="number">
								 <?php echo $activePasses;?>
							</div>
							<div class="desc">
								Active Passes
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<div class="dashboard-stat red-intense">
						<div class="visual">
							<i class="fa fa-taxi"></i>
						</div>
						<div class="details">
							<div class="number">
								 <?php echo $activeVehicles;?>
							</div>
							<div class="desc">
								Active Vehicles
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<div class="dashboard-stat green-haze">
						<div class="visual">
							<i class="fa fa-group"></i>
						</div>
						<div class="details">
							<div class="number">
								 <?php echo $registeredUser;?>
							</div>
							<div class="desc">
								 Registered Users
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<div class="dashboard-stat purple-plum">
						<div class="visual">
							<i class="fa fa-taxi"></i>
						</div>
						<div class="details">
							<div class="number">
								 <?php echo $activeGuest;?>
							</div>
							<div class="desc">
								 Active Guest Vehicles
							</div>
						</div>
					</div>
				</div>
			</div>
			<!---------------------Pie Charts Start--------------->
			<div class="row">
				<div class="col-md-6">
					<div class="portlet box red">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-gift"></i>Passes Status
							</div>
							<div class="tools">
							
								<a href="javascript:;" class="collapse">
								</a>
							</div>
						</div>
						<div class="portlet-body">
							<div class="row">
                                <div class="col-md-2">
									<span class="label label-sm label-success" style="background-color:#53dc9e">
									<?php echo "Active: ".$activePasses; ?> </span>
								</div>
								<div class="col-md-2">
									<span class="label label-sm label-warning"  style="background-color:#eb7575">
										<?php echo "Inactive: ".$inActivePasses; ?> </span>
								</div> 
								<div class="col-md-2">
									<span class="label label-sm label-info" style="background-color:#cfb72f">
											<?php echo "Expired: ".$expiredPasses; ?> </span>
								</div>
								<div class="col-md-6">
								</div>

							</div>
							<div id="passesStatus" class="chart" style="height:300px;">
							<?php 

                                                          //$result= array(0,3,5);
							$this->Js->set('pieChartArray', $result);
							?>
							</div>
						</div>
					</div>
				</div>
						<div class="col-md-6">
					<div class="portlet box yellow">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-gift"></i>Vehicle Status
							</div>
							<div class="tools">
								
								<a href="javascript:;" class="collapse">
								</a>
							</div>
						</div>
						<div class="portlet-body">
							<div class="row">
                                <div class="col-md-3">
									<span class="label label-sm label-success" style="background-color:#53dc9e">
									<?php echo "Active vehicles: ".$activeVehicles; ?> </span>
								</div>
								<div class="col-md-4">
									<span class="label label-sm label-warning" style="background-color:#eb7575">
										<?php echo "Active Guest Vehicles: ".$activeGuest; ?> </span>
								</div> 
								<div class="col-md-4">
									<span class="label label-sm label-info" style="background-color:#cfb72f">
											<?php echo "InActive Vehicles: ".$inActivePasses; ?> </span>
								</div>
								<div class="col-md-5">
								</div>

							</div>
							<div id="passesStatus2" class="chart" style="height:300px;">
						<?php 
									
									
									$this->Js->set('pieChartArray2', $result2);
									
						?>
							</div>
						</div>
					</div>
				</div>	
		
			</div>	
			<!---------------------Pie Charts End--------------->
			<div class="row">
				<div class="col-md-12">
                                    <div class="portlet box purple">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-gift"></i>Created Passes Trends
							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								
								
							</div>
						</div>
						<div class="portlet-body">
						<div class="row">
							<div class="col-md-2">
									</div>
                                <div class="col-md-2">
									<span class="label label-sm label-success">
									<?php echo $monthname.": ".$counter1; ?> </span>
								</div>
								<div class="col-md-2">
									<span class="label label-sm label-warning">
										<?php echo $lastMonthName.": ".$counter2; ?> </span>
								</div> 
								<div class="col-md-2">
									<span class="label label-sm label-info">
											<?php echo $thirdLastMonthName.": ".$counter3; ?> </span>
								</div>
								<div class="col-md-2">
									<span class="label label-sm label-danger">
											<?php echo $fourthLastMonthName.": ".$counter4; ?> </span>
								</div>
								<div class="col-md-2">
									</div>
								

							</div>
							<div id="chart_1_2" class="chart">
						<?php 

													
															$this->Js->set('c1',$counter1);
															$this->Js->set('c2',$counter2);
															$this->Js->set('c3',$counter3);
															$this->Js->set('c4',$counter4);
															$this->Js->set('monthname',$monthname);
															$this->Js->set('lastMonthName',$lastMonthName);
															$this->Js->set('thirdLastMonthName',$thirdLastMonthName);
															$this->Js->set('fourthLastMonthName',$fourthLastMonthName);

															echo $this->Js->writeBuffer(array('onDomReady' => false));
							?>

							
							</div>
						</div>
					</div>


				</div>
			</div>
			
			
        </div>
    </div>

<script>
 $(document).ready(function() {  
 		  var data1 = [
                [window.app.c1, 10], [window.app.c2, 20], [window.app.c3, 30], [window.app.c4, 40]
            ];
          var ticks = [[10, window.app.monthname], [20, window.app.lastMonthName], [30, window.app.thirdLastMonthName], [40, window.app.fourthLastMonthName]];
 

	console.log(data1);
        var options = {
                    series:{
                        bars:{show: true}
                    },
                    bars:{
                        horizontal:true,
                        barWidth:6,
                                    lineWidth: 0, // in pixels
                                    shadowSize: 0,
                                    align: 'left'
                    },
                      xaxis: {
                axisLabel: "alloted",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 10,
                
                tickColor: "#5E5E5E",
                 tickLength: 0,
                color: "black"
            },
            yaxis: {
                axisLabel: "Months",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 3,
                tickColor: "#5E5E5E",
                ticks: ticks,
				tickLength:0,
                color: "black"
            },
     grid:{
                        hoverable: true,
                         tickColor: "#eee",
                                borderColor: "#eee",
                               // borderWidth: 1
                    }
            };
           
            $.plot($("#chart_1_2"), [data1], options); 
				
 
  var data = window.app.pieChartArray;
          
		
 
   $.plot($("#passesStatus"), data, {
                series: {
                    pie: {
                        show: true,
                        radius: 1,
                        label: {
                            show: true,
                            radius: 3 / 4,
                            formatter: function(label, series) {
                                return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">' + label + '<br/>' + Math.round(series.percent) + '%</div>';
                            },
                            background: {
                                opacity: 0.5
                            }
                        }
                    }
                },
                legend: {
                    show: false
                }
            });

			
			var data3 = window.app.pieChartArray2;
          
			console.log(data3);
 
   $.plot($("#passesStatus2"), data3, {
                series: {
                    pie: {
                        show: true,
                        radius: 1,
                        label: {
                            show: true,
                            radius: 3 / 4,
                            formatter: function(label, series) {
                                return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">' + label + '<br/>' + Math.round(series.percent) + '%</div>';
                            },
                            background: {
                                opacity: 0.5
                            }
                        }
                    }
                },
                legend: {
                    show: false
                }
            });


   
});
</script>
