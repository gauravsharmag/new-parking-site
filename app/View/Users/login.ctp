<!-- BEGIN LOGIN FORM -->
<?php
        echo $this->Form->create('User',array('class'=>'login-form'));

?>
<h3 class="form-title">Login to your account</h3>
<div id="flashMessages">
                 <?php  echo $this->Session->flash();?>
</div>
<?php
		echo $this->Form->hidden('requestedUrl',array('default'=>$requestedURL));
        echo $this->Form->input('username',array('div'=>array('class'=>'form-group'),'label'=>array('class'=>'control-label visible-ie8 visible-ie9','text'=>'Username'),'before'=>'<div class="input-icon"><i class="fa fa-user"></i>','after'=>'</div>','class'=>'form-control placeholder-no-fix'));
		echo $this->Form->input('password',array('div'=>array('class'=>'form-group'),'label'=>array('class'=>'control-label visible-ie8 visible-ie9','text'=>'Password'),'before'=>'<div class="input-icon"><i class="fa fa-key"></i>','after'=>'</div>','class'=>'form-control placeholder-no-fix'));

 ?>
		<div class="form-actions">
			<label class="checkbox">
			<!--<input type="checkbox" name="remember" value="1"/> Remember me </label>-->
			<button type="submit" class="btn green pull-right">
			Login <i class="m-icon-swapright m-icon-white"></i>
			</button>
		</div>


        <div class="forget-password">
			<h4>Forgot your password ?</h4>
			<p>
				 no worries, click <a href="javascript:;" id="forget-password">
				here </a>
				to reset your password.
			</p>
		</div>
		<div class="create-account">
			<p>
				 Don't have an account yet ?&nbsp; <?php  echo $this->Html->link(__('Register Here'), array('action' => 'register')); ?>
			</p>
		</div>
<?php echo $this->Form->end(); ?>
<!-- END LOGIN FORM -->
<!-- BEGIN FORGOT PASSWORD FORM -->
	<?php         echo $this->Form->create('User',array('url'=>array('action'=>'forgetPwd'),'class'=>'forget-form')); ?>
		<h3>Forget Password ?</h3> 
		<p>
			 Enter your e-mail address below to reset your password.
		</p>
		<div class="form-group">
			<div class="input-icon">
				<i class="fa fa-envelope"></i>
				<input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Email" name="email"/>
			</div>
		</div>
		<div class="form-actions">
			<button type="button" id="back-btn" class="btn">
			<i class="m-icon-swapleft"></i> Back </button>
			<button type="submit" class="btn green pull-right">
			Submit <i class="m-icon-swapright m-icon-white"></i>
			</button>
		</div>
		<div class="create-account">
			<p>
				 Don't have an account yet ?&nbsp; <?php  echo $this->Html->link(__('Register Here'), array('action' => 'register')); ?>
			</p>
		</div>
	<?php echo $this->Form->end(); ?>
	<!-- END FORGOT PASSWORD FORM -->

