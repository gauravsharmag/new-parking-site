<style type="text/css" media="print">
    @page 
    {
        size: auto;   /* auto is the initial value */
        margin-bottom: 7mm;
    }
</style>
<div class="row">
<div class="col-xs-4">
<?php if(isset($userDetails['User']['property_logo'])){
			echo $this->Html->image('logo/'.$userDetails['User']['property_logo'][0],array('height'=>'45','width'=>'130'));
		}else{
			echo $this->Html->image('onlineparking-logo.png',array('height'=>'45','width'=>'130')); 
		}
?>
</div>
<div class="col-xs-4">
<h3><b><?php echo $userDetails['User']['first_name'].' '.
$userDetails['User']['last_name'];
?>
</b></h3>
</div>
<div class="col-xs-4">
<h4><b>Property: </b><?php
for($i=0;$i<count($userDetails['User']['assigned_Property']);$i++){
echo $userDetails['User']['assigned_Property'][$i]."<br>";
}
?></h4>
</div>
</div>

<div class="row">
<div class="col-xs-6">
<h3>Account Details</h3>
<dl class="dl-horizontal">
  <dt>Full Name</dt>
  <dd><?php echo $userDetails['User']['first_name'].' '.
$userDetails['User']['last_name'];
?></dd>
  <dt>User Name</dt>
  <dd><?php echo $userDetails['User']['username'];?></dd>
  <dt>Address</dt>
  <dd><?php echo $userDetails['User']['address_line_1'].' '.$userDetails['User']['address_line_2']."<br>".
$userDetails['User']['city'].' '.$userDetails['User']['state'].' '.$userDetails['User']['zip'];
?></dd>
<dt>Email</dt>
<dd><?php echo $userDetails['User']['email'];?></dd>
<dt>Phone</dt>
<dd><?php echo $userDetails['User']['phone'];?></dd>
</dl>

</div>
<div class="col-xs-6">
<?php if(isset($userDetails['BillingAddress'][0])){?>
<h3>Billing Address</h3>
<dl class="dl-horizontal">
    <dt>Name</dt>
    <dd><?php
echo $userDetails['BillingAddress'][0]['first_name'].' '.
$userDetails['BillingAddress'][0]['last_name'];
?></dd>
    <dt>Address</dt>
    <dd><?php echo $userDetails['BillingAddress'][0]['address_line_1'].' '.$userDetails['BillingAddress'][0]['address_line_2'].'<br>'.
$userDetails['BillingAddress'][0]['city'].' '.$userDetails['BillingAddress'][0]['state'].'<br>'.
$userDetails['BillingAddress'][0]['zip'];
?></dd>
    <dt>Email</dt>
    <dd><?php echo $userDetails['BillingAddress'][0]['email']; ?></dd>
    <dt>Phone</dt>
    <dd><?php echo $userDetails['BillingAddress'][0]['phone']; ?></dd>
</dl>
<?php } ?>
</div>
</div>

<div class="row">
<div class="table-container">   
<div class="note">
<b> Vehicle Details </b><?php echo empty($userDetails['Vehicle'])?": No Record Found":" "?>
</div>
<?php if(!empty($userDetails['Vehicle'])){?>
<div class="table-responsive">
<table class="table table-striped table-bordered table-hover">
<thead>
<tr>
<th width="5%">#</th>
<th>Name</th>
<th>Make</th>
<th>Model</th>
<th>Color</th>
<th>Plate Number</th>
<th>State</th>
<th>Vin</th>
</tr>
</thead>
<tbody>
<?php $srNo=1;foreach($userDetails['Vehicle'] as $vehicle){?>
<tr>
<td>
<?php echo $srNo++; ?>
</td>
<td>
<?php echo $vehicle['owner'];?>
</td>
<td>
<?php echo $vehicle['make'];?>
</td>
<td>
<?php echo $vehicle['model'];?>
</td>
<td>
<?php echo $vehicle['color'];?>
</td>
<td>
<?php echo $vehicle['license_plate_number'];?>
</td>
<td>
<?php echo $vehicle['license_plate_state'];?>
</td>
<td>
<?php echo $vehicle['last_4_digital_of_vin'];?>
</td>
</tr>
<?php }?>
</tbody>
</table>
</div>
<?php }?>
</div>
<div class="table-container"> 
<div class="note">
<b> Pass Details </b><?php echo empty($userDetails['CustomerPass'])?": No Record Found":" "?>
</div>     
<?php if(!empty($userDetails['CustomerPass'])){?> 
<div class="table-responsive">
<table  class="table table-striped table-bordered table-hover">
<thead>
<tr>
<th width="5%">#</th>
<!--<th>Permit Details</th>-->
<th>Pass Details</th>
<th>Vehicle Plate Number</th>
<th>Status</th>                             
</tr>
</thead>
<tbody>
<?php $srNo=1;foreach($userDetails['CustomerPass'] as $pass){?>
<tr>
<td>
<?php echo $srNo++; ?>
</td>
<!--<td>
<?php //echo $this->requestAction(array('controller'=>'Packages','action'=>'getPackageNamePassBought',$pass['id']))."<br>";
//echo is_null($pass['membership_vaild_upto'])?"Permit Not Bought":date("m/d/Y H:i:s", strtotime($pass['membership_vaild_upto']));
?>
</td>-->
<td>
<?php echo $this->requestAction(array('controller'=>'Passes','action'=>'getPassName',$pass['id']))."<br>";
echo date("m/d/Y H:i:s", strtotime($pass['pass_valid_upto']));
?>
</td>
<td>
<?php
if($pass['vehicle_id']==null){
echo "No Vehicle Registered"."<br>";
}else{
$vehiclePlateNumber=$this->requestAction(array('controller'=>'vehicles','action'=>'getVehiclePlateNumber',$pass['vehicle_id']));
echo $vehiclePlateNumber['Vehicle']['license_plate_number'];
}
?>
</td>
<td>
<?php
$dt = new DateTime();
$currentDateTime= $dt->format('Y-m-d H:i:s');
if(($pass['membership_vaild_upto']==null||$pass['membership_vaild_upto']<$currentDateTime) && $pass['pass_valid_upto']>$currentDateTime){
echo "<span class='label label-sm label-info'>Valid Pass</span>"."<br>";
}elseif($pass['pass_valid_upto']<$currentDateTime){
echo "<span class='label label-sm label-warning'>Expired Pass</span>"."<br>";
}elseif($pass['membership_vaild_upto']>$currentDateTime && $pass['pass_valid_upto']>$currentDateTime){
echo "<span class='label label-sm label-success'>Active Pass</span>"."<br>";
}
if($pass['RFID_tag_number']==null){
echo "No Tag Assigned"."<br>";
}else{
echo $pass['RFID_tag_number'];
}
?>
</td>                                                     
</tr>
<?php }?>
</tbody>
</table>
</div>
<?php }?>
</div>
</div>
