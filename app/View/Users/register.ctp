<style>
.login .content {
       width: 600px;
}
body {
  background: #384047;
  font-family: sans-serif;
  font-size: 10px;
}

form {
  background: #fff;
  padding: 4em 4em 2em;
  max-width: 550px;
//  margin: 50px auto 0;
  box-shadow: 0 0 1em #222;
  border-radius: 2px;
  width:100%;
}
form h2 {
  margin: 0 0 50px 0;
  padding: 10px;
  text-align: center;
  font-size: 30px;
  color: #666666;
  border-bottom: solid 1px #e5e5e5;
}
form p {
  margin: 0 0 3em 0;
  position: relative;
    
}
form input {
  display: block;
  box-sizing: border-box;
  width: 100%;
  outline: none;
  margin: 0;
}
form input[type="text"],
form input[type="password"] {
  background: #fff;
  border: 1px solid #dbdbdb;
  font-size: 1.2em;
  padding: .7em .5em;
  border-radius: 2px;
}
form input[type="text"]:focus,
form input[type="password"]:focus {
  background: #fff;
}

form input[type="text"],form input[type="email"], form input[type="password"], select {
    background: #fff;
    border: 1px solid #dbdbdb;
    font-size: 1.2em;
    padding: .7em .5em;
    border-radius: 2px;
    width: 100%;
}
      
form span {
  display: block;
  background: #F9A5A5;
  padding: 2px 5px;
  color: #666;
}
form input[type="submit"] {
  background: rgba(148, 186, 101, 0.7);
  box-shadow: 0 3px 0 0 rgba(123, 163, 73, 0.7);
  border-radius: 2px;
  border: none;
  color: #fff;
  cursor: pointer;
  display: block;
  font-size: 2em;
  line-height: 1.6em;
  margin: 2em 0 0;
  outline: none;
  padding: .8em 0;
  text-shadow: 0 1px #68B25B;
}
form input[type="submit"]:hover {
  background: #94af65;
  text-shadow: 0 1px 3px rgba(70, 93, 41, 0.7);
}
form label.floatLabel {
  position: absolute;
  left: 8px;
  top: 12px;
  color: #999;
  font-size: 16px;
  display: inline-block;
  padding: 0px 5px;
  font-weight: 400;
  background-color: rgba(255, 255, 255, 0);
  -moz-transition: color 0.3s, top 0.3s, background-color 0.8s;
  -o-transition: color 0.3s, top 0.3s, background-color 0.8s;
  -webkit-transition: color 0.3s, top 0.3s, background-color 0.8s;
  transition: color 0.3s, top 0.3s, background-color 0.8s;
}
form label.floatLabel {
  top: -11px;
  background-color: rgba(255, 255, 255, 0.8);
  font-size: 14px;
}
form .termsAgree input {
    width: initial;
    display: inline;
}
.input{margin-bottom:20px;}
form input[type="number"], form input[type="tel"]{
    background: #fff;
    border: 1px solid #dbdbdb;
    font-size: 1.2em;
    padding: .7em .5em;
    border-radius: 2px;
    width: 100%;
}

@media (max-width: 480px)
{
.login .content {
    width: 100% !important;
}
}


</style>


<?php echo $this->Form->create('User',array());?>

		<h3 style="text-align:center"><b>Sign Up for <?php /*debug($property);*/ echo($property['name']);?></b></h3>
		<h5 style="text-align:center">
			 Enter your personal details below:
		</h5><br>
      
	  <?php echo $this->Session->flash(); ?>
	  
	 <div class="row">
		
				<div class="col-md-6"> 
					   <label for="first_name" class="floatLabel">First Name</label>
					   <?php  echo $this->Form->input('first_name',array(
															   'label'=>false,
															   'error'=>array('attributes'=>array('class'=>'model-error'))
															  )
													  );?>
				</div>
			
				<div class="col-md-6"> 
					   <label for="last_name" class="floatLabel">Last Name</label>
					   <?php  echo $this->Form->input('last_name',array(
															 'label'=>false,
															  'error'=>array('attributes'=>array('class'=>'model-error'))
																	  )
													  );?>
				</div>
			
	 </div>	<!-------------row end-------------->
	 <div class="row">
		  
			 <div class="col-md-6"> 
				       <label for=" email " class="floatLabel">Email</label>
					  <?php  echo $this->Form->input('email',array(
															   'label'=>false,
															   'error'=>array('attributes'=>array('class'=>'model-error'))
															  )
													  );?>
			 </div>	
		  
		  
			<div class="col-md-6"> 
				      <label for="address_line_1" class="floatLabel">Address Line 1</label>
					  <?php  echo $this->Form->input('address_line_1',array(
															   'label'=>false,
															   'error'=>array('attributes'=>array('class'=>'model-error'))
															  )
													  );?>
			</div>
		  
	 </div> <!-------------row end-------------->
	 <div class="row">
		  
			<div class="col-md-6"> 
				 <label for="address_line_2" class="floatLabel">Apartment/Suite #</label>
				 <?php  echo $this->Form->input('address_line_2',array(
															   'label'=>false,
															   'error'=>array('attributes'=>array('class'=>'model-error'))
															  )
													  );?>
			</div>	
	 </div> <!-------------row end-------------->
	 <div class="row">
		 
			<div class="col-md-6"> 
				 <label for="city" class="floatLabel">City</label>
				 <?php  echo $this->Form->input('city',array(
															   'label'=>false,
															   'error'=>array('attributes'=>array('class'=>'model-error'))
															  )
													  );?>
			</div>	
	
			<div class="col-md-6"> 
			    <label for="state" class="floatLabel">State</label>
				<?php  echo $this->Form->input('state',array(
															   'label'=>false,
															   //'type'=>'select',
															   'error'=>array('attributes'=>array('class'=>'model-error'))
															  )
													  );?>
			</div>
		 
	</div> <!-------------row end-------------->
	
	<div class="row">
	
			<div class="col-md-6"> 
				<label for="zip" class="floatLabel">Zip</label>
			    <?php  echo $this->Form->input('zip',array(
																	   'label'=>false,
																	   'error'=>array('attributes'=>array('class'=>'model-error'))
																	  )
															  );?>
			</div>	
	
			<div class="col-md-6"> 
				<label for="phone" class="floatLabel">Phone</label>
			   <?php  echo $this->Form->input('phone',array(
																	   'label'=>false,
																	   'error'=>array('attributes'=>array('class'=>'model-error'))
																	  )
															  );?>
			</div>
		
	</div> <!-------------row end-------------->
    <div class="row">
	
			<div class="col-md-6"> 
				<label for="property_password" class="floatLabel">Property Password</label>
			    <?php  
			    echo $this->Form->hidden('property_id',array('value'=>$property['id']));
			    echo $this->Form->input('property_password',array(
																	   'label'=>false,
																	   'type'=>'password',
																	   'error'=>array('attributes'=>array('class'=>'model-error'))
																	  )
															  );?>
						 
			
			</div>
			
			<div class="col-md-6">
				<label for="username" class="floatLabel">Username</label>
				 <?php  echo $this->Form->input('username',array(
																	   'label'=>false,
																	   'error'=>array('attributes'=>array('class'=>'model-error'))
																	  )
															  );?> 
			</div>
		
		</div> <!-------------row end-------------->
	
  	
    
		<div class="row">
		
			<div class="col-md-6"> 
				<label for="password" class="floatLabel">Password</label>
				 <?php  echo $this->Form->input('password',array(
																	   'label'=>false,
																	   'type'=>'password',
																	   'error'=>array('attributes'=>array('class'=>'model-error'))
																	  )
															  );?> 
			</div>	
		
			<div class="col-md-6"> 
				<label for="password_confirmation" class="floatLabel">Confirm Password</label>
				 <?php  echo $this->Form->input('password_confirmation',array(
																	   'label'=>false,
																	   'type'=>'password',
																	   'error'=>array('attributes'=>array('class'=>'model-error'))
																	  )
															  );?> 
			</div>
		</div> <!-------------row end-------------->	
	    
	  <div class="row">
		<div class="col-md-6"> 
			<label for="coupon" class="floatLabel">Coupon</label>
			
			<?php   $couponRequired=false;
			           if($property['coupon_selection']){
				        $couponRequired=true;
			          }
					echo $this->Form->input('coupon',array(
													   'label'=>false,
													   'required'=>$couponRequired,
													   'error'=>array('attributes'=>array('class'=>'model-error'))
													  )
											);?>
		</div>	
     </div>			
        <div class="form-group termsAgree">
			
		<label>
		     <?php echo $this->Form->input('tos', array(
		                             'type'=>'checkbox',
		                             'required'=>true,
		                             'label'=>false,
		                             'error'=>array('attributes'=>array('class'=>'model-error')),'div'=>false, true)); ?>
		                             
		   <label for="UserTos">I confirm that I have read <a href="" data-toggle="modal" data-target="#terms_popup">terms and conditions</a></label>
        
        </label>
        </div>
		
		<div class="form-actions">
        <?php echo $this->Html->link('<i class="m-icon-swapleft"></i> Back',array('controller'=>'users','action'=>'login'),array('escape'=>false,'class'=>'btn blue')); ?>
			
			<button type="submit" id="register-submit-btn" class="btn green pull-right">
			Sign Up <i class="m-icon-swapright m-icon-white"></i>
			</button>
		</div>
 <?php echo $this->Form->end();
     ?>
<!-- Modal -->
<div class="modal fade" id="terms_popup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Terms &amp; Conditions</h4>
      </div>
      <div class="modal-body">
        <?php echo $property['terms_and_conditions'];?>
      </div>
    </div>
  </div>
</div>
