<style>
.login .content {
       width: 600px !important;
}
</style>


<?php echo $this->Form->create('User',array());?>

		<h3>Sign Up for <?php /*debug($property);*/ echo($property['name']);?></h3>
		<p>
			 Enter your personal details below:
		</p>
      
	  <div id="flashMessages">
                	<h2><?php  echo $this->Session->flash();?></h2>
            </div>
<div class="row">
	<div class="col-md-6"> 
	  <label>First Name</label>
      <?php  echo $this->Form->input('first_name',array('div'=>array('class'=>'form-group'),'label'=>false,'before'=>'<div class="input-icon"><i class="fa fa-font"></i>','after'=>'</div>','class'=>'form-control placeholder-no-fix','error'=>array('attributes'=>array('class'=>'model-error'))));?>
	</div>	
    <div class="col-md-6">
		<label>Last Name</label>  
		<?php echo $this->Form->input('last_name',array('div'=>array('class'=>'form-group'),'label'=>array('class'=>'control-label visible-ie8 visible-ie9'),'before'=>'<div class="input-icon"><i class="fa fa-font"></i>','after'=>'</div>','class'=>'form-control placeholder-no-fix','error'=>array('attributes'=>array('class'=>'model-error')))); ?>
    </div>
</div>	<!-------------row end-------------->
<div class="row">
	<div class="col-md-6"> 
		<label>Email</label>
    	 <?php  echo $this->Form->input('email',array('div'=>array('class'=>'form-group'),'label'=>array('class'=>'control-label visible-ie8 visible-ie9'),'before'=>'<div class="input-icon"><i class="fa fa-envelope"></i>','after'=>'</div>','class'=>'form-control placeholder-no-fix','error'=>array('attributes'=>array('class'=>'model-error')))); ?>
	</div>	
    <div class="col-md-6"> 
		<label>Address Line 1</label>
		<?php   echo $this->Form->input('address_line_1',array('div'=>array('class'=>'form-group'),'label'=>array('class'=>'control-label visible-ie8 visible-ie9'),'before'=>'<div class="input-icon"><i class="fa fa-check"></i>','after'=>'</div>','class'=>'form-control placeholder-no-fix','error'=>array('attributes'=>array('class'=>'model-error')))); ?>
    </div>
</div> <!-------------row end-------------->
<div class="row">
	<div class="col-md-6"> 
	<label>Apartment/Suite #</label>
     <?php echo $this->Form->input('address_line_2',array('div'=>array('class'=>'form-group'),'label'=>array('class'=>'control-label visible-ie8 visible-ie9'),'before'=>'<div class="input-icon"><i class="fa fa-check"></i>','after'=>'</div>','class'=>'form-control placeholder-no-fix','error'=>array('attributes'=>array('class'=>'model-error'))));?>
	</div>	
    <div class="col-md-6"> 
		<?php  //echo $this->Form->input('street',array('div'=>array('class'=>'form-group'),'label'=>array('class'=>'control-label visible-ie8 visible-ie9'),'before'=>'<div class="input-icon"><i class="fa fa-map-marker"></i>','after'=>'</div>','class'=>'form-control placeholder-no-fix','placeholder'=>'Street')); ?>
    </div>
</div> <!-------------row end-------------->
<div class="row">
	<div class="col-md-6"> 
		<label>City</label>
		<?php	echo $this->Form->input('city',array('div'=>array('class'=>'form-group'),'label'=>array('class'=>'control-label visible-ie8 visible-ie9'),'before'=>'<div class="input-icon"><i class="fa fa-location-arrow"></i>','after'=>'</div>','class'=>'form-control placeholder-no-fix','error'=>array('attributes'=>array('class'=>'model-error'))));?>
	</div>	
    <div class="col-md-6"> 
		<label>State</label>
		<?php  echo $this->Form->input('state',array('type' => 'select','options' => $states,'empty'=>'State','div'=>array('class'=>'form-group'),'label'=>array('class'=>'control-label visible-ie8 visible-ie9'),'before'=>'<div class="input-icon"><i class="fa fa-location-arrow"></i>','after'=>'</div>','class'=>'form-control placeholder-no-f	ix','error'=>array('attributes'=>array('class'=>'model-error'))));?>
    </div>
</div> <!-------------row end-------------->
<div class="row">
	<div class="col-md-6"> 
		<label>Zip</label>
		<?php	echo $this->Form->input('zip',array('div'=>array('class'=>'form-group'),'label'=>array('class'=>'control-label visible-ie8 visible-ie9'),'before'=>'<div class="input-icon"><i class="fa fa-location-arrow"></i>','after'=>'</div>','class'=>'form-control placeholder-no-fix','error'=>array('attributes'=>array('class'=>'model-error')))); ?>
	</div>	
    <div class="col-md-6"> 
		<label>Phone</label>
		<?php	 echo $this->Form->input('phone',array('div'=>array('class'=>'form-group'),'label'=>array('class'=>'control-label visible-ie8 visible-ie9'),'before'=>'<div class="input-icon"><i class="fa fa-location-arrow"></i>','after'=>'</div>','class'=>'form-control placeholder-no-fix','error'=>array('attributes'=>array('class'=>'model-error'))));?>

    </div>
</div> <!-------------row end-------------->

<div class="row">
	<div class="col-md-6"> 
		<label>Property Password</label>
		<?php echo $this->Form->hidden('property_id',array('value'=>$property['id']));
			echo $this->Form->input('property_password',array('div'=>array('class'=>'form-group'),'label'=>array('class'=>'control-label visible-ie8 visible-ie9','text'=>'Property Password'),'before'=>'<div class="input-icon"><i class="fa fa-lock"></i>','after'=>'</div>','class'=>'form-control placeholder-no-fix','type'=>'password','error'=>array('attributes'=>array('class'=>'model-error'))));   
		   
		   ?>
	</div>	
    <div class="col-md-6"> 
		<label>Username</label>
     <?php echo $this->Form->input('username',array('div'=>array('class'=>'form-group'),'label'=>array('class'=>'control-label visible-ie8 visible-ie9'),'before'=>'<div class="input-icon"><i class="fa fa-user"></i>','after'=>'</div>','class'=>'form-control placeholder-no-fix','error'=>array('attributes'=>array('class'=>'model-error'))));?>

    </div>
</div> <!-------------row end-------------->
	
    	 
<div class="row">
	<div class="col-md-6"> 
		<label>Password</label>
   	<?php echo $this->Form->input('password',array('div'=>array('class'=>'form-group'),'label'=>array('class'=>'control-label visible-ie8 visible-ie9'),'before'=>'<div class="input-icon"><i class="fa fa-lock"></i>','after'=>'</div>','class'=>'form-control placeholder-no-fix','error'=>array('attributes'=>array('class'=>'model-error')))); ?>
	</div>	
    <div class="col-md-6"> 
		<label>Confirm Password</label>
    <?php echo $this->Form->input('password_confirmation',array('div'=>array('class'=>'form-group'),'label'=>array('class'=>'control-label visible-ie8 visible-ie9','text'=>'Re-type Your Password'),'before'=>'<div class="input-icon"><i class="fa fa-check"></i>','after'=>'</div>','class'=>'form-control placeholder-no-fix','type'=>'password','error'=>array('attributes'=>array('class'=>'model-error'))));
		
		?>

    </div>
</div> 
<div class="row">
	<div class="col-md-6"> 
		<label>Coupon</label>
   	<?php  $couponRequired=false;
			if($property['coupon_selection']){
				$couponRequired=true;
			}
			echo $this->Form->input('coupon',array('div'=>array('class'=>'form-group'),'label'=>array('class'=>'control-label visible-ie8 visible-ie9'),'before'=>'<div class="input-icon"><i class="fa fa-lock"></i>','after'=>'</div>','class'=>'form-control placeholder-no-fix','error'=>array('attributes'=>array('class'=>'model-error')),'required'=>$couponRequired)); 
			
		?>
	</div>	
    
</div>
<!-------------row end-------------->	
		
			
<!--	
		<p>
			 Enter your account details below:
		</p>
       
			
			
			
			-->
			
        <div class="form-group">
			
		<label>
		<?php echo $this->Form->input('tos', array(
		'type'=>'checkbox','required'=>true,'label'=>false,'error'=>array('attributes'=>array('class'=>'model-error')),'div'=>false, true)); ?>
		<label for="UserTos">I confirm that I have read <a href="" data-toggle="modal" data-target="#terms_popup">terms and conditions</a></label>
        
        </label>
        </div>
		
		<div class="form-actions">
        <?php echo $this->Html->link('<i class="m-icon-swapleft"></i> Back',array('controller'=>'users','action'=>'login'),array('escape'=>false,'class'=>'btn blue')); ?>
			
			<button type="submit" id="register-submit-btn" class="btn green pull-right">
			Sign Up <i class="m-icon-swapright m-icon-white"></i>
			</button>
		</div>
 <?php echo $this->Form->end();
     echo $this->Js->writeBuffer();?>
<!-- Modal -->
<div class="modal fade" id="terms_popup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Terms &amp; Conditions</h4>
      </div>
      <div class="modal-body">
        <?php echo $property['terms_and_conditions'];?>
      </div>
    </div>
  </div>
</div>
