<div class="page-content-wrapper">
        <div class="page-content" style="min-height:1089px">
            
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                    <h3 class="page-title">
                    <?php echo CakeSession::read('PropertyName');?> <small>User Details</small>
                    </h3>
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <i class="fa fa-home"></i>
                            <?php if(AuthComponent::user('role_id')==1){
                                    echo $this->Html->link('Home',array(
                                                 'controller' => 'Users',
                                                 'action' => 'superAdminHome',
                                                 'full_base' => true
                                                                  )
                                              );
                              }
                              ?>
                                      
                            <i class="fa fa-angle-right"></i>
                        </li>
                        <li>  
                           User Details
                        </li>
                        
                       
						
                    </ul>
                    <!-- END PAGE TITLE & BREADCRUMB-->
                </div>
            </div>
            <!-- END PAGE HEADER-->
            <div id="flashMessages">
                	<h2><?php  echo $this->Session->flash();?></h2>
            </div>
            <!-- BEGIN PAGE CONTENT-->
            <div class="row">
                <div class="col-md-12">
                    <div class="tabbable tabbable-custom boxless tabbable-reversed">
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a data-toggle="tab" href="#tab_0">
                                General Info </a>
                            </li>
                           <!-- <li class="">
                                <a data-toggle="tab" href="#tab_1">
                                Vehicles </a>
                            </li>
                            <li class="">
                                <a data-toggle="tab" href="#tab_2">
                                Passes </a>
                            </li>
                            <li class="">
                                <a data-toggle="tab" href="#tab_3">
                                Transactions </a>
                            </li>
                            <li>
                                <a data-toggle="tab" href="#tab_4">
                                Billing Address </a>
                            </li>-->
                        </ul>
                        <div class="tab-content">
                            <div id="tab_0" class="tab-pane active">
								<div class="nav-pills">
									 <ul class="nav nav-tabs">
										<li>
											<a href="/admin/users/edit/<?php echo $userDetails['User']['id']?>"><button type="button" class="btn yellow">
											<span>Edit</span>
											</button></a> 
										</li>
									<?php if( $userDetails['User']['role_id']==4){?>
										<li>
											<a href="/admin/CustomerPasses/customer_view/<?php echo $userDetails['User']['id']?>" target="_blank"><button type="button" class="btn blue">
											<span>Manage User</span>
											</button></a> 
										</li>
										<li class="btn-group">
											<a href="/admin/tickets/contact_user/<?php echo $userDetails['User']['id']?>" ><button type="button" class="btn red">
												<span>Contact</span>
											</button></a>
										</li>
										<li>
											<a href="/admin/Users/create_csv/<?php echo $userDetails['User']['id']?>" target="_blank"><button type="button" class="btn blue">
											<span>Download CSV</span>
											</button></a> 
										</li>
									<?php } ?>	
									 <li>
										<a href="/admin/users/print_details/<?php echo $userDetails['User']['id']?>" target="_blank"><button type="button" class="btn green">
										<span>Print</span>
										</button></a>
									</li>
									</ul>
										
								</div>
                                <div class="portlet purple box">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-gift"></i>Account Details
                                        </div>
                                        <div class="tools">
                                            <a class="collapse" href="javascript:;">
                                            </a>
                                        </div>
                                    </div>
                                    <div class="portlet-body ">
										 
                                        <!-- BEGIN FORM-->
                                             <div class="table-responsive">
                                                <table class="table table-striped table-bordered table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th>Full Name</th>
                                                            <th>User Name</th>
                                                            <th>Email</th>
                                                            <th>Phone</th>
                                                            <th>Address</th>
                                                            <th>Guest Credits Left</th>
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td><?php echo $userDetails['User']['first_name'].' '.
                                                                           $userDetails['User']['last_name'];
                                                                ?>
                                                            </td>
                                                            <td>
                                                                <?php echo $userDetails['User']['username'];?>
                                                            </td>
                                                            <td>
                                                                <?php echo $userDetails['User']['email'];?>
                                                            </td>
                                                             <td>
                                                                <?php echo $userDetails['User']['phone'];?>
                                                            </td>
                                                             <td>
                                                                <?php echo $userDetails['User']['address_line_1'].' '.$userDetails['User']['address_line_2']."<br>".$userDetails['User']['city'].' '.$userDetails['User']['state'].' '.$userDetails['User']['zip'];

                                                                ?>
                                                            </td>
                                                            <td>
                                                                <?php echo $userDetails['User']['guest_credits'];?>
                                                            </td>
                                                            <td>
                                                            <?php if($userDetails['User']['archived']==0){
																		echo $this->Form->postLink('Archive',['controller'=>'Users','action'=>'archive_user',$userDetails['User']['id']],['confirm' => __('Are you sure you want to archive this user ?'),'class'=>'btn red']);
																   }else{
																		echo "USER ARCHIVED";
																   }
															 ?>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        <!-- END FORM-->
                                    </div>
                                </div>
                            </div>
                            <div class="portlet box blue">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-gift"></i>Vehicle Details
                                        </div>
                                        <div class="tools">
                                            <a class="collapse" href="javascript:;">
                                            </a>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <!-- BEGIN FORM-->
                                        <div class="table-container">       
                                            <div class="table-responsive">
                                                <table class="table table-striped table-bordered table-hover">
                                                    <thead>
                                                        <tr>
                                                             <th width="5%">#</th>
                                                             <th>Name</th>
                                                             <th>Make</th>
                                                             <th>Model</th>
                                                             <th>Color</th>
                                                             <th>Plate Number</th>
                                                             <th>State</th>
                                                             <th>Vin</th>
                                                             <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                      <?php $srNo=1;foreach($userDetails['Vehicle'] as $vehicle){?>
                                                        <tr>
                                                            <td>
                                                                <?php echo $srNo++; ?>
                                                            </td>
                                                            <td>
                                                                <?php echo $vehicle['owner'];?>
                                                            </td>
                                                            <td>
                                                                <?php echo $vehicle['make'];?>
                                                            </td>
                                                            <td>
                                                                <?php echo $vehicle['model'];?>
                                                            </td>
                                                            <td>
                                                                <?php echo $vehicle['color'];?>
                                                            </td>
                                                            <td>
                                                                <?php echo $vehicle['license_plate_number'];?>
                                                            </td>
                                                            <td>
                                                                <?php echo $vehicle['license_plate_state'];?>
                                                            </td>
                                                            <td>
                                                                <?php echo $vehicle['last_4_digital_of_vin'];?>
                                                            </td>
                                                            <td>
                                                                <?php echo "<a class='btn green btn-xs green-stripe' href='/admin/Vehicles/edit/".$vehicle['id']."'>Edit</a>";?>
                                                            </td>
                                                        </tr>
                                                    <?php }?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <!-- END FORM-->
                                    </div>
                            </div>
                            <div class="portlet box green">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-gift"></i>Passes
                                        </div>
                                        <div class="tools">
                                            <a class="collapse" href="javascript:;">
                                            </a>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <!-- BEGIN FORM-->
                                        <div class="table-container">       
                                            <div class="table-responsive">
                                                <table  class="table table-striped table-bordered table-hover">
                                                    <thead>
                                                        <tr>
                                                             <th width="5%">#</th>
                                                             <th>Parking Location</th>
                                                             <th>Pass Name</th>
                                                             <th>Vehicle Plate Number</th>
                                                             <th>Status</th>
                                                             <th>Parking Location Expiration Date</th>
                                                             <th>Pass Expiration Date</th>
                                                             <th>RFID Tag Number</th>
                                                             <th>Assigned Location</th>
                                                             <th>Last Seen</th>
                                                             <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                      <?php $srNo=1;foreach($userDetails['CustomerPass'] as $pass){?>
                                                        <tr>
                                                            <td>
                                                                <?php echo $srNo++; ?>
                                                            </td>
                                                            <td>
                                                                <?php echo $this->requestAction(array('controller'=>'Packages','action'=>'getPackageNamePassBought',$pass['id']));?>
                                                            </td>
                                                            <td>
                                                                <?php echo $this->requestAction(array('controller'=>'Passes','action'=>'getPName',$pass['id']));?>
                                                            </td>
                                                            <td>
                                                                  <?php
                                                                    if($pass['vehicle_id']==null){
                                                                        echo "No Vehicle Registered"."<br>";
                                                                    }else{
                                                                        $vehiclePlateNumber=$this->requestAction(array('controller'=>'vehicles','action'=>'getVehiclePlateNumber',$pass['vehicle_id']));
                                                                        echo $vehiclePlateNumber['Vehicle']['license_plate_number'];
                                                                    }
                                                                   ?>
                                                            </td>
                                                            <td>
                                                                <?php
                                                                    $dt = new DateTime();
                                                                    $currentDateTime= $dt->format('Y-m-d H:i:s');
                                                                    if(($pass['membership_vaild_upto']==null||$pass['membership_vaild_upto']<$currentDateTime) && $pass['pass_valid_upto']>$currentDateTime){
                                                                        echo "<span class='label label-sm label-info'>Valid Pass</span>";
                                                                    }elseif($pass['pass_valid_upto']<$currentDateTime){
                                                                        echo "<span class='label label-sm label-warning'>Expired Pass</span>";
                                                                    }elseif($pass['membership_vaild_upto']>$currentDateTime && $pass['pass_valid_upto']>$currentDateTime){
                                                                        echo "<span class='label label-sm label-success'>Active Pass</span>";
                                                                    }
                                                                   ?>
                                                            </td>
                                                            <td class="numeric">
                                                                <?php 
                                                                    echo is_null($pass['membership_vaild_upto'])?"Permit Not Bought":date("m/d/Y H:i:s", strtotime($pass['membership_vaild_upto']));
                                                                ?>
                                                            </td>
                                                            <td class="numeric">
                                                                <?php echo date("m/d/Y H:i:s", strtotime($pass['pass_valid_upto']));?>
                                                            </td>
                                                            <td>
                                                                  <?php
                                                                    if($pass['RFID_tag_number']==null){
                                                                        echo "No Tag Assigned"."<br>";
                                                                    }else{
                                                                        echo $pass['RFID_tag_number'];
                                                                    }
                                                                   ?>
                                                            </td>
                                                            <td>
                                                                  <?php
                                                                    if($pass['assigned_location']){
																		echo $pass['assigned_location'];
                                                                        
                                                                    }else{
                                                                        echo "NA";
                                                                    }
                                                                   ?>
                                                            </td>
                                                            <td class="numeric">
                                                                <?php if($pass['last_seen']){
																		echo date("m/d/Y H:i:s", strtotime($pass['last_seen']));
																	  }else{
																		echo "No Data";
																	  }
																	?>
                                                            </td>
                                                            <td>
																<?php echo "<a class='btn green btn-xs green-stripe' href='/admin/CustomerPasses/edit/".$pass['id']."'>Edit</a>";?>
																<?php echo "&nbsp;<a class='btn blue btn-xs blue-stripe' href='javascript:;' onClick='getHistory(".$pass['id'].")'>TAG HISTORY</a>";?>
																<?php echo $this->Form->postLink(__('Delete'), array('controller'=>'CustomerPasses','action' => 'delete', $pass['id']), array('class'=>'btn red btn-xs red-stripe'), __('Are you sure you want to delete this pass?')); ?>
															</td>	
                                                        </tr>
                                                    <?php }?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <!-- END FORM-->
                                    </div>
                                </div>
                                <div class="portlet yellow box">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-gift"></i>Transactions
                                        </div>
                                        <div class="tools">
                                            <a class="collapse" href="javascript:;">
                                            </a>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <!-- BEGIN FORM-->
                                        <div class="table-container">       
                                            <div class="table-responsive">
                                                <table id="transaction_list" class="table table-striped table-bordered table-hover">
                                                    <thead>
                                                        <tr>
                                                             <th>Pay Pal Id</th>
                                                             <th>Date</th>
                                                             <th>Amount</th>
                                                             <th>Result</th>
                                                             <th>Pass</th>
                                                             <th>First Name</th>
                                                             <th>Last Name</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                       <tr>
                                                            <td colspan="5" class="dataTables_empty">Loading Data...</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <!-- END FORM-->
                                    </div>
                                </div>
                            </div>
                            <?php if(isset($userDetails['BillingAddress'][0])){?>
                                <div class="portlet box grey-cascade">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-gift"></i>Billing Address
                                        </div>
                                        <div class="tools">
                                            <a class="collapse" href="javascript:;">
                                            </a>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <!-- BEGIN FORM-->
                                        <div class="table-container">       
                                            <div class="table-responsive">
                                                <table class="table table-striped table-bordered table-hover">
                                                    <thead>
                                                        <tr>
                                                             <th>Name</th>
                                                             <th>Email</th>
                                                             <th>Phone</th>
                                                             <th>Address</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                       <tr>
                                                            <td>
                                                                <?php
                                                                    echo $userDetails['BillingAddress'][0]['first_name'].' '.
                                                                         $userDetails['BillingAddress'][0]['last_name'];
                                                                ?>
                                                            </td>
                                                             <td>
                                                                <?php
                                                                    echo $userDetails['BillingAddress'][0]['email'];
                                                                ?>
                                                            </td>
                                                            <td>
                                                                <?php
                                                                    echo $userDetails['BillingAddress'][0]['phone'];
                                                                ?>
                                                            </td>
                                                            <td>
                                                                <?php
                                                                    echo $userDetails['BillingAddress'][0]['address_line_1'].' '.$userDetails['BillingAddress'][0]['address_line_2'].'<br>'.
                                                                         $userDetails['BillingAddress'][0]['city'].' '.$userDetails['BillingAddress'][0]['state'].'<br>'.
                                                                            $userDetails['BillingAddress'][0]['zip'];
                                                                ?>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <!-- END FORM-->
                                </div>
                            </div>
                            <?php }?>
                            <!--Tickets Start-->
                                <div class="portlet box red">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-gift"></i>Communication History
                                        </div>
                                        <div class="tools">
                                            <a class="collapse" href="javascript:;">
                                            </a>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <!-- BEGIN FORM-->
                                        <div class="table-container">       
                                            <div class="table-responsive">
                                                <table class="table table-striped table-bordered table-hover" id="tickets">
                                                    <thead>
														<tr>

															<th>Subject</th>
															<th>From</th>
															<th>To</th>
															<th>Sent On</th>
															<th>Status</th>
															<th>Action</th>
														</tr>
													</thead>
                                                    <tbody>
                                                       <tr>
                                                            <td colspan="6" align="center">No Data Found</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <!-- END FORM-->
                                </div>
                            </div>
                            <!--Tickets End-->
                            
                           
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PAGE CONTENT-->
        </div>
    </div>
<div id="tag_details_model" class="modal fade" aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close" aria-hidden="true" data-dismiss="modal" type="button"><i class="fa  fa-times "></i></button>
                <h3>Tag History</h3>
            </div>
            <div class="modal-body">
                <div id="tag_details">

                </div>
            </div>
        </div>
    </div>
</div>
<?php echo $this->Html->script('block.js'); ?>
<?php echo $this->Html->script('//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js'); ?>
<?php echo $this->Html->css('//cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css'); ?>
<?php echo $this->Html->script('jquery.validate.min.js'); ?>
<script type="text/javascript">
$(function(){
    $('#transaction_list').dataTable({
        "bProcessing": false,
        "bServerSide": true,
        "sAjaxSource": "<?php echo $this->Html->Url(array('controller' => 'Transactions', 'action' => 'get_transactions',$userDetails['User']['id'])); ?>"
    });
    $('#tickets').dataTable({
		"bProcessing": false,
		"bServerSide": true,
		"bDestroy": true,
		"aoColumns": [
			{"mData":"Ticket.subject"},
			{"mData":"User.first_name"},
			{"mData":"Recipient.first_name"},
			{"mData":"Ticket.created"},
			{"mData":"Ticket.status"},
			{"mData":"Ticket.id"},
		],
		"sAjaxSource":"/admin/Tickets/get_tickets_user/<?php echo $userDetails['User']['id']; ?>"
		//"sAjaxSource":"<?php echo $this->Html->Url(array('controller' => 'SubscriptionPayments', 'action' => 'sub_revenew_table',1)); ?>"
	});
    
});
function getHistory(CustomerPassID){
	console.log(CustomerPassID);
	$.blockUI({ css: { 
						border: 'none', 
						padding: '15px', 
						backgroundColor: '#000', 
						'-webkit-border-radius': '10px', 
						'-moz-border-radius': '10px', 
						opacity: .5, 
						color: '#fff' 
			} }); 
	 $.ajax({
            url: "/admin/CustomerPasses/tag_histories/"+CustomerPassID,
            success: function(rsp) {
                $.unblockUI();
                $("#tag_details").empty();
                $("#tag_details").append(rsp);
                $('#tag_details_model').modal('show');
            },
            error: function(rsp) {
				$.unblockUI();
			}
        });
}

</script> 
