<div class="page-content-wrapper">
		<div class="page-content">
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					Users<small> Add New User</small>
					</h3>
					<ul class="page-breadcrumb breadcrumb">
					
						<li>
							<i class="fa fa-home"></i>
							<?php echo $this->Html->link('Home',array('controller'=>'users','action'=>'superAdminHome')); ?>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<?php echo $this->Html->link('List Users',array('controller'=>'users','action'=>'index')); ?>
							
						</li>
						
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			 <div id="flashMessages">
                	<h2><?php  echo $this->Session->flash();?></h2>
            </div>
<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<div class="tabbable tabbable-custom boxless tabbable-reversed">
						
						
							<div class="tab-pane active" id="tab_0">
								<div class="portlet box green">
									<div class="portlet-title">
										<div class="caption">
											<i class="fa fa-gift"></i><?php echo __('Add User'); ?>
										</div>
										
									</div>
                                   
										<!-- BEGIN FORM-->
										
									<div class="portlet-body form">

									<?php echo $this->Form->create('User',array('class'=>'form-horizontal'));// debug($roles);?>
                                    <div class="form-body">
												
                                                <h2><?php  echo $this->Session->flash();?></h2>
                                            <?php

												echo $this->Form->input('first_name',array('error'=>array('attributes'=>array('class'=>'model-error')),'required'=>true,'div'=>array('class'=>'form-group'),'label'=>array('class'=>'col-md-3 control-label','text'=>'First Name'),'between'=>'<div class="col-md-4">','after'=>'</div>','class'=>'form-control','placeholder'=>'First Name'));
												echo $this->Form->input('last_name',array('error'=>array('attributes'=>array('class'=>'model-error')),'required'=>true,'div'=>array('class'=>'form-group'),'label'=>array('class'=>'col-md-3 control-label','text'=>'Last Name'),'between'=>'<div class="col-md-4">','after'=>'</div>','class'=>'form-control','placeholder'=>'Last Name'));
                                       			echo $this->Form->input('username',array('error'=>array('attributes'=>array('class'=>'model-error')),'required'=>true,'div'=>array('class'=>'form-group'),'label'=>array('class'=>'col-md-3 control-label','text'=>'Username'),'between'=>'<div class="col-md-4">','after'=>'</div>','class'=>'form-control','placeholder'=>'Username'));
												
												echo $this->Form->input('email',array('error'=>array('attributes'=>array('class'=>'model-error')),'required'=>true,'div'=>array('class'=>'form-group'),'label'=>array('class'=>'col-md-3 control-label','text'=>'Email Address'),'between'=>'<div class="col-md-4"><div class="input-group"><span class="input-group-addon"><i class="fa fa-envelope"></i>														</span>','after'=>'</div></div>','class'=>'form-control','placeholder'=>'Email Address','type'=>'email'));
												
												echo $this->Form->input('password',array('error'=>array('attributes'=>array('class'=>'model-error')),'error'=>array('attributes'=>array('class'=>'model-error')),'required'=>true,'div'=>array('class'=>'form-group'),'label'=>array('class'=>'col-md-3 control-label','text'=>'Password'),'between'=>'<div class="col-md-4"><div class="input-group">','after'=>'<span class="input-group-addon"><i class="fa fa-user"></i>													</span></div></div>','class'=>'form-control','placeholder'=>'Password'));
												
												echo $this->Form->input('password_confirmation',array('error'=>array('attributes'=>array('class'=>'model-error')),'error'=>array('attributes'=>array('class'=>'model-error')),'required'=>true,'div'=>array('class'=>'form-group'),'label'=>array('class'=>'col-md-3 control-label','text'=>'Password Confirmation'),'between'=>'<div class="col-md-4"><div class="input-group">','after'=>'<span class="input-group-addon"><i class="fa fa-user"></i>													</span></div></div>','class'=>'form-control','placeholder'=>'Password Confirmation','type'=>'password'));
												
												echo $this->Form->input('role_id',array('error'=>array('attributes'=>array('class'=>'model-error')),'required'=>true,'empty'=>'Select One','div'=>array('class'=>'form-group'),'label'=>array('class'=>'col-md-3 control-label','text'=>'Role'),'between'=>'<div class="col-md-4"><div class="input-group">','after'=>'</div></div>','class'=>'form-control'));

										    ?>
                                           <div class="form-actions fluid">
												<div class="col-md-offset-3 col-md-9">
													<button type="submit" class="btn blue">Submit</button>
													<!--<button type="button" class="btn default">Cancel</button>-->
												</div>
											</div>
                                         <?php echo $this->Form->end(); ?>   
                                     </div>   
                                   
										<!-- END FORM-->
									</div>
								</div>							
								
							</div>
							
							
						
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
</div>
</div>



