
<?php echo $this->Form->create();
    // debug($users);
     $user=$users;

?>
<div class="page-content-wrapper">
<div class="page-content" style="min-height:1213px">
			
			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					User Profile <small><?php echo ($user['User']['first_name']." ".$user['User']['last_name']);?></small>
					</h3>
					<ul class="page-breadcrumb breadcrumb">
					
						<li>
							<i class="fa fa-home"></i>
							<?php echo $this->Html->link(
                                              'Home',
                                              array(
                                                 'controller' => 'Users',
                                                 'action' => 'myAccount',
                                                 'full_base' => true
                                                                  )
                                              );?>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">User Profile</a>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
			<div id="flashMessages">
                	<h2><?php  echo $this->Session->flash();?></h2>
            </div>
			<!-- BEGIN PAGE CONTENT-->
			<div class="row profile">
				<div class="col-md-12">
					<!--BEGIN TABS-->
					<div class="tabbable tabbable-custom tabbable-full-width">
						<ul class="nav nav-tabs">
							<li class="">
								<a data-toggle="tab" href="#tab_1_1">
								Account </a>
							</li>

						</ul>
						<div class="tab-content">
	                    <div id="tab_1_1" class="tab-pane active">
								<div class="row profile-account">
									<div class="col-md-3">
										<ul class="ver-inline-menu tabbable margin-bottom-10">
											<li class="active">
												<a href="#tab_1-1" data-toggle="tab">
												<i class="fa fa-cog"></i> Personal info </a>
												<span class="after">
												</span>
											</li>
											<li class="">
												<a href="#tab_3-3" data-toggle="tab">
												<i class="fa fa-lock"></i> Change Password </a>
											</li>
											<?php if(AuthComponent::user('role_id')==4){ ?>
												<li class="">
													<a href="#tab_3-4" data-toggle="tab">
													<i class="fa fa-bell-o"></i> SMS Notifications </a>
												</li>
											<?php } ?>
										</ul>
									</div>
									<div class="col-md-9">
										<div class="tab-content">
											<div class="tab-pane active" id="tab_1-1">
                                                    
                                                    <?php echo $this->Form->create();
                                                        echo $this->Form->hidden('id',array('default'=>$user['User']['id']));
                                                    ?>
													<div class="form-group">
													    <?php  echo $this->Form->input('first_name',array('class'=>'form-control','value'=>$user['User']['first_name'],'error'=>array('attributes'=>array('class'=>'model-error'))));?>
                                                    </div>
													<div class="form-group">
														 <?php  echo $this->Form->input('last_name',array('class'=>'form-control','value'=>$user['User']['last_name'],'error'=>array('attributes'=>array('class'=>'model-error'))));?>
													</div>
													<div class="form-group">
														<?php  echo $this->Form->input('email',array('class'=>'form-control','value'=>$user['User']['email'],'error'=>array('attributes'=>array('class'=>'model-error'))));?>
													</div>
													<div class="form-group">
														<?php  echo $this->Form->input('address_line_1',array('class'=>'form-control','value'=>$user['User']['address_line_1'],'error'=>array('attributes'=>array('class'=>'model-error'))));?>
                                                          													</div>
													<div class="form-group">
														<?php  echo $this->Form->input('address_line_2',array('label'=>array('text'=>'Apartment/Suite #'),'class'=>'form-control','value'=>$user['User']['address_line_2'],'error'=>array('attributes'=>array('class'=>'model-error'))));?>
													</div>
													<div class="form-group">
														<?php  echo $this->Form->input('city',array('label'=>array('text'=>'City'),'class'=>'form-control','value'=>$user['User']['city'],'error'=>array('attributes'=>array('class'=>'model-error'))));?>
													</div>
													<div class="form-group">
                                                        <?php  echo $this->Form->input('state',array('type' => 'select',
														            'options' => $states,
														            'empty'=>'State','class'=>'form-control','value'=>$user['User']['state'],'error'=>array('attributes'=>array('class'=>'model-error'))));
														?>
													</div>
													<div class="form-group">
														<?php  echo $this->Form->input('zip',array('class'=>'form-control','value'=>$user['User']['zip'],'error'=>array('attributes'=>array('class'=>'model-error'))));?>
													</div>
                                                    <div class="form-group">
														<?php  echo $this->Form->input('phone',array('class'=>'form-control','value'=>$user['User']['phone'],'error'=>array('attributes'=>array('class'=>'model-error'))));?>
													</div>
													 <div class="form-actions fluid">
                                                           <div class="col-md-offset-3 col-md-9">
                                                                  <button type="submit" class="btn green">Submit</button>
                                                           </div>
                                                     </div>
                                                 <?php echo $this->Form->end( ); ?>
											</div>											
											<div class="tab-pane" id="tab_3-3">
												 <?php echo $this->Form->create();?>
													<div class="form-group">
														<?php  echo $this->Form->input('current_password',array('class'=>'form-control','type'=>'password','text'=>'Current Password','required'=>'true','error'=>array('attributes'=>array('class'=>'model-error'))));?>
													</div>
													<div class="form-group">
														<?php  echo $this->Form->input('new_password',array('class'=>'form-control','type'=>'password','required'=>'true','label'=>'New Password','required'=>'true','error'=>array('attributes'=>array('class'=>'model-error'))));?>
													</div>
													<div class="form-group">
														<?php  echo $this->Form->input('confirm_password',array('class'=>'form-control','type'=>'password','required'=>'true','label'=>'Retype New Password','required'=>'true','error'=>array('attributes'=>array('class'=>'model-error'))));?>
													</div>
													 <div class="form-actions fluid">
                                                           <div class="col-md-offset-3 col-md-9">
                                                                 <button type="submit" class="btn green">Change</button>
                                                           </div>
                                                     </div>
                                                 <?php echo $this->Form->end( ); ?>
											</div>
											<?php if(AuthComponent::user('role_id')==4){ ?>
											<div class="tab-pane" id="tab_3-4">
												<div class="alert alert-danger">
													<strong>Current Status : </strong> <?php echo $currentUser['User']['un_subscribed']?"Un-subscribed":"Subscribed"; ?> 
												</div>
												<?php echo $this->Form->create('User',array('class'=>'form-inline','url'=>array('action'=>'smsSubscription')));?>
													<div class="form-group">
														<label class="sr-only" for="exampleInputEmail2">SMS Notifications</label>
														<?php  echo $this->Form->input('un_subscribed',array('class'=>'form-control',
																											 'type'=>'select',
																											 'label'=>FALSE,
																											 'value'=>$currentUser['User']['un_subscribed']?0:1,
																											 'options'=>array('Subscribe','Unsubscribe'),
																											 'required'=>'true',
																											 'error'=>array(
																															'attributes'=>array('class'=>'model-error')
																															)
																											));?>
													</div>
													<button type="submit" class="btn btn-default">Submit</button>
												 <?php echo $this->Form->end( ); ?>
											</div> 
											<?php } ?>
										</div>
									</div>
									<!--end col-md-9-->
								</div>
							</div>
					    </div>
					<!--END TABS-->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>

