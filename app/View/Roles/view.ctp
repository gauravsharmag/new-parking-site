<div class="page-content-wrapper">
    <div class="page-content">
			
    	<!-- BEGIN FORM-->
        
            <div class="form-body">
                <h2 class="margin-bottom-20"> View Role Info -  <?php echo h($role['Role']['role_name']); ?> </h2>
                <h3 class="form-section">Role Info</h3>
                
                <!--/row-->
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Role Name:</label>
                            <div class="col-md-9">
                                <p class="form-control-static">
                                   <?php echo h($role['Role']['role_name']); ?>
                                </p>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                 <!--   <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">ID:</label>
                            <div class="col-md-9">
                                <p class="form-control-static">
                                    <?php echo h($user['User']['id']); ?>
                                </p>
                            </div>
                        </div>
                    </div>-->
                    <!--/span-->
                </div>
               
               
               <br />

                
                
            </div>
           
            <div class="form-actions fluid">
                <div class="row">
                    <div class="col-md-6">
                        <div class="col-md-offset-3 col-md-9">
                          <?php echo $this->Html->link("<i class='fa fa-pencil'></i> Edit",array('controller'=>'roles','action'=>'edit',$role['Role']['id']),array('class'=>'btn green','escape'=>false)); ?>
                            <!--<button type="submit" class="btn green"><i class="fa fa-pencil"></i> Edit</button>
                            <button type="button" class="btn default">Cancel</button>-->
                        </div>
                    </div>
                    <div class="col-md-6">
                    </div>
                </div>
            </div>
     
        <!-- END FORM-->
 


<!--<div class="roles view">
<h2><?php echo __('Role'); ?></h2>
	<dl>

		<dt><?php echo __('Role Name'); ?></dt>
		<dd>
			<?php echo h($role['Role']['role_name']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div>
    <?php echo $this->element('homePage');?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Role'), array('action' => 'edit', $role['Role']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Role'), array('action' => 'delete', $role['Role']['id']), array(), __('Are you sure you want to delete # %s?', $role['Role']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Roles'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Role'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Users'); ?></h3>
	<?php if (!empty($role['User'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Username'); ?></th>
		<th><?php echo __('Email'); ?></th>
		<th><?php echo __('Password'); ?></th>
		<th><?php echo __('Role Id'); ?></th>
		<th><?php echo __('First Name'); ?></th>
		<th><?php echo __('Last Name'); ?></th>
		<th><?php echo __('Address Line 1'); ?></th>
		<th><?php echo __('Address Line 2'); ?></th>
		<th><?php echo __('Street'); ?></th>
		<th><?php echo __('City'); ?></th>
		<th><?php echo __('State'); ?></th>
		<th><?php echo __('Zip'); ?></th>
		<th><?php echo __('Phone'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($role['User'] as $user): ?>
		<tr>
			<td><?php echo $user['id']; ?></td>
			<td><?php echo $user['username']; ?></td>
			<td><?php echo $user['email']; ?></td>
			<td><?php echo $user['password']; ?></td>
			<td><?php echo $user['role_id']; ?></td>
			<td><?php echo $user['first_name']; ?></td>
			<td><?php echo $user['last_name']; ?></td>
			<td><?php echo $user['address_line_1']; ?></td>
			<td><?php echo $user['address_line_2']; ?></td>
			<td><?php echo $user['street']; ?></td>
			<td><?php echo $user['city']; ?></td>
			<td><?php echo $user['state']; ?></td>
			<td><?php echo $user['zip']; ?></td>
			<td><?php echo $user['phone']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'users', 'action' => 'view', $user['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'users', 'action' => 'edit', $user['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'users', 'action' => 'delete', $user['id']), array(), __('Are you sure you want to delete # %s?', $user['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>-->


   </div>	        
</div>
