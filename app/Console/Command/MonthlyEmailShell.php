<?php

App::uses('ComponentCollection', 'Controller');
App::import('Component', 'Email');

class MonthlyEmailShell extends AppShell {

    public function main() {
        //debug($_SERVER);die;
        $domain_name = Configure::read('SITE_URL');
        $collection = new ComponentCollection();
        $email = new EmailComponent($collection);
        
        $month_ini = new DateTime("first day of last month");
		$month_end = new DateTime("last day of last month");
        $toDate = $month_end->format('Y-m-d');
        $fromDate = $month_ini->format('Y-m-d');
        $toDate = $toDate . ' 23:59:59';
        $fromDate = $fromDate . ' 00:00:00';
        
        $conditions = " Transaction.date_time BETWEEN '" . $fromDate . "' AND '" . $toDate . "' ";
        //debug($conditions);die;
        $this->loadModel('UserGuestPass');
        $this->loadModel('CustomerPass');
        $this->loadModel('Transaction');
        $this->loadModel('Property');
        //RENEWAL PASS REPORT
        $properties = $this->Property->find('list');
        $propertyList = array_values($properties);
        $propertyIds = array_keys($properties);
        $array = array();
        for ($i = 0; $i < count($propertyList); $i++) {
            $array[] = array(
                'PropertyName' => $propertyList[$i],
            );
        }

        $this->Transaction->unbindModel(array('hasMany' => 'CustomerPass'));
        $freeRenewedPass = $this->Transaction->find('all', array('conditions' => array('Transaction.comments' => 'passRenewed', 'Transaction.amount' => 0, $conditions),
            'fields' => array('SUM(Transaction.amount)', 'count(*) AS TotalPasses', 'Property.name as PropertyName'),
            'joins' => array(
                array(
                    'table' => 'properties',
                    'alias' => 'Property',
                    'type' => 'LEFT',
                    'conditions' => array(
                        'Property.id=Pass.property_id'
                    )
                )
            ),
            'group' => array('Property.id'),
            'order' => array('Property.id' => 'ASC')
        ));

        $this->Transaction->unbindModel(array('hasMany' => 'CustomerPass'));
        $paidRenewedPass = $this->Transaction->find('all', array('conditions' => array('Transaction.comments' => 'passRenewed', 'Transaction.amount!=0', $conditions),
            'fields' => array('SUM(Transaction.amount)', 'count(*) AS TotalPasses', 'Property.name as PropertyName'),
            'joins' => array(
                array(
                    'table' => 'properties',
                    'alias' => 'Property',
                    'type' => 'LEFT',
                    'conditions' => array(
                        'Property.id=Pass.property_id'
                    )
                )
            ),
            'group' => array('Property.id'),
            'order' => array('Property.id' => 'ASC')
        ));

        $finalArr = array();
        for ($j = 0; $j < count($array); $j++) {
            $array[$j]['PropertyID'] = $propertyIds[$j];
            $array[$j]['FreePassAmount'] = 0;
            $array[$j]['FreePassCount'] = 0;
            $array[$j]['PaidPassAmount'] = 0;
            $array[$j]['PaidPassCount'] = 0;
            $var = 0;
            $counter = 0;
            foreach ($freeRenewedPass as $freePass) {
                if ($freePass['Property']['PropertyName'] == $array[$j]['PropertyName']) {
                    $array[$j]['FreePassAmount'] = $freePass[0]['SUM(`Transaction`.`amount`)'];
                    $array[$j]['FreePassCount'] = $freePass[0]['TotalPasses'];
                }
                $var++;
            }
            foreach ($paidRenewedPass as $paidPass) {
                if ($paidPass['Property']['PropertyName'] == $array[$j]['PropertyName']) {
                    $array[$j]['PaidPassAmount'] = $paidPass[0]['SUM(`Transaction`.`amount`)'];
                    $array[$j]['PaidPassCount'] = $paidPass[0]['TotalPasses'];
                }
                $counter++;
            }
        }
        //NEW PASS BOUGHT AND COST REPORT
        $cost = $this->Transaction->find('all', array(
            'fields' => array('pu.property_id', 'p.name', 'SUM(Transaction.amount) as cost'),
            'joins' => array(
                array(
                    'table' => 'property_users',
                    'alias' => 'pu',
                    'type' => 'LEFT',
                    'conditions' => array(
                        'pu.user_id=Transaction.user_id'
                    )
                ),
                array(
                    'table' => 'properties',
                    'alias' => 'p',
                    'type' => 'LEFT',
                    'conditions' => array(
                        'p.id=pu.property_id'
                    )
                )
            ),
            'group' => array('pu.property_id'),
            'conditions' => $conditions,
            'order' => array('pu.property_id' => 'ASC')
        ));
        $this->Property->recursive = -1;
        $allProperty = $newPassProperty = $this->Property->find('all', array('fields' => array('id', 'name'), 'order' => array('id' => 'ASC')));
        $this->CustomerPass->recursive = -1;
        $passCount = $this->CustomerPass->find('all', array('fields' => array('property_id', 'count(*) as passcount'),
            'conditions' => array('RFID_tag_number IS NOT NULL and created BETWEEN "' . $fromDate . '" AND "' . $toDate . '"'),
            'group' => array('CustomerPass.property_id'),
            'order' => array('CustomerPass.property_id' => 'ASC')
                )
        );
        $totalPasses = $totalAmount = 0;
        for ($i = 0; $i < count($newPassProperty); $i++) {
            $newPassProperty[$i]['Property']['cost'] = 0;
            $newPassProperty[$i]['Property']['passCount'] = 0;
            for ($j = 0; $j < count($cost); $j++) {
                if ($cost[$j]['pu']['property_id'] == $newPassProperty[$i]['Property']['id']) {
                    $newPassProperty[$i]['Property']['cost'] = $cost[$j][0]['cost'];
                    $totalAmount = $totalAmount + $cost[$j][0]['cost'];
                }
            }
            for ($k = 0; $k < count($passCount); $k++) {
                if ($passCount[$k]['CustomerPass']['property_id'] == $newPassProperty[$i]['Property']['id']) {
                    $newPassProperty[$i]['Property']['passCount'] = $passCount[$k][0]['passcount'];
                    $totalPasses = $totalPasses + $passCount[$k][0]['passcount'];
                }
            }
        }

        //GUestPass
        $totalPaid = $totalDays = $totalFreedays = $totalAmt = 0;
        $this->UserGuestPass->recursive = -1;
        $conditions = " created BETWEEN '" . $fromDate . "' AND '" . $toDate . "' ";
        $data = $this->UserGuestPass->find('all', array('conditions' => array($conditions), 'fields' => array('sum(days) AS days', 'sum(paid) AS paid', 'sum(free) AS free', 'sum(amount) AS amount', 'property_id'), 'group' => 'property_id', 'order' => array('property_id' => 'ASC')));

        for ($i = 0; $i < count($allProperty); $i++) {
            for ($j = 0; $j < count($data); $j++) {
                if ($data[$j]['UserGuestPass']['property_id'] == $allProperty[$i]['Property']['id']) {
                    $allProperty[$i]['Property']['days'] = $data[$j][0]['days'];
                    $totalDays = $totalDays + $data[$j][0]['days'];
                    $allProperty[$i]['Property']['paidDays'] = $data[$j][0]['paid'];
                    $totalPaid = $totalPaid + $data[$j][0]['paid'];
                    $allProperty[$i]['Property']['freeDays'] = $data[$j][0]['days'] - $data[$j][0]['paid'];
                    $totalFreedays = $totalFreedays + ($data[$j][0]['days'] - $data[$j][0]['paid']);
                    $allProperty[$i]['Property']['paidAmount'] = $data[$j][0]['amount'];
                    $totalAmt+=$data[$j][0]['amount'];
                }
            }
            if (!isset($allProperty[$i]['Property']['days'])) {
                $allProperty[$i]['Property']['days'] = NULL;
                $allProperty[$i]['Property']['paidDays'] = NULL;
                $allProperty[$i]['Property']['freeDays'] = NULL;
                $allProperty[$i]['Property']['paidAmount'] = NULL;
            }
        }
        //debug($array);
        //debug($newPassProperty);
        //debug($allProperty);
        //$this->set(compact('array', 'toDate', 'fromDate', 'newPassProperty', 'totalAmt', 'allProperty', 'totalAmount', 'totalPaid', 'totalDays', 'totalFreedays', 'totalPasses'));
        $Email = new CakeEmail();
        $Email->config('smtp');
        $Email->template('monthly', 'default');
        $Email->emailFormat('html');
        $Email->viewVars(compact('array', 'toDate', 'fromDate', 'newPassProperty', 'totalAmt', 'allProperty', 'totalAmount', 'totalPaid', 'totalDays', 'totalFreedays', 'totalPasses'));
        $Email->from(array('noreply@' . $domain_name => Configure::read('SITE')));
        $Email->to(array('shalender@netgen.in','raj@netgen.in','david@5hmg.com'));
        $Email->subject(Configure::read('SITE').' Reports: ' . date('F d, Y', strtotime($fromDate)) . ' TO ' . date('F d, Y', strtotime($toDate)));
        if ($Email->send()) {
            CakeLog::write('MonthlyReport', 'mail sent');
        } else {
            CakeLog::write('MonthlyReport', 'mail not sent');
        }
    }

}
