<?php
class RebuildAroShell extends AppShell {

    public $uses = array('User', 'Role');

    public function main() {
		$this->Role->recursive=-1;
        $groups = $this->Role->find('all');
        $this->User->recursive=-1;
        $users = $this->User->find('all',array('fields'=>array('id','role_id')));

        $aro = new Aro();

        foreach ($groups as $group) {
            $aro->create();
            $aro->save(array(
                'foreign_key' => $group['Role']['id'],
                'model' => 'Role',
                'parent_id' => null
            ));
        }

        $aros = array();

        // Index
        $i = 0;

        foreach ($users as $user) {
            $aros[$i++] = array(
                'foreign_key' => $user['User']['id'],
                'model' => 'User',
                'parent_id' => $user['User']['role_id']
            );
        }

        foreach ($aros as $data) {
            $aro->create();
             try{
				$aro->save($data);
			 }catch(Exception $e){
				debug($e);die;
			 }
        }
    }

}
