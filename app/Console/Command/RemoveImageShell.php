<?php
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');

class RemoveImageShell extends AppShell {
	
    public function main() {
		$this->loadModel('Property');
		$this->Property->recursive=-1;
		$allProperty=$this->Property->find('all');
		$fileName=[];
		foreach($allProperty as $property){
			$fileName[]=$property['Property']['logo'];
		}
		$dir = new Folder(WWW_ROOT.'/img/logo');
		$allFiles=$dir->read(false);
		foreach($allFiles[1] as $img){
			if(!in_array($img,$fileName)){
				$name=WWW_ROOT.'img/logo/'.$img;
				$file = new File($name);
				$file->delete();
			}
		}
	}
	
}
