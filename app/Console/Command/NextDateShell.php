<?php
App::uses('ComponentCollection', 'Controller');
App::uses('PayFlowComponent', 'Controller/Component');

class NextDateShell extends AppShell {
	
    public function main() {
		
        $this->loadModel('RecurringProfile');
        $this->loadModel('CustomerPass');
		$output=array();
		$output = $this->RecurringProfile->find('all',array('conditions'=>array('status'=>'Active')));
		$dt = new DateTime();
		CakeLog::write('nextDateRecurringCron',' Triggered At '.$dt->format('Y-m-d H:i:s'));
		$collection = new ComponentCollection();
        $this->PayFlow = new PayFlowComponent($collection);
		//debug($output);die;
		for($i=0;$i<count($output);$i++){
			$result=$this->PayFlow->enquiry_profile($output[$i]['RecurringProfile']['recurring_profile_id']);
			//debug($result);
			if($result){
				if($result['STATUS'] == 'ACTIVE'){
					//debug($result['NEXTPAYMENT']);
					$month= substr($result['NEXTPAYMENT'],0,2);
					$day= substr($result['NEXTPAYMENT'],2,2);
					$year= substr($result['NEXTPAYMENT'],4,4);
					$date=$year.'-'.$month.'-'.$day;
					$array['RecurringProfile']=array('id'=>$output[$i]['RecurringProfile']['id'],
													 'next_payment_date'=>$date,
													 'customer_pass_id'=>$this->CustomerPass->field('id',array('recurring_profile_id'=>$output[$i]['RecurringProfile']['recurring_profile_id'])),
													 'paid'=>0
													);
					if($this->RecurringProfile->save($array)){
						CakeLog::write($output[$i]['RecurringProfile']['recurring_profile_id'],'Next date Set to '.$date);
					}else{
						CakeLog::write($output[$i]['RecurringProfile']['recurring_profile_id'],'Next date Set Failure date was '.$date);
					}
					//debug($array);die;
				}else{
					$this->CustomerPass->unbindModel(array('belongsTo'=>array('User','Vehicle','Property','Transaction','Package','Pass')));
					$this->CustomerPass->updateAll(array('recurring_profile_status' => "'".$result['STATUS']."'"),
											array('recurring_profile_id' => $output[$i]['RecurringProfile']['recurring_profile_id'])
											
										);
				}
			}
			//debug($result);die;
		}
    }
}
