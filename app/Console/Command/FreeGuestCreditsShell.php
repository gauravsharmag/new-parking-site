<?php

class FreeGuestCreditsShell extends AppShell {
	
    public function main() {
		$month = date("m");
        $year = date("Y");
        $this->loadModel('Cron');
        $this->loadModel('User');
        $dt = new DateTime();
        $currentDateTime = $dt->format('m/d/Y H:i:s');
        $current_cron = $this->Cron->find('first', array('conditions' => array('month' => $month, 'year' => $year)));
        if (empty($current_cron)) {
            CakeLog::write('NoCronFound', $currentDateTime . ' : Cron Creation Attempt On : ' . $currentDateTime);
            $array['Cron'] = array('month' => $month, 'year' => $year);
            $this->Cron->create();
            if ($this->Cron->save($array)) {
				CakeLog::write('NoCronFound', $currentDateTime . ' : Cron Creation Attempt On : ' . $currentDateTime . ' was successful');
                $cronId = $this->Cron->getLastInsertId();
                $this->loadModel('Property');
                $this->Property->recursive = -1;
                $allProperties = $this->Property->find('all', array('fields' => array('id', 'name', 'free_guest_credits'),
																	'conditions'=>array(
																						'archived'=>0
																	)
                ));
                
                $this->loadModel('CronJob');
                for ($i = 0; $i < count($allProperties); $i++) {
                    $arr['CronJob'] = array('cron_id' => $cronId, 'property_id' => $allProperties[$i]['Property']['id'], 'credits_given' => $allProperties[$i]['Property']['free_guest_credits']);
                    $this->CronJob->create();
                    $this->CronJob->save($arr);
                }
                $this->loadModel('PropertyUser');
                for ($i = 0; $i < count($allProperties); $i++) {
					$allPropertyUsersToUPdate=$this->PropertyUser->find('all',array('conditions'=>array(
																										'property_id'=> $allProperties[$i]['Property']['id'],
																										'PropertyUser.role_id'=>4,
																										'User.archived'=>0	
																									)));
					
					$usersInProperty=array();
					foreach($allPropertyUsersToUPdate as $userInfo){
						$this->User->clear();
						$this->User->id=$userInfo['User']['id'];
						$returnRes=$this->User->saveField('guest_credits',$allProperties[$i]['Property']['free_guest_credits']);
						$usersInProperty[]=$userInfo['User']['id'];
					}
					$this->User->clear();
					$pendingUser=$this->User->find('all',array(
														'conditions'=>array(
															'User.guest_credits != '.$allProperties[$i]['Property']['free_guest_credits'],
															'User.id'=>$usersInProperty
														)
													));
					
                    if (!$pendingUser) {
                        $this->CronJob->updateAll(array('completed' => 1), array('cron_id' => $cronId, 'property_id' => $allProperties[$i]['Property']['id']));
                        CakeLog::write('guestCreditsUpdated', 'guestCreditsUpdated : All guest credits updated for property : ' . $allProperties[$i]['Property']['name'] . ' credits assigned :' . $allProperties[$i]['Property']['free_guest_credits'] . '');
                    } else {
                        CakeLog::write('guestCreditsUpdateFailed', 'guestCreditsUpdateFailed : All guest credits updation for property : ' . $allProperties[$i]['Property']['name'] . ' credits assigned :' . $allProperties[$i]['Property']['free_guest_credits'] . ' failed');
                    }
                }
                if (!($this->CronJob->hasAny(array('completed' => 0, 'cron_id' => $cronId)))) {
                    $this->Cron->updateAll(array('completed' => 1), array('id' => $cronId));
                    CakeLog::write('NoCronFound', $currentDateTime . ' : All guest credits updated On : ' . $currentDateTime . ' was successful');
                } else {
                    
                }
            } else {
                CakeLog::write('NoCronFound', $currentDateTime . ' : Cron Creation Attempt On : ' . $currentDateTime . ' was un-successful');
            }
        } else {
            if (!$current_cron['Cron']['completed']) {
                CakeLog::write('CronFound', $currentDateTime . ' : Cron Found On : ' . $currentDateTime . ' but was not completed');
                $this->update_cron($current_cron['Cron']['id']);
            } else {
                CakeLog::write('CronFound', $currentDateTime . ' : Cron Found On : ' . $currentDateTime . ' but was completed');
            }
        }
	}
	public function update_cron($cron_id) {
        $cronId = $cron_id;
        $this->loadModel('CronJob');
        $this->loadModel('User');
        $this->loadModel('PropertyUser');
        $cronJobs = $this->CronJob->find('all', array('conditions' => array('cron_id' => $cronId, 'completed' => 0)));
        $dt = new DateTime();
        $currentDateTime = $dt->format('m/d/Y H:i:s');
        if ($cronJobs) {
            CakeLog::write('CronFound', $currentDateTime . ' : Cron Found On : ' . $currentDateTime . ' Attempted to update cron jobs for Cron Id : ' . $cronId);
            $this->LoadModel('Property');
            for ($i = 0; $i < count($cronJobs); $i++) {
				$allPropertyUsersToUPdate=$this->PropertyUser->find('all',array('conditions'=>array(
																										'property_id'=> $cronJobs[$i]['CronJob']['property_id'],
																										'PropertyUser.role_id'=>4,
																										'User.archived'=>0	
																									)));
																									
                $credits = $this->Property->field('free_guest_credits', array('id' => $cronJobs[$i]['CronJob']['property_id']));
                $property_name = $this->Property->field('name', array('id' => $cronJobs[$i]['CronJob']['property_id']));
               
				$usersInProperty=array();
				foreach($allPropertyUsersToUPdate as $userInfo){
					$this->User->clear();
					$this->User->id=$userInfo['User']['id'];
					$returnRes=$this->User->saveField('guest_credits',$credits);
					$usersInProperty[]=$userInfo['User']['id'];
				}
				$this->User->clear();
				$pendingUser=$this->User->find('all',array(
													'conditions'=>array(
														'User.guest_credits != '.$credits,
														'User.id'=>$usersInProperty
													)
												));
               
                if (!$pendingUser) {
                    $this->CronJob->updateAll(array('completed' => 1), array('cron_id' => $cronId, 'property_id' => $cronJobs[$i]['CronJob']['property_id']));
                    CakeLog::write('guestCreditsUpdated', 'guestCreditsUpdated : All guest credits updated for property name : ' . $property_name . ' credits assigned :' . $credits . ' under cron ID: ' . $cronId);
                } else {
                    CakeLog::write('guestCreditsUpdateFailed', 'guestCreditsUpdateFailed : All guest credits updation for property name : ' . $property_name . ' credits assigned :' . $credits . ' failed under cron ID:' . $cronId);
                }
            }
            if (!($this->CronJob->hasAny(array('completed' => 0, 'cron_id' => $cronId)))) {
                if ($this->Cron->updateAll(array('completed' => 1), array('id' => $cronId))) {
                    CakeLog::write('CronFound', $currentDateTime . ' : All guest credits updation attempt On : ' . $currentDateTime . ' was successful for cron ID: ' . $cronId);
                } else {
                    CakeLog::write('CronFound', $currentDateTime . ' : All guest credits updation attempt On : ' . $currentDateTime . ' was un-successful for Cron ID: ' . $cronId);
                }
            }
        } else {
            if ($this->Cron->updateAll(array('completed' => 1), array('id' => $cronId))) {
                CakeLog::write('CronFound', $currentDateTime . ' : All guest credits updation attempt On : ' . $currentDateTime . ' was already completed for cron ID : ' . $cronId);
            } else {
                CakeLog::write('CronFound', $currentDateTime . ' : All guest credits updation attempt On : ' . $currentDateTime . ' was already completed for cron ID : ' . $cronId . ' .But Cron Has not been been marked completed ');
            }
        }
    }
}
