<?php

App::import('Core', 'Helper');
App::uses('ComponentCollection', 'Controller');
App::import('Component', 'Email');
App::import('Component', 'SmsNotification');

class ExpirationAlertShell extends AppShell {

    public function main() {  
        $domain_name = Configure::read('SITE_URL');
        $collection = new ComponentCollection();
        $email = new EmailComponent($collection);
        $sms = new SmsNotificationComponent($collection);
        $this->loadModel('CustomerPass');
        $dt = date("Y-m-d");
        $all_cp = $this->CustomerPass->find('all', array('fields' => 'CustomerPass.*,Pass.name as Pass_name,Property.name as Property_name,Property.sub_domain, Property.logo,User.first_name, User.last_name, User.email, User.un_subscribed, User.phone_verified, User.id, User.phone',
														   'conditions'=>array(
																				'OR'=>array(
																							array('date(CustomerPass.pass_valid_upto) BETWEEN ? AND ?' => array($dt, date( "Y-m-d", strtotime("$dt +14 day")))), 
																							array('date(CustomerPass.pass_valid_upto)'=>array($dt, date( "Y-m-d", strtotime("$dt -1 day"))))
																				),
																				'CustomerPass.pass_archived'=>0
															)
        )); 
          foreach ($all_cp as $cp) {
            if ($cp['Pass']['Pass_name'] && $cp['User']['email'] && $cp['Property']['Property_name']) {
                $now = strtotime(date('Y-m-d'));
                $your_date = strtotime($cp['CustomerPass']['pass_valid_upto']);
                $datediff = $your_date - $now;
                $remaining_days = floor($datediff / (60 * 60 * 24));
                 if ($remaining_days == 14) {
                    $Email = new CakeEmail();
                    $Email->config('smtp');
                    $Email->template('before_14_days', 'default');
                    $Email->emailFormat('html');
                    $Email->viewVars(array('cp' => $cp));
                    $Email->from(array('noreply@' . $domain_name => Configure::read('SITE')));
                    $Email->to($cp['User']['email']);

                    $Email->subject('Pass Expiration Alert');
					if($cp['User']['un_subscribed']==0){   
						if($cp['User']['phone_verified']==1){   
							$message='This is a message from Online Parking Pass - your parking pass '.$cp['Pass']['Pass_name'].' with '.$cp['Property']['Property_name'].' will be expiring in '.$remaining_days.' days.';
							$sms->sendMessage($cp['User']['id'],$message,$cp['User']['phone']);
						}
					}
                    if ($Email->send()) {
                        debug('mail sent 14');
                        CakeLog::write('14DaysBeforeExpirationMailSent', 'mail sent to: ' . $cp['User']['email']);
                    } else {
                        CakeLog::write('14DaysBeforeExpirationMailSent', 'mail not sent to: ' . $cp['User']['email']);
                    }
                } else if ($remaining_days == 7) {
                    $Email = new CakeEmail();
                    $Email->config('smtp');
                    $Email->template('before_week_expiry_alert', 'default');
                    $Email->emailFormat('html');
                    $Email->viewVars(array('cp' => $cp));
                    $Email->from(array('noreply@' . $domain_name => Configure::read('SITE')));
                    $Email->to($cp['User']['email']);

                    $Email->subject('Pass Expiration Alert');
					if($cp['User']['un_subscribed']==0){   
						if($cp['User']['phone_verified']==1){   
							$message='This is a message from Online Parking Pass - your parking pass '.$cp['Pass']['Pass_name'].' with '.$cp['Property']['Property_name'].' will be expiring in '.$remaining_days.' days.';
							$sms->sendMessage($cp['User']['id'],$message,$cp['User']['phone']);
						}
					}
                    if ($Email->send()) {
                        debug('mail sent 7');
                        CakeLog::write('7DaysBeforeExpirationMailSent', 'mail sent to: ' . $cp['User']['email']);
                    } else {
                        CakeLog::write('7DaysBeforeExpirationMailSent', 'mail not sent to: ' . $cp['User']['email']);
                    }
                } else if ($remaining_days == 3) {
                    $Email = new CakeEmail();
                    $Email->config('smtp');
                    $Email->template('before_3_days_expiry_alert', 'default');
                    $Email->emailFormat('html');
                    $Email->viewVars(array('cp' => $cp));
                    $Email->from(array('noreply@' . $domain_name => Configure::read('SITE')));
                    $Email->to($cp['User']['email']);
                    $Email->subject('Pass Expiration Alert');
                    if($cp['User']['un_subscribed']==0){   
						if($cp['User']['phone_verified']==1){   
							$message='This is a message from Online Parking Pass - your parking pass '.$cp['Pass']['Pass_name'].' with '.$cp['Property']['Property_name'].' will be expiring in '.$remaining_days.' days.';
							$sms->sendMessage($cp['User']['id'],$message,$cp['User']['phone']);
						}
					}
                    if ($Email->send()) {
                        debug('mail sent 3');
                        CakeLog::write('3DaysBeforeExpirationMailSent', 'mail sent to: ' . $cp['User']['email']);
                    } else {
                        CakeLog::write('3DaysBeforeExpirationMailSent', 'mail not sent to: ' . $cp['User']['email']);
                    }
                } else if ($remaining_days == 0) {
                    $Email = new CakeEmail();
                    $Email->config('smtp');
                    $Email->template('today_expiry_alert', 'default');
                    $Email->emailFormat('html');
                    $Email->viewVars(array('cp' => $cp));
                    $Email->from(array('noreply@' . $domain_name => Configure::read('SITE')));
                    $Email->to($cp['User']['email']);
                    $Email->subject('Pass Expiration Alert');
                    if($cp['User']['un_subscribed']==0){   
						if($cp['User']['phone_verified']==1){  
							$message='This is a message from Online Parking Pass - your parking pass '.$cp['Pass']['Pass_name'].' with '.$cp['Property']['Property_name'].' will expire today.';
							$sms->sendMessage($cp['User']['id'],$message,$cp['User']['phone']);
						}
					}
                    if ($Email->send()) {
                        debug('mail sent 0');
                        CakeLog::write('TodayExpirationAlertMailSent', 'mail sent to: ' . $cp['User']['email']);
                    } else {
                        CakeLog::write('TodayExpirationAlertMailSent', 'mail not sent to: ' . $cp['User']['email']);
                    }
                } else if ($remaining_days == -1) {
                    $Email = new CakeEmail();
                    $Email->config('smtp');
                    $Email->template('pass_expired_alert', 'default');
                    $Email->emailFormat('html');
                    $Email->viewVars(array('cp' => $cp));
                    $Email->from(array('noreply@' . $domain_name => Configure::read('SITE')));
                    $Email->to($cp['User']['email']);
                    $Email->subject('Pass Expiration Alert');
                    if($cp['User']['un_subscribed']==0){   
						if($cp['User']['phone_verified']==1){ 
							$message='This is a message from Online Parking Pass - your parking pass '.$cp['Pass']['Pass_name'].' with '.$cp['Property']['Property_name'].' has been expired.';
							$sms->sendMessage($cp['User']['id'],$message,$cp['User']['phone']);
						}
					}
                    if ($Email->send()) {
                        debug('mail sent -1');
                        CakeLog::write('ExpiredAlertMailSent', 'mail sent to: ' . $cp['User']['email']);
                    } else {
                        CakeLog::write('ExpiredAlertMailSent', 'mail not sent to: ' . $cp['User']['email']);
                    }
                }
            } else {
                debug('no result found');
            }
        }
        echo "�";
        exit;
    }

}
