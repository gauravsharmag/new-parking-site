<?php
App::uses('ComponentCollection', 'Controller');
App::uses('PayFlowComponent', 'Controller/Component');
App::import('Component', 'Email');

class UpdatePassShell extends AppShell {
   public function main() {
	  $dt = new DateTime();
	  $this->loadModel('CustomerPass');
	  $currentDate= $dt->format('Y-m-d');
	  CakeLog::write('updatePassRecurringCron',' Triggered At '.$dt->format('Y-m-d H:i:s'));
	  $this->loadModel('RecurringProfile');
	  $output = $this->RecurringProfile->find('all',array('conditions'=>array('status'=>'Active','paid'=>0)));
	  $collection = new ComponentCollection();
	  $this->PayFlow = new PayFlowComponent($collection);
	  for($i=0;$i<count($output);$i++){
		$result=$this->PayFlow->enquiry_profile_payment($output[$i]['RecurringProfile']['recurring_profile_id']);
		//debug($output[$i]['RecurringProfile']['recurring_profile_id']);
		//debug($result);
		$searchResult=NULL;
		if($result){
			$currentDateToSearch=$dt->format('d-M-y');
			$searchResult = array_filter(array_values($result), function ($item) use ($currentDateToSearch) {
											if (stripos($item, $currentDateToSearch) !== false) {
												return true;
											}else{
												return false;
											}
										}
								);
		}
		//debug($searchResult); 
		if($searchResult){
			$valuelist=array_values($searchResult);
			$index=substr(array_search($valuelist[count($valuelist)-1],$result),-1);
			if($result['P_RESULT'.$index]==0 && $result['P_TRANSTATE'.$index]==8){
				$this->CustomerPass->recursive=-1;
				$customerPassData=$this->CustomerPass->find('first',array('fields'=>array('pass_id','id','user_id'),'conditions'=>array('recurring_profile_id'=>$output[$i]['RecurringProfile']['recurring_profile_id'])));
				if($customerPassData){
					if(!empty($customerPassData)){
						$this->loadModel('Pass');
						$passDetails=$packageDetails=array();
						$this->Pass->recursive=-1;
						$passDetails=$this->Pass->find('first',array('conditions'=>array('id'=>$customerPassData['CustomerPass']['pass_id'])));
						if($passDetails){
							if($passDetails['Pass']['is_fixed_duration']==0){
								$dt = new DateTime();
								$currentDateTime= $dt->format('Y-m-d H:i:s');
								switch ($passDetails['Pass']['duration_type']) 
									{
															case "Hour":
															$date = new DateTime($currentDateTime);
															$k=(int)$passDetails['Pass']['duration'];
															$date->add(new DateInterval('PT'.$k.'H'));
															$passDetails['Pass']['expiration_date']=date_format($date, 'Y-m-d H:i:s');
															break;
															case "Day":
																$date = new DateTime($currentDateTime);
																$k=$passDetails['Pass']['duration'];
																$date->add(new DateInterval('P'.$k.'D'));
																$passDetails['Pass']['expiration_date']=date_format($date, 'Y-m-d H:i:s');
															break;
															case "Week":
																$date = new DateTime($currentDateTime);
																$k=(int)$passDetails['Pass']['duration']*7;
																$date->add(new DateInterval('P'.$k.'D'));
																$passDetails['Pass']['expiration_date']=date_format($date, 'Y-m-d H:i:s');
															break;
															case "Month":
																$date = new DateTime($currentDateTime);
																$k=(int)$passDetails['Pass']['duration'];
																$date->add(new DateInterval('P'.$k.'M'));
																$passDetails['Pass']['expiration_date']=date_format($date, 'Y-m-d H:i:s');
															break;
															case "Year":
																$date = new DateTime($currentDateTime);
																$k=(int)$passDetails['Pass']['duration'];
																$date->add(new DateInterval('P'.$k.'Y'));
																$passDetails['Pass']['expiration_date']=date_format($date, 'Y-m-d H:i:s');
															break;
									}
							}
							if($passDetails['Pass']['package_id']){
											$this->loadModel('Package');
											$this->Package->recursive=-1;
											$packageDetails=$this->Package->find('first',array('conditions'=>array('id'=>$passDetails['Pass']['package_id'])));
												if($packageDetails['Package']['is_fixed_duration']==0){
														$dt = new DateTime();
														$currentDateTime= $dt->format('Y-m-d H:i:s');
														switch ($packageDetails['Package']['duration_type']) 
															{
																case "Hour":
																	$date = new DateTime($currentDateTime);
																	$k=(int)$packageDetails['Package']['duration'];
																	$date->add(new DateInterval('PT'.$k.'H'));
																	$packageDetails['Package']['expiration_date']=date_format($date, 'Y-m-d H:i:s');
																break;
																case "Day":
																	$date = new DateTime($currentDateTime);
																	$k=$packageDetails['Package']['duration'];
																	$date->add(new DateInterval('P'.$k.'D'));
																	$packageDetails['Package']['expiration_date']=date_format($date, 'Y-m-d H:i:s');
																break;
																case "Week":
																	$date = new DateTime($currentDateTime);
																	$k=(int)$packageDetails['Package']['duration']*7;
																	$date->add(new DateInterval('P'.$k.'D'));
																	$packageDetails['Package']['expiration_date']=date_format($date, 'Y-m-d H:i:s');
																break;
																case "Month": 
																	$date = new DateTime($currentDateTime);
																	$k=(int)$packageDetails['Package']['duration'];
																	$date->add(new DateInterval('P'.$k.'M'));
																	$packageDetails['Package']['expiration_date']=date_format($date, 'Y-m-d H:i:s');
																break;
																case "Year":
																	$date = new DateTime($currentDateTime);
																	$k=(int)$packageDetails['Package']['duration'];
																	$date->add(new DateInterval('P'.$k.'Y'));
																	$packageDetails['Package']['expiration_date']=date_format($date, 'Y-m-d H:i:s');
																break;
															}
												} 
									$date3 = new DateTime($packageDetails['Package']['expiration_date']);
									$datePack=$date3->format('Y-m-d');
									$packageDetails['Package']['expiration_date']= $datePack.' 23:59:59';
								}else{
											CakeLog::write('noPackageFoundForRecurringProfile', $output[$i]['RecurringProfile']['recurring_profile_id']." : No Package Found for this Recurring ID ");
								}
								$dt = new DateTime();
								$currentDateTime= $dt->format('Y-m-d H:i:s');
								$response=array('AMT'=>$result['P_AMT'.$index],
														'ACK'=>'Success',
												);
								$arraySerialize=serialize($response);
								$dt = new DateTime();
								$currentDateTime= $dt->format('Y-m-d H:i:s');
								$transactionArray['Transaction']['paypal_tranc_id']=$result['P_PNREF'.$index];
								$transactionArray['Transaction']['user_id']=$customerPassData['CustomerPass']['user_id'];
								$transactionArray['Transaction']['date_time']=$currentDateTime;
								$transactionArray['Transaction']['amount']=$response['AMT'];
								$transactionArray['Transaction']['result']=$response['ACK'];
								$transactionArray['Transaction']['pass_id']=$passDetails['Pass']['id'];
								$transactionArray['Transaction']['transaction_array']=$arraySerialize;
								$transactionArray['Transaction']['first_name']='recurring_first_name';
								$transactionArray['Transaction']['last_name']='recurring_last_name';
								$transactionArray['Transaction']['message']='This is a recurring payment for recurring ID : '.$output[$i]['RecurringProfile']['recurring_profile_id'];
								$transactionArray['Transaction']['comments'] = "passActivated";
								$transactionArray['Transaction']['recurring_id'] = $output[$i]['RecurringProfile']['recurring_profile_id'];
								$transactionArray['Transaction']['transaction_type'] = 1;	
								$this->loadModel('Transaction');
								if ($this->Transaction->save($transactionArray,false)) {
									$transactionId=$this->Transaction->getLastInsertId();
									$this->loadModel('CustomerPass');
									$previousId=$this->CustomerPass->field('vehicle_id',array('id'=>$customerPassData['CustomerPass']['id']));
									$packageId=$packageExpiry=null;
									if($passDetails['Pass']['package_id']){
										if($packageDetails['Package']['is_guest']==0){
											$packageId=$packageDetails['Package']['id'];
											$packageExpiry=$packageDetails['Package']['expiration_date'];
										}
									}
									$array['CustomerPass']=array(
											  'id'=>$customerPassData['CustomerPass']['id'],
										 // 'pass_id'=>$passDetails['Pass']['id'],
										  //'pass_valid_upto'=>$passDetails['Pass']['expiration_date'],
											  'membership_vaild_upto'=>$packageExpiry,
											  'package_id'=>$packageId,
											  'transaction_id'=>$transactionId
									);
									if($this->CustomerPass->save($array,false)){
										//$this->loadModel('RecurringProfile');
										$arr['RecurringProfile']=array('id'=>$output[$i]['RecurringProfile']['id'],
																		'paid'=>1
										);
										if($this->RecurringProfile->save($arr,false)){
											$passName=$this->Pass->givePassName($passDetails['Pass']['id']);
											CakeLog::write('recurringPayment', $output[$i]['RecurringProfile']['recurring_profile_id']." : Recurring Payment Done ");
											CakeLog::write('passRenewedRecurring', $output[$i]['RecurringProfile']['recurring_profile_id'].' : Pass Activated with Pass ID: '.$passDetails['Pass']['id'].' and Pass Name: '.$passName.', and Customer Pass ID: <a href="/admin/CustomerPasses/edit/'.$customerPassData['CustomerPass']['id'].'">'.$customerPassData['CustomerPass']['id'].'</a> renewed by Recurring Payment, Amount Paid: $'.$response['AMT'].'');
										}else{
											CakeLog::write('PaidNotSaved',  $output[$i]['RecurringProfile']['recurring_profile_id']." : Paid Not Saved For this Recurring ID ");
										}
									}else{
										CakeLog::write('passRenewedRecurringFailed',$output[$i]['RecurringProfile']['recurring_profile_id'].' Pass renewal failed.');
									}
								}else{
									CakeLog::write('transactionNotSaved',  $output[$i]['RecurringProfile']['recurring_profile_id']." : Transaction Data Not Saved For this Recurring ID ");
								}
						}else{
								CakeLog::write('noPassFoundForRecurringProfile', $output[$i]['RecurringProfile']['recurring_profile_id']." : No Pass Found for this Recurring ID ");
						}
					}else{
						CakeLog::write('noRecurringProfileFound', $output[$i]['RecurringProfile']['recurring_profile_id']." : No Customer Pass Found for this Recurring ID ");
					}
				}else{
					CakeLog::write('noRecurringProfileFound', $output[$i]['RecurringProfile']['recurring_profile_id']." : No Customer Pass Found for this Recurring ID ");
				}
			}else{
				 $this->CustomerPass->unbindModel(array('belongsTo'=>array('Vehicle','Transaction')));
				  $customerPassDetails=$this->CustomerPass->find('first',array('conditions'=>array('CustomerPass.id'=>$output[$i]['RecurringProfile']['customer_pass_id'])));
				  if($customerPassDetails){
					$this->Email->to = array('shalender@netgen.in',$customerPassDetails['User']['email']);
					$this->Email->subject = 'Recurring Payment Failed';
					$this->Email->from = Configure::read('SITE').'<noreply@'.Configure::read('SITE_URL').'>';
					$this->Email->sendAs = 'html';
					$this->Email->send("Dear Customer,<br>
										This is to inform you that the recurring payment for the pass : ".$customerPassDetails['Pass']['name']." in Property : ".$customerPassDetails['Property']['name']."	has been failed. 							
										Thanks<br>
										Sincerely,<br>".Configure::read('SITE'));
				  }
				CakeLog::write('RecurringPaymentFailed',$output[$i]['RecurringProfile']['recurring_profile_id'].' The recurring payment result was not success, The PNREF was : '.$result['P_PNREF'.$index].', Amount: '.$result['P_AMT'.$index].', Tender: '.$result['P_TENDER'.$index].', TransTime: '.$result['P_TRANSTIME'.$index].' AND TransState: '.$result['P_TRANSTATE'.$index]);
			}
		}
	  }
   }
}
