<?php
App::uses('ComponentCollection', 'Controller');
App::uses('PayFlowComponent', 'Controller/Component');

class DeactivateProfileShell extends AppShell {
	 public function main() {
		 $dt = new DateTime();
		 $this->loadModel('RecurringProfile');
		 $this->loadModel('CustomerPass');
		 $currentDate= $dt->format('Y-m-d');
		 CakeLog::write('deactivateRecurringCron',' Triggered At '.$dt->format('Y-m-d H:i:s'));
		 $this->CustomerPass->recursive=-1;
		 $con='DATE(pass_valid_upto) <= "'.$currentDate.'"';
		// debug($con);die;
		 $output = $this->CustomerPass->find('all',array('conditions'=>array($con,'recurring_profile_id !='=>NULL,'is_guest_pass'=>0)));
		 if($output){
			 $collection = new ComponentCollection();
			 $this->PayFlow = new PayFlowComponent($collection);
			for($i=0;$i<count($output);$i++){
				$result=$this->PayFlow->deactivate_profile($output[$i]['CustomerPass']['recurring_profile_id']);
				if($result){
					if($result['RESULT']==0 && $result['RESPMSG']=='Approved'){
						//$this->RecurringProfile->unbindModel(array('belongsTo'=>array('User','Vehicle','Property','Transaction','Package','Pass')));
						$recurringProfileId=$this->RecurringProfile->field('id',array('recurring_profile_id'=>$output[$i]['CustomerPass']['recurring_profile_id']));
						if($recurringProfileId){
							$arr['RecurringProfile']=array('id'=>$recurringProfileId,
															'status'=>'Cancelled'
														);
							$this->RecurringProfile->save($arr,false);
						}
						
						/*$this->RecurringProfile->updateAll(array('RecurringProfile.status= "Cancelled"'),
															array('RecurringProfile.recurring_profile_id'=>$output[$i]['CustomerPass']['recurring_profile_id'])
													);*/
						$array['CustomerPass']=array('id'=>$output[$i]['CustomerPass']['id'],
													 'recurring_profile_status'=>'Cancelled'
													);
						if($this->CustomerPass->save($array,false)){
							CakeLog::write('ProfileDeactivation',$output[$i]['CustomerPass']['recurring_profile_id'].' Recurring Profile deactivated successfully');

						}else{
							CakeLog::write('ProfileDeactivation',$output[$i]['CustomerPass']['recurring_profile_id'].' Recurring Profile deactivated but status not updated in Customer Pass Table');
						}
					}else{
						CakeLog::write('ProfileDeactivation',$output[$i]['CustomerPass']['recurring_profile_id'].' Recurring Profile deactivation failed');

					}
				}
			}
		 }
     }
}
