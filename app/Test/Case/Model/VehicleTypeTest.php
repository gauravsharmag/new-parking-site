<?php
App::uses('VehicleType', 'Model');

/**
 * VehicleType Test Case
 *
 */
class VehicleTypeTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.vehicle_type',
		'app.property',
		'app.pass',
		'app.user',
		'app.role',
		'app.transaction',
		'app.vehicle',
		'app.property_user'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->VehicleType = ClassRegistry::init('VehicleType');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->VehicleType);

		parent::tearDown();
	}

}
