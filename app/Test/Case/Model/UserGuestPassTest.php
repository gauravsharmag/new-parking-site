<?php
App::uses('UserGuestPass', 'Model');

/**
 * UserGuestPass Test Case
 *
 */
class UserGuestPassTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.user_guest_pass',
		'app.user',
		'app.role',
		'app.property_user',
		'app.property',
		'app.customer_pass',
		'app.vehicle',
		'app.transaction',
		'app.pass',
		'app.package',
		'app.billing_address'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->UserGuestPass = ClassRegistry::init('UserGuestPass');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->UserGuestPass);

		parent::tearDown();
	}

}
