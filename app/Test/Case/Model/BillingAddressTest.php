<?php
App::uses('BillingAddress', 'Model');

/**
 * BillingAddress Test Case
 *
 */
class BillingAddressTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.billing_address',
		'app.user',
		'app.role',
		'app.property_user',
		'app.property',
		'app.customer_pass',
		'app.vehicle',
		'app.package',
		'app.transaction',
		'app.pass'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->BillingAddress = ClassRegistry::init('BillingAddress');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->BillingAddress);

		parent::tearDown();
	}

}
