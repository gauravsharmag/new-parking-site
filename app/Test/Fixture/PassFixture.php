<?php
/**
 * PassFixture
 *
 */
class PassFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'property_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'cost_1st_year' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'cost_after_1st_year' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'duration' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false),
		'duration_type' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 6, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'deposit' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'expiration_date' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'sort_order' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'is_fixed_duration' => array('type' => 'integer', 'null' => true, 'default' => null, 'length' => 4, 'unsigned' => false),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'property_id' => 1,
			'cost_1st_year' => 1,
			'cost_after_1st_year' => 1,
			'duration' => 1,
			'duration_type' => 'Lore',
			'deposit' => 1,
			'expiration_date' => '2014-07-23 11:08:06',
			'sort_order' => 1,
			'is_fixed_duration' => 1
		),
	);

}
