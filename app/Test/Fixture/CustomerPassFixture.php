<?php
/**
 * CustomerPassFixture
 *
 */
class CustomerPassFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'user_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'vehicle_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false),
		'property_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'transaction_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'status' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 8, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'membership_vaild_upto' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'pass_valid_upto' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'RFID_tag_number' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false),
		'assigned_location' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 15, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'package_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'user_id' => 1,
			'vehicle_id' => 1,
			'property_id' => 1,
			'transaction_id' => 1,
			'status' => 'Lorem ',
			'membership_vaild_upto' => '2014-07-08 06:50:12',
			'pass_valid_upto' => '2014-07-08 06:50:12',
			'RFID_tag_number' => 1,
			'assigned_location' => 'Lorem ipsum d',
			'package_id' => 1
		),
	);

}
