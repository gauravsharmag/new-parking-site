<?php
/**
 * RefundFixture
 *
 */
class RefundFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'user_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'property_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'pass' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 100, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'permit' => array('type' => 'integer', 'null' => true, 'default' => null, 'length' => 100, 'unsigned' => false),
		'pass_cost' => array('type' => 'integer', 'null' => false, 'default' => '0', 'unsigned' => false),
		'pass_deposit' => array('type' => 'integer', 'null' => false, 'default' => '0', 'unsigned' => false),
		'package_cost' => array('type' => 'integer', 'null' => false, 'default' => '0', 'unsigned' => false),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'refunded' => array('type' => 'integer', 'null' => false, 'default' => '0', 'unsigned' => false),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'user_id' => 1,
			'property_id' => 1,
			'pass' => 'Lorem ipsum dolor sit amet',
			'permit' => 1,
			'pass_cost' => 1,
			'pass_deposit' => 1,
			'package_cost' => 1,
			'created' => '2017-11-17 04:32:31',
			'modified' => '2017-11-17 04:32:31',
			'refunded' => 1
		),
	);

}
