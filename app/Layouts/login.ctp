<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" >
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8"/>
<title>My Parking Pass | Powered by Digital6</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta content="" name="description"/>
<meta content="" name="author"/>
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
<?php echo $this->Html->charset();
	  echo $this->Html->css('/assets/global/plugins/font-awesome/css/font-awesome.min.css');
	  echo $this->Html->css('/assets/global/plugins/simple-line-icons/simple-line-icons.min.css');
	  echo $this->Html->css('/assets/global/plugins/bootstrap/css/bootstrap.min.css');
	  echo $this->Html->css('/assets/global/plugins/uniform/css/uniform.default.css');
	  echo $this->Html->css('/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css');
	  echo $this->Html->css('/assets/global/plugins/select2/select2.css');
	  echo $this->Html->css('/assets/admin/pages/css/login.css');
	  echo $this->Html->css('/assets/global/css/components.css');
	  echo $this->Html->css('/assets/global/css/plugins.css');
	  echo $this->Html->css('/assets/admin/layout/css/layout.css');
	  echo $this->Html->css('/assets/admin/layout/css/themes/default.css',array('id'=>'style_color'));
	  echo $this->Html->css('/css/custom.css');
	  echo $this->Html->css('custom.css');

?>
<link rel="shortcut icon" href="favicon.ico"/>

</head>
<body class="login">
<!-- BEGIN LOGO -->
<div class="logo">
	
   <?php echo $this->Html->link($this->Html->image('onlineparking-logo.png',array('height'=>'121','width'=>'300')),'',array('escape'=>false)); ?>
</div>
<!-- END LOGO -->
<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
<div class="menu-toggler sidebar-toggler">
</div>
<!-- END SIDEBAR TOGGLER BUTTON -->
<!-- BEGIN LOGIN -->
<div class="content">
	<!-- BEGIN LOGIN FORM -->
	<?php echo $this->fetch('content'); ?>
	
</div>
<!-- END LOGIN -->
<!-- BEGIN COPYRIGHT -->
<div class="copyright">
	 2014 &copy; <a href="http://digital6technologies.com" target="_blank">Powered by Digital6 Technologies</a>
</div>
<!-- END COPYRIGHT -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="../../assets/global/plugins/respond.min.js"></script>
<script src="../../assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
<?php 
//
	  echo $this->Html->script('/assets/global/plugins/jquery-1.11.0.min.js');
	  echo $this->Html->script('/assets/global/plugins/jquery-migrate-1.2.1.min.js');
//IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip
	  echo $this->Html->script('/assets/global/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js');
	  echo $this->Html->script('/assets/global/plugins/bootstrap/js/bootstrap.min.js');
	  echo $this->Html->script('/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js');
	  echo $this->Html->script('/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js');
	  echo $this->Html->script('/assets/global/plugins/jquery.blockui.min.js');
	  echo $this->Html->script('/assets/global/plugins/jquery.cokie.min.js');
	  echo $this->Html->script('/assets/global/plugins/uniform/jquery.uniform.min.js');
	  echo $this->Html->script('/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js');
// END CORE PLUGINS
// BEGIN PAGE LEVEL PLUGINS

	  echo $this->Html->script('/assets/global/plugins/jquery-validation/js/jquery.validate.min.js');
	 // echo $this->Html->script('/assets/global/plugins/select2/select2.min.js');
// END PAGE LEVEL PLUGINS
// BEGIN PAGE LEVEL SCRIPTS
	  echo $this->Html->script('/assets/global/scripts/metronic.js');
	  echo $this->Html->script('/assets/admin/layout/scripts/layout.js');
	  echo $this->Html->script('/assets/admin/layout/scripts/quick-sidebar.js');
	  echo $this->Html->script('/assets/admin/pages/scripts/login.js');
?>


<!-- END PAGE LEVEL SCRIPTS -->
<script>
		jQuery(document).ready(function() {     
		  Metronic.init(); // init metronic core components
Layout.init(); // init current layout
QuickSidebar.init() // init quick sidebar
		  Login.init();
		});
	</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
