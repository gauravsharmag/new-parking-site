<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" >
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8"/>
<title>My Parking Pass | Powered by Digital6</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta content="" name="description"/>
<meta content="" name="author"/>
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
<?php
	  echo $this->Html->charset();
	  echo $this->Html->css('/assets/global/plugins/font-awesome/css/font-awesome.min.css');
	  echo $this->Html->css('/assets/global/plugins/simple-line-icons/simple-line-icons.min.css');
	  echo $this->Html->css('/assets/global/plugins/bootstrap/css/bootstrap.min.css');
	  //echo $this->Html->css('/assets/global/plugins/uniform/css/uniform.default.css');
	  //echo $this->Html->css('/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css');
	  //echo $this->Html->css('/assets/global/plugins/gritter/css/jquery.gritter.css');
	  echo $this->Html->css('/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css');
	  //echo $this->Html->css('/assets/global/plugins/select2/select2.css');
	  //echo $this->Html->css('/assets/global/plugins/jqvmap/jqvmap/jqvmap.css');
	  echo $this->Html->css('/assets/global/css/components.css');
	  echo $this->Html->css('/assets/global/css/plugins.css');
	  echo $this->Html->css('/assets/admin/layout/css/layout.css');
	  echo $this->Html->css('/assets/admin/layout/css/themes/default.css',array('id'=>'style_color'));
	  //page level style sheet
	  echo $this->Html->css('/assets/global/plugins/clockface/css/clockface.css');
	  echo $this->Html->css('/assets/global/plugins/bootstrap-datepicker/css/datepicker3.css');
      echo $this->Html->css('/assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css');
      //echo $this->Html->css('/assets/global/plugins/bootstrap-colorpicker/css/colorpicker.css');
      echo $this->Html->css('/assets/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css');
      echo $this->Html->css('/assets/global/plugins/bootstrap-datetimepicker/css/datetimepicker.css');
	   //end of page level
	  echo $this->Html->css('/assets/admin/layout/css/custom.css');
	  echo $this->Html->css('custom.css');
	  
	      echo $this->Html->script('/assets/global/plugins/jquery-1.11.0.min.js');
	  echo $this->Html->script('/assets/global/plugins/jquery-migrate-1.2.1.min.js');

   	  //end of page level
   
	 
	 
?>
<?php 
		echo $scripts_for_layout;


?>
<link rel="shortcut icon" href="favicon.ico"/>

</head>
<!-- BEGIN BODY -->
<!-- DOC: Apply "page-header-fixed-mobile" and "page-footer-fixed-mobile" class to body element to force fixed header or footer in mobile devices -->
<!-- DOC: Apply "page-sidebar-closed" class to the body and "page-sidebar-menu-closed" class to the sidebar menu element to hide the sidebar by default -->
<!-- DOC: Apply "page-sidebar-hide" class to the body to make the sidebar completely hidden on toggle -->
<!-- DOC: Apply "page-sidebar-closed-hide-logo" class to the body element to make the logo hidden on sidebar toggle -->
<!-- DOC: Apply "page-sidebar-hide" class to body element to completely hide the sidebar on sidebar toggle -->
<!-- DOC: Apply "page-sidebar-fixed" class to have fixed sidebar -->
<!-- DOC: Apply "page-footer-fixed" class to the body element to have fixed footer -->
<!-- DOC: Apply "page-sidebar-reversed" class to put the sidebar on the right side -->
<!-- DOC: Apply "page-full-width" class to the body element to have full width page without the sidebar menu -->
<body class="page-header-fixed page-quick-sidebar-over-content">
<!-- BEGIN HEADER -->

<?php echo $this->element('header'); ?>
<div class="clearfix">
</div>
<!-- END HEADER-->
<!-- BEGIN CONTAINER -->
<div class="page-container">
<!-- BEGIN SIDEBAR -->

<?php echo $this->element('manager'); ?>
<?php echo $this->fetch('content'); ?>
<!-- END SIDEBAR -->
</div>
<!-- END CONTAINER -->

<!-- BEGIN COPYRIGHT -->
<div class="copyright">
	 2014 &copy; <a href="http://digital6technologies.com" target="_blank">Powered by Digital6 Technologies</a>
</div>
<!-- END COPYRIGHT -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="../../assets/global/plugins/respond.min.js"></script>
<script src="../../assets/global/plugins/excanvas.min.js"></script>
<![endif]-->
<?php
//
	 //IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip
			echo $this->Html->script('/assets/global/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js');
			echo $this->Html->script('/assets/global/plugins/bootstrap/js/bootstrap.min.js');
			echo $this->Html->script('/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js');
			//echo $this->Html->script('/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js');
			//echo $this->Html->script('/assets/global/plugins/jquery.blockui.min.js');
			//echo $this->Html->script('/assets/global/plugins/jquery.cokie.min.js');
			//echo $this->Html->script('/assets/global/plugins/uniform/jquery.uniform.min.js');
			//echo $this->Html->script('/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js');
	// END CORE PLUGINS
	
	// BEGIN PAGE LEVEL PLUGINS
			echo $this->Html->script('/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js');
			echo $this->Html->script('/assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js');
			echo $this->Html->script('/assets/global/plugins/clockface/js/clockface.js');
			echo $this->Html->script('/assets/global/plugins/bootstrap-daterangepicker/moment.min.js');
			echo $this->Html->script('/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.js');
			//echo $this->Html->script('/assets/global/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js');
			echo $this->Html->script('/assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js');
			//echo $this->Html->script('/assets/global/plugins/jquery.sparkline.min.js');
			//echo $this->Html->script('/assets/global/plugins/gritter/js/jquery.gritter.js');
			//echo $this->Html->script('/assets/global/plugins/select2/select2.min.js');
			echo $this->Html->script('/assets/global/plugins/datatables/media/js/jquery.dataTables.min.js');
			echo $this->Html->script('/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js');
	// END PAGE LEVEL PLUGINS
	
	// BEGIN PAGE LEVEL SCRIPTS
			echo $this->Html->script('/assets/global/scripts/metronic.js');
			echo $this->Html->script('/assets/admin/layout/scripts/layout.js');
			echo $this->Html->script('/assets/admin/layout/scripts/quick-sidebar.js');
			echo $this->Html->script('/assets/admin/pages/scripts/tasks.js');
			echo $this->Html->script('/assets/admin/pages/scripts/components-pickers.js');
			echo $this->Html->script('/assets/admin/pages/scripts/charts.js');
			echo $this->Html->script('/assets/global/plugins/flot/jquery.flot.min.js');
			echo $this->Html->script('/assets/global/plugins/flot/jquery.flot.resize.min.js');
			echo $this->Html->script('/assets/global/plugins/flot/jquery.flot.pie.min.js');
			echo $this->Html->script('/assets/global/plugins/flot/jquery.flot.stack.min.js');
			echo $this->Html->script('/assets/global/plugins/flot/jquery.flot.crosshair.min.js');
			echo $this->Html->script('/assets/global/plugins/flot/jquery.flot.categories.min.js');
			echo $this->Html->script('ckeditor/ckeditor');
	 
?>


<!-- END PAGE LEVEL SCRIPTS -->
<script>
		jQuery(document).ready(function() {
		  Metronic.init(); // init metronic core components
Layout.init(); // init current layout
QuickSidebar.init() // init quick sidebar
		  ComponentsPickers.init();
		});
	</script>
<!-- END JAVASCRIPTS -->
<?php ?> <?php echo $this->element('sql_dump'); ?> <?php  ?>
</body>
<!-- END BODY -->
</html>
