<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Here, we are connecting '/' (base path) to controller called 'Pages',
 * its action called 'display', and we pass a param to select the view file
 * to use (in this case, /app/View/Pages/home.ctp)...
 */
/*
   * Domain: park.com
   * Subdomains: loc,loc1....loc7 e.g loc.park.com,www.loc.park.com
   */
$full_domain = Set::extract($_SERVER, 'HTTP_HOST');
$uri=Set::extract($_SERVER, 'REQUEST_URI');

$domain_parts = explode('.', $full_domain);
$subdomain = null;
$domain = null;
if (count($domain_parts) == 3 && $domain_parts[0] != 'www') {
    $subdomain = $domain_parts[0];
    $domain = "{$domain_parts[1]}.{$domain_parts[2]}";
}
elseif (count($domain_parts) == 4 && $domain_parts[0] == 'www')
{
    $subdomain = $domain_parts[1];
    $domain = "{$domain_parts[2]}.{$domain_parts[3]}";
} else {
    $domain = $full_domain;
}
// loc.park.com/admin/users/index
if(stristr($uri,'/admin')){
    Router::connect('/admin', array('controller' => 'users','action'=>'login','admin'=>true));
}elseif(stristr($uri,'/cron_update_vehicles')){
    Router::connect('/cron_update_vehicles', array('controller' => 'users','action'=>'cron_update_vehicles'));
}elseif(stristr($uri,'/logout')){
    Router::connect('/users/logout', array('controller' => 'users','action'=>'logout'));
}elseif(stristr($uri,'/apps')){
		Router::mapResources('apps');
		Router::parseExtensions();
		Router::resourceMap(array(
			array('action' => 'vehicle_search', 'method' => 'POST'),
			array('action' => 'get_user', 'method' => 'POST'),
			array('action' => 'get_properties', 'method' => 'POST'),
			array('action' => 'api_login', 'method' => 'POST'),
			array('action' => 'single_vehicle_search', 'method' => 'POST'),
			array('action' => 'property_vehicle_details', 'method' => 'POST'),
			array('action' => 'vehicle_last_seen', 'method' => 'POST'),
			array('action' => 'properties', 'method' => 'POST'),
			array('action' => 'get_all_rfids', 'method' => 'POST'),
			array('action' => 'get_property_vehicles', 'method' => 'POST'),
			array('action' => 'cost_listing', 'method' => 'POST'),    
			array('action' => 'get_vehicle_search', 'method' => 'POST'),
			array('action' => 'properties_api', 'method' => 'POST')
		));
}else{
    if(empty($subdomain)){
        Router::connect('/*', array('controller' => 'users', 'action' => 'login','admin'=>true));
        return;
    }

    $subdomain = strtolower($subdomain);

    App::import('Model', 'Property');
    $property_model = new Property();
	$property_model->recursive=-1;
    $property = $property_model->find('first',array('conditions'=>array('Property.sub_domain'=>$subdomain,'Property.archived'=>0)));
   
    if(empty($property))
    {
        throw new NotFoundException(__('Not Such Property Exists'));
        //Router::connect('/*', array('controller' => 'users', 'action' => 'home'));
        return;
    }
    $basic_params = array(
        'Property' => $property['Property'],
        'sub_domain' => $property['Property']['sub_domain']
    );

    $property_routes = array(
        '/' => array('users', 'login'),
        '/users/'=>array('users','account'),
        '/users/:action'=>array('users'),
        '/users/:action/*'=>array('users'),
        '/properties/:action'=>array('properties'),
        '/properties/:action/*'=>array('properties')
    );
    $route_commands = array();
    $i = 0;
    foreach ($property_routes as $url => $args) {
        $route_commands[$i] = array();
        foreach ($basic_params as $k => $v) {
            $route_commands[$i][$k] = $v;
        }
        $controller = $args[0];
        $route_commands[$i]['controller'] = $controller;
        $action = null;
        if (isset($args[1])) {
            $action = $args[1];
        }
        if (!empty($action)) {
            $route_commands[$i]['action'] = $action;
        }
        if (isset($args[2]) && is_array($args[2])) {
            foreach ($args[2] as $k => $v) {
                $route_commands[$i][$k] = $v;
            }
        }
        Router::connect($url, $route_commands[$i]);
        $i++;
    }
}
Router::connect('/', array('controller' => 'users', 'action' => 'login'));
//Router::connect('/', array('controller' => 'pages', 'action' => 'home'));
/**
 * ...and connect the rest of 'Pages' controller's URLs.
 */
//Router::connect('/pages/*', array('controller' => 'pages', 'action' => 'display'));

/**
 * Load all plugin routes. See the CakePlugin documentation on
 * how to customize the loading of plugin routes.
 */
CakePlugin::routes();

/**
 * Load the CakePHP default routes. Only remove this if you do not want to use
 * the built-in default routes.
 */
require CAKE . 'Config' . DS . 'routes.php';
