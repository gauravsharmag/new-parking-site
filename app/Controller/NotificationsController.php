<?php
App::uses('AppController', 'Controller');

/**
 * Notifications Controller
 *
 */
class NotificationsController extends AppController {
	 public $components = array('SmsNotification');
	 public function beforeFilter(){ 
        parent::beforeFilter();
    }
    
    public function admin_index(){
		if(Configure::read('SMS_NOTIFICATION')){
			$response=$this->SmsNotification->sendOTP('+91-988-270-0751');
			debug($response);die;
		}
	}
	public function sendOpt(){
		$this->layout=$this->autoRender=false;
		$response=array(
						 'success'=>false
						);
		if(Configure::read('SMS_NOTIFICATION')){
			$this->loadModel('User');
			$currentPhone=$this->User->field('phone',array('User.id'=>$this->Auth->user('id')));
			$response=$this->SmsNotification->sendOTP($currentPhone,$this->Auth->user('id'));
		}
		echo json_encode($response);die;
	}
	public function verifyOtp(){
		$this->layout=$this->autoRender=false;
		$currentOtp=CakeSession::read('CURRENT_OTP');
		if($currentOtp==$this->request->data['Notification']['otp']){
			$this->loadModel('User');
			$this->User->id=$this->Auth->user('id');
			$this->User->saveField('phone_verified',1);
			$this->Session->setFlash(__('Phone verified successfully.'),'success');
			CakeSession::delete('CURRENT_OTP');
		}else{
			$this->Session->setFlash(__('Invalid OTP. Please try again.'),'error');
		}
		return $this->redirect(['controller'=>'CustomerPasses','action'=>'view']);
	}
}
