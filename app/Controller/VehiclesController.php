<?php
App::uses('AppController', 'Controller');
/**
 * Vehicles Controller
 *
 * @property Vehicle $Vehicle
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class VehiclesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');
	 public function beforeFilter()
    { 
        parent::beforeFilter();
        $this->Security->validatePost = false;
        //$this->Auth->allow();
    }
/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->layout='customer';
		//debug($this->Auth->user('id'));
		//die();
		$dt = new DateTime();
        $currentDateTime= $dt->format('Y-m-d H:i:s');
		$this->Vehicle->recursive=1;
		$this->Vehicle->unbindModel(array('hasMany'=>array('CustomerPass')));
		$customerOtherVehicles=$activeVehicles=array();
		$activeVehicles=$this->Vehicle->find('all',
							array(
								'fields'=>'Vehicle.*,CustomerPass.*',
								'joins'=>array(
									array(
										'table'=>'customer_passes',
										'alias'=>'CustomerPass',
										'type'=>'INNER',
										'conditions'=>array(
											'CustomerPass.property_id'=>$this->Session->read('PropertyId'),
											'CustomerPass.pass_valid_upto >'=>$currentDateTime,
											'CustomerPass.membership_vaild_upto >'=>$currentDateTime,
											'CustomerPass.vehicle_id=Vehicle.id'
										)
									)
								),
								'conditions'=>array(
									'Vehicle.user_id'=>$this->Auth->user('id')
									)
								));
		$activeIds=0;
		if(!empty($activeVehicles)){						
			$activeIds=Set::extract('/Vehicle/id',$activeVehicles);						
		}
		$this->Vehicle->recursive=-1;
		$customerOtherVehicles=$this->Vehicle->find('all',array('conditions'=>array('NOT'=>array('id'=>$activeIds),'user_id'=>$this->Auth->user('id'),'property_id'=>$this->Session->read('PropertyId'))));
		
		$this->set(compact('activeVehicles','customerOtherVehicles'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view() {
		
	}

/**
 * add method
 *
 * @return void
 */
	public function add($customerPass=null) {
        $this->layout='customer';
        $this->loadModel('CustomerPass');
        $cstPass=new CustomerPass();
        $result=$cstPass->hasAny(array('id'=>$customerPass));
        
       
        if($result==true){
			$vehicleId=$this->CustomerPass->field('vehicle_id',array('id'=>$customerPass));
			 if(!is_null($vehicleId)){
				 //  $this->Session->setFlash('The vehicle has been assigned','warning');
					$this->redirect(array('action'=>'add_vehicles'));
			 }
             if ($this->request->is('post')) {
				if(!is_null($vehicleId)){
				 //  $this->Session->setFlash('The vehicle has been assigned','warning');
					$this->redirect(array('action'=>'add_vehicles'));
				}
				 
				 $this->Vehicle->updateAll(array('Vehicle.customer_pass_id'=>NULL),array('Vehicle.customer_pass_id'=>$customerPass));
                 
                 $this->request->data['Vehicle']['property_id']=$this->Session->read('PropertyId');
                 $this->request->data['Vehicle']['customer_pass_id']=$customerPass;
                 $this->request->data['Vehicle']['user_id']=$this->Auth->user('id');
                 $this->Vehicle->set($this->request->data);
                 if($this->Vehicle->validates()){
					 $this->Vehicle->create();
						if ($this->Vehicle->save($this->request->data,false)) {
							CakeLog::write('newVehicleAdded', ''.AuthComponent::user('username').' : New vehicle added with Vehicle ID:  <a href="/admin/Vehicles/edit/'.$this->Vehicle->getLastInsertID().'">'.$this->Vehicle->getLastInsertID().' </a> by User: '.AuthComponent::user('first_name').' '.AuthComponent::user('last_name').' In Property: '.CakeSession::read('PropertyName').'');
							$arr['CustomerPass']['id']=$customerPass;
							$arr['CustomerPass']['vehicle_id']=$this->Vehicle->getLastInsertID();
							if($this->request->data['Vehicle']['assigned_location']){
								$arr['CustomerPass']['assigned_location']=trim($this->request->data['Vehicle']['assigned_location']);
							}
							if( $this->Vehicle->CustomerPass->save($arr,false)){
								CakeLog::write('newVehicleAddedPassAssigned', ''.AuthComponent::user('username').' : New vehicle added with Vehicle ID:  <a href="/admin/Vehicles/edit/'.$this->Vehicle->getLastInsertID().'">'.$this->Vehicle->getLastInsertID().' </a> by User: '.AuthComponent::user('first_name').' '.AuthComponent::user('last_name').' in Property: '.CakeSession::read('PropertyName').' and sssigned to CustomerPass: <a href="/admin/CustomerPasses/edit/'.$customerPass.'">'.$customerPass.'</a>');
								$this->Session->setFlash('The vehicle has been saved','success');
							// return $this->redirect(array('controller'=>'CustomerPasses','action' => 'view'));
								return $this->redirect(array('action' => 'add_vehicles'));
							}
						}else {
							$this->Session->setFlash('The vehicle could not be saved','error');
						}
				}
            }
        }
        else{
            $this->Session->setFlash(__('Url rewriting not allowed'));
        }
        $dt = new DateTime();
        $currentDateTime= $dt->format('Y-m-d H:i:s');
		$this->Vehicle->recursive=1;
		$this->Vehicle->unbindModel(array('hasMany'=>array('CustomerPass')));
		$customerOtherVehicles=$activeVehicles=array();
		$activeVehicles=$this->Vehicle->find('all',
							array(
								'fields'=>'Vehicle.*,CustomerPass.*',
								'joins'=>array(
									array(
										'table'=>'customer_passes',
										'alias'=>'CustomerPass',
										'type'=>'INNER',
										'conditions'=>array(
											'CustomerPass.property_id'=>$this->Session->read('PropertyId'),
											'CustomerPass.pass_valid_upto >'=>$currentDateTime,
											'CustomerPass.membership_vaild_upto >'=>$currentDateTime,
											'CustomerPass.vehicle_id=Vehicle.id'
										)
									)
								),
								'conditions'=>array(
									'Vehicle.user_id'=>$this->Auth->user('id')
									)
								));
		$activeIds=0;
		if(!empty($activeVehicles)){						
			$activeIds=Set::extract('/Vehicle/id',$activeVehicles);						
		}
		$this->Vehicle->recursive=-1;
		$customerOtherVehicles=$this->Vehicle->find('all',array('conditions'=>array('NOT'=>array('id'=>$activeIds),'user_id'=>$this->Auth->user('id'),'property_id'=>$this->Session->read('PropertyId'))));
        $this->loadModel('State');
        $state=new State();
        $states=$state->find('list');
        //debug($customerOtherVehicles);
        //die();
        $this->set(compact('states','customerOtherVehicles','customerPass'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->layout='customer';
		if (!$this->Vehicle->exists($id)) {
			throw new NotFoundException(__('Invalid vehicle'));
		}
		if ($this->request->is(array('post', 'put'))) {
			$this->request->data['Vehicle']['id']=$id;
			if ($this->Vehicle->save($this->request->data)) {
				CakeLog::write('vehicleDetailEdited', ''.AuthComponent::user('username').' : Vehicle detail edited Vehicle ID: '.$id.' by User: '.AuthComponent::user('first_name').' '.AuthComponent::user('last_name').' in Property: '.CakeSession::read('PropertyName').'');
				$this->Session->setFlash('The vehicle has been saved.','success');
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash('The vehicle could not be saved','error');
			}
		} 	
		$options = array('conditions' => array('Vehicle.' . $this->Vehicle->primaryKey => $id));
		$this->loadModel('State');
        $state=new State();
        $states=$state->find('list'); 
		$this->request->data = $this->Vehicle->find('first', $options);	
		$this->set(compact('states'));	
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Vehicle->id = $id;
		if (!$this->Vehicle->exists()) {
			throw new NotFoundException(__('Invalid vehicle'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Vehicle->delete()) {
			$this->Session->setFlash(__('The vehicle has been deleted.'));
		} else {
			$this->Session->setFlash(__('The vehicle could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
	
	}
	public function admin_get_vehicles()
	{
		$this->autoRender=$this->layout=false;
		$output=array();
		$output = $this->Vehicle->getVehicleData();
		//var_dump($output);
        echo json_encode($output);
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Vehicle->exists($id)) {
			throw new NotFoundException(__('Invalid vehicle'));
		}
		$options = array('conditions' => array('Vehicle.' . $this->Vehicle->primaryKey => $id));
		$this->set('vehicle', $this->Vehicle->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
public function admin_add($customerPass,$userId,$propertyIdAdmin) {
		
		$this->loadModel('Property');
		$this->Property->recursive=-1;
		$propertyName=$this->Property->find('first',array('fields'=>array('name'),'conditions'=>array('id'=>$propertyIdAdmin)));
		$this->loadModel('User');
		$this->User->recursive=-1;
     	$userDetails=$this->User->find('first',array('conditions'=>array('id'=>$userId)));
			
		$this->layout="admin_customer";
        $this->loadModel('CustomerPass');
        $cstPass=new CustomerPass();
        $result=$cstPass->hasAny(array('id'=>$customerPass,'user_id'=>$userId));
        if($result==true){
			$vehicleId=$this->CustomerPass->field('vehicle_id',array('id'=>$customerPass));
			 if(!is_null($vehicleId)){
				 //  $this->Session->setFlash('The vehicle has been assigned','warning');
				return $this->redirect(array('controller'=>'CustomerPasses','action' => 'customer_view',$userId));
			 }
             if ($this->request->is('post')) {
				  if(!is_null($vehicleId)){
					$this->Session->setFlash('The vehicle has been assigned','warning');
					 return $this->redirect(array('controller'=>'CustomerPasses','action' => 'customer_view',$userId));
				  }else{
					$this->Vehicle->updateAll(array('Vehicle.customer_pass_id'=>NULL),array('Vehicle.customer_pass_id'=>$customerPass));
					$this->Vehicle->clear();
					$this->request->data['Vehicle']['property_id']=$propertyIdAdmin;
					$this->request->data['Vehicle']['customer_pass_id']=$customerPass;
					$this->request->data['Vehicle']['user_id']=$userId;
					$this->Vehicle->set($this->request->data);
					$this->Vehicle->validate=$this->Vehicle->validateAdmin;
					if($this->Vehicle->validates()){
					    $this->Vehicle->create();
						if ($this->Vehicle->save($this->request->data,false)) {
						CakeLog::write('newVehicleAddedAdmin', ''.AuthComponent::user('username').' : New vehicle added with Vehicle ID:  <a href="/admin/Vehicles/edit/'.$this->Vehicle->getLastInsertID().'">'.$this->Vehicle->getLastInsertID().' </a> by User: '.AuthComponent::user('first_name').' '.AuthComponent::user('last_name').' In Property: '.$propertyName['Property']['name'].'');
						$arr['CustomerPass']['id']=$customerPass;
						$arr['CustomerPass']['vehicle_id']=$this->Vehicle->getLastInsertID();
						if( $this->Vehicle->CustomerPass->save($arr,false)){
							CakeLog::write('newVehicleAddedPassAssignedAdmin', ''.AuthComponent::user('username').' : New vehicle added with Vehicle ID:  <a href="/admin/Vehicles/edit/'.$this->Vehicle->getLastInsertID().'">'.$this->Vehicle->getLastInsertID().' </a> by User: '.AuthComponent::user('first_name').' '.AuthComponent::user('last_name').' in Property: '.$propertyName['Property']['name'].' and sssigned to CustomerPass: <a href="/admin/CustomerPasses/edit/'.$customerPass.'">'.$customerPass.'</a>');
							$this->Session->setFlash('The vehicle has been saved','success');
							return $this->redirect(array('controller'=>'CustomerPasses','action' => 'customer_view',$userId));
							}
						}else {
							$this->Session->setFlash('The vehicle could not be saved','error');
						}
				 }
              }
            }
        }
        else{
            $this->Session->setFlash('This Pass does not belong to currenty user');
        }
        $dt = new DateTime();
        $currentDateTime= $dt->format('Y-m-d H:i:s');
		$this->Vehicle->recursive=1;
		$this->Vehicle->unbindModel(array('hasMany'=>array('CustomerPass')));
		$customerOtherVehicles=$activeVehicles=array();
		$activeVehicles=$this->Vehicle->find('all',
							array(
								'fields'=>'Vehicle.*,CustomerPass.*',
								'joins'=>array(
									array(
										'table'=>'customer_passes',
										'alias'=>'CustomerPass',
										'type'=>'INNER',
										'conditions'=>array(
											'CustomerPass.property_id'=>$propertyIdAdmin,
											'CustomerPass.pass_valid_upto >'=>$currentDateTime,
											'CustomerPass.membership_vaild_upto >'=>$currentDateTime,
											'CustomerPass.vehicle_id=Vehicle.id'
										)
									)
								),
								'conditions'=>array(
									'Vehicle.user_id'=>$userId
									)
								));
		$activeIds=0;
		if(!empty($activeVehicles)){						
			$activeIds=Set::extract('/Vehicle/id',$activeVehicles);						
		}
		$this->Vehicle->recursive=-1;
		$customerOtherVehicles=$this->Vehicle->find('all',array('conditions'=>array('NOT'=>array('id'=>$activeIds),'user_id'=>$userId,'property_id'=>$propertyIdAdmin)));
        $this->loadModel('State');
        $state=new State();
        $states=$state->find('list');
        $this->set(compact('states','customerOtherVehicles','customerPass','userDetails','propertyName','propertyIdAdmin'));
} 

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Vehicle->exists($id)) {
			throw new NotFoundException(__('Invalid vehicle'));
		}
		if ($this->request->is(array('post', 'put'))) { 
			$propertyID=$this->Vehicle->field('property_id',array('id'=>$this->request->data['Vehicle']['id']));
			$this->request->data['Vehicle']['property_id']=$propertyID;
			$this->Vehicle->set($this->request->data);
			$this->Vehicle->validate=$this->Vehicle->validateAdmin;
            if($this->Vehicle->validates()){
				if ($this->Vehicle->save($this->request->data,false)) {
					CakeLog::write('vehicleDetailEditedAdmin', ''.AuthComponent::user('username').' : Vehicle details updated Vehicle ID:  <a href="/admin/Vehicles/edit/'.$id.'">'.$id.' </a> by Name: '.AuthComponent::user('first_name').' '.AuthComponent::user('last_name').' ');  
					$this->Session->setFlash('The vehicle has been saved.','success');
					return $this->redirect(array('action' => 'index'));
				} else {
					$this->Session->setFlash('The vehicle could not be saved.','error');
				}
			}
		} 
		$customerPass=$this->Vehicle->field('customer_pass_id',array('Vehicle.id'=>$id));
		if(!is_null($customerPass)){
			$this->redirect(array('controller'=>'CustomerPasses','action'=>'edit',$customerPass));
		}else{	
			$fields="Vehicle.*,User.*,Property.name";
			$joins=array(
						array(
							'table'=>'users',
							'alias'=>'User',
							'type'=>'LEFT',
							'conditions'=>array(
								'User.id=Vehicle.user_id'
							)
						),
						array(
							'table'=>'properties',
							'alias'=>'Property',
							'type'=>'LEFT',
							'conditions'=>array(
								'Property.id=Vehicle.property_id'
							)
						)
				);
			$this->Vehicle->unbindModel(array('hasMany'=>array('CustomerPass'),'belongsTo'=>array('Property','User'),'hasOne'=>array('ChangeVehicleDetail')));				 
			$result=$this->Vehicle->find('first',array(
											'fields'=>$fields,
											'joins'=>$joins,
									 'conditions'=>array('Vehicle.id'=>$id)
						));	
			$this->request->data =$result;
		}
		$this->loadModel('State');
        $states=$this->State->find('list');
        $this->set(compact('states'));
	}


/********************************************************************
 * admin_delete method
 */
	public function admin_delete($id = null) {
		$this->Vehicle->id = $id;
		if (!$this->Vehicle->exists()) {
			throw new NotFoundException(__('Invalid vehicle'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Vehicle->delete()) {
			$this->Session->setFlash(__('The vehicle has been deleted.'));
		} else {
			$this->Session->setFlash(__('The vehicle could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
    public function getVehiclePlateNumber($id){
        $this->Vehicle->recursive=-1;
        return $this->Vehicle->find('first',array('fields'=>array('license_plate_number'),'conditions'=>array('id'=>$id)));
     }
      public function admin_getVehiclePlateNumber($id){
        $this->Vehicle->recursive=-1;
        return $this->Vehicle->find('first',array('fields'=>array('license_plate_number'),'conditions'=>array('id'=>$id)));
     }
    public function register(){
		$this->layout='customer';
		if ($this->request->is('post')) {
                 $this->Vehicle->create();
                 $this->request->data['Vehicle']['property_id']=$this->Session->read('PropertyId');
                 $this->request->data['Vehicle']['user_id']=$this->Auth->user('id');
                 $this->Vehicle->create();
                if ($this->Vehicle->save($this->request->data)) {
                  return $this->redirect(array('action' => 'index'));      
                }
                else {
                    $this->Session->setFlash('The vehicle could not be registered','error');
                }
            }
		$this->loadModel('State');
        $state=new State();
        $states=$state->find('list');
        $this->set(compact('states'));
	}
	public function addSelectedVehicle($customerPass=null,$vehicleId=null)
	{
		 $this->loadModel('CustomerPass');
		 //SET VEHICLE ID TO NULL IF IT IS ATTACHED TO ANY OTHER PASS
		 $this->CustomerPass->updateAll(array('CustomerPass.vehicle_id'=>NULL),array('CustomerPass.vehicle_id'=>$vehicleId));
		 //SET CUSTOMER PASS ID TO NULL IF IT IS ATTACHED TO ANY OTHER VEHICLE
		 $this->Vehicle->updateAll(array('Vehicle.customer_pass_id'=>NULL),array('Vehicle.customer_pass_id'=>$customerPass));
		 //ASSIGN PASS TO VEHICLE
		 $this->CustomerPass->updateAll(array('CustomerPass.vehicle_id'=>$vehicleId),array('CustomerPass.id'=>$customerPass));
		 //ASSIGN VEHICLE TO PASS
		 $this->Vehicle->updateAll(array('Vehicle.customer_pass_id'=>$customerPass),array('Vehicle.id'=>$vehicleId));
		 
		 CakeLog::write('listedVehicleAdded', ''.AuthComponent::user('username').' : Listed vehicle added to Customer Pass ID: <a href="/admin/CustomerPasses/edit/'.$customerPass.'">'.$customerPass.'</a>, selected Vehicle ID:  <a href="/admin/Vehicles/edit/'.$vehicleId.'">'.$vehicleId.' </a>  by User: '.AuthComponent::user('first_name').' '.AuthComponent::user('last_name').', in Property: '.CakeSession::read('PropertyName').'');

		 $this->Session->setFlash('Vehicle added successfully','success');
		 return $this->redirect(array('action'=>'add_vehicles'));
	}
	public function admin_addSelectedVehicle($customerPass=null,$vehicleId=null,$userId=null,$propertyIdAdmin=null)
	{
		  $this->loadModel('CustomerPass');
		  
		  $this->loadModel('Property');
		$this->Property->recursive=-1;
		$propertyName=$this->Property->find('first',array('fields'=>array('name'),'conditions'=>array('id'=>$propertyIdAdmin)));
		$this->loadModel('User');
		$this->User->recursive=-1;
     	$userDetails=$this->User->find('first',array('conditions'=>array('id'=>$userId)));
     	
		 //$vehicleId=$this->CustomerPass->field('vehicle_id',array('id'=>$customerPass));
		/* if(!is_null($vehicleId)){
				    $this->Session->setFlash('The vehicle has already assigned','warning');
					$this->redirect(array('action'=>'add_vehicles'));
		 }*/
		 //SET VEHICLE ID TO NULL IF IT IS ATTACHED TO ANY OTHER PASS
		 $this->CustomerPass->updateAll(array('CustomerPass.vehicle_id'=>NULL),array('CustomerPass.vehicle_id'=>$vehicleId));
		 //SET CUSTOMER PASS ID TO NULL IF IT IS ATTACHED TO ANY OTHER VEHICLE
		 $this->Vehicle->updateAll(array('Vehicle.customer_pass_id'=>NULL),array('Vehicle.customer_pass_id'=>$customerPass));
		 //ASSIGN PASS TO VEHICLE
		 $this->CustomerPass->updateAll(array('CustomerPass.vehicle_id'=>$vehicleId),array('CustomerPass.id'=>$customerPass));
		 //ASSIGN VEHICLE TO PASS
		 $this->Vehicle->updateAll(array('Vehicle.customer_pass_id'=>$customerPass),array('Vehicle.id'=>$vehicleId));
		
		
		 //$this->CustomerPass->query('UPDATE customer_passes SET vehicle_id = '.$vehicleId.' where id='.$customerPass.'');
         //$this->Vehicle->query('UPDATE vehicles SET customer_pass_id = null where user_id = '.$userId.' and customer_pass_id ='.$customerPass.' and property_id='.$propertyIdAdmin.'');
         //$this->Vehicle->query('UPDATE vehicles SET customer_pass_id = '.$customerPass.' where id='.$vehicleId.'');
		 
		 CakeLog::write('listedVehicleAddedAdmin', ''.AuthComponent::user('username').' : Listed vehicle added to Customer Pass ID: <a href="/admin/CustomerPasses/edit/'.$customerPass.'">'.$customerPass.'</a>, selected Vehicle ID:  <a href="/admin/Vehicles/edit/'.$vehicleId.'">'.$vehicleId.' </a>  by User: '.AuthComponent::user('first_name').' '.AuthComponent::user('last_name').', in Property: '.$propertyName['Property']['name'].'');

		 $this->Session->setFlash('Vehicle added successfully','success');
		// $this->redirect(array('action'=>'add_vehicles'));
		 return $this->redirect(array('controller'=>'CustomerPasses','action'=>'customer_view',$userId));
		 
	}
	public function setGuestPackageOptions_bkp($customerPassId=null,$passId=null,$permitId=null)
	{
		
		
		$this->layout='customer';
		 $this->loadModel('CustomerPass');
		if (!$this->CustomerPass->exists($customerPassId)) {
            throw new NotFoundException(__('Invalid Customer Pass'));
        }else{
			$userID=$this->CustomerPass->field('user_id',array('id'=>$customerPassId));
			if($userID != AuthComponent::user('id')){
				 throw new NotFoundException(__('Invalid DATA'));
			}
			$rfid=$this->CustomerPass->field('RFID_tag_number',array('id'=>$customerPassId));
			if(is_null($rfid)){
				$this->Session->setFlash('Sorry, As RFID tag has not been assigned yet, Pass cannot be activated','warning');
				$this->redirect(array('controller'=>'CustomerPasses','action'=>'view'));
			}
		}
		 $this->loadModel('Package');
		if (!$this->Package->exists($permitId)) {
            throw new NotFoundException(__('Invalid package'));
        }
        $this->loadModel('Pass');
        if (!$this->Pass->exists($passId)) {
            throw new NotFoundException(__('Invalid pass'));
        }
        $packageDate=$this->CustomerPass->field('membership_vaild_upto',array('id'=>$customerPassId));
        if(!is_null($packageDate)){
			$dt = new DateTime();
			$currentDateTime= $dt->format('Y-m-d H:i:s');
			if($packageDate>$currentDateTime){
				$this->Session->setFlash('This pass is already active','warning');
				$this->redirect(array('controller'=>'CustomerPasses','action'=>'view'));
			}
		}
        $this->loadModel('Property');
        $freeGuestCredits=$this->Property->field('free_guest_credits',array('id'=>$this->Session->read('PropertyId')));
        $permitCost=$this->Package->field('cost',array('id'=>$permitId));
        $remainingCredits=$this->Auth->user('guest_credits');
        
        if ($this->request->is('post')) { 
			$this->CustomerPass->updateAll(array('CustomerPass.vehicle_id'=>NULL),array('CustomerPass.id'=>$customerPassId));
			$this->Vehicle->updateAll(array('Vehicle.customer_pass_id'=>NULL),array('Vehicle.customer_pass_id'=>$customerPassId));
			$toDate=$this->request->data['to'];
			$toDate = date("Y-m-d", strtotime($toDate));
			$passValidation=$this->CustomerPass->field('pass_valid_upto',array('id'=>$customerPassId));	
				$hr24=false;
				if($toDate>=$passValidation){
					$this->Session->setFlash('You cannot active your permit beyond the pass expiry date','error');
					$toDate=date("Y-m-d", strtotime($passValidation));
					$hr24=true;
					//$toDate=$toDate." 23:59:59";
				}
				$dt = new DateTime();
				$currentDateTime= $dt->format('Y-m-d');
				$datetime1 = date_create($toDate);
				$datetime2 = date_create($currentDateTime);
				$interval = date_diff($datetime1, $datetime2);
				$currentCreditUsed=$interval->days;
				$newCredits=0;
				$amount=0;
				if((int)$remainingCredits>=$interval->days){
					$newCredits=$remainingCredits-$interval->days;
					$amount=0;
				}else{
					$newCredits=0;
					$creditPayable=$interval->days-$remainingCredits;
					$amount=$creditPayable*$permitCost;
				}
				
				if(!empty($this->request->data['Vehicle']['vehicle_id'])){
					$this->Vehicle->updateValidations();
				}
				$this->Vehicle->set($this->request->data);
				if($this->Vehicle->validates()){	
				$date = new DateTime();
				$date->add(new DateInterval('P'.$interval->days.'D'));
				$permitExpiryDate=date_format($date, 'Y-m-d H:i:s');
				if($hr24){
					$permitExpiryDate=date_format($date, 'Y-m-d');
					$permitExpiryDate=$permitExpiryDate." 23:59:59"; 
				}
				
				$vehicleID=null;
			    if(empty($this->request->data['Vehicle']['vehicle_id'])){
						    $this->CustomerPass->recursive=-1;
							$vehicle['Vehicle']=array('owner'=>$this->request->data['Vehicle']['owner'],
										  'make'=>$this->request->data['Vehicle']['make'],
										  'model'=>$this->request->data['Vehicle']['model'],
										  'color'=>$this->request->data['Vehicle']['color'],
										  'license_plate_number'=>$this->request->data['Vehicle']['license_plate_number'],
										  'license_plate_state'=>$this->request->data['Vehicle']['license_plate_state'],
										  'last_4_digital_of_vin'=>$this->request->data['Vehicle']['last_4_digital_of_vin'],
										  'user_id'=>$this->Auth->user('id'),
										  'property_id'=>$this->Session->read('PropertyId')
											);
							if($this->Vehicle->save($vehicle,false)){
									CakeLog::write('newGuestVehicleAdded', ''.AuthComponent::user('username').' : New Guest Vehicle Added with vehicle ID:  <a href="/admin/Vehicles/edit/'.$this->Vehicle->getLastInsertID().'">'.$this->Vehicle->getLastInsertID().' </a> by User: '.AuthComponent::user('first_name').' '.AuthComponent::user('last_name').' In Property: '.CakeSession::read('PropertyName').'');
									$vehicleID=$this->Vehicle->getLastInsertId();
									$this->Session->setFlash('Your vehicle details has been saved','success');
							}else{
								$this->Session->setFlash('Vehicle not saved','error');
								$this->redirect(array('action'=>'setGuestPackageOptions',$customerPassId,$passId,$permitId)); 
							}
				}else{
					$vehicleID=$this->request->data['Vehicle']['vehicle_id'];
					$this->CustomerPass->updateAll(array('CustomerPass.vehicle_id'=>NULL),array('CustomerPass.vehicle_id'=>$vehicleID));
					$this->Vehicle->updateAll(array('Vehicle.customer_pass_id'=>NULL),array('Vehicle.customer_pass_id'=>$customerPassId));
				}
				if($amount!=0)
				{					
						$this->Session->write('toDate',$toDate);
						$this->Session->write('vehicleID',$vehicleID);
						$this->redirect(array('controller'=>'Transactions','action'=>'guestCheckOut',$customerPassId,$passId,$permitId));
					
				}
				else{
					 $this->request->data['Transaction']['card_number']='1111111111111111';
					 $this->request->data['Transaction']['cvv']='111';
					 $this->request->data['Transaction']['month']='11';
					 $this->request->data['Transaction']['year']='9999';
					 $this->request->data['Transaction']['first_name_cc']='Netgen';
					 $this->request->data['Transaction']['last_name_cc']='Solutions';				 
				}
								$response=array('result'=>'Free guest credits tarnsaction');
								$dt = new DateTime();
							    $currentDateTime= $dt->format('Y-m-d H:i:s');
								$dateTransaction = $currentDateTime;
								$arraySerialize=serialize($response);
								$transactionArray['Transaction']['paypal_tranc_id']="Free Guest";
								$transactionArray['Transaction']['user_id']=$this->Auth->user('id');
								$transactionArray['Transaction']['date_time']=$dateTransaction;
								$transactionArray['Transaction']['amount']=0;
								$transactionArray['Transaction']['result']='Success';
								$transactionArray['Transaction']['pass_id']=$passId;
								$transactionArray['Transaction']['transaction_array']=$arraySerialize;
								$transactionArray['Transaction']['first_name']='Free';
								$transactionArray['Transaction']['last_name']='Guest';	
								$transactionArray['Transaction']['comments'] = "guestPassActivated";
								$this->loadModel('Transaction');
								$this->Transaction->create();
								if ($this->Transaction->save($transactionArray,false)) {
											$transactionId=$this->Transaction->getLastInsertId();
											$this->loadModel('Pass');
											$passExpiry=$this->Pass->field('expiration_date',array('id'=>$passId));
											$this->loadModel('CustomerPass');
											$customerPassArray['CustomerPass']['transaction_id']=$transactionId;
											$customerPassArray['CustomerPass']['vehicle_id']=$vehicleID;
											$customerPassArray['CustomerPass']['membership_vaild_upto']= $permitExpiryDate;
											$customerPassArray['CustomerPass']['id']=$customerPassId;
											$customerPassArray['CustomerPass']['package_id']= $permitId;
											$customerPassArray['CustomerPass']['is_guest_pass']=1;
											$this->CustomerPass->create();
											if($this->CustomerPass->save($customerPassArray,false)){
																$this->loadModel('UserGuestPass');
																$vehicleInfo=$this->Vehicle->field('vehicle_info',array('id'=>$vehicleID));
																$userPassArray=array('UserGuestPass'=>array('user_id'=>AuthComponent::user('id'),
																											'customer_pass_id'=>$customerPassId,
																											'vehicle_id'=>$vehicleID,
																											'vehicle_details'=>$vehicleInfo,
																											'days'=>$currentCreditUsed,
																											'amount'=>$amount,
																											'to_date'=>$permitExpiryDate,
																											'property_id'=>$this->Session->read('PropertyId')
																));
																$this->UserGuestPass->save($userPassArray,false); 
																CakeLog::write('guestPassUpdated', ''.AuthComponent::user('username').' : Guest Pass Updated, Vehicle Assigned With Vehicle ID:  <a href="/admin/Vehicles/edit/'.$vehicleID.'">'.$vehicleID.' </a> And Customer Pass ID: <a href="/admin/CustomerPasses/edit/'.$customerPassId.'">'.$customerPassId.'</a> by User'.AuthComponent::user('first_name').' '.AuthComponent::user('last_name').' In Property '.CakeSession::read('PropertyName').'For '.$currentCreditUsed.' Days, Amount Paid: $'.$amount.'');
																$this->Vehicle->query('UPDATE vehicles SET customer_pass_id='.$customerPassId.' where id = '.$vehicleID.'');
																$this->loadModel('User');
																$user['User']['id']=$this->Auth->user('id');
																$user['User']['guest_credits']=$newCredits;
																$this->User->create();
																if($this->User->save($user,false)){
																	$_SESSION['Auth']['User']['guest_credits']=$newCredits;
																	$this->Session->setFlash('Your pass has been activated','sucess');
																	$this->Session->delete('Allowed');
																	$this->Session->write('Allowed',true);
																	return $this->redirect(array('controller'=>'CustomerPasses','action' => 'view'));
																}else{
																		$this->Session->setFlash(__('User Table not updated','error'));															
																}
														
													}												
											    }else {
											$this->Session->setFlash('Pass not saved','error');												
										}								
								
				    }else{
						$this->Session->setFlash('Validation Failed','error');
					}
		   } 
		$dt = new DateTime();
        $currentDateTime= $dt->format('Y-m-d H:i:s');
		$this->Vehicle->recursive=1;
		$this->Vehicle->unbindModel(array('hasMany'=>array('CustomerPass')));
		$customerOtherVehicles=$activeVehicles=array();
		$activeVehicles=$this->Vehicle->find('all',
							array(
								'fields'=>'Vehicle.*,CustomerPass.*',
								'joins'=>array(
									array(
										'table'=>'customer_passes',
										'alias'=>'CustomerPass',
										'type'=>'INNER',
										'conditions'=>array(
											'CustomerPass.property_id'=>$this->Session->read('PropertyId'),
											'CustomerPass.pass_valid_upto >'=>$currentDateTime,
											'CustomerPass.membership_vaild_upto >'=>$currentDateTime,
											'CustomerPass.vehicle_id=Vehicle.id'
										)
									)
								),
								'conditions'=>array(
									'Vehicle.user_id'=>$this->Auth->user('id') 
									)
								));
		$activeIds=0;
		if(!empty($activeVehicles)){						
			$activeIds=Set::extract('/Vehicle/id',$activeVehicles);						
		}
		$this->Vehicle->recursive=-1;
		$customerOtherVehicles=$this->Vehicle->find('list',array('fields'=>array('owner'),'conditions'=>array('NOT'=>array('id'=>$activeIds),'user_id'=>$this->Auth->user('id'),'property_id'=>$this->Session->read('PropertyId'))));
		
        $this->loadModel('State');
        $state=new State();
        $states=$state->find('list');
        $this->set(compact('freeGuestCredits','remainingCredits','states','permitCost','customerOtherVehicles'));
	}
/************************
 * Manager All Vehicles
 */	
	public function manager_all_vehicles(){
			$this->layout='manager';
	}
	public function manager_get_all_vehicle_details(){
		$this->autoRender=$this->layout=false;
		$output=array();
		$output = $this->Vehicle->getManagerAllVehicleData();
        echo json_encode($output);
	}
	public function manager_other_vehicles(){
			$this->layout='manager';
	}
	public function manager_get_other_vehicle_details(){
		$this->autoRender=$this->layout=false;
		$output=array();
		$output = $this->Vehicle->getManagerOtherVehicleData();
        echo json_encode($output);
	}
/************************
 * Adminstrator All Vehicles
 */	
	public function admin_all_vehicles(){
			$this->layout='administrator';
			$this->Session->write('PropertySelection',true);
	}
	public function admin_get_all_vehicle_details(){
		$this->autoRender=$this->layout=false;
		$output=array();
		$output = $this->Vehicle->getManagerAllVehicleData();
        echo json_encode($output);
	}
	public function admin_other_vehicles(){
			$this->layout='administrator';
			$this->Session->write('PropertySelection',true);
	}
	public function admin_get_other_vehicle_details(){
		$this->autoRender=$this->layout=false;
		$output=array();
		$output = $this->Vehicle->getManagerOtherVehicleData();
        echo json_encode($output);
	}
	
/*************************************************************
 * Search SuperAdmin  //////// Changes  Done On 7th Nov to Add VIN Search
 *
 */
	public function admin_search(){
		if($this->Auth->user('role_id')==1){
			$this->layout='default';
		}else{
			$this->layout='administrator';
		}
		$searchArray=array();
		if ($this->request->is(array('post', 'put'))) { 
			$this->request->data['Vehicle']['search_key']=trim($this->request->data['Vehicle']['search_key']);
			//debug($this->request->data);
			//die;
			$this->loadModel('CustomerPass');
			$this->Vehicle->unbindModel(array('hasMany'=>array('CustomerPass')));
			$this->Vehicle->recursive=-1;	
					$customerVehicles=$this->Vehicle->find('all',
							array(
								'fields'=>'Vehicle.*,CustomerPass.*,User.*',
								'joins'=>array(
									array(
										'table'=>'customer_passes',
										'alias'=>'CustomerPass',
										'type'=>'LEFT',
										'conditions'=>array(
											'CustomerPass.vehicle_id=Vehicle.id'
										)
									),
									array(
										'table'=>'users',//Changes Done on 11th Nov to Add User DATA
										'alias'=>'User',
										'type'=>'LEFT',
										'conditions'=>array(
											'Vehicle.user_id=User.id'
										)
									)
								),
								//Changes Done on 7th Nov to enable Vin Search
								'conditions'=>array("MATCH(Vehicle.license_plate_number,Vehicle.last_4_digital_of_vin) AGAINST ('+".$this->request->data['Vehicle']['search_key']."*' IN BOOLEAN MODE) "
									//"license_plate_number LIKE '%".$this->request->data['Vehicle']['search_key']."%' OR last_4_digital_of_vin LIKE '%".$this->request->data['Vehicle']['search_key']."%' OR User.last_name LIKE '%".$this->request->data['Vehicle']['search_key']."%'"								
									)
								));
				//debug($customerVehicles);die;
			if(!empty($customerVehicles)){
				$status="unknown";
				foreach($customerVehicles as $vehicle){
					$status="unknown";
					if(is_null($vehicle['Vehicle']['customer_pass_id'])){
						$vehicle['CustomerPass']['RFID_tag_number']='No RFID';
						$vehicle['CustomerPass']['pass_valid_upto']='No Pass';
						$vehicle['CustomerPass']['membership_vaild_upto']=NULL;
						$status="noRFID";
					}
					$searchArray[]=array('Vehicle'=>array('make'=>$vehicle['Vehicle']['make'],
														  'model'=>$vehicle['Vehicle']['model'],
														  'color'=>$vehicle['Vehicle']['color'],
														  'license_plate_number'=>$vehicle['Vehicle']['license_plate_number'],
														  'vin'=>$vehicle['Vehicle']['last_4_digital_of_vin'],
														  'license_plate_state'=>$vehicle['Vehicle']['license_plate_state'],
														  'RFID_tag_number'=>$vehicle['CustomerPass']['RFID_tag_number'],
														  'pass_valid_upto'=>$vehicle['CustomerPass']['pass_valid_upto'],
														  'membership_vaild_upto'=>$vehicle['CustomerPass']['membership_vaild_upto'],
														  'last_seen'=>$vehicle['CustomerPass']['last_seen'],
														  'status'=>$status,
														  'id'=>$vehicle['Vehicle']['id'],
														  'nameOfUser'=>$vehicle['User']['first_name'].' '.$vehicle['User']['last_name'],
														  'username'=>"<a href='/admin/users/view_user_details/".$vehicle['User']['id']."'>".$vehicle['User']['username']."</a>",
														  'contact'=>$vehicle['User']['phone']
									 					)					
						);
					}	
			}
				$this->CustomerPass->unbindModel(array('belongsTo'=>array('User','Property','Transaction','Package','Pass')));
				$rfidVehicles=$this->CustomerPass->find('all',
							array(
								'fields'=>'Vehicle.*,CustomerPass.*,User.*',
								'joins'=>array(
									array(
										'table'=>'vehicles',
										'alias'=>'vehicle',
										'type'=>'LEFT',
										'conditions'=>array(
											'vehicle.id=CustomerPass.vehicle_id'
										)
									),
									array(
										'table'=>'users',
										'alias'=>'User',
										'type'=>'LEFT',
										'conditions'=>array(
											'CustomerPass.user_id=User.id'
										)
									)
								),
								'conditions'=>array("MATCH(CustomerPass.RFID_tag_number) AGAINST ('+".$this->request->data['Vehicle']['search_key']."*'IN BOOLEAN MODE) OR MATCH(User.last_name) AGAINST ('+".$this->request->data['Vehicle']['search_key']."*' IN BOOLEAN MODE)"
									//"RFID_tag_number LIKE '%".$this->request->data['Vehicle']['search_key']."%' OR User.last_name LIKE '%".$this->request->data['Vehicle']['search_key']."%'"									
									)
							));
				if(!empty($rfidVehicles)){
				foreach($rfidVehicles as $vehicle1){
					$status="unknown";
					$id=$vehicle1['Vehicle']['id'];
					if(is_null($vehicle1['CustomerPass']['vehicle_id'])){
						$vehicle1['Vehicle']['last_4_digital_of_vin']=$vehicle1['Vehicle']['make']=$vehicle1['Vehicle']['model']=$vehicle1['Vehicle']['color']=$vehicle1['Vehicle']['license_plate_number']=$vehicle1['Vehicle']['license_plate_state']='No Vehicle';
						$status="noVehicle";
						$id=$vehicle1['CustomerPass']['id'];
					}
					$searchArray[]=array('Vehicle'=>array('make'=>$vehicle1['Vehicle']['make'],
														  'model'=>$vehicle1['Vehicle']['model'],
														  'color'=>$vehicle1['Vehicle']['color'],
														  'license_plate_number'=>$vehicle1['Vehicle']['license_plate_number'],
														  'vin'=>$vehicle1['Vehicle']['last_4_digital_of_vin'],
														  'license_plate_state'=>$vehicle1['Vehicle']['license_plate_state'],
														  'RFID_tag_number'=>$vehicle1['CustomerPass']['RFID_tag_number'],
														  'pass_valid_upto'=>$vehicle1['CustomerPass']['pass_valid_upto'],
														  'membership_vaild_upto'=>$vehicle1['CustomerPass']['membership_vaild_upto'],
														  'last_seen'=>$vehicle1['CustomerPass']['last_seen'],
														  'status'=>$status,
														  'id'=>$id,
														  'nameOfUser'=>$vehicle1['User']['first_name'].' '.$vehicle1['User']['last_name'],
														  'username'=>"<a href='/admin/users/view_user_details/".$vehicle1['User']['id']."'>".$vehicle1['User']['username']."</a>",
														  'contact'=>$vehicle1['User']['phone']
														)					
						);
					}
				}
		$searchArray = array_map("unserialize", array_unique(array_map("serialize", $searchArray)));
		//$searchArray=array_unique($searchArray);
		$this->set(compact('searchArray'));
		}
	}
/*************************************************************
 * Search Property Administrator  //////// Changes  Done On 7th Nov to Add VIN Search
 *
 */
	public function admin_pa_search(){
		$this->Session->write('PropertySelection',false);
		$this->layout='administrator';
		//debug($this->request->data);die;
		if ($this->request->is(array('post', 'put'))) { 
			$this->request->data['Vehicle']['search_key']=trim($this->request->data['Vehicle']['search_key']);
			$searchArray=array();
			$this->loadModel('PropertyUser');
			$this->PropertyUser->recursive=-1;
			$adminProperties=$this->PropertyUser->find('list',array('fields'=>array('property_id'),'conditions'=>array('user_id'=>$this->Auth->user('id'))));
			if(count($adminProperties)==1){
				$s=array_values($adminProperties);
				$propertyIds=$s[0];
			}else{
				$propertyIds=array_values($adminProperties);
			}
			$this->loadModel('CustomerPass');
			$this->Vehicle->unbindModel(array('hasMany'=>array('CustomerPass')));
			$this->Vehicle->recursive=-1;
					$conditionRFID=$conditionPlateOrVin="";
					if(is_array($propertyIds)){
						$conditionPlateOrVin=array("( license_plate_number LIKE '%".$this->request->data['Vehicle']['search_key']."%' OR last_4_digital_of_vin LIKE '%".$this->request->data['Vehicle']['search_key']."%' OR User.last_name LIKE '%".$this->request->data['Vehicle']['search_key']."%')",'Vehicle.property_id IN '=>$propertyIds);
						$conditionRFID=array('CustomerPass.property_id IN'=>$propertyIds,"( RFID_tag_number LIKE '%".$this->request->data['Vehicle']['search_key']."%' OR User.last_name LIKE '%".$this->request->data['Vehicle']['search_key']."%' ) ");
						
					}else{
						$conditionPlateOrVin=array("( license_plate_number LIKE '%".$this->request->data['Vehicle']['search_key']."%' OR last_4_digital_of_vin LIKE '%".$this->request->data['Vehicle']['search_key']."%' OR User.last_name LIKE '%".$this->request->data['Vehicle']['search_key']."%')",'Vehicle.property_id '=>$propertyIds);
						$conditionRFID=array('CustomerPass.property_id '=>$propertyIds,"( RFID_tag_number LIKE '%".$this->request->data['Vehicle']['search_key']."%' OR User.last_name LIKE '%".$this->request->data['Vehicle']['search_key']."%' )");
					}
					$customerVehicles=$this->Vehicle->find('all',
							array(
								'fields'=>'Vehicle.*,CustomerPass.*,User.*',
								'joins'=>array(
									array(
										'table'=>'customer_passes',
										'alias'=>'CustomerPass',
										'type'=>'LEFT',
										'conditions'=>array(
											'CustomerPass.vehicle_id=Vehicle.id'
										)
									),
									array(
										'table'=>'users',//Changes Done on 11th Nov to Add User DATA
										'alias'=>'User',
										'type'=>'LEFT',
										'conditions'=>array(
											'Vehicle.user_id=User.id'
										)
									)
								),
								'conditions'=>$conditionPlateOrVin
								));
			
					if(!empty($customerVehicles)){
							$status="unknown";
							foreach($customerVehicles as $vehicle){
								$status="unknown";
								if(is_null($vehicle['Vehicle']['customer_pass_id'])){
										$vehicle['CustomerPass']['RFID_tag_number']='No RFID';
										$vehicle['CustomerPass']['pass_valid_upto']='No Pass';
										$vehicle['CustomerPass']['membership_vaild_upto']=NULL;
										$status="noRFID";
								}
								$searchArray[]=array('Vehicle'=>array('make'=>$vehicle['Vehicle']['make'],
														  'model'=>$vehicle['Vehicle']['model'],
														  'color'=>$vehicle['Vehicle']['color'],
														  'license_plate_number'=>$vehicle['Vehicle']['license_plate_number'],
														  'vin'=>$vehicle['Vehicle']['last_4_digital_of_vin'],
														  'license_plate_state'=>$vehicle['Vehicle']['license_plate_state'],
														  'RFID_tag_number'=>$vehicle['CustomerPass']['RFID_tag_number'],
														  'pass_valid_upto'=>$vehicle['CustomerPass']['pass_valid_upto'],
														  'membership_vaild_upto'=>$vehicle['CustomerPass']['membership_vaild_upto'],
														  'status'=>$status,
														  'id'=>$vehicle['Vehicle']['id'],
														  'nameOfUser'=>$vehicle['User']['first_name'].' '.$vehicle['User']['last_name'],
														  'username'=>"<a href='/admin/users/pa_view_user_details/".$vehicle['User']['id']."'>".$vehicle['User']['username']."</a>",
														  'contact'=>$vehicle['User']['phone']
									 					)					
												);
							}	
					}
				$this->CustomerPass->unbindModel(array('belongsTo'=>array('User','Property','Transaction','Package','Pass')));
				$rfidVehicles=$this->CustomerPass->find('all',
							array(
								'fields'=>'Vehicle.*,CustomerPass.*,User.*',
								'joins'=>array(
									array(
										'table'=>'vehicles',
										'alias'=>'vehicle',
										'type'=>'LEFT',
										'conditions'=>array(
											'vehicle.id=CustomerPass.vehicle_id'
										)
									),
									array(
										'table'=>'users',
										'alias'=>'User',
										'type'=>'LEFT',
										'conditions'=>array(
											'CustomerPass.user_id=User.id'
										)
									)
								),
								'conditions'=>$conditionRFID
							));
				if(!empty($rfidVehicles)){
				foreach($rfidVehicles as $vehicle1){
					$status="unknown";
					$id=$vehicle1['Vehicle']['id'];
					if(is_null($vehicle1['CustomerPass']['vehicle_id'])){
						$vehicle1['Vehicle']['last_4_digital_of_vin']=$vehicle1['Vehicle']['make']=$vehicle1['Vehicle']['model']=$vehicle1['Vehicle']['color']=$vehicle1['Vehicle']['license_plate_number']=$vehicle1['Vehicle']['license_plate_state']='No Vehicle';
						$status="noVehicle";
						$id=$vehicle1['CustomerPass']['id'];
					}
					$searchArray[]=array('Vehicle'=>array('make'=>$vehicle1['Vehicle']['make'],
														  'model'=>$vehicle1['Vehicle']['model'],
														  'color'=>$vehicle1['Vehicle']['color'],
														  'license_plate_number'=>$vehicle1['Vehicle']['license_plate_number'],
														  'vin'=>$vehicle1['Vehicle']['last_4_digital_of_vin'],
														  'license_plate_state'=>$vehicle1['Vehicle']['license_plate_state'],
														  'RFID_tag_number'=>$vehicle1['CustomerPass']['RFID_tag_number'],
														  'pass_valid_upto'=>$vehicle1['CustomerPass']['pass_valid_upto'],
														  'membership_vaild_upto'=>$vehicle1['CustomerPass']['membership_vaild_upto'],
														  'status'=>$status,
														  'id'=>$id,
														  'nameOfUser'=>$vehicle1['User']['first_name'].' '.$vehicle1['User']['last_name'],
														  'username'=>"<a href='/admin/users/pa_view_user_details/".$vehicle1['User']['id']."'>".$vehicle1['User']['username']."</a>",
														  'contact'=>$vehicle1['User']['phone']
														)					
						);
					}
				}
				
		$searchArray = array_map("unserialize", array_unique(array_map("serialize", $searchArray)));
		$this->set(compact('searchArray'));
		}	
	}
/*************************************************************
 * Search Manager //////// Changes  Done On 7th Nov to Add VIN Search
 */
	public function manager_search(){
		$this->Session->write('PropertySelection',false);
		$this->layout='manager';
		//debug($this->request->data);die;
		if ($this->request->is(array('post', 'put'))) { 
			$this->request->data['Vehicle']['search_key']=trim($this->request->data['Vehicle']['search_key']);
			$searchArray=array();
			$this->loadModel('CustomerPass');
			$this->Vehicle->unbindModel(array('hasMany'=>array('CustomerPass')));
			$this->Vehicle->recursive=-1;
				
					$customerVehicles=$this->Vehicle->find('all',
							array(
								'fields'=>'Vehicle.*,CustomerPass.*,User.*',
								'joins'=>array(
									array(
										'table'=>'customer_passes',
										'alias'=>'CustomerPass',
										'type'=>'LEFT',
										'conditions'=>array(
											'CustomerPass.vehicle_id=Vehicle.id'
										)
									),
									array(
										'table'=>'users',//Changes Done on 11th Nov to Add User DATA
										'alias'=>'User',
										'type'=>'LEFT',
										'conditions'=>array(
											'Vehicle.user_id=User.id'
										)
									)
								),
								'conditions'=>array(
									"( license_plate_number LIKE '%".$this->request->data['Vehicle']['search_key']."%' OR last_4_digital_of_vin LIKE '%".$this->request->data['Vehicle']['search_key']."%' OR User.last_name LIKE '%".$this->request->data['Vehicle']['search_key']."%' )",
									'Vehicle.property_id'=>CakeSession::read('PropertyId')								)
								));
					if(!empty($customerVehicles)){
							$status="unknown";
							foreach($customerVehicles as $vehicle){
								$status="unknown";
								if(is_null($vehicle['Vehicle']['customer_pass_id'])){
										$vehicle['CustomerPass']['RFID_tag_number']='No RFID';
										$vehicle['CustomerPass']['pass_valid_upto']='No Pass';
										$vehicle['CustomerPass']['membership_vaild_upto']=NULL;
										$status="noRFID";
								}
								$searchArray[]=array('Vehicle'=>array('make'=>$vehicle['Vehicle']['make'],
														  'model'=>$vehicle['Vehicle']['model'],
														  'color'=>$vehicle['Vehicle']['color'],
														  'license_plate_number'=>$vehicle['Vehicle']['license_plate_number'],
														  'vin'=>$vehicle['Vehicle']['last_4_digital_of_vin'],
														  'license_plate_state'=>$vehicle['Vehicle']['license_plate_state'],
														  'RFID_tag_number'=>$vehicle['CustomerPass']['RFID_tag_number'],
														  'pass_valid_upto'=>$vehicle['CustomerPass']['pass_valid_upto'],
														  'membership_vaild_upto'=>$vehicle['CustomerPass']['membership_vaild_upto'],
														  'status'=>$status,
														  'id'=>$vehicle['Vehicle']['id'],
														  'nameOfUser'=>$vehicle['User']['first_name'].' '.$vehicle['User']['last_name'],
														  'username'=>"<a href='/users/manager_view_user_details/".$vehicle['User']['id']."'>".$vehicle['User']['username']."</a>",
														  'contact'=>$vehicle['User']['phone']
									 					)					
												);
							}	
						}
				$this->CustomerPass->unbindModel(array('belongsTo'=>array('User','Property','Transaction','Package','Pass')));
				$rfidVehicles=$this->CustomerPass->find('all',
							array(
								'fields'=>'Vehicle.*,CustomerPass.*,User.*',
								'joins'=>array(
									array(
										'table'=>'vehicles',
										'alias'=>'vehicle',
										'type'=>'LEFT',
										'conditions'=>array(
											'vehicle.id=CustomerPass.vehicle_id'
										)
									),
									array(
										'table'=>'users',
										'alias'=>'User',
										'type'=>'LEFT',
										'conditions'=>array(
											'CustomerPass.user_id=User.id'
										)
									)
								),
								'conditions'=>array('CustomerPass.property_id'=>CakeSession::read('PropertyId'),
									"( RFID_tag_number LIKE '%".$this->request->data['Vehicle']['search_key']."%' OR User.last_name LIKE '%".$this->request->data['Vehicle']['search_key']."%' )"
																		)
							));
				if(!empty($rfidVehicles)){
				foreach($rfidVehicles as $vehicle1){
					$status="unknown";
					$id=$vehicle1['Vehicle']['id'];
					if(is_null($vehicle1['CustomerPass']['vehicle_id'])){
						$vehicle1['Vehicle']['last_4_digital_of_vin']=$vehicle1['Vehicle']['make']=$vehicle1['Vehicle']['model']=$vehicle1['Vehicle']['color']=$vehicle1['Vehicle']['license_plate_number']=$vehicle1['Vehicle']['license_plate_state']='No Vehicle';
						$status="noVehicle";
						$id=$vehicle1['CustomerPass']['id'];
					}
					$searchArray[]=array('Vehicle'=>array('make'=>$vehicle1['Vehicle']['make'],
														  'model'=>$vehicle1['Vehicle']['model'],
														  'color'=>$vehicle1['Vehicle']['color'],
														  'license_plate_number'=>$vehicle1['Vehicle']['license_plate_number'],
														  'vin'=>$vehicle1['Vehicle']['last_4_digital_of_vin'],
														  'license_plate_state'=>$vehicle1['Vehicle']['license_plate_state'],
														  'RFID_tag_number'=>$vehicle1['CustomerPass']['RFID_tag_number'],
														  'pass_valid_upto'=>$vehicle1['CustomerPass']['pass_valid_upto'],
														  'membership_vaild_upto'=>$vehicle1['CustomerPass']['membership_vaild_upto'],
														  'status'=>$status,
														  'id'=>$id,
														  'nameOfUser'=>$vehicle1['User']['first_name'].' '.$vehicle1['User']['last_name'],
														  'username'=>"<a href='/users/manager_view_user_details/".$vehicle1['User']['id']."'>".$vehicle1['User']['username']."</a>",
														  'contact'=>$vehicle1['User']['phone']
														)					
						);
					}
				}
			
		
		$searchArray = array_map("unserialize", array_unique(array_map("serialize", $searchArray)));
		$this->set(compact('searchArray'));
		}	
	}
	public function admin_get_search(){
		$this->autoRender=$this->layout=false;
		$output=array();
		$output = $this->Vehicle->getAllVehicle();
        echo json_encode($output);
	}
/******************************
 * Super Admin Property Wise Vehicle Details
 */
	public function admin_get_property_vehicle_details($propertyId){
		$this->autoRender=$this->layout=false;
		$output=array();
		$output = $this->Vehicle->adminPropertyVehicles($propertyId);
        echo json_encode($output);
	}

/******************************
 * Super Admin View Property Wise Vehicle Details
 */
	public function admin_complete_vehicle_detail($vehicleId){
		if (!$this->Vehicle->exists($vehicleId)) {
            throw new NotFoundException(__('Invalid Vehicle'));
        }
		$this->Vehicle->unbindModel(array('belongsTo'=>array('Property')));
		$this->Vehicle->recursive=1;
		$this->request->data=$this->Vehicle->find('first',array('conditions'=>array('Vehicle.id'=>$vehicleId)));
	}
/******************************
 * Super Admin Property Wise Vehicle Details REPORTS
 */	
	public function admin_list_vehicles(){
		$this->loadModel('Property');
		$property_list=$this->Property->find('list',array('fields'=>'name'));
		$id=array_keys($property_list);
		$selectedId=$id[0];
		$this->set(compact('property_list','selectedId'));		
	}
/******************************
 * Property Admin Property Wise Vehicle Details REPORTS
 */	
	public function admin_list_vehicles_pa(){
		$this->layout='administrator';
		
		
		$this->loadModel('Property');
		$property_list=CakeSession::read('AdminAllProperty');
		$id=array_keys($property_list);
		$selectedId=$id[0];
		$this->set(compact('property_list','selectedId'));		
	}
/*************************************************************
 * Add Vehicles in sequence
 */ 
 
 	public function add_vehicles() {
		$this->layout=$this->autoRender=false;
		$this->loadModel('Package');
		$this->loadModel('Pass');
		$this->loadModel('CustomerPass');
		$this->Package->recursive=-1;
		$guestPassArray=$this->Package->find('all',array('fields'=>array('id','pass_id'),'conditions'=>array('property_id'=>CakeSession::read('PropertyId'),'is_guest'=>1)));
		$guestPackageIds=array();
		$guestPassIds=array();
		if(!empty($guestPassArray)){
			for($i=0;$i<count($guestPassArray);$i++){
				$guestPackageIds[]=$guestPassArray[$i]['Package']['id'];
				$guestPassIds[]=$guestPassArray[$i]['Package']['pass_id'];
			}
			$passArray=array();
			if(count($guestPassIds)==1){
				$this->CustomerPass->recursive=-1;
				$passArray=$this->CustomerPass->find('all',array('fields'=>array('id','pass_id'),'conditions'=>array('user_id'=>AuthComponent::user('id'),'vehicle_id'=>NULL,'pass_id !='=>$guestPassIds[0],'package_id !='=>NULL)));
			}else{
				$this->CustomerPass->recursive=-1;
				$passArray=$this->CustomerPass->find('all',array('fields'=>array('id','pass_id'),'conditions'=>array('user_id'=>AuthComponent::user('id'),'vehicle_id'=>NULL,'pass_id NOT IN'=>$guestPassIds,'package_id !='=>NULL)));
			}
			if(!empty($passArray)){
				$passName=$this->Pass->givePassName($passArray[0]['CustomerPass']['pass_id']);
				$str="Add Vehicle to ".$passName." pass";
				$this->Session->write('Allowed',false);
				$this->Session->setFlash($str, 'notice');
				return $this->redirect(array('controller' => 'Vehicles', 'action' => 'add',$passArray[0]['CustomerPass']['id']));
			}else{
			  return $this->redirect(array('controller' => 'CustomerPasses', 'action' => 'view'));
			} 
		}else{
			$this->CustomerPass->recursive=-1;
			$passArray=$this->CustomerPass->find('all',array('fields'=>array('id','pass_id'),'conditions'=>array('user_id'=>AuthComponent::user('id'),'vehicle_id'=>NULL,'package_id !='=>NULL)));
			if(!empty($passArray)){
				$passName=$this->Pass->givePassName($passArray[0]['CustomerPass']['pass_id']);
				$str="Add Vehicle to ".$passName." pass";
				$this->Session->setFlash($str, 'notice');
				$this->Session->write('Allowed',false);
				return $this->redirect(array('controller' => 'Vehicles', 'action' => 'add',$passArray[0]['CustomerPass']['id']));
			}else{
			  return $this->redirect(array('controller' => 'CustomerPasses', 'action' => 'view'));
			} 
		}    
	}
/****************************************************************
 * Admin User Vehicles
 */
 	public function admin_vehicle_index() {
		$this->layout='admin_customer';
		$this->view='index';
		//debug($this->Auth->user('id'));
		//die();
		$dt = new DateTime();
        $currentDateTime= $dt->format('Y-m-d H:i:s');
		$this->Vehicle->recursive=1;
		$this->Vehicle->unbindModel(array('hasMany'=>array('CustomerPass')));
		$customerOtherVehicles=$activeVehicles=array();
		$activeVehicles=$this->Vehicle->find('all',
							array(
								'fields'=>'Vehicle.*,CustomerPass.*',
								'joins'=>array(
									array(
										'table'=>'customer_passes',
										'alias'=>'CustomerPass',
										'type'=>'INNER',
										'conditions'=>array(
											'CustomerPass.property_id'=>CakeSession::read('PropertyIdAdmin'),
											'CustomerPass.pass_valid_upto >'=>$currentDateTime,
											'CustomerPass.membership_vaild_upto >'=>$currentDateTime,
											'CustomerPass.vehicle_id=Vehicle.id'
										)
									)
								),
								'conditions'=>array(
									'Vehicle.user_id'=>CakeSession::read('UserIdAdmin')
									)
								));
		$activeIds=0;
		if(!empty($activeVehicles)){						
			$activeIds=Set::extract('/Vehicle/id',$activeVehicles);						
		}
		$this->Vehicle->recursive=-1;
		$customerOtherVehicles=$this->Vehicle->find('all',array('conditions'=>array('NOT'=>array('id'=>$activeIds),'user_id'=>CakeSession::read('UserIdAdmin'),'property_id'=>CakeSession::read('PropertyIdAdmin'))));
		
		$this->set(compact('activeVehicles','customerOtherVehicles'));
	}
/***********************************************************
 * Admin Select Guest Options
 */
  	public function admin_setGuestPackageOptions($customerPassId=null,$passId=null,$permitId=null,$userId,$propertyIdAdmin)
	{
		$this->loadModel('Property');
		$this->Property->recursive=-1;
		$propertyName=$this->Property->find('first',array('fields'=>array('name'),'conditions'=>array('id'=>$propertyIdAdmin)));
		$this->loadModel('User');
		$this->User->recursive=-1;
     	$userDetails=$this->User->find('first',array('conditions'=>array('id'=>$userId)));
     	
		$this->layout='admin_customer';
	//	$this->view="set_guest_package_options";
		 $this->loadModel('CustomerPass');
		if (!$this->CustomerPass->exists($customerPassId)) {
            throw new NotFoundException(__('Invalid Customer Pass'));
        }else{
			$userID=$this->CustomerPass->field('user_id',array('id'=>$customerPassId));
			if($userID != $userId){
				 throw new NotFoundException(__('Invalid DATA'));
			}
			$rfid=$this->CustomerPass->field('RFID_tag_number',array('id'=>$customerPassId));
			if(is_null($rfid)){
				//$this->Session->setFlash('Sorry, As RFID tag has not been assigned yet, Pass cannot be activated','warning');  
				//$this->redirect(array('controller'=>'CustomerPasses','action'=>'customer_view',$userId));
			}
		}
		 $this->loadModel('Package');
		if (!$this->Package->exists($permitId)) {
            throw new NotFoundException(__('Invalid package'));
        }
        $this->loadModel('Pass');
        if (!$this->Pass->exists($passId)) {
            throw new NotFoundException(__('Invalid pass'));
        }
        $packageDate=$this->CustomerPass->field('membership_vaild_upto',array('id'=>$customerPassId));
        if(!is_null($packageDate)){
			$dt = new DateTime();
			$currentDateTime= $dt->format('Y-m-d H:i:s');
			if($packageDate>$currentDateTime){
				$this->Session->setFlash('This pass is already active','warning');
				$this->redirect(array('controller'=>'CustomerPasses','action'=>'customer_view',$userId));
			}
		}
        $this->loadModel('Property');
        $freeGuestCredits=$this->Property->field('free_guest_credits',array('id'=>$propertyIdAdmin));
        $permitCost=$this->Package->field('cost',array('id'=>$permitId));
        $this->loadModel('User');
        $remainingCredits=$this->User->field('guest_credits',array('id'=>$userId));
        if ($this->request->is('post')) { 
				$this->CustomerPass->updateAll(array('CustomerPass.vehicle_id'=>NULL),array('CustomerPass.id'=>$customerPassId));
				$this->Vehicle->updateAll(array('Vehicle.customer_pass_id'=>NULL),array('Vehicle.customer_pass_id'=>$customerPassId));
				$toDate=$this->request->data['to'];
				$toDate = date("Y-m-d", strtotime($toDate));
				$dt = new DateTime();
				$currentDateTime= $dt->format('Y-m-d');
				$datetime1 = date_create($toDate);
				$datetime2 = date_create($currentDateTime);
				$interval = date_diff($datetime1, $datetime2);
				$currentCreditUsed=$interval->days;
				$newCredits=0;
				$amount=0;
				if((int)$remainingCredits>=$interval->days){
					$newCredits=$remainingCredits-$interval->days;
					$amount=0;
				}else{
					$newCredits=0;
					$creditPayable=$interval->days-$remainingCredits;  
					$amount=$creditPayable*$permitCost;
				}
				
				if(!empty($this->request->data['Vehicle']['vehicle_id'])){
					$this->Vehicle->updateValidations();
					
				}else{
					$this->request->data['Vehicle']['property_id']=$propertyIdAdmin;
					$this->Vehicle->validate=$this->Vehicle->validateAdmin;
				}
				$this->Vehicle->set($this->request->data);
				
				
				if($this->Vehicle->validates()){	
					
					$date = new DateTime();
					$date->add(new DateInterval('P'.$interval->days.'D'));
					$permitExpiryDate=date_format($date, 'Y-m-d H:i:s');
					$vehicleID=null;
					if(empty($this->request->data['Vehicle']['vehicle_id'])){
						$vehicle['Vehicle']=array('owner'=>$this->request->data['Vehicle']['owner'],
											  'make'=>$this->request->data['Vehicle']['make'],
											  'model'=>$this->request->data['Vehicle']['model'],
											  'color'=>$this->request->data['Vehicle']['color'],
											  'license_plate_number'=>$this->request->data['Vehicle']['license_plate_number'],
											  'license_plate_state'=>$this->request->data['Vehicle']['license_plate_state'],
											  'last_4_digital_of_vin'=>$this->request->data['Vehicle']['last_4_digital_of_vin'],
											  'user_id'=>$userId,
											  'property_id'=>$propertyIdAdmin
												);
								
									$this->Vehicle->create();
									if($this->Vehicle->save($vehicle,false)){
										CakeLog::write('newGuestVehicleAddedAdmin', ''.AuthComponent::user('username').' : New Guest Vehicle Added with vehicle ID:  <a href="/admin/Vehicles/edit/'.$this->Vehicle->getLastInsertID().'">'.$this->Vehicle->getLastInsertID().' </a> by User: '.AuthComponent::user('first_name').' '.AuthComponent::user('last_name').' In Property: '.$propertyName['Property']['name'].'');
										$vehicleID=$this->Vehicle->getLastInsertId();
										$this->Session->setFlash('Your vehicle details has been saved','success');
									}else{
										$this->Session->setFlash('Vehicle not saved','error');
										$this->redirect(array('action'=>'setGuestPackageOptions',$customerPassId,$passId,$permitId,$userId,$propertyIdAdmin));
									}
								
					}else{
						$vehicleID=$this->request->data['Vehicle']['vehicle_id'];
						$this->CustomerPass->updateAll(array('CustomerPass.vehicle_id'=>NULL),array('CustomerPass.vehicle_id'=>$vehicleID));
						$this->Vehicle->updateAll(array('Vehicle.customer_pass_id'=>NULL),array('Vehicle.customer_pass_id'=>$customerPassId));
					}
					if($amount!=0)
					{		
						
							$this->Session->write('toDate',$toDate);
							$this->Session->write('vehicleID',$vehicleID);
							$this->redirect(array('controller'=>'Transactions','action'=>'guestCheckOut',$customerPassId,$passId,$permitId,$userId,$propertyIdAdmin));
						
					}
					else{
						 $this->request->data['Transaction']['card_number']='1111111111111111';
						 $this->request->data['Transaction']['cvv']='111';
						 $this->request->data['Transaction']['month']='11';
						 $this->request->data['Transaction']['year']='9999';
						 $this->request->data['Transaction']['first_name_cc']='Netgen';
						 $this->request->data['Transaction']['last_name_cc']='Solutions';				 
					}
									$response=array('result'=>'Free guest credits tarnsaction');
									$dt = new DateTime();
									$currentDateTime= $dt->format('Y-m-d H:i:s');
									$dateTransaction = $currentDateTime;
									$arraySerialize=serialize($response);
									$transactionArray['Transaction']['paypal_tranc_id']="Free Guest";
									$transactionArray['Transaction']['user_id']=$userId;
									$transactionArray['Transaction']['date_time']=$dateTransaction;
									$transactionArray['Transaction']['amount']=0;
									$transactionArray['Transaction']['result']='Success';
									$transactionArray['Transaction']['pass_id']=$passId;
									$transactionArray['Transaction']['transaction_array']=$arraySerialize;
									$transactionArray['Transaction']['first_name']='Free';
									$transactionArray['Transaction']['last_name']='Guest';	
									$transactionArray['Transaction']['comments'] = "guestPassActivated";
									$this->loadModel('Transaction');
									$this->Transaction->create();
									if ($this->Transaction->save($transactionArray,false)) {
												$transactionId=$this->Transaction->getLastInsertId();
												$this->loadModel('Pass');
												$passExpiry=$this->Pass->field('expiration_date',array('id'=>$passId));
												$this->loadModel('CustomerPass');
												$customerPassArray['CustomerPass']['transaction_id']=$transactionId;
												$customerPassArray['CustomerPass']['vehicle_id']=$vehicleID;
												$customerPassArray['CustomerPass']['membership_vaild_upto']= $permitExpiryDate;
												$customerPassArray['CustomerPass']['id']=$customerPassId;
												$customerPassArray['CustomerPass']['package_id']= $permitId;
												$customerPassArray['CustomerPass']['is_guest_pass']=1;
												$this->CustomerPass->create();
												if($this->CustomerPass->save($customerPassArray,false)){
																	$this->loadModel('UserGuestPass');
																	$vehicleInfo=$this->Vehicle->field('vehicle_info',array('id'=>$vehicleID));
																	$userPassArray=array('UserGuestPass'=>array('user_id'=>$userId,
																												'customer_pass_id'=>$customerPassId,
																												'vehicle_id'=>$vehicleID,
																												'vehicle_details'=>$vehicleInfo,
																												'days'=>$currentCreditUsed,
																												'amount'=>$amount,
																												'to_date'=>$permitExpiryDate,
																												'property_id'=>$propertyIdAdmin
																	));
																	$this->UserGuestPass->save($userPassArray,false); 
																	CakeLog::write('guestPassUpdatedAdmin', ''.AuthComponent::user('username').' : Guest Pass Updated, Vehicle Assigned With Vehicle ID:  <a href="/admin/Vehicles/edit/'.$vehicleID.'">'.$vehicleID.' </a> And Customer Pass ID: <a href="/admin/CustomerPasses/edit/'.$customerPassId.'">'.$customerPassId.'</a> by User'.AuthComponent::user('first_name').' '.AuthComponent::user('last_name').' In Property '.CakeSession::read('PropertyNameAdmin').'For '.$currentCreditUsed.' Days, Amount Paid: $'.$amount.'');
																	$this->Vehicle->query('UPDATE vehicles SET customer_pass_id='.$customerPassId.' where id = '.$vehicleID.'');
																	$this->loadModel('User');
																	$user['User']['id']=$userId;
																	$user['User']['guest_credits']=$newCredits;
																	$this->User->create();
																	if($this->User->save($user,false)){
																		$this->Session->setFlash('Your pass has been activated','sucess');
																		$this->Session->delete('Allowed');
																		$this->Session->write('Allowed',true);
																		return $this->redirect(array('controller'=>'CustomerPasses','action' => 'customer_view',$userId));
																	}else{
																			$this->Session->setFlash('User Table not updated','error');															
																	}
															
														}												
													}else {
												$this->Session->setFlash('Pass not saved','error');												
											}								
									
				    }else{
						$this->Session->setFlash('Validation Failed','error');
					}
		   } 
		$dt = new DateTime();
        $currentDateTime= $dt->format('Y-m-d H:i:s');
		$this->Vehicle->recursive=1;
		$this->Vehicle->unbindModel(array('hasMany'=>array('CustomerPass')));
		$customerOtherVehicles=$activeVehicles=array();
		$activeVehicles=$this->Vehicle->find('all',
							array(
								'fields'=>'Vehicle.*,CustomerPass.*',
								'joins'=>array(
									array(
										'table'=>'customer_passes',
										'alias'=>'CustomerPass',
										'type'=>'INNER',
										'conditions'=>array(
											'CustomerPass.property_id'=>$propertyIdAdmin,
											'CustomerPass.pass_valid_upto >'=>$currentDateTime,
											'CustomerPass.membership_vaild_upto >'=>$currentDateTime,
											'CustomerPass.vehicle_id=Vehicle.id'
										)
									)
								),
								'conditions'=>array(
									'Vehicle.user_id'=>$userId
									)
								));
		$activeIds=0;
		if(!empty($activeVehicles)){						
			$activeIds=Set::extract('/Vehicle/id',$activeVehicles);						
		}
		$this->Vehicle->recursive=-1;
		$customerOtherVehicles=$this->Vehicle->find('list',array('fields'=>array('owner'),'conditions'=>array('NOT'=>array('id'=>$activeIds),'user_id'=>$userId,'property_id'=>$propertyIdAdmin)));
		
        $this->loadModel('State');
        $state=new State();
        $states=$state->find('list');
        $this->set(compact('freeGuestCredits','remainingCredits','states','permitCost','customerOtherVehicles','userId','propertyIdAdmin'));
    }
/******************************************************************
 * admin_optional_search method
 * Created On 2nd March 2016
 * By: Shalender
 * @return Search Results
 */
	public function admin_optional_search(){  
		if ($this->request->is('post')) {
			$this->loadModel('CustomerPass');
			$searchArray=array(); 
			//debug($this->request->data);die;
			//Vehicle Options
				if(($this->request->data['Vehicle']['search_option']=='license_plate_number')||($this->request->data['Vehicle']['search_option']=='last_4_digital_of_vin')){
						 $this->request->data['Vehicle']['search_key_to_search']=trim($this->request->data['Vehicle']['search_key_to_search']);
						$this->Vehicle->unbindModel(array('hasMany'=>array('CustomerPass')));
						$this->Vehicle->recursive=-1;	
						$customerVehicles=$this->Vehicle->find('all',
										array(
											'fields'=>'Vehicle.*,CustomerPass.*,User.*',
											'joins'=>array(
												array(
													'table'=>'customer_passes',
													'alias'=>'CustomerPass',
													'type'=>'LEFT',
													'conditions'=>array(
														'CustomerPass.vehicle_id=Vehicle.id'
													)
												),
												array(
													'table'=>'users',
													'alias'=>'User',
													'type'=>'LEFT',
													'conditions'=>array(
														'Vehicle.user_id=User.id'
													)
												)
											),
											'conditions'=>array(
													$this->request->data['Vehicle']['search_option']." LIKE '%".$this->request->data['Vehicle']['search_key_to_search']."%'"								
												)
											));
						if(!empty($customerVehicles)){
							$status="unknown";
							foreach($customerVehicles as $vehicle){
								$status="unknown";
								if(is_null($vehicle['Vehicle']['customer_pass_id'])){
									$vehicle['CustomerPass']['RFID_tag_number']='No RFID';
									$vehicle['CustomerPass']['pass_valid_upto']='No Pass';
									$vehicle['CustomerPass']['membership_vaild_upto']=NULL;
									$status="noRFID";
								}
								$searchArray[]=array('Vehicle'=>array('make'=>$vehicle['Vehicle']['make'],
																	  'model'=>$vehicle['Vehicle']['model'],
																	  'color'=>$vehicle['Vehicle']['color'],
																	  'license_plate_number'=>$vehicle['Vehicle']['license_plate_number'],
																	  'vin'=>$vehicle['Vehicle']['last_4_digital_of_vin'],
																	  'license_plate_state'=>$vehicle['Vehicle']['license_plate_state'],
																	  'RFID_tag_number'=>$vehicle['CustomerPass']['RFID_tag_number'],
																	  'pass_valid_upto'=>$vehicle['CustomerPass']['pass_valid_upto'],
																	  'membership_vaild_upto'=>$vehicle['CustomerPass']['membership_vaild_upto'],
																	  'last_seen'=>$vehicle['CustomerPass']['last_seen'],
																	  'status'=>$status,
																	  'id'=>$vehicle['Vehicle']['id'],
																	  'nameOfUser'=>$vehicle['User']['first_name'].' '.$vehicle['User']['last_name'],
																	  'username'=>"<a href='/admin/users/view_user_details/".$vehicle['User']['id']."'>".$vehicle['User']['username']."</a>",
																	  'contact'=>$vehicle['User']['phone']
																	)					
									);
								}	
						}
				}else{
					$customerConditions='';
					if($this->request->data['Vehicle']['search_option']=='RFID_tag_number'){
						$customerConditions="RFID_tag_number LIKE '%".$this->request->data['Vehicle']['search_key_to_search']."%'";
					}else{
						$customerConditions="User.last_name LIKE '%".$this->request->data['Vehicle']['search_key_to_search']."%'";
					}
					$this->CustomerPass->unbindModel(array('belongsTo'=>array('User','Property','Transaction','Package','Pass')));
					$rfidVehicles=$this->CustomerPass->find('all',
								array(
									'fields'=>'Vehicle.*,CustomerPass.*,User.*',
									'joins'=>array(
										array(
											'table'=>'vehicles',
											'alias'=>'VEHICLES',
											'type'=>'LEFT',
											'conditions'=>array(
												'VEHICLES.id=CustomerPass.vehicle_id'
											)
										),
										array(
											'table'=>'users',
											'alias'=>'User',
											'type'=>'LEFT',
											'conditions'=>array(
												'CustomerPass.user_id=User.id'
											)
										)
									),
									'conditions'=>array($customerConditions)
								));
					if(!empty($rfidVehicles)){
					foreach($rfidVehicles as $vehicle1){
						$status="unknown";
						$id=$vehicle1['Vehicle']['id'];
						if(is_null($vehicle1['CustomerPass']['vehicle_id'])){
							$vehicle1['Vehicle']['last_4_digital_of_vin']=$vehicle1['Vehicle']['make']=$vehicle1['Vehicle']['model']=$vehicle1['Vehicle']['color']=$vehicle1['Vehicle']['license_plate_number']=$vehicle1['Vehicle']['license_plate_state']='No Vehicle';
							$status="noVehicle";
							$id=$vehicle1['CustomerPass']['id'];
						}
						$searchArray[]=array('Vehicle'=>array('make'=>$vehicle1['Vehicle']['make'],
															  'model'=>$vehicle1['Vehicle']['model'],
															  'color'=>$vehicle1['Vehicle']['color'],
															  'license_plate_number'=>$vehicle1['Vehicle']['license_plate_number'],
															  'vin'=>$vehicle1['Vehicle']['last_4_digital_of_vin'],
															  'license_plate_state'=>$vehicle1['Vehicle']['license_plate_state'],
															  'RFID_tag_number'=>$vehicle1['CustomerPass']['RFID_tag_number'],
															  'pass_valid_upto'=>$vehicle1['CustomerPass']['pass_valid_upto'],
															  'membership_vaild_upto'=>$vehicle1['CustomerPass']['membership_vaild_upto'],
															  'last_seen'=>$vehicle1['CustomerPass']['last_seen'],
															  'status'=>$status,
															  'id'=>$id,
															  'nameOfUser'=>$vehicle1['User']['first_name'].' '.$vehicle1['User']['last_name'],
															  'username'=>"<a href='/admin/users/view_user_details/".$vehicle1['User']['id']."'>".$vehicle1['User']['username']."</a>",
															  'contact'=>$vehicle1['User']['phone']
															)					
							);
						}
				}
				
			}
			$searchArray = array_map("unserialize", array_unique(array_map("serialize", $searchArray)));
			$this->set(compact('searchArray'));
	  }
	}
		/*******************************************************************
	 *  New Guest Pass Add Options
	 * 
	 */
	public function setGuestPackageOptions($customerPassId=null,$passId=null,$permitId=null){
		return $this->redirect(array('controller'=>'CustomerPasses','action'=>'activateGuestPass',$customerPassId,$passId,$permitId));
		$this->layout='customer';
		 $this->loadModel('CustomerPass');
		if (!$this->CustomerPass->exists($customerPassId)) {
            throw new NotFoundException(__('Invalid Customer Pass'));
        }else{
			$userID=$this->CustomerPass->field('user_id',array('id'=>$customerPassId));
			if($userID != AuthComponent::user('id')){
				 throw new NotFoundException(__('Invalid DATA'));
			}
			$rfid=$this->CustomerPass->field('RFID_tag_number',array('id'=>$customerPassId));
			if(is_null($rfid)){
				$this->Session->setFlash('Sorry, As RFID tag has not been assigned yet, Pass cannot be activated','warning');
				$this->redirect(array('controller'=>'CustomerPasses','action'=>'view'));
			}
		}
		 $this->loadModel('Package');
		if (!$this->Package->exists($permitId)) {
            throw new NotFoundException(__('Invalid package'));
        }
        $this->loadModel('Pass');
        if (!$this->Pass->exists($passId)) {
            throw new NotFoundException(__('Invalid pass'));
        }
        $packageDate=$this->CustomerPass->field('membership_vaild_upto',array('id'=>$customerPassId));
        if(!is_null($packageDate)){
			$dt = new DateTime();
			$currentDateTime= $dt->format('Y-m-d H:i:s');
			if($packageDate>$currentDateTime){
				$this->Session->setFlash('This pass is already active','warning');
				$this->redirect(array('controller'=>'CustomerPasses','action'=>'view'));
			}
		}
        $this->loadModel('Property');
        $freeGuestCredits=$this->Property->field('free_guest_credits',array('id'=>$this->Session->read('PropertyId')));
        $permitCost=$this->Package->field('cost',array('id'=>$permitId));
        $remainingCredits=$this->Auth->user('guest_credits');
        
        if ($this->request->is('post')) { 
			$toDate=$this->request->data['Vehicle']['to'];
			$toDate = date("Y-m-d", strtotime($toDate));
			$passValidation=$this->CustomerPass->field('pass_valid_upto',array('id'=>$customerPassId));	
				$hr24=false;
				if($toDate>=$passValidation){
					$this->Session->setFlash('You cannot activate your permit beyond the pass expiry date','error');
					$toDate=date("Y-m-d", strtotime($passValidation));
					$hr24=true;
					//$toDate=$toDate." 23:59:59";
				}
				$dt = new DateTime();
				$currentDateTime= $dt->format('Y-m-d');
				$datetime1 = date_create($toDate);
				$datetime2 = date_create($currentDateTime);
				$interval = date_diff($datetime1, $datetime2);
				$currentCreditUsed=$interval->days;
				$newCredits=0;
				$amount=0;
				if((int)$remainingCredits>=$interval->days){
					$newCredits=$remainingCredits-$interval->days;
					$amount=0;
				}else{
					$newCredits=0;
					$creditPayable=$interval->days-$remainingCredits;
					$amount=$creditPayable*$permitCost;
				}
					
				$date = new DateTime();
				$date->add(new DateInterval('P'.$interval->days.'D'));
				$permitExpiryDate=date_format($date, 'Y-m-d H:i:s');
				if($hr24){
					$permitExpiryDate=date_format($date, 'Y-m-d');
					$permitExpiryDate=$permitExpiryDate." 23:59:59"; 
				}
				
				$vehicleID=null;
			   
				if($amount!=0) {					
						$this->Session->write('toDate',$toDate);
						$this->redirect(array('controller'=>'Transactions','action'=>'guestCheckOut',$customerPassId,$passId,$permitId));
					
				}
				else{
					 $this->request->data['Transaction']['card_number']='1111111111111111';
					 $this->request->data['Transaction']['cvv']='111';
					 $this->request->data['Transaction']['month']='11';
					 $this->request->data['Transaction']['year']='9999';
					 $this->request->data['Transaction']['first_name_cc']='Netgen';
					 $this->request->data['Transaction']['last_name_cc']='Solutions';				 
				}
				$response=array('result'=>'Free guest credits transaction');
				$dt = new DateTime();
				$currentDateTime= $dt->format('Y-m-d H:i:s');
				$dateTransaction = $currentDateTime;
				$arraySerialize=serialize($response);
				$transactionArray['Transaction']['paypal_tranc_id']="Free Guest";
				$transactionArray['Transaction']['user_id']=$this->Auth->user('id');
				$transactionArray['Transaction']['date_time']=$dateTransaction;
				$transactionArray['Transaction']['amount']=0;
				$transactionArray['Transaction']['result']='Success';
				$transactionArray['Transaction']['pass_id']=$passId;
				$transactionArray['Transaction']['transaction_array']=$arraySerialize;
				$transactionArray['Transaction']['first_name']='Free';
				$transactionArray['Transaction']['last_name']='Guest';	
				$transactionArray['Transaction']['comments'] = "guestPassActivated";
				$this->loadModel('Transaction');
				$this->Transaction->create();
				if ($this->Transaction->save($transactionArray,false)) {
							$transactionId=$this->Transaction->getLastInsertId();
							$this->loadModel('Pass');
							$passExpiry=$this->Pass->field('expiration_date',array('id'=>$passId));
							$this->loadModel('CustomerPass');
							$this->loadModel('Vehicle');
							$customerPassArray['CustomerPass']['transaction_id']=$transactionId;
							//$customerPassArray['CustomerPass']['vehicle_id']=$vehicleID;
							$customerPassArray['CustomerPass']['membership_vaild_upto']= $permitExpiryDate;
							$customerPassArray['CustomerPass']['id']=$customerPassId;
							$customerPassArray['CustomerPass']['package_id']= $permitId;
							$customerPassArray['CustomerPass']['is_guest_pass']=1;
							
							if($this->CustomerPass->save($customerPassArray,false)){
												$this->CustomerPass->updateAll(array('CustomerPass.vehicle_id'=>NULL),array('CustomerPass.id'=>$customerPassId));
												$this->Vehicle->updateAll(array('Vehicle.customer_pass_id'=>NULL),array('Vehicle.customer_pass_id'=>$customerPassId));
												$userPassArray=array('UserGuestPass'=>array('user_id'=>AuthComponent::user('id'),
																							'customer_pass_id'=>$customerPassId,
																							//'vehicle_id'=>$vehicleID,
																							//'vehicle_details'=>$vehicleInfo,
																							'days'=>$currentCreditUsed,
																							'amount'=>$amount,
																							'to_date'=>$permitExpiryDate,
																							'property_id'=>$this->Session->read('PropertyId')
												));
												$this->loadModel('UserGuestPass');
												if($this->UserGuestPass->save($userPassArray,false)){
													$userGuestPassId=	$this->UserGuestPass->getLastInsertID();
													CakeLog::write('guestPassUpdated', ''.AuthComponent::user('username').' : Guest Pass Updated, Customer Pass ID: <a href="/admin/CustomerPasses/edit/'.$customerPassId.'">'.$customerPassId.'</a> by User : '.AuthComponent::user('first_name').' '.AuthComponent::user('last_name').' In Property '.CakeSession::read('PropertyName').'For '.$currentCreditUsed.' Days, Amount Paid: $'.$amount.'');
												
													$this->loadModel('User');
													$user['User']['id']=$this->Auth->user('id');
													$user['User']['guest_credits']=$newCredits;
													$this->User->create();
													if($this->User->save($user,false)){
														$_SESSION['Auth']['User']['guest_credits']=$newCredits;
														$this->Session->setFlash('Please add vehicle details now.','success');
														$this->Session->delete('Allowed');
														$this->Session->write('Allowed',true);
														return $this->redirect('/Vehicles/add_guest_vehicle/'.$customerPassId.'/'.$userGuestPassId);
													}else{
															$this->Session->setFlash(__('User Table not updated','error'));															
													}
											  }else{
												$this->Session->setFlash(__('Internal Error Has Occured, Please Contact Admin','error'));						
											  }
									}												
								}else {
							$this->Session->setFlash('Pass not saved','error');												
						}								
								
				  
		   } 
		
        $this->set(compact('freeGuestCredits','remainingCredits','permitCost'));
	}
	public function add_guest_vehicle($customerPassId,$userGuestPass=null){ 
		$this->layout='customer';
		$this->loadModel('CustomerPass');
		if (!$this->CustomerPass->exists($customerPassId)) {
            throw new NotFoundException(__('Invalid Customer Pass'));
        }else{
			$userID=$this->CustomerPass->field('user_id',array('id'=>$customerPassId));
			if($userID != AuthComponent::user('id')){
				 throw new NotFoundException(__('Invalid DATA'));
			}
			$rfid=$this->CustomerPass->field('RFID_tag_number',array('id'=>$customerPassId));
			if(is_null($rfid)){
				$this->Session->setFlash('Sorry, As RFID tag has not been assigned yet, Pass cannot be activated','warning');
				$this->redirect(array('controller'=>'CustomerPasses','action'=>'view'));
			}
			$previousVehicle=$this->CustomerPass->field('vehicle_id',array('id'=>$customerPassId));
			if(!is_null($previousVehicle)){
				$this->Session->setFlash('A vehicle is already assigned to this pass','warning');
				$this->redirect(array('controller'=>'CustomerPasses','action'=>'view'));
			}
		}
        $packageDate=$this->CustomerPass->field('membership_vaild_upto',array('id'=>$customerPassId));
        if(!is_null($packageDate)){
			$dt = new DateTime();
			$currentDateTime= $dt->format('Y-m-d H:i:s');
			if($packageDate<$currentDateTime){
				$this->Session->setFlash('The permit is already expired please buy permit first.','warning');
				$this->redirect(array('controller'=>'CustomerPasses','action'=>'view'));
			}
		} 
		if($this->request->is('POST')){
			//debug($this->request->data);die;
			$vehicleID=null;
			if(!empty($this->request->data['Vehicle']['vehicle_id'])){
				$this->Vehicle->updateValidations();
			}
			$this->Vehicle->set($this->request->data);
			if($this->Vehicle->validates()){
				if(empty($this->request->data['Vehicle']['vehicle_id'])){
							$this->CustomerPass->recursive=-1;
							$vehicle['Vehicle']=array('owner'=>$this->request->data['Vehicle']['owner'],
										  'make'=>$this->request->data['Vehicle']['make'],
										  'model'=>$this->request->data['Vehicle']['model'],
										  'color'=>$this->request->data['Vehicle']['color'],
										  'license_plate_number'=>$this->request->data['Vehicle']['license_plate_number'],
										  'license_plate_state'=>$this->request->data['Vehicle']['license_plate_state'],
										  'last_4_digital_of_vin'=>$this->request->data['Vehicle']['last_4_digital_of_vin'],
										  'user_id'=>$this->Auth->user('id'),
										  'property_id'=>$this->Session->read('PropertyId')
											);
							if($this->Vehicle->save($vehicle,false)){
									CakeLog::write('newGuestVehicleAdded', ''.AuthComponent::user('username').' : New Guest Vehicle Added with vehicle ID:  <a href="/admin/Vehicles/edit/'.$this->Vehicle->getLastInsertID().'">'.$this->Vehicle->getLastInsertID().' </a> by User: '.AuthComponent::user('first_name').' '.AuthComponent::user('last_name').' In Property: '.CakeSession::read('PropertyName').'');
									$vehicleID=$this->Vehicle->getLastInsertId();
									//$this->Session->setFlash('Your vehicle details has been saved','success');
							}else{
								$this->Session->setFlash('Vehicle not saved','error');
							}
				}else{
					$vehicleID=$this->request->data['Vehicle']['vehicle_id'];
					$this->CustomerPass->updateAll(array('CustomerPass.vehicle_id'=>NULL),array('CustomerPass.vehicle_id'=>$vehicleID));
					$this->Vehicle->updateAll(array('Vehicle.customer_pass_id'=>NULL),array('Vehicle.customer_pass_id'=>$customerPassId));
				}
				if($vehicleID){
					$this->Vehicle->id=$vehicleID;
					$this->Vehicle->saveField('customer_pass_id',$customerPassId);
					$this->CustomerPass->id=$customerPassId; 
					$this->CustomerPass->saveField('vehicle_id',$vehicleID);
					CakeLog::write('guestVehicleAssigned', ''.AuthComponent::user('username').' : Guest Vehicle Assigned vehicle ID:  <a href="/admin/Vehicles/edit/'.$vehicleID.'">'.$vehicleID.' </a> by User: '.AuthComponent::user('first_name').' '.AuthComponent::user('last_name').' In Property: '.CakeSession::read('PropertyName').' and assigned to Customer Pass ID: <a href="/admin/CustomerPasses/edit/'.$customerPassId.'">'.$customerPassId.'</a>');
					$this->loadModel('UserGuestPass');
					$userGuestPassArr=array();
					$this->UserGuestPass->recursive=-1;
					if($userGuestPass){
						$userGuestPassArr=$this->UserGuestPass->find('first',array('conditions'=>array('UserGuestPass.id'=>$userGuestPass)));
					}else{
						$userGuestPassArr=$this->UserGuestPass->find('first',array('conditions'=>array('UserGuestPass.customer_pass_id'=>$customerPassId),'order'=>array('UserGuestPass.id'=>'DESC')));
					}
					if($userGuestPassArr){
						$vehicleInfo=$this->Vehicle->field('vehicle_info',array('id'=>$vehicleID));
						$userGuestPassArr['UserGuestPass']['vehicle_id']=$vehicleID;
						$userGuestPassArr['UserGuestPass']['vehicle_details']=$vehicleInfo;
						$this->UserGuestPass->save($userGuestPassArr,false); 
					}
					//debug($userGuestPassArr);die;
					$this->Session->setFlash('Vehicle has been added successfully','success');
					$this->redirect(array('controller'=>'CustomerPasses','action'=>'view'));
	
				}else{
					//$this->Session->setFlash('Internal error. Please try again','error');
				}
			}
		}
		$dt = new DateTime();
        $currentDateTime= $dt->format('Y-m-d H:i:s');
		$this->Vehicle->recursive=1;
		$this->Vehicle->unbindModel(array('hasMany'=>array('CustomerPass'))); 
		$customerOtherVehicles=$activeVehicles=array();
		$activeVehicles=$this->Vehicle->find('all', 
							array(
								'fields'=>'Vehicle.*,CustomerPass.*',
								'joins'=>array(
									array(
										'table'=>'customer_passes', 
										'alias'=>'CustomerPass',
										'type'=>'INNER',
										'conditions'=>array(
											'CustomerPass.property_id'=>$this->Session->read('PropertyId'), 
											'CustomerPass.pass_valid_upto >'=>$currentDateTime,
											'CustomerPass.membership_vaild_upto >'=>$currentDateTime,
											'CustomerPass.vehicle_id=Vehicle.id'
										)
									)
								),
								'conditions'=>array(
									'Vehicle.user_id'=>$this->Auth->user('id') 
									)
								));
		$activeIds=0;
		if(!empty($activeVehicles)){						
			$activeIds=Set::extract('/Vehicle/id',$activeVehicles);						
		}
		$this->Vehicle->recursive=-1;
		$customerOtherVehicles=$this->Vehicle->find('list',array('fields'=>array('owner'),'conditions'=>array('NOT'=>array('id'=>$activeIds),'user_id'=>$this->Auth->user('id'),'property_id'=>$this->Session->read('PropertyId'))));
		
        $this->loadModel('State');
        $state=new State();
        $states=$state->find('list');
        $this->set(compact('states','customerOtherVehicles'));
        /*debug($customerOtherVehicles);
		debug($customerPassId);
		debug($userGuestPass); 
		die;*/
	}
	/*******************************************************************
	 * Existing Vehicle details
	 */
	 public function my_vehicle_details($vehicleID){
		$this->layout=false;
		$vehicleDetails=$this->Vehicle->find('first',array('conditions'=>array('Vehicle.id'=>$vehicleID)));
		$this->set(compact('vehicleDetails'));
	 }
	  public function get_guest_vehicle_ajax($customerPassId,$userGuestPass=null){ 
		$this->layout=false;
		$this->loadModel('CustomerPass');
		if (!$this->CustomerPass->exists($customerPassId)) {
            throw new NotFoundException(__('Invalid Customer Pass'));
        }else{
			$userID=$this->CustomerPass->field('user_id',array('id'=>$customerPassId));
			if($userID != AuthComponent::user('id')){
				 throw new NotFoundException(__('Invalid DATA'));
			}
			$rfid=$this->CustomerPass->field('RFID_tag_number',array('id'=>$customerPassId));
			if(is_null($rfid)){
				$this->Session->setFlash('Sorry, As RFID tag has not been assigned yet, Pass cannot be activated','warning');
				$this->redirect(array('controller'=>'CustomerPasses','action'=>'view'));
			}
			$previousVehicle=$this->CustomerPass->field('vehicle_id',array('id'=>$customerPassId));
			if(!is_null($previousVehicle)){
				$this->Session->setFlash('A vehicle is already assigned to this pass','warning');
				$this->redirect(array('controller'=>'CustomerPasses','action'=>'view'));
			}
		}
        $packageDate=$this->CustomerPass->field('membership_vaild_upto',array('id'=>$customerPassId));
        if(!is_null($packageDate)){
			$dt = new DateTime();
			$currentDateTime= $dt->format('Y-m-d H:i:s');
			if($packageDate<$currentDateTime){
				$this->Session->setFlash('The permit is already expired please buy permit first.','warning');
				$this->redirect(array('controller'=>'CustomerPasses','action'=>'view'));
			}
		} 

		$dt = new DateTime();
        $currentDateTime= $dt->format('Y-m-d H:i:s');
		$this->Vehicle->recursive=1;
		$this->Vehicle->unbindModel(array('hasMany'=>array('CustomerPass'))); 
		$customerOtherVehicles=$activeVehicles=array();
		$activeVehicles=$this->Vehicle->find('all', 
							array(
								'fields'=>'Vehicle.*,CustomerPass.*',
								'joins'=>array(
									array(
										'table'=>'customer_passes', 
										'alias'=>'CustomerPass',
										'type'=>'INNER',
										'conditions'=>array(
											'CustomerPass.property_id'=>$this->Session->read('PropertyId'), 
											'CustomerPass.pass_valid_upto >'=>$currentDateTime,
											'CustomerPass.membership_vaild_upto >'=>$currentDateTime,
											'CustomerPass.vehicle_id=Vehicle.id'
										)
									)
								),
								'conditions'=>array(
									'Vehicle.user_id'=>$this->Auth->user('id') 
									)
								));
		$activeIds=0;
		if(!empty($activeVehicles)){						
			$activeIds=Set::extract('/Vehicle/id',$activeVehicles);						
		}
		$this->Vehicle->recursive=-1;
		$customerOtherVehicles=$this->Vehicle->find('list',array('fields'=>array('owner'),'conditions'=>array('NOT'=>array('id'=>$activeIds),'user_id'=>$this->Auth->user('id'),'property_id'=>$this->Session->read('PropertyId'))));
		
        $this->loadModel('State');
        $state=new State();
        $states=$state->find('list');
        $this->set(compact('states','customerOtherVehicles','customerPassId','userGuestPass'));
       
	}
	public function add_guest_vehicle_ajax($customerPassId,$userGuestPass=null){ 
		$this->layout=$this->autoRender=false;
		if($this->request->is('POST')){
			$response=array(
						'success'=>true,
						'message'=>'',
					);	
			$this->loadModel('CustomerPass');
			$vehicleID=null;
			if(!empty($this->request->data['Vehicle']['vehicle_id'])){
				$this->Vehicle->updateValidations();
			}
			$this->Vehicle->set($this->request->data);
			if($this->Vehicle->validates()){
				if(empty($this->request->data['Vehicle']['vehicle_id'])){
							$this->CustomerPass->recursive=-1;
							$vehicle['Vehicle']=array('owner'=>$this->request->data['Vehicle']['owner'],
										  'make'=>$this->request->data['Vehicle']['make'],
										  'model'=>$this->request->data['Vehicle']['model'],
										  'color'=>$this->request->data['Vehicle']['color'],
										  'license_plate_number'=>$this->request->data['Vehicle']['license_plate_number'],
										  'license_plate_state'=>$this->request->data['Vehicle']['license_plate_state'],
										  'last_4_digital_of_vin'=>$this->request->data['Vehicle']['last_4_digital_of_vin'],
										  'user_id'=>$this->Auth->user('id'),
										  'property_id'=>$this->Session->read('PropertyId')
											);
							if($this->Vehicle->save($vehicle,false)){
									CakeLog::write('newGuestVehicleAdded', ''.AuthComponent::user('username').' : New Guest Vehicle Added with vehicle ID:  <a href="/admin/Vehicles/edit/'.$this->Vehicle->getLastInsertID().'">'.$this->Vehicle->getLastInsertID().' </a> by User: '.AuthComponent::user('first_name').' '.AuthComponent::user('last_name').' In Property: '.CakeSession::read('PropertyName').'');
									$vehicleID=$this->Vehicle->getLastInsertId();
							}else{
								$this->Session->setFlash('Vehicle not saved','error');
							}
				}else{
					$vehicleID=$this->request->data['Vehicle']['vehicle_id'];
					$this->CustomerPass->updateAll(array('CustomerPass.vehicle_id'=>NULL),array('CustomerPass.vehicle_id'=>$vehicleID));
					$this->Vehicle->updateAll(array('Vehicle.customer_pass_id'=>NULL),array('Vehicle.customer_pass_id'=>$customerPassId));
				}
				if($vehicleID){
					$this->Vehicle->id=$vehicleID;
					$this->Vehicle->saveField('customer_pass_id',$customerPassId);
					$this->CustomerPass->id=$customerPassId; 
					$this->CustomerPass->saveField('vehicle_id',$vehicleID);
					CakeLog::write('guestVehicleAssigned', ''.AuthComponent::user('username').' : Guest Vehicle Assigned vehicle ID:  <a href="/admin/Vehicles/edit/'.$vehicleID.'">'.$vehicleID.' </a> by User: '.AuthComponent::user('first_name').' '.AuthComponent::user('last_name').' In Property: '.CakeSession::read('PropertyName').' and assigned to Customer Pass ID: <a href="/admin/CustomerPasses/edit/'.$customerPassId.'">'.$customerPassId.'</a>');
					$this->loadModel('UserGuestPass');
					$userGuestPassArr=array();
					$this->UserGuestPass->recursive=-1;
					if($userGuestPass){
						$userGuestPassArr=$this->UserGuestPass->find('first',array('conditions'=>array('UserGuestPass.id'=>$userGuestPass)));
					}else{
						$userGuestPassArr=$this->UserGuestPass->find('first',array('conditions'=>array('UserGuestPass.customer_pass_id'=>$customerPassId),'order'=>array('UserGuestPass.id'=>'DESC')));
					}
					if($userGuestPassArr){
						$vehicleInfo=$this->Vehicle->field('vehicle_info',array('id'=>$vehicleID));
						$userGuestPassArr['UserGuestPass']['vehicle_id']=$vehicleID;
						$userGuestPassArr['UserGuestPass']['vehicle_details']=$vehicleInfo;
						$this->UserGuestPass->save($userGuestPassArr,false); 
					}
					$response=array(
						'success'=>true,
						'message'=>'Pass activated successfully.',
					);
	
				}else{
					$response=array(
						'success'=>false,
						'message'=>'Vehicle Not Saved',
					);	
					
				}
			}else{
				$errors=$this->Vehicle->invalidFields();
				$finalError=array();
				foreach($errors as $field=>$error){
					$finalError[$field]=implode(", ",array_unique($error));
				}
				$finalError=array_unique(array_values($finalError));
				$finalError=implode(",<br> ",$finalError);
				$response['success'] = false;
				$response['message'] = $finalError;
			}
		}
		echo json_encode($response);
	}
}
