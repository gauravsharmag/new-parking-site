<?php
App::uses('AppController', 'Controller');
/**
 * Packages Controller
 *
 * @property Package $Package
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class PackagesController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator','Session');
    public $helpers = array('Html', 'Form','Session');
	 public function beforeFilter()
    { 
        parent::beforeFilter();
        $this->Auth->allow(); 
        $this->Security->validatePost = false;
    }
	var $packagesFixed=null;
	var $packagesOthers=null;

    /**
     * index method
     *
     * @return void
     */
    public function index() {
       		
        $this->layout='customer';
		$dt = new DateTime();
        $currentDateTime= $dt->format('Y-m-d H:i:s');
		$this->Package->recursive=-1;
        $packagesFixed=$this->Package->find('all',array('conditions'=>array('property_id'=>$this->Session->read('PropertyId'),'AND'=>array('is_fixed_duration'=>'1','expiration_date >'=>$currentDateTime))));
        $this->Package->recursive=-1;
        $packagesOthers=$this->Package->find('all',array('conditions'=>array('property_id'=>$this->Session->read('PropertyId'),'AND'=>array('is_fixed_duration'=>'0'))));
		$countFixed=count($packagesFixed);
		$countOthers=count($packagesOthers);
		$allPermits=array_merge($packagesFixed,$packagesOthers);
		$count=$countFixed+$countOthers;
        $this->set(array('packagesFixed'=>$allPermits,'countFixed'=> $count
        ));
    }
	 
	/**
	*	Buy Permit
	*/
	public function selectPermitToBuy()
	{
		$this->layout='customer';
		$dt = new DateTime();
        $currentDateTime= $dt->format('Y-m-d H:i:s');
		$this->layout='customer';
		$this->Package->recursive=-1;
        $packagesFixed=$this->Package->find('all',array('conditions'=>array('property_id'=>$this->Session->read('PropertyId'),'AND'=>array('is_fixed_duration'=>'1','expiration_date >'=>$currentDateTime,'is_required'=>'0','pass_id'=>null))));
		/*$packagesBoughtAndFixedNoPass=$this->Package->find('all',array('conditions'=>array('id in ( select package_id from customer_passes where user_id = '.$this->Auth->user('id').' and property_id ='.$this->Session->read('PropertyId').' and  pass_valid_upto<\''.$currentDateTime.'\') and is_fixed_duration = 1 and expiration_date >\''. $currentDateTime.'\' and pass_id = null and  property_id = '.$this->Session->read('PropertyId').'')));
		
		$packagesBoughtNoPass=$this->Package->find('all',array('conditions'=>array('id in ( select package_id from customer_passes where user_id = '.$this->Auth->user('id').' and property_id ='.$this->Session->read('PropertyId').' and  pass_valid_upto<\''.$currentDateTime.'\') and is_fixed_duration = 0 and pass_id = null and  property_id = '.$this->Session->read('PropertyId').'')));
        
		$packagesNotBoughtAndFixedNoPass=$this->Package->find('all',array('conditions'=>array('id not in ( select package_id from customer_passes where user_id = '.$this->Auth->user('id').' and property_id ='.$this->Session->read('PropertyId').') and is_fixed_duration = 1 and pass_id = null and expiration_date >\''. $currentDateTime.'\' and  property_id = '.$this->Session->read('PropertyId').'')));
		
		$packagesNotBoughtNoPass=$this->Package->find('all',array('conditions'=>array('id not in ( select package_id from customer_passes where user_id = '.$this->Auth->user('id').' and property_id ='.$this->Session->read('PropertyId').') and is_fixed_duration = 0 and pass_id = null and  property_id = '.$this->Session->read('PropertyId').'')));
		
		debug($packagesBoughtAndFixedNoPass);
		debug($packagesBoughtNoPass);
		debug($packagesNotBoughtAndFixedNoPass);
		debug($packagesNotBoughtNoPass);
		die();*/
		
		
		
		
		
		
		$this->Package->recursive=-1;
        $packagesOthers=$this->Package->find('all',array('conditions'=>array('property_id'=>$this->Session->read('PropertyId'),'AND'=>array('is_fixed_duration'=>'0','is_required'=>'0','pass_id'=>null))));
		$countFixed=count($packagesFixed);
		$countOthers=count($packagesOthers);
		$this->set(array('packagesFixed'=>$packagesFixed,'countFixed'=> $countFixed,
                         'packagesOthers'=>$packagesOthers,'countOthers'=> $countOthers
					));
	}
    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        if (!$this->Package->exists($id)) {
            throw new NotFoundException(__('Invalid package'));
        }
        $options = array('conditions' => array('Package.' . $this->Package->primaryKey => $id));
        $this->set('package', $this->Package->find('first', $options));
    }

 
    /**
     * admin_index method
     *
     * @return void
     */
    public function admin_index($propertyId=null,$propertyName=null) {

        //debug($propertyId);
        //die();
        //  $this->Package->recursive = 0;
        // $packages=$this->Package->find('all',array('conditions'=>array('property_id'=>$propertyId)));
        // $this->set('packages', $this->Paginator->paginate());

        $this->Package->recursive = -1;
        /* $this->paginate['Package'] = array(
            // 'limit' => 9,
             'order' => array('Package.id' => 'asc'),
             'conditions' => array('property_id'=>$propertyId)
         );*/
        $condition = array('property_id'=>$propertyId);
        $packages = $this->paginate('Package',$condition);
        // $p
        $this->set(compact('packages','propertyName','propertyId') );
    }

    /**
     * admin_view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_view($id = null) {
        if (!$this->Package->exists($id)) {
            throw new NotFoundException(__('Invalid package'));
        }
        $options = array('conditions' => array('Package.' . $this->Package->primaryKey => $id));
        $this->set('package', $this->Package->find('first', $options));
    }

    /**
     * admin_add method
     *
     * @return void
     */
    public function admin_add($id=null,$propertyName=null) {
         if ($this->request->is('post')) {
            $passId=$this->request->data['Package']['pass_id'];
            if($this->request->data['Package']['is_fixed_duration' ]==0)
            {
                unset($this->request->data['Package']['start_date']);
                unset($this->request->data['Package']['expiration_date']);
            }
            else
            {
                $this->request->data['Package']['duration']='';
                $this->request->data['Package']['duration_type']='';
            }
            if($this->request->data['Package']['is_guest' ]==0)
            {
                $this->request->data['Package']['free_guest_credits']='';
            }
            $this->Package->create();
            if ($this->Package->save($this->request->data)) {
				
                if(!empty( $passId)){
                    $lastPackageId=$this->Package->getLastInsertID();
                    $this->loadModel('Pass');
                    $passObj=new Pass();
                    $arr['Pass']['id']=$passId;
                    $arr['Pass']['package_id']= $lastPackageId;
                    $passObj->save($arr,false);
                }
                $packageName=$this->Package->givePackageName($this->Package->getLastInsertID());
				CakeLog::write('newPackageAddedAdmin', ''.AuthComponent::user('username').' : New Package Added in  Property ID:  <a href="/admin/properties/view/'.$id.'">'.$id.' </a> PropertyName : '.$propertyName.' by User: '.AuthComponent::user('first_name').' '.AuthComponent::user('last_name').' New Package ID: '.$this->Package->getLastInsertID().' and Package Name : '.$packageName.'');

                $this->Session->setFlash('The package has been saved.','success');
                return $this->redirect(array('action' => 'add',$this->request->data['Package']['property_id'],$propertyName));
            } else {
                  $this->Session->setFlash('The package could not be saved.','error');
            }
        }
        $passes=$this->Package->Pass->find('list',array('fields'=>array('name'),'conditions'=>array('property_id'=>$id,'and'=>array('package_id'=>null))));
        $this->Package->recursive = -1;
        $condition = array('property_id'=>$id);
        $packages = $this->paginate('Package',$condition);
        $propertyId=$id;
        $this->set(compact('id','propertyName','passes','packages','propertyId'));
    }

    /**
     * admin_edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_edit($id = null,$propertyName=null,$propertyId=null) {
        if (!$this->Package->exists($id)) {
            throw new NotFoundException(__('Invalid package'));
        }
        if ($this->request->is(array('post', 'put'))) {
            $passId=$this->request->data['Package']['pass_id'];
            if($this->request->data['Package']['is_fixed_duration' ]==0)
            {
               $this->request->data['Package']['start_date']=null;
                $this->request->data['Package']['expiration_date']=null;
            }
            else
            {
                $this->request->data['Package']['duration']=null;
                $this->request->data['Package']['duration_type']=null;
            }

            if($this->request->data['Package']['is_guest' ]==0)
            {
                $this->request->data['Package']['free_guest_credits']='';
            }
            $this->loadModel('Pass');
            $passObj=new Pass();
            $this->Package->set($this->request->data);
            if($this->Package->validates()){
              
                if($this->request->data['Package']['remove_assigned_pass']==0){
                    if(!empty($passId)){
                        $passObj->query('UPDATE passes SET package_id = null where package_id='.$this->request->data['Package']['id'].'');
                        $arr['Pass']['id']=$this->request->data['Package']['pass_id'];
                        $arr['Pass']['package_id']= $id;
                        $passObj->save($arr,false);
                    }
                }else{
                    $passObj->query('UPDATE passes SET package_id = null where package_id='.$this->request->data['Package']['id'].'');
                    $this->Package->query('UPDATE packages SET pass_id = null where id='.$this->request->data['Package']['id'].'');
                    $this->request->data['Package']['pass_id']=null;
                }
                unset($this->request->data['Package']['remove_assigned_pass']);
                if ($this->Package->save($this->request->data)) {
					$packageName=$this->Package->givePackageName($id);
					CakeLog::write('packageEditedAdmin', ''.AuthComponent::user('username').' : Package Details Edited in  Property ID:  <a href="/admin/properties/view/'.$propertyId.'">'.$propertyId.' </a> PropertyName : '.$propertyName.' by User: '.AuthComponent::user('first_name').' '.AuthComponent::user('last_name').'  Package ID: '.$id.' and Package Name : '.$packageName.'');
                    $this->Session->setFlash('The package has been saved successfully.', 'success');
                    return $this->redirect(array('action' => 'add',$propertyId,$propertyName));
                }
                else {
                    $this->Session->setFlash('The package could not be saved','error');
                }
            }
            else {
                $this->Session->setFlash('Validation Failure','error');
            }

        } else {
            $options = array('conditions' => array('Package.' . $this->Package->primaryKey => $id));
            $this->Package->recursive=-1;
            $this->request->data = $this->Package->find('first', $options);
            $this->request->data['Package']['start_date'] = date("m/d/Y", strtotime($this->request->data['Package']['start_date']));
            $this->request->data['Package']['expiration_date']= date("m/d/Y",strtotime($this->request->data['Package']['expiration_date']));

        }
        $this->Package->recursive=-1;
        $properties = $this->Package->Property->find('list');
        $passes=$this->Package->Pass->find('list',array('fields'=>array('name'),'conditions'=>array('property_id'=>$propertyId,'and'=>array('package_id'=>null))));
        $this->set(compact('properties','propertyName','propertyId','passes'));
    }

    /**
     * admin_delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_delete($id = null,$propertyName=null,$propertyId=null) {
		$this->Package->id = $id;
        if (!$this->Package->exists()) {
            throw new NotFoundException(__('Invalid package'));
        }
        
         $refer=explode('/',$this->referer());
        // debug($refer);
         //debug($this->referer());
         //die;
        $this->loadModel('Pass');
        $passId=$this->Pass->field('id',array('package_id'=>$id));
        // debug($passId);die;
        $this->request->allowMethod('post', 'delete');
        $packageName=$this->Package->givePackageName($id);
        if ($this->Package->delete()) {
			if($passId!=false){
				$array['Pass']=array('id'=>$passId,
									 'package_id'=>null
				);
				$this->Pass->save($array,false);
			}
			
			CakeLog::write('packageDeletedAdmin', ''.AuthComponent::user('username').' : Package Deleted in  Property ID:  <a href="/admin/properties/view/'.$propertyId.'">'.$propertyId.' </a> PropertyName : '.$propertyName.' by User: '.AuthComponent::user('first_name').' '.AuthComponent::user('last_name').'  Package ID: '.$id.' and Package Name : '.$packageName.'');

            $this->Session->setFlash('The package has been deleted.','success');
            if($refer[5]=="view"){
				return $this->redirect(array('controller'=>'Properties','action' => 'view',$propertyId));
			}else{
				return $this->redirect(array('action' => 'add',$propertyId,$propertyName));
			}
            
        } else {
            $this->Session->setFlash('The package could not be deleted. Please, try again.','error');
            return $this->redirect(array('action' => 'index',$propertyId,$propertyName));
        }
    }
    public function getCurrentUserPassStatus($id=null,$currentUser=null)
    {
        $this->layout='customer';
        $propertyId=$id;
        $this->Package->Property->recursive=0;
        $totalPassesAllowed=$this->Package->Property->find('first',array('fields'=>array('total_passes_per_user'),'conditions'=>array('Property.id'=> $propertyId)));
        $totalPassesAllowed=$totalPassesAllowed['Property']['total_passes_per_user'];

        $this->loadModel('CustomerPass');
        $customerPass= new CustomerPass();

        $currentPassStatus=$customerPass->getCurrentPassStatus($currentUser,$propertyId);
        $currentUserPassStatusAndDetails=$customerPass->getCurrentPassStatusAndDetails($currentUser,$propertyId);

        //$remainingPasses=(int)$totalPassesAllowed-($currentPassStatus['passesValidPackagesValid']+$currentPassStatus['passesValidPackageExpired']+$currentPassStatus['expiredPasses']);

        //$passesEligibleForPackage=$remainingPasses+$currentPassStatus['passesValidPackageExpired'];

        return array('totalPassesAllowed'=>$totalPassesAllowed,'currentPassStatus'=>$currentPassStatus,'currentUserPassStatusAndDetails'=>$currentUserPassStatusAndDetails);
    }
	public function getCorrespondingPackage($id=null)
	{
		
		if (!$this->Package->hasAny(array('pass_id'=>$id))) {
            throw new NotFoundException(__('Invalid package'));
        }
		$result=$this->Package->find('first',array('fields'=>array('pass_id',''),'conditions'=>array('id'=>$id)));
		debug($result);
		die();
		$this->redirect(array('controller'=>'Transactions','action'=>'check_out',));
	}
	public function getPackageNamePassBought($id=null){
		$this->loadModel('CustomerPass');
		$cstPass=new CustomerPass();
		$cstPackageId=$cstPass->field('package_id',array('id'=>$id));
		if($cstPackageId==null){
			$cstPassId=$cstPass->field('pass_id',array('id'=>$id));
			$i=$this->Package->field('name',array('pass_id'=>$cstPassId));
			if($i==null){
				return "No Package Assigned";
			}
			else{
				return $i;
			}
		}else{
			return $this->Package->field('name',array('id'=>$cstPackageId));
		}
	}
	public function remainingPassPackageName($id=null){
		$rslt=$this->Package->field('name',array('pass_id'=>$id));
		if($rslt==null){
			return "No Package Assigned";
		}
		else{
			return $rslt;
		}
	}
	public function admin_remainingPassPackageName($id=null){
		$rslt=$this->Package->field('name',array('pass_id'=>$id));
		if($rslt==null){
			return "No Package Assigned";
		}
		else{
			return $rslt;
		}
	}
	public function getPackageName($id=null){
		return $this->Package->field('name',array('id'=>$id));
	}
	public function admin_getPackageName($id=null){
		return $this->Package->field('name',array('id'=>$id));
	}
	public function admin_getPackageNamePassBought($id=null){
		$this->loadModel('CustomerPass');
		$cstPass=new CustomerPass();
		$cstPackageId=$cstPass->field('package_id',array('id'=>$id));
		if($cstPackageId==null){
			$cstPassId=$cstPass->field('pass_id',array('id'=>$id));
			$i=$this->Package->field('name',array('pass_id'=>$cstPassId));
			if($i==null){
				return "No Package Assigned";
			}
			else{
				return $i;
			}
		}else{
			return $this->Package->field('name',array('id'=>$cstPackageId));
		}
	}
	
	
}
