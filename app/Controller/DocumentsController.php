<?php
App::uses('AppController', 'Controller');
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');

class DocumentsController extends AppController {

	public $components = array(
							   'NewDatatable'
							);
	public function beforeFilter(){
        parent::beforeFilter();
        $this->Security->validatePost = false;
    }
	public function admin_add(){
		$tempArr=explode('.',$this->request->data['Document']['file_name']['name']);
		$ext=$tempArr[count($tempArr)-1];
		$fileName=uniqid().'.'.$ext;
        if(move_uploaded_file($this->request->data['Document']['file_name']['tmp_name'], WWW_ROOT.'files/docs/'.$fileName)){
			$this->request->data['Document']['file_name']=$fileName;
			if($this->Document->save($this->request->data,false)){
				$this->Session->setFlash('The document has been uploaded.','success');
			}else{
				$this->Session->setFlash('The document cannot be uploaded. Please try again','error');
			}
		}else{
			$this->Session->setFlash('The document cannot be uploaded. Please try again','error');
		} 
        return $this->redirect($this->referer());  
	}
	public function admin_delete($id){
		$this->Document->id = $id;
        if (!$this->Document->exists()) {
            throw new NotFoundException(__('Invalid Document'));
        }
        $fileName=$this->Document->field('file_name',array('id'=>$id));
        if ($this->Document->delete()) {
			$file= new File(WWW_ROOT."files/docs/".$fileName,false, 0777);
			if($file->exists()){
				$file->delete();
			}
			$this->Session->setFlash('The document has been deleted.','success');
		}else{
			$this->Session->setFlash('The document cannot be deleted. Please try again','error');
		}
		 return $this->redirect($this->referer());  
	}
	public function admin_get_documents($propertyId){
		$aColumns = array('name', 'file_name','id');
        $sIndexColumn = " documents.id ";
        $sTable = " documents ";
        $sJoinTable='';
        $sConditions=' property_id = '.$propertyId ;
       
        $returnArr=$this->NewDatatable->getData(array('columns'=>$aColumns,'index_column'=>$sIndexColumn,'table'=>$sTable,'join'=>$sJoinTable,'conditions'=>$sConditions));
        echo json_encode($returnArr);
        die; 
	}
}
