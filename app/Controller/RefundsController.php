<?php
App::uses('AppController', 'Controller');
/**
 * Refunds Controller
 *
 */
class RefundsController extends AppController {

	public $components = array('NewDatatable');
	public function beforeFilter(){ 
        parent::beforeFilter();
    }
	public function admin_index(){
		$this->loadModel('Property');
		$properties=$this->Property->find('list');
		$this->set(compact('properties'));
	}
	public function admin_getData($status,$property=false){
		$conditions=' Refund.refunded= '.$status;
		if($status==3){
			$conditions='';
		}
		if($property){
			if($conditions){
				$conditions.=' AND ';
			}
			$conditions.=' Refund.property_id = '.$property;
		}
		
		$aColumns = array('User.first_name',
						  'properties.name',
						  'Refund.pass',
						  'Refund.permit',
						  'Refund.pass_cost',
						  'Refund.pass_deposit',
						  'Refund.package_cost',
						  'Refund.total_cost',
						  'Refund.created',
						  'Refund.refunded',
						  'Refund.id',
		
						  'User.last_name', 
						  'User.username', 
						  'User.email', 
						  'User.phone',
						  'User.address_line_1',
						  'User.address_line_2',
						  'User.city',
						  'User.state',
						  'User.zip',
						  'Refund.user_id',
						  );
        $sIndexColumn = " User.id ";
        $sTable = " refunds Refund ";
        $sJoinTable=' INNER JOIN users User on User.id=Refund.user_id
					  INNER JOIN properties on properties.id=Refund.property_id
         ';
        $sConditions=$conditions;
       
        $returnArr=$this->NewDatatable->getData(array('columns'=>$aColumns,'index_column'=>$sIndexColumn,'table'=>$sTable,'join'=>$sJoinTable,'conditions'=>$sConditions));
        echo json_encode($returnArr);
        die;
	}
	public function admin_refund($id,$operation){
		$this->Refund->id=$id;
		$this->Refund->saveField('refunded',$operation);
		$this->Session->setFlash('Request processed successfully.','success');
		return $this->redirect(['action'=>'index']);
	}
}
