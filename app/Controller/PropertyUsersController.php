<?php
App::uses('AppController', 'Controller');
/**
 * PropertyUsers Controller
 *
 * @property PropertyUser $PropertyUser
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class PropertyUsersController extends AppController {
 public function beforeFilter()
    { 
        parent::beforeFilter();
		//$this->Auth->allow(); 
    }
 public function admin_user_details(){
		$this->autoRender=$this->layout=false;
		$output=array();
		$output = $this->PropertyUser->getTableDataCorrected();
		//$output = array_map("unserialize", array_unique(array_map("serialize", $output))); 
		//var_dump($output);
        echo json_encode($output);
	 }
	 public function admin_pa_reset_property(){
		 $this->autoRender=false;
		if ($this->request->is('post')) {
			$array=explode('/',$this->request->data['PropertyUser']['page_url']);
			$propertyId=$this->PropertyUser->field('property_id',array('id'=>$this->request->data['PropertyUser']['property_user_id']));
			$this->loadModel('Property');
			$propertyName=$this->Property->field('name',array('id'=>$propertyId));
			
			$this->Session->write('CurrentProperty',$this->request->data['PropertyUser']['property_user_id']);
			$this->Session->write('PropertyName',$propertyName);
			$this->Session->write('PropertyId',$propertyId);
			$this->redirect(array('controller'=>$array[2],'action'=>$array[3]));
		}
		
		
	 }
	 public function admin_change_manager(){
		  $this->autoRender=false;
		if($this->request->is('POST')){
			 if(empty($this->request->data['PropertyUser']['user_id'])){
				$this->Session->setFlash('Please Select Property Manager','error');
				$this->redirect($this->referer());
			}else{
				$this->loadModel('User');
				if (!$this->User->exists($this->request->data['PropertyUser']['user_id'])) {
					$this->Session->setFlash('Please Select Property Manager','error');
					$this->redirect($this->referer());
				}else{
						$data= $this->PropertyUser->find('first',array('conditions'=>array('PropertyUser.property_id'=>$this->request->data['PropertyUser']['property_id'],'PropertyUser.role_id'=>3)));
						if($data){
							$this->PropertyUser->delete($data['PropertyUser']['id']);
						} 
						$arr['PropertyUser']=array(
												   'user_id'=>$this->request->data['PropertyUser']['user_id'],
												   'property_id'=>$this->request->data['PropertyUser']['property_id'],
												   'role_id'=>3
						);
						$this->PropertyUser->create();
						if($this->PropertyUser->save($arr,false)){
							$this->Session->setFlash('Property Manager Changed Successfully','success');
							$this->redirect($this->referer());
						}else{
							$this->Session->setFlash('Property Manager Cannot Be Changed, Try Again','error');
							$this->redirect($this->referer());
						}
					
				}
			}
		}
	}
	public function admin_change_property(){
		 $this->autoRender=false;
		 if($this->request->is('POST')){
			 if(empty($this->request->data['PropertyUser']['new_user_id'])){
				$this->Session->setFlash('Please Select Property Manager','error');
				$this->redirect($this->referer());
			}else{
				$this->loadModel('User');
				if (!$this->User->exists($this->request->data['PropertyUser']['new_user_id'])) {
					$this->Session->setFlash('Please Select Property Manager','error');
					$this->redirect($this->referer());
				}else{
					$this->PropertyUser->recursive=-1;
					$data= $this->PropertyUser->find('first',array('conditions'=>array('PropertyUser.property_id'=>$this->request->data['PropertyUser']['property_id'],'PropertyUser.role_id'=>3)));
					if($data){
						$this->PropertyUser->delete($data['PropertyUser']['id']);
					} 
					$data1= $this->PropertyUser->find('first',array('conditions'=>array('PropertyUser.property_id'=>$this->request->data['PropertyUser']['oldproperty'],'PropertyUser.role_id'=>3)));
					if($data1){
						$this->PropertyUser->delete($data1['PropertyUser']['id']);
					} 
					$arr['PropertyUser']=array(
											   'user_id'=>$this->request->data['PropertyUser']['new_user_id'],
											   'property_id'=>$this->request->data['PropertyUser']['property_id'],
											   'role_id'=>3
					);
					$this->PropertyUser->create();
					if($this->PropertyUser->save($arr,false)){
						$this->Session->setFlash('Property Manager Changed Successfully','success');
						$this->redirect($this->referer());
					}else{
						$this->Session->setFlash('Property Manager Cannot Be Changed, Try Again','error');
						$this->redirect($this->referer());
					}
				}
			}
		}
		 
	}
	public function admin_delete(){ 
		$this->PropertyUser->recursive=-1;
		$data= $this->PropertyUser->find('first',array('conditions'=>array('PropertyUser.user_id'=>$this->request->params['pass'][0],'PropertyUser.role_id'=>3)));
		if($data){
			if($this->PropertyUser->delete($data['PropertyUser']['id'])){
				$this->Session->setFlash('Property Manager Removed Successfully','success');
				$this->redirect($this->referer());
			}else{
				$this->Session->setFlash('Property Manager Cannot Be removed, Try Again','error');
				$this->redirect($this->referer());
			}
		}else{
			$this->Session->setFlash('Property Manager Not Found, Try Again','error');
			$this->redirect($this->referer());
		}
	}
	/*******************************************************************
	 * create Csv Property Wise
	 */
	 public function admin_create_csv($propertyId){
		$allUserIds=$this->PropertyUser->find('list',array(
														   'conditions'=>array(
																				'PropertyUser.property_id'=>$propertyId,
																				'PropertyUser.role_id'=>4
														   ),
														   'fields'=>array('id','user_id')
		));
		header("Content-type: application/csv");
		header("Content-Disposition: attachment; filename=Property_CSV_".$propertyId.".csv");
		header("Pragma: no-cache");
		header("Expires: 0");
		header("Content-Type: application/octet-stream"); 
		$fp = fopen('php://output', 'w');
		$row=array(
					'Full Name',
					'User Name',
					'Email',
					'Phone',
					'Address',
					'Vehicle Name',
					'Make',
					'Model',
					'Color',
					'Plate Number',
					'State',
					'VIN',
					'Parking Location',
					'Pass Name',
					'Status',
					'Pass Expiration Date',
					'RFID Tag'
		);
		fputcsv($fp, $row); 
		$this->loadModel('User');
		$this->loadModel('CustomerPass');
		foreach($allUserIds as $key=>$userID){
			$user=$this->User->find('first',array('conditions'=>array('User.id'=>$userID)));
			if($user){
				$customerPasses=$this->CustomerPass->find('all',array('conditions'=>array('CustomerPass.user_id'=>$userID)));
				$dt = new DateTime();
				$currentDateTime = $dt->format('Y-m-d H:i:s');
				foreach($customerPasses as $pass){
					$status_msg='';
					if ($pass['CustomerPass']['pass_valid_upto'] < $currentDateTime) {
						$status = "PassExpired";
						$status_msg = "Pass Expired";
						if (is_null($pass['CustomerPass']['package_id'])) {
							$status_msg = $status_msg . ", Package Was Not Bought";
						}
					} else {
						if (is_null($pass['CustomerPass']['package_id'])) {
							$status = "PassValid";
							$status_msg = "Pass Is Valid But Package Not Bought";
						} else {
							if ($pass['CustomerPass']['membership_vaild_upto'] < $currentDateTime) {
								$status = "PassExpired";
								$status_msg = "Pass Is Valid But Package Expired";
							} else {
								$status = "PassActive";
								$status_msg = "Pass Active";
							}
						}
					}
					
					$data=array(
								$pass['User']['first_name'].' '.$pass['User']['last_name'],
								$pass['User']['username'],
								$pass['User']['email'],
								$pass['User']['phone'],
								$pass['User']['address_line_1'].' '.$pass['User']['address_line_2'].' '.$pass['User']['city'].' '.$pass['User']['state'].' '.$pass['User']['zip'],
								$pass['Vehicle']['owner']?$pass['Vehicle']['owner']:'',
								$pass['Vehicle']['make']?$pass['Vehicle']['make']:'',
								$pass['Vehicle']['model']?$pass['Vehicle']['model']:'',
								$pass['Vehicle']['color']?$pass['Vehicle']['color']:'',
								$pass['Vehicle']['license_plate_number']?$pass['Vehicle']['license_plate_number']:'',
								$pass['Vehicle']['license_plate_state']?$pass['Vehicle']['license_plate_state']:'',
								$pass['Vehicle']['last_4_digital_of_vin']?$pass['Vehicle']['last_4_digital_of_vin']:'',
								$pass['Package']['name']?$pass['Package']['name']:'',
								$pass['Pass']['name']?$pass['Pass']['name']:'',
								$status_msg,
								$pass['CustomerPass']['pass_valid_upto']?date('m-d-Y H:i:s',strtotime($pass['CustomerPass']['pass_valid_upto'])):'',
								$pass['CustomerPass']['RFID_tag_number']?$pass['CustomerPass']['RFID_tag_number']:''
					);
					fputcsv($fp, $data); 
				}
			}
		}
		stream_get_contents($fp);
		die;
		
	 }
}
