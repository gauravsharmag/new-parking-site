<?php
App::uses('AppController', 'Controller');

/**
 * CustomerPasses Controller
 *
 * @property CustomerPass $CustomerPass
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 * FINAL TESTING THE FLOW
 */
class CustomerPassesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator',
							   'NewDatatable', 
							   'Session',
							   'PayFlow',
							   'Paypal'
							);
	public function beforeFilter(){
        parent::beforeFilter();
        $this->Security->validatePost = false;
    }

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->CustomerPass->recursive = 0;
		$this->set('customerPasses', $this->Paginator->paginate());
	}
	

/*********************************************************************
 * view method
 */
	public function view() {
		
        $this->layout='customer';
        $dt = new DateTime();
        $currentDateTime= $dt->format('Y-m-d H:i:s');
        $this->CustomerPass->recursive=-1;
        $customerActivePass=$this->CustomerPass->find('all',array('conditions'=>array('user_id'=>$this->Auth->user('id'),'AND'=>array('membership_vaild_upto >'=>$currentDateTime,'pass_valid_upto >'=>$currentDateTime))));
        $this->CustomerPass->recursive=-1;
        $customerValidPass=$this->CustomerPass->find('all',array('conditions'=>array('user_id'=>$this->Auth->user('id'),'AND'=>array('OR'=>array('membership_vaild_upto'=>null,'membership_vaild_upto <'=>$currentDateTime),'pass_valid_upto >'=>$currentDateTime))));
        $this->CustomerPass->recursive=-1;
        $customerRenewableExpiredPass=$this->CustomerPass->find('all',array('conditions'=>array('pass_id in (select id from passes where property_id = '.$this->Session->read('PropertyId').' and is_fixed_duration = 0) and user_id ='.$this->Auth->user('id').' and pass_valid_upto <\''.$currentDateTime.'\'')));
        $this->CustomerPass->recursive=-1;
        $customerNonRenewableExpiredPass=$this->CustomerPass->find('all',array('conditions'=>array('pass_id in (select id from passes where property_id = '.$this->Session->read('PropertyId').' and is_fixed_duration = 1) and user_id ='.$this->Auth->user('id').' and pass_valid_upto <\''.$currentDateTime.'\'')));
		$this->loadModel('Pass');
		$this->loadModel('Property');
		$couponSelection=$this->Property->field('coupon_selection',array('id'=>$this->Session->read('PropertyId')));
		$remainingPasses=array();
		if($couponSelection){
			$remainingPasses=array();
		}else{
			$remainingPasses=$this->Pass->getRemainingPass($this->Session->read('PropertyId'),$this->Auth->user('id'));
		}
        $noCurrentPass=$this->CustomerPass->hasAny(array('user_id'=>$this->Auth->user('id')));
        $this->CustomerPass->User->recursive=-1;
        $currentUser=$this->CustomerPass->User->find('first',array('conditions'=>array('User.id'=>$this->Session->read('Auth.User.id'))));
        $this->Session->write('Allowed',true);
		$this->set(array('customerActivePass'=>$customerActivePass,
            'customerValidPass'=>$customerValidPass,
            'customerRenewableExpiredPass'=>$customerRenewableExpiredPass,
            'customerNonRenewableExpiredPass'=>$customerNonRenewableExpiredPass,
			'remainingPasses'=>$remainingPasses,
			'noCurrentPass'=>$noCurrentPass,
			'currentUser'=>$currentUser
			));
	}


/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->CustomerPass->create();
			if ($this->CustomerPass->save($this->request->data)) {
				$this->Session->setFlash(__('The customer pass has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The customer pass could not be saved. Please, try again.'));
			}
		}
		$users = $this->CustomerPass->User->find('list');
		$vehicles = $this->CustomerPass->Vehicle->find('list');
		$properties = $this->CustomerPass->Property->find('list');
		$transactions = $this->CustomerPass->Transaction->find('list');
		$packages = $this->CustomerPass->Package->find('list');
		$this->set(compact('users', 'vehicles', 'properties', 'transactions', 'packages'));
	}


/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->CustomerPass->id = $id;
		if (!$this->CustomerPass->exists()) {
			throw new NotFoundException(__('Invalid customer pass'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->CustomerPass->delete()) {
			$this->Session->setFlash(__('The customer pass has been deleted.'));
		} else {
			$this->Session->setFlash(__('The customer pass could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

/***********************************************************************
 * admin_index method
 */
	public function admin_index() {
		$this->CustomerPass->recursive = 0;
		$this->set('customerPasses', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->CustomerPass->exists($id)) {
			throw new NotFoundException(__('Invalid customer pass'));
		}
		$options = array('conditions' => array('CustomerPass.' . $this->CustomerPass->primaryKey => $id));
		$this->set('customerPass', $this->CustomerPass->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->CustomerPass->create();
			if ($this->CustomerPass->save($this->request->data)) {
				$this->Session->setFlash(__('The customer pass has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The customer pass could not be saved. Please, try again.'));
			}
		}
		$users = $this->CustomerPass->User->find('list');
		$vehicles = $this->CustomerPass->Vehicle->find('list');
		$properties = $this->CustomerPass->Property->find('list');
		$transactions = $this->CustomerPass->Transaction->find('list');
		$packages = $this->CustomerPass->Package->find('list');
		$this->set(compact('users', 'vehicles', 'properties', 'transactions', 'packages'));
	}
/**********************
 * Edited On 8th Dec,2015
 * Edited On 18th March,2015
 * 
 */
  public function admin_edit($id = null) {

        if (!$this->CustomerPass->exists($id)) {
            throw new NotFoundException(__('Invalid customer pass'));
        }
        $vehicle = 0;
        $owner = $make = $model = $color = $plateNo = $plateState = $vin = $propertyId = $userId = '';
        if ($this->request->is(array('post', 'put'))) {

            $propertyId = $this->CustomerPass->field('property_id', array('id' => $id));

            $this->request->data['CustomerPass']['property_id'] = $propertyId;

            $owner = $this->request->data['CustomerPass']['owner'];
            $make = $this->request->data['CustomerPass']['make'];
            $model = $this->request->data['CustomerPass']['model'];
            $color = $this->request->data['CustomerPass']['color'];
            $plateNo = $this->request->data['CustomerPass']['license_plate_number'];
            $plateState = $this->request->data['CustomerPass']['license_plate_state'];
            $vin = $this->request->data['CustomerPass']['last_4_digital_of_vin'];
            $this->CustomerPass->set($this->request->data);
            if ($this->request->data['CustomerPass']['add_vehicle'] == 0) {
                $this->CustomerPass->updateValidations();
            } else {
                $vehicle = $this->request->data['CustomerPass']['add_vehicle'];
            }
            if ($this->CustomerPass->validates()) {
                if (isset($this->request->data['CustomerPass']['pass_valid_upto'])) {
                    $this->request->data['CustomerPass']['pass_valid_upto'] = date("Y-m-d H:i:s", strtotime($this->request->data['CustomerPass']['pass_valid_upto']));
                }
                if (isset($this->request->data['CustomerPass']['membership_vaild_upto'])) {
                    $this->request->data['CustomerPass']['membership_vaild_upto'] = date("Y-m-d H:i:s", strtotime($this->request->data['CustomerPass']['membership_vaild_upto']));
                } else {
                    $this->request->data['CustomerPass']['membership_vaild_upto'] = NULL;
                }
                if ($this->request->data['CustomerPass']['add_vehicle'] != 0) {

                    if (empty($this->request->data['CustomerPass']['selected_vehicle'])) {
                       
                        if ($this->request->data['CustomerPass']['vehicle_id'] != '') {
							//IF SAME VEHICLE ONLY EDIT DETAILS
                            $vehicleArray['Vehicle'] = array('id' => $this->request->data['CustomerPass']['vehicle_id'],
                                'owner' => $this->request->data['CustomerPass']['owner'],
                                'make' => $this->request->data['CustomerPass']['make'],
                                'model' => $this->request->data['CustomerPass']['model'],
                                'color' => $this->request->data['CustomerPass']['color'],
                                'license_plate_number' => $this->request->data['CustomerPass']['license_plate_number'],
                                'license_plate_state' => $this->request->data['CustomerPass']['license_plate_state'],
                                'last_4_digital_of_vin' => $this->request->data['CustomerPass']['last_4_digital_of_vin'],
                                'last_4_digital_of_vin' => $this->request->data['CustomerPass']['last_4_digital_of_vin'],
                            );
                            $this->loadModel('Vehicle');
                            if ($this->Vehicle->save($vehicleArray, false)) {
                                $arrayCp['CustomerPass'] = array('id' => (int) $this->request->data['CustomerPass']['id'],
                                    'RFID_tag_number' => $this->request->data['CustomerPass']['RFID_tag_number'],
                                    'pass_valid_upto' => $this->request->data['CustomerPass']['pass_valid_upto'],
                                    'membership_vaild_upto' => $this->request->data['CustomerPass']['membership_vaild_upto']
                                );
                                if ($this->CustomerPass->save($arrayCp, false)) {
                                    CakeLog::write('passEditedORVehicleDetailsEditedAdmin', '' . AuthComponent::user('username') . ' : Customer Pass Edited OR Vehicle Details Changed, Customer Pass ID: <a href="/admin/CustomerPasses/edit/' . $id . '">' . $id . '</a>  by User: ' . AuthComponent::user('first_name') . ' ' . AuthComponent::user('last_name') . ', Vehicle added and assigned having  Vehicle ID <a href="/admin/Vehicles/edit/' . $this->request->data['CustomerPass']['vehicle_id'] . '">' . $this->request->data['CustomerPass']['vehicle_id'] . ' </a>');
                                    $this->Session->setFlash('The details saved successfully.', 'success');
                                    return $this->redirect($this->referer());
                                } else {
                                    $this->Session->setFlash('The details cannot be saved', 'error');
                                }
                            } else {
                                $this->Session->setFlash('The vehicle not saved', 'error');
                            }
                        } else {
							//NEW VEHICLE ADDED REMOVE PASS FROM ALL PREVIOUS VEHICLE
                            $this->loadModel('Vehicle');
                            $this->Vehicle->updateAll(array('Vehicle.customer_pass_id'=>NULL),array('Vehicle.customer_pass_id'=>$id));
                            $this->Vehicle->clear();  
                            $userId = $this->CustomerPass->field('user_id', array('id' => $id));
                            $vehicleArray['Vehicle'] = array(
                                'property_id' => $propertyId,
                                'user_id' => $userId,
                                'customer_pass_id' => $id,
                                'owner' => $this->request->data['CustomerPass']['owner'],
                                'make' => $this->request->data['CustomerPass']['make'],
                                'model' => $this->request->data['CustomerPass']['model'],
                                'color' => $this->request->data['CustomerPass']['color'],
                                'license_plate_number' => $this->request->data['CustomerPass']['license_plate_number'],
                                'license_plate_state' => $this->request->data['CustomerPass']['license_plate_state'],
                                'last_4_digital_of_vin' => $this->request->data['CustomerPass']['last_4_digital_of_vin'],
                                'last_4_digital_of_vin' => $this->request->data['CustomerPass']['last_4_digital_of_vin'],
                            );
                            if ($this->Vehicle->save($vehicleArray, false)) {
                                $lastInsertedId = $this->Vehicle->getLastInsertId();
                                $arrayCp['CustomerPass'] = array('id' => (int) $this->request->data['CustomerPass']['id'],
                                    'RFID_tag_number' => $this->request->data['CustomerPass']['RFID_tag_number'],
                                    'pass_valid_upto' => $this->request->data['CustomerPass']['pass_valid_upto'],
                                    'membership_vaild_upto' => $this->request->data['CustomerPass']['membership_vaild_upto'],
                                    'vehicle_id' => $lastInsertedId
                                );
                                if ($this->CustomerPass->save($arrayCp, false)) {
                                    CakeLog::write('passEditedAndNewVehicleAddedAdmin', '' . AuthComponent::user('username') . ' : Customer Pass Edited And New Vehicle Added, Customer Pass ID: <a href="/admin/CustomerPasses/edit/' . $id . '">' . $id . '</a>  by User: ' . AuthComponent::user('first_name') . ' ' . AuthComponent::user('last_name') . ', Vehicle added and assigned having  Vehicle ID <a href="/admin/Vehicles/edit/' . $lastInsertedId . '">' . $lastInsertedId . ' </a>');
                                    $this->Session->setFlash('The details saved successfully.', 'success');
                                     return $this->redirect($this->referer());
                                } else {
                                    $this->Session->setFlash('The details cannot be saved', 'error');
                                }
                            }
                        }
                    } else {
						// REMOVE VEHICLE FROM PREVIOUS PASS IF ATTACHED TO ANY PASS
						$this->CustomerPass->recursive=-1;
						$previousPass=$this->CustomerPass->find('all',array('conditions'=>array('vehicle_id'=>$this->request->data['CustomerPass']['selected_vehicle'])));
						if($previousPass){
							$this->CustomerPass->updateAll(array('vehicle_id'=>NULL),array('vehicle_id'=>$this->request->data['CustomerPass']['selected_vehicle']));
						}
						$this->loadModel('Vehicle');
						//SET CUSTOMER PASS ID TO NULL IF IT IS ATTACHED TO ANY OTHER VEHICLE
						$this->Vehicle->updateAll(array('Vehicle.customer_pass_id'=>NULL),array('Vehicle.customer_pass_id'=>$id));
						$this->CustomerPass->clear(); 
                        $vehicleArray['Vehicle'] = array(
                            'id' => $this->request->data['CustomerPass']['selected_vehicle'],
                            'customer_pass_id' => $id,
                        );
                        
                        if ($this->Vehicle->save($vehicleArray, false)) {
                            $lastInsertedId = $this->Vehicle->getLastInsertId();
                            $arrayCp['CustomerPass'] = array('id' => (int) $this->request->data['CustomerPass']['id'],
                                'RFID_tag_number' => $this->request->data['CustomerPass']['RFID_tag_number'],
                                'pass_valid_upto' => $this->request->data['CustomerPass']['pass_valid_upto'],
                                'membership_vaild_upto' => $this->request->data['CustomerPass']['membership_vaild_upto'],
                                'vehicle_id' => $this->request->data['CustomerPass']['selected_vehicle']
                            );
                            
                            if ($this->CustomerPass->save($arrayCp, false)) {
                                CakeLog::write('passEditedAndListedVehicleSelectAdmin', '' . AuthComponent::user('username') . ' : Customer Pass Edited And Listed Vehicle Assigned, Customer Pass ID: <a href="/admin/CustomerPasses/edit/' . $id . '">' . $id . '</a>  by User: ' . AuthComponent::user('first_name') . ' ' . AuthComponent::user('last_name') . ', Vehicle assigned having  Vehicle ID <a href="/admin/Vehicles/edit/' . $this->request->data['CustomerPass']['selected_vehicle'] . '">' . $this->request->data['CustomerPass']['selected_vehicle'] . ' </a>');
                                $this->Session->setFlash('The details saved successfully.', 'success');
                                 return $this->redirect($this->referer());
                            } else {
                                $this->Session->setFlash('The details cannot be saved', 'error');
                            }
                        }
                    }
                } else {
					//THERE IS ONLY PASS TO EDIT
                    $arrayCp['CustomerPass'] = array('id' => (int) $this->request->data['CustomerPass']['id'],
                        'RFID_tag_number' => $this->request->data['CustomerPass']['RFID_tag_number'],
                        'pass_valid_upto' => $this->request->data['CustomerPass']['pass_valid_upto'],
                        'membership_vaild_upto' => $this->request->data['CustomerPass']['membership_vaild_upto']
                    );	
                    if ($this->CustomerPass->save($arrayCp, false)) {
                        CakeLog::write('onlyPassEditedAdmin', '' . AuthComponent::user('username') . ' : Only Customer Pass Details Edited, Customer Pass ID: <a href="/admin/CustomerPasses/edit/' . $id . '">' . $id . '</a>  by User: ' . AuthComponent::user('first_name') . ' ' . AuthComponent::user('last_name') . ' ');
                        $this->Session->setFlash('The details saved successfully.', 'success');
                         return $this->redirect($this->referer());
                    } else {
                        $this->Session->setFlash('The details cannot be saved', 'error');
                    }
                }
            } else {
                $this->Session->setFlash('Validation Failed', 'error');
            }
        }
        $options = array('conditions' => array('CustomerPass.' . $this->CustomerPass->primaryKey => $id));
        $this->CustomerPass->unBindModel(array('belongsTo' => array('Transaction', 'Property')));
        $this->request->data = $this->CustomerPass->find('first', $options);
        if ($vehicle) {
            $this->request->data['CustomerPass']['add_vehicle'] = 1;
            $this->request->data['CustomerPass']['owner'] = $owner;
            $this->request->data['CustomerPass']['make'] = $make;
            $this->request->data['CustomerPass']['model'] = $model;
            $this->request->data['CustomerPass']['color'] = $color;
            $this->request->data['CustomerPass']['license_plate_number'] = $plateNo;
            $this->request->data['CustomerPass']['license_plate_state'] = $plateState;
            $this->request->data['CustomerPass']['last_4_digital_of_vin'] = $vin;
        } else {
            $this->request->data['CustomerPass']['owner'] = $this->request->data['Vehicle']['owner'];
            $this->request->data['CustomerPass']['make'] = $this->request->data['Vehicle']['make'];
            $this->request->data['CustomerPass']['model'] = $this->request->data['Vehicle']['model'];
            $this->request->data['CustomerPass']['color'] = $this->request->data['Vehicle']['color'];
            $this->request->data['CustomerPass']['license_plate_number'] = $this->request->data['Vehicle']['license_plate_number'];
            $this->request->data['CustomerPass']['license_plate_state'] = $this->request->data['Vehicle']['license_plate_state'];
            $this->request->data['CustomerPass']['last_4_digital_of_vin'] = $this->request->data['Vehicle']['last_4_digital_of_vin'];
            if (!is_null($VehicleId = $this->CustomerPass->field('vehicle_id', array('id' => $id)))) {
                $this->request->data['CustomerPass']['add_vehicle'] = 1;
            } else {
                $this->request->data['CustomerPass']['add_vehicle'] = 0;
            }
        }

        $this->request->data['CustomerPass']['pass_name'] = $this->request->data['Pass']['name'];
        $this->request->data['CustomerPass']['package_name'] = $this->request->data['Package']['name'];
        $this->request->data['CustomerPass']['customer_name'] = $this->request->data['User']['first_name'] . ' ' . $this->request->data['User']['last_name'] . '<br>' .
                $this->request->data['User']['address_line_1'] . ' ' . $this->request->data['User']['address_line_2'] . '<br>' .
                $this->request->data['User']['city'] . ' ' . $this->request->data['User']['state'] . '<br>' .
                $this->request->data['User']['zip'] . '<br>' .
                $this->request->data['User']['email'] . ' ' . $this->request->data['User']['phone'];
        $this->request->data['CustomerPass']['pass_valid_upto'] = date("m/d/Y H:i:s", strtotime($this->request->data['CustomerPass']['pass_valid_upto']));
        if (is_null($this->request->data['CustomerPass']['membership_vaild_upto'])) {
            $this->request->data['CustomerPass']['membership_vaild_upto'] = NULL;
        } else {
            $this->request->data['CustomerPass']['membership_vaild_upto'] = date("m/d/Y H:i:s", strtotime($this->request->data['CustomerPass']['membership_vaild_upto']));
        }
        if (!is_null($VehicleId = $this->CustomerPass->field('vehicle_id', array('id' => $id)))) {
            $this->request->data['CustomerPass']['vehicle_id'] = $VehicleId;
        }
        $this->loadModel('State');
        $state = new State();
        $states = $state->find('list');
        $this->loadModel('Vehicle');
        $vehicle_info = $this->Vehicle->find('list', array('fields' => array('vehicle_info'), 'conditions' => array('user_id' => $this->request->data['CustomerPass']['user_id'], 'property_id' => $this->request->data['CustomerPass']['property_id'], 'customer_pass_id' => Null)));
        $this->set(compact('states', 'vehicle_info'));
    }


/***********************************************************************
 * admin_delete method
 */
	public function admin_delete($id = null) {
		$result=$this->processDelete($id);
		if($result){
			$this->Session->setFlash(__('The customer pass has been deleted.'));
		}else{
			$this->Session->setFlash(__('The customer pass could not be deleted. Please, try again.'));
		}
		return $this->redirect($this->referer());
	}
	
    public function getExpiryDate($id=null)
    {
        $this->CustomerPass->recursive=-1;
        return $this->CustomerPass->find('first',array('fields'=>array('pass_valid_upto'),'conditions'=>array('pass_id'=>$id)));
    }
 /***************************************************************
  * Passes Details
  */
	public function getCustomerPassDetails($id=null){
		if (!$this->CustomerPass->exists($id)) {
            throw new NotFoundException(__('Invalid DATA'));
        }else{
			$userID=$this->CustomerPass->field('user_id',array('id'=>$id));
			if($userID != AuthComponent::user('id')){
				 throw new NotFoundException(__('Invalid DATA'));
			}
		}
        $packageDate=$this->CustomerPass->field('membership_vaild_upto',array('id'=>$id));
        if(!is_null($packageDate)){
			$dt = new DateTime();
			$currentDateTime= $dt->format('Y-m-d H:i:s');
			if($packageDate>$currentDateTime){
				$this->Session->setFlash('This pass is already active','warning');
				$this->redirect(array('action'=>'view'));
			}
		}
		$passId=$this->CustomerPass->field('pass_id',array('id'=>$id));
		$this->loadModel('Pass');
		$pass=new Pass();
		$permitId=$pass->field('package_id',array('id'=>$passId));
		if($permitId==null){
			$this->Session->write('PassToBuy',$passId);		
			$this->redirect(array('controller'=>'Packages','action'=>'selectPermitToBuy'));
		}else{
			$this->loadModel('Package');
			$isGuest=$this->Package->field('is_guest',array('id'=>$permitId));
			if($isGuest==1)
			{
			  $this->redirect(array('controller'=>'Vehicles','action'=>'setGuestPackageOptions',$id,$passId,$permitId));
			}
			else{
				$this->loadModel('Package');
				$permitCost=$this->Package->field('cost',array('id'=>$permitId));
				//debug($permitCost);
				if($permitCost==0){
								$arr['details']['pass_Id']=$passId;
								$arr['details']['customer_pass_Id']=$id;
								$arr['details']['permit_Id']=$permitId;
								$arraySerialize=serialize($arr);
								$dt = new DateTime();
								$currentDateTime= $dt->format('Y-m-d H:i:s');
								$transactionArray['Transaction']['paypal_tranc_id']='No Payment Pass Activation';
								$transactionArray['Transaction']['user_id']=$this->Auth->user('id');
								$transactionArray['Transaction']['date_time']=$currentDateTime;
								$transactionArray['Transaction']['amount']=0;
								$transactionArray['Transaction']['result']='success';
								$transactionArray['Transaction']['pass_id']=$passId;
								$transactionArray['Transaction']['transaction_array']=$arraySerialize;
								$transactionArray['Transaction']['first_name']=$this->Auth->user('first_name');
								$transactionArray['Transaction']['last_name']=$this->Auth->user('last_name');
								$transactionArray['Transaction']['comments'] = "passActivated";	
								$this->loadModel('Transaction');
								$this->Transaction->create();
								if($this->Transaction->save($transactionArray,false)){
											$this->Package->recursive=-1;
											$result=$this->Package->find('first',array('conditions'=>array('id'=>$permitId)));
											if($result['Package']['is_fixed_duration']==0){
													switch ($result['Package']['duration_type']) {
														case "Hour":
															$date = new DateTime();
															$k=(int)$result['Package']['duration'];
															$date->add(new DateInterval('PT'.$k.'H'));
															$result['Package']['expiration_date']=date_format($date, 'Y-m-d H:i:s');
															break;
														case "Day":
															$date = new DateTime();
															$k=$result['Package']['duration'];
															$date->add(new DateInterval('P'.$k.'D'));
															$result['Package']['expiration_date']=date_format($date, 'Y-m-d H:i:s');
															break;
														case "Week":
															$date = new DateTime();
															$k=(int)$result['Package']['duration']*7;
															$date->add(new DateInterval('P'.$k.'D'));
															$result['Package']['expiration_date']=date_format($date, 'Y-m-d H:i:s');
															break;
														case "Month":
															$date = new DateTime();
															$k=(int)$result['Package']['duration'];
															$date->add(new DateInterval('P'.$k.'M'));
															$result['Package']['expiration_date']=date_format($date, 'Y-m-d H:i:s');
															break;
														case "Year":
															$date = new DateTime();
															$k=(int)$result['Package']['duration'];
															$date->add(new DateInterval('P'.$k.'Y'));
															$result['Package']['expiration_date']=date_format($date, 'Y-m-d H:i:s');
															break;
													}
											}
											$this->loadModel('CustomerPass');
											$customerPassArray['CustomerPass']['transaction_id']=$this->Transaction->getLastInsertId();
											$customerPassArray['CustomerPass']['membership_vaild_upto']=$result['Package']['expiration_date'];
											$customerPassArray['CustomerPass']['id']=$id;
											$customerPassArray['CustomerPass']['package_id']= $permitId;
											$previousId=$this->CustomerPass->field('vehicle_id',array('id'=>$id));
											
											if($this->CustomerPass->save($customerPassArray,false)){
												$this->loadModel('Package');
												$packageName=$this->Package->givePackageName($permitId);
												CakeLog::write('validPassActivated', ''.AuthComponent::user('username').' : Valid Pass Activated Customer Pass ID: <a href="/admin/CustomerPasses/edit/'.$id.'">'.$id.'</a> Package ID: '.$permitId.', Package Name: '.$packageName.' Activated by User: '.AuthComponent::user('first_name').' '.AuthComponent::user('last_name').' In Property: '.CakeSession::read('PropertyName').', Cost Of Activation is ZERO');
												$this->Session->setFlash('Pass activated successfully','success');
												$this->redirect(array('controller'=>'Vehicles','action'=>'add',$id));
											}else{
												$this->Session->setFlash('Pass could not be activated','error');
											}
								}else{
									$this->Session->setFlash('Pass could not be activated','error');	
							    }	
				}else{
					$this->redirect(array('controller'=>'Transactions','action'=>'activatePass',$id,$passId,$permitId));
				}	
			}
		}
	}
	public function guestPass()
	{
		$this->layout='customer';
		$this->loadModel('Package');  
		$this->Package->recursive=-1;  
		$guestPass=array();
		$pack=$this->Package->find('all',array('fields'=>'pass_id,name','conditions'=>array('property_id'=>$this->Session->read('PropertyId'),'is_guest'=>1)));
		if(!empty($pack)){
			for($i=0;$i<count($pack);$i++){
				$this->CustomerPass->recursive=-1;
				$passExists=$this->CustomerPass->find('first',array('conditions'=>array('pass_id'=>$pack[$i]['Package']['pass_id'],'user_id'=>$this->Auth->user('id'))));
				if($passExists){
					$guestPass[$i]=$passExists;
			    }
			}
			if(!empty($guestPass)){
				$this->CustomerPass->recursive=-1;
				$dt = new DateTime();
				$currentDateTime= $dt->format('Y-m-d H:i:s');
				for($i=0;$i<count($guestPass);$i++){
					if($guestPass[$i]['CustomerPass']['membership_vaild_upto']>$currentDateTime && $guestPass[$i]['CustomerPass']['pass_valid_upto']>$currentDateTime){
						$guestPass[$i]['CustomerPass']['status']='active';
					}
					elseif(($guestPass[$i]['CustomerPass']['membership_vaild_upto']<$currentDateTime ||$guestPass[$i]['CustomerPass']['membership_vaild_upto']==null)&& $guestPass[$i]['CustomerPass']['pass_valid_upto']>$currentDateTime){
						$guestPass[$i]['CustomerPass']['status']='valid';
					}
					elseif($guestPass[$i]['CustomerPass']['membership_vaild_upto']<$currentDateTime && $guestPass[$i]['CustomerPass']['pass_valid_upto']<$currentDateTime){
						$guestPass[$i]['CustomerPass']['status']='expired';
					}
				}
			}
		}else{
			$this->CustomerPass->recursive=-1;
			$guestPass=$this->CustomerPass->find('all',array('conditions'=>array('is_guest_pass'=>1,'user_id'=>$this->Auth->user('id'))));
			$dt = new DateTime();
			$currentDateTime= $dt->format('Y-m-d H:i:s');
			if($guestPass){
				for($i=0;$i<count($guestPass);$i++){
					$guestPass[$i]['CustomerPass']['original_pass']=0;
					if($guestPass[$i]['CustomerPass']['membership_vaild_upto']>$currentDateTime && $guestPass[$i]['CustomerPass']['pass_valid_upto']>$currentDateTime){
						$guestPass[$i]['CustomerPass']['status']='active';
					}
					elseif(($guestPass[$i]['CustomerPass']['membership_vaild_upto']<$currentDateTime ||$guestPass[$i]['CustomerPass']['membership_vaild_upto']==null)&& $guestPass[$i]['CustomerPass']['pass_valid_upto']>$currentDateTime){
						$guestPass[$i]['CustomerPass']['status']='valid';
					}
					elseif($guestPass[$i]['CustomerPass']['membership_vaild_upto']<$currentDateTime && $guestPass[$i]['CustomerPass']['pass_valid_upto']<$currentDateTime){
						$guestPass[$i]['CustomerPass']['status']='expired';
					}
				}
			}
		}
		$this->set(compact('guestPass'));
	}
/************************************************************************
 * Add RFID Tags
 */
	public function admin_addRfid()
	{
		$this->loadModel('Property');
		$property_list=$this->Property->find('list',array('fields'=>'name'));
		$id=array_keys($property_list);
		$selectedId=$id[0];
		$this->loadModel('Pass');
		$passes_list=$this->Pass->find('list',array('fields'=>'name','conditions'=>array('property_id'=>$selectedId)));
		$pass_id=array_keys($property_list);
		$selectedPassId=$pass_id[0];
		$this->set(compact('property_list','selectedId','passes_list','selectedPassId'));
	}
	public function admin_viewRfid()
	{
		    $this->loadModel('Property');
			$property_list=$this->Property->find('list',array('fields'=>'name'));
			$id=array_keys($property_list);
			$selectedId=$id[0];
			$this->set(compact('property_list','selectedId'));
	}
	public function admin_get_passes(){
		$conditions=false;
		if($this->request->query['property']){
			$conditions=' properties.id= '.$this->request->query['property'];
		}
		if($this->request->query['type']!=''){
			if($conditions){
				$conditions= $conditions.' AND ';
			}
			$conditions=$conditions.' users.manager_approved= '.$this->request->query['type'];
		}
		$this->autoRender=$this->layout=false;
		$output=array();
		$output = $this->CustomerPass->getTableData($conditions);
        echo json_encode($output);
	}
	public function admin_view_passes($property_id)
	{
		
		$this->autoRender=$this->layout=false;
		$output=array();
		$output = $this->CustomerPass->getRFIDDataPropertyWise($property_id);
        echo json_encode($output);
	}
/***********************************************************************
 * Renew Pass
 */ 	 
public function renewPass($customerPassId=null,$advance=false){
		if (!$this->CustomerPass->hasAny(array('id'=>$customerPassId,'user_id'=>AuthComponent::user('id')))) {
			CakeLog::write('wrongPassRenewAttempt', ''.AuthComponent::user('username').' : Attempt to renew wrong Pass with  Customer Pass ID: <a href="/admin/CustomerPasses/edit/'.$customerPassId.'">'.$customerPassId.'</a> was done by by User: '.AuthComponent::user('first_name').' '.AuthComponent::user('last_name').', in Property: '.CakeSession::read('PropertyName'));
			throw new NotFoundException(__('Invalid customer pass'));
		}
		$passId=$this->CustomerPass->field('pass_id',array('id'=>$customerPassId));
		$this->loadModel('Pass');
		$this->Pass->recursive=-1;
		$passDetails=$this->Pass->find('first',array('conditions'=>array('id'=>$passId)));
		if(empty($passDetails)){
			$this->Session->setFlash('Sorry, this pass cannot be renewed as this pass has been removed from this property','warning');	
			$this->redirect(array('action'=>'view'));
		}
		$packageId=$passDetails['Pass']['package_id'];
		//debug($packageId);
		if(!is_null($packageId)){
			$this->loadModel('Package');
			if(!$this->Package->exists($packageId)){
				$this->Session->setFlash('Sorry, this pass cannot be renewed as this pass has been removed from this property','warning');	
				$this->redirect(array('action'=>'view'));
			}
		}
		$advance=false;
		$pass_expiry = $this->CustomerPass->field('pass_valid_upto', array('id' => $customerPassId));
		if($passDetails['Pass']['is_fixed_duration']==0){
					$pass_expiry = $this->CustomerPass->field('pass_valid_upto', array('id' => $customerPassId));
					$dt = new DateTime();
					$currentDateTime = $dt->format('Y-m-d H:i:s');
					if($pass_expiry>$currentDateTime){
						$currentDateTime=$pass_expiry; 
						$advance=true;
					}
                    switch ($passDetails['Pass']['duration_type']) 
                    {
                        case "Hour":
                            $date = new DateTime($currentDateTime);
                            $k=(int)$passDetails['Pass']['duration'];
                            $date->add(new DateInterval('PT'.$k.'H'));
                            $passDetails['Pass']['expiration_date']=date_format($date, 'Y-m-d H:i:s');
                            break;
                        case "Day":
                            $date = new DateTime($currentDateTime);
                            $k=$passDetails['Pass']['duration'];
                            $date->add(new DateInterval('P'.$k.'D'));
                            $passDetails['Pass']['expiration_date']=date_format($date, 'Y-m-d H:i:s');
                            break;
                        case "Week":
                            $date = new DateTime($currentDateTime);
                            $k=(int)$passDetails['Pass']['duration']*7;
                            $date->add(new DateInterval('P'.$k.'D'));
                            $passDetails['Pass']['expiration_date']=date_format($date, 'Y-m-d H:i:s');
                            break;
                        case "Month":
                            $date = new DateTime($currentDateTime);
                            $k=(int)$passDetails['Pass']['duration'];
                            $date->add(new DateInterval('P'.$k.'M'));
                            $passDetails['Pass']['expiration_date']=date_format($date, 'Y-m-d H:i:s');
                            break;
                        case "Year":
                            $date = new DateTime($currentDateTime);
                            $k=(int)$passDetails['Pass']['duration'];
                            $date->add(new DateInterval('P'.$k.'Y'));
                            $passDetails['Pass']['expiration_date']=date_format($date, 'Y-m-d H:i:s');
                            break;
                    }
                    $packageDetails=array();
                    $totalRenewAmount=$passDetails['Pass']['cost_after_1st_year'];
                    $date3 = new DateTime($passDetails['Pass']['expiration_date']);
					$datePack=$date3->format('Y-m-d');
					$passDetails['Pass']['expiration_date']= $datePack.' 23:59:59';
					if($passDetails['Pass']['package_id']){
						$this->loadModel('Package');
						$this->Package->recursive=-1;
						$packageDetails=$this->Package->find('first',array('conditions'=>array('id'=>$passDetails['Pass']['package_id'])));
						if($packageDetails){
							if($packageDetails['Package']['is_guest']==0){
								$totalRenewAmount=$totalRenewAmount+$packageDetails['Package']['cost'];
							}
						}
					}
                    if($totalRenewAmount==0){
								$arr['details']['pass_Id']=$passId;
								$arr['details']['customer_pass_Id']=$customerPassId;
								$arraySerialize=serialize($arr);
								$dt = new DateTime();
								$currentDateTime= $dt->format('Y-m-d H:i:s');
								$transactionArray['Transaction']['paypal_tranc_id']='Pass Renew No Payment';
								$transactionArray['Transaction']['user_id']=$this->Auth->user('id');
								$transactionArray['Transaction']['date_time']=$currentDateTime;
								$transactionArray['Transaction']['amount']=0;
								$transactionArray['Transaction']['result']='success';
								$transactionArray['Transaction']['pass_id']=$passId;
								$transactionArray['Transaction']['transaction_array']=$arraySerialize;
								$transactionArray['Transaction']['first_name']=$this->Auth->user('first_name');
								$transactionArray['Transaction']['last_name']=$this->Auth->user('last_name');
								$transactionArray['Transaction']['comments'] = "passRenewed";
								$transactionArray['Transaction']['transaction_type'] = 1;	
								$this->loadModel('Transaction');
								$this->Transaction->create();
								if($this->Transaction->save($transactionArray,false)){
									$transactionId=$this->Transaction->getLastInsertId();
									$previousId=$this->CustomerPass->field('vehicle_id',array('id'=>$customerPassId));
									$package_id=$packageExpiryDate=NULL;
									if($packageDetails){
										if($packageDetails['Package']['is_guest']==0){
												if($packageDetails['Package']['is_fixed_duration']==0){
														$package_expiry_date = $this->CustomerPass->field('pass_valid_upto', array('id' => $customerPassId));
														$dt = new DateTime();
														$currentDateTime = $dt->format('Y-m-d H:i:s');
														if($package_expiry_date){
															if($package_expiry_date>$currentDateTime){
																$currentDateTime=$package_expiry_date; 
															}
														}
														switch ($packageDetails['Package']['duration_type']) 
														{
															case "Hour":
																$date = new DateTime($currentDateTime);
																$k=(int)$packageDetails['Package']['duration'];
																$date->add(new DateInterval('PT'.$k.'H'));
																$packageDetails['Package']['expiration_date']=date_format($date, 'Y-m-d H:i:s');
																break;
															case "Day":
																$date = new DateTime($currentDateTime);
																$k=$packageDetails['Package']['duration'];
																$date->add(new DateInterval('P'.$k.'D'));
																$packageDetails['Package']['expiration_date']=date_format($date, 'Y-m-d H:i:s');
																break;
															case "Week":
																$date = new DateTime($currentDateTime);
																$k=(int)$packageDetails['Package']['duration']*7;
																$date->add(new DateInterval('P'.$k.'D'));
																$packageDetails['Package']['expiration_date']=date_format($date, 'Y-m-d H:i:s');
																break;
															case "Month": 
																$date = new DateTime($currentDateTime);
																$k=(int)$packageDetails['Package']['duration'];
																$date->add(new DateInterval('P'.$k.'M'));
																$packageDetails['Package']['expiration_date']=date_format($date, 'Y-m-d H:i:s');
																break;
															case "Year":
																$date = new DateTime($currentDateTime);
																$k=(int)$packageDetails['Package']['duration'];
																$date->add(new DateInterval('P'.$k.'Y'));
																$packageDetails['Package']['expiration_date']=date_format($date, 'Y-m-d H:i:s');
																break;
														}
													}
													$date4 = new DateTime($packageDetails['Package']['expiration_date']);
													$datePack=$date4->format('Y-m-d');
													$packageExpiryDate=$packageDetails['Package']['expiration_date']= $datePack.' 23:59:59';
													$package_id=$packageDetails['Package']['id'];
										}else{
											$package_id=$packageDetails['Package']['id'];
										}
									}
									$array['CustomerPass']=array(
													  'id'=>$customerPassId,
													  'pass_id'=>$passId,
													  'pass_valid_upto'=> $passDetails['Pass']['expiration_date'], 
													  'membership_vaild_upto'=>$packageExpiryDate,
													  'package_id'=>$package_id,
													 // 'vehicle_id'=>null,
													  'transaction_id'=>$transactionId
									);
									if($packageDetails){
										if($packageDetails['Package']['is_guest']==1){
												$array['CustomerPass']['is_guest_pass']=1;
										}else{
											$array['CustomerPass']['is_guest_pass']=0;
										}
										if($advance){
											if($packageDetails['Package']['is_guest']==1){
												unset($array['CustomerPass']['membership_vaild_upto']);
											}
										}else{
											if($packageDetails['Package']['is_guest']==1){
												$this->loadModel('Vehicle');
												//SET CUSTOMER PASS ID TO NULL IF IT IS ATTACHED TO ANY OTHER VEHICLE
												$this->Vehicle->updateAll(array('Vehicle.customer_pass_id'=>NULL),array('Vehicle.customer_pass_id'=>$customerPassId));
												$array['CustomerPass']['vehicle_id']=null;
												if($previousId){
													 //SET VEHICLE ID TO NULL IF IT IS ATTACHED TO ANY OTHER PASS
													$this->CustomerPass->updateAll(array('CustomerPass.vehicle_id'=>NULL),array('CustomerPass.vehicle_id'=>$previousId));
													
													$vehicleArray['Vehicle']=array('id'=>$previousId,
																				   'customer_pass_id'=>null								
													);
													if($this->Vehicle->save($vehicleArray,false)){
														CakeLog::write('passRenewedVehicleNull', ''.AuthComponent::user('username').' : Pass renewed with Pass ID: '.$passId.' Customer Pass ID: <a href="/admin/CustomerPasses/edit/'.$customerPassId.'">'.$customerPassId.'</a> renewed by User: '.AuthComponent::user('first_name').' '.AuthComponent::user('last_name').', in Property: '.CakeSession::read('PropertyName').', VEHICLE SET NULL');
													}else{
														CakeLog::write('passRenewedVehicleNotNULL', ''.AuthComponent::user('username').' : Pass renewed with Pass ID: '.$passId.' And Customer Pass ID: <a href="/admin/CustomerPasses/edit/'.$customerPassId.'">'.$customerPassId.'</a> renewed by User: '.AuthComponent::user('first_name').' '.AuthComponent::user('last_name').', in Property: '.CakeSession::read('PropertyName').', VEHICLE NOT SET NULL');
													}
												}else{
													CakeLog::write('passRenewedNOVehicle', ''.AuthComponent::user('username').' : Pass renewed with Pass ID: '.$passId.' Customer Pass ID: <a href="/admin/CustomerPasses/edit/'.$customerPassId.'">'.$customerPassId.'</a> renewed by User: '.AuthComponent::user('first_name').' '.AuthComponent::user('last_name').', in Property: '.CakeSession::read('PropertyName').', VEHICLE NOT FOUND');
												}
											}
										}
									}
										//if($this->Vehicle->save($vehicleArray,false)){
											if($this->CustomerPass->save($array,false)){
												$this->loadModel('Pass');
												$passName=$this->Pass->givePassName($passId);
												if($advance){
													CakeLog::write('passRenewedAdvance', ''.AuthComponent::user('username').' : Pass renewed IN ADVANCE with Pass ID: '.$passId.' and Pass Name: '.$passName.', And Customer Pass ID: <a href="/admin/CustomerPasses/edit/'.$customerPassId.'">'.$customerPassId.'</a> renewed by User: '.AuthComponent::user('first_name').' '.AuthComponent::user('last_name').', in Property: '.CakeSession::read('PropertyName').', Amount Paid: $0 EXPIRY DATE WAS: '.$pass_expiry);
												}else{
													CakeLog::write('passRenewed', ''.AuthComponent::user('username').' : Pass renewed with Pass ID: '.$passId.' and Pass Name: '.$passName.', And Customer Pass ID: <a href="/admin/CustomerPasses/edit/'.$customerPassId.'">'.$customerPassId.'</a> renewed by User: '.AuthComponent::user('first_name').' '.AuthComponent::user('last_name').', in Property: '.CakeSession::read('PropertyName').', Amount Paid: $0');
												}
												$this->Session->setFlash('Pass renewed successfully','success');
												if($packageDetails['Package']['is_guest']==0){
													$this->Session->write('Allowed',false);
													return $this->redirect(array('controller'=>'Vehicles','action' => 'add',$customerPassId));
												}else{
													$this->redirect(array('action'=>'view'));
												}
											}else{
												$this->Session->setFlash('Process Failed','error');
												$this->redirect(array('action'=>'view'));
											}
									/*}else{
										$this->Session->setFlash('Vehicle Not Updated','error');
									}	*/
								}else{
									$this->Session->setFlash('Internal Error','error');
									$this->redirect(array('action'=>'view'));
							}	
					}else{
							$this->redirect(array('controller'=>'Transactions','action'=>'passRenew',$customerPassId,$passId,$advance));
					}
		}
		else{
			$this->Session->setFlash('Pass is not renewable','error');
			$this->redirect(array('action'=>'view'));
		}
	}
/*************************************************************88
 * Manager Active Vehicles
 */
	public function manager_active_vehicles(){
		$this->layout='manager';
	}
	public function manager_get_active_vehicles(){
		$this->autoRender=$this->layout=false;
		$output=array();
		$output = $this->CustomerPass->getActiveVehicle();
        echo json_encode($output);
	}
/*************************************************************
 * Manager In-Active Vehicles
 */
	public function manager_inactive_vehicles(){
		$this->layout='manager';
	}
	public function manager_get_inactive_vehicles(){
		$this->autoRender=$this->layout=false;
		$output=array();
		$output = $this->CustomerPass->getInActiveVehicle();
        echo json_encode($output);
	}
/*************************************************************
 * Manager guest Vehicles
 */
	public function manager_guest_vehicles(){
		$this->layout='manager';
	}
	public function manager_get_guest_vehicles(){
		$this->autoRender=$this->layout=false;
		$output=array();
		$output = $this->CustomerPass->getGuestVehicle();
        echo json_encode($output);
	}
/*************************************************************
 * Manager Active Guest Vehicles
 */
	public function manager_active_guest_vehicles(){
		$this->layout='manager';
	}
	public function manager_get_active_guest_vehicles(){
		$this->autoRender=$this->layout=false;
		$output=array();
		$output = $this->CustomerPass->getActiveGuestVehicle();
        echo json_encode($output);
	}



/*************************************************************
 * Administrator Active Vehicles
 */
	public function admin_active_vehicles(){
		$this->layout='administrator';
		$this->Session->write('PropertySelection',true);
	}
	public function admin_get_active_vehicles(){
		$this->autoRender=$this->layout=false;
		$output=array();
		$output = $this->CustomerPass->getActiveVehicle();
        echo json_encode($output);
	}
/*************************************************************
 * Administrator In-Active Vehicles
 */
	public function admin_inactive_vehicles(){
		$this->layout='administrator';
		$this->Session->write('PropertySelection',true);
	}
	public function admin_get_inactive_vehicles(){
		$this->autoRender=$this->layout=false;
		$output=array();
		$output = $this->CustomerPass->getInActiveVehicle();
        echo json_encode($output);
	}
/*************************************************************
 * Administrator guest Vehicles
 */
	public function admin_guest_vehicles(){
		$this->layout='administrator';
		$this->Session->write('PropertySelection',true);
		
	}
	public function admin_get_guest_vehicles(){
		$this->autoRender=$this->layout=false;
		$output=array();
		$output = $this->CustomerPass->getGuestVehicle();
        echo json_encode($output);
	}
/*************************************************************
 * Administrator Active Guest Vehicles
 */ 
	public function admin_active_guest_vehicles(){
		$this->layout='administrator';
		$this->Session->write('PropertySelection',true);
	}
	public function admin_get_active_guest_vehicles(){
		$this->autoRender=$this->layout=false;
		$output=array();
		$output = $this->CustomerPass->getActiveGuestVehicle();
        echo json_encode($output);
	}

    /*     * ********************************
     * SuperAdmin Add RFID TAG
     */

    public function admin_user_passes($userId) {
        $this->loadModel('User');
        if (!$this->User->exists($userId)) {
            throw new NotFoundException(__('Invalid user'));
        }
        if ($this->request->is('POST')) {

            $finalArray = array();
            $valueArray = array();
            $saveAllowed = true;
            $repeatedRfid = false;
            $rfidArray = array();
            $locationArray=array();
            $repeatedLocation=false;
            $save_array;
            $counter=0;
            foreach ($this->request->data['CustomerPass'] as $key => $value) {
                $k = explode('/', $key);
                if ($k[0] == 'RFID_tag_number') {
                    if ($k[2] != 'disabled') {
                        if (!preg_match("/^[a-zA-Z0-9]*$/", $value)) {
                            $this->CustomerPass->validationErrors['' . $k[0] . '/' . $k[1] . '/'] = array("RFID Tag " . $value . " is wrong.");
                            $saveAllowed = false;
                        } else {
                            $j = $this->CustomerPass->hasAny(array('RFID_tag_number' => $value));
                            if ($j) {
                                $this->CustomerPass->validationErrors['' . $k[0] . '/' . $k[1] . '/'] = array("RFID Tag " . $value . " already exists.");
                                $saveAllowed = false;
                            } else {
                                if(in_array($value, $rfidArray)){
                                    $this->CustomerPass->validationErrors['' . $k[0] . '/' . $k[1] . '/'] = array("RFID Tag " . $value . " repeated");
                                    $saveAllowed = false;
                                } else if ($value) {
                                    $rfidArray[$counter]=$value;
                                    $save_array[$counter]['id']=$k[1];
                                    $save_array[$counter][$k[0]]=$value;
                                    $this->CustomerPass->validationErrors['' . $k[0] . '/' . $k[1] . '/'] = array("RFID Tag " . $value . " can be set");
                                }
                            }
                        }
                    }
                }
                if ($k[0] == 'assigned_location') {
					if($value){
						if ($k[2] != 'disabled') {
							if (!preg_match("/^[a-zA-Z0-9 ]*$/", $value)) {
								$this->CustomerPass->validationErrors['' . $k[0] . '/' . $k[1] . '/'] = array("Assigned Location " . $value . " is wrong.");
								$saveAllowed = false;
							} else {
								$property_id=$this->CustomerPass->field('property_id',array('id'=>$k[1]));
								$j = $this->CustomerPass->hasAny(array('assigned_location' => $value,'property_id'=>$property_id));
								if ($j) {
									$this->CustomerPass->validationErrors['' . $k[0] . '/' . $k[1] . '/'] = array("Assigned Location " . $value . " already exists.");
									$saveAllowed = false;
								} else {
									if(in_array($value, $locationArray)){
										$this->CustomerPass->validationErrors['' . $k[0] . '/' . $k[1] . '/'] = array("Assigned Location " . $value . " repeated");
										$saveAllowed = false;
									}else if ($value) {
										$locationArray[$counter]=$value;
										$save_array[$counter-1]['id']=$k[1];
										$save_array[$counter-1][$k[0]]=$value;
										$this->CustomerPass->validationErrors['' . $k[0] . '/' . $k[1] . '/'] = array("Assigned Location " . $value . " can be set");
									}
								}
							}
						}
					}
                }
                $counter++;
            }
            if ($saveAllowed) {
                
                if ($this->CustomerPass->saveMany($save_array, array('validate' => false))) {
                    CakeLog::write('rfidTagsAssignedAdmin', '' . AuthComponent::user('username') . ' : RFID Tags Assigned to User ID: <a href="/admin/users/view_user_details/' . $userId . '">' . $userId . '</a>  by User: ' . AuthComponent::user('first_name') . ' ' . AuthComponent::user('last_name') . '');
                    $this->Session->setFlash('RFID Tags added Successfully', 'success');
                    $this->redirect(array('controller' => 'Users', 'action' => 'view_user_details', $userId));
                } else {
                    $this->Session->setFlash('RFID Tags could not be saved', 'error');
                    $this->redirect(array('action' => 'addRfid'));
                }
            } else {
                if ($repeatedRfid) {
                    $this->Session->setFlash('RFID Tags cannot be same', 'error');
                } else {
                    $this->Session->setFlash('RFID Tags could not be saved', 'error');
                }
            }
        }
        App::import('Controller', 'Properties');
        App::import('Controller', 'Packages');
        App::import('Controller', 'Passes');
        $pass = new PassesController;
        $package = new PackagesController;
        $property = new PropertiesController();
        $this->CustomerPass->recursive = -1;
        $array = $this->CustomerPass->find('all', array('conditions' => array('CustomerPass.user_id' => $userId),
													  'joins'=>array(
																	array(
																		  'table'=>'vehicles',
																		  'alias'=>'Vehicle',
																		  'type'=>'LEFT',
																		  'conditions'=>array(
																			'CustomerPass.vehicle_id=Vehicle.id'
																		  )
																	)
													  ),
													  'fields'=>array('CustomerPass.*','Vehicle.*')
													   
        ));
        for ($i = 0; $i < count($array); $i++) {
            if (is_null($array[$i]['CustomerPass']['membership_vaild_upto'])) {
                $array[$i]['CustomerPass']['membership_vaild_upto'] = "Package Not Bought";
            } else {
                $array[$i]['CustomerPass']['membership_vaild_upto'] = date("m/d/Y H:i:s", strtotime($array[$i]['CustomerPass']['membership_vaild_upto']));
            }
            if (is_null($array[$i]['CustomerPass']['pass_valid_upto'])) {
                $array[$i]['CustomerPass']['pass_valid_upto'] = "Pass Not Bought";
            } else {
                $array[$i]['CustomerPass']['pass_valid_upto'] = date("m/d/Y H:i:s", strtotime($array[$i]['CustomerPass']['pass_valid_upto']));
            }
            if (!is_null($array[$i]['CustomerPass']['property_id'])) {
                $array[$i]['CustomerPass']['property_id'] = $property->admin_getPropertyNameRFIF($array[$i]['CustomerPass']['property_id']);
            }
            if (!is_null($array[$i]['CustomerPass']['package_id'])) {
                $array[$i]['CustomerPass']['package_id'] = $package->getPackageName($array[$i]['CustomerPass']['package_id']);
            } else {
                $array[$i]['CustomerPass']['package_id'] = "No Package";
            }
            if (!is_null($array[$i]['CustomerPass']['pass_id'])) {
                $array[$i]['CustomerPass']['pass_id'] = $pass->admin_getPassName($array[$i]['CustomerPass']['pass_id']);
            } else {
                $array[$i]['CustomerPass']['pass_id'] = "No Pass";
            }
        }
		//debug($array);die;
        $this->User->recursive = -1;
        $userDetails = $this->User->find('first', array('conditions' => array('id' => $userId)));
        $this->request->data = $array;
        $this->set(compact('array', 'userDetails'));
        //debug($array);die;
    }
 /********************************************************************
 * CRON UPDATE
 */
	public function cron_update(){
		$this->layout=$this->autoRender=false;
		$dt = new DateTime();
        $currentDateTime= $dt->format('Y-m-d H:i:s');
        $result=$this->CustomerPass->updateAll(array('pass_expire'=>'1'),array('pass_valid_upto <'=>$currentDateTime));
	}
/*****************
*Pass expiration details TODAY for admins.
*/	  
		public function admin_view_expirations_today(){ 
		
		if($this->Auth->user('role_id')==2){
			$this->layout='administrator';
			$this->Session->write('PropertySelection',true);
			$this->CustomerPass->unBindModel( array('belongsTo' => array('Property','Transaction','Package','Pass')));
			$this->CustomerPass->recursive=0;
			$expiringTodayDetails = $this->CustomerPass->find('all',array('conditions'=>array(
																			'CustomerPass.pass_valid_upto >' => date('Y-m-d', strtotime("-1 day")),
																			'CustomerPass.pass_valid_upto <' => date('Y-m-d', strtotime("1 day")),
																			'CustomerPass.property_id'=>$this->Session->read('PropertyId'))));
			$this->loadModel('Pass');
			$this->loadModel('Package');
			$this->loadModel('Property');
			for($i=0;$i<count($expiringTodayDetails);$i++){
					$expiringTodayDetails[$i]['CustomerPass']['property_id']=$this->Property->givePropertyName($expiringTodayDetails[$i]['CustomerPass']['property_id']);
					$expiringTodayDetails[$i]['CustomerPass']['pass_id']=$this->Pass->givePassName($expiringTodayDetails[$i]['CustomerPass']['pass_id']);
					if(!is_null($expiringTodayDetails[$i]['CustomerPass']['package_id'])){
						$expiringTodayDetails[$i]['CustomerPass']['package_id']=$this->Package->givePackageName($expiringTodayDetails[$i]['CustomerPass']['package_id']);
					}else{
						$expiringTodayDetails[$i]['CustomerPass']['package_id']="Permit Not Bought";
						$expiringTodayDetails[$i]['CustomerPass']['membership_vaild_upto']="NA";
					}
			}
			$expiringToday=count($expiringTodayDetails);
			$this->set(compact('expiringToday','expiringTodayDetails'));
		}elseif($this->Auth->user('role_id')==1){
			$this->CustomerPass->unBindModel( array('belongsTo' => array('Property','Transaction','Package','Pass')));
			$this->CustomerPass->recursive=0;
			$expiringTodayDetails = $this->CustomerPass->find('all',array('conditions'=>array( 
																				'CustomerPass.pass_valid_upto >' => date('Y-m-d', strtotime("-1 day")),
																				'CustomerPass.pass_valid_upto <' => date('Y-m-d', strtotime("1 day")))));
			$this->loadModel('Pass');
			$this->loadModel('Package');
			$this->loadModel('Property');
			for($i=0;$i<count($expiringTodayDetails);$i++){
					$expiringTodayDetails[$i]['CustomerPass']['property_id']=$this->Property->givePropertyName($expiringTodayDetails[$i]['CustomerPass']['property_id']);
					$expiringTodayDetails[$i]['CustomerPass']['pass_id']=$this->Pass->givePassName($expiringTodayDetails[$i]['CustomerPass']['pass_id']);
					if(!is_null($expiringTodayDetails[$i]['CustomerPass']['package_id'])){
						$expiringTodayDetails[$i]['CustomerPass']['package_id']=$this->Package->givePackageName($expiringTodayDetails[$i]['CustomerPass']['package_id']);
					}else{
						$expiringTodayDetails[$i]['CustomerPass']['package_id']="Permit Not Bought";
						$expiringTodayDetails[$i]['CustomerPass']['membership_vaild_upto']="NA";
					}
			}
			$expiringToday=count($expiringTodayDetails);
			$this->set(compact('expiringToday','expiringTodayDetails'));
		}
	 }
/*****************
*Pass expiration details LAST MONTH for admins.
*/		 
	public function admin_view_expirations_last(){ 
		
		if($this->Auth->user('role_id')==2){
			$this->layout='administrator';
			$this->Session->write('PropertySelection',true);
			$this->CustomerPass->unBindModel( array('belongsTo' => array('Property','Transaction','Package','Pass')));
			$this->CustomerPass->recursive=0;
			$expiredLastMonthDetails = $this->CustomerPass->find('all',array('conditions'=>array(
																					'CustomerPass.pass_valid_upto >' => date('Y-m-d', strtotime("-1 day")),  
																					'CustomerPass.pass_valid_upto <' => date('Y-m-d', strtotime("1 day")), 
																					'CustomerPass.property_id'=>$this->Session->read('PropertyId'))));
			$this->loadModel('Pass');
			$this->loadModel('Package');
			$this->loadModel('Property');
			for($i=0;$i<count($expiredLastMonthDetails);$i++){
					$expiredLastMonthDetails[$i]['CustomerPass']['property_id']=$this->Property->givePropertyName($expiredLastMonthDetails[$i]['CustomerPass']['property_id']);
					$expiredLastMonthDetails[$i]['CustomerPass']['pass_id']=$this->Pass->givePassName($expiredLastMonthDetails[$i]['CustomerPass']['pass_id']);
					if(!is_null($expiredLastMonthDetails[$i]['CustomerPass']['package_id'])){
						$expiredLastMonthDetails[$i]['CustomerPass']['package_id']=$this->Package->givePackageName($expiredLastMonthDetails[$i]['CustomerPass']['package_id']);
					}else{
						$expiredLastMonthDetails[$i]['CustomerPass']['package_id']="Permit Not Bought";
						$expiredLastMonthDetails[$i]['CustomerPass']['membership_vaild_upto']="NA";
					}
			}
			$expiredLastMonth=count($expiredLastMonthDetails);
			$this->set(compact('expiredLastMonth','expiredLastMonthDetails'));
			
			
		}elseif($this->Auth->user('role_id')==1){
			$this->CustomerPass->unBindModel( array('belongsTo' => array('Property','Transaction','Package','Pass')));
			$this->CustomerPass->recursive= 0;
			$expiredLastMonthDetails = $this->CustomerPass->find('all',array('conditions'=>array( 
																					'CustomerPass.pass_valid_upto >' => date('Y-m-d', strtotime("-1 day")),  
																					'CustomerPass.pass_valid_upto <' => date('Y-m-d', strtotime("1 day")))));
			$this->loadModel('Pass');
			$this->loadModel('Package');
			$this->loadModel('Property');
			for($i=0;$i<count($expiredLastMonthDetails);$i++){
					$expiredLastMonthDetails[$i]['CustomerPass']['property_id']=$this->Property->givePropertyName($expiredLastMonthDetails[$i]['CustomerPass']['property_id']);
					$expiredLastMonthDetails[$i]['CustomerPass']['pass_id']=$this->Pass->givePassName($expiredLastMonthDetails[$i]['CustomerPass']['pass_id']);
					if(!is_null($expiredLastMonthDetails[$i]['CustomerPass']['package_id'])){
						$expiredLastMonthDetails[$i]['CustomerPass']['package_id']=$this->Package->givePackageName($expiredLastMonthDetails[$i]['CustomerPass']['package_id']);
					}else{
						$expiredLastMonthDetails[$i]['CustomerPass']['package_id']="Permit Not Bought";
						$expiredLastMonthDetails[$i]['CustomerPass']['membership_vaild_upto']="NA";
					}
			}
			$expiredLastMonth=count($expiredLastMonthDetails);
			$this->set(compact('expiredLastMonth','expiredLastMonthDetails'));
			}
	 }
/*****************
*Pass expiration details NEXT MONTH for admins.
*/	 
	public function admin_view_expirations_next(){ 
		if($this->Auth->user('role_id')==2){
			$this->layout='administrator';
			$this->Session->write('PropertySelection',true);
			$this->CustomerPass->unBindModel( array('belongsTo' => array('Property','Transaction','Package','Pass')));
			$this->CustomerPass->recursive= 0;
			$expiringNextMonthDetails = $this->CustomerPass->find('all',array('conditions'=>array('CustomerPass.pass_valid_upto >=' => date('Y-m-d', strtotime("1 day")),  
																								  'CustomerPass.pass_valid_upto <' => date('Y-m-d', strtotime("1 month")), 
																								  'CustomerPass.property_id'=>$this->Session->read('PropertyId'))));
			$this->loadModel('Pass');
			$this->loadModel('Package');
			$this->loadModel('Property');
			for($i=0;$i<count($expiringNextMonthDetails);$i++){
					$expiringNextMonthDetails[$i]['CustomerPass']['property_id']=$this->Property->givePropertyName($expiringNextMonthDetails[$i]['CustomerPass']['property_id']);
					$expiringNextMonthDetails[$i]['CustomerPass']['pass_id']=$this->Pass->givePassName($expiringNextMonthDetails[$i]['CustomerPass']['pass_id']);
					if(!is_null($expiringNextMonthDetails[$i]['CustomerPass']['package_id'])){
						$expiringNextMonthDetails[$i]['CustomerPass']['package_id']=$this->Package->givePackageName($expiringNextMonthDetails[$i]['CustomerPass']['package_id']);
					}else{
						$expiringNextMonthDetails[$i]['CustomerPass']['package_id']="Permit Not Bought";
						$expiringNextMonthDetails[$i]['CustomerPass']['membership_vaild_upto']="NA";
					}
			}
			$expiringNextMonth=count($expiringNextMonthDetails);
			$this->set(compact('expiringNextMonth','expiringNextMonthDetails'));
		}elseif($this->Auth->user('role_id')==1){
			$this->CustomerPass->unBindModel( array('belongsTo' => array('Property','Transaction','Package','Pass')));
			$this->CustomerPass->recursive= 0;
			$expiringNextMonthDetails = $this->CustomerPass->find('all',array('conditions'=>array('CustomerPass.pass_valid_upto >=' => date('Y-m-d', strtotime("1 day")),  
																								  'CustomerPass.pass_valid_upto <' => date('Y-m-d', strtotime("1 month")))));
			$this->loadModel('Pass');
			$this->loadModel('Package');
			$this->loadModel('Property');
			for($i=0;$i<count($expiringNextMonthDetails);$i++){
					$expiringNextMonthDetails[$i]['CustomerPass']['property_id']=$this->Property->givePropertyName($expiringNextMonthDetails[$i]['CustomerPass']['property_id']);
					$expiringNextMonthDetails[$i]['CustomerPass']['pass_id']=$this->Pass->givePassName($expiringNextMonthDetails[$i]['CustomerPass']['pass_id']);
					if(!is_null($expiringNextMonthDetails[$i]['CustomerPass']['package_id'])){
						$expiringNextMonthDetails[$i]['CustomerPass']['package_id']=$this->Package->givePackageName($expiringNextMonthDetails[$i]['CustomerPass']['package_id']);
					}else{
						$expiringNextMonthDetails[$i]['CustomerPass']['package_id']="Permit Not Bought";
						$expiringNextMonthDetails[$i]['CustomerPass']['membership_vaild_upto']="NA";
					}
			}
			$expiringNextMonth=count($expiringNextMonthDetails);
			$this->set(compact('expiringNextMonth','expiringNextMonthDetails'));
		}
	 }
/*****************
*Pass expiration details TODAY for property managers.
*/	 	
		
	public function manager_view_expirations_today(){ 
			$this->layout='manager';
			$this->CustomerPass->unBindModel( array('belongsTo' => array('Property','Transaction','Package','Pass')));
			$this->CustomerPass->recursive=0;
			$expiringTodayDetails = $this->CustomerPass->find('all',array('conditions'=>array( 
																			'CustomerPass.pass_valid_upto >' => date('Y-m-d', strtotime("-1 day")),
																			'CustomerPass.pass_valid_upto <' => date('Y-m-d', strtotime("1 day")),
																			'CustomerPass.property_id'=>$this->Session->read('PropertyId'))));
			$this->loadModel('Pass');
			$this->loadModel('Package');
			$this->loadModel('Property');
			for($i=0;$i<count($expiringTodayDetails);$i++){
					$expiringTodayDetails[$i]['CustomerPass']['property_id']=$this->Property->givePropertyName($expiringTodayDetails[$i]['CustomerPass']['property_id']);
					$expiringTodayDetails[$i]['CustomerPass']['pass_id']=$this->Pass->givePassName($expiringTodayDetails[$i]['CustomerPass']['pass_id']);
					if(!is_null($expiringTodayDetails[$i]['CustomerPass']['package_id'])){
						$expiringTodayDetails[$i]['CustomerPass']['package_id']=$this->Package->givePackageName($expiringTodayDetails[$i]['CustomerPass']['package_id']);
					}else{
						$expiringTodayDetails[$i]['CustomerPass']['package_id']="Permit Not Bought";
						$expiringTodayDetails[$i]['CustomerPass']['membership_vaild_upto']="NA";
					}
			}
			$expiringToday=count($expiringTodayDetails);
			$this->set(compact('expiringToday','expiringTodayDetails'));
		}
/*****************
*Pass expiration details LAST MONTH for property managers.
*/		
	public function manager_view_expirations_last(){ 
			$this->layout='manager';
			$this->CustomerPass->unBindModel( array('belongsTo' => array('Property','Transaction','Package','Pass')));
			$this->CustomerPass->recursive=0;
			$expiredLastMonthDetails = $this->CustomerPass->find('all',array('conditions'=>array(
																					'CustomerPass.pass_valid_upto >' => date('Y-m-d', strtotime("-1 day")),  
																					'CustomerPass.pass_valid_upto <' => date('Y-m-d', strtotime("1 day")), 
																					'CustomerPass.property_id'=>$this->Session->read('PropertyId'))));
			$this->loadModel('Pass');
			$this->loadModel('Package');
			$this->loadModel('Property');
			for($i=0;$i<count($expiredLastMonthDetails);$i++){
					$expiredLastMonthDetails[$i]['CustomerPass']['property_id']=$this->Property->givePropertyName($expiredLastMonthDetails[$i]['CustomerPass']['property_id']);
					$expiredLastMonthDetails[$i]['CustomerPass']['pass_id']=$this->Pass->givePassName($expiredLastMonthDetails[$i]['CustomerPass']['pass_id']);
					if(!is_null($expiredLastMonthDetails[$i]['CustomerPass']['package_id'])){
						$expiredLastMonthDetails[$i]['CustomerPass']['package_id']=$this->Package->givePackageName($expiredLastMonthDetails[$i]['CustomerPass']['package_id']);
					}else{
						$expiredLastMonthDetails[$i]['CustomerPass']['package_id']="Permit Not Bought";
						$expiredLastMonthDetails[$i]['CustomerPass']['membership_vaild_upto']="NA";
					}
			}
			$expiredLastMonth=count($expiredLastMonthDetails);
			$this->set(compact('expiredLastMonth','expiredLastMonthDetails'));
		} 
/*****************
*Pass expiration details NEXT MONTH for property managers.
*/			
		public function manager_view_expirations_next(){ 
			$this->layout='manager';
			$this->CustomerPass->unBindModel( array('belongsTo' => array('Property','Transaction','Package','Pass')));
			$this->CustomerPass->recursive= 0;
			$expiringNextMonthDetails = $this->CustomerPass->find('all',array('conditions'=>array('CustomerPass.pass_valid_upto >=' => date('Y-m-d', strtotime("1 day")),  
																								  'CustomerPass.pass_valid_upto <' => date('Y-m-d', strtotime("1 month")), 
																								  'CustomerPass.property_id'=>$this->Session->read('PropertyId'))));
			$this->loadModel('Pass');
			$this->loadModel('Package');
			$this->loadModel('Property');
			for($i=0;$i<count($expiringNextMonthDetails);$i++){
					$expiringNextMonthDetails[$i]['CustomerPass']['property_id']=$this->Property->givePropertyName($expiringNextMonthDetails[$i]['CustomerPass']['property_id']);
					$expiringNextMonthDetails[$i]['CustomerPass']['pass_id']=$this->Pass->givePassName($expiringNextMonthDetails[$i]['CustomerPass']['pass_id']);
					if(!is_null($expiringNextMonthDetails[$i]['CustomerPass']['package_id'])){
						$expiringNextMonthDetails[$i]['CustomerPass']['package_id']=$this->Package->givePackageName($expiringNextMonthDetails[$i]['CustomerPass']['package_id']);
					}else{
						$expiringNextMonthDetails[$i]['CustomerPass']['package_id']="Permit Not Bought";
						$expiringNextMonthDetails[$i]['CustomerPass']['membership_vaild_upto']="NA";
					}
			}
			$expiringNextMonth=count($expiringNextMonthDetails);
			$this->set(compact('expiringNextMonth','expiringNextMonthDetails'));
		}
/***********************************************************************
 * Super Admin Property Wise Passes Details
 */
		public function admin_property_wise_passes(){
			$this->loadModel('Property');
			$property_list=$this->Property->find('list',array('fields'=>'name'));
			$id=array_keys($property_list);
			$selectedId=$id[0];
			$this->loadModel('Pass');
			$passes_list=$this->Pass->find('list',array('fields'=>'name','conditions'=>array('property_id'=>$selectedId)));
			$pass_id=array_keys($property_list);
			$selectedPassId=$pass_id[0];
			$this->set(compact('property_list','selectedId','passes_list','selectedPassId'));
		}
		public function admin_get_passes_detail($pass_id,$property_id){
			$this->autoRender=false;
			$response=array();
			$dt = new DateTime();
			$currentDateTime= $dt->format('Y-m-d H:i:s');
			$this->CustomerPass->recursive=-1;
			$totalPass=$this->CustomerPass->find('count',array('conditions'=>array('pass_id'=>$pass_id)));
			$activePass=$this->CustomerPass->find('count',array('conditions'=>array('membership_vaild_upto >'=>$currentDateTime,'pass_valid_upto >'=>$currentDateTime,'property_id'=>$property_id,'pass_id'=>$pass_id)));
			$validPassPackageNotBought=$this->CustomerPass->find('count',array('conditions'=>array('membership_vaild_upto'=>NULL,'pass_valid_upto >'=>$currentDateTime,'property_id'=>$property_id,'pass_id'=>$pass_id)));
			$validPassPackageExpired=$this->CustomerPass->find('count',array('conditions'=>array('membership_vaild_upto <'=>$currentDateTime,'pass_valid_upto >'=>$currentDateTime,'property_id'=>$property_id,'pass_id'=>$pass_id)));
			$expiredPass=$this->CustomerPass->find('count',array('conditions'=>array('pass_valid_upto <'=>$currentDateTime,'property_id'=>$property_id,'pass_id'=>$pass_id)));
			$activeVehicles=$this->CustomerPass->find('count',array('conditions'=>array('membership_vaild_upto >'=>$currentDateTime,'pass_valid_upto >'=>$currentDateTime,'property_id'=>$property_id,'pass_id'=>$pass_id,'vehicle_id !='=>NULL)));
			$vehicleNotAssigned=$this->CustomerPass->find('count',array('conditions'=>array('membership_vaild_upto >'=>$currentDateTime,'pass_valid_upto >'=>$currentDateTime,'property_id'=>$property_id,'pass_id'=>$pass_id,'vehicle_id'=>NULL)));
			$expiringToday = $this->CustomerPass->find('count',array('conditions'=>array( 
																			'CustomerPass.pass_valid_upto >' => date('Y-m-d', strtotime("-1 day")),
																			'CustomerPass.pass_valid_upto <' => date('Y-m-d', strtotime("1 day")),
																			'CustomerPass.pass_id'=>$pass_id)));
			$expiredLastMonth = $this->CustomerPass->find('count',array('conditions'=>array(
																					'CustomerPass.pass_valid_upto <' => date('Y-m-d', strtotime("-1 day")),  
																					'CustomerPass.pass_valid_upto >' => date('Y-m-d', strtotime("-1 month")), 
																					'CustomerPass.pass_id'=>$pass_id)));
			$expiringNextMonth = $this->CustomerPass->find('count',array('conditions'=>array('CustomerPass.pass_valid_upto >=' => date('Y-m-d', strtotime("1 day")),  
																								  'CustomerPass.pass_valid_upto <' => date('Y-m-d', strtotime("1 month")), 
																								  'CustomerPass.pass_id'=>$pass_id)));
			$this->loadModel('Pass');
			$totalPackageAmount=$totalPassCost=$totalPassDeposit=$totalPassCost=$totalPassRenewal=0;
			$packageId=$this->Pass->field('package_id',array('id'=>$pass_id));
			$this->loadModel('Transaction');
			$this->Transaction->virtualFields['total_amount']='SUM(Transaction.amount)';
			$this->Transaction->recursive=-1;
			$totalTransactionAmount=$this->Transaction->find('all',array('fields'=>array('total_amount'),'conditions'=>array("date_time <= '2014-11-21 01:04:25'",'comments'=>NULL)));
			$totalAmountTransaction=0;
			if(!is_null($totalTransactionAmount[0]['Transaction']['total_amount'])){
						$totalAmountTransaction=$totalTransactionAmount[0]['Transaction']['total_amount'];
			}	
			if(!is_null($packageId)){
				$this->loadModel('Package');
				$isGuest=$this->Package->field('is_guest',array('id'=>$packageId));
				if($isGuest){
					$this->loadModel('UserGuestPass');
					$this->UserGuestPass->virtualFields['totalAmount']='SUM(UserGuestPass.amount)';
					$this->UserGuestPass->recursive=-1;
					$totalGuestAmount=$this->UserGuestPass->find('all',array('fields'=>array('totalAmount'),'conditions'=>array('property_id'=>$property_id)));
					if(!is_null($totalGuestAmount[0]['UserGuestPass']['totalAmount'])){
						$totalPackageAmount=$totalGuestAmount[0]['UserGuestPass']['totalAmount'];
					}
				}else{
					$this->CustomerPass->virtualFields['totalAmount']='SUM(CustomerPass.package_cost)';
					$this->CustomerPass->recursive=-1;
					$amount=$this->CustomerPass->find('all',array('fields'=>array('totalAmount'),'conditions'=>array('pass_id'=>$pass_id)));
					$packageAmount1=$packageAmount2=0;
					if(!is_null($amount[0]['CustomerPass']['totalAmount'])){
						$packageAmount1=$amount[0]['CustomerPass']['totalAmount'];
					}
					$this->loadModel('Transaction');
					$this->Transaction->virtualFields['package_cost']='SUM(Transaction.amount)';
					$this->Transaction->recursive=-1;
					$packageBoughtAmount=$this->Transaction->find('all',array('fields'=>array('package_cost'),'conditions'=>array('pass_id' =>$pass_id,'comments'=>'passActivated')));			
					if(!is_null($packageBoughtAmount[0]['Transaction']['package_cost'])){
							$packageAmount2=$packageBoughtAmount[0]['Transaction']['package_cost'];
					}
					$totalPackageAmount=$packageAmount1+$packageAmount2;
				}
			
			}
			
			$this->CustomerPass->virtualFields['passCost']='SUM(CustomerPass.pass_cost)';
			$this->CustomerPass->virtualFields['passDeposit']='SUM(CustomerPass.pass_deposit)';
			$this->CustomerPass->recursive=-1;
			$amount=$this->CustomerPass->find('all',array('fields'=>array('passCost','passDeposit'),'conditions'=>array('pass_id'=>$pass_id)));
			if(!is_null($amount[0]['CustomerPass']['passCost'])){
						$totalPassCost=$amount[0]['CustomerPass']['passCost'];
			}
			if(!is_null($amount[0]['CustomerPass']['passDeposit'])){
						$totalPassDeposit=$amount[0]['CustomerPass']['passDeposit'];
			}
			$this->Transaction->virtualFields['passRenewal']='SUM(Transaction.amount)';
			$this->Transaction->recursive=-1;
			$passRenewalAmount=$this->Transaction->find('all',array('fields'=>array('passRenewal'),'conditions'=>array('pass_id' =>$pass_id,'comments'=>'passRenewed')));			
			if(!is_null($passRenewalAmount[0]['Transaction']['passRenewal'])){
						$totalPassRenewal=$passRenewalAmount[0]['Transaction']['passRenewal'];
			}
			$totalPassAmount=$totalPassCost+$totalPassDeposit+$totalPassRenewal;
			$response['totalAmountTransaction']=$totalAmountTransaction;
			$response['totalPackageAmount']=$totalPackageAmount;
			$response['totalPassCost']=$totalPassCost;
			$response['totalPassDeposit']=$totalPassDeposit;
			$response['totalPassRenewal']=$totalPassRenewal;
			$response['totalPassAmount']=$totalPassAmount;
			$response['totalPass']=$totalPass;
			$response['activePass']=$activePass;
			$response['validPassPackageNotBought']=$validPassPackageNotBought;
			$response['validPassPackageExpired']=$validPassPackageExpired;
			$response['expiredPass']=$expiredPass;
			$response['activeVehicles']=$activeVehicles;
			$response['vehicleNotAssigned']=$vehicleNotAssigned;
			$response['expiringToday']=$expiringToday;
			$response['expiredLastMonth']=$expiredLastMonth;
			$response['expiringNextMonth']=$expiringNextMonth;
			$this->loadModel('Pass');
			$isGuest=$this->Pass->Package->field('is_guest',array('pass_id'=>$pass_id));
			if($isGuest){
				$response['isGuest']=true;
			}else{
				$response['isGuest']=false;
			}	
			$this->loadModel('Pass');
			$passName=$this->Pass->givePassName($pass_id);
			$response['passName']=$passName;
			echo json_encode($response);
		}
		public function admin_passes_property_wise($pass_id)
		{
			$this->autoRender=$this->layout=false;
			$output=array();
			$output = $this->CustomerPass->get_data_passwise($pass_id);
			echo json_encode($output);
		}
/***********************************************************************
 * Super Admin Property Wise Passes Details In Given Interval
 */
		public function admin_property_wise_pass_details(){
			$this->loadModel('Property');
			$property_list=$this->Property->find('list',array('fields'=>'name'));
			$id=array_keys($property_list);
			$selectedId=$id[0];
			$this->loadModel('Pass');
			$passes_list=$this->Pass->find('list',array('fields'=>'name','conditions'=>array('property_id'=>$selectedId)));
			$pass_id=array_keys($property_list);
			$selectedPassId=$pass_id[0];
			$this->set(compact('property_list','selectedId','passes_list','selectedPassId'));
		}
		public function admin_property_wise_pass_details_pa(){
			$this->layout='administrator';
			$this->loadModel('Property');
			$property_list=CakeSession::read('AdminAllProperty');
			$id=array_keys($property_list);
			$selectedId=$id[0];
			$this->loadModel('Pass');
			$passes_list=$this->Pass->find('list',array('fields'=>'name','conditions'=>array('property_id'=>$selectedId)));
			$pass_id=array_keys($property_list);
			$selectedPassId=$pass_id[0];
			$this->set(compact('property_list','selectedId','passes_list','selectedPassId'));
			$this->render('admin_property_wise_pass_details');	
		}
		public function admin_get_passes_detail_interval(){
			$this->autoRender=false;
			$toDate=date('Y-m-d',strtotime($this->request->query['toDate']));
			$fromDate=date('Y-m-d',strtotime($this->request->query['fromDate']));
			$conditions='';
			$conditionsCount='';
			if($toDate==$fromDate){
				$toDate=$fromDate.' 23:59:59';
			}else{
				$toDate=$toDate.' 23:59:59';
			}
			$pass_id=$this->request->query['pass'];
			$conditions= " cp.pass_id= ".$pass_id." AND cp.created BETWEEN '".$fromDate."' AND '".$toDate."' ";
			$conditionsCount=array('pass_id'=>$pass_id,'created BETWEEN ? AND ?' => array($fromDate,$toDate));
			$response=array();
			$this->CustomerPass->recursive=-1;
			$totalPass=$this->CustomerPass->find('count',array('conditions'=>$conditionsCount));
			$response['totalPass']=$totalPass;
			$response['condition']=$conditions;
			$this->loadModel('Pass');
			$passName=$this->Pass->givePassName($pass_id);
			$response['passName']=$passName;
			echo json_encode($response);    
			
		}
		public function admin_passes_interval()
		{
			$this->autoRender=$this->layout=false;
			$part1=array_keys($this->request->params['named']);
			$part2=array_values($this->request->params['named']);
			$condition=$part1[0].":".$part2[0];
			$output=array();
			$output = $this->CustomerPass->get_data_passwise_interval(json_decode($condition));
			echo json_encode($output);
		}
/*************************************************
 * 
 */
 public function admin_customer_view($user_id) {
	 
		$this->loadModel('PropertyUser');
		$propertyId=$this->PropertyUser->field('property_id',array('user_id'=>$user_id));
		if($this->Auth->user('role_id')!=1){	
			$this->layout='admin_customer';
		}
		$this->loadModel('User');
		$this->User->recursive=-1;
		$userDetail=$this->User->find('first',array('conditions'=>array('id'=>$user_id)));
		$this->loadModel('Property');
		$this->Property->recursive=-1;
		$propertyName=$this->Property->find('first',array('fields'=>array('name'),'conditions'=>array('id'=>$propertyId)));

		$userFullName=$userDetail['User']['first_name'].' '.$userDetail['User']['last_name'];
		$userName=$userDetail['User']['username'];
        $customerActivePass=$customerValidPass=$customerRenewableExpiredPass=$customerNonRenewableExpiredPass=$remainingPasses=$noCurrentPass=array();
        if($userDetail['User']['role_id']==4){
			$dt = new DateTime();
			$currentDateTime= $dt->format('Y-m-d H:i:s');
			$this->CustomerPass->recursive=-1;
			$customerActivePass=$this->CustomerPass->find('all',array('conditions'=>array('user_id'=>$user_id,'AND'=>array('membership_vaild_upto >'=>$currentDateTime,'pass_valid_upto >'=>$currentDateTime))));
			$this->CustomerPass->recursive=-1;
			$customerValidPass=$this->CustomerPass->find('all',array('conditions'=>array('user_id'=>$user_id,'AND'=>array('OR'=>array('membership_vaild_upto'=>null,'membership_vaild_upto <'=>$currentDateTime),'pass_valid_upto >'=>$currentDateTime))));
			$this->CustomerPass->recursive=-1;
			$customerRenewableExpiredPass=$this->CustomerPass->find('all',array('conditions'=>array('pass_id in (select id from passes where property_id = '.$propertyId.' and is_fixed_duration = 0) and user_id ='.$user_id.' and pass_valid_upto <\''.$currentDateTime.'\'')));
			$this->CustomerPass->recursive=-1;
			$customerNonRenewableExpiredPass=$this->CustomerPass->find('all',array('conditions'=>array('pass_id in (select id from passes where property_id = '.$propertyId.' and is_fixed_duration = 1) and user_id ='.$user_id.' and pass_valid_upto <\''.$currentDateTime.'\'')));
			$this->loadModel('Pass');
			$this->loadModel('Property');
			$couponSelection=$this->Property->field('coupon_selection',array('id'=>$propertyId));
			$remainingPasses=array();
			$remainingPasses=$this->Pass->getRemainingPass($propertyId,$user_id);
			$noCurrentPass=$this->CustomerPass->hasAny(array('user_id'=>$user_id));
        }
		$this->set(array('customerActivePass'=>$customerActivePass,
            'customerValidPass'=>$customerValidPass,
            'customerRenewableExpiredPass'=>$customerRenewableExpiredPass,
            'customerNonRenewableExpiredPass'=>$customerNonRenewableExpiredPass,
			'remainingPasses'=>$remainingPasses,
			'noCurrentPass'=>$noCurrentPass,
			'userDetail'=>$userDetail,
			'propertyName'=>$propertyName,
			'propertyIdAdmin'=>$propertyId));
	
	}
/***************************************************************
 * Admin get Passes Details
*/
public function admin_getCustomerPassDetails($id=null,$userId,$propertyIdAdmin){
		//debug();die;
		$this->loadModel('Property');
		$this->Property->recursive=-1;
		$propertyName=$this->Property->find('first',array('fields'=>array('name'),'conditions'=>array('id'=>$propertyIdAdmin)));
		$this->loadModel('User');
		$this->User->recursive=-1;
     	$userDetails=$this->User->find('first',array('conditions'=>array('id'=>$userId)));
		if (!$this->CustomerPass->exists($id)) {
            throw new NotFoundException(__('Invalid DATA NO PASS'));
        }else{
			$userID=$this->CustomerPass->field('user_id',array('id'=>$id));
			if($userID != $userID){
				 throw new NotFoundException(__('Invalid DATA 1'));
			}
		}
        $packageDate=$this->CustomerPass->field('membership_vaild_upto',array('id'=>$id));
        if(!is_null($packageDate)){
			$dt = new DateTime();
			$currentDateTime= $dt->format('Y-m-d H:i:s');
			if($packageDate>$currentDateTime){
				$this->Session->setFlash('This pass is already active','warning');
				$this->redirect(array('action'=>'view'));
			}
		}
		$passId=$this->CustomerPass->field('pass_id',array('id'=>$id));
		$this->loadModel('Pass');
		$pass=new Pass();
		$permitId=$pass->field('package_id',array('id'=>$passId));
		if($permitId==null){
			$this->Session->write('PassToBuy',$passId);		
			$this->redirect(array('controller'=>'Packages','action'=>'selectPermitToBuy'));
		}else{
			$this->loadModel('Package');
			$isGuest=$this->Package->field('is_guest',array('id'=>$permitId));
			if($isGuest==1)
			{
			  $this->redirect(array('controller'=>'Vehicles','action'=>'setGuestPackageOptions',$id,$passId,$permitId,$userId,$propertyIdAdmin));
			}
			else{
				$this->loadModel('Package');
				$permitCost=$this->Package->field('cost',array('id'=>$permitId));
				if($permitCost==0){
								$arr['details']['pass_Id']=$passId;
								$arr['details']['customer_pass_Id']=$id;
								$arr['details']['permit_Id']=$permitId;
								$arraySerialize=serialize($arr);
								$dt = new DateTime();
								$currentDateTime= $dt->format('Y-m-d H:i:s');
								$transactionArray['Transaction']['paypal_tranc_id']='No Payment Pass Activation';
								$transactionArray['Transaction']['user_id']=$userId;
								$transactionArray['Transaction']['date_time']=$currentDateTime;
								$transactionArray['Transaction']['amount']=0;
								$transactionArray['Transaction']['result']='success';
								$transactionArray['Transaction']['pass_id']=$passId;
								$transactionArray['Transaction']['transaction_array']=$arraySerialize;
								$transactionArray['Transaction']['first_name']="AdminActivate";
								$transactionArray['Transaction']['last_name']="UserPass";
								$transactionArray['Transaction']['comments'] = "passActivated";	
								$this->loadModel('Transaction');
								$this->Transaction->create();
								if($this->Transaction->save($transactionArray,false)){
											$this->Package->recursive=-1;
											$result=$this->Package->find('first',array('conditions'=>array('id'=>$permitId)));
											//debug($result);
											if($result['Package']['is_fixed_duration']==0){
													switch ($result['Package']['duration_type']) {
														case "Hour":
															$date = new DateTime();
															$k=(int)$result['Package']['duration'];
															$date->add(new DateInterval('PT'.$k.'H'));
															$result['Package']['expiration_date']=date_format($date, 'Y-m-d H:i:s');
															break;
														case "Day":
															$date = new DateTime();
															$k=$result['Package']['duration'];
															$date->add(new DateInterval('P'.$k.'D'));
															$result['Package']['expiration_date']=date_format($date, 'Y-m-d H:i:s');
															break;
														case "Week":
															$date = new DateTime();
															$k=(int)$result['Package']['duration']*7;
															$date->add(new DateInterval('P'.$k.'D'));
															$result['Package']['expiration_date']=date_format($date, 'Y-m-d H:i:s');
															break;
														case "Month":
															$date = new DateTime();
															$k=(int)$result['Package']['duration'];
															$date->add(new DateInterval('P'.$k.'M'));
															$result['Package']['expiration_date']=date_format($date, 'Y-m-d H:i:s');
															break;
														case "Year":
															$date = new DateTime();
															$k=(int)$result['Package']['duration'];
															$date->add(new DateInterval('P'.$k.'Y'));
															$result['Package']['expiration_date']=date_format($date, 'Y-m-d H:i:s');
															break;
													}
											}
											$this->loadModel('CustomerPass');
											$customerPassArray['CustomerPass']['transaction_id']=$this->Transaction->getLastInsertId();
											$customerPassArray['CustomerPass']['membership_vaild_upto']=$result['Package']['expiration_date'];
											$customerPassArray['CustomerPass']['id']=$id;
											$customerPassArray['CustomerPass']['package_id']= $permitId;
											$previousId=$this->CustomerPass->field('vehicle_id',array('id'=>$id));
											if($this->CustomerPass->save($customerPassArray,false)){
												$this->loadModel('Package');
												$packageName=$this->Package->givePackageName($permitId);
												CakeLog::write('validPassActivatedAdmin', ''.AuthComponent::user('username').' : Valid Pass Activated Customer Pass ID: <a href="/admin/CustomerPasses/edit/'.$id.'">'.$id.'</a> Package ID: '.$permitId.', Package Name: '.$packageName.' Activated by User: '.AuthComponent::user('first_name').' '.AuthComponent::user('last_name').' In Property: '.$propertyName['Property']['name'].', Cost Of Activation is ZERO');
												$this->Session->setFlash('Pass activated successfully','success');
												$this->redirect(array('controller'=>'Vehicles','action'=>'add',$id,$userId,$propertyIdAdmin));
											}else{
												$this->Session->setFlash('Pass could not be activated','error');
											}
								}else{
									$this->Session->setFlash('Pass could not be activated','error');	
							    }	
				}else{
					$this->redirect(array('controller'=>'Transactions','action'=>'activatePass',$id,$passId,$permitId,$userId,$propertyIdAdmin));
				}		
				
			}
		}
  }
 /**************************************************************
  * Admin Renew Pass Updated ON: 1st Feb 2016
  */	 
    public function admin_renewPass($customerPassId = null, $userId, $propertyIdAdmin,$advance=false) {
        $this->loadModel('Property');
        $this->Property->recursive = -1;
        $propertyName = $this->Property->find('first', array('fields' => array('name'), 'conditions' => array('id' => $propertyIdAdmin)));
        $this->loadModel('User');
        $this->User->recursive = -1;
        $userDetails = $this->User->find('first', array('conditions' => array('id' => $userId)));
        if (!$this->CustomerPass->exists($customerPassId)) {
            throw new NotFoundException(__('Invalid customer pass'));
        }
        $passId = $this->CustomerPass->field('pass_id', array('id' => $customerPassId));
        $this->loadModel('Pass');
        $this->Pass->recursive = -1;
        $passDetails = $this->Pass->find('first', array('conditions' => array('id' => $passId)));
        if (empty($passDetails)) {
            $this->Session->setFlash('Sorry, this pass cannot be renewed as this pass has been removed from this property', 'warning');
            $this->redirect(array('action' => 'view', $userId));
        }
        $packageId = $passDetails['Pass']['package_id'];
        $pass_expiry = $this->CustomerPass->field('pass_valid_upto', array('id' => $customerPassId));
       
        if (!is_null($packageId)) {
            $this->loadModel('Package');
            if (!$this->Package->exists($packageId)) {
                $this->Session->setFlash('Sorry, this pass cannot be renewed as this pass has been removed from this property', 'warning');
                $this->redirect(array('action' => 'view', $userId));
            }
        }
		$advance=false;
		$pass_expiry = $this->CustomerPass->field('pass_valid_upto', array('id' => $customerPassId));
        if ($passDetails['Pass']['is_fixed_duration'] == 0) {
			$pass_expiry = $this->CustomerPass->field('pass_valid_upto', array('id' => $customerPassId));
			$dt = new DateTime();
			$currentDateTime = $dt->format('Y-m-d H:i:s');
			if($pass_expiry>$currentDateTime){
				$currentDateTime=$pass_expiry; 
				$advance=true;
			}
            switch ($passDetails['Pass']['duration_type']) {
                case "Hour":
                    $date = new DateTime($currentDateTime);
                    $k = (int) $passDetails['Pass']['duration'];
                    $date->add(new DateInterval('PT' . $k . 'H'));
                    $passDetails['Pass']['expiration_date'] = date_format($date, 'Y-m-d H:i:s');
                    break;
                case "Day":
                    $date = new DateTime($currentDateTime);
                    $k = $passDetails['Pass']['duration'];
                    $date->add(new DateInterval('P' . $k . 'D'));
                    $passDetails['Pass']['expiration_date'] = date_format($date, 'Y-m-d H:i:s');
                    break;
                case "Week":
                    $date = new DateTime($currentDateTime);
                    $k = (int) $passDetails['Pass']['duration'] * 7;
                    $date->add(new DateInterval('P' . $k . 'D'));
                    $passDetails['Pass']['expiration_date'] = date_format($date, 'Y-m-d H:i:s');
                    break;
                case "Month":
                    $date = new DateTime($currentDateTime);
                    $k = (int) $passDetails['Pass']['duration'];
                    $date->add(new DateInterval('P' . $k . 'M'));
                    $passDetails['Pass']['expiration_date'] = date_format($date, 'Y-m-d H:i:s');
                    break;
                case "Year":
                    $date = new DateTime($currentDateTime);
                    $k = (int) $passDetails['Pass']['duration'];
                    $date->add(new DateInterval('P' . $k . 'Y'));
                    $passDetails['Pass']['expiration_date'] = date_format($date, 'Y-m-d H:i:s');
                    break;
            }
            $packageDetails = array();
            $totalRenewAmount = $passDetails['Pass']['cost_after_1st_year'];
            $date3 = new DateTime($passDetails['Pass']['expiration_date']);
            $datePack = $date3->format('Y-m-d');
            $passDetails['Pass']['expiration_date'] = $datePack . ' 23:59:59';
            if ($passDetails['Pass']['package_id']) {
                $this->loadModel('Package');
                $this->Package->recursive = -1;
                $packageDetails = $this->Package->find('first', array('conditions' => array('id' => $passDetails['Pass']['package_id'])));
                if ($packageDetails) {
                    if ($packageDetails['Package']['is_guest'] == 0) {
                        $totalRenewAmount = $totalRenewAmount + $packageDetails['Package']['cost'];
                    }
                }
            }
            if ($totalRenewAmount == 0) {
                $arr['details']['pass_Id'] = $passId;
                $arr['details']['customer_pass_Id'] = $customerPassId;
                $arraySerialize = serialize($arr);
                $dt = new DateTime();
				$currentDateTimeTransac = $dt->format('Y-m-d H:i:s');
                $transactionArray['Transaction']['paypal_tranc_id'] = 'Pass Renew No Payment';
                $transactionArray['Transaction']['user_id'] = $userId;
                $transactionArray['Transaction']['date_time'] = $currentDateTimeTransac;
                $transactionArray['Transaction']['amount'] = 0;
                $transactionArray['Transaction']['result'] = 'success';
                $transactionArray['Transaction']['pass_id'] = $passId;
                $transactionArray['Transaction']['transaction_array'] = $arraySerialize;
                $transactionArray['Transaction']['first_name'] = "AdminRenew";
                $transactionArray['Transaction']['last_name'] = "Pass";
                $transactionArray['Transaction']['comments'] = "passRenewed";
                $transactionArray['Transaction']['transaction_type'] = 1;	
                $this->loadModel('Transaction');
                $this->Transaction->create();
                if ($this->Transaction->save($transactionArray, false)) {
                    $transactionId = $this->Transaction->getLastInsertId();
                    $previousId = $this->CustomerPass->field('vehicle_id', array('id' => $customerPassId));
                    $package_id = $packageExpiryDate = NULL;
                    if ($packageDetails) {
                        if ($packageDetails['Package']['is_guest'] == 0) {
                            if ($packageDetails['Package']['is_fixed_duration'] == 0) {
								$package_expiry_date = $this->CustomerPass->field('pass_valid_upto', array('id' => $customerPassId));
								$dt = new DateTime();
								$currentDateTime = $dt->format('Y-m-d H:i:s');
								if($package_expiry_date){
									if($package_expiry_date>$currentDateTime){
										$currentDateTime=$package_expiry_date; 
									}
								}
                                switch ($packageDetails['Package']['duration_type']) {
                                    case "Hour":
                                        $date = new DateTime($currentDateTime);
                                        $k = (int) $packageDetails['Package']['duration'];
                                        $date->add(new DateInterval('PT' . $k . 'H'));
                                        $packageDetails['Package']['expiration_date'] = date_format($date, 'Y-m-d H:i:s');
                                        break;
                                    case "Day":
                                        $date = new DateTime($currentDateTime);
                                        $k = $packageDetails['Package']['duration'];
                                        $date->add(new DateInterval('P' . $k . 'D'));
                                        $packageDetails['Package']['expiration_date'] = date_format($date, 'Y-m-d H:i:s');
                                        break;
                                    case "Week":
                                        $date = new DateTime($currentDateTime);
                                        $k = (int) $packageDetails['Package']['duration'] * 7;
                                        $date->add(new DateInterval('P' . $k . 'D'));
                                        $packageDetails['Package']['expiration_date'] = date_format($date, 'Y-m-d H:i:s');
                                        break;
                                    case "Month":
                                        $date = new DateTime($currentDateTime);
                                        $k = (int) $packageDetails['Package']['duration'];
                                        $date->add(new DateInterval('P' . $k . 'M'));
                                        $packageDetails['Package']['expiration_date'] = date_format($date, 'Y-m-d H:i:s');
                                        break;
                                    case "Year":
                                        $date = new DateTime($currentDateTime);
                                        $k = (int) $packageDetails['Package']['duration'];
                                        $date->add(new DateInterval('P' . $k . 'Y'));
                                        $packageDetails['Package']['expiration_date'] = date_format($date, 'Y-m-d H:i:s');
                                        break;
                                }
                            }
                            $date4 = new DateTime($packageDetails['Package']['expiration_date']);
                            $datePack = $date4->format('Y-m-d');
                            $packageExpiryDate = $packageDetails['Package']['expiration_date'] = $datePack . ' 23:59:59';
                            $package_id = $packageDetails['Package']['id'];
                        }else{
							$package_id=$packageDetails['Package']['id'];
						}
                    }
                    $array['CustomerPass'] = array(
                        'id' => $customerPassId,
                        'pass_id' => $passId,
                        'pass_valid_upto' => $passDetails['Pass']['expiration_date'],
                        'membership_vaild_upto' => $packageExpiryDate,
                        'package_id' => $package_id,
                        //'vehicle_id' => null,
                        'transaction_id' => $transactionId
                    );
					 if($packageDetails){
						if($packageDetails['Package']['is_guest']==1){
								$array['CustomerPass']['is_guest_pass']=1;
						}else{
							$array['CustomerPass']['is_guest_pass']=0;
						} 
					   if($advance){
							if($packageDetails['Package']['is_guest']==1){
								unset($array['CustomerPass']['membership_vaild_upto']);
							}
						}else{
							if($packageDetails['Package']['is_guest']==1){
								$this->loadModel('Vehicle');
								//SET CUSTOMER PASS ID TO NULL IF IT IS ATTACHED TO ANY OTHER VEHICLE
								$this->Vehicle->updateAll(array('Vehicle.customer_pass_id'=>NULL),array('Vehicle.customer_pass_id'=>$customerPassId));
								$array['CustomerPass']['vehicle_id']=null;
								
								if($previousId){
									 //SET VEHICLE ID TO NULL IF IT IS ATTACHED TO ANY OTHER PASS
									$this->CustomerPass->updateAll(array('CustomerPass.vehicle_id'=>NULL),array('CustomerPass.vehicle_id'=>$previousId));
									$vehicleArray['Vehicle']=array('id'=>$previousId,
																   'customer_pass_id'=>null								
									);
									if($this->Vehicle->save($vehicleArray,false)){
										CakeLog::write('passRenewedVehicleNull', ''.AuthComponent::user('username').' : Pass renewed with Pass ID: '.$passId.' Customer Pass ID: <a href="/admin/CustomerPasses/edit/'.$customerPassId.'">'.$customerPassId.'</a> renewed by User: '.AuthComponent::user('first_name').' '.AuthComponent::user('last_name').', in Property: '.CakeSession::read('PropertyName').', VEHICLE SET NULL');
									}else{
										CakeLog::write('passRenewedVehicleNotNULL', ''.AuthComponent::user('username').' : Pass renewed with Pass ID: '.$passId.' And Customer Pass ID: <a href="/admin/CustomerPasses/edit/'.$customerPassId.'">'.$customerPassId.'</a> renewed by User: '.AuthComponent::user('first_name').' '.AuthComponent::user('last_name').', in Property: '.CakeSession::read('PropertyName').', VEHICLE NOT SET NULL');
									}
								}else{
									CakeLog::write('passRenewedNOVehicle', ''.AuthComponent::user('username').' : Pass renewed with Pass ID: '.$passId.' Customer Pass ID: <a href="/admin/CustomerPasses/edit/'.$customerPassId.'">'.$customerPassId.'</a> renewed by User: '.AuthComponent::user('first_name').' '.AuthComponent::user('last_name').', in Property: '.CakeSession::read('PropertyName').', VEHICLE NOT FOUND');

								}
							}
						}
					}
                   // if ($this->Vehicle->save($vehicleArray, false)) {
                        if ($this->CustomerPass->save($array, false)) {
                            $this->loadModel('Pass');
                            $passName = $this->Pass->givePassName($passId);
                            if($advance){
								CakeLog::write('passRenewedAdvanceAdmin', '' . AuthComponent::user('username') . ' : Pass renewed IN ADVANCE with Pass ID: ' . $passId . ' and Pass Name: ' . $passName . ', And Customer Pass ID: <a href="/admin/CustomerPasses/edit/' . $customerPassId . '">' . $customerPassId . '</a> renewed by User: ' . AuthComponent::user('first_name') . ' ' . AuthComponent::user('last_name') . ', in Property: ' . $propertyName['Property']['name'] . ', Amount Paid: $0 EXPIRY DATE WAS : '.$pass_expiry);
							}else{
								CakeLog::write('passRenewedAdmin', '' . AuthComponent::user('username') . ' : Pass renewed with Pass ID: ' . $passId . ' and Pass Name: ' . $passName . ', And Customer Pass ID: <a href="/admin/CustomerPasses/edit/' . $customerPassId . '">' . $customerPassId . '</a> renewed by User: ' . AuthComponent::user('first_name') . ' ' . AuthComponent::user('last_name') . ', in Property: ' . $propertyName['Property']['name'] . ', Amount Paid: $0');
							}
                            if ($packageDetails['Package']['is_guest'] == 0) {
								$this->Session->setFlash('Pass renewed successfully', 'success');
							}else{
								$this->Session->setFlash('Pass renewed successfully', 'success');
							}
                            
                            $this->redirect(array('action' => 'customer_view', $userId));
                        } else {
                            $this->Session->setFlash('Process Failed', 'error');
                            $this->redirect(array('action' => 'customer_view', $userId));
                        }
                    /*} else {
                        $this->Session->setFlash('Vehicle Not Updated', 'error');
                    }*/
                } else {
                    $this->Session->setFlash('Internal Error', 'error');
                    $this->redirect(array('action' => 'customer_view', $userId));
                }
            } else {
                $this->redirect(array('controller' => 'Transactions', 'action' => 'passRenew', $customerPassId, $passId, $userId, $propertyIdAdmin,$advance));
            }
        } else {
            $this->Session->setFlash('Pass is not renewable', 'error');
            $this->redirect(array('action' => 'customer_view', $userId));
        }
    }
 // Updations done on 10th Feb 2015
public function get_recurring_profiles($customerPassId=null){
	 //debug($customerPassId);die;
	 $this->autoRender=$this->layout=false;
	 $this->loadModel('RecurringProfile');
	 $output=array();
	 $output = $this->RecurringProfile->get_pass_recurring_profile($customerPassId);
	 //debug($output);die;
	 echo json_encode($output);
 
 }
// Updations done on 10th Feb 2015
  public function recurring_profile_details($recurringProfileId=null){
	 //debug($customerPassId);die;
	 $this->autoRender=$this->layout=false;
	 $this->loadModel('RecurringProfile');
	 $output=array();
	 $output = $this->RecurringProfile->get_recurring_profile_detail($recurringProfileId);
	 //debug($output);die;
	 echo json_encode($output);
 
 }
 /*********************************************************
   * Passes expiring in given interval
   */
  public function admin_pass_expiry(){
			$this->loadModel('Property');
			$property_list=$this->Property->find('list',array('fields'=>'name'));
			$id=array_keys($property_list);
			$selectedId=$id[0];
			$this->loadModel('Pass');
			$passes_list=$this->Pass->find('list',array('fields'=>'name','conditions'=>array('property_id'=>$selectedId)));
			$pass_id=array_keys($property_list);
			$selectedPassId=$pass_id[0];
			$this->set(compact('property_list','selectedId','passes_list','selectedPassId'));
		}
	public function admin_get_passes_expiring_interval(){ 
			$this->autoRender=false;
			//debug($this->request);
			$dataArray= array_keys($this->request->query);
			$data=json_decode($dataArray[0]);
			$value = get_object_vars($data);
			//debug($value);die;
			$date1 = new DateTime($value['fromDate']);
			$fromDate= $date1->format('Y-m-d');
			$date2 = new DateTime($value['todate']); 
			$toDate= $date2->format('Y-m-d');
			$pass_id=$value['passId'];
			$property_id=$value['propertyId'];   
			$conditions='';
			$conditionsCount='';
			if($date1==$date2){
				$toDate=$fromDate.' 23:59:59';
			}else{
				$toDate=$toDate.' 23:59:59';
			}
			$conditions=" cp.property_id= ".$property_id." AND";
			$conditionsCount['property_id']=$property_id;
			if($pass_id){
				$conditions= " cp.pass_id= ".$pass_id." AND";
				$conditionsCount['pass_id']=$pass_id;
			}
			$conditions= $conditions." cp.pass_valid_upto BETWEEN '".$fromDate."' AND '".$toDate."' ";
			$conditionsCount['pass_valid_upto BETWEEN ? AND ?'] = array($fromDate,$toDate);
			//debug($conditions);die;
			$response=array();
			$this->CustomerPass->recursive=-1;
			$totalPass=$this->CustomerPass->find('count',array('conditions'=>$conditionsCount));
			$response['totalPass']=$totalPass;
			$response['condition']=$conditions;
			$this->loadModel('Pass');
			$passName=$this->Pass->givePassName($pass_id);
			$response['passName']=$passName;
			echo json_encode($response);      
			
		}
		public function admin_passes_interval_new()  
		{
			$this->autoRender=$this->layout=false;
			$part1=array_keys($this->request->params['named']);
			$part2=array_values($this->request->params['named']);
			$condition=$part1[0].":".$part2[0];
			$output=array();
			$output = $this->CustomerPass->get_data_passwise_interval(json_decode($condition));
			echo json_encode($output);
		}
		public function admin_passes_expiry_interval()
		{
			$this->autoRender=$this->layout=false;
			$part1=array_keys($this->request->params['named']);
			$part2=array_values($this->request->params['named']);
			$condition=$part1[0].":".$part2[0];
			//debug($condition);die;
			$output=array();
			$output = $this->CustomerPass->get_data_passwise_interval(json_decode($condition));
			echo json_encode($output);
		}
		 /*******************************
  * TO FIND THE NEXT PAYMENT DATE
  */
  public function admin_get_next_date(){  
	$this->autoRender=$this->layout=false;
	$this->loadModel('RecurringProfile');
	$output=array();
	$output = $this->RecurringProfile->find('all',array('conditions'=>array('status'=>'Active')));
	$dt = new DateTime();
	CakeLog::write('nextDateRecurringCron',' Triggered At '.$dt->format('Y-m-d H:i:s'));
	//debug($output);die;
	for($i=0;$i<count($output);$i++){
		$result=$this->PayFlow->enquiry_profile($output[$i]['RecurringProfile']['recurring_profile_id']);
		//debug($result);
		if($result){
			if($result['STATUS'] == 'ACTIVE'){
				//debug($result['NEXTPAYMENT']);
				$month= substr($result['NEXTPAYMENT'],0,2);
				$day= substr($result['NEXTPAYMENT'],2,2);
				$year= substr($result['NEXTPAYMENT'],4,4);
				$date=$year.'-'.$month.'-'.$day;
				$array['RecurringProfile']=array('id'=>$output[$i]['RecurringProfile']['id'],
												 'next_payment_date'=>$date,
												 'customer_pass_id'=>$this->CustomerPass->field('id',array('recurring_profile_id'=>$output[$i]['RecurringProfile']['recurring_profile_id'])),
												 'paid'=>0
												);
				if($this->RecurringProfile->save($array)){
					CakeLog::write($output[$i]['RecurringProfile']['recurring_profile_id'],$output[$i]['RecurringProfile']['recurring_profile_id'].' : Next date Set to '.$date);
				}else{
					CakeLog::write($output[$i]['RecurringProfile']['recurring_profile_id'],$output[$i]['RecurringProfile']['recurring_profile_id'].' : Next date Set Failure date was '.$date);
				}
				//debug($array);die;
			}else{
				$this->CustomerPass->unbindModel(array('belongsTo'=>array('User','Vehicle','Property','Transaction','Package','Pass')));
				$this->CustomerPass->updateAll(array('recurring_profile_status' => "'".$result['STATUS']."'"),
										array('recurring_profile_id' => $output[$i]['RecurringProfile']['recurring_profile_id'])
										
									);
			}
		}
		//debug($result);die;
	}
	//$this->PayFlow->enquiry_profile('RT0000000001');
	
  }
   /*******************************
  * TO UPDATE PASS DATA
  */
  public function admin_update_pass_data(){  
	  $this->autoRender=$this->layout=false;
	  $dt = new DateTime();
	  $currentDate= $dt->format('Y-m-d');
	 // CakeLog::write('updatePassRecurringCron',' Triggered At '.$dt->format('Y-m-d H:i:s'));
	  $this->loadModel('RecurringProfile');
	  $output = $this->RecurringProfile->find('all',array('conditions'=>array('status'=>'Active','paid'=>0)));
	  for($i=0;$i<count($output);$i++){
		$result=$this->PayFlow->enquiry_profile_payment($output[$i]['RecurringProfile']['recurring_profile_id']);
		debug($output[$i]['RecurringProfile']['recurring_profile_id']);
		debug($result); 
		$searchResult=NULL;
		if($result){
			$currentDateToSearch=$dt->format('d-M-y');
			//$currentDateToSearch=date('d-M-y',strtotime('2015-11-09'));
			debug($currentDateToSearch); 
			$searchResult = array_filter(array_values($result), function ($item) use ($currentDateToSearch) {
											if (stripos($item, $currentDateToSearch) !== false) {
												return true;
											}else{
												return false;
											}
										}
								);
		}
		debug($searchResult); 
		
	  }die;
  }
  /*******************************
  * TO UPDATE PASS DATA
  */
  public function admin_deactivate_profile(){  
	 $this->autoRender=$this->layout=false;
	 $dt = new DateTime();
	 $this->loadModel('RecurringProfile');
	 $currentDate= $dt->format('Y-m-d');
	 CakeLog::write('deactivateRecurringCron',' Triggered At '.$dt->format('Y-m-d H:i:s'));
	 $this->CustomerPass->recursive=-1;
	 $output = $this->CustomerPass->find('all',array('conditions'=>array('DATE(pass_valid_upto)'=>$currentDate,'recurring_profile_id !='=>NULL,'is_guest_pass'=>0)));
	 if($output){
		for($i=0;$i<count($output);$i++){
			$result=$this->PayFlow->deactivate_profile($output[$i]['CustomerPass']['recurring_profile_id']);
			if($result){
				if($result['RESULT']==0 && $result['RESPMSG']=='Approved'){
					//$this->RecurringProfile->unbindModel(array('belongsTo'=>array('User','Vehicle','Property','Transaction','Package','Pass')));
					$recurringProfileId=$this->RecurringProfile->field('id',array('recurring_profile_id'=>$output[$i]['CustomerPass']['recurring_profile_id']));
					if($recurringProfileId){
						$arr['RecurringProfile']=array('id'=>$recurringProfileId,
														'status'=>'Cancelled'
													);
						$this->RecurringProfile->save($arr,false);
					}
					$array['CustomerPass']=array('id'=>$output[$i]['CustomerPass']['id'],
												 'recurring_profile_status'=>'Cancelled'
												);
					if($this->CustomerPass->save($array,false)){
						CakeLog::write('ProfileDeactivation',$output[$i]['CustomerPass']['recurring_profile_id'].' Recurring Profile deactivated successfully');

					}else{
						CakeLog::write('ProfileDeactivation',$output[$i]['CustomerPass']['recurring_profile_id'].' Recurring Profile deactivated but status not updated in Customer Pass Table');
					}
				}else{
					CakeLog::write('ProfileDeactivation',$output[$i]['CustomerPass']['recurring_profile_id'].' Recurring Profile deactivation failed');

				}
			}
		}
	 }
  }
  public function admin_last_seen(){
			if(AuthComponent::user('role_id')==2){
				$this->layout='administrator';
			}
			$this->loadModel('LastSeenVehicle');
			$rfids=$this->LastSeenVehicle->find('list',array(
																 'fields'=>array('LastSeenVehicle.rfid','LastSeenVehicle.rfid'),
																 'joins'=>array(
																				array(
																					  'type'=>'inner',
																					  'table'=>'customer_passes',
																					  'conditions'=>array('LastSeenVehicle.customer_pass_id=customer_passes.id')
																				
																				)
																 )
																 ));
			$this->loadModel('Property');
			$allProperties=$this->Property->find('list');
			if(AuthComponent::user('role_id')==2){
				$allProperties=CakeSession::read('AdminAllProperty');
			}
			$this->set(compact('rfids','allProperties'));
		}
		public function admin_get_last_seen($rfidTag){
			$this->autoRender=$this->layout=false;
			$output=array();
			$this->loadModel('LastSeenVehicle');
			$output = $this->LastSeenVehicle->getLastSeenStatus($rfidTag);
			echo json_encode($output);
			
		}
		public function admin_get_last_seen_propertywise(){
			$fromdate=date('Y-m-d H:i:s',strtotime($this->request->query['fromDate']));
			$toDate=date('Y-m-d',strtotime($this->request->query['toDate']));
			$toDate=$toDate.' 23:59:59';
			$propertyId=$this->request->query['propertyId'];
			$this->autoRender=$this->layout=false;
			$output=array();
			$this->loadModel('LastSeenVehicle');
			$output = $this->LastSeenVehicle->getLastSeenStatusProppertyWise($fromdate,$toDate,$propertyId);
			echo json_encode($output);
			
		}
	/*****************************************************************
	 * Function to extend the Guest Pass IF PAYMENT NOT REQUIRED
	 * Added On: 7th March, 2016
	 * By: Shalender
	 */
	 public function extendGuestPass() {
        $this->autoRender = $this->layout = false;
        if($this->request->is('POST') || $this->request->is('PUT')){
			
			$this->CustomerPass->recursive = -1;
			$passData = $this->CustomerPass->find('first', array('conditions' => array('id' => $this->request->data['CustomerPass']['customerPassId'])));
	
			if(is_numeric($this->request->data['CustomerPass']['days'])){
				if ($passData) {
					$date1 = new DateTime($passData['CustomerPass']['membership_vaild_upto']);
					$testdate= new DateTime($passData['CustomerPass']['membership_vaild_upto']);
					$date2 = new DateTime($passData['CustomerPass']['pass_valid_upto']);
					$testdate->modify("+" . $this->request->data['CustomerPass']['days'] . " days");
					$passExpiryDate=$date2->format('Y-m-d');
					$permitExpiryDate=$testdate->format('Y-m-d');
					$datediff =strtotime($passExpiryDate)-strtotime($permitExpiryDate);
					$daysLeft=floor($datediff/(60*60*24));
					if($daysLeft<0){
						 $this->Session->setFlash('Pass Cannot Be Updated', 'error');
						 $this->redirect(array('action' => 'view'));
					}
					
					if ($daysLeft >= 0) {
						
						$date1->modify("+" . $this->request->data['CustomerPass']['days'] . " days");
						$toDate = $date1->format('Y-m-d H:i:s');
						$leftCredits=0;
						 if ($this->request->data['CustomerPass']['days'] <  AuthComponent::user('guest_credits')) {
							$leftCredits= AuthComponent::user('guest_credits') - $this->request->data['CustomerPass']['days'];
						 }elseif($this->request->data['CustomerPass']['days'] > AuthComponent::user('guest_credits')){
							$days = $this->request->data['CustomerPass']['days'];
							$this->CustomerPass->recursive = -1;
							$customerPassData = $passData;
							$this->loadModel('Package');
							$this->Package->recursive = -1;
							$packageData = $this->Package->find('first', array('conditions' => array('id' => $customerPassData['CustomerPass']['package_id'])));
							$daysPayable = ($days - AuthComponent::user('guest_credits'));
							$totalAmount = ($packageData['Package']['cost'] * $daysPayable);
							if ($totalAmount > 0) {
								CakeSession::write('CustomerPassID', $this->request->data['CustomerPass']['customerPassId']);
								CakeSession::write('DAYS', $this->request->data['CustomerPass']['days']);
								$this->redirect(array('action' => 'update_guest_pass'));
							}
						 }
						$array['CustomerPass'] = array('id' => $this->request->data['CustomerPass']['customerPassId'],
							'membership_vaild_upto' => $toDate
							);
						 if ($this->CustomerPass->save($array, false)) {
							$this->loadModel('Vehicle');
							$this->loadModel('UserGuestPass');
							$array['UserGuestPass'] = array('user_id' => AuthComponent::user('id'),
								'customer_pass_id' => $this->request->data['CustomerPass']['customerPassId'],
								'vehicle_id' => $passData['CustomerPass']['vehicle_id'],
								'vehicle_details' => $this->Vehicle->field('vehicle_info', array('id' => $passData['CustomerPass']['vehicle_id'])),
								'days' => $this->request->data['CustomerPass']['days'],
								'free' => $this->request->data['CustomerPass']['days'],
								'amount' => 0,
								'created' => $passData['CustomerPass']['membership_vaild_upto'],
								'to_date' => $toDate,
								'property_id' => $passData['CustomerPass']['property_id']
							);

							$this->UserGuestPass->create();
							if ($this->UserGuestPass->save($array, false)) {
								$this->loadModel('User');
								$arr['User'] = array('id' => AuthComponent::user('id'),
									'guest_credits' => $leftCredits
								);
								if ($this->User->save($arr, false)) {
									$_SESSION['Auth']['User']['guest_credits'] = $leftCredits;
									CakeLog::write('GuestPassExtended',AuthComponent::user('username').' : Pass date extended successfully FROM : '.$passData['CustomerPass']['membership_vaild_upto'].' TO '.$toDate.' FOR DAYS: '.$this->request->data['CustomerPass']['days'].' AMOUNT : $0');
									$this->Session->setFlash('Pass date extended successfully', 'success');
									$this->redirect(array('action' => 'view'));
								}
							}
						}else{
							$this->Session->setFlash('An Error Has Occured, PLease Try Again', 'error');
							$this->redirect(array('action' => 'view'));
						}
					} else {
						$this->Session->setFlash('Pass not found', 'error');
						$this->redirect(array('action' => 'view'));
					}
				} else {
					$this->Session->setFlash('Pass not found', 'error');
					$this->redirect(array('action' => 'view'));
				}
			}else {
					$this->Session->setFlash('Invalid Request', 'error');
					$this->redirect(array('action' => 'view'));
			}
	  }else{
		$this->Session->setFlash('Invalid Request', 'error');
		$this->redirect(array('action' => 'view'));
	  }
    }
	/*****************************************************************
	 * Function to extend the Guest Pass IF PAYMENT REQUIRED
	 * Added On: 7th March, 2016
	 * By: Shalender
	 */
	
	public function update_guest_pass() {
        $this->layout = 'customer';
        if (CakeSession::check('CustomerPassID')) {
            $customerPassID = CakeSession::read('CustomerPassID');
            $days = CakeSession::read('DAYS');
            $this->CustomerPass->recursive = -1;
            $customerPassData = $this->CustomerPass->find('first', array('conditions' => array('id' => $customerPassID)));
            if ($customerPassData['CustomerPass']['is_guest_pass'] == 1) {
                $date1 = new DateTime($customerPassData['CustomerPass']['membership_vaild_upto']);
                $date1->modify("+" . $days . " days");
                $toDate = $date1->format('Y-m-d H:i:s');
                $this->loadModel('Package');
                $this->Package->recursive = -1;
                $packageData = $this->Package->find('first', array('conditions' => array('id' => $customerPassData['CustomerPass']['package_id'])));
                $daysPayable = ($days - AuthComponent::user('guest_credits'));
                if ($this->request->is('POST')) {
                    $validateArray = array(
                        'email' => array(
                            'Email Required' => array('rule' => 'email', 'allowEmpty' => false, 'required' => true, 'message' => 'Email is not correct',)
                        ),
                        'first_name' => array('minimum length' => array(
                                'rule' => array('minLength', '2'),
                                'message' => 'Minimum 2 characters required',
                                'allowEmpty' => false,
                                'required' => true),
                            'real name' => array('rule' => '/^[a-zA-Z ]*$/', 'message' => 'Only letters and spaces.')
                        ),
                        'last_name' => array('minimum length' => array(
                                'rule' => array('minLength', '2'),
                                'message' => 'Minimum 2 characters required',
                                'allowEmpty' => false,
                                'required' => true),
                            'real name' => array('rule' => '/^[a-zA-Z ]*$/', 'message' => 'Only letters and spaces.')
                        ),
                        'address_line_1' => array(
                            'rule' => array('minLength', '2'),
                            'message' => 'Minimum 2 characters required',
                            'allowEmpty' => false,
                            'required' => true
                        ),
                        'address_line_2' => array(
                            'rule' => array('minLength', '2'),
                            'message' => 'Minimum 2 characters required',
                            'required' => true,
                            'allowEmpty' => true
                        ),
                        'zip' => array(
                            'rule' => array('postal', null, 'us'),
                            'message' => 'Please check your zip code',
                            'required' => true,
                            'allowEmpty' => false
                        ),
                        'phone' => array(
                            'rule' => array('phone', null, 'us'),
                            'message' => 'Please check your phone number',
                            'required' => true,
                            'allowEmpty' => false
                        ),
                        'city' => array('minimum length' => array(
                                'rule' => array('minLength', '2'),
                                'message' => 'Minimum 2 characters required',
                                'allowEmpty' => false,
                                'required' => true),
                            'real name' => array('rule' => '/^[a-zA-Z ]*$/', 'message' => 'Only letters and spaces allowed.')
                        ),
                        'state' => array(
                            'rule' => array('minLength', '2'),
                            'message' => 'Minimum 2 characters required',
                            'required' => true,
                            'allowEmpty' => false
                        ),
                        'card_number' => array(
                            'numeric' => array(
                                'rule' => 'numeric',
                                'message' => 'Only numeric data allowed',
                                'allowEmpty' => false,
                                'required' => true
                            ),
                            'minimum length' => array('rule' => array('minLength', '15'), 'message' => 'Min 15 numbers'),
                            'maximum length' => array('rule' => array('maxLength', '16'), 'message' => 'Max 16 numbers'),
                        ),
                        'cvv' => array(
                            'numeric' => array(
                                'rule' => 'numeric',
                                'message' => 'Only numeric data allowed',
                                'allowEmpty' => false,
                                'required' => true
                            ),
                            'minimum length' => array('rule' => array('minLength', '3'), 'message' => 'Min. 3 numbers'),
                            'maximum length' => array('rule' => array('maxLength', '4'), 'message' => 'Max. 4 numbers')
                        ),
                        'month' => array(
                            'rule' => array('minLength', '1'),
                            'message' => 'Select Month',
                            'required' => true,
                            'allowEmpty' => false
                        ),
                        'year' => array(
                            'rule' => array('minLength', '1'),
                            'message' => 'Select Month',
                            'required' => true,
                            'allowEmpty' => false
                        ),
                        'first_name_cc' => array('minimum length' => array(
                                'rule' => array('minLength', '2'),
                                'message' => 'Minimum 2 characters required',
                                'allowEmpty' => false,
                                'required' => true),
                            'real name' => array('rule' => '/^[a-zA-Z ]*$/', 'message' => 'Only letters and spaces.')
                        ),
                        'last_name_cc' => array('minimum length' => array(
                                'rule' => array('minLength', '2'),
                                'message' => 'Minimum 2 characters required',
                                'allowEmpty' => false,
                                'required' => true),
                            'real name' => array('rule' => '/^[a-zA-Z ]*$/', 'message' => 'Only letters and spaces.')
                        )
                    );
                    $this->CustomerPass->validate = $validateArray;
                    $this->CustomerPass->set($this->request->data);
                    if ($this->CustomerPass->validates()) {
                        $totalAmount = ($packageData['Package']['cost'] * $daysPayable);
                        $errorMessage = null;
                        $response = null;
                        if ($totalAmount > 0) {
                            $payment = array(
                                'amount' => (int) $totalAmount,
                                'card' => $this->request->data['CustomerPass']['card_number'], // This is a sandbox CC
                                'expiry' => array(
                                    'M' => $this->request->data['CustomerPass']['month'],
                                    'Y' => $this->request->data['CustomerPass']['year'],
                                ),
                                'cvv' => $this->request->data['CustomerPass']['cvv'],
                                'currency' => 'USD' // Defaults to GBP if not provided
                            );
                            $errorMessage = null;
                            $response = null;
                            try {
                                $response = $this->Paypal->doDirectPayment($payment);
                                // Check $response[ACK]='Success' OR 'OK'
                            } catch (Exception $e) {
                                $errorMessage = $e->getMessage();
                            }
                        } else {
                            $response['TIMESTAMP'] = date('Y-m-d H:i:s');
                            $response['TRANSACTIONID'] = 'GuestExtended';
                            $response['ACK'] = 'Success';
                            $response['AMT'] = '0';
                            $response['PassDetail'] = 'Guest Pass Extended';
                            $errorMessage = null;
                            $this->request->data['Transaction']['first_name_cc'] = $this->Auth->user('first_name');
                            $this->request->data['Transaction']['last_name_cc'] = $this->Auth->user('last_name');
                        }
                        if ($errorMessage == null) {
                            if ($response['ACK'] == 'Success') {
                                $dateTransaction = date('Y-m-d H:i:s', strtotime($response['TIMESTAMP']));
                                $arraySerialize = serialize($response);
                                $transactionArray['Transaction']['paypal_tranc_id'] = $response['TRANSACTIONID'];
                                $transactionArray['Transaction']['user_id'] = $this->Auth->user('id');
                                $transactionArray['Transaction']['date_time'] = $dateTransaction;
                                $transactionArray['Transaction']['amount'] = $response['AMT'];
                                $transactionArray['Transaction']['result'] = $response['ACK'];
                                $transactionArray['Transaction']['message'] = 'Guest Pass extension payment';
                                $transactionArray['Transaction']['pass_id'] = $customerPassData['CustomerPass']['pass_id'];
                                $transactionArray['Transaction']['transaction_array'] = $arraySerialize;
                                $transactionArray['Transaction']['first_name'] = $this->request->data['CustomerPass']['first_name_cc'];
                                $transactionArray['Transaction']['last_name'] = $this->request->data['CustomerPass']['last_name_cc'];
                                $this->loadModel('Transaction');
                                $this->Transaction->create();
                                if ($this->Transaction->save($transactionArray, false)) {
                                    $customerPassArray['CustomerPass']['id'] = $customerPassData['CustomerPass']['id'];
                                    $customerPassArray['CustomerPass']['membership_vaild_upto'] = $toDate;
                                    if ($this->CustomerPass->save($customerPassArray, false)) {
                                        $billAddressArray['BillingAddress']['user_id'] = $this->Auth->user('id');
                                        $billAddressArray['BillingAddress']['email'] = $this->request->data['CustomerPass']['email'];
                                        $billAddressArray['BillingAddress']['first_name'] = $this->request->data['CustomerPass']['first_name'];
                                        $billAddressArray['BillingAddress']['last_name'] = $this->request->data['CustomerPass']['last_name'];
                                        $billAddressArray['BillingAddress']['address_line_1'] = $this->request->data['CustomerPass']['address_line_1'];
                                        $billAddressArray['BillingAddress']['address_line_2'] = $this->request->data['CustomerPass']['address_line_2'];
                                        $billAddressArray['BillingAddress']['city'] = $this->request->data['CustomerPass']['city'];
                                        $billAddressArray['BillingAddress']['state'] = $this->request->data['CustomerPass']['state'];
                                        $billAddressArray['BillingAddress']['zip'] = $this->request->data['CustomerPass']['zip'];
                                        $billAddressArray['BillingAddress']['phone'] = $this->request->data['CustomerPass']['phone'];
                                        $this->loadModel('BillingAddress');
                                        $rslt = $this->BillingAddress->hasAny(array('user_id' => $this->Auth->user('id')));
                                        if ($rslt == true) {
                                            $billAddressId = $this->BillingAddress->find('first', array('fields' => array('id'), 'conditions' => array('user_id' => $this->Auth->user('id'))));
                                            $billAddressArray['BillingAddress']['id'] = $billAddressId['BillingAddress']['id'];
                                        }
                                        if ($this->BillingAddress->save($billAddressArray, false)) {
                                            $this->loadModel('User');
                                            $arr['User'] = array('id' => AuthComponent::user('id'),
                                                'guest_credits' => 0
                                            );
                                            if ($this->User->save($arr, false)) {
                                                $this->loadModel('Vehicle');
                                                $this->loadModel('UserGuestPass');
                                                $array['UserGuestPass'] = array('user_id' => AuthComponent::user('id'),
                                                    'customer_pass_id' => $customerPassData['CustomerPass']['id'],
                                                    'vehicle_id' => $customerPassData['CustomerPass']['vehicle_id'],
                                                    'vehicle_details' => $this->Vehicle->field('vehicle_info', array('id' => $customerPassData['CustomerPass']['vehicle_id'])),
                                                    'days' => $days,
                                                    'free' => $days - $daysPayable,
                                                    'paid' => $daysPayable,
                                                    'amount' => $totalAmount,
                                                    'created' => $customerPassData['CustomerPass']['membership_vaild_upto'],
                                                    'to_date' => $date1->format('Y-m-d H:i:s'),
                                                    'property_id' => $customerPassData['CustomerPass']['property_id']
                                                );
                                                $this->UserGuestPass->create();
                                                if ($this->UserGuestPass->save($array, false)) {
                                                    $_SESSION['Auth']['User']['guest_credits'] = 0;
                                                    CakeLog::write('GuestPassExtended',AuthComponent::user('username').' : Guest Pass date extended successfully FROM : '.$customerPassData['CustomerPass']['membership_vaild_upto'].' TO '.$toDate.' FOR DAYS: '.$days.' AMOUNT : $ '.$totalAmount);
                                                    $this->Session->setFlash('Pass date extended successfully', 'success');
                                                    $this->Session->delete('CustomerPassID');
                                                    $this->Session->delete('DAYS');
                                                    return $this->redirect(array('action' => 'view'));
                                                } else {
                                                    CakeLog::write($customerPassData['CustomerPass']['id'], 'The Guest Pass data was not saved');
                                                }
                                            }
                                        } else {
                                            $this->Session->setFlash(__('Billing Address Not Saved'));
                                            return $this->redirect(array('action' => 'view'));
                                        }
                                    } else {
                                        $this->Session->setFlash('Pass not saved', 'error');
                                        return $this->redirect(array('action' => 'view'));
                                    }
                                } else {
                                    $this->Session->setFlash('The transaction could not be saved. Contact Admin', 'error');
                                    return $this->redirect(array('action' => 'view'));
                                }
                            } else {
                                $this->Session->setFlash('Transaction Failed', 'error');
                                return $this->redirect(array('action' => 'view'));
                            }
                        } else {
                            $this->Session->setFlash($errorMessage, 'error');
                        }
                    }
                } 
                $this->loadModel('BillingAddress');
                $this->BillingAddress->recursive = -1;
                $billingAddress = $this->BillingAddress->find('first', array('conditions' => array('user_id' => AuthComponent::user('id'))));
                $this->loadModel('State');
                $states = $this->State->find('list');
                $this->set(compact('customerPassData', 'toDate', 'daysPayable', 'packageData', 'billingAddress', 'states', 'days'));
            } else {
                throw new NotFoundException(__('Invalid customer pass'));
            }
            //debug($customerPassData);die;
        } else {
            throw new NotFoundException(__('Invalid customer pass'));
        }
    }
    public function admin_tag_histories($customerPassId){
		$this->layout=false;
		$currentRFID=$this->CustomerPass->field('RFID_tag_number',array('CustomerPass.id'=>$customerPassId));
		$this->loadModel('CustomerPassBackup');  
		$history=$this->CustomerPassBackup->find('all',array('fields'=>array('CustomerPassBackup.RFID_tag_number','CustomerPassBackup.backup_date'),
																   'conditions'=>array('CustomerPassBackup.previous_id'=>$customerPassId,
																					   'CustomerPassBackup.RFID_tag_number IS NOT NULL',
																					   'CustomerPassBackup.RFID_tag_number != '=>$currentRFID,
																					   'CustomerPassBackup.backup_date=(SELECT MAX(backup_date) from customer_pass_backup where RFID_tag_number = CustomerPassBackup.RFID_tag_number )'
																					   ),
																   'group'=>array('CustomerPassBackup.RFID_tag_number'),
																   'order'=>array('id'=>'DESC')
			
			)); 
		$this->set(compact('history','currentRFID'));
	}
	/*******************************************************************
	 * admin_no_vehicle
	 * 
	 */
	public function admin_no_vehicle(){
		if($this->Auth->user('role_id')!=2){
			$this->loadModel('Property');    
			$property_list=$this->Property->find('list',array('fields'=>'name'));
			$id=array_keys($property_list); 
			$selectedId=$id[0];
			$this->set(compact('property_list','selectedId'));
		}else{ 
			$this->layout='administrator';
			$property_list=CakeSession::read('AdminAllProperty');
			$this->set(compact('property_list','selectedId'));
			$this->render('adm_no_vehicle');
		}
	}
	public function admin_get_no_vehicle($propertyId){
		$aColumns = array('user.first_name',
						  'user.last_name',
						  'user.email',
						  'user.phone',
						  'pass.name',
						  'customer_passes.RFID_tag_number',
						  'customer_passes.pass_valid_upto',
						  'customer_passes.id',
						  'customer_passes.membership_vaild_upto',
						  'customer_passes.user_id',
						  );
        $sIndexColumn = " customer_passes.id ";
        $sTable = " customer_passes ";
        $sJoinTable =	"INNER JOIN users user ON user.id = customer_passes.user_id
						 INNER JOIN passes pass ON pass.id = customer_passes.pass_id
        ";
       $sConditions = ' customer_passes.vehicle_id IS NULL AND DATE(pass_valid_upto)>= "'.date('Y-m-d').'" AND DATE(membership_vaild_upto)>= "'.date('Y-m-d').'" AND customer_passes.property_id='.$propertyId.' AND customer_passes.pass_archived=0';

        $returnArr = $this->NewDatatable->getData(array('columns' => $aColumns, 'index_column' => $sIndexColumn, 'table' => $sTable, 'join'=>$sJoinTable, 'conditions' => $sConditions));
        echo json_encode($returnArr);
        die;
	}
	/*******************************************************************
	 * manage no_vehicle function
	 * 
	 */
	public function no_vehicle(){
		$this->layout='manager';
	}/*******************************************************************
	 * activateGuestPass
	 * 
	 */
	public function activateGuestPass($customerPassId=null,$passId=null,$permitId=null){
		 $this->layout='customer';
		
		if (!$this->CustomerPass->exists($customerPassId)) {
            throw new NotFoundException(__('Invalid Customer Pass'));
        }else{
			$userID=$this->CustomerPass->field('user_id',array('id'=>$customerPassId));
			if($userID != AuthComponent::user('id')){
				 throw new NotFoundException(__('Invalid DATA'));
			}
			$rfid=$this->CustomerPass->field('RFID_tag_number',array('id'=>$customerPassId));
			if(is_null($rfid)){
				$this->Session->setFlash('Sorry, As RFID tag has not been assigned yet, Pass cannot be activated','warning');
				$this->redirect(array('controller'=>'CustomerPasses','action'=>'view'));
			}
		}
		 $this->loadModel('Package');
		if (!$this->Package->exists($permitId)) {
            throw new NotFoundException(__('Invalid package'));
        }
        $this->loadModel('Pass');
        if (!$this->Pass->exists($passId)) {
            throw new NotFoundException(__('Invalid pass'));
        }
        $packageDate=$this->CustomerPass->field('membership_vaild_upto',array('id'=>$customerPassId));
        if(!is_null($packageDate)){
			$dt = new DateTime();
			$currentDateTime= $dt->format('Y-m-d H:i:s');
			if($packageDate>$currentDateTime){
				$this->Session->setFlash('This pass is already active','warning');
				$this->redirect(array('controller'=>'CustomerPasses','action'=>'view'));
			}
		}
        $this->loadModel('Property');
        $freeGuestCredits=$this->Property->field('free_guest_credits',array('id'=>$this->Session->read('PropertyId')));
        $permitCost=$this->Package->field('cost',array('id'=>$permitId));
        $remainingCredits=$this->Auth->user('guest_credits');
        $this->set(compact('customerPassId','freeGuestCredits','remainingCredits','permitCost','passId','permitId'));
        
        $dt = new DateTime();
        $currentDateTime= $dt->format('Y-m-d H:i:s');
        $this->loadModel('Vehicle');
        $this->Vehicle->recursive=1;
		$this->Vehicle->unbindModel(array('hasMany'=>array('CustomerPass'))); 
		$customerOtherVehicles=$activeVehicles=array();
		$activeVehicles=$this->Vehicle->find('all', 
							array(
								'fields'=>'Vehicle.*,CustomerPass.*',
								'joins'=>array(
									array(
										'table'=>'customer_passes', 
										'alias'=>'CustomerPass',
										'type'=>'INNER',
										'conditions'=>array(
											'CustomerPass.property_id'=>$this->Session->read('PropertyId'), 
											'CustomerPass.pass_valid_upto >'=>$currentDateTime,
											'CustomerPass.membership_vaild_upto >'=>$currentDateTime,
											'CustomerPass.vehicle_id=Vehicle.id'
										)
									)
								),
								'conditions'=>array(
									'Vehicle.user_id'=>$this->Auth->user('id') 
									)
								));
		$activeIds=0;
		if(!empty($activeVehicles)){						
			$activeIds=Set::extract('/Vehicle/id',$activeVehicles);						
		}
		$this->Vehicle->recursive=-1;
		$customerOtherVehicles=$this->Vehicle->find('list',array('fields'=>array('owner'),'conditions'=>array('NOT'=>array('id'=>$activeIds),'user_id'=>$this->Auth->user('id'),'property_id'=>$this->Session->read('PropertyId'))));
		
        $this->loadModel('State');
        $state=new State();
        $states=$state->find('list');
        $this->set(compact('states','customerOtherVehicles','customerPassId','userGuestPass'));
	}
	public function checkForPayment($customerPassId=null,$passId=null,$permitId=null){
		
		$this->layout=$this->autoRender=false;
		$response=array(
						'success'=>true,
						'message'=>'',
						'beyond'=>false,
						'payment'=>false
		);
		$this->layout='customer';
		$this->loadModel('CustomerPass');
		if (!$this->CustomerPass->exists($customerPassId)) {
			$response=array(
						'success'=>false,
						'message'=>'Invalid Customer Pass',
						'beyond'=>false,
						'payment'=>false
			);
        }else{
			$userID=$this->CustomerPass->field('user_id',array('id'=>$customerPassId));
			if($userID != AuthComponent::user('id')){
				$response=array(
						'success'=>false,
						'message'=>'Invalid DATA',
						'beyond'=>false,
						'payment'=>false
				);
				 
			}
			$rfid=$this->CustomerPass->field('RFID_tag_number',array('id'=>$customerPassId));
			if(is_null($rfid)){
				$response=array(
						'success'=>false,
						'message'=>'Sorry, As RFID tag has not been assigned yet, Pass cannot be activated',
						'beyond'=>false,
						'payment'=>false
				);
			}
		}
		 $this->loadModel('Package');
		if (!$this->Package->exists($permitId)) {
			$response=array(
						'success'=>false,
						'message'=>'Invalid package',
						'beyond'=>false,
						'payment'=>false
				);
        }
        $this->loadModel('Pass');
        if (!$this->Pass->exists($passId)) {
			$response=array(
						'success'=>false,
						'message'=>'Invalid pass',
						'payment'=>false
				);
        }
        $packageDate=$this->CustomerPass->field('membership_vaild_upto',array('id'=>$customerPassId));
        if(!is_null($packageDate)){
			$dt = new DateTime();
			$currentDateTime= $dt->format('Y-m-d H:i:s');
			if($packageDate>$currentDateTime){
				$this->Session->setFlash('This pass is already active','warning');
				$this->redirect(array('controller'=>'CustomerPasses','action'=>'view'));
			}
		}
        $this->loadModel('Property');
        $freeGuestCredits=$this->Property->field('free_guest_credits',array('id'=>$this->Session->read('PropertyId')));
        $permitCost=$this->Package->field('cost',array('id'=>$permitId));
        $remainingCredits=$this->Auth->user('guest_credits');
		if($this->request->is('post') || $this->request->is('put')){ 
			$toDate=$this->request->data['Vehicle']['to'];
			$toDate = date("Y-m-d", strtotime($toDate));
			$passValidation=$this->CustomerPass->field('pass_valid_upto',array('id'=>$customerPassId));	
				$hr24=false;
				if($toDate>$passValidation){
					$response=array(
							'success'=>true,
							'message'=>'You cannot activate your permit beyond the pass expiry date. Pass can be only activated upto : '.date("m-d-Y", strtotime($passValidation)),
							'beyond'=>true,
							'payment'=>false
						);	
					$toDate=date("Y-m-d", strtotime($passValidation));
					$hr24=true;
				
				}else{
					//debug($this->request->data);
					$vehicleID=null;
					$vehicleToSet=$vehicleID='';
					$isVehicleOk=false;
					$addNewVehicle=false;
					$vehicleValidation=false;
					$this->loadModel('Vehicle'); 
					$assignedLocation=$this->request->data['Vehicle']['assigned_location'];
					if(!$this->request->data['Vehicle']['vehicle_id']){
						$vehicleToSet['Vehicle']=array('owner'=>$this->request->data['Vehicle']['owner'],
											  'make'=>$this->request->data['Vehicle']['make'],
											  'model'=>$this->request->data['Vehicle']['model'],
											  'color'=>$this->request->data['Vehicle']['color'],
											  'license_plate_number'=>$this->request->data['Vehicle']['license_plate_number'],
											  'license_plate_state'=>$this->request->data['Vehicle']['license_plate_state'],
											  'last_4_digital_of_vin'=>$this->request->data['Vehicle']['last_4_digital_of_vin'],
											  'user_id'=>$this->Auth->user('id'),
											  'property_id'=>$this->Session->read('PropertyId')
												);
						$this->Vehicle->set($vehicleToSet);
						if($this->Vehicle->validates()){
							$isVehicleOk=true;
							$addNewVehicle=true;
							$vehicleValidation=true;
						}else{
							$errors=$this->Vehicle->invalidFields();
							$finalError=array();
							foreach($errors as $field=>$error){
								$finalError[$field]=implode(", ",array_unique($error));
							}
							$finalError=array_unique(array_values($finalError));
							$finalError=implode(",<br> ",$finalError);
							$response['success'] = false;
							$response['message'] = $finalError;
						}
					}else{
						$vehicleID=$this->request->data['Vehicle']['vehicle_id'];
						$isVehicleOk=true;
						$vehicleValidation=true;
					}
					if($vehicleValidation){
						
							$dt = new DateTime();
							$currentDateTime= $dt->format('Y-m-d');
							$datetime1 = date_create($toDate);
							$datetime2 = date_create($currentDateTime);
							$interval = date_diff($datetime1, $datetime2);
							$currentCreditUsed=$interval->days;
							$newCredits=0;
							$amount=0;
							if($interval->days>0){
								if((int)$remainingCredits>=$interval->days){
									$newCredits=$remainingCredits-$interval->days;
									$amount=0;
								}else{
									$newCredits=0;
									$creditPayable=$interval->days-$remainingCredits;
									$amount=$creditPayable*$permitCost;
								}
									
								$date = new DateTime();
								$date->add(new DateInterval('P'.$interval->days.'D'));
								$permitExpiryDate=date_format($date, 'Y-m-d H:i:s');
								if($hr24){
									$permitExpiryDate=date_format($date, 'Y-m-d');
									$permitExpiryDate=$permitExpiryDate." 23:59:59"; 
								}
								if($amount!=0) {
									$this->Session->delete('toDate');
									$this->Session->write('toDate',$toDate);
									$response=array(
										'success'=>true,
										'message'=>'Make Payment',
										'beyond'=>false,
										'payment'=>false
									);					
								}
								else{
									 $this->request->data['Transaction']['card_number']='1111111111111111';
									 $this->request->data['Transaction']['cvv']='111';
									 $this->request->data['Transaction']['month']='11';
									 $this->request->data['Transaction']['year']='9999';
									 $this->request->data['Transaction']['first_name_cc']='Netgen';
									 $this->request->data['Transaction']['last_name_cc']='Solutions';				 
								
									$response=array('result'=>'Free guest credits transaction');
									$dt = new DateTime();
									$currentDateTime= $dt->format('Y-m-d H:i:s');
									$dateTransaction = $currentDateTime;
									$arraySerialize=serialize($response);
									$transactionArray['Transaction']['paypal_tranc_id']="Free Guest";
									$transactionArray['Transaction']['user_id']=$this->Auth->user('id');
									$transactionArray['Transaction']['date_time']=$dateTransaction;
									$transactionArray['Transaction']['amount']=0;
									$transactionArray['Transaction']['result']='Success';
									$transactionArray['Transaction']['pass_id']=$passId;
									$transactionArray['Transaction']['transaction_array']=$arraySerialize;
									$transactionArray['Transaction']['first_name']='Free';
									$transactionArray['Transaction']['last_name']='Guest';	
									$transactionArray['Transaction']['comments'] = "guestPassActivated";
									$this->loadModel('Transaction');
									$this->Transaction->create();
									if ($this->Transaction->save($transactionArray,false)) {
										$transactionId=$this->Transaction->getLastInsertId();
										$this->loadModel('Pass');
										$passExpiry=$this->Pass->field('expiration_date',array('id'=>$passId));
										$this->loadModel('CustomerPass');
										$this->loadModel('Vehicle');
										if(!$vehicleID){
											$vehicleToSet['Vehicle']['customer_pass_id']=$customerPassId;
											if($this->Vehicle->save($vehicleToSet,false)){
												$vehicleID=$this->Vehicle->getLastInsertId();
												CakeLog::write('NewGuestVehicle', ''.AuthComponent::user('username').' : New Guest Vehicle added for Customer Pass ID: <a href="/admin/CustomerPasses/edit/'.$customerPassId.'">'.$customerPassId.'</a> by User : '.AuthComponent::user('first_name').' '.AuthComponent::user('last_name').' In Property '.CakeSession::read('PropertyName').' AND NEW VEHICLE ID : '.$vehicleID);
											}
											
										}
										$this->Vehicle->updateAll(array('Vehicle.customer_pass_id'=>NULL),array('Vehicle.customer_pass_id'=>$customerPassId));
										$this->CustomerPass->updateAll(array('CustomerPass.vehicle_id'=>NULL),array('CustomerPass.vehicle_id'=>$vehicleID));
										$customerPassArray['CustomerPass']['transaction_id']=$transactionId;
										$customerPassArray['CustomerPass']['vehicle_id']=$vehicleID;
										$customerPassArray['CustomerPass']['membership_vaild_upto']= $permitExpiryDate;
										$customerPassArray['CustomerPass']['id']=$customerPassId;
										$customerPassArray['CustomerPass']['package_id']= $permitId;
										$customerPassArray['CustomerPass']['is_guest_pass']=1;
										if($assignedLocation){
											$customerPassArray['CustomerPass']['assigned_location']=$assignedLocation;
										}
										if($this->CustomerPass->save($customerPassArray,false)){
											$this->Vehicle->updateAll(array('Vehicle.customer_pass_id'=>$customerPassId),array('Vehicle.id'=>$vehicleID));
											$vehicleInfo=$this->Vehicle->field('vehicle_info',array('id'=>$vehicleID));
											$userPassArray=array('UserGuestPass'=>array('user_id'=>AuthComponent::user('id'),
																						'customer_pass_id'=>$customerPassId,
																						'vehicle_id'=>$vehicleID,
																						'vehicle_details'=>$vehicleInfo,
																						'days'=>$currentCreditUsed,
																						'amount'=>$amount,
																						'to_date'=>$permitExpiryDate,
																						'property_id'=>$this->Session->read('PropertyId')
											));
											
											$this->loadModel('UserGuestPass');
											if($this->UserGuestPass->save($userPassArray,false)){
												$userGuestPassId=	$this->UserGuestPass->getLastInsertID();
												CakeLog::write('guestPassUpdated', ''.AuthComponent::user('username').' : Guest Pass Updated, Customer Pass ID: <a href="/admin/CustomerPasses/edit/'.$customerPassId.'">'.$customerPassId.'</a> by User : '.AuthComponent::user('first_name').' '.AuthComponent::user('last_name').' In Property '.CakeSession::read('PropertyName').'For '.$currentCreditUsed.' Days, Amount Paid: $'.$amount.' AND VEHICLE ID  IS : '.$vehicleID.' AND VEHICLE INFO : '.$vehicleInfo);
											
												$this->loadModel('User');
												$user['User']['id']=$this->Auth->user('id');
												$user['User']['guest_credits']=$newCredits;
												$this->User->clear();
												if($this->User->save($user,false)){
													$_SESSION['Auth']['User']['guest_credits']=$newCredits;
													$this->Session->setFlash('Please add vehicle details now.','success');
													$this->Session->delete('Allowed');
													$this->Session->write('Allowed',true);
													$response=array(
														'success'=>true,
														'message'=>'Make Payment',
														'payment'=>true,
														'beyond'=>false,
														'userguestpass'=>$userGuestPassId
													);
												}else{
													$response=array(
														'success'=>false,
														'message'=>'Internal Error Has Occured, Please Contact Admin',
														'beyond'=>false,
														'payment'=>false
													);														
												}
										  }else{
											  $response=array(
													'success'=>false,
													'message'=>'Internal Error Has Occured, Please Contact Admin',
													'beyond'=>false,
													'payment'=>false
												);						
										  }
										}else{
											$response=array(
													'success'=>false,
													'message'=>'Error while saving transaction, Please try again',
													'beyond'=>false,
													'payment'=>false
											);
										}												
								}else {
									$response=array(
											'success'=>false,
											'message'=>'Error while saving transaction, Please try again',
											'beyond'=>false,
											'payment'=>false
									);											
								}
							}			
						}else{
							CakeLog::write('zeroDaysError', ''.AuthComponent::user('username').' : FROM DATE : '.$currentDateTime.' TO DATE : '.$toDate.' INTERVAL DAYS : '.$interval->days );
							$response=array(
											'success'=>false,
											'message'=>'From and to date cannot be same. If they are not same and you are still getting the error please contact admin',
											'beyond'=>false,
											'payment'=>false
									);	
						}
					}					
				}			
		}else{
			$response=array(
						'success'=>false,
						'message'=>'Invalid Request',
						'beyond'=>false,
						'payment'=>false
				);
		}
		echo json_encode($response);
		
	}
	/*******************************************************************
	 * admin_renwed_pass
	 * 
	 */
	 public function admin_renewed_pass(){
		$this->loadModel('Property');
		$property_list=$this->Property->find('list',array('fields'=>'name'));
		$id=array_keys($property_list);
		$selectedId=$id[0];
		$this->loadModel('Pass');
		$passes_list=$this->Pass->find('list',array('fields'=>'name','conditions'=>array('property_id'=>$selectedId)));
		$pass_id=array_keys($property_list);
		$selectedPassId=$pass_id[0];
		$this->set(compact('property_list','selectedId','passes_list','selectedPassId'));
	 }
	 public function unapproved_users(){
		$this->layout='manager';
	 }
	 public function admin_pass_approval(){
		$propertyId='';
		if($this->Auth->user('role_id')==3){
		 $propertyId=$this->Session->read('PropertyId');
		}else{
			$propertyId=$this->request->query['property'];
		}
		$approved=$this->request->query['type']; 
		$conditions='';
		if($propertyId){
			$conditions.=' AND cp.property_id='.$propertyId;
		}
		if($approved !=''){
			$conditions.=' AND cp.approved='.$approved;
		}
		$aColumns = array(
							'user.first_name', 
							'property.name',
							'pass.name',
							'vehicle.owner', 
							'vehicle.license_plate_number', 
							'cp.pass_valid_upto', 
							'cp.membership_vaild_upto',
							'cp.approved',
							'cp.id',
							'cp.user_id',
							'user.last_name',
							'user.username',
							'user.email',
							'user.address_line_1',
							'user.address_line_2',
							'user.city',
							'user.state',
							'user.zip',
							'user.phone',
							'vehicle.make',
							'vehicle.model',
							'vehicle.color', 
							'vehicle.license_plate_state', 
							'vehicle.last_4_digital_of_vin' 
						);
        $sIndexColumn = " cp.id ";
        $sTable = " customer_passes cp ";
        $sJoinTable=' LEFT JOIN vehicles vehicle ON cp.vehicle_id=vehicle.id
					  INNER JOIN users user ON user.id=cp.user_id
					  INNER JOIN passes pass ON pass.id=cp.pass_id
					  INNER JOIN properties property ON property.id=cp.property_id
					  ';
        $sConditions=' cp.RFID_tag_number IS NULL AND  cp.pass_archived=0 '.$conditions;
       
        $returnArr=$this->NewDatatable->getData(array('columns'=>$aColumns,'index_column'=>$sIndexColumn,'table'=>$sTable,'join'=>$sJoinTable,'conditions'=>$sConditions));
        echo json_encode($returnArr);
        die;  
	 }
	public function approve($id){
		$this->layout=false;
		if($this->request->is('POST') || $this->request->is('PUT')){
			$this->request->data['CustomerPass']['approved']=1;
			$validateApprove=array(
				 'pass_valid_upto'=> array(
					'date and time' => array(
					   'rule' => 'checkdate',
						'message' => 'Incorrect Date Time',
						'allowEmpty' => false,
						'required' => true, 
					)
				),
				'membership_vaild_upto'=> array(
					'date and time' => array(
						'rule' => 'checkdate',
						'message' => 'Incorrect Date Time',
						'allowEmpty' => true,
						'required' => false, 
					)
				)
			
			);
			$this->CustomerPass->validate=$validateApprove;
			$this->CustomerPass->set($this->request->data);
			if($this->CustomerPass->validates()){
				$this->CustomerPass->recursive=-1;
				$currentPass=$this->CustomerPass->find('first',array('conditions'=>array('CustomerPass.id'=>$this->request->data['CustomerPass']['id'])));
				
				if (isset($this->request->data['CustomerPass']['pass_valid_upto'])) {
					list($date,$time)=explode(' ',$currentPass['CustomerPass']['pass_valid_upto']);
					$this->request->data['CustomerPass']['pass_valid_upto']=$this->request->data['CustomerPass']['pass_valid_upto'].' '.$time;
                    $this->request->data['CustomerPass']['pass_valid_upto'] = date("Y-m-d H:i:s", strtotime($this->request->data['CustomerPass']['pass_valid_upto']));
                }
                if (isset($this->request->data['CustomerPass']['membership_vaild_upto']) && $this->request->data['CustomerPass']['membership_vaild_upto']) {
                    list($date,$time)=explode(' ',$currentPass['CustomerPass']['membership_vaild_upto']);
					$this->request->data['CustomerPass']['membership_vaild_upto']=$this->request->data['CustomerPass']['membership_vaild_upto'].' '.$time;
                    $this->request->data['CustomerPass']['membership_vaild_upto'] = date("Y-m-d H:i:s", strtotime($this->request->data['CustomerPass']['membership_vaild_upto']));
                } else {
                    $this->request->data['CustomerPass']['membership_vaild_upto'] = NULL;
                }
				$this->CustomerPass->clear();
				if($this->CustomerPass->save($this->request->data,array('validate' => false))){
					$this->Session->setFlash('Pass approved successfully.', 'success');
				}else{
					$this->Session->setFlash('Pass could not be approved. Please try again', 'error');
				}
			}else{
				$this->Session->setFlash('Pass could not be approved. Please check the dates properly', 'error');
			}
			return $this->redirect(['action'=>'unapproved_users']);
		}
		$this->CustomerPass->recursive=-1;
		$customerPass=$this->CustomerPass->find('first',array(
															  'conditions'=>array(
																				'CustomerPass.id'=>$id
															  )
														));
		$this->set(compact('customerPass'));
	}
	private function processDelete($id){
		$this->CustomerPass->id = $id;
		if (!$this->CustomerPass->exists()) {
			throw new NotFoundException(__('Invalid customer pass'));
		}
		$this->CustomerPass->unbindModel(array('belongsTo'=>'Transaction'));
		$customerPassData=$this->CustomerPass->find('first',array('conditions'=>array('CustomerPass.id'=>$id)));
		
		$this->loadModel('DeletedCustomerPass');
		$arr=array(
					'by_user'=>AuthComponent::user('id'),
					'of_user'=>$customerPassData['User']['id'],
					'pass_data'=>json_encode($customerPassData)
		);
		if($this->DeletedCustomerPass->save($arr)){
			//Add Refund Entry
			$this->loadModel('Pass');
			$passName=$this->Pass->givePassName($customerPassData['CustomerPass']['pass_id']);
			$resfundArr['Refund']=[
									'user_id'=>$customerPassData['CustomerPass']['user_id'],
									'property_id'=>$customerPassData['CustomerPass']['property_id'],
									'transaction_id'=>$customerPassData['CustomerPass']['transaction_id'],
									'pass'=>$passName,
									'permit'=>$customerPassData['Package']['name'],
									'pass_cost'=>$customerPassData['CustomerPass']['pass_cost'],
									'pass_deposit'=>$customerPassData['CustomerPass']['pass_deposit'],
									'package_cost'=>$customerPassData['CustomerPass']['package_cost'],
									'total_cost'=>$customerPassData['CustomerPass']['pass_cost']+$customerPassData['CustomerPass']['pass_deposit']+$customerPassData['CustomerPass']['package_cost'],
									'deleted_customer_pass_id'=>$this->DeletedCustomerPass->getLastInsertID()
								  ];
			
			$this->loadModel('Refund');
			if($this->Refund->save($resfundArr)){
				CakeLog::write('deletePassRefund', ''.AuthComponent::user('username').' BACKUP DONE CUSTOMER PASS ID : '.$id.' Pass Name : '.$passName.' OF USER ID : '.$customerPassData['User']['id'].' WITH REFUND ID : '.$this->Refund->getLastInsertID());
			}
			CakeLog::write('deletePassInitiated', ''.AuthComponent::user('username').' BACKUP DONE CUSTOMER PASS ID : '.$id);
			if($customerPassData['CustomerPass']['vehicle_id']){
				$this->loadModel('Vehicle');
				$this->CustomerPass->updateAll(array('CustomerPass.vehicle_id'=>NULL),array('CustomerPass.id'=>$id));
				if($this->Vehicle->updateAll(array('Vehicle.customer_pass_id'=>NULL),array('Vehicle.customer_pass_id'=>$id))){
					CakeLog::write('vehicleDetached', ''.AuthComponent::user('username').' Vehicle Removed VEHICLE ID : '.$customerPassData['CustomerPass']['vehicle_id'].' CUSTOMER PASS ID : '.$id);
				}
			}
			//DELETE ChangeVehicleDetail Requests
			$this->loadModel('ChangeVehicleDetail');
			$this->ChangeVehicleDetail->deleteAll(array('ChangeVehicleDetail.customer_pass_id' =>$id), false);
			//DELETE BACKUPS
			$this->loadModel('CustomerPassBackup');
			$this->CustomerPassBackup->deleteAll(array('CustomerPassBackup.previous_id' =>$id), false);
			//DELETE BACKUPS
			$this->loadModel('LastSeenVehicle');
			$this->LastSeenVehicle->deleteAll(array('LastSeenVehicle.customer_pass_id' =>$id), false);
			
			$this->loadModel('RecurringProfile');
			$recurringProfile=$this->RecurringProfile->find('all',array('conditions'=>array('RecurringProfile.customer_pass_id'=>$id)));
			if($recurringProfile){
				foreach($recurringProfile as $recProfile){
					$result=$this->PayFlow->deactivate_profile($recProfile['RecurringProfile']['recurring_profile_id']);
					if($result){
						if($result['RESULT']==0 && $result['RESPMSG']=='Approved'){
								$arr1['RecurringProfile']=array('id'=>$recProfile['RecurringProfile']['id'],
																'status'=>'Cancelled'
															);
								if($this->RecurringProfile->save($arr1,false)){
									CakeLog::write('ProfileDeactivation',$recProfile['RecurringProfile']['id'].' Recurring Profile deactivation COMPLETED... RECURRING PROFILE ID : '.$recProfile['RecurringProfile']['id'].'  CUSTOMER PASS ID : '.$id);
								}else{
									CakeLog::write('ProfileDeactivation',$recProfile['RecurringProfile']['id'].' Recurring Profile deactivation DONE BUT NOT SAVED... RECURRING PROFILE ID : '.$recProfile['RecurringProfile']['id'].'  CUSTOMER PASS ID : '.$id);
								}
						}else{
							CakeLog::write('ProfileDeactivation',$recProfile['RecurringProfile']['id'].' Recurring Profile deactivation failed... RECURRING PROFILE ID : '.$recProfile['RecurringProfile']['id'].'  CUSTOMER PASS ID : '.$id);
						}
					}else{
						CakeLog::write('DeactivateRecurring', ''.AuthComponent::user('username').' NO RESULT....RECURRING PROFILE ID : '.$recProfile['RecurringProfile']['id'].' CUSTOMER PASS ID : '.$id);
					}
				}
			}
			//DELETE UserGuestPass Data
			$this->loadModel('UserGuestPass');
			$this->UserGuestPass->deleteAll(array('UserGuestPass.customer_pass_id' =>$id), false);
			
			$this->request->allowMethod('post', 'delete');
			if ($this->CustomerPass->delete()) {
				CakeLog::write('deletePassCompleted', ''.AuthComponent::user('username').' DELETED CUSTOMER PASS ID : '.$id);
				return true;
			} else {
				return false;
			}	
		}else{
			return false;
		}
		return false;
	}
	
	
}
