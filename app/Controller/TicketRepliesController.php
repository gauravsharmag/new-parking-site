<?php

class TicketRepliesController extends AppController {

    var $helpers = array('Js');
    var $components = array('RequestHandler','Email');

    public function beforeFilter() {
        parent::beforeFilter();
    }

    public function view_single($id = null) {
        $this->layout = 'customer';
        $ticket = $this->TicketReply->Ticket->findById($id);
         if (!$ticket) {
                throw new NotFoundException(__('Invalid ticket'));
            }
        if ($ticket['Ticket']['user_id'] == $this->Auth->user('id') || $ticket['Ticket']['recipient_id']== $this->Auth->user('id')  ) {
            $ticketReply = $this->TicketReply->find('all', array('conditions' => array('TicketReply.ticket_id' => $id)));
            $this->TicketReply->Ticket->updateAll(array('user_reply' => 2), array('Ticket.id' => $ticket['Ticket']['id']));
            $log = $this->Auth->user();
            $this->set(array( 'authUser' => $log, 'ticketReply' => $ticketReply, 'ticket' => $ticket));
        } else {
            throw new NotFoundException('You are not authorised for that location');
        }
    }

    public function add() {
		$this->loadModel('PropertyUser');
			  
        if ($this->request->is('post') || $this->request->is('put')) {
            $this->TicketReply->create();
			 
            if ($this->TicketReply->save($this->request->data)) {
                $this->TicketReply->Ticket->updateAll(
                        array(
                    'reply_admin' => 0,
                   'user_reply' => 1,
                    'status' => $this->request->data['TicketReply']['status']
                        ), array(
                    'Ticket.id' => $this->request->data['TicketReply']['ticket_id']
                        )
                );
                $this->loadModel('PropertyUser');
			    $propertyManager=$this->PropertyUser->find('first',array('conditions'=>array('property_id'=>$_SESSION['PropertyId'],'PropertyUser.role_id'=>3),'fields'=>array('User.email')));
                if($propertyManager){
					$this->sendEmail($this->request->data['TicketReply']['ticket_id'],$this->request->data['TicketReply']['reply_body'],$propertyManager['User']['email']);
				}
                
                $this->Session->setFlash('Message sent', 'success');
                return $this->redirect(array('action' => 'view_single', $this->request->data['TicketReply']['ticket_id']));
            }
            $this->Session->setFlash('Message not sent.', 'error');
            return $this->redirect($this->referer());
        }
    }

    public function admin_view_single($id = null) {
        $this->Session->write('PropertySelection', false);
        $ticket = $this->TicketReply->Ticket->findById($id);
        $this->loadModel('User');
        $sender = $this->User->findById($ticket['Ticket']['user_id']);
        $ticketReply = $this->TicketReply->find('all', array('conditions' => array('TicketReply.ticket_id' => $id)));
        if ($this->Auth->user('role_id') == 2) {
           // $this->TicketReply->Ticket->updateAll(array('reply_admin' => 2), array('Ticket.id' => $ticket['Ticket']['id']));
        } else {
           // $this->TicketReply->Ticket->updateAll(array('reply_admin' => 2), array('Ticket.id' => $ticket['Ticket']['id']));
        }
        $user_id = $this->Auth->user('id');
        $name = $this->Auth->user('username');
        $log = $this->Auth->user();
        $this->set(array('name' => $name, 'user_id' => $user_id, 'authUser' => $log, 'ticketReply' => $ticketReply, 'sender' => $sender, 'ticket' => $ticket));
    }

    public function manager_view_single($id = null) {
        $this->layout = "manager";
        $ticket = $this->TicketReply->Ticket->findById($id);
        $ticketReply = $this->TicketReply->find('all', array('conditions' => array('TicketReply.ticket_id' => $id)));
        $this->loadModel('User');
        $sender = $this->User->findById($ticket['Ticket']['user_id']);
        //$this->TicketReply->Ticket->updateAll(array('reply_manager' => 2), array('Ticket.id' => $ticket['Ticket']['id']));
        $user_id = $this->Auth->user('id');
        $name = $this->Auth->user('username');
        $log = $this->Auth->user();
        $this->set(array('name' => $name, 'sender' => $sender, 'user_id' => $user_id, 'authUser' => $log, 'ticketReply' => $ticketReply, 'ticket' => $ticket));
    }

    public function admin_add() {
        $this->render = false;
        if($this->request->data['TicketReply']['reply_body']){
			if ($this->request->is('post')) {
				$this->TicketReply->create();
				if ($this->TicketReply->save($this->request->data)) {
					$ticket_id = $this->TicketReply->field('ticket_id', array('id' => $this->TicketReply->getLastInsertID()));
					$this->TicketReply->Ticket->updateAll(array('reply_admin' =>1,  'user_reply' => 0), array('Ticket.id' => $this->request->data['TicketReply']['ticket_id']));
					$this->sendEmail($this->request->data['TicketReply']['ticket_id'],$this->request->data['TicketReply']['reply_body']);
					$this->Session->setFlash('Message sent', 'success');
					return $this->redirect(array('action' => 'admin_view_single', $this->request->data['TicketReply']['ticket_id']));
				}
				$this->Session->setFlash('Message not sent.', 'error');
			}
		}else{
			$this->Session->setFlash('Ticket Reply cannot be left empty.', 'error');
			return $this->redirect(array('action' => 'manager_view_single', $this->request->data['TicketReply']['ticket_id']));
		}
    }

    public function manager_add() {
        $this->render = false;
		if($this->request->data['TicketReply']['reply_body']){
			if ($this->request->is('post')) {
				$this->TicketReply->create();
				if ($this->TicketReply->save($this->request->data)) {
					$ticket_id = $this->TicketReply->field('ticket_id', array('id' => $this->TicketReply->getLastInsertID()));
					$this->sendEmail($this->request->data['TicketReply']['ticket_id'],$this->request->data['TicketReply']['reply_body']);
					$this->Session->setFlash('Message sent', 'success');
					return $this->redirect(array('action' => 'manager_view_single', $this->request->data['TicketReply']['ticket_id']));
				}
				$this->Session->setFlash('Message not sent.', 'error');
			}
		}else{
			$this->Session->setFlash('Ticket Reply cannot be left empty.', 'error');
			return $this->redirect(array('action' => 'manager_view_single', $this->request->data['TicketReply']['ticket_id']));
		}
    }
	private function sendEmail($ticketID,$replyData,$managerEmail=false){
		 $ticketData=$this->TicketReply->Ticket->find('first', 
				array('fields'=>array('Ticket.*','Recipient.*','pu.*','p.*'),
					  'conditions' => array('Ticket.id' => $ticketID),
					  'joins'=>array(
									array(
										'table' => 'property_users',
										'alias' => 'pu',
										'type' => 'LEFT',
										'conditions' => array(
											'pu.user_id = Recipient.id'
										)
									),
									array(
										'table' => 'properties',
										'alias' => 'p',
										'type' => 'LEFT',
										'conditions' => array(
											'p.id = pu.property_id'
										)
									)
					  )
				));
		if(isset($ticketData['Recipient']['email']) && $ticketData['Recipient']['email']){
			$ticketData['Recipient']['email']=trim($ticketData['Recipient']['email']);
			$toUser=$ticketData['Recipient']['email'];
			if($managerEmail){
				$toUser=$managerEmail;
			}
			if($ticketData){
				$protocol = stripos($_SERVER['SERVER_PROTOCOL'], 'https') === true ? 'https://' : 'http://';
				$url = $protocol.trim($ticketData['p']['sub_domain']).'.'.Configure::read('SITE_URL') . "/ticket_replies/view_single/" .$ticketData['Ticket']['id'];
				if($managerEmail){
					$url = $protocol.trim($ticketData['p']['sub_domain']).'.'.Configure::read('SITE_URL') . "/ticket_replies/manager_view_single/" .$ticketData['Ticket']['id'];
				}
				$url2 = $protocol.trim($ticketData['p']['sub_domain']).'.'.Configure::read('SITE_URL') . "/users/login";
				$propertyName=$ticketData['p']['name'];
				$ms = $url;
				$ms = wordwrap($ms, 1000);
				$ms2 = $url2;
				$ms2 = wordwrap($ms2, 1000);
				$this->Email->template = 'reply';
				$this->Email->from = 'Ticket Reply' . ' <alerts@internetparkingpass.com>';
				$this->Email->to = $toUser;
				$this->Email->subject = $ticketData['Ticket']['subject'];
				$this->Email->sendAs = 'html';
				$msg_line_1="<h3><b>TICKET :</b></h3>".$ticketData['Ticket']['message'];
				$msg_line_2="<hr><h3><b>REPLY :</b></h3>".$replyData;
				if($managerEmail){
					$msg_line_2="<hr><h3><b>CUSTOMER REPLY :</b></h3>".$replyData;
				}
				$this->set(compact('url','url2','msg_line_2','msg_line_1','propertyName','managerEmail'));
				$this->Email->smtpOptions=Configure::read('SMTP_SETTING');
				$this->Email->delivery='smtp';
				$this->Email->send();
			 }
		}else{
			$this->Session->setFlash('It seems that either the email is not valid or does not exist. Please verify the user email','error');
			
		}
	}		
}

?>
