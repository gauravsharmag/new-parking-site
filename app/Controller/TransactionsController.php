<?php

App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');

/**
 * Transactions Controller
 *
 * @property Transaction $Transaction
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class TransactionsController extends AppController {

    var $pass = array();
    var $totalPassCost = 0;

    public function beforeFilter() {
        parent::beforeFilter();
        $this->Security->validatePost = false;
        //$this->Auth->allow();
    } 

    public $components = array('Paginator',
							   'Session', 
							   'Paypal', 
							   'PaypalIPNListener', 
							   'Email',
							   'PayFlow',
							   'NewDatatable'
							);

    public function admin_index() {
        $this->Transaction->recursive = 0;
        $this->set('transactions', $this->Paginator->paginate());
    }

    /**
     * admin_view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_view($id = null) {
        if (!$this->Transaction->exists($id)) {
            throw new NotFoundException(__('Invalid transaction'));
        }
        $options = array('conditions' => array('Transaction.' . $this->Transaction->primaryKey => $id));
        $this->set('transaction', $this->Transaction->find('first', $options));
    }

    /**
     * admin_add method
     *
     * @return void
     */
    public function admin_add() {
        if ($this->request->is('post')) {
            $this->Transaction->create();
            if ($this->Transaction->save($this->request->data)) {
                $this->Session->setFlash(__('The transaction has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The transaction could not be saved. Please, try again.'));
            }
        }
        $users = $this->Transaction->User->find('list');
        $passes = $this->Transaction->Pass->find('list');
        $this->set(compact('users', 'passes'));
    }

    /**
     * admin_edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_edit($id = null) {
        if (!$this->Transaction->exists($id)) {
            throw new NotFoundException(__('Invalid transaction'));
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($this->Transaction->save($this->request->data)) {
                $this->Session->setFlash(__('The transaction has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The transaction could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('Transaction.' . $this->Transaction->primaryKey => $id));
            $this->request->data = $this->Transaction->find('first', $options);
        }
        $users = $this->Transaction->User->find('list');
        $passes = $this->Transaction->Pass->find('list');
        $this->set(compact('users', 'passes'));
    }

    /**
     * admin_delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function admin_delete($id = null) {
        $this->Transaction->id = $id;
        if (!$this->Transaction->exists()) {
            throw new NotFoundException(__('Invalid transaction'));
        }
        $this->request->allowMethod('post', 'delete');
        if ($this->Transaction->delete()) {
            $this->Session->setFlash(__('The transaction has been deleted.'));
        } else {
            $this->Session->setFlash(__('The transaction could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }

    public function order_result() {
        $this->layout = 'customer';
    }

    public function check_out($passId = null, $packageId = null) {

        $this->loadModel('CustomerPass');
        $customerPass = new CustomerPass();
        $dt = new DateTime();
        $currentDateTime = $dt->format('Y-m-d H:i:s');
        $this->layout = 'customer';


        $this->loadModel('Package');
        $package = new Package();
        $package->recursive = -1;
        $packageDetails = $package->find('first', array('conditions' => array('id' => $packageId)));

        if ($packageDetails['Package']['is_fixed_duration'] == 0) {
            switch ($packageDetails['Package']['duration_type']) {
                case "Hour":
                    $date = new DateTime($currentDateTime);
                    $k = (int) $packageDetails['Package']['duration'];
                    $date->add(new DateInterval('PT' . $k . 'H'));
                    $packageDetails['Package']['expiration_date'] = date_format($date, 'Y-m-d H:i:s');
                    break;
                case "Day":
                    $date = new DateTime($currentDateTime);
                    $k = $packageDetails['Package']['duration'];
                    $date->add(new DateInterval('P' . $k . 'D'));
                    $packageDetails['Package']['expiration_date'] = date_format($date, 'Y-m-d H:i:s');
                    break;
                case "Week":
                    $date = new DateTime($currentDateTime);
                    $k = (int) $packageDetails['Package']['duration'] * 7;
                    $date->add(new DateInterval('P' . $k . 'D'));
                    $packageDetails['Package']['expiration_date'] = date_format($date, 'Y-m-d H:i:s');
                    break;
                case "Month":
                    $date = new DateTime($currentDateTime);
                    $k = (int) $packageDetails['Package']['duration'];
                    $date->add(new DateInterval('P' . $k . 'M'));
                    $packageDetails['Package']['expiration_date'] = date_format($date, 'Y-m-d H:i:s');
                    break;
                case "Year":
                    $date = new DateTime($currentDateTime);
                    $k = (int) $packageDetails['Package']['duration'];
                    $date->add(new DateInterval('P' . $k . 'Y'));
                    $packageDetails['Package']['expiration_date'] = date_format($date, 'Y-m-d H:i:s');
                    break;
            }
        }

        $customerPass->recursive = -1;
        $isPassExists = $customerPass->hasAny(array('pass_id' => $passId));


        if ($isPassExists == false) {
            $this->loadModel('Pass');
            $pass = new Pass();
            $pass->recursive = -1;
            $remainingPassDetails = $pass->find('first', array('conditions' => array('id' => $passId)));
            if ($remainingPassDetails['Pass']['is_fixed_duration'] == 0) {
                switch ($remainingPassDetails['Pass']['duration_type']) {
                    case "Hour":
                        $date = new DateTime($currentDateTime);
                        $k = (int) $remainingPassDetails['Pass']['duration'];
                        $date->add(new DateInterval('PT' . $k . 'H'));
                        $remainingPassDetails['Pass']['expiration_date'] = date_format($date, 'Y-m-d H:i:s');
                        break;
                    case "Day":
                        $date = new DateTime($currentDateTime);
                        $k = $remainingPassDetails['Pass']['duration'];
                        $date->add(new DateInterval('P' . $k . 'D'));
                        $remainingPassDetails['Pass']['expiration_date'] = date_format($date, 'Y-m-d H:i:s');
                        break;
                    case "Week":
                        $date = new DateTime($currentDateTime);
                        $k = (int) $remainingPassDetails['Pass']['duration'] * 7;
                        $date->add(new DateInterval('P' . $k . 'D'));
                        $remainingPassDetails['Pass']['expiration_date'] = date_format($date, 'Y-m-d H:i:s');
                        break;
                    case "Month":
                        $date = new DateTime($currentDateTime);
                        $k = (int) $remainingPassDetails['Pass']['duration'];
                        $date->add(new DateInterval('P' . $k . 'M'));
                        $remainingPassDetails['Pass']['expiration_date'] = date_format($date, 'Y-m-d H:i:s');
                        break;
                    case "Year":
                        $date = new DateTime($currentDateTime);
                        $k = (int) $remainingPassDetails['Pass']['duration'];
                        $date->add(new DateInterval('P' . $k . 'Y'));
                        $remainingPassDetails['Pass']['expiration_date'] = date_format($date, 'Y-m-d H:i:s');
                        break;
                }
            }
            $remainingPassDetails['Pass']['cost_after_1st_year'] = 0;
            $this->pass = $remainingPassDetails;
        } else {


            $noPermit = $customerPass->hasAny(array('pass_id' => $passId, 'package_id' => null, 'user_id' => $this->Auth->user('id')));
            $expiredPermit = $customerPass->hasAny(array('pass_id' => $passId, ' membership_vaild_upto <' => $currentDateTime, 'user_id' => $this->Auth->user('id')));

            if ($noPermit == true || $expiredPermit == true) {
                $this->loadModel('Pass');
                $this->loadModel('Pass');
                $pass = new Pass();
                $pass->recursive = -1;
                $passDetails = $pass->find('first', array('conditions' => array('id' => $passId)));
                $passDetails['Pass']['cost_after_1st_year'] = 0;
                $passDetails['Pass']['cost_1st_year'] = 0;
                $passDetails['Pass']['deposit'] = 0;
                $passDetails['Pass']['expiration_date'] = $customerPass->field('pass_valid_upto', array('pass_id' => $passId));
                $this->pass = $passDetails;
            } else {
                $this->loadModel('Pass');
                $pass = new Pass();
                $pass->recursive = -1;
                $renewPassDetails = $pass->find('first', array('conditions' => array('id' => $passId)));
                if ($renewPassDetails['Pass']['is_fixed_duration'] == 0) {
                    switch ($renewPassDetails['Pass']['duration_type']) {
                        case "Hour":
                            $date = new DateTime($currentDateTime);
                            $k = (int) $renewPassDetails['Pass']['duration'];
                            $date->add(new DateInterval('PT' . $k . 'H'));
                            $renewPassDetails['Pass']['expiration_date'] = date_format($date, 'Y-m-d H:i:s');
                            break;
                        case "Day":
                            $date = new DateTime($currentDateTime);
                            $k = $renewPassDetails['Pass']['duration'];
                            $date->add(new DateInterval('P' . $k . 'D'));
                            $renewPassDetails['Pass']['expiration_date'] = date_format($date, 'Y-m-d H:i:s');
                            break;
                        case "Week":
                            $date = new DateTime($currentDateTime);
                            $k = (int) $renewPassDetails['Pass']['duration'] * 7;
                            $date->add(new DateInterval('P' . $k . 'D'));
                            $renewPassDetails['Pass']['expiration_date'] = date_format($date, 'Y-m-d H:i:s');
                            break;
                        case "Month":
                            $date = new DateTime($currentDateTime);
                            $k = (int) $renewPassDetails['Pass']['duration'];
                            $date->add(new DateInterval('P' . $k . 'M'));
                            $renewPassDetails['Pass']['expiration_date'] = date_format($date, 'Y-m-d H:i:s');
                            break;
                        case "Year":
                            $date = new DateTime($currentDateTime);
                            $k = (int) $renewPassDetails['Pass']['duration'];
                            $date->add(new DateInterval('P' . $k . 'Y'));
                            $renewPassDetails['Pass']['expiration_date'] = date_format($date, 'Y-m-d H:i:s');
                            break;
                    }
                }
                $renewPassDetails['Pass']['cost_1st_year'] = 0;
                $renewPassDetails['Pass']['deposit'] = 0;
                $this->pass = $renewPassDetails;
            }
        }
        $pass = $this->pass;



        if ($this->request->is('post')) {

            debug($pass);
            debug($this->request->data);
            die();
            $this->request->data['Transaction']['user_id'] = $this->Auth->user('id');

            $this->loadModel('Package');
            $package = new Package();
            $package->recursive = -1;
            $packageCost = $package->find('first', array('fields' => array('cost'), 'conditions' => array('id' => $this->request->data['Transaction']['packageId'])));


            $customerPass->recursive = -1;
            $isPassExists = $customerPass->hasAny(array('pass_id' => $this->request->data['Transaction']['pass_id']));
            if ($isPassExists == false) {
                $this->loadModel('Pass');
                $pass = new Pass();
                $pass->recursive = -1;
                $remainingPassCost = $pass->find('first', array('fields' => array('cost_1st_year', 'deposit'), 'conditions' => array('id' => $passId)));
                //debug($remainingPassCost);
                $this->totalPassCost = $remainingPassCost['Pass']['cost_1st_year'] + $remainingPassCost['Pass']['deposit'];
                //debug($totalPassCost);
            } else {
                $passValidPackageExpireDetails = $customerPass->find('first', array('conditions' => array('pass_id' => $this->request->data['Transaction']['pass_id'], 'AND' => array('pass_valid_upto >' => $currentDateTime, 'OR' => array('membership_vaild_upto <' => $currentDateTime, 'membership_vaild_upto' => NULL)))));
                //$passValidPackageExpireDetails=$customerPass->hasAny(array('pass_id'=>$this->request->data['Transaction']['pass_id'],'pass_valid_upto >'=> $currentDateTime,'membership_vaild_upto <'=> $currentDateTime));
                if (!empty($passValidPackageExpireDetails)) {
                    $this->totalPassCost = 0;
                } else {
                    $this->loadModel('Pass');
                    $pass = new Pass();
                    $pass->recursive = -1;
                    $renewPassCost = $pass->find('first', array('fields' => array('cost_after_1st_year'), 'conditions' => array('id' => $this->request->data['Transaction']['pass_id'])));
                    $this->totalPassCost = $renewPassCost['Pass']['cost_after_1st_year'];
                }
            }

            /*
              Test Acount details
              Credit card number:
              4223544948124359
              Credit card type:
              Visa
              Expiration date:
              12/2017
              CVV:
              123

             */

            $amount = (int) $packageCost['Package']['cost'] + (int) $this->totalPassCost;
            if ($amount == 0) {
                $this->request->data['Transaction']['card_number'] = '1111111111111111';
                $this->request->data['Transaction']['cvv'] = '111';
                $this->request->data['Transaction']['month'] = '11';
                $this->request->data['Transaction']['year'] = '9999';
                $this->request->data['Transaction']['first_name_cc'] = 'Netgen';
                $this->request->data['Transaction']['last_name_cc'] = 'Solutions';
            }


            $this->Transaction->set($this->request->data);
            if ($this->Transaction->validates()) {

                //debug($amount);
                //die();
                if ($amount != 0) {
                    $payment = array(
                        'amount' => $amount,
                        'card' => $this->request->data['Transaction']['card_number'], // This is a sandbox CC
                        'expiry' => array(
                            'M' => $this->request->data['Transaction']['month'],
                            'Y' => $this->request->data['Transaction']['year'],
                        ),
                        'cvv' => $this->request->data['Transaction']['cvv'],
                        'currency' => 'USD' // Defaults to GBP if not provided
                    );
                    $errorMessage = null;
                    $response = null;
                    try {
                        $response = $this->Paypal->doDirectPayment($payment);
                        // Check $response[ACK]='Success' OR 'OK'
                    } catch (Exception $e) {
                        $errorMessage = $e->getMessage();
                    }
                    // debug($errorMessage);
                    //debug($response);
                } else {
                    $errorMessage = null;
                    $response['ACK'] = 'Success';
                    $response['TRANSACTIONID'] = 'No Amount';
                    $response['AMT'] = 0;
                    $response['ACK'] = 'Success';
                    $arraySerialize = array('No Amount Transaction');
                    $dt = new DateTime();
                    $currentDateTime = $dt->format('Y-m-d H:i:s');
                    $response['TIMESTAMP'] = $currentDateTime;
                }
                if ($errorMessage == null) {
                    if ($response['ACK'] == 'Success') {

                        //debug($errorMessage);
                        //debug($response);

                        $this->Transaction->create();
                        $dateTransaction = date('Y-m-d H:i:s', strtotime($response['TIMESTAMP']));
                        $arraySerialize = serialize($response);
                        $transactionArray['Transaction']['paypal_tranc_id'] = $response['TRANSACTIONID'];
                        $transactionArray['Transaction']['user_id'] = $this->request->data['Transaction']['user_id'];
                        $transactionArray['Transaction']['date_time'] = $dateTransaction;
                        $transactionArray['Transaction']['amount'] = $response['AMT'];
                        $transactionArray['Transaction']['result'] = $response['ACK'];
                        $transactionArray['Transaction']['pass_id'] = $this->request->data['Transaction']['pass_id'];
                        $transactionArray['Transaction']['transaction_array'] = $arraySerialize;
                        $transactionArray['Transaction']['first_name'] = $this->request->data['Transaction']['first_name_cc'];
                        $transactionArray['Transaction']['last_name'] = $this->request->data['Transaction']['last_name_cc'];
                        //die();
                        if ($this->Transaction->save($transactionArray, false)) {

                            $customerPassArray['CustomerPass']['user_id'] = $this->request->data['Transaction']['user_id'];
                            $customerPassArray['CustomerPass']['property_id'] = $this->Session->read('PropertyId');
                            $customerPassArray['CustomerPass']['transaction_id'] = $this->Transaction->getLastInsertId();
                            $customerPassArray['CustomerPass']['pass_id'] = $this->request->data['Transaction']['pass_id'];
                            $customerPassArray['CustomerPass']['membership_vaild_upto'] = $this->request->data['Transaction']['packageExpiryDate'];
                            $customerPassArray['CustomerPass']['pass_valid_upto'] = $this->request->data['Transaction']['passExpiryDate'];
                            $customerPassArray['CustomerPass']['package_id'] = $this->request->data['Transaction']['packageId'];

                            $result = $customerPass->hasAny(array('pass_id' => $this->request->data['Transaction']['pass_id'], 'user_id' => $this->request->data['Transaction']['user_id']));
                            if ($result == true) {
                                $customerPassId = $customerPass->find('first', array('fields' => array('id'), 'conditions' => array('pass_id' => $this->request->data['Transaction']['pass_id'], 'user_id' => $this->request->data['Transaction']['user_id'])));
                                $customerPassArray['CustomerPass']['id'] = $customerPassId['CustomerPass']['id'];
                                $createdOn = $customerPass->field('created_on', array('id' => $customerPassId['CustomerPass']['id']));
                                $customerPassArray['CustomerPass']['created_on'] = $createdOn;
                            }
                            //debug($customerPassArray);
                            //die();
                            $customerPass->create();
                            if ($customerPass->save($customerPassArray, false)) {

                                $billAddressArray['BillingAddress']['user_id'] = $this->request->data['Transaction']['user_id'];
                                $billAddressArray['BillingAddress']['email'] = $this->request->data['Transaction']['email'];
                                $billAddressArray['BillingAddress']['first_name'] = $this->request->data['Transaction']['first_name'];
                                $billAddressArray['BillingAddress']['last_name'] = $this->request->data['Transaction']['last_name'];
                                $billAddressArray['BillingAddress']['address_line_1'] = $this->request->data['Transaction']['address_line_1'];
                                $billAddressArray['BillingAddress']['address_line_2'] = $this->request->data['Transaction']['address_line_2'];
                                $billAddressArray['BillingAddress']['city'] = $this->request->data['Transaction']['city'];
                                $billAddressArray['BillingAddress']['state'] = $this->request->data['Transaction']['state'];
                                $billAddressArray['BillingAddress']['zip'] = $this->request->data['Transaction']['zip'];
                                $billAddressArray['BillingAddress']['phone'] = $this->request->data['Transaction']['phone'];

                                $this->loadModel('BillingAddress');
                                $billAddress = new BillingAddress();
                                $rslt = $billAddress->hasAny(array('user_id' => $this->request->data['Transaction']['user_id']));
                                if ($rslt == true) {
                                    $billAddressId = $billAddress->find('first', array('fields' => array('id'), 'conditions' => array('user_id' => $this->request->data['Transaction']['user_id'])));
                                    $billAddressArray['BillingAddress']['id'] = $billAddressId['BillingAddress']['id'];
                                }
                                $billAddress->create();
                                if ($billAddress->save($billAddressArray, false)) {
                                    $customerPass->recursive = -1;
                                    $newCustomerPass = $customerPass->find('first', array('fields' => array('id'), 'conditions' => array('user_id' => $this->Auth->user('id'), 'property_id' => $this->Session->read('PropertyId'), 'pass_id' => $passId)));
                                    //debug($newCustomerPass);
                                    //die();
                                    $this->Session->setFlash(__('Your transaction has been completed succesfully'));
                                    return $this->redirect(array('controller' => 'vehicles', 'action' => 'add', $newCustomerPass['CustomerPass']['id']));
                                } else {
                                    $this->Session->setFlash(__('Billing Address Not Saved'));
                                }
                            } else {
                                $this->Session->setFlash(__('Pass not saved'));
                                //return $this->redirect(array('action' => 'check_out','pass'=>$passId,'package'=>$packageId));
                            }
                        } else {
                            $this->Session->setFlash(__('The transaction could not be saved. Contact Admin'));
                            //return $this->redirect(array('action' => 'check_out','pass'=>$passId,'package'=>$packageId));
                        }
                    } else {
                        $this->Session->setFlash(__('Transaction Failed'));
                        //return $this->redirect(array('action' => 'check_out','pass'=>$passId,'package'=>$packageId));
                    }
                } else {
                    $this->Session->setFlash(__($errorMessage));
                    //return $this->redirect(array('action' => 'check_out','pass'=>$passId,'package'=>$packageId));
                }
            }
        }



        $this->loadModel('BillingAddress');
        $billAdd = new BillingAddress();
        $billAdd->recursive = -1;
        $billingAddress = $billAdd->find('first', array('conditions' => array('user_id' => $this->Auth->user('id'))));
        //debug($billingAddress);
        // die();
        $this->loadModel('State');
        $state = new State();
        $states = $state->find('list');
        //die();
        $this->set(compact('states', 'packageDetails', 'pass', 'billingAddress'));
    }

    /*     * ***********************************************
     * Buy Cumpulsory Pass
     */

    public function compulsoryPasses() {
        return $this->redirect(array('controller' => 'Transactions', 'action' => 'select_passes'));
    }

	public function buyRemainingPass($id=null){
		$this->layout='customer';
		$this->loadModel('Pass');
		$this->loadModel('Package');
        $pass=new Pass();
        if (!$pass->exists($id)) {
            throw new NotFoundException(__('Invalid pass'));
        }
		$dt = new DateTime();
        $currentDateTime= $dt->format('Y-m-d H:i:s');
		$pass->recursive=-1;
        $remainingPass=$pass->find('first',array('conditions'=>array('id'=>$id)));	
        $package=null;
        $totalAmount=0;
		$totalAmount=$totalAmount+($remainingPass['Pass']['deposit']+$remainingPass['Pass']['cost_1st_year']);
        if($remainingPass['Pass']['package_id']){
			$this->Package->recursive=-1;
			$package=$this->Package->find('first',array('conditions'=>array('id'=>$remainingPass['Pass']['package_id'])));	
			if($package['Package']['is_guest']==0){
				$totalAmount=$totalAmount+$package['Package']['cost'];
			}else{
				$package=NULL;
			}
		}
		if($remainingPass['Pass']['is_fixed_duration']==0){
		  switch ($remainingPass['Pass']['duration_type']) {
                        case "Hour":
                            $date = new DateTime($currentDateTime);
                            $k=(int)$remainingPass['Pass']['duration'];
                            $date->add(new DateInterval('PT'.$k.'H'));
                            $remainingPass['Pass']['expiration_date']=date_format($date, 'Y-m-d H:i:s');
                            break;
                        case "Day":
                            $date = new DateTime($currentDateTime);
                            $k=$remainingPass['Pass']['duration'];
                            $date->add(new DateInterval('P'.$k.'D'));
                            $remainingPass['Pass']['expiration_date']=date_format($date, 'Y-m-d H:i:s');
                            break;
                        case "Week":
                            $date = new DateTime($currentDateTime);
                            $k=(int)$remainingPass['Pass']['duration']*7;
                            $date->add(new DateInterval('P'.$k.'D'));
                            $remainingPass['Pass']['expiration_date']=date_format($date, 'Y-m-d H:i:s');
                            break;
                        case "Month":
                            $date = new DateTime($currentDateTime);
                            $k=(int)$remainingPass['Pass']['duration'];
                            $date->add(new DateInterval('P'.$k.'M'));
                            $remainingPass['Pass']['expiration_date']=date_format($date, 'Y-m-d H:i:s');
                            break;
                        case "Year":
                            $date = new DateTime($currentDateTime);
                            $k=(int)$remainingPass['Pass']['duration'];
                            $date->add(new DateInterval('P'.$k.'Y'));
                           $remainingPass['Pass']['expiration_date']=date_format($date, 'Y-m-d H:i:s');
                            break;
                    }
         }
         if ($this->request->is('post')){
			$this->loadModel('CustomerPass');
			if($this->CustomerPass->hasAny(array('user_id'=>$this->Auth->user('id'),'pass_id'=>$remainingPass['Pass']['id']))){
				$this->Session->write('Allowed',true);
				$this->redirect(array('controller'=>'Vehicles','action'=>'add_vehicles'));	 
			}
			 if($package){
				if($package['Package']['is_fixed_duration']==0){
					switch ($package['Package']['duration_type']) {
                        case "Hour":
                            $date = new DateTime($currentDateTime);
                            $k=(int)$package['Package']['duration'];
                            $date->add(new DateInterval('PT'.$k.'H'));
                            $package['Package']['expiration_date']=date_format($date, 'Y-m-d H:i:s');
                            break;
                        case "Day":
                            $date = new DateTime($currentDateTime);
                            $k=$remainingPass['Pass']['duration'];
                            $date->add(new DateInterval('P'.$k.'D'));
                            $package['Package']['expiration_date']=date_format($date, 'Y-m-d H:i:s');
                            break;
                        case "Week":
                            $date = new DateTime($currentDateTime);
                            $k=(int)$package['Package']['duration']*7;
                            $date->add(new DateInterval('P'.$k.'D'));
                            $package['Package']['expiration_date']=date_format($date, 'Y-m-d H:i:s');
                            break;
                        case "Month":
                            $date = new DateTime($currentDateTime);
                            $k=(int)$package['Package']['duration'];
                            $date->add(new DateInterval('P'.$k.'M'));
                            $package['Package']['expiration_date']=date_format($date, 'Y-m-d H:i:s');
                            break;
                        case "Year":
                            $date = new DateTime($currentDateTime);
                            $k=(int)$package['Package']['duration'];
                            $date->add(new DateInterval('P'.$k.'Y'));
                           $package['Package']['expiration_date']=date_format($date, 'Y-m-d H:i:s');
                            break;
                    }
				}
			 }
			$this->Transaction->set($this->request->data);
			if($totalAmount==0){
				$this->Transaction->updateValidations();
			}
            if($this->Transaction->validates()){
				if($totalAmount>0)
				{					
					$payment = array(
                        'amount' => (int)$totalAmount,
                        'card' => $this->request->data['Transaction']['card_number'], // This is a sandbox CC
                        'expiry' => array(
                            'M' => $this->request->data['Transaction']['month'],
                            'Y' => $this->request->data['Transaction']['year'],
                        ),
                        'cvv' => $this->request->data['Transaction']['cvv'],
                        'currency' => 'USD' // Defaults to GBP if not provided
                    );
					$errorMessage=null;
					$response=null;
                    try {
                        $response=$this->Paypal->doDirectPayment($payment);
                        // Check $response[ACK]='Success' OR 'OK'
                    } catch (Exception $e) {
                        $errorMessage=$e->getMessage();
                    }
		
				}else{				
					$response['TIMESTAMP']=date('Y-m-d H:i:s');
					$response['TRANSACTIONID']='No Payment Remaining Pass';
					$response['ACK']='Success';
					$response['AMT']='0';
					$response['PassDetail']= $remainingPass;
					$errorMessage=null;
					$this->request->data['Transaction']['first_name_cc']=$this->Auth->user('first_name');
					$this->request->data['Transaction']['last_name_cc']=$this->Auth->user('last_name');
				}
				if($errorMessage==null){
					if($response['ACK']=='Success'){
						$dateTransaction = date('Y-m-d H:i:s', strtotime ($response['TIMESTAMP']));
						$arraySerialize=serialize($response);
						$transactionArray['Transaction']['paypal_tranc_id']=$response['TRANSACTIONID'];
						$transactionArray['Transaction']['user_id']=$this->Auth->user('id');
						$transactionArray['Transaction']['date_time']=$dateTransaction;
						$transactionArray['Transaction']['amount']=$response['AMT'];
						$transactionArray['Transaction']['result']=$response['ACK'];
						$transactionArray['Transaction']['pass_id']=$id;
						$transactionArray['Transaction']['transaction_array']=$arraySerialize;
						$transactionArray['Transaction']['first_name']=$this->request->data['Transaction']['first_name_cc'];
						$transactionArray['Transaction']['last_name']=$this->request->data['Transaction']['last_name_cc'];	
						//$transactionArray['Transaction']['comments'] = "passPurchased";	  
						$this->Transaction->create();
						if ($this->Transaction->save($transactionArray,false)) {
							$customerPassArray['CustomerPass']['user_id']=$this->Auth->user('id');
							$customerPassArray['CustomerPass']['property_id']=$this->Session->read('PropertyId');
							$customerPassArray['CustomerPass']['transaction_id']=$this->Transaction->getLastInsertId();
							$customerPassArray['CustomerPass']['pass_id']=$remainingPass['Pass']['id'];
							$customerPassArray['CustomerPass']['pass_valid_upto']=$remainingPass['Pass']['expiration_date'];
							$customerPassArray['CustomerPass']['pass_cost'] = $remainingPass['Pass']['cost_1st_year'];
							$customerPassArray['CustomerPass']['pass_deposit'] = $remainingPass['Pass']['deposit']; 
							if($package){
								$customerPassArray['CustomerPass']['membership_vaild_upto']=$package['Package']['expiration_date'];
								$customerPassArray['CustomerPass']['package_id']=$package['Package']['id'];
								$customerPassArray['CustomerPass']['package_cost'] = $package['Package']['cost'];
							}
							/*if($totalAmount!=0){
							 if($remainingPass['Pass']['is_fixed_duration'] == 0){
								if($remainingPass['Pass']['package_id']){
									$this->loadModel('Package');
									$this->Package->recursive=-1;
									$packageDetails=$this->Package->find('first',array('conditions'=>array('id'=>$remainingPass['Pass']['package_id'])));
									if($packageDetails){
										if($packageDetails['Package']['is_guest']==1){
											if($packageDetails['Package']['is_recurring']==1){
												$recurringPayment = array(
																		'amount' =>$totalAmount,
																		'card' => $this->request->data['Transaction']['card_number'], 
																		'expiry' => array(
																						'M' => $this->request->data['Transaction']['month'],
																						'Y' => $this->request->data['Transaction']['year'],
																					),
																		'cvv' => $this->request->data['Transaction']['cvv'],
																		'currency' => 'USD' ,
																		'BILLINGPERIOD'=>$remainingPass['Pass']['duration_type'],
																		'BILLINGFREQUENCY'=>$remainingPass['Pass']['duration'],
																		'PROFILESTARTDATE'=>$remainingPass['Pass']['expiration_date'],
																		'FIRSTNAME' => AuthComponent::user('first_name'),
																		'LASTNAME' => AuthComponent::user('last_name'), 
																		'EMAIL'=>AuthComponent::user('email'),
																		'STREET' => AuthComponent::user('address_line_1'),			
																		'CITY' => AuthComponent::user('city'),				
																		'STATE' => AuthComponent::user('state'),						
																		'ZIP' => AuthComponent::user('zip')
																	);
												$errorMessage==null;
												try {
													$recurringResponse = $this->Paypal->createRecurringProfile($recurringPayment);
												}catch (Exception $e) {
													$errorMessage=$e->getMessage();
												}
												if($errorMessage==null){
													if($recurringResponse['ACK']=='Success'){
														CakeLog::write('recurringProfileCreatedRemainingGuestPass', ''.AuthComponent::user('username').' : Recurring Profile created by User: '.AuthComponent::user('first_name').' '.AuthComponent::user('last_name').
															' In Property: '.CakeSession::read('PropertyName').', for Pass Name : '.$remainingPass['Pass']['name'].
															' Profile Id : '.$recurringResponse['PROFILEID'].
															' Profile Status : '.$recurringResponse['PROFILESTATUS'].''); 
														$customerPassArray['CustomerPass']['recurring_profile_id']=$recurringResponse['PROFILEID'];
														$customerPassArray['CustomerPass']['recurring_profile_status']=$recurringResponse['PROFILESTATUS'];
													}else{
													CakeLog::write('recurringProfileCreationRemainigGuestPassFailed', ''.AuthComponent::user('username').' : Recurring Profile creation failed User: '.AuthComponent::user('first_name').' '.AuthComponent::user('last_name').
														' In Property: '.CakeSession::read('PropertyName').', for Pass Name : '.$remainingPass['Pass']['name'].''); 
												}
											  }else{
													CakeLog::write('recurringProfileCreationRemainigGuestPassException', ''.AuthComponent::user('username').' : Recurring Profile creation failed User: '.AuthComponent::user('first_name').' '.AuthComponent::user('last_name').
														' In Property: '.CakeSession::read('PropertyName').', for Pass Name : '.$remainingPass['Pass']['name'].' Exception: '.$errorMessage.''); 

											  }
											}
										}
									}
								}
							 }
							}*/
							$result=$this->CustomerPass->hasAny(array('pass_id'=>$customerPassArray['CustomerPass']['pass_id'],'user_id'=>$this->Auth->user('id')));									
							$newCustomerPass=$customerPassId=NULL;
							if($result==true){
								$customerPassId=$this->CustomerPass->field('id',array(array('pass_id'=>$customerPassArray['CustomerPass']['pass_id'],'user_id'=>$this->Auth->user('id'))));
								$customerPassArray['CustomerPass']['id']=$customerPassId;		
							}	
							$this->CustomerPass->create();
							if($this->CustomerPass->save($customerPassArray,false)){
								if($customerPassId){
									$newCustomerPass=$customerPassId;
								}else{
									$newCustomerPass=$this->CustomerPass->getLastInsertId();
								}
								$this->loadModel('Pass');
								$passName=$this->Pass->givePassName($remainingPass['Pass']['id']);
								CakeLog::write('nonCompulsoryPassPurchased', ''.AuthComponent::user('username').' : Non compulsory pass/remaining pass purchased with Pass ID: '.$remainingPass['Pass']['id'].' and Pass Name: '.$passName.', purchased by User: '.AuthComponent::user('first_name').' '.AuthComponent::user('last_name').', in Property: '.CakeSession::read('PropertyName').' Amount Paid: $'.$response['AMT'].'');
								$billAddressArray['BillingAddress']['user_id']=$this->Auth->user('id');
								$billAddressArray['BillingAddress']['email']=$this->request->data['Transaction']['email'];
								$billAddressArray['BillingAddress']['first_name']=$this->request->data['Transaction']['first_name'];
								$billAddressArray['BillingAddress']['last_name']=$this->request->data['Transaction']['last_name'];
								$billAddressArray['BillingAddress']['address_line_1']=$this->request->data['Transaction']['address_line_1'];
								$billAddressArray['BillingAddress']['address_line_2']=$this->request->data['Transaction']['address_line_2'];
								$billAddressArray['BillingAddress']['city']=$this->request->data['Transaction']['city'];
								$billAddressArray['BillingAddress']['state']=$this->request->data['Transaction']['state'];
								$billAddressArray['BillingAddress']['zip']=$this->request->data['Transaction']['zip'];
								$billAddressArray['BillingAddress']['phone']=$this->request->data['Transaction']['phone'];
								$this->loadModel('BillingAddress');
								$billAddress=new BillingAddress();
								$billAddress->create(); 
								$rslt=$billAddress->hasAny(array('user_id'=>$this->Auth->user('id')));
								if($rslt==true){
									$billAddressId=$billAddress->find('first',array('fields'=>array('id'),'conditions'=>array('user_id'=>$this->Auth->user('id'))));
									$billAddressArray['BillingAddress']['id']=$billAddressId['BillingAddress']['id'];
								}
								if($billAddress->save($billAddressArray,false)){																
									$this->Session->setFlash('Passes Saved Successfully','success');
									return $this->redirect(array('controller'=>'vehicles','action' => 'add',$newCustomerPass));

								}else{
									return $this->redirect(array('controller'=>'CustomerPasses','action' => 'view'));
								}				
							}else {
								$this->Session->setFlash('Pass not saved','error');	
								$this->redirect(array('action'=>'buyRemainingPass'));
							}
						}else {
							$this->Session->setFlash('The transaction could not be saved. Contact Admin','error');
						}	
					}else{
						$this->Session->setFlash('Transaction Failed','error');
					}	
				}else{
				  $this->Session->setFlash($errorMessage,'error');
				}	
			}else{
				$this->Session->setFlash('Validation Failed','error');
			}
		 }
        $this->loadModel('State');
		$state=new State();
		$states=$state->find('list');	
		$this->loadModel('BillingAddress');
        $billAdd = new BillingAddress();
        $billAdd->recursive=-1;
        $billingAddress= $billAdd->find('first',array('conditions'=>array('user_id'=>$this->Auth->user('id'))));	
		$this->set(compact('states','remainingPass','totalAmount','billingAddress','package'));
	}

    public function guestCheckOut($customerPassId = null, $passId = null, $permitId = null) {
        $this->loadModel('CustomerPass');
        if (!$this->CustomerPass->exists($customerPassId)) {
            throw new NotFoundException(__('Invalid Customer Pass'));
        }
        $this->loadModel('Package');
        if (!$this->Package->exists($permitId)) {
            throw new NotFoundException(__('Invalid package'));
        }
        $this->loadModel('Pass');
        if (!$this->Pass->exists($passId)) {
            throw new NotFoundException(__('Invalid pass'));
        }

        $this->loadModel('Property');
        $freeGuestCredits = $this->Property->field('free_guest_credits', array('id' => $this->Session->read('PropertyId')));
        $permitCost = $this->Package->field('cost', array('id' => $permitId));
        $previousUsedCredits = $this->Auth->user('guest_credits');
        $toDate = $this->Session->read('toDate');
        $toDate = date("Y-m-d", strtotime($toDate));
        $dt = new DateTime();
        $currentDateTime = $dt->format('Y-m-d');
        $datetime1 = date_create($toDate);
        $datetime2 = date_create($currentDateTime);
        $interval = date_diff($datetime1, $datetime2);
        $currentCreditUsed = $interval->days;
        $creditPayable=$newCredits = 0;
        $amount = 0;
        if ((int) $previousUsedCredits >= $interval->days) {
            $newCredits = $previousUsedCredits - $interval->days;
            $amount = 0;
        } else {
            $newCredits = 0;
            $creditPayable = $interval->days - $previousUsedCredits;
            $amount = $creditPayable * $permitCost;
        }

        if ($this->request->is('post')) {
            $this->Transaction->set($this->request->data);
            if ($this->Transaction->validates()) {
                $date = new DateTime();
                $date->add(new DateInterval('P' . $currentCreditUsed . 'D'));
                $permitExpiryDate = date_format($date, 'Y-m-d H:i:s');


                $payment = array(
                    'amount' => (int) $amount,
                    'card' => $this->request->data['Transaction']['card_number'], // This is a sandbox CC
                    'expiry' => array(
                        'M' => $this->request->data['Transaction']['month'],
                        'Y' => $this->request->data['Transaction']['year'],
                    ),
                    'cvv' => $this->request->data['Transaction']['cvv'],
                    'currency' => 'USD' // Defaults to GBP if not provided
                );

                $errorMessage = null;
                $response = null;
                try {
                    $response = $this->Paypal->doDirectPayment($payment);
                    // Check $response[ACK]='Success' OR 'OK'
                } catch (Exception $e) {
                    $errorMessage = $e->getMessage();
                }
                if ($errorMessage == null) {
                    if ($response['ACK'] == 'Success') {
                        $dateTransaction = date('Y-m-d H:i:s', strtotime($response['TIMESTAMP']));
                        $arraySerialize = serialize($response);
                        $transactionArray['Transaction']['paypal_tranc_id'] = $response['TRANSACTIONID'];
                        $transactionArray['Transaction']['user_id'] = $this->Auth->user('id');
                        $transactionArray['Transaction']['date_time'] = $dateTransaction;
                        $transactionArray['Transaction']['amount'] = $response['AMT'];
                        $transactionArray['Transaction']['result'] = $response['ACK'];
                        $transactionArray['Transaction']['pass_id'] = $passId;
                        $transactionArray['Transaction']['transaction_array'] = $arraySerialize;
                        $transactionArray['Transaction']['first_name'] = $this->request->data['Transaction']['first_name_cc'];
                        $transactionArray['Transaction']['last_name'] = $this->request->data['Transaction']['last_name_cc'];
                        $transactionArray['Transaction']['comments'] = "guestPassActivated";
                        $this->Transaction->create();
                        if ($this->Transaction->save($transactionArray, false)) {
                            $this->loadModel('CustomerPass');
                            $customerPassArray['CustomerPass']['transaction_id'] = $this->Transaction->getLastInsertId();
                            $customerPassArray['CustomerPass']['membership_vaild_upto'] = $permitExpiryDate;
                            $customerPassArray['CustomerPass']['id'] = $customerPassId;
                            $customerPassArray['CustomerPass']['package_id'] = $permitId;
                            $customerPassArray['CustomerPass']['is_guest_pass'] = 1;
                       
                            $this->CustomerPass->create();
                            if ($this->CustomerPass->save($customerPassArray, false)) {
									$this->loadModel('Vehicle');
									$this->CustomerPass->updateAll(array('CustomerPass.vehicle_id'=>NULL),array('CustomerPass.id'=>$customerPassId));
									$this->Vehicle->updateAll(array('Vehicle.customer_pass_id'=>NULL),array('Vehicle.customer_pass_id'=>$customerPassId));
									
                               
                                    $this->loadModel('UserGuestPass');
                 
                                    $userPassArray = array('UserGuestPass' => array('user_id' => AuthComponent::user('id'),
                                            'customer_pass_id' => $customerPassId,
                                    
                                            'days' => $currentCreditUsed,
                                            'free' => $currentCreditUsed-$creditPayable, 
                                            'paid' => $creditPayable,
                                            'amount' => $amount,
                                            'to_date' => $permitExpiryDate,
                                            'property_id' => $this->Session->read('PropertyId')
                                    ));
                                    $this->UserGuestPass->save($userPassArray, false);
									$userGuestPassId=	$this->UserGuestPass->getLastInsertID();
                                    $billAddressArray['BillingAddress']['email'] = $this->request->data['Transaction']['email'];
                                    $billAddressArray['BillingAddress']['first_name'] = $this->request->data['Transaction']['first_name'];
                                    $billAddressArray['BillingAddress']['last_name'] = $this->request->data['Transaction']['last_name'];
                                    $billAddressArray['BillingAddress']['address_line_1'] = $this->request->data['Transaction']['address_line_1'];
                                    $billAddressArray['BillingAddress']['address_line_2'] = $this->request->data['Transaction']['address_line_2'];
                                    $billAddressArray['BillingAddress']['city'] = $this->request->data['Transaction']['city'];
                                    $billAddressArray['BillingAddress']['state'] = $this->request->data['Transaction']['state'];
                                    $billAddressArray['BillingAddress']['zip'] = $this->request->data['Transaction']['zip'];
                                    $billAddressArray['BillingAddress']['phone'] = $this->request->data['Transaction']['phone'];
                                    $this->loadModel('BillingAddress');
                                    $billAddress = new BillingAddress();
                                    $billAddress->create();
                                    $rslt = $billAddress->hasAny(array('user_id' => $this->Auth->user('id')));
                                    if ($rslt == true) {
                                        $billAddressId = $billAddress->find('first', array('fields' => array('id'), 'conditions' => array('user_id' => $this->Auth->user('id'))));
                                        $billAddressArray['BillingAddress']['id'] = $billAddressId['BillingAddress']['id'];
                                    }
                                    if ($billAddress->save($billAddressArray, false)) {
                                        $this->loadModel('User');
                                        $user['User']['id'] = $this->Auth->user('id');
                                        $user['User']['guest_credits'] = $newCredits;
                                        $this->User->create();
                                        if ($this->User->save($user, false)) {
											$_SESSION['Auth']['User']['guest_credits']=$newCredits;
											CakeLog::write('guestPassUpdatedPaid', ''.AuthComponent::user('username').' : Guest Pass Updated, Customer Pass ID: <a href="/admin/CustomerPasses/edit/'.$customerPassId.'">'.$customerPassId.'</a> by User : '.AuthComponent::user('first_name').' '.AuthComponent::user('last_name').' In Property '.CakeSession::read('PropertyName').'For '.$currentCreditUsed.' Days, Amount Paid: $'.$amount.'');
                                            $this->Session->setFlash('Your transaction has been completed succesfully, Please add vehicle details now.', 'sucess');
                                            $this->Session->delete('Allowed');
                                            $this->Session->delete('vehicleID');
                                            $this->Session->write('Allowed', true);
                                           return $this->redirect('/Vehicles/add_guest_vehicle/'.$customerPassId.'/'.$userGuestPassId);
                                        } else {
                                            $this->Session->setFlash(__('User Table not updated', 'error'));
                                        }
                                    } else {
                                        $this->Session->setFlash('Billing Address Not Saved', 'error');
                                    }
                               
                            } else {
                                $this->Session->setFlash('Pass not saved', 'error');
                            }
                        } else {
                            $this->Session->setFlash('The transaction could not be saved. Contact Admin', 'error');
                        }
                    } else {
                        $this->Session->setFlash('Transaction Failed', 'error');
                    }
                } else {
                    $this->Session->setFlash($errorMessage, 'error');
                }
            }
        }
        $this->layout = 'customer';
        $this->loadModel('BillingAddress');
        $this->BillingAddress->recursive = -1;
        $billingAddress = $this->BillingAddress->find('first', array('conditions' => array('user_id' => $this->Auth->user('id'))));
        $this->loadModel('State');
        $state = new State();
        $states = $state->find('list');
        $this->set(compact('freeGuestCredits', 'previousUsedCredits', 'billingAddress', 'states', 'permitCost', 'newCredits', 'amount', 'currentCreditUsed', 'creditPayable'));
    }

/*************************************************************
 * Pass Renewal 
 */
 public function passRenew($customerPassId=null,$passId=null,$advance=false)
 {
	$this->loadModel('CustomerPass');
	$renewAllowed=true;
	if (!$this->CustomerPass->hasAny(array('id'=>$customerPassId,'user_id'=>AuthComponent::user('id')))) {
			CakeLog::write('wrongPassRenewAttempt', ''.AuthComponent::user('username').' : Attempt to renew wrong Pass with  Customer Pass ID: <a href="/admin/CustomerPasses/edit/'.$customerPassId.'">'.$customerPassId.'</a> was done by by User: '.AuthComponent::user('first_name').' '.AuthComponent::user('last_name').', in Property: '.CakeSession::read('PropertyName'));
			throw new NotFoundException(__('Invalid customer pass'));
	}else{
		if($advance==false){
			$this->CustomerPass->recursive=-1;
			$packageDate=$this->CustomerPass->field('pass_valid_upto',array('id'=>$customerPassId));
			if(!is_null($packageDate)){
				$dt = new DateTime();
				$currentDateTime= $dt->format('Y-m-d H:i:s');
				if($packageDate>$currentDateTime){
					$this->Session->setFlash('This pass is already active','warning');
					return $this->redirect(array('controller'=>'Vehicles','action'=>'add_vehicles'));
				}
			}
		}
	}
	$this->loadModel('Pass');
    if (!$this->Pass->exists($passId)) {
            $this->Session->setFlash('Sorry, this pass cannot be renewed as this pass has been removed from this property','warning');	
            $this->redirect(array('controller'=>'CustomerPasses','action'=>'view'));
    }else{
		$packageId=$this->Pass->field('package_id',array('id'=>$passId));
		if(!is_null($packageId)){
			$this->loadModel('Package');
			if(!$this->Package->exists($packageId)){
				$this->Session->setFlash('Sorry, this pass cannot be renewed as this pass has been removed from this property','warning');	
				$renewAllowed=false;
			}
		}
	}
	$this->layout='customer';

	$this->loadModel('Pass');
	$this->Pass->recursive=-1;
	$passDetails=$this->Pass->find('first',array('conditions'=>array('id'=>$passId)));
	$advance=false;
	$pass_expiry = $this->CustomerPass->field('pass_valid_upto', array('id' => $customerPassId));
	if($passDetails['Pass']['is_fixed_duration']==0){
					$pass_expiry = $this->CustomerPass->field('pass_valid_upto', array('id' => $customerPassId));
					$dt = new DateTime();
					$currentDateTime = $dt->format('Y-m-d H:i:s');
					if($pass_expiry>$currentDateTime){
						$currentDateTime=$pass_expiry; 
						$advance=true;
					}
                    switch ($passDetails['Pass']['duration_type']) 
                    {
                        case "Hour":
                            $date = new DateTime($currentDateTime);
                            $k=(int)$passDetails['Pass']['duration'];
                            $date->add(new DateInterval('PT'.$k.'H'));
                            $passDetails['Pass']['expiration_date']=date_format($date, 'Y-m-d H:i:s');
                            break;
                        case "Day":
                            $date = new DateTime($currentDateTime);
                            $k=$passDetails['Pass']['duration'];
                            $date->add(new DateInterval('P'.$k.'D'));
                            $passDetails['Pass']['expiration_date']=date_format($date, 'Y-m-d H:i:s');
                            break;
                        case "Week":
                            $date = new DateTime($currentDateTime);
                            $k=(int)$passDetails['Pass']['duration']*7;
                            $date->add(new DateInterval('P'.$k.'D'));
                            $passDetails['Pass']['expiration_date']=date_format($date, 'Y-m-d H:i:s');
                            break;
                        case "Month":
                            $date = new DateTime($currentDateTime);
                            $k=(int)$passDetails['Pass']['duration'];
                            $date->add(new DateInterval('P'.$k.'M'));
                            $passDetails['Pass']['expiration_date']=date_format($date, 'Y-m-d H:i:s');
                            break;
                        case "Year":
                            $date = new DateTime($currentDateTime);
                            $k=(int)$passDetails['Pass']['duration'];
                            $date->add(new DateInterval('P'.$k.'Y'));
                            $passDetails['Pass']['expiration_date']=date_format($date, 'Y-m-d H:i:s');
                            break;
                    }
     }
     $packageDetails=array();
     if($passDetails['Pass']['package_id']){
		$this->loadModel('Package');
		$this->Package->recursive=-1;
		$packageDetails=$this->Package->find('first',array('conditions'=>array('id'=>$passDetails['Pass']['package_id'])));
	 }
	 $passRenewalcost=$passDetails['Pass']['cost_after_1st_year'];
	 if($packageDetails){
		 if($packageDetails['Package']['is_guest']==0){
			$passRenewalcost=$passRenewalcost+$packageDetails['Package']['cost'];
		}
	 }
     $date3 = new DateTime($passDetails['Pass']['expiration_date']);
	 $datePack=$date3->format('Y-m-d');
     $passDetails['Pass']['expiration_date']= $datePack.' 23:59:59';
	 
     $passName=$passDetails['Pass']['name'];
     $passExpiryDate=date("m/d/Y H:i:s", strtotime($passDetails['Pass']['expiration_date']));
    
	if ($this->request->is('post')) {
		$recurring_profile_id=$recurring_profile_status=NULL;
		//debug($this->request->data);
		//debug($passDetails);
		//die;
		$this->Transaction->set($this->request->data);
		if($this->Transaction->validates()){
					$payment = array(
                        'amount' => (int)$passRenewalcost,
                        'card' => $this->request->data['Transaction']['card_number'], // This is a sandbox CC
                        'expiry' => array(
                            'M' => $this->request->data['Transaction']['month'],
                            'Y' => $this->request->data['Transaction']['year'],
                        ),
                        'cvv' => $this->request->data['Transaction']['cvv'],
                        'currency' => 'USD' // Defaults to GBP if not provided
                    );
					$errorMessage=null;
					$response=null;
                    try {
                        $response=$this->Paypal->doDirectPayment($payment);
                        // Check $response[ACK]='Success' OR 'OK'
                    } catch (Exception $e) {
                        $errorMessage=$e->getMessage();
                    }
					if($errorMessage==null){
						if($response['ACK']=='Success'){
								
								$dateTransaction = date('Y-m-d H:i:s', strtotime ($response['TIMESTAMP']));
								$arraySerialize=serialize($response);
								$transactionArray['Transaction']['paypal_tranc_id']=$response['TRANSACTIONID'];
								$transactionArray['Transaction']['user_id']=$this->Auth->user('id');
								$transactionArray['Transaction']['date_time']=$dateTransaction;
								$transactionArray['Transaction']['amount']=$response['AMT'];
								$transactionArray['Transaction']['result']=$response['ACK'];
								$transactionArray['Transaction']['pass_id']=$passDetails['Pass']['id'];
								$transactionArray['Transaction']['transaction_array']=$arraySerialize;
								$transactionArray['Transaction']['first_name']=$this->request->data['Transaction']['first_name_cc'];
								$transactionArray['Transaction']['last_name']=$this->request->data['Transaction']['last_name_cc'];
								$transactionArray['Transaction']['comments'] = "passRenewed";				
								$transactionArray['Transaction']['transaction_type'] = 1;	
								$this->Transaction->create();
								if ($this->Transaction->save($transactionArray,false)) {	
											$transactionId=$this->Transaction->getLastInsertId();
											$this->loadModel('CustomerPass');
											$previousId=$this->CustomerPass->field('vehicle_id',array('id'=>$customerPassId));
											$package_id=$packageExpiryDate=NULL;
											$customerPassArray=array();
											if($packageDetails){
												if($packageDetails['Package']['is_guest'] == 1){
													$package_id=$packageDetails['Package']['id'];
												}
												if($packageDetails['Package']['is_fixed_duration']==0 && $packageDetails['Package']['is_guest'] == 0){
														$package_expiry_date = $this->CustomerPass->field('pass_valid_upto', array('id' => $customerPassId));
														$dt = new DateTime();
														$currentDateTime = $dt->format('Y-m-d H:i:s');
														if($package_expiry_date){
															if($package_expiry_date>$currentDateTime){
																$currentDateTime=$package_expiry_date; 
															}
														}
														switch ($packageDetails['Package']['duration_type']) 
														{
															case "Hour":
																$date = new DateTime($currentDateTime);
																$k=(int)$packageDetails['Package']['duration'];
																$date->add(new DateInterval('PT'.$k.'H'));
																$packageDetails['Package']['expiration_date']=date_format($date, 'Y-m-d H:i:s');
																break;
															case "Day":
																$date = new DateTime($currentDateTime);
																$k=$packageDetails['Package']['duration'];
																$date->add(new DateInterval('P'.$k.'D'));
																$packageDetails['Package']['expiration_date']=date_format($date, 'Y-m-d H:i:s');
																break;
															case "Week":
																$date = new DateTime($currentDateTime);
																$k=(int)$packageDetails['Package']['duration']*7;
																$date->add(new DateInterval('P'.$k.'D'));
																$packageDetails['Package']['expiration_date']=date_format($date, 'Y-m-d H:i:s');
																break;
															case "Month": 
																$date = new DateTime($currentDateTime);
																$k=(int)$packageDetails['Package']['duration'];
																$date->add(new DateInterval('P'.$k.'M'));
																$packageDetails['Package']['expiration_date']=date_format($date, 'Y-m-d H:i:s');
																break;
															case "Year":
																$date = new DateTime($currentDateTime);
																$k=(int)$packageDetails['Package']['duration'];
																$date->add(new DateInterval('P'.$k.'Y'));
																$packageDetails['Package']['expiration_date']=date_format($date, 'Y-m-d H:i:s');
																break;
														}
														$date4 = new DateTime($packageDetails['Package']['expiration_date']);
														$datePack=$date4->format('Y-m-d');
														$packageExpiryDate=$packageDetails['Package']['expiration_date']= $datePack.' 23:59:59';
														$package_id=$packageDetails['Package']['id'];
													}
													//RECURRING CHECK START
													if(Configure::read('RECURRING')){
													  if($passDetails['Pass']['is_fixed_duration'] == 0){
														 if($packageDetails['Package']['is_fixed_duration']==0){
															 if($packageDetails['Package']['is_recurring'] == 1){
																if($packageDetails['Package']['is_guest'] == 0 && $packageDetails['Package']['cost']>0){
																	$this->loadModel('RecurringProfile');
																	$recurAmount=$packageDetails['Package']['cost'];
																	$recurringPayment = array();
																	$previousProfile=$this->CustomerPass->field('recurring_profile_id',array('id'=>$customerPassId));
																	if($previousProfile){
																		$recurringPayment = array(
																						'amount' => $recurAmount,
																						'currency' => 'USD' ,
																						'BILLINGPERIOD'=>$packageDetails['Package']['duration_type'],
																						'BILLINGFREQUENCY'=>$packageDetails['Package']['duration'],
																						'PROFILESTARTDATE'=>$packageDetails['Package']['expiration_date'],
																						'EMAIL'=>AuthComponent::user('email'),
																						'STREET' => AuthComponent::user('address_line_1'),						
																						'ZIP' => AuthComponent::user('zip'),
																						'PROFILEID'=>$previousProfile,
																						'expiry' => array(
																									'M' => $this->request->data['Transaction']['month'],
																									'Y' => $this->request->data['Transaction']['year'],
																								)
																					);
																	}else{
																		$recurringPayment = array(
																						'amount' => $recurAmount,
																						'card' => $this->request->data['Transaction']['card_number'], 
																						'expiry' => array(
																									'M' => $this->request->data['Transaction']['month'],
																									'Y' => $this->request->data['Transaction']['year'],
																								),
																						'cvv' => $this->request->data['Transaction']['cvv'],
																						'currency' => 'USD' ,
																						'BILLINGPERIOD'=>$packageDetails['Package']['duration_type'],
																						'BILLINGFREQUENCY'=>$packageDetails['Package']['duration'],
																						'PROFILESTARTDATE'=>$packageDetails['Package']['expiration_date'],
																						'FIRSTNAME' => AuthComponent::user('first_name'),
																						'LASTNAME' => AuthComponent::user('last_name'), 
																						'EMAIL'=>AuthComponent::user('email'),
																						'STREET' => AuthComponent::user('address_line_1'),			
																						'CITY' => AuthComponent::user('city'),				
																						'STATE' => AuthComponent::user('state'),						
																						'ZIP' => AuthComponent::user('zip')
																					);
																	}
																	
																	$errorMessage=null;
																	try {
																		if($previousProfile){
																			$recurringResponse = $this->PayFlow->modifyRecurringProfilePayFlow($recurringPayment);
																		}else{
																			$recurringResponse = $this->PayFlow->createRecurringProfilePayFlow($recurringPayment);
																		}
																	}catch (Exception $e) {
																		$errorMessage=$e->getMessage();
																	}
																	if($errorMessage==null){
																		if(isset($recurringResponse['RESULT']) && $recurringResponse['RESULT'] == 0) {
																			if($recurringResponse['RESPMSG']=='Approved'){
																				$recurringResponse['PROFILESTATUS']='Active';
																				$array['RecurringProfile']=array(
																								 'recurring_profile_id'=>$recurringResponse['PROFILEID'],
																								 'message'=>'Recurring Profile created',
																								 'status'=>$recurringResponse['PROFILESTATUS'],
																								  'status'=>$recurringResponse['PROFILESTATUS'],
																								 'customer_pass_id'=>$customerPassId
																				);
																				if($previousProfile){
																					$idRecurring=$this->RecurringProfile->field('id',array('recurring_profile_id'=>$previousProfile));
																					$array['RecurringProfile']['id']=$idRecurring;
																				}else{
																					$this->RecurringProfile->create();
																				}
																				if($this->RecurringProfile->save($array)){
																					if($previousProfile){
																						CakeLog::write('profileDataModified','Profile Data Modified for ID: '.$recurringResponse['PROFILEID']);
																					}else{
																						CakeLog::write('profileDataSaved','Profile Data Saved for ID: '.$recurringResponse['PROFILEID']);
																					}
																				}else{
																					if($previousProfile){
																						CakeLog::write('profileDataNotModified','Profile Data Not Modified for ID: '.$recurringResponse['PROFILEID']);
																					}else{	
																						CakeLog::write('profileDataNotSaved','Profile Data Not Saved for ID: '.$recurringResponse['PROFILEID']);
																					}
																				}
																				if($previousProfile){
																					CakeLog::write('recurringProfileModified', ''.AuthComponent::user('username').' : Recurring Profile created by User: '.AuthComponent::user('first_name').' '.AuthComponent::user('last_name').
																					' In Property: '.CakeSession::read('PropertyName').', for Pass Name : '.$passDetails['Pass']['name'].
																					' Profile Id : '.$recurringResponse['PROFILEID'].
																					' Profile Status : '.$recurringResponse['PROFILESTATUS'].''); 
																					$recurring_profile_id=$recurringResponse['PROFILEID'];
																					$recurring_profile_status=$recurringResponse['PROFILESTATUS'];
																				}else{
																					CakeLog::write('recurringProfileCreated', ''.AuthComponent::user('username').' : Recurring Profile created by User: '.AuthComponent::user('first_name').' '.AuthComponent::user('last_name').
																					' In Property: '.CakeSession::read('PropertyName').', for Pass Name : '.$passDetails['Pass']['name'].
																					' Profile Id : '.$recurringResponse['PROFILEID'].
																					' Profile Status : '.$recurringResponse['PROFILESTATUS'].''); 
																					$recurring_profile_id=$recurringResponse['PROFILEID'];
																					$recurring_profile_status=$recurringResponse['PROFILESTATUS'];
																				}
																			}
																		}else{
																			if($previousProfile){
																				  CakeLog::write('recurringProfileModificationFailed1', ''.AuthComponent::user('username').' : Recurring Profile creation failed User: '.AuthComponent::user('first_name').' '.AuthComponent::user('last_name').
																				' In Property: '.CakeSession::read('PropertyName').', for Pass Name : '.$passDetails['Pass']['name'].''); 
																			
																			}else{
																				CakeLog::write('recurringProfileCreationFailed1', ''.AuthComponent::user('username').' : Recurring Profile creation failed User: '.AuthComponent::user('first_name').' '.AuthComponent::user('last_name').
																				' In Property: '.CakeSession::read('PropertyName').', for Pass Name : '.$passDetails['Pass']['name'].''); 
																			}
																		}
																	}else{
																		if($previousProfile){
																				  CakeLog::write('recurringProfileModificationFailed', ''.AuthComponent::user('username').' : Recurring Profile creation failed User: '.AuthComponent::user('first_name').' '.AuthComponent::user('last_name').
																				' In Property: '.CakeSession::read('PropertyName').', for Pass Name : '.$passDetails['Pass']['name'].''); 
																			
																		}else{
																			CakeLog::write('recurringProfileCreationException', ''.AuthComponent::user('username').' : Recurring Profile creation failed User: '.AuthComponent::user('first_name').' '.AuthComponent::user('last_name').
																			' In Property: '.CakeSession::read('PropertyName').', for Pass Name : '.$passDetails['Pass']['name'].' Exception: '.$errorMessage.''); 
																		}
																	}
																}
															}
														}
													}
												}
												//RECURRING CHECK END
											}
											
											$customerPassArray['CustomerPass']=array(
													  'id'=>$customerPassId,
													  'pass_id'=>$passId,
													  'pass_valid_upto'=>$passDetails['Pass']['expiration_date'],
													  'membership_vaild_upto'=>$packageExpiryDate,
													  'package_id'=>$package_id,
													  //'vehicle_id'=>null,
													  'transaction_id'=>$transactionId
													 // 'pass_renewal_cost'=>$passDetails['Pass']['cost_after_1st_year']
											);
											if($packageDetails){ 
												if($packageDetails['Package']['is_guest']==1){
														$customerPassArray['CustomerPass']['is_guest_pass']=1;
												}else{
													$customerPassArray['CustomerPass']['is_guest_pass']=0;
												}
												if($advance){
													if($packageDetails['Package']['is_guest']==1){
														unset($customerPassArray['CustomerPass']['membership_vaild_upto']);
													}
												}else{
													if($packageDetails['Package']['is_guest']==1){
														$this->loadModel('Vehicle');
														//SET CUSTOMER PASS ID TO NULL IF IT IS ATTACHED TO ANY OTHER VEHICLE
														$this->Vehicle->updateAll(array('Vehicle.customer_pass_id'=>NULL),array('Vehicle.customer_pass_id'=>$customerPassId));
														$customerPassArray['CustomerPass']['vehicle_id']=null;
														if($previousId){
															//SET VEHICLE ID TO NULL IF IT IS ATTACHED TO ANY OTHER PASS
															$this->CustomerPass->updateAll(array('CustomerPass.vehicle_id'=>NULL),array('CustomerPass.vehicle_id'=>$previousId));
															$vehicleArray['Vehicle']=array('id'=>$previousId,
																						   'customer_pass_id'=>null								
															);
															if($this->Vehicle->save($vehicleArray,false)){
																CakeLog::write('passRenewedVehicleNull', ''.AuthComponent::user('username').' : Pass renewed with Pass ID: '.$passId.' Customer Pass ID: <a href="/admin/CustomerPasses/edit/'.$customerPassId.'">'.$customerPassId.'</a> renewed by User: '.AuthComponent::user('first_name').' '.AuthComponent::user('last_name').', in Property: '.CakeSession::read('PropertyName').', VEHICLE SET NULL');
															}else{
																CakeLog::write('passRenewedVehicleNotNULL', ''.AuthComponent::user('username').' : Pass renewed with Pass ID: '.$passId.' And Customer Pass ID: <a href="/admin/CustomerPasses/edit/'.$customerPassId.'">'.$customerPassId.'</a> renewed by User: '.AuthComponent::user('first_name').' '.AuthComponent::user('last_name').', in Property: '.CakeSession::read('PropertyName').', VEHICLE NOT SET NULL');
															}
														}
													}
												}
											}
											if($recurring_profile_id){
												$customerPassArray['CustomerPass']['recurring_profile_id']=$recurring_profile_id;
											}
											if($recurring_profile_status){
												$customerPassArray['CustomerPass']['recurring_profile_status']=$recurring_profile_status;
											}
											$this->loadModel('Vehicle');
											$vehicleArray['Vehicle']=array('id'=>$previousId,
																		   'customer_pass_id'=>null
																			
											);
											if($this->CustomerPass->save($customerPassArray,false)){
														$this->loadModel('Pass');
														$passName=$this->Pass->givePassName($passId);
														if($advance){
															CakeLog::write('passRenewedAdvance', ''.AuthComponent::user('username').' : Pass renewed IN ADVANCE with Pass ID: '.$passId.' and Pass Name: '.$passName.', and Customer Pass ID: <a href="/admin/CustomerPasses/edit/'.$customerPassId.'">'.$customerPassId.'</a> renewed by User: '.AuthComponent::user('first_name').' '.AuthComponent::user('last_name').', in Property: '.CakeSession::read('PropertyName').', Amount Paid: $'.$passRenewalcost.' EXPIRY DATE WAS : '.$pass_expiry);
														}else{
															CakeLog::write('passRenewed', ''.AuthComponent::user('username').' : Pass renewed with Pass ID: '.$passId.' and Pass Name: '.$passName.', and Customer Pass ID: <a href="/admin/CustomerPasses/edit/'.$customerPassId.'">'.$customerPassId.'</a> renewed by User: '.AuthComponent::user('first_name').' '.AuthComponent::user('last_name').', in Property: '.CakeSession::read('PropertyName').', Amount Paid: $'.$passRenewalcost.'');
														}
														$billAddressArray['BillingAddress']['user_id']=$this->Auth->user('id');
														$billAddressArray['BillingAddress']['email']=$this->request->data['Transaction']['email'];
														$billAddressArray['BillingAddress']['first_name']=$this->request->data['Transaction']['first_name'];
														$billAddressArray['BillingAddress']['last_name']=$this->request->data['Transaction']['last_name'];
														$billAddressArray['BillingAddress']['address_line_1']=$this->request->data['Transaction']['address_line_1'];
														$billAddressArray['BillingAddress']['address_line_2']=$this->request->data['Transaction']['address_line_2'];
														$billAddressArray['BillingAddress']['city']=$this->request->data['Transaction']['city'];
														$billAddressArray['BillingAddress']['state']=$this->request->data['Transaction']['state'];
														$billAddressArray['BillingAddress']['zip']=$this->request->data['Transaction']['zip'];
														$billAddressArray['BillingAddress']['phone']=$this->request->data['Transaction']['phone'];
													
														$this->loadModel('BillingAddress');
														$billAddress=new BillingAddress();
														$billAddress->create();
														$rslt=$billAddress->hasAny(array('user_id'=>$this->Auth->user('id')));
														if($rslt==true){
															$billAddressId=$billAddress->find('first',array('fields'=>array('id'),'conditions'=>array('user_id'=>$this->Auth->user('id'))));
															$billAddressArray['BillingAddress']['id']=$billAddressId['BillingAddress']['id'];
														}
														if($billAddress->save($billAddressArray,false)){																
																$this->Session->setFlash('Your transaction has been completed succesfully. Pass renewed successfully. ','sucess');
																$this->Session->delete('Allowed');
																$this->Session->write('Allowed',false);
																return $this->redirect(array('controller'=>'CustomerPasses','action' => 'view'));
														}else{
															$this->Session->setFlash(__('Billing Address Not Saved'));	 
															$this->redirect(array('controller'=>'CustomerPasses','action' => 'view'));
														}				
										}
										else {
											$this->Session->setFlash('Pass not saved','error');	
											$this->redirect(array('action'=>'buyRemainingPass'));
										}
								} else {
									$this->Session->setFlash('The transaction could not be saved. Contact Admin','error');
								}																
						}
						else{
							$this->Session->setFlash('Transaction Failed','error');
						}					
					}
					else{
						$this->Session->setFlash($errorMessage,'error');
					}
		}
		else{
			$this->Session->setFlash('Validation Failure','error');
		}
	}
	$this->layout='customer';
	$this->loadModel('BillingAddress');
	$this->BillingAddress->recursive=-1;
    $billingAddress=$this->BillingAddress->find('first',array('conditions'=>array('user_id'=>$this->Auth->user('id'))));
    $this->loadModel('State');
    $state=new State();
    $states=$state->find('list');
    $this->set(compact('billingAddress','states','passRenewalcost','passExpiryDate','passName','renewAllowed'));
	
 }
    /*     * ******************************************
     * Activate Pass Or Buy Corresponding Permit Updated on 9th Feb 2015
     */

    public function activatePass($customerPassId = null, $passId = null, $permitId = null) {
        $this->layout = 'customer';
        $this->loadModel('CustomerPass');
        if (!$this->CustomerPass->exists($customerPassId)) {
            throw new NotFoundException(__('Invalid Customer Pass'));
        } else {
            $userID = $this->CustomerPass->field('user_id', array('id' => $customerPassId));
            if ($userID != AuthComponent::user('id')) {
                throw new NotFoundException(__('Invalid DATA'));
            }
        }
        $this->loadModel('Pass');
        if (!$this->Pass->exists($passId)) {
            throw new NotFoundException(__('Invalid Pass'));
        }
        $this->loadModel('Package');
        if (!$this->Package->exists($permitId)) {
            throw new NotFoundException(__('Invalid Package'));
        }
        $packageDate = $this->CustomerPass->field('membership_vaild_upto', array('id' => $customerPassId));
        if (!is_null($packageDate)) {
            $dt = new DateTime();
            $currentDateTime = $dt->format('Y-m-d H:i:s');
            if ($packageDate > $currentDateTime) {
                $this->Session->setFlash('This pass is already active', 'warning');
                $this->redirect(array('controller' => 'Vehicles', 'action' => 'add_vehicles'));
            }
        }
        $this->CustomerPass->recursive = -1;
        $customerDetails = $this->CustomerPass->find('first', array('conditions' => array('id' => $customerPassId)));

        $this->Package->recursive = -1;
        $packageDetails = $this->Package->find('first', array('conditions' => array('id' => $permitId)));



        if ($packageDetails['Package']['is_fixed_duration'] == 0) {
            $dt = new DateTime();
            $currentDateTime = $dt->format('Y-m-d H:i:s');
            switch ($packageDetails['Package']['duration_type']) {
                case "Hour":
                    $date = new DateTime($currentDateTime);
                    $k = (int) $packageDetails['Package']['duration'];
                    $date->add(new DateInterval('PT' . $k . 'H'));
                    $packageDetails['Package']['expiration_date'] = date_format($date, 'Y-m-d H:i:s');
                    break;
                case "Day":
                    $date = new DateTime($currentDateTime);
                    $k = $packageDetails['Package']['duration'];
                    $date->add(new DateInterval('P' . $k . 'D'));
                    $packageDetails['Package']['expiration_date'] = date_format($date, 'Y-m-d H:i:s');
                    break;
                case "Week":
                    $date = new DateTime($currentDateTime);
                    $k = (int) $packageDetails['Package']['duration'] * 7;
                    $date->add(new DateInterval('P' . $k . 'D'));
                    $packageDetails['Package']['expiration_date'] = date_format($date, 'Y-m-d H:i:s');
                    break;
                case "Month":
                    $date = new DateTime($currentDateTime);
                    $k = (int) $packageDetails['Package']['duration'];
                    $date->add(new DateInterval('P' . $k . 'M'));
                    $packageDetails['Package']['expiration_date'] = date_format($date, 'Y-m-d H:i:s');
                    break;
                case "Year":
                    $date = new DateTime($currentDateTime);
                    $k = (int) $packageDetails['Package']['duration'];
                    $date->add(new DateInterval('P' . $k . 'Y'));
                    $packageDetails['Package']['expiration_date'] = date_format($date, 'Y-m-d H:i:s');
                    break;
            }
        }
        if ($this->request->is('post')) {
            $this->request->data['Transaction']['user_id'] = $this->Auth->user('id');
            $this->Transaction->set($this->request->data);
            if ($this->Transaction->validates()) {
                $payment = array(
                    'amount' => $packageDetails['Package']['cost'],
                    'card' => $this->request->data['Transaction']['card_number'], // This is a sandbox CC
                    'expiry' => array(
                        'M' => $this->request->data['Transaction']['month'],
                        'Y' => $this->request->data['Transaction']['year'],
                    ),
                    'cvv' => $this->request->data['Transaction']['cvv'],
                    'currency' => 'USD' // Defaults to GBP if not provided
                );
                $errorMessage = null;
                $response = null;
                try {
                    $response = $this->Paypal->doDirectPayment($payment);
                    // Check $response[ACK]='Success' OR 'OK'
                } catch (Exception $e) {
                    $errorMessage = $e->getMessage();
                }
                if ($errorMessage == null) {
                    if ($response['ACK'] == 'Success') {
                        $this->Transaction->create();
                        $dateTransaction = date('Y-m-d H:i:s', strtotime($response['TIMESTAMP']));
                        $arraySerialize = serialize($response);
                        $transactionArray['Transaction']['paypal_tranc_id'] = $response['TRANSACTIONID'];
                        $transactionArray['Transaction']['user_id'] = $this->request->data['Transaction']['user_id'];
                        $transactionArray['Transaction']['date_time'] = $dateTransaction;
                        $transactionArray['Transaction']['amount'] = $response['AMT'];
                        $transactionArray['Transaction']['result'] = $response['ACK'];
                        $transactionArray['Transaction']['pass_id'] = $passId;
                        $transactionArray['Transaction']['transaction_array'] = $arraySerialize;
                        $transactionArray['Transaction']['first_name'] = $this->request->data['Transaction']['first_name_cc'];
                        $transactionArray['Transaction']['last_name'] = $this->request->data['Transaction']['last_name_cc'];
                        $transactionArray['Transaction']['comments'] = "passActivated";
                        if ($this->Transaction->save($transactionArray, false)) {
                            $customerPassArray['CustomerPass']['transaction_id'] = $this->Transaction->getLastInsertId();
                            $customerPassArray['CustomerPass']['membership_vaild_upto'] = $packageDetails['Package']['expiration_date'];
                            $customerPassArray['CustomerPass']['package_id'] = $permitId;
                            $customerPassArray['CustomerPass']['id'] = $customerPassId;
							//RECURRING CHECK START
							if(Configure::read('RECURRING')){
								if ($packageDetails['Package']['is_fixed_duration'] == 0) {
									if ($packageDetails['Package']['is_recurring'] == 1) {
										if ($packageDetails['Package']['is_guest'] == 0) {
											if ($packageDetails['Package']['cost'] > 0) {
												$this->loadModel('RecurringProfile');
												$recurAmount = $packageDetails['Package']['cost'];
												$recurringPayment = array();
												$previousProfile=$this->CustomerPass->field('recurring_profile_id',array('id'=>$customerPassId));
												$this->loadModel('User');
												$this->User->recursive=-1;
												$userDetails=$this->User->find('first',array('conditions'=>array('id'=>AuthComponent::user('id'))));
												if($previousProfile){
													$recurringPayment = array(
																	'amount' => $recurAmount,
																	'currency' => 'USD' ,
																	'BILLINGPERIOD'=>$packageDetails['Package']['duration_type'],
																	'BILLINGFREQUENCY'=>$packageDetails['Package']['duration'],
																	'PROFILESTARTDATE'=>$packageDetails['Package']['expiration_date'],
																	'EMAIL'=>$userDetails['User']['email'],
																	'STREET' => $userDetails['User']['address_line_1'],						
																	'ZIP' => $userDetails['User']['zip'],
																	'PROFILEID'=>$previousProfile,
																	'expiry' => array(
																					'M' => $this->request->data['Transaction']['month'],
																					'Y' => $this->request->data['Transaction']['year'],
																				)
																);
												}else{
													$recurringPayment = array(
															'amount' => $recurAmount,
															'card' => $this->request->data['Transaction']['card_number'],
															'expiry' => array(
																'M' => $this->request->data['Transaction']['month'],
																'Y' => $this->request->data['Transaction']['year'],
															),
															'cvv' => $this->request->data['Transaction']['cvv'],
															'currency' => 'USD',
															'BILLINGPERIOD' => $packageDetails['Package']['duration_type'],
															'BILLINGFREQUENCY' => $packageDetails['Package']['duration'],
															'PROFILESTARTDATE' => $packageDetails['Package']['expiration_date'],
															'FIRSTNAME' => AuthComponent::user('first_name'),
															'LASTNAME' => AuthComponent::user('last_name'),
															'EMAIL' => AuthComponent::user('email'),
															'STREET' => AuthComponent::user('address_line_1'),
															'CITY' => AuthComponent::user('city'),
															'STATE' => AuthComponent::user('state'),
															'ZIP' => AuthComponent::user('zip')
														);
												}
											   $errorMessage=null;
											   try {
													if($previousProfile){
														$recurringResponse = $this->PayFlow->modifyRecurringProfilePayFlow($recurringPayment);
													}else{
														$recurringResponse = $this->PayFlow->createRecurringProfilePayFlow($recurringPayment);
													}
												}catch (Exception $e) {
													$errorMessage=$e->getMessage();
												}
												if ($errorMessage == null) {
													if(isset($recurringResponse['RESULT']) && $recurringResponse['RESULT'] == 0) {
														if($recurringResponse['RESPMSG']=='Approved'){
															$recurringResponse['PROFILESTATUS']='Active';
															$array['RecurringProfile']=array(
																			 'recurring_profile_id'=>$recurringResponse['PROFILEID'],
																			 'message'=>$previousProfile?'Recurring Profile modified':'Recurring Profile created',
																			 'status'=>$recurringResponse['PROFILESTATUS'],
																			 'customer_pass_id'=>$customerPassId
															);
															if($previousProfile){
																$idRecurring=$this->RecurringProfile->field('id',array('recurring_profile_id'=>$previousProfile));
																$array['RecurringProfile']['id']=$idRecurring;
															}else{
																$this->RecurringProfile->create();
															}
															if($this->RecurringProfile->save($array)){
																if($previousProfile){
																	CakeLog::write('profileDataModified','Profile Data Modified for ID: '.$recurringResponse['PROFILEID']);
																}else{
																	CakeLog::write('profileDataSaved','Profile Data Saved for ID: '.$recurringResponse['PROFILEID']);
																}
															}else{
																if($previousProfile){
																	CakeLog::write('profileDataNotModified','Profile Data Not Modified for ID: '.$recurringResponse['PROFILEID']);
																}else{	
																	CakeLog::write('profileDataNotSaved','Profile Data Not Saved for ID: '.$recurringResponse['PROFILEID']);
																}
															}
														}
													}else{
														if($previousProfile){
															  CakeLog::write('recurringProfileModificationFailed', ''.$userDetails['User']['username'].' : Recurring Profile creation failed User: '.AuthComponent::user('first_name').' '.AuthComponent::user('last_name')); 
														
														}else{
															CakeLog::write('recurringProfileCreationFailed', ''.$userDetails['User']['username'].' : Recurring Profile creation failed User: '.AuthComponent::user('first_name').' '.AuthComponent::user('last_name')); 
														}
													}
												}else{
													if($previousProfile){
															  CakeLog::write('recurringProfileModificationFailed', ''.$userDetails['User']['username'].' : Recurring Profile creation failed User: '.AuthComponent::user('first_name').' '.AuthComponent::user('last_name')); 
														
													}else{
														CakeLog::write('recurringProfileCreationException', ''.$userDetails['User']['username'].' : Recurring Profile creation failed User: '.AuthComponent::user('first_name').' '.AuthComponent::user('last_name')); 
													}
												}
											}
										}
									}
								}
							}
							//RECURRING CHECK END

                            $previousId = $this->CustomerPass->field('vehicle_id', array('id' => $customerPassId));
                            if ($this->CustomerPass->save($customerPassArray, false)) {

                                $this->loadModel('Package');
                                $packageName = $this->Package->givePackageName($permitId);
                                CakeLog::write('validPassActivated', '' . AuthComponent::user('username') . ' : Valid Pass Activated Customer Pass ID: <a href="/admin/CustomerPasses/edit/' . $customerPassId . '">' . $customerPassId . '</a> Package ID: ' . $permitId . ', Package Name: ' . $packageName . ' Activated by User: ' . AuthComponent::user('first_name') . ' ' . AuthComponent::user('last_name') . ' In Property: ' . CakeSession::read('PropertyName') . ', Cost Of Activation: ' . $response['AMT'] . '');
                                $billAddressArray['BillingAddress']['user_id'] = $this->request->data['Transaction']['user_id'];
                                $billAddressArray['BillingAddress']['email'] = $this->request->data['Transaction']['email'];
                                $billAddressArray['BillingAddress']['first_name'] = $this->request->data['Transaction']['first_name'];
                                $billAddressArray['BillingAddress']['last_name'] = $this->request->data['Transaction']['last_name'];
                                $billAddressArray['BillingAddress']['address_line_1'] = $this->request->data['Transaction']['address_line_1'];
                                $billAddressArray['BillingAddress']['address_line_2'] = $this->request->data['Transaction']['address_line_2'];
                                $billAddressArray['BillingAddress']['city'] = $this->request->data['Transaction']['city'];
                                $billAddressArray['BillingAddress']['state'] = $this->request->data['Transaction']['state'];
                                $billAddressArray['BillingAddress']['zip'] = $this->request->data['Transaction']['zip'];
                                $billAddressArray['BillingAddress']['phone'] = $this->request->data['Transaction']['phone'];

                                $this->loadModel('BillingAddress');
                                $billAddress = new BillingAddress();
                                $rslt = $billAddress->hasAny(array('user_id' => $this->request->data['Transaction']['user_id']));
                                if ($rslt == true) {
                                    $billAddressId = $billAddress->find('first', array('fields' => array('id'), 'conditions' => array('user_id' => $this->request->data['Transaction']['user_id'])));
                                    $billAddressArray['BillingAddress']['id'] = $billAddressId['BillingAddress']['id'];
                                }
                                $billAddress->create();
                                if ($billAddress->save($billAddressArray, false)) {
                                    $this->CustomerPass->recursive = -1;
                                    $newCustomerPass = $this->CustomerPass->find('first', array('fields' => array('id'), 'conditions' => array('user_id' => $this->Auth->user('id'), 'property_id' => $this->Session->read('PropertyId'), 'pass_id' => $passId)));
                                    $this->Session->setFlash('Your transaction has been completed succesfully', 'success');
                                    return $this->redirect(array('controller' => 'vehicles', 'action' => 'add', $newCustomerPass['CustomerPass']['id']));
                                } else {
                                    $this->Session->setFlash('Billing Address Not Saved', 'error');
                                }
                            } else {
                                $this->Session->setFlash('Pass not saved', 'error');
                            }
                        } else {
                            $this->Session->setFlash('The transaction could not be saved. Contact Admin', 'error');
                        }
                    } else {
                        $this->Session->setFlash('Transaction Failed', 'error');
                    }
                } else {
                    $this->Session->setFlash($errorMessage, 'error');
                }
            }
        }
        $this->Pass->recursive = -1;
        $pass = $this->Pass->find('first', array('conditions' => array('id' => $passId)));
        $this->loadModel('BillingAddress');
        $billAdd = new BillingAddress();
        $billAdd->recursive = -1;
        $billingAddress = $billAdd->find('first', array('conditions' => array('user_id' => $this->Auth->user('id'))));
        $this->loadModel('State');
        $state = new State();
        $states = $state->find('list');
        $this->set(compact('states', 'packageDetails', 'customerDetails', 'billingAddress'));
    }

    public function get_transactions($userId) {
        $this->autoRender = $this->layout = false;
        $output = array();
        if (!is_null($userId)) {
            $output = $this->Transaction->getTransactionData($userId);
        }
        echo json_encode($output);
    }

    public function admin_get_transactions($userId) {
        $this->autoRender = $this->layout = false;
        $output = array();
        if (!is_null($userId)) {
            $output = $this->Transaction->getTransactionData($userId);
        }
        echo json_encode($output);
    }

    /*     * ***********************************************
     * Select Passes Updation Done On 10th Feb 2015
     */

    public function select_passes() {
        if ($this->request->is('POST')) {
            if ($this->request->data['Transaction']['other_passes'] == 'select' && $this->request->data['Transaction']['guest_pass'] == 'no') {
                $this->Session->setFlash('Please Select Atleast One Pass', 'warning');
            } else {
                $this->Session->write('other_passes', $this->request->data['Transaction']['other_passes']);
                $this->Session->write('guest_pass', $this->request->data['Transaction']['guest_pass']);
                $this->redirect(array('action' => 'buy_selected_passes'));
            }
        }
        $this->layout = 'customer';
        $this->loadModel('Package');
        $this->loadModel('Pass');
        $this->Pass->recursive = -1;
        $dt = new DateTime();
        $currentDateTime = $dt->format('Y-m-d H:m:s');
        $totalPassNumber = $this->Pass->find('count', array('conditions' => array(
                'Pass.property_id' => CakeSession::read('PropertyId')
            )
        ));
        $guestPassNumber = $this->Package->find('count', array('conditions' => array(
                'Package.property_id' => CakeSession::read('PropertyId'),
                'Package.is_guest ' => 1, 'Package.pass_id IS NOT NULL AND (Package.expiration_date IS NULL OR Package.expiration_date > \'' . $currentDateTime . '\')')
        ));
        $guestPassRequiredNumber = $this->Package->find('count', array('conditions' => array(
                'Package.property_id' => CakeSession::read('PropertyId'),
                ' Package.is_required' => 1, 'Package.pass_id IS NOT NULL AND (Package.expiration_date IS NULL OR Package.expiration_date > \'' . $currentDateTime . '\')',
                'Package.is_guest ' => 1)
        ));
        $otherPassesRequiredNumber = $this->Package->find('count', array('conditions' => array(
                'Package.property_id' => CakeSession::read('PropertyId'),
                ' Package.is_required' => 1,
                'Package.is_guest ' => 0, 'Package.pass_id IS NOT NULL AND (Package.expiration_date IS NULL OR Package.expiration_date > \'' . $currentDateTime . '\')')
        ));
        $otherPassesNotRequiredNumber = $this->Package->find('count', array('conditions' => array(
                'Package.property_id' => CakeSession::read('PropertyId'),
                ' Package.is_required' => 0,
                'Package.is_guest ' => 0, 'Package.pass_id IS NOT NULL AND (Package.expiration_date IS NULL OR Package.expiration_date > \'' . $currentDateTime . '\')')
        ));

        $this->Pass->recursive = -1;
        $totalPassNoPackage = $this->Pass->find('count', array('conditions' => array('Pass.package_id IS NULL AND (Pass.expiration_date IS NULL OR Pass.expiration_date > \'' . $currentDateTime . '\')', 'property_id' => CakeSession::read('PropertyId'))));
        $totalPassNumberNoGuest = $otherPassesRequiredNumber + $totalPassNoPackage + $otherPassesNotRequiredNumber;
        $guestPass = $guestSelectOptions = array();
        if ($guestPassNumber) {
            $this->Package->recursive = -1;
            $guestPass = $this->Package->find('first', array(
                'fields' => array('Package.*,Pass.*'),
                'joins' => array(
                    array(
                        'table' => 'passes',
                        'alias' => 'Pass',
                        'type' => 'LEFT',
                        'conditions' => array(
                            'Pass.id=Package.pass_id'
                        )
                    )
                ),
                'conditions' => array('Package.property_id' => CakeSession::read('PropertyId'), ' Package.is_guest' => 1, '(Package.expiration_date IS NULL OR Package.expiration_date > \'' . $currentDateTime . '\')')
            ));
            if ($guestPassRequiredNumber) {
                $guestSelectOptions['required'] = "Required";
            } else if (($guestPassNumber - $guestPassRequiredNumber) != 0) {
                $guestSelectOptions['yes'] = "YES";
                $guestSelectOptions['no'] = "NO";
            } else {
                $guestSelectOptions['noPass'] = "No Guest Pass";
            }
        } else {
            $guestSelectOptions['noPass'] = "No Guest Pass";
        }

        $otherPassSelectOptions = array();

        if ($otherPassesRequiredNumber) {
            for ($i = $otherPassesRequiredNumber; $i <= $totalPassNumberNoGuest; $i++) {
                $otherPassSelectOptions[$i] = $i;
            }
        } else {
            $otherPassSelectOptions['select'] = 'Select Pass';
            for ($i = 1; $i <= $totalPassNumberNoGuest; $i++) {
                $otherPassSelectOptions[$i] = $i;
            }
        }

        $this->set(compact('otherPassSelectOptions', 'guestSelectOptions', 'guestPass'));
    }

    /*     * ****************************************************************
     * Buy Selected Passes Updation Done On 10th Feb 2015
     */

     public function buy_selected_passes(){
		$passNo=CakeSession::read('other_passes');
		$guestPass=CakeSession::read('guest_pass');
		$this->layout = 'customer'; 
		$this->loadModel('Pass');
		$this->loadModel('Package'); 
		$this->Package->recursive=-1;
		$dt = new DateTime();
        $currentDateTime = $dt->format('Y-m-d H:i:s');
		$allPasses=$allGuestPasses=$allOtherPasses=$otherRequiredPasses=$otherPasses=$returnArray=array();
		$totalPassNumber = $this->Pass->find('count', array('conditions' => array(
																						'Pass.property_id' => CakeSession::read('PropertyId')
																					)
																));
		$otherPassesRequiredNumber = $this->Package->find('count', array('conditions' => array(
																						'Package.property_id' => CakeSession::read('PropertyId'), 
																						' Package.is_required' => 1, 
																						'Package.is_guest ' => 0,'Package.pass_id IS NOT NULL AND (Package.expiration_date IS NULL OR Package.expiration_date > \''.$currentDateTime.'\')')
																));
		if($passNo!='select'){
		if($passNo==0 || $passNo>$totalPassNumber){
			$passNo=$totalPassNumber;
		}
		if($passNo==$otherPassesRequiredNumber){
			$otherRequiredPasses = $this->Package->find('all', array(
													'fields' => array('Package.*,Pass.*'),
													'joins' => array(
																	array(
																			'table' => 'passes',
																			'alias' => 'Pass',
																			'type' => 'LEFT',
																			'conditions' => array(
																									'Pass.id=Package.pass_id'
																									)
																		)
																	),
													'conditions' => array('Package.property_id' => CakeSession::read('PropertyId'), 
																		  ' Package.is_guest' => 0,'Package.is_required'=>1,'Package.pass_id IS NOT NULL AND ((Package.expiration_date IS NULL OR Package.expiration_date > \''.$currentDateTime.'\') AND (Pass.expiration_date IS NULL OR Pass.expiration_date > \''.$currentDateTime.'\'))')
											));
		  $allOtherPasses=$otherRequiredPasses ;
		}else{
			$this->Package->recursive=-1;
			$otherRequiredPasses = $this->Package->find('all', array(
													'fields' => array('Package.*,Pass.*'),
													'joins' => array(
																	array(
																			'table' => 'passes',
																			'alias' => 'Pass',
																			'type' => 'LEFT',
																			'conditions' => array(
																									'Pass.id=Package.pass_id'
																									)
																		)
																	),
													'conditions' => array('Package.property_id' => CakeSession::read('PropertyId'), 
																		  ' Package.is_guest' => 0,'Package.is_required'=>1,'Package.pass_id IS NOT NULL AND ((Package.expiration_date IS NULL OR Package.expiration_date > \''.$currentDateTime.'\') AND (Pass.expiration_date IS NULL OR Pass.expiration_date > \''.$currentDateTime.'\'))')
											));
			$countRequiredPasses=count($otherRequiredPasses);
			$nextPassCount=$passNo-$countRequiredPasses;
			$limit=0;
			if($nextPassCount>0){
				$otherNonRequiredPasses = $this->Package->find('all', array(
													'fields' => array('Package.*,Pass.*'),
													'joins' => array(
																	array(
																			'table' => 'passes',
																			'alias' => 'Pass',
																			'type' => 'LEFT',
																			'conditions' => array(
																									'Pass.id=Package.pass_id'
																									)
																		)
																	),
													'conditions' => array('Package.property_id' => CakeSession::read('PropertyId'), 
																		  ' Package.is_guest' => 0,'Package.is_required'=>0,'Package.pass_id IS NOT NULL AND ((Package.expiration_date IS NULL OR Package.expiration_date > \''.$currentDateTime.'\') AND (Pass.expiration_date IS NULL OR Pass.expiration_date > \''.$currentDateTime.'\'))'),
													'limit'=>$nextPassCount
											));
				$countNonRequiredPasses=count($otherNonRequiredPasses);
				$limit=$passNo-($countRequiredPasses+$countNonRequiredPasses);
			}
			if($limit>0){
				$this->Pass->recursive=-1;
				$otherPasses = $this->Pass->find('all', array(
													       'conditions' => array('Pass.property_id' => CakeSession::read('PropertyId'),'Pass.package_id'=>NULL,'(Pass.expiration_date IS NULL OR Pass.expiration_date > \''.$currentDateTime.'\')'), 
														  'limit'=>$limit
											));
			}
			if($passNo==1){ 
				$arr=array();
				if(!empty($otherRequiredPasses)){
					$arr[]=$otherRequiredPasses[0];
				}elseif(!empty($otherNonRequiredPasses)){
					$arr[]=$otherNonRequiredPasses[0];
				}elseif(!empty($otherPasses)){
					$arr[]=$otherPasses[0];
				}
				$allOtherPasses=$arr;
			}else{
				$allOtherPasses=array_merge($otherRequiredPasses,$otherNonRequiredPasses,$otherPasses);
			}
		}
		}
		if($guestPass=="required" || $guestPass=="yes"){
			$allGuestPasses = $this->Package->find('all', array(
													'fields' => array('Package.*,Pass.*'),
													'joins' => array(
																	array(
																			'table' => 'passes',
																			'alias' => 'Pass',
																			'type' => 'LEFT',
																			'conditions' => array(
																									'Pass.id=Package.pass_id'
																									)
																		)
																	),
													'conditions' => array('Package.property_id' => CakeSession::read('PropertyId'), 
																		  ' Package.is_guest' => 1,'Package.pass_id IS NOT NULL AND (Package.expiration_date IS NULL OR Package.expiration_date > \''.$currentDateTime.'\')'
																		  ),
													'limit'=>1
											));		
		}
		$allPasses=array_merge($allOtherPasses,$allGuestPasses);
		$totalAmount=0;
		$returnArray=array();
		for ($i = 0; $i < count($allPasses); $i++) {
			$passTotalCost=0;
			$returnArray[$i]['Pass']['passName']=$allPasses[$i]['Pass']['name'];
			if(isset($allPasses[$i]['Package'])){
				$dt = new DateTime();
                $currentDateTime = $dt->format('Y-m-d H:i:s');
				if ($allPasses[$i]['Package']['is_fixed_duration'] == 0) {
                        switch ($allPasses[$i]['Package']['duration_type']) {
                            case "Hour":
                                $date = new DateTime($currentDateTime);
                                $k = (int) $allPasses[$i]['Package']['duration'];
                                $date->add(new DateInterval('PT' . $k . 'H'));
                                $allPasses[$i]['Package']['expiration_date'] = date_format($date, 'Y-m-d H:i:s');
                                break;
                            case "Day":
                                $date = new DateTime($currentDateTime);
                                $k = $allPasses[$i]['Package']['duration'];
                                $date->add(new DateInterval('P' . $k . 'D'));
                                $allPasses[$i]['Package']['expiration_date'] = date_format($date, 'Y-m-d H:i:s');
                                break;
                            case "Week":
                                $date = new DateTime($currentDateTime);
                                $k = (int) $allPasses[$i]['Package']['duration'] * 7;
                                $date->add(new DateInterval('P' . $k . 'D'));
                                $allPasses[$i]['Package']['expiration_date'] = date_format($date, 'Y-m-d H:i:s');
                                break;
                            case "Month":
                                $date = new DateTime($currentDateTime);
                                $k = (int) $allPasses[$i]['Package']['duration'];
                                $date->add(new DateInterval('P' . $k . 'M'));
                                $allPasses[$i]['Package']['expiration_date'] = date_format($date, 'Y-m-d H:i:s');
                                break;
                            case "Year":
                                $date = new DateTime($currentDateTime);
                                $k = (int) $allPasses[$i]['Package']['duration'];
                                $date->add(new DateInterval('P' . $k . 'Y'));
                                $allPasses[$i]['Package']['expiration_date'] = date_format($date, 'Y-m-d H:i:s');
                                break;
                        }
                }
                $date3 = new DateTime($allPasses[$i]['Package']['expiration_date']);
				$datePack=$date3->format('Y-m-d');
                $allPasses[$i]['Package']['expiration_date']= $datePack.' 23:59:59';
             
				$returnArray[$i]['Pass']['packageName']=$allPasses[$i]['Package']['name'];
				if($allPasses[$i]['Package']['is_required']==1){
							 $returnArray[$i]['Pass']['required']="Yes";
				}else{
						    $returnArray[$i]['Pass']['required']="No";
				}
				if ($allPasses[$i]['Package']['is_guest'] == 1) {
						$returnArray[$i]['Pass']['isGuest']="Yes";
						$returnArray[$i]['Pass']['packageCost']="NA";
						$passTotalCost=$allPasses[$i]['Pass']['deposit'] + $allPasses[$i]['Pass']['cost_1st_year'];
						$totalAmount = $totalAmount + $passTotalCost;
				}else{
					 $returnArray[$i]['Pass']['isGuest']="No";
					 $packageNameDetail='$ '.$allPasses[$i]['Package']['cost'];
						  if($allPasses[$i]['Package']['is_fixed_duration']==0){
							 if($allPasses[$i]['Package']['is_recurring'] == 1){
								if($allPasses[$i]['Package']['is_guest'] == 0){
									if($allPasses[$i]['Package']['cost']>0){
										$packageNameDetail=$packageNameDetail.'<br> <font color="red">Recurring Payment*</font><br>
																				Interval : '.$allPasses[$i]['Package']['duration'].' '.$allPasses[$i]['Package']['duration_type'];
									}
								}
							 }
						  }
					 $returnArray[$i]['Pass']['packageCost']=$packageNameDetail;
					 $passTotalCost=$allPasses[$i]['Pass']['deposit'] + $allPasses[$i]['Pass']['cost_1st_year'] + $allPasses[$i]['Package']['cost'];
					 $totalAmount = $totalAmount + $passTotalCost;
				}
			}else{
					$returnArray[$i]['Pass']['packageName']= "No Package";
					$returnArray[$i]['Pass']['packageCost']="NA";
					$returnArray[$i]['Pass']['required']="No";
					$returnArray[$i]['Pass']['isGuest']="No";
					$passTotalCost=$allPasses[$i]['Pass']['deposit'] + $allPasses[$i]['Pass']['cost_1st_year'];
					$totalAmount = $totalAmount +$passTotalCost;
			}
			$returnArray[$i]['Pass']['passDeposit']=$allPasses[$i]['Pass']['deposit'];
			$returnArray[$i]['Pass']['cost']=$allPasses[$i]['Pass']['cost_1st_year'];
			$dt = new DateTime();
            $currentDateTime = $dt->format('Y-m-d H:i:s');
			if ($allPasses[$i]['Pass']['is_fixed_duration'] == 0) {
                        switch ($allPasses[$i]['Pass']['duration_type']) {
                            case "Hour":
                                $date = new DateTime($currentDateTime);
                                $k = (int) $allPasses[$i]['Pass']['duration'];
                                $date->add(new DateInterval('PT' . $k . 'H'));
                                $allPasses[$i]['Pass']['expiration_date'] = date_format($date, 'Y-m-d H:i:s');
                                break;
                            case "Day":
                                $date = new DateTime($currentDateTime);
                                $k = $allPasses[$i]['Pass']['duration'];
                                $date->add(new DateInterval('P' . $k . 'D'));
                                $allPasses[$i]['Pass']['expiration_date'] = date_format($date, 'Y-m-d H:i:s');
                                break;
                            case "Week":
                                $date = new DateTime($currentDateTime);
                                $k = (int) $allPasses[$i]['Pass']['duration'] * 7;
                                $date->add(new DateInterval('P' . $k . 'D'));
                                $allPasses[$i]['Pass']['expiration_date'] = date_format($date, 'Y-m-d H:i:s');
                                break;
                            case "Month":
                                $date = new DateTime($currentDateTime);
                                $k = (int) $allPasses[$i]['Pass']['duration'];
                                $date->add(new DateInterval('P' . $k . 'M'));
                                $allPasses[$i]['Pass']['expiration_date'] = date_format($date, 'Y-m-d H:i:s');
                                break;
                            case "Year":
                                $date = new DateTime($currentDateTime);
                                $k = (int) $allPasses[$i]['Pass']['duration'];
                                $date->add(new DateInterval('P' . $k . 'Y'));
                                $allPasses[$i]['Pass']['expiration_date'] = date_format($date, 'Y-m-d H:i:s');
                                break;
                        }
              }
              $date1 = new DateTime($allPasses[$i]['Pass']['expiration_date']);
              $datePass=$date1->format('Y-m-d');
              $allPasses[$i]['Pass']['expiration_date']= $datePass.' 23:59:59';
              $expiryDate= date("m/d/Y",strtotime( $allPasses[$i]['Pass']['expiration_date']));
              $returnArray[$i]['Pass']['expiryDate']=$expiryDate;
              $returnArray[$i]['Pass']['passCost']=$passTotalCost;
              
        }
        if($this->request->is('Post')){
			$this->loadModel('CustomerPass');
            if ($this->CustomerPass->hasAny(array('user_id' => $this->Auth->user('id')))) {
				  $this->Session->setFlash('You have already purchased passes', 'warning');
				  $this->Session->delete('other_passes');
				  $this->Session->delete('guest_pass');
				  $this->Session->write('Allowed', true);
				  return $this->redirect(array('controller' => 'CustomerPasses', 'action' => 'view'));
            }
            $netAmountPayable=$totalAmount;
            if(isset($this->request->data['Transaction']['coupon'])){
				if(!empty($this->request->data['Transaction']['coupon'])){
					$this->loadModel('TableCoupon');
					$coupon=$this->TableCoupon->find('first',array('conditions'=>array('coupon_code'=>$this->request->data['Transaction']['coupon'],'property_id'=>$_SESSION['PropertyId'])));
					if($coupon){
						if($coupon['TableCoupon']['valid']==1){
							$dt = new DateTime();
							$currentDateTime= $dt->format('Y-m-d H:i:s');
							if($coupon['TableCoupon']['start_date']){
								if($coupon['TableCoupon']['start_date']<$currentDateTime){
									if($coupon['TableCoupon']['expiration_date']){
										if($coupon['TableCoupon']['expiration_date']>$currentDateTime){
											$newtotalAmount=$totalAmount-(($coupon['TableCoupon']['discount']/100)*$totalAmount);
										}
									}else{
										$newtotalAmount=$totalAmount-(($coupon['TableCoupon']['discount']/100)*$totalAmount);
									}
								}
							}else{
								if($coupon['TableCoupon']['expiration_date']){
									if($coupon['TableCoupon']['expiration_date']>$currentDateTime){
										$newtotalAmount=$totalAmount-(($coupon['TableCoupon']['discount']/100)*$totalAmount);
									}
								}else{	
									$newtotalAmount=$totalAmount-(($coupon['TableCoupon']['discount']/100)*$totalAmount);
								}
							}
							CakeLog::write('firstCouponApplied', AuthComponent::user('username').' : First Coupon Applied, COUPON CODE: '.$this->request->data['Transaction']['coupon'].', TOTAL AMOUNT: '.$totalAmount.', DISCOUNT: '.(($coupon['TableCoupon']['discount']/100)*$totalAmount).', NEW AMOUNT: '.$newtotalAmount);
							$newtotalAmount=number_format($newtotalAmount,2);
							$totalAmount=$newtotalAmount;
						}
					}
				}
			}
            if ($totalAmount == 0) {
                $this->Transaction->updateValidations();
            }
            $this->Transaction->set($this->request->data);
            if ($this->Transaction->validates()) {
				 $errorMessage = null;
                $response = null;
                if ($totalAmount > 0) {
                    $payment = array(
                        'amount' => $totalAmount,
                        'card' => $this->request->data['Transaction']['card_number'], 
                        'expiry' => array(
                            'M' => $this->request->data['Transaction']['month'],
                            'Y' => $this->request->data['Transaction']['year'],
                        ),
                        'cvv' => $this->request->data['Transaction']['cvv'],
                        'currency' => 'USD' 
                    );

                    try {
                         $response = $this->Paypal->doDirectPayment($payment);
                    } catch (Exception $e) {
                        $errorMessage = $e->getMessage();
                    }
                } else {
                    $response['TIMESTAMP'] = date('Y-m-d H:i:s');
                    $response['TRANSACTIONID'] = 'No Payment';
                    $response['ACK'] = 'Success';
                    $response['AMT'] = '0';
                    $response['PassDetail'] = $allPasses;

                    $this->request->data['Transaction']['first_name_cc'] = $this->Auth->user('first_name');
                    $this->request->data['Transaction']['last_name_cc'] = $this->Auth->user('last_name');
                }
                 if ($errorMessage == null) {
					if ($response['ACK'] == 'Success') {
						$passArray = ' ';
						$passName = ' ';
						
                        for ($i = 0; $i < count($allPasses); $i++) {
                            $passArray = $passArray . $allPasses[$i]['Pass']['id'];
                            $passName = $passName . $allPasses[$i]['Pass']['name'];
                            if ($i < (count($allPasses) - 1)) {
								$passArray = $passArray .',';
								$passName = $passName .',';
							}
                        }
                        $passArray=trim($passArray);
                        $passName=trim($passName);
                        $dateTransaction = date('Y-m-d H:i:s', strtotime($response['TIMESTAMP']));
                        $arraySerialize = serialize($response);
                        $transactionArray['Transaction']['paypal_tranc_id'] = $response['TRANSACTIONID'];
                        $transactionArray['Transaction']['user_id'] = $this->Auth->user('id');
                        $transactionArray['Transaction']['date_time'] = $dateTransaction;
                        $transactionArray['Transaction']['amount'] = $response['AMT'];
                        $transactionArray['Transaction']['result'] = $response['ACK'];
                        $transactionArray['Transaction']['pass_id'] = $passArray;
                        $transactionArray['Transaction']['transaction_array'] = $arraySerialize;
                        $transactionArray['Transaction']['first_name'] = $this->request->data['Transaction']['first_name_cc'];
                        $transactionArray['Transaction']['last_name'] = $this->request->data['Transaction']['last_name_cc'];	
                        $this->Transaction->create();          	
                        if ($this->Transaction->save($transactionArray, false)) {
							 $transaction = $this->Transaction->getLastInsertId();
							 $this->loadModel('CustomerPass');
                             $cstPass = new CustomerPass;
                             for ($i = 0; $i < count($allPasses); $i++) {
                                $customerPassArray[$i]['user_id'] = $this->Auth->user('id');
                                $customerPassArray[$i]['property_id'] = $this->Session->read('PropertyId');
                                $customerPassArray[$i]['transaction_id'] = $transaction;
                                $customerPassArray[$i]['pass_id'] = $allPasses[$i]['Pass']['id'];
                                $customerPassArray[$i]['pass_cost'] = $allPasses[$i]['Pass']['cost_1st_year'];
                                $customerPassArray[$i]['pass_deposit'] = $allPasses[$i]['Pass']['deposit'];
                                if(isset($allPasses[$i]['Package'])){
										 if ($allPasses[$i]['Package']['is_guest'] == 0) {
													$customerPassArray[$i]['package_id'] = $allPasses[$i]['Package']['id'];
													$customerPassArray[$i]['package_cost'] = $allPasses[$i]['Package']['cost'];
													$customerPassArray[$i]['membership_vaild_upto'] = $allPasses[$i]['Package']['expiration_date'];
										 }elseif($allPasses[$i]['Package']['is_guest']==1){
												$customerPassArray[$i]['is_guest_pass']=1;
										 }
								}
								//RECURRING CHECK START
								if(Configure::read('RECURRING')){
									if($allPasses[$i]['Pass']['is_fixed_duration'] == 0){
										  if(isset($allPasses[$i]['Package'])){
											  if($allPasses[$i]['Package']['is_fixed_duration']==0){
												  if($allPasses[$i]['Package']['is_recurring'] == 1){
													 if($allPasses[$i]['Package']['is_guest'] == 0){
														if($allPasses[$i]['Package']['cost']>0){
															$this->loadModel('RecurringProfile');
															$recurAmount=$allPasses[$i]['Package']['cost'];
															$recurringPayment = array(
																'amount' => $recurAmount,
																'card' => $this->request->data['Transaction']['card_number'], 
																'expiry' => array(
																		'M' => $this->request->data['Transaction']['month'],
																		'Y' => $this->request->data['Transaction']['year'],
																	),
																'cvv' => $this->request->data['Transaction']['cvv'],
																'currency' => 'USD' ,
																'BILLINGPERIOD'=>$allPasses[$i]['Package']['duration_type'],
																'BILLINGFREQUENCY'=>$allPasses[$i]['Package']['duration'],
																'PROFILESTARTDATE'=>$allPasses[$i]['Package']['expiration_date'],
																'FIRSTNAME' => AuthComponent::user('first_name'),
																'LASTNAME' => AuthComponent::user('last_name'), 
																'EMAIL'=>AuthComponent::user('email'),
																'STREET' => AuthComponent::user('address_line_1'),			
																'CITY' => AuthComponent::user('city'),				
																'STATE' => AuthComponent::user('state'),						
																'ZIP' => AuthComponent::user('zip')
															);
															$errorMessage=null;
															
															try {
																$recurringResponse = $this->PayFlow->createRecurringProfilePayFlow($recurringPayment);
															}catch (Exception $e) {
																$errorMessage=$e->getMessage();
															}
															
															if($errorMessage==null){
																if(isset($recurringResponse['RESULT']) && $recurringResponse['RESULT'] == 0) {
																	if($recurringResponse['RESPMSG']=='Approved'){
																		$recurringResponse['PROFILESTATUS']='Active';
																		$array['RecurringProfile']=array(
																									 'recurring_profile_id'=>$recurringResponse['PROFILEID'],
																									 'message'=>'Recurring Profile created',
																									 'status'=>$recurringResponse['PROFILESTATUS']
																		);
																		$this->RecurringProfile->create();
																		if($this->RecurringProfile->save($array)){
																				CakeLog::write('pofileDataSaved','Profile Data Saved for ID: '.$recurringResponse['PROFILEID']);
																		}else{
																				CakeLog::write('pofileDataNotSaved','Profile Data Not Saved for ID: '.$recurringResponse['PROFILEID']);
																		}
																		CakeLog::write('recurringProfileCreated', ''.AuthComponent::user('username').' : Recurring Profile created by User: '.AuthComponent::user('first_name').' '.AuthComponent::user('last_name').
																			' In Property: '.CakeSession::read('PropertyName').', for Pass Name : '.$allPasses[$i]['Pass']['name'].
																			' Profile Id : '.$recurringResponse['PROFILEID'].
																			' Profile Status : '.$recurringResponse['PROFILESTATUS'].''); 
																		$customerPassArray[$i]['recurring_profile_id']=$recurringResponse['PROFILEID'];
																		$customerPassArray[$i]['recurring_profile_status']=$recurringResponse['PROFILESTATUS'];
																	}
																}else{
																	CakeLog::write('recurringProfileCreationFailed', ''.AuthComponent::user('username').' : Recurring Profile creation failed User: '.AuthComponent::user('first_name').' '.AuthComponent::user('last_name').
																		' In Property: '.CakeSession::read('PropertyName').', for Pass Name : '.$allPasses[$i]['Pass']['name'].''); 
																}
															}else{
																	CakeLog::write('recurringProfileCreationException', ''.AuthComponent::user('username').' : Recurring Profile creation failed User: '.AuthComponent::user('first_name').' '.AuthComponent::user('last_name').
																			' In Property: '.CakeSession::read('PropertyName').', for Pass Name : '.$allPasses[$i]['Pass']['name'].' Exception: '.$errorMessage.''); 
															}
														 }
													   }
												
													}
												}
											
											 }
										}
									}
									//RECURRING CHECK END
									
                                $customerPassArray[$i]['pass_valid_upto'] = $allPasses[$i]['Pass']['expiration_date'];
                                
                             }									
                            $cstPass->create();
                            if ($cstPass->saveAll($customerPassArray, array('validate' => false))) {
								    CakeLog::write('selectedPassesPurchased', ''.AuthComponent::user('username').' : Passes Purchased by User: '.AuthComponent::user('first_name').' '.AuthComponent::user('last_name').' In Property: '.CakeSession::read('PropertyName').', Passes Id : '.$passArray.' Passes Name : '.$passName.' Amount Paid: $'.$response['AMT'].'');
									$billAddressArray['BillingAddress']['user_id'] = $this->Auth->user('id');
									$billAddressArray['BillingAddress']['email'] = $this->request->data['Transaction']['email'];
									$billAddressArray['BillingAddress']['first_name'] = $this->request->data['Transaction']['first_name'];
									$billAddressArray['BillingAddress']['last_name'] = $this->request->data['Transaction']['last_name'];
									$billAddressArray['BillingAddress']['address_line_1'] = $this->request->data['Transaction']['address_line_1'];
									$billAddressArray['BillingAddress']['address_line_2'] = $this->request->data['Transaction']['address_line_2'];
									$billAddressArray['BillingAddress']['city'] = $this->request->data['Transaction']['city'];
									$billAddressArray['BillingAddress']['state'] = $this->request->data['Transaction']['state'];
									$billAddressArray['BillingAddress']['zip'] = $this->request->data['Transaction']['zip'];
									$billAddressArray['BillingAddress']['phone'] = $this->request->data['Transaction']['phone'];

									$this->loadModel('BillingAddress');
									$billAddress = new BillingAddress();
									$billAddress->create();
									if ($billAddress->save($billAddressArray, false)) {
										$this->Session->setFlash('Your transaction has been completed succesfully, Add vehicles now', 'success');
										$this->Session->delete('Allowed');
										$this->Session->delete('other_passes');
										$this->Session->delete('guest_pass');
										$this->Session->write('Allowed', true);
										$this->redirect(array('controller'=>'Vehicles','action'=>'add_vehicles'));
									}else {
										$this->Session->setFlash('Billing Address Not Saved', 'error');
										return $this->redirect(array('controller' => 'CustomerPasses', 'action' => 'view'));
									}
						    }else {
                                $this->Session->setFlash('Pass not saved', 'error');
                            }
					    }else {
                            $this->Session->setFlash('The transaction could not be saved. Contact Admin', 'error');
                        }
					}else {
                        $this->Session->setFlash('Transaction Failed', 'error');
                    }
					 
				 }else {
                    $this->Session->setFlash($errorMessage, 'error');
                }
		    }else {
				$totalAmount=$netAmountPayable;
                $this->Session->setFlash('Validation Failure', 'error');
            }
		}
        $this->loadModel('State');
        $state = new State();
        $states = $state->find('list');
        $this->set(compact('totalAmount','returnArray','states'));
	}

    /*     * **********************************************************************
     * Admin Buy remaining pass customer
     */

   	public function admin_buyRemainingPass($id=null,$userId=null,$propertyIdAdmin=null){
        $this->layout='admin_customer';
        $this->view='remainingpass';
		$this->loadModel('Pass');
		$this->loadModel('Package');
        $pass=new Pass();
        if (!$pass->exists($id)) {
            throw new NotFoundException(__('Invalid pass'));
        }
        $dt = new DateTime();
        $currentDateTime= $dt->format('Y-m-d H:i:s');
        $pass->recursive=-1;
		$remainingPass=$pass->find('first',array('conditions'=>array('id'=>$id)));	
        $userDetails= $package=null;
        $this->loadModel('User');
		$this->User->recursive=-1;
     	$userDetails=$this->User->find('first',array('conditions'=>array('id'=>$userId)));
        $totalAmount=0;
		$totalAmount=$totalAmount+($remainingPass['Pass']['deposit']+$remainingPass['Pass']['cost_1st_year']);
		$this->loadModel('Property');
		$this->Property->recursive=-1;
		$propertyNameAdmin=$this->Property->find('first',array('fields'=>array('name'),'conditions'=>array('id'=>$propertyIdAdmin)));
        if($remainingPass['Pass']['package_id']){
			$this->Package->recursive=-1;
			$package=$this->Package->find('first',array('conditions'=>array('id'=>$remainingPass['Pass']['package_id'])));	
			if($package['Package']['is_guest']==0){
				$totalAmount=$totalAmount+$package['Package']['cost'];
			}else{
				$package=NULL;
			}
		}
		if($remainingPass['Pass']['is_fixed_duration']==0){
		  switch ($remainingPass['Pass']['duration_type']) {
                        case "Hour":
                            $date = new DateTime($currentDateTime);
                            $k=(int)$remainingPass['Pass']['duration'];
                            $date->add(new DateInterval('PT'.$k.'H'));
                            $remainingPass['Pass']['expiration_date']=date_format($date, 'Y-m-d H:i:s');
                            break;
                        case "Day":
                            $date = new DateTime($currentDateTime);
                            $k=$remainingPass['Pass']['duration'];
                            $date->add(new DateInterval('P'.$k.'D'));
                            $remainingPass['Pass']['expiration_date']=date_format($date, 'Y-m-d H:i:s');
                            break;
                        case "Week":
                            $date = new DateTime($currentDateTime);
                            $k=(int)$remainingPass['Pass']['duration']*7;
                            $date->add(new DateInterval('P'.$k.'D'));
                            $remainingPass['Pass']['expiration_date']=date_format($date, 'Y-m-d H:i:s');
                            break;
                        case "Month":
                            $date = new DateTime($currentDateTime);
                            $k=(int)$remainingPass['Pass']['duration'];
                            $date->add(new DateInterval('P'.$k.'M'));
                            $remainingPass['Pass']['expiration_date']=date_format($date, 'Y-m-d H:i:s');
                            break;
                        case "Year":
                            $date = new DateTime($currentDateTime);
                            $k=(int)$remainingPass['Pass']['duration'];
                            $date->add(new DateInterval('P'.$k.'Y'));
                           $remainingPass['Pass']['expiration_date']=date_format($date, 'Y-m-d H:i:s');
                            break;
                    }
         }
        $date4 = new DateTime($remainingPass['Pass']['expiration_date']);
		$datePack=$date4->format('Y-m-d');
		$remainingPass['Pass']['expiration_date']= $datePack.' 23:59:59';  
		if ($this->request->is('post')){
			$this->loadModel('CustomerPass');
			if($this->CustomerPass->hasAny(array('user_id'=>$userId,'id'=>$remainingPass['Pass']['id']))){
				$this->Session->write('Allowed',true);
				throw new NotFoundException(__('Not Allowed'));
			}
			 if($package){
				if($package['Package']['is_fixed_duration']==0){
					switch ($package['Package']['duration_type']) {
                        case "Hour":
                            $date = new DateTime($currentDateTime);
                            $k=(int)$package['Package']['duration'];
                            $date->add(new DateInterval('PT'.$k.'H'));
                            $package['Package']['expiration_date']=date_format($date, 'Y-m-d H:i:s');
                            break;
                        case "Day":
                            $date = new DateTime($currentDateTime);
                            $k=$remainingPass['Pass']['duration'];
                            $date->add(new DateInterval('P'.$k.'D'));
                            $package['Package']['expiration_date']=date_format($date, 'Y-m-d H:i:s');
                            break;
                        case "Week":
                            $date = new DateTime($currentDateTime);
                            $k=(int)$package['Package']['duration']*7;
                            $date->add(new DateInterval('P'.$k.'D'));
                            $package['Package']['expiration_date']=date_format($date, 'Y-m-d H:i:s');
                            break;
                        case "Month":
                            $date = new DateTime($currentDateTime);
                            $k=(int)$package['Package']['duration'];
                            $date->add(new DateInterval('P'.$k.'M'));
                            $package['Package']['expiration_date']=date_format($date, 'Y-m-d H:i:s');
                            break;
                        case "Year":
                            $date = new DateTime($currentDateTime);
                            $k=(int)$package['Package']['duration'];
                            $date->add(new DateInterval('P'.$k.'Y'));
                           $package['Package']['expiration_date']=date_format($date, 'Y-m-d H:i:s');
                            break;
                    }
				}
				$date3 = new DateTime($package['Package']['expiration_date']);
				$datePack=$date3->format('Y-m-d');
				$package['Package']['expiration_date']= $datePack.' 23:59:59';
			 }
			$this->Transaction->set($this->request->data);
			if($totalAmount==0){
				$this->Transaction->updateValidations();
			}
            if($this->Transaction->validates()){
				if($totalAmount>0)
				{					
					$payment = array(
                        'amount' => (int)$totalAmount,
                        'card' => $this->request->data['Transaction']['card_number'], // This is a sandbox CC
                        'expiry' => array(
                            'M' => $this->request->data['Transaction']['month'],
                            'Y' => $this->request->data['Transaction']['year'],
                        ),
                        'cvv' => $this->request->data['Transaction']['cvv'],
                        'currency' => 'USD' // Defaults to GBP if not provided
                    );
					$errorMessage=null;
					$response=null;
                    try {
                        $response=$this->Paypal->doDirectPayment($payment);
                        // Check $response[ACK]='Success' OR 'OK'
                    } catch (Exception $e) {
                        $errorMessage=$e->getMessage();
                    }
		
				}else{				
					$response['TIMESTAMP']=date('Y-m-d H:i:s');
					$response['TRANSACTIONID']='No Payment Remaining Pass';
					$response['ACK']='Success';
					$response['AMT']='0';
					$response['PassDetail']= $remainingPass;
					$errorMessage=null;
					$this->request->data['Transaction']['first_name_cc']=$this->Auth->user('first_name');
					$this->request->data['Transaction']['last_name_cc']=$this->Auth->user('last_name');
				}
				if($errorMessage==null){
					if($response['ACK']=='Success'){
						$dateTransaction = date('Y-m-d H:i:s', strtotime ($response['TIMESTAMP']));
						$arraySerialize=serialize($response);
						$transactionArray['Transaction']['paypal_tranc_id']=$response['TRANSACTIONID'];
						$transactionArray['Transaction']['user_id']=$userId;
						$transactionArray['Transaction']['date_time']=$dateTransaction;
						$transactionArray['Transaction']['amount']=$response['AMT'];
						$transactionArray['Transaction']['result']=$response['ACK'];
						$transactionArray['Transaction']['pass_id']=$id;
						$transactionArray['Transaction']['transaction_array']=$arraySerialize;
						$transactionArray['Transaction']['first_name']=$this->request->data['Transaction']['first_name_cc'];
						$transactionArray['Transaction']['last_name']=$this->request->data['Transaction']['last_name_cc'];	
						//$transactionArray['Transaction']['comments'] = "passPurchased";	  
						$this->Transaction->create();
						if ($this->Transaction->save($transactionArray,false)) {
							$customerPassArray['CustomerPass']['user_id']=$userId;
							$customerPassArray['CustomerPass']['property_id']=$propertyIdAdmin;
							$customerPassArray['CustomerPass']['transaction_id']=$this->Transaction->getLastInsertId();
							$customerPassArray['CustomerPass']['pass_id']=$remainingPass['Pass']['id'];
							$customerPassArray['CustomerPass']['pass_valid_upto']=$remainingPass['Pass']['expiration_date'];
							$customerPassArray['CustomerPass']['pass_cost'] = $remainingPass['Pass']['cost_1st_year'];
							$customerPassArray['CustomerPass']['pass_deposit'] = $remainingPass['Pass']['deposit']; 
							if($package){
								$customerPassArray['CustomerPass']['membership_vaild_upto']=$package['Package']['expiration_date'];
								$customerPassArray['CustomerPass']['package_id']=$package['Package']['id'];
								$customerPassArray['CustomerPass']['package_cost'] = $package['Package']['cost'];
							}
							//RECURRING CHECK START
							if(Configure::read('RECURRING')){
								if($remainingPass['Pass']['is_fixed_duration'] == 0){  
									  if(isset($package['Package'])){
										  if($package['Package']['is_fixed_duration']==0){
											  if($package['Package']['is_recurring'] == 1){
												 if($package['Package']['is_guest'] == 0){
													if($package['Package']['cost']>0){
														
														$this->loadModel('RecurringProfile');
														$recurAmount=$package['Package']['cost'];
														$recurringPayment = array(
															'amount' => $recurAmount,
															'card' => $this->request->data['Transaction']['card_number'], 
															'expiry' => array(
																	'M' => $this->request->data['Transaction']['month'],
																	'Y' => $this->request->data['Transaction']['year'],
																),
															'cvv' => $this->request->data['Transaction']['cvv'],
															'currency' => 'USD' ,
															'BILLINGPERIOD'=>$package['Package']['duration_type'],
															'BILLINGFREQUENCY'=>$package['Package']['duration'],
															'PROFILESTARTDATE'=>$package['Package']['expiration_date'],
															'FIRSTNAME' => $userDetails['User']['first_name'],
															'LASTNAME' => $userDetails['User']['last_name'], 
															'EMAIL'=> $userDetails['User']['email'],
															'STREET' => $userDetails['User']['address_line_1'],			
															'CITY' => $userDetails['User']['city'],				
															'STATE' => $userDetails['User']['state'],						
															'ZIP' => $userDetails['User']['zip']
														);
														$errorMessage=null;
														try {
															$recurringResponse = $this->PayFlow->createRecurringProfilePayFlow($recurringPayment);
														}catch (Exception $e) {
															$errorMessage=$e->getMessage();
														}
														if($errorMessage==null){
															//debug($recurringResponse);die;
															if(isset($recurringResponse['RESULT']) && $recurringResponse['RESULT'] == 0) {
																if($recurringResponse['RESPMSG']=='Approved'){
																	$recurringResponse['PROFILESTATUS']='Active';
																	$array['RecurringProfile']=array(
																								 'recurring_profile_id'=>$recurringResponse['PROFILEID'],
																								 'message'=>'Recurring Profile created',
																								 'status'=>$recurringResponse['PROFILESTATUS']
																	);
																	$this->RecurringProfile->create();
																	if($this->RecurringProfile->save($array)){
																			CakeLog::write('pofileDataSaved','Profile Data Saved for ID: '.$recurringResponse['PROFILEID']);
																	}else{
																			CakeLog::write('pofileDataNotSaved','Profile Data Not Saved for ID: '.$recurringResponse['PROFILEID']);
																	}
																	CakeLog::write('recurringProfileCreated', ''. $userDetails['User']['username'].' : Recurring Profile created by User: '.AuthComponent::user('first_name').' '.AuthComponent::user('last_name').
																		' In Property: '.$propertyNameAdmin['Property']['name'].', for Pass Name : '.$remainingPass['Pass']['name'].
																		' Profile Id : '.$recurringResponse['PROFILEID'].
																		' Profile Status : '.$recurringResponse['PROFILESTATUS'].''); 
																	$customerPassArray['CustomerPass']['recurring_profile_id']=$recurringResponse['PROFILEID'];
																	$customerPassArray['CustomerPass']['recurring_profile_status']=$recurringResponse['PROFILESTATUS'];
																}
															}else{
																CakeLog::write('recurringProfileCreationFailed', ''.$userDetails['User']['username'].' : Recurring Profile creation failed User: '.AuthComponent::user('first_name').' '.AuthComponent::user('last_name').
																	' In Property: '.$propertyNameAdmin['Property']['name'].', for Pass Name : '.$remainingPass['Pass']['name'].''); 
															}
														}else{
																CakeLog::write('recurringProfileCreationException', ''.$userDetails['User']['username'].' : Recurring Profile creation failed User: '.AuthComponent::user('first_name').' '.AuthComponent::user('last_name').
																		' In Property: '.$propertyNameAdmin['Property']['name'].', for Pass Name : '.$remainingPass['Pass']['name'].' Exception: '.$errorMessage.''); 
														}
													}
												 }
										
												}
											}
										
										 }
									}
								}
								//RECURRING CHECK END
							$result=$this->CustomerPass->hasAny(array('pass_id'=>$customerPassArray['CustomerPass']['pass_id'],'user_id'=>$userDetails['User']['id']));									
							$newCustomerPass=$customerPassId=NULL;
							if($result==true){
								$customerPassId=$this->CustomerPass->field('id',array(array('pass_id'=>$customerPassArray['CustomerPass']['pass_id'],'user_id'=>$userDetails['User']['id'])));
								$customerPassArray['CustomerPass']['id']=$customerPassId;		
							}	
							$this->CustomerPass->create();
							if($this->CustomerPass->save($customerPassArray,false)){
								if($customerPassId){
									$newCustomerPass=$customerPassId;
								}else{
									$newCustomerPass=$this->CustomerPass->getLastInsertId();
								}
								$this->loadModel('Pass');
								$passName=$this->Pass->givePassName($remainingPass['Pass']['id']);
								CakeLog::write('nonCompulsoryPassPurchasedAdmin', ''.$userDetails['User']['username'].' : Non compulsory pass/remaining pass purchased with Pass ID: '.$remainingPass['Pass']['id'].' and Pass Name: '.$passName.', purchased by User: '.AuthComponent::user('first_name').' '.AuthComponent::user('last_name').', in Property: '.$propertyNameAdmin['Property']['name'].' Amount Paid: $'.$response['AMT'].'');
								$billAddressArray['BillingAddress']['user_id']=$userDetails['User']['id'];
								$billAddressArray['BillingAddress']['email']=$this->request->data['Transaction']['email'];
								$billAddressArray['BillingAddress']['first_name']=$this->request->data['Transaction']['first_name'];
								$billAddressArray['BillingAddress']['last_name']=$this->request->data['Transaction']['last_name'];
								$billAddressArray['BillingAddress']['address_line_1']=$this->request->data['Transaction']['address_line_1'];
								$billAddressArray['BillingAddress']['address_line_2']=$this->request->data['Transaction']['address_line_2'];
								$billAddressArray['BillingAddress']['city']=$this->request->data['Transaction']['city'];
								$billAddressArray['BillingAddress']['state']=$this->request->data['Transaction']['state'];
								$billAddressArray['BillingAddress']['zip']=$this->request->data['Transaction']['zip'];
								$billAddressArray['BillingAddress']['phone']=$this->request->data['Transaction']['phone'];
								$this->loadModel('BillingAddress');
								$billAddress=new BillingAddress();
								$billAddress->create(); 
								$rslt=$billAddress->hasAny(array('user_id'=>$userDetails['User']['id']));
								if($rslt==true){
									$billAddressId=$billAddress->find('first',array('fields'=>array('id'),'conditions'=>array('user_id'=>$userDetails['User']['id'])));
									$billAddressArray['BillingAddress']['id']=$billAddressId['BillingAddress']['id'];
								}
								if($billAddress->save($billAddressArray,false)){																
									$this->Session->setFlash('Passes Saved Successfully','success');
									return $this->redirect(array('controller'=>'CustomerPasses','action' => 'customer_view',$userId));
								}else{
									$this->Session->setFlash(__('Billing Address Not Saved'));	
									return $this->redirect(array('controller'=>'CustomerPasses','action' => 'customer_view',$userId));
								}				
							}else {
								$this->Session->setFlash('Pass not saved','error');	
								$this->redirect(array('action'=>'buyRemainingPass'));
							}
						}else {
							$this->Session->setFlash('The transaction could not be saved. Contact Admin','error');
						}	
					}else{
						$this->Session->setFlash('Transaction Failed','error');
					}	
				}else{
				  $this->Session->setFlash($errorMessage,'error');
				}
			}else{
				$this->Session->setFlash('Validation Failed','error');
			}
			
		}				
		$this->loadModel('State');
		$state=new State();
		$states=$state->find('list');	
		$this->loadModel('BillingAddress');
        $billAdd = new BillingAddress();
        $billAdd->recursive=-1;
        $billingAddress= $billAdd->find('first',array('conditions'=>array('user_id'=>$userId)));
        if(empty($billingAddress)){
			$billingAddress['BillingAddress']=$userDetails['User'];
		}	
		$this->set(compact('states','remainingPass','totalAmount','billingAddress','package','propertyNameAdmin'));
		
       
	}

    /*     * ******************************************
     * Admin Activate Pass Or Buy Corresponding Permit
     */

    public function admin_activatePass($customerPassId=null,$passId=null,$permitId=null,$userId=null,$propertyIdAdmin=null)
	{
		$this->loadModel('Property');
		$this->Property->recursive=-1;
		$propertyName=$this->Property->find('first',array('fields'=>array('name'),'conditions'=>array('id'=>$propertyIdAdmin)));
		$this->loadModel('User');
		$this->User->recursive=-1;
     	$userDetails=$this->User->find('first',array('conditions'=>array('id'=>$userId)));
     	
		$this->layout='admin_customer';
		$this->view="activate_pass";
		$this->loadModel('CustomerPass');
		if (!$this->CustomerPass->exists($customerPassId)) {
            throw new NotFoundException(__('Invalid Customer Pass'));
		}else{
			$userID=$this->CustomerPass->field('user_id',array('id'=>$customerPassId));
			if($userID != $userId){
				 throw new NotFoundException(__('Invalid DATA'));
			}
		}
		$this->loadModel('Pass');
		if (!$this->Pass->exists($passId)) {
            throw new NotFoundException(__('Invalid Pass'));
		}
		$this->loadModel('Package');
		if (!$this->Package->exists($permitId)) {
            throw new NotFoundException(__('Invalid Package'));
		}
		 $packageDate=$this->CustomerPass->field('membership_vaild_upto',array('id'=>$customerPassId));
        if(!is_null($packageDate)){
			$dt = new DateTime();
			$currentDateTime= $dt->format('Y-m-d H:i:s');
			if($packageDate>$currentDateTime){
				$this->Session->setFlash('This pass is already active','warning');
				$this->redirect(array('controller'=>'CustomerPasses','action'=>'customer_view',$userId));
			}
		}
		$this->CustomerPass->recursive=-1;
		$customerDetails=$this->CustomerPass->find('first',array('conditions'=>array('id'=>$customerPassId)));
	
		$this->Package->recursive=-1;
		$packageDetails=$this->Package->find('first',array('conditions'=>array('id'=>$permitId)));
		
		
		
		if($packageDetails['Package']['is_fixed_duration']==0){
					$dt = new DateTime();
					$currentDateTime= $dt->format('Y-m-d H:i:s');
                    switch ($packageDetails['Package']['duration_type']) 
                    {
                        case "Hour":
                            $date = new DateTime($currentDateTime);
                            $k=(int)$packageDetails['Package']['duration'];
                            $date->add(new DateInterval('PT'.$k.'H'));
                            $packageDetails['Package']['expiration_date']=date_format($date, 'Y-m-d H:i:s');
                            break;
                        case "Day":
                            $date = new DateTime($currentDateTime);
                            $k=$packageDetails['Package']['duration'];
                            $date->add(new DateInterval('P'.$k.'D'));
                            $packageDetails['Package']['expiration_date']=date_format($date, 'Y-m-d H:i:s');
                            break;
                        case "Week":
                            $date = new DateTime($currentDateTime);
                            $k=(int)$packageDetails['Package']['duration']*7;
                            $date->add(new DateInterval('P'.$k.'D'));
							$packageDetails['Package']['expiration_date']=date_format($date, 'Y-m-d H:i:s');
                            break;
                        case "Month": 
                            $date = new DateTime($currentDateTime);
                            $k=(int)$packageDetails['Package']['duration'];
                            $date->add(new DateInterval('P'.$k.'M'));
                            $packageDetails['Package']['expiration_date']=date_format($date, 'Y-m-d H:i:s');
                            break;
                        case "Year":
                            $date = new DateTime($currentDateTime);
                            $k=(int)$packageDetails['Package']['duration'];
                            $date->add(new DateInterval('P'.$k.'Y'));
                            $packageDetails['Package']['expiration_date']=date_format($date, 'Y-m-d H:i:s');
                            break;
                    }
		}
		$date4 = new DateTime($packageDetails['Package']['expiration_date']);
		$datePack=$date4->format('Y-m-d');
		$packageDetails['Package']['expiration_date']= $datePack.' 23:59:59';     
		if ($this->request->is('post'))
        {
			$this->request->data['Transaction']['user_id']=$userId;      
            $this->Transaction->set($this->request->data);
            if($this->Transaction->validates())
            {                
				$payment = array(
							'amount' =>  $packageDetails['Package']['cost'],
								'card' => $this->request->data['Transaction']['card_number'], // This is a sandbox CC
								'expiry' => array(
									'M' => $this->request->data['Transaction']['month'],
									'Y' => $this->request->data['Transaction']['year'],
									),
								'cvv' => $this->request->data['Transaction']['cvv'],
								'currency' => 'USD' // Defaults to GBP if not provided
							);
				$errorMessage=null;
				$response=null;
				try {
						$response=$this->Paypal->doDirectPayment($payment);
						// Check $response[ACK]='Success' OR 'OK'
				} catch (Exception $e) {
						$errorMessage=$e->getMessage();
				}
				if($errorMessage==null){
						if($response['ACK']=='Success'){				
								$this->Transaction->create();
								$dateTransaction = date('Y-m-d H:i:s', strtotime ($response['TIMESTAMP']));
								$arraySerialize=serialize($response);
								$transactionArray['Transaction']['paypal_tranc_id']=$response['TRANSACTIONID'];
								$transactionArray['Transaction']['user_id']=$this->request->data['Transaction']['user_id'];
								$transactionArray['Transaction']['date_time']=$dateTransaction;
								$transactionArray['Transaction']['amount']=$response['AMT'];
								$transactionArray['Transaction']['result']=$response['ACK'];
								$transactionArray['Transaction']['pass_id']=$passId;
								$transactionArray['Transaction']['transaction_array']=$arraySerialize;
								$transactionArray['Transaction']['first_name']=$this->request->data['Transaction']['first_name_cc'];
								$transactionArray['Transaction']['last_name']=$this->request->data['Transaction']['last_name_cc'];
								$transactionArray['Transaction']['comments'] = "passActivated";	
								if ($this->Transaction->save($transactionArray,false)) {	
										$customerPassArray['CustomerPass']['transaction_id']=$this->Transaction->getLastInsertId();
										$customerPassArray['CustomerPass']['membership_vaild_upto']=$packageDetails['Package']['expiration_date'];
										$customerPassArray['CustomerPass']['package_id']=$permitId;
										$customerPassArray['CustomerPass']['id']=$customerPassId;
										$previousId=$this->CustomerPass->field('vehicle_id',array('id'=>$customerPassId));
										$recurring_profile_status=$recurring_profile_id=$package_id=$packageExpiryDate=NULL;
											//RECURRING CHECK START
											if(Configure::read('RECURRING')){
												 if($packageDetails['Package']['is_fixed_duration']==0){
													 if($packageDetails['Package']['is_recurring'] == 1){
														if($packageDetails['Package']['is_guest'] == 0 && $packageDetails['Package']['cost']>0){
															$this->loadModel('RecurringProfile');
															$recurAmount=$packageDetails['Package']['cost'];
															$recurringPayment = array();
															$previousProfile=$this->CustomerPass->field('recurring_profile_id',array('id'=>$customerPassId));
															$this->loadModel('User');
															$this->User->recursive=-1;
															$userDetails=$this->User->find('first',array('conditions'=>array('id'=>$userId)));
															if($previousProfile){
																$recurringPayment = array(
																				'amount' => $recurAmount,
																				'currency' => 'USD' ,
																				'BILLINGPERIOD'=>$packageDetails['Package']['duration_type'],
																				'BILLINGFREQUENCY'=>$packageDetails['Package']['duration'],
																				'PROFILESTARTDATE'=>$packageDetails['Package']['expiration_date'],
																				'EMAIL'=>$userDetails['User']['email'],
																				'STREET' => $userDetails['User']['address_line_1'],						
																				'ZIP' => $userDetails['User']['zip'],
																				'PROFILEID'=>$previousProfile,
																				'expiry' => array(
																					'M' => $this->request->data['Transaction']['month'],
																					'Y' => $this->request->data['Transaction']['year'],
																				)
																			);
															}else{
																$recurringPayment = array(
																				'amount' => $recurAmount,
																				'card' => $this->request->data['Transaction']['card_number'], 
																				'expiry' => array(
																							'M' => $this->request->data['Transaction']['month'],
																							'Y' => $this->request->data['Transaction']['year'],
																						),
																				'cvv' => $this->request->data['Transaction']['cvv'],
																				'currency' => 'USD' ,
																				'BILLINGPERIOD'=>$packageDetails['Package']['duration_type'],
																				'BILLINGFREQUENCY'=>$packageDetails['Package']['duration'],
																				'PROFILESTARTDATE'=>$packageDetails['Package']['expiration_date'],
																				'FIRSTNAME' => $userDetails['User']['first_name'],
																				'LASTNAME' => $userDetails['User']['last_name'], 
																				'EMAIL'=> $userDetails['User']['email'],
																				'STREET' => $userDetails['User']['address_line_1'],			
																				'CITY' => $userDetails['User']['city'],				
																				'STATE' => $userDetails['User']['state'],						
																				'ZIP' => $userDetails['User']['zip']
																			);
															}
														//	debug($recurringPayment);die;
															$errorMessage=null;
															try {
																if($previousProfile){
																	$recurringResponse = $this->PayFlow->modifyRecurringProfilePayFlow($recurringPayment);
																}else{
																	$recurringResponse = $this->PayFlow->createRecurringProfilePayFlow($recurringPayment);
																}
															}catch (Exception $e) {
																$errorMessage=$e->getMessage();
															}
															if($errorMessage==null){
															//	debug($recurringResponse);
																if(isset($recurringResponse['RESULT']) && $recurringResponse['RESULT'] == 0) {
																	if($recurringResponse['RESPMSG']=='Approved'){
																		$recurringResponse['PROFILESTATUS']='Active';
																		$array['RecurringProfile']=array(
																						 'recurring_profile_id'=>$recurringResponse['PROFILEID'],
																						 'message'=>'Recurring Profile created',
																						 'status'=>$recurringResponse['PROFILESTATUS'],
																						 'customer_pass_id'=>$customerPassId
																		);
																		if($previousProfile){
																			$idRecurring=$this->RecurringProfile->field('id',array('recurring_profile_id'=>$previousProfile));
																			$array['RecurringProfile']['id']=$idRecurring;
																		}else{
																			$this->RecurringProfile->create();
																		}
																		if($this->RecurringProfile->save($array)){
																			if($previousProfile){
																				CakeLog::write('profileDataModifiedAdmin','Profile Data Modified for ID: '.$recurringResponse['PROFILEID']);
																			}else{
																				CakeLog::write('profileDataSavedAdmin','Profile Data Saved for ID: '.$recurringResponse['PROFILEID']);
																			}
																		}else{
																			if($previousProfile){
																				CakeLog::write('profileDataNotModifiedAdmin','Profile Data Not Modified for ID: '.$recurringResponse['PROFILEID']);
																			}else{	
																				CakeLog::write('profileDataNotSavedAdmin','Profile Data Not Saved for ID: '.$recurringResponse['PROFILEID']);
																			}
																		}
																		if($previousProfile){
																			CakeLog::write('recurringProfileModifiedAdmin', ''.$userDetails['User']['username'].' : Recurring Profile created by User: '.AuthComponent::user('first_name').' '.AuthComponent::user('last_name').
																			' In Property: '.$propertyName['Property']['name'].', for Package ID : '.$packageDetails['Package']['id'].
																			' Profile Id : '.$recurringResponse['PROFILEID'].
																			' Profile Status : '.$recurringResponse['PROFILESTATUS'].''); 
																			$recurring_profile_id=$recurringResponse['PROFILEID'];
																			$recurring_profile_status=$recurringResponse['PROFILESTATUS'];

																		}else{
																			CakeLog::write('recurringProfileCreated', ''.$userDetails['User']['username'].' : Recurring Profile created by User: '.AuthComponent::user('first_name').' '.AuthComponent::user('last_name').
																			' In Property: '.$propertyName['Property']['name'].', for Package ID : '.$packageDetails['Package']['id'].
																			' Profile Id : '.$recurringResponse['PROFILEID'].
																			' Profile Status : '.$recurringResponse['PROFILESTATUS'].''); 
																			$recurring_profile_id=$recurringResponse['PROFILEID'];
																			$recurring_profile_status=$recurringResponse['PROFILESTATUS'];
																		}
																	}
																}else{
																	if($previousProfile){
																		  CakeLog::write('recurringProfileModificationFailedAdmin1', ''.$userDetails['User']['username'].' : Recurring Profile creation failed User: '.AuthComponent::user('first_name').' '.AuthComponent::user('last_name').
																		' In Property: '.$propertyName['Property']['name'].', for Package ID : '.$packageDetails['Package']['id'].''); 
																	
																	}else{
																		CakeLog::write('recurringProfileCreationFailedAdmin1', ''.$userDetails['User']['username'].' : Recurring Profile creation failed User: '.AuthComponent::user('first_name').' '.AuthComponent::user('last_name').
																		' In Property: '.$propertyName['Property']['name'].', for Package ID : '.$packageDetails['Package']['id'].''); 
																	}
																}
															}else{
																if($previousProfile){
																		  CakeLog::write('recurringProfileModificationFailedAdmin', ''.$userDetails['User']['username'].' : Recurring Profile creation failed User: '.AuthComponent::user('first_name').' '.AuthComponent::user('last_name').
																		' In Property: '.$propertyName['Property']['name'].', for Package ID : '.$packageDetails['Package']['id'].''); 
																	
																}else{
																	CakeLog::write('recurringProfileCreationExceptionAdmin', ''.$userDetails['User']['username'].' : Recurring Profile creation failed User: '.AuthComponent::user('first_name').' '.AuthComponent::user('last_name').
																	' In Property: '.$propertyName['Property']['name'].', for Package ID : '.$packageDetails['Package']['id'].' Exception: '.$errorMessage.''); 
																}
															}
														}
													}
												}
											}
											//RECURRING CHECK END
											if($recurring_profile_id){
												$customerPassArray['CustomerPass']['recurring_profile_id']=$recurring_profile_id;
											}
											if($recurring_profile_status){
												$customerPassArray['CustomerPass']['recurring_profile_status']=$recurring_profile_status;
											}
										if($this->CustomerPass->save($customerPassArray,false)){
												
													$this->loadModel('Package');
													$packageName=$this->Package->givePackageName($permitId);
													CakeLog::write('validPassActivatedAdmin', ''.AuthComponent::user('username').' : Valid Pass Activated Customer Pass ID: <a href="/admin/CustomerPasses/edit/'.$customerPassId.'">'.$customerPassId.'</a> Package ID: '.$permitId.', Package Name: '.$packageName.' Activated by User: '.AuthComponent::user('first_name').' '.AuthComponent::user('last_name').' In Property: '.$propertyName['Property']['name'].', Cost Of Activation: '.$response['AMT'].'');
													$billAddressArray['BillingAddress']['user_id']=$this->request->data['Transaction']['user_id'];
													$billAddressArray['BillingAddress']['email']=$this->request->data['Transaction']['email'];
													$billAddressArray['BillingAddress']['first_name']=$this->request->data['Transaction']['first_name'];
													$billAddressArray['BillingAddress']['last_name']=$this->request->data['Transaction']['last_name'];
													$billAddressArray['BillingAddress']['address_line_1']=$this->request->data['Transaction']['address_line_1'];
													$billAddressArray['BillingAddress']['address_line_2']=$this->request->data['Transaction']['address_line_2'];
													$billAddressArray['BillingAddress']['city']=$this->request->data['Transaction']['city'];
													$billAddressArray['BillingAddress']['state']=$this->request->data['Transaction']['state'];
													$billAddressArray['BillingAddress']['zip']=$this->request->data['Transaction']['zip'];
													$billAddressArray['BillingAddress']['phone']=$this->request->data['Transaction']['phone'];
													
													$this->loadModel('BillingAddress');
													$billAddress=new BillingAddress();
													$rslt=$billAddress->hasAny(array('user_id'=>$this->request->data['Transaction']['user_id']));
													if($rslt==true){
														$billAddressId=$billAddress->find('first',array('fields'=>array('id'),'conditions'=>array('user_id'=>$this->request->data['Transaction']['user_id'])));
														$billAddressArray['BillingAddress']['id']=$billAddressId['BillingAddress']['id'];
													}
													$billAddress->create();
													if($billAddress->save($billAddressArray,false)){
														$this->CustomerPass->recursive=-1;
														$newCustomerPass=$this->CustomerPass->find('first',array('fields'=>array('id'),'conditions'=>array('user_id'=>$userId,'property_id'=>$propertyIdAdmin,'pass_id'=>$passId)));
														$this->Session->setFlash('Your transaction has been completed succesfully','success');
														return $this->redirect(array('controller'=>'vehicles','action' => 'add',$newCustomerPass['CustomerPass']['id'],$userId,$propertyIdAdmin));
													}else{
															$this->Session->setFlash('Billing Address Not Saved','error');								
													}
										}
										else {
											$this->Session->setFlash('Pass not saved','error');										
										}									
								} else {
										$this->Session->setFlash('The transaction could not be saved. Contact Admin','error');									
								}
						}
						else{
							$this->Session->setFlash('Transaction Failed','error');		
							}
					}
					else{
						$this->Session->setFlash($errorMessage,'error');						
					} 
            }
        }
        $this->Pass->recursive=-1;
		$pass=$this->Pass->find('first',array('conditions'=>array('id'=>$passId)));
		$this->loadModel('BillingAddress');
        $billAdd = new BillingAddress();
        $billAdd->recursive=-1;
        $billingAddress= $billAdd->find('first',array('conditions'=>array('user_id'=>$userId)));
        $this->loadModel('State');
        $state=new State();
        $states=$state->find('list');
        $this->set(compact('states','packageDetails','customerDetails','billingAddress','userDetails','propertyName','propertyIdAdmin'));
	
	}

    /*     * *********************************************************************
     * Admin Guest Check Out
     */

   	public function admin_guestCheckOut($customerPassId=null,$passId=null,$permitId=null,$userId,$propertyIdAdmin)
	{
	   
		$this->loadModel('Property');
		$this->Property->recursive=-1;
		$propertyName=$this->Property->find('first',array('fields'=>array('name'),'conditions'=>array('id'=>$propertyIdAdmin)));
		$this->loadModel('User');
		$this->User->recursive=-1;
     	$userDetails=$this->User->find('first',array('conditions'=>array('id'=>$userId)));
     	
		$this->loadModel('CustomerPass'); 
		if (!$this->CustomerPass->exists($customerPassId)) {
            throw new NotFoundException(__('Invalid Customer Pass'));
        }
		 $this->loadModel('Package');
		if (!$this->Package->exists($permitId)) {
            throw new NotFoundException(__('Invalid package'));
        }
        $this->loadModel('Pass');
        if (!$this->Pass->exists($passId)) {
            throw new NotFoundException(__('Invalid pass'));
        }
        $this->layout="admin_customer";
        $this->view="guest_check_out";
        $this->loadModel('Property');
        $freeGuestCredits=$this->Property->field('free_guest_credits',array('id'=>$propertyIdAdmin)); 
        $permitCost=$this->Package->field('cost',array('id'=>$permitId));
      
        $previousUsedCredits=$this->User->field('guest_credits',array('id'=>$userId));
		$toDate=$this->Session->read('toDate');
		$toDate = date("Y-m-d", strtotime($toDate));
		
		$dt = new DateTime();
		$currentDateTime= $dt->format('Y-m-d');
		$datetime1 = date_create($toDate);
		$datetime2 = date_create($currentDateTime);
		$interval = date_diff($datetime1, $datetime2);
		
		$currentCreditUsed=$interval->days;
		
		$newCredits=0;
		
		$amount=0;
		
		if((int)$previousUsedCredits>=$interval->days){
			$newCredits=$previousUsedCredits-$interval->days;
			$amount=0;
		}else{
			$newCredits=0;
			$creditPayable=$interval->days-$previousUsedCredits;
			
			$amount=$creditPayable*$permitCost;
		}
		
		if ($this->request->is('post')) {
				 $this->Transaction->set($this->request->data);
				  if($this->Transaction->validates()){
						$date = new DateTime();
						$date->add(new DateInterval('P'.$currentCreditUsed.'D'));
						$permitExpiryDate=date_format($date, 'Y-m-d H:i:s');
						
						
						$payment = array(
                        'amount' => (int)$amount,
                        'card' => $this->request->data['Transaction']['card_number'], // This is a sandbox CC
                        'expiry' => array(
                            'M' => $this->request->data['Transaction']['month'],
                            'Y' => $this->request->data['Transaction']['year'],
                        ),
                        'cvv' => $this->request->data['Transaction']['cvv'],
                        'currency' => 'USD' // Defaults to GBP if not provided
						);
                    
						$errorMessage=null;
						$response=null;
						try {
							$response=$this->Paypal->doDirectPayment($payment);
							// Check $response[ACK]='Success' OR 'OK'
						} catch (Exception $e) {
							$errorMessage=$e->getMessage();
						}
						if($errorMessage==null){
							if($response['ACK']=='Success'){
								$dateTransaction = date('Y-m-d H:i:s', strtotime ($response['TIMESTAMP']));
								$arraySerialize=serialize($response);
								$transactionArray['Transaction']['paypal_tranc_id']=$response['TRANSACTIONID'];
								$transactionArray['Transaction']['user_id']=$userId;
								$transactionArray['Transaction']['date_time']=$dateTransaction;
								$transactionArray['Transaction']['amount']=$response['AMT'];
								$transactionArray['Transaction']['result']=$response['ACK'];
								$transactionArray['Transaction']['pass_id']=$passId;
								$transactionArray['Transaction']['transaction_array']=$arraySerialize;
								$transactionArray['Transaction']['first_name']=$this->request->data['Transaction']['first_name_cc'];
								$transactionArray['Transaction']['last_name']=$this->request->data['Transaction']['last_name_cc'];
								$transactionArray['Transaction']['comments'] = "guestPassActivated";
								$this->Transaction->create();
								if ($this->Transaction->save($transactionArray,false)) {
											$this->loadModel('CustomerPass');
											$customerPassArray['CustomerPass']['transaction_id']=$this->Transaction->getLastInsertId();
											$customerPassArray['CustomerPass']['membership_vaild_upto']= $permitExpiryDate;
											$customerPassArray['CustomerPass']['id']=$customerPassId;
											$customerPassArray['CustomerPass']['package_id']= $permitId;
											$customerPassArray['CustomerPass']['is_guest_pass']=1;
											$customerPassArray['CustomerPass']['vehicle_id']=$this->Session->read('vehicleID');
											$this->CustomerPass->create();
											if($this->CustomerPass->save($customerPassArray,false)){
														
														$this->loadModel('Vehicle');
														$vehicle['Vehicle']['customer_pass_id']=$customerPassId;
														$vehicle['Vehicle']['id']=$this->Session->read('vehicleID');
														if($this->Vehicle->save($vehicle,false))
														{
															    $this->loadModel('UserGuestPass'); 
																$vehicleInfo=$this->Vehicle->field('vehicle_info',array('id'=>$this->Session->read('vehicleID')));
																$userPassArray=array('UserGuestPass'=>array('user_id'=>AuthComponent::user('id'),
																											'customer_pass_id'=>$customerPassId,
																											'vehicle_id'=>$this->Session->read('vehicleID'),
																											'vehicle_details'=>$vehicleInfo,
																											'days'=>$currentCreditUsed,
																											'free' => $currentCreditUsed-$creditPayable, 
																											'paid' => $creditPayable,
																											'amount'=>$amount,
																											'to_date'=>$permitExpiryDate,
																											'property_id'=>$propertyIdAdmin
																));
																$this->UserGuestPass->save($userPassArray,false); 
															
															$billAddressArray['BillingAddress']['email']=$this->request->data['Transaction']['email'];
															$billAddressArray['BillingAddress']['first_name']=$this->request->data['Transaction']['first_name'];
															$billAddressArray['BillingAddress']['last_name']=$this->request->data['Transaction']['last_name'];
															$billAddressArray['BillingAddress']['address_line_1']=$this->request->data['Transaction']['address_line_1'];
															$billAddressArray['BillingAddress']['address_line_2']=$this->request->data['Transaction']['address_line_2'];
															$billAddressArray['BillingAddress']['city']=$this->request->data['Transaction']['city'];
															$billAddressArray['BillingAddress']['state']=$this->request->data['Transaction']['state'];
															$billAddressArray['BillingAddress']['zip']=$this->request->data['Transaction']['zip'];
															$billAddressArray['BillingAddress']['phone']=$this->request->data['Transaction']['phone'];
															$this->loadModel('BillingAddress');
															$billAddress=new BillingAddress();
															$billAddress->create();
															$rslt=$billAddress->hasAny(array('user_id'=>$this->Auth->user('id')));
															if($rslt==true){
																$billAddressId=$billAddress->find('first',array('fields'=>array('id'),'conditions'=>array('user_id'=>$userId)));
																$billAddressArray['BillingAddress']['id']=$billAddressId['BillingAddress']['id'];
															}
															if($billAddress->save($billAddressArray,false)){
																$this->loadModel('User');
																$user['User']['id']=$userId;
																$user['User']['guest_credits']=$newCredits;
																//$this->User->create();
																if($this->User->save($user,false)){
																	$this->Session->setFlash('Your transaction has been completed succesfully','sucess');
																	$this->Session->delete('vehicleID');
																	return $this->redirect(array('controller'=>'CustomerPasses','action' => 'customer_view',$userId));
																}else{
																		$this->Session->setFlash(__('User Table not updated','error'));															
																}
															}else{
														$this->Session->setFlash('Billing Address Not Saved','error');	
														}												
													}else {
														$this->Session->setFlash('Vehicle details not updated','error');
													}
										    }else {
											$this->Session->setFlash('Pass not saved','error');												
										}								
								}else {
									$this->Session->setFlash('The transaction could not be saved. Contact Admin','error');
								}						    
					    }else{
							$this->Session->setFlash('Transaction Failed','error');
						}
				    }else{
						$this->Session->setFlash($errorMessage,'error');
					}
				  }
		}
		$this->layout='customer';
		$this->loadModel('BillingAddress');
		$this->BillingAddress->recursive=-1;
        $billingAddress=$this->BillingAddress->find('first',array('conditions'=>array('user_id'=>$userId)));
        $this->loadModel('State');
        $state=new State();
        $states=$state->find('list');
        $this->set(compact('freeGuestCredits','previousUsedCredits','billingAddress','states','permitCost','newCredits','amount','currentCreditUsed',
        'creditPayable'));
	
  }

 /*************************************************************
 * Pass Renewal Admin
 */
   public function admin_passRenew($customerPassId=null,$passId=null,$userId=null,$propertyIdAdmin=null,$advance=false)
 {
	$this->loadModel('Property');
		$this->Property->recursive=-1;
		$propertyName=$this->Property->find('first',array('fields'=>array('name'),'conditions'=>array('id'=>$propertyIdAdmin)));
		$this->loadModel('User');
		$this->User->recursive=-1;
     	$userDetails=$this->User->find('first',array('conditions'=>array('id'=>$userId)));
	$this->loadModel('CustomerPass');
	$renewAllowed=true;
	if (!$this->CustomerPass->exists($customerPassId)) {
            throw new NotFoundException(__('Invalid Customer Pass'));
	}
	$this->loadModel('Pass');
    if (!$this->Pass->exists($passId)) {
            $this->Session->setFlash('Sorry, this pass cannot be renewed as this pass has been removed from this property','warning');	
            $this->redirect(array('controller'=>'CustomerPasses','action'=>'view'));
    }else{
		if($advance==false){
			$packageId=$this->Pass->field('package_id',array('id'=>$passId));
			if(!is_null($packageId)){
				$this->loadModel('Package');
				if(!$this->Package->exists($packageId)){
					$this->Session->setFlash('Sorry, this pass cannot be renewed as this pass has been removed from this property','warning');	
					$renewAllowed=false;
				}
			}
		}
	}
	$this->layout='admin_customer'; 
	$this->view='pass_renew';
	$this->loadModel('Pass');
	$this->Pass->recursive=-1;
	$passDetails=$this->Pass->find('first',array('conditions'=>array('id'=>$passId)));
	$advance=false;
	$pass_expiry = $this->CustomerPass->field('pass_valid_upto', array('id' => $customerPassId));
	if($passDetails['Pass']['is_fixed_duration']==0){
					$pass_expiry = $this->CustomerPass->field('pass_valid_upto', array('id' => $customerPassId));
					$dt = new DateTime();
					$currentDateTime = $dt->format('Y-m-d H:i:s');
					if($pass_expiry>$currentDateTime){
						$currentDateTime=$pass_expiry; 
						$advance=true;
					}
                    switch ($passDetails['Pass']['duration_type']) 
                    {
                        case "Hour":
                            $date = new DateTime($currentDateTime);
                            $k=(int)$passDetails['Pass']['duration'];
                            $date->add(new DateInterval('PT'.$k.'H'));
                            $passDetails['Pass']['expiration_date']=date_format($date, 'Y-m-d H:i:s');
                            break;
                        case "Day":
                            $date = new DateTime($currentDateTime);
                            $k=$passDetails['Pass']['duration'];
                            $date->add(new DateInterval('P'.$k.'D'));
                            $passDetails['Pass']['expiration_date']=date_format($date, 'Y-m-d H:i:s');
                            break;
                        case "Week":
                            $date = new DateTime($currentDateTime);
                            $k=(int)$passDetails['Pass']['duration']*7;
                            $date->add(new DateInterval('P'.$k.'D'));
                            $passDetails['Pass']['expiration_date']=date_format($date, 'Y-m-d H:i:s');
                            break;
                        case "Month":
                            $date = new DateTime($currentDateTime);
                            $k=(int)$passDetails['Pass']['duration'];
                            $date->add(new DateInterval('P'.$k.'M'));
                            $passDetails['Pass']['expiration_date']=date_format($date, 'Y-m-d H:i:s');
                            break;
                        case "Year":
                            $date = new DateTime($currentDateTime);
                            $k=(int)$passDetails['Pass']['duration'];
                            $date->add(new DateInterval('P'.$k.'Y'));
                            $passDetails['Pass']['expiration_date']=date_format($date, 'Y-m-d H:i:s');
                            break;
                    }
     }
    $packageDetails=array();
     if($passDetails['Pass']['package_id']){
		$this->loadModel('Package');
		$this->Package->recursive=-1;
		$packageDetails=$this->Package->find('first',array('conditions'=>array('id'=>$passDetails['Pass']['package_id'])));
	 }
	 $passRenewalcost=$passDetails['Pass']['cost_after_1st_year'];
	 if($packageDetails){
		 if($packageDetails['Package']['is_guest']==0){
			$passRenewalcost=$passRenewalcost+$packageDetails['Package']['cost'];
		}
	 }
     $date3 = new DateTime($passDetails['Pass']['expiration_date']);
	 $datePack=$date3->format('Y-m-d');
     $passDetails['Pass']['expiration_date']= $datePack.' 23:59:59';
     $passName=$passDetails['Pass']['name'];
     $passExpiryDate=date("m/d/Y H:i:s", strtotime($passDetails['Pass']['expiration_date']));
   
	if ($this->request->is('post')) {
		$this->Transaction->set($this->request->data);
		if($this->Transaction->validates()){
					$payment = array(
                        'amount' => (int)$passRenewalcost,
                        'card' => $this->request->data['Transaction']['card_number'], // This is a sandbox CC
                        'expiry' => array(
                            'M' => $this->request->data['Transaction']['month'],
                            'Y' => $this->request->data['Transaction']['year'],
                        ),
                        'cvv' => $this->request->data['Transaction']['cvv'],
                        'currency' => 'USD' // Defaults to GBP if not provided
                    );
					$errorMessage=null;
					$response=null;
                    try {
                        $response=$this->Paypal->doDirectPayment($payment);
                        // Check $response[ACK]='Success' OR 'OK'
                    } catch (Exception $e) {
                        $errorMessage=$e->getMessage();
                    }
					if($errorMessage==null){
						if($response['ACK']=='Success'){
								
								$dateTransaction = date('Y-m-d H:i:s', strtotime ($response['TIMESTAMP']));
								$arraySerialize=serialize($response);
								$transactionArray['Transaction']['paypal_tranc_id']=$response['TRANSACTIONID'];
								$transactionArray['Transaction']['user_id']=$userId;
								$transactionArray['Transaction']['date_time']=$dateTransaction;
								$transactionArray['Transaction']['amount']=$response['AMT'];
								$transactionArray['Transaction']['result']=$response['ACK'];
								$transactionArray['Transaction']['pass_id']=$passDetails['Pass']['id'];
								$transactionArray['Transaction']['transaction_array']=$arraySerialize;
								$transactionArray['Transaction']['first_name']=$this->request->data['Transaction']['first_name_cc'];
								$transactionArray['Transaction']['last_name']=$this->request->data['Transaction']['last_name_cc'];
								$transactionArray['Transaction']['comments'] = "passRenewed";		
								$transactionArray['Transaction']['transaction_type'] = 1;		
								$this->Transaction->create();
								if ($this->Transaction->save($transactionArray,false)) {	
											$transactionId=$this->Transaction->getLastInsertId();
											$this->loadModel('CustomerPass');
											$previousId=$this->CustomerPass->field('vehicle_id',array('id'=>$customerPassId));
											$recurring_profile_status=$recurring_profile_id=$package_id=$packageExpiryDate=NULL;
											if($packageDetails){
												if($packageDetails['Package']['is_guest'] == 1){
													$package_id=$packageDetails['Package']['id'];
												}
												if($packageDetails['Package']['is_fixed_duration']==0 && $packageDetails['Package']['is_guest'] == 0){
														$package_expiry_date = $this->CustomerPass->field('pass_valid_upto', array('id' => $customerPassId));
														$dt = new DateTime();
														$currentDateTime = $dt->format('Y-m-d H:i:s');
														if($package_expiry_date){
															if($package_expiry_date>$currentDateTime){
																$currentDateTime=$package_expiry_date; 
															}
														}
														switch ($packageDetails['Package']['duration_type']) 
														{
															case "Hour":
																$date = new DateTime($currentDateTime);
																$k=(int)$packageDetails['Package']['duration'];
																$date->add(new DateInterval('PT'.$k.'H'));
																$packageDetails['Package']['expiration_date']=date_format($date, 'Y-m-d H:i:s');
																break;
															case "Day":
																$date = new DateTime($currentDateTime);
																$k=$packageDetails['Package']['duration'];
																$date->add(new DateInterval('P'.$k.'D'));
																$packageDetails['Package']['expiration_date']=date_format($date, 'Y-m-d H:i:s');
																break;
															case "Week":
																$date = new DateTime($currentDateTime);
																$k=(int)$packageDetails['Package']['duration']*7;
																$date->add(new DateInterval('P'.$k.'D'));
																$packageDetails['Package']['expiration_date']=date_format($date, 'Y-m-d H:i:s');
																break;
															case "Month": 
																$date = new DateTime($currentDateTime);
																$k=(int)$packageDetails['Package']['duration'];
																$date->add(new DateInterval('P'.$k.'M'));
																$packageDetails['Package']['expiration_date']=date_format($date, 'Y-m-d H:i:s');
																break;
															case "Year":
																$date = new DateTime($currentDateTime);
																$k=(int)$packageDetails['Package']['duration'];
																$date->add(new DateInterval('P'.$k.'Y'));
																$packageDetails['Package']['expiration_date']=date_format($date, 'Y-m-d H:i:s');
																break;
														}
														$date4 = new DateTime($packageDetails['Package']['expiration_date']);
														$datePack=$date4->format('Y-m-d');
														$packageExpiryDate=$packageDetails['Package']['expiration_date']= $datePack.' 23:59:59';
														$package_id=$packageDetails['Package']['id'];
													}
													//RECURRING CHECK START
													if(Configure::read('RECURRING')){
														if($passDetails['Pass']['is_fixed_duration'] == 0){
															 if($packageDetails['Package']['is_fixed_duration']==0){
																 if($packageDetails['Package']['is_recurring'] == 1){
																	if($packageDetails['Package']['is_guest'] == 0 && $packageDetails['Package']['cost']>0){
																		$this->loadModel('RecurringProfile');
																		$recurAmount=$packageDetails['Package']['cost'];
																		$recurringPayment = array();
																		$previousProfile=$this->CustomerPass->field('recurring_profile_id',array('id'=>$customerPassId));
																		$this->loadModel('User');
																		$this->User->recursive=-1;
																		$userDetails=$this->User->find('first',array('conditions'=>array('id'=>$userId)));
																		if($previousProfile){
																			$recurringPayment = array(
																							'amount' => $recurAmount,
																							'currency' => 'USD' ,
																							'BILLINGPERIOD'=>$packageDetails['Package']['duration_type'],
																							'BILLINGFREQUENCY'=>$packageDetails['Package']['duration'],
																							'PROFILESTARTDATE'=>$packageDetails['Package']['expiration_date'],
																							'EMAIL'=>$userDetails['User']['email'],
																							'STREET' => $userDetails['User']['address_line_1'],						
																							'ZIP' => $userDetails['User']['zip'],
																							'PROFILEID'=>$previousProfile,
																							 'expiry' => array(
																										'M' => $this->request->data['Transaction']['month'],
																										'Y' => $this->request->data['Transaction']['year'],
																									)
																						);
																		}else{
																			$recurringPayment = array(
																							'amount' => $recurAmount,
																							'card' => $this->request->data['Transaction']['card_number'], 
																							'expiry' => array(
																										'M' => $this->request->data['Transaction']['month'],
																										'Y' => $this->request->data['Transaction']['year'],
																									),
																							'cvv' => $this->request->data['Transaction']['cvv'],
																							'currency' => 'USD' ,
																							'BILLINGPERIOD'=>$packageDetails['Package']['duration_type'],
																							'BILLINGFREQUENCY'=>$packageDetails['Package']['duration'],
																							'PROFILESTARTDATE'=>$packageDetails['Package']['expiration_date'],
																							'FIRSTNAME' => $userDetails['User']['first_name'],
																							'LASTNAME' => $userDetails['User']['last_name'], 
																							'EMAIL'=> $userDetails['User']['email'],
																							'STREET' => $userDetails['User']['address_line_1'],			
																							'CITY' => $userDetails['User']['city'],				
																							'STATE' => $userDetails['User']['state'],						
																							'ZIP' => $userDetails['User']['zip']
																						);
																		}
																		
																		$recurringResponse=$errorMessage=null;
																		try {
																			if($previousProfile){
																				$recurringResponse = $this->PayFlow->modifyRecurringProfilePayFlow($recurringPayment);
																			}else{
																				$recurringResponse = $this->PayFlow->createRecurringProfilePayFlow($recurringPayment);
																			}
																		}catch (Exception $e) {
																			$errorMessage=$e->getMessage();
																		}
																		if($errorMessage==null){
																			if(isset($recurringResponse['RESULT']) && $recurringResponse['RESULT'] == 0) {
																				if($recurringResponse['RESPMSG']=='Approved'){
																					$recurringResponse['PROFILESTATUS']='Active';
																					$array['RecurringProfile']=array(
																									 'recurring_profile_id'=>$recurringResponse['PROFILEID'],
																									 'message'=>'Recurring Profile created',
																									 'status'=>$recurringResponse['PROFILESTATUS'],
																									  'status'=>$recurringResponse['PROFILESTATUS'],
																									 'customer_pass_id'=>$customerPassId
																					);
																					if($previousProfile){
																						$idRecurring=$this->RecurringProfile->field('id',array('recurring_profile_id'=>$previousProfile));
																						$array['RecurringProfile']['id']=$idRecurring;
																					}else{
																						$this->RecurringProfile->create();
																					}
																					if($this->RecurringProfile->save($array)){
																						if($previousProfile){
																							CakeLog::write('pofileDataModifiedAdmin','Profile Data Modified for ID: '.$recurringResponse['PROFILEID']);
																						}else{
																							CakeLog::write('pofileDataSavedAdmin','Profile Data Saved for ID: '.$recurringResponse['PROFILEID']);
																						}
																					}else{
																						if($previousProfile){
																							CakeLog::write('pofileDataNotModifiedAdmin','Profile Data Not Modified for ID: '.$recurringResponse['PROFILEID']);
																						}else{	
																							CakeLog::write('pofileDataNotSavedAdmin','Profile Data Not Saved for ID: '.$recurringResponse['PROFILEID']);
																						}
																					}
																					if($previousProfile){
																						CakeLog::write('recurringProfileModifiedAdmin', ''.$userDetails['User']['username'].' : Recurring Profile created by User: '.AuthComponent::user('first_name').' '.AuthComponent::user('last_name').
																						' In Property: '.$propertyName['Property']['name'].', for Pass Name : '.$passDetails['Pass']['name'].
																						' Profile Id : '.$recurringResponse['PROFILEID'].
																						' Profile Status : '.$recurringResponse['PROFILESTATUS'].''); 
																						$recurring_profile_id=$recurringResponse['PROFILEID'];
																						$recurring_profile_status=$recurringResponse['PROFILESTATUS'];

																					}else{
																						CakeLog::write('recurringProfileCreated', ''.$userDetails['User']['username'].' : Recurring Profile created by User: '.AuthComponent::user('first_name').' '.AuthComponent::user('last_name').
																						' In Property: '.$propertyName['Property']['name'].', for Pass Name : '.$passDetails['Pass']['name'].
																						' Profile Id : '.$recurringResponse['PROFILEID'].
																						' Profile Status : '.$recurringResponse['PROFILESTATUS'].''); 
																						$recurring_profile_id=$recurringResponse['PROFILEID'];
																						$recurring_profile_status=$recurringResponse['PROFILESTATUS'];
																					}
																				}
																			}else{
																				if($previousProfile){
																					  CakeLog::write('recurringProfileModificationFailedAdmin1', ''.$userDetails['User']['username'].' : Recurring Profile creation failed User: '.AuthComponent::user('first_name').' '.AuthComponent::user('last_name').
																					' In Property: '.$propertyName['Property']['name'].', for Pass Name : '.$passDetails['Pass']['name'].' ERROR : '.json_encode($recurringResponse)); 
																				
																				}else{
																					CakeLog::write('recurringProfileCreationFailedAdmin1', ''.$userDetails['User']['username'].' : Recurring Profile creation failed User: '.AuthComponent::user('first_name').' '.AuthComponent::user('last_name').
																					' In Property: '.$propertyName['Property']['name'].', for Pass Name : '.$passDetails['Pass']['name'].' ERROR : '.json_encode($recurringResponse)); 
																				}
																			}
																		}else{
																			if($previousProfile){
																					  CakeLog::write('recurringProfileModificationFailedAdmin', ''.$userDetails['User']['username'].' : Recurring Profile creation failed User: '.AuthComponent::user('first_name').' '.AuthComponent::user('last_name').
																					' In Property: '.$propertyName['Property']['name'].', for Pass Name : '.$passDetails['Pass']['name'].' ERROR : '.json_encode($recurringResponse)); 
																				
																			}else{
																				CakeLog::write('recurringProfileCreationExceptionAdmin', ''.$userDetails['User']['username'].' : Recurring Profile creation failed User: '.AuthComponent::user('first_name').' '.AuthComponent::user('last_name').
																				' In Property: '.$propertyName['Property']['name'].', for Pass Name : '.$passDetails['Pass']['name'].' Exception: '.$errorMessage.''); 
																			}
																		}
																	}
																}
															}
														}
													}
													//RECURRING CHECK END
											}
											$customerPassArray=array();
											$customerPassArray['CustomerPass']=array(
													  'id'=>$customerPassId,
													  'pass_id'=>$passId,
													  'pass_valid_upto'=>$passDetails['Pass']['expiration_date'],
													  'membership_vaild_upto'=>$packageExpiryDate,
													  'package_id'=>$package_id,
													  //'vehicle_id'=>null,
													  'transaction_id'=>$transactionId
													 // 'pass_renewal_cost'=>$passDetails['Pass']['cost_after_1st_year']
												);
											if($recurring_profile_id){
												$customerPassArray['CustomerPass']['recurring_profile_id']=$recurring_profile_id;
											}
											if($recurring_profile_status){
												$customerPassArray['CustomerPass']['recurring_profile_status']=$recurring_profile_status;
											}
											if($packageDetails){
												if($packageDetails['Package']['is_guest']==1){
														$customerPassArray['CustomerPass']['is_guest_pass']=1;
												}else{
													$customerPassArray['CustomerPass']['is_guest_pass']=0;
												}
												if($advance){
													if($packageDetails['Package']['is_guest']==1){
														unset($customerPassArray['CustomerPass']['membership_vaild_upto']);
													}
												}else{
													if($packageDetails['Package']['is_guest']==1){
														$this->loadModel('Vehicle');
														//SET CUSTOMER PASS ID TO NULL IF IT IS ATTACHED TO ANY OTHER VEHICLE
														$this->Vehicle->updateAll(array('Vehicle.customer_pass_id'=>NULL),array('Vehicle.customer_pass_id'=>$customerPassId));
														$customerPassArray['CustomerPass']['vehicle_id']=null;
														if($previousId){
															//SET VEHICLE ID TO NULL IF IT IS ATTACHED TO ANY OTHER PASS
															$this->CustomerPass->updateAll(array('CustomerPass.vehicle_id'=>NULL),array('CustomerPass.vehicle_id'=>$previousId));
															$vehicleArray['Vehicle']=array('id'=>$previousId,
																						   'customer_pass_id'=>null								
															);
															if($this->Vehicle->save($vehicleArray,false)){
																CakeLog::write('passRenewedVehicleNull', ''.AuthComponent::user('username').' : Pass renewed with Pass ID: '.$passId.' Customer Pass ID: <a href="/admin/CustomerPasses/edit/'.$customerPassId.'">'.$customerPassId.'</a> renewed by User: '.AuthComponent::user('first_name').' '.AuthComponent::user('last_name').', in Property: '.CakeSession::read('PropertyName').', VEHICLE SET NULL');
															}else{
																CakeLog::write('passRenewedVehicleNotNULL', ''.AuthComponent::user('username').' : Pass renewed with Pass ID: '.$passId.' And Customer Pass ID: <a href="/admin/CustomerPasses/edit/'.$customerPassId.'">'.$customerPassId.'</a> renewed by User: '.AuthComponent::user('first_name').' '.AuthComponent::user('last_name').', in Property: '.CakeSession::read('PropertyName').', VEHICLE NOT SET NULL');
															}
														}
													}
												}
											}
											$this->CustomerPass->create();
											if($this->CustomerPass->save($customerPassArray,false)){
														$this->loadModel('Pass');
														$passName=$this->Pass->givePassName($passId);
														if($advance){
															CakeLog::write('passRenewedAdvanceAdmin', ''.AuthComponent::user('username').' : Pass renewed IN ADVANCE with Pass ID: '.$passId.' and Pass Name: '.$passName.', and Customer Pass ID: <a href="/admin/CustomerPasses/edit/'.$customerPassId.'">'.$customerPassId.'</a> renewed by User: '.AuthComponent::user('first_name').' '.AuthComponent::user('last_name').', in Property: '.$propertyName['Property']['name'].', Amount Paid: $'.$passRenewalcost.' EXPIRY DATE WAS : '.$pass_expiry);
														}else{
															CakeLog::write('passRenewedAdmin', ''.AuthComponent::user('username').' : Pass renewed with Pass ID: '.$passId.' and Pass Name: '.$passName.', and Customer Pass ID: <a href="/admin/CustomerPasses/edit/'.$customerPassId.'">'.$customerPassId.'</a> renewed by User: '.AuthComponent::user('first_name').' '.AuthComponent::user('last_name').', in Property: '.$propertyName['Property']['name'].', Amount Paid: $'.$passRenewalcost.'');
														}
														$billAddressArray['BillingAddress']['user_id']=$userId;
														$billAddressArray['BillingAddress']['email']=$this->request->data['Transaction']['email'];
														$billAddressArray['BillingAddress']['first_name']=$this->request->data['Transaction']['first_name'];
														$billAddressArray['BillingAddress']['last_name']=$this->request->data['Transaction']['last_name'];
														$billAddressArray['BillingAddress']['address_line_1']=$this->request->data['Transaction']['address_line_1'];
														$billAddressArray['BillingAddress']['address_line_2']=$this->request->data['Transaction']['address_line_2'];
														$billAddressArray['BillingAddress']['city']=$this->request->data['Transaction']['city'];
														$billAddressArray['BillingAddress']['state']=$this->request->data['Transaction']['state'];
														$billAddressArray['BillingAddress']['zip']=$this->request->data['Transaction']['zip'];
														$billAddressArray['BillingAddress']['phone']=$this->request->data['Transaction']['phone'];
													
														$this->loadModel('BillingAddress');
														$billAddress=new BillingAddress();
														$billAddress->create();
														$rslt=$billAddress->hasAny(array('user_id'=>$this->Auth->user('id')));
														if($rslt==true){
															$billAddressId=$billAddress->find('first',array('fields'=>array('id'),'conditions'=>array('user_id'=>$passId)));
															$billAddressArray['BillingAddress']['id']=$billAddressId['BillingAddress']['id'];
														}
														if($billAddress->save($billAddressArray,false)){																
																$this->Session->setFlash('Your transaction has been completed succesfully. Pass renewed successfully. Now please add existing or new vehicle to this pass by clicking on the "Add Vhicle"','sucess');
																$this->Session->delete('Allowed');
																$this->Session->write('Allowed',true);
																return $this->redirect(array('controller'=>'CustomerPasses','action' => 'customer_view',$userId));
														}else{
															$this->Session->setFlash(__('Billing Address Not Saved'));	
															return $this->redirect(array('controller'=>'CustomerPasses','action' => 'customer_view',$userId));
														}				
										}
										else {
											$this->Session->setFlash('Pass not saved','error');	
											return $this->redirect(array('controller'=>'CustomerPasses','action' => 'customer_view',$userId));
										}
								} else {
									$this->Session->setFlash('The transaction could not be saved. Contact Admin','error');
								}																
						}
						else{
							$this->Session->setFlash('Transaction Failed','error');
						}					
					}
					else{
						$this->Session->setFlash($errorMessage,'error');
					}
		}
		else{
			$this->Session->setFlash('Validation Failure','error');
		}
	}
	$this->loadModel('BillingAddress');
	$this->BillingAddress->recursive=-1;
    $billingAddress=$this->BillingAddress->find('first',array('conditions'=>array('user_id'=>$userId)));
    $this->loadModel('State');
    $state=new State();
    $states=$state->find('list');
    $this->set(compact('billingAddress','states','passRenewalcost','passExpiryDate','passName','renewAllowed'));
 }

    /*     * **********************************************
     * Function to handle IPN Reponses Added on 10th Feb 2015
     */

    public function admin_recurringPaymentReponse() {
        $this->autoRender = false;
        //$this->loadModel('CustomerPass');
        //$this->loadModel('Pass');
        //$this->CustomerPass->recursive=-1;
        //$customerPassData=$this->CustomerPass->find('first',array('fields'=>array('pass_id','id','user_id'),'conditions'=>array('recurring_profile_id'=>'I-LUCMKYCLUYAJ')));
        //$this->Pass->recursive=-1;
        //$passDetails=$this->Pass->find('first',array('conditions'=>array('id'=>$customerPassData['CustomerPass']['pass_id'])));
        //debug($passDetails);
        //debug($customerPassData);die;
        if ($this->request->data) {
            //echo "Message Recieved";die;
            $verified = false;
            CakeLog::write('recurringResponseParsed', serialize($this->request->data));
            $this->PaypalIPNListener->use_sandbox = false;
            try {
                $verified = $this->PaypalIPNListener->processIpn($this->request->data);
            } catch (Exception $e) {
                CakeLog::write('recurringError', $e->getMessage());
            }

            if ($verified) {
                $data = $this->request->data;
                $response = "";
                foreach ($data as $key => $value) {
                    $response = $response . "<br>" . $key . " :-" . $value;
                }
                if (isset($data['recurring_payment_id'])) {
                    if ($data['txn_type'] == 'recurring_payment_profile_created') {
                        $this->loadModel('CustomerPass');
                        $customerPassId = $this->CustomerPass->field('id', array('recurring_profile_id' => $data['recurring_payment_id']));
                        if ($customerPassId) {
                            $user_id = $this->CustomerPass->field('user_id', array('recurring_profile_id' => $data['recurring_payment_id']));
                            if ($user_id) {
                                $this->loadModel('User');
                                $this->User->recursive = -1;
                                $userData = $this->User->find('first', array('conditions' => array('id' => $user_id)));
                                if ($userData) {
                                    $msg = 'Dear ' . $userData['User']['first_name'] . ' ' . $userData['User']['last_name'] . '
										This email is to inform you that your recurring payment ';
                                    $pass_id = $this->CustomerPass->field('pass_id', array('recurring_profile_id' => $data['recurring_payment_id']));
                                    $property_id = $this->CustomerPass->field('property_id', array('recurring_profile_id' => $data['recurring_payment_id']));
                                    $property_name = '';
                                    $pass_name = '';
                                    if ($property_id) {
                                        $this->loadModel('Property');
                                        $property_name = $this->Property->field('name', array('id' => $property_id));
                                        if ($property_name) {
                                            $msg = $msg . ' in property : ' . $property_name;
                                        }
                                    }
                                    if ($pass_id) {
                                        $this->loadModel('Pass');
                                        $pass_name = $this->Pass->field('name', array('id' => $pass_id));
                                        if ($pass_name) {
                                            $msg = $msg . ' for pass : ' . $pass_name;
                                        }
                                    }
                                    $msg = $msg . ' has been successfully created.
										
										Recurring Profile Details :
													 
										Name : ' . $data['first_name'] . ' ' . $data['last_name'] . '
										Initial Payment : ' . $data['initial_payment_amount'] . ' ' . $data['currency_code'] . '
										Recurring Amount : ' . $data['amount'] . ' ' . $data['currency_code'] . '
										Payment Cycle : ' . $data['payment_cycle'];
                                    $this->Email->from = 'Online Parking Pass<noreply@internetparkingpass.com>';
                                    $this->Email->to = $userData['User']['email'];
                                    $this->Email->subject = 'Recurring Payment Information';
                                    /* $this->Email->smtpOptions = array(
                                      'port'=>'25',
                                      'timeout'=>'30',
                                      'host' => 'localhost',
                                      'username'=>'alert@digital6hosting.com',
                                      'password'=>'fUMLb6!8=TfM'
                                      //'client' => 'smtp_helo_hostname'
                                      );
                                      $this->Email->delivery = 'smtp'; */
                                    try {
                                        if ($this->Email->send($msg)) {
                                            CakeLog::write('recurringProfileEmailVerification', $data['recurring_payment_id'] . ' : Recurring Profile email verification done');
                                        }
                                    } catch (Exception $e) {
                                        CakeLog::write('recurringProfileEmailVerificationError', $data['recurring_payment_id'] . ' : Recurring Profile email verification Exception Generated');
                                    }
                                }
                            }
                            $this->loadModel('RecurringProfile');
                            $this->RecurringProfile->updateAll(array('customer_pass_id' => $customerPassId), array('recurring_profile_id' => $data['recurring_payment_id']));
                        }
                        CakeLog::write('recurringrecurringProfileCreatedResponse', $data['recurring_payment_id'] . " :" . $response);
                        CakeLog::write('recurringProfileCreated', $data['recurring_payment_id'] . " : Recurring Profile Created ");
                    } elseif ($data['txn_type'] == 'recurring_payment_profile_cancel') {
                        $this->loadModel('CustomerPass');
                        $this->CustomerPass->updateAll(array('recurring_profile_status' => "'" . $data['profile_status'] . "'"), array('recurring_profile_id' => $data['recurring_payment_id']));
                        $this->loadModel('RecurringProfile');
                        //	$recurringId=$this->RecurringProfile->field('id',array('recurring_profile_id'=>$data['recurring_payment_id']));
                        //if($recurringId){
                        $array['RecurringProfile'] = array('recurring_profile_id' => $data['recurring_payment_id'],
                            'message' => 'Recurring Profile Deactivated',
                            'status' => $data['profile_status']
                        );
                        $this->RecurringProfile->create();
                        $this->RecurringProfile->save($array);
                        //}

                        CakeLog::write('recurringProfileCanceledResponse', $data['recurring_payment_id'] . " :" . $response);
                        CakeLog::write('recurringProfileCanceled', $data['recurring_payment_id'] . " : Recurring Profile Canceled");
                    } elseif ($data['txn_type'] == 'recurring_payment_profile_modify') {
                        CakeLog::write('recurringProfileModifiedResponse', $data['recurring_payment_id'] . " :" . $response);
                        CakeLog::write('recurringProfileModified', $data['recurring_payment_id'] . " : Recurring Profile Modified");
                    } elseif ($data['txn_type'] == 'recurring_payment') {
                        CakeLog::write('recurringPaymentResponse', $data['recurring_payment_id'] . " :" . $response);
                        if ($data['payment_status'] == 'Completed') {
                            $this->loadModel('CustomerPass');
                            $this->CustomerPass->recursive = -1;
                            $customerPassData = $this->CustomerPass->find('first', array('fields' => array('pass_id', 'id', 'user_id'), 'conditions' => array('recurring_profile_id' => $data['recurring_payment_id'])));
                            if ($customerPassData) {
                                if (!empty($customerPassData)) {
                                    $this->loadModel('Pass');
                                    $passDetails = $packageDetails = array();
                                    $this->Pass->recursive = -1;
                                    $passDetails = $this->Pass->find('first', array('conditions' => array('id' => $customerPassData['CustomerPass']['pass_id'])));
                                    if ($passDetails) {
                                        if ($passDetails['Pass']['is_fixed_duration'] == 0) {
                                            $dt = new DateTime();
                                            $currentDateTime = $dt->format('Y-m-d H:i:s');
                                            switch ($passDetails['Pass']['duration_type']) {
                                                case "Hour":
                                                    $date = new DateTime($currentDateTime);
                                                    $k = (int) $passDetails['Pass']['duration'];
                                                    $date->add(new DateInterval('PT' . $k . 'H'));
                                                    $passDetails['Pass']['expiration_date'] = date_format($date, 'Y-m-d H:i:s');
                                                    break;
                                                case "Day":
                                                    $date = new DateTime($currentDateTime);
                                                    $k = $passDetails['Pass']['duration'];
                                                    $date->add(new DateInterval('P' . $k . 'D'));
                                                    $passDetails['Pass']['expiration_date'] = date_format($date, 'Y-m-d H:i:s');
                                                    break;
                                                case "Week":
                                                    $date = new DateTime($currentDateTime);
                                                    $k = (int) $passDetails['Pass']['duration'] * 7;
                                                    $date->add(new DateInterval('P' . $k . 'D'));
                                                    $passDetails['Pass']['expiration_date'] = date_format($date, 'Y-m-d H:i:s');
                                                    break;
                                                case "Month":
                                                    $date = new DateTime($currentDateTime);
                                                    $k = (int) $passDetails['Pass']['duration'];
                                                    $date->add(new DateInterval('P' . $k . 'M'));
                                                    $passDetails['Pass']['expiration_date'] = date_format($date, 'Y-m-d H:i:s');
                                                    break;
                                                case "Year":
                                                    $date = new DateTime($currentDateTime);
                                                    $k = (int) $passDetails['Pass']['duration'];
                                                    $date->add(new DateInterval('P' . $k . 'Y'));
                                                    $passDetails['Pass']['expiration_date'] = date_format($date, 'Y-m-d H:i:s');
                                                    break;
                                            }
                                        }
                                        if ($passDetails['Pass']['package_id']) {
                                            $this->loadModel('Package');
                                            $this->Package->recursive = -1;
                                            $packageDetails = $this->Package->find('first', array('conditions' => array('id' => $passDetails['Pass']['package_id'])));
                                            if ($packageDetails['Package']['is_fixed_duration'] == 0) {
                                                $dt = new DateTime();
                                                $currentDateTime = $dt->format('Y-m-d H:i:s');
                                                switch ($packageDetails['Package']['duration_type']) {
                                                    case "Hour":
                                                        $date = new DateTime($currentDateTime);
                                                        $k = (int) $packageDetails['Package']['duration'];
                                                        $date->add(new DateInterval('PT' . $k . 'H'));
                                                        $packageDetails['Package']['expiration_date'] = date_format($date, 'Y-m-d H:i:s');
                                                        break;
                                                    case "Day":
                                                        $date = new DateTime($currentDateTime);
                                                        $k = $packageDetails['Package']['duration'];
                                                        $date->add(new DateInterval('P' . $k . 'D'));
                                                        $packageDetails['Package']['expiration_date'] = date_format($date, 'Y-m-d H:i:s');
                                                        break;
                                                    case "Week":
                                                        $date = new DateTime($currentDateTime);
                                                        $k = (int) $packageDetails['Package']['duration'] * 7;
                                                        $date->add(new DateInterval('P' . $k . 'D'));
                                                        $packageDetails['Package']['expiration_date'] = date_format($date, 'Y-m-d H:i:s');
                                                        break;
                                                    case "Month":
                                                        $date = new DateTime($currentDateTime);
                                                        $k = (int) $packageDetails['Package']['duration'];
                                                        $date->add(new DateInterval('P' . $k . 'M'));
                                                        $packageDetails['Package']['expiration_date'] = date_format($date, 'Y-m-d H:i:s');
                                                        break;
                                                    case "Year":
                                                        $date = new DateTime($currentDateTime);
                                                        $k = (int) $packageDetails['Package']['duration'];
                                                        $date->add(new DateInterval('P' . $k . 'Y'));
                                                        $packageDetails['Package']['expiration_date'] = date_format($date, 'Y-m-d H:i:s');
                                                        break;
                                                }
                                            }
                                        } else {
                                            CakeLog::write('noPackageFoundForRecurringProfile', $data['recurring_payment_id'] . " : No Package Found for this Recurring ID ");
                                        }
                                        $dt = new DateTime();
                                        $currentDateTime = $dt->format('Y-m-d H:i:s');
                                        $response = array('AMT' => $data['amount'],
                                            'ACK' => 'Success',
                                        );
                                        $arraySerialize = serialize($response);
                                        $dt = new DateTime();
                                        $currentDateTime = $dt->format('Y-m-d H:i:s');
                                        $transactionArray['Transaction']['paypal_tranc_id'] = $data['txn_id'];
                                        $transactionArray['Transaction']['user_id'] = $customerPassData['CustomerPass']['user_id'];
                                        $transactionArray['Transaction']['date_time'] = $currentDateTime;
                                        $transactionArray['Transaction']['amount'] = $response['AMT'];
                                        $transactionArray['Transaction']['result'] = $response['ACK'];
                                        $transactionArray['Transaction']['pass_id'] = $passDetails['Pass']['id'];
                                        $transactionArray['Transaction']['transaction_array'] = $arraySerialize;
                                        $transactionArray['Transaction']['first_name'] = $data['first_name'];
                                        $transactionArray['Transaction']['last_name'] = $data['last_name'];
                                        $transactionArray['Transaction']['message'] = 'This is a recurring payment for recurring ID : ' . $data['recurring_payment_id'];
                                        $transactionArray['Transaction']['comments'] = "passActivated";
                                        if ($this->Transaction->save($transactionArray, false)) {
                                            $transactionId = $this->Transaction->getLastInsertId();
                                            $this->loadModel('CustomerPass');
                                            $previousId = $this->CustomerPass->field('vehicle_id', array('id' => $customerPassId));
                                            $packageId = $packageExpiry = null;
                                            if ($passDetails['Pass']['package_id']) {
                                                if ($packageDetails['Package']['is_guest'] == 0) {
                                                    $packageId = $packageDetails['Package']['id'];
                                                    $packageExpiry = $packageDetails['Package']['expiration_date'];
                                                }
                                            }
                                            $array['CustomerPass'] = array(
                                                'id' => $customerPassData['CustomerPass']['id'],
                                                // 'pass_id'=>$passDetails['Pass']['id'],
                                                //'pass_valid_upto'=>$passDetails['Pass']['expiration_date'],
                                                'membership_vaild_upto' => $packageExpiry,
                                                'package_id' => $packageId,
                                                'transaction_id' => $transactionId
                                            );
                                            if ($this->CustomerPass->save($array, false)) {
                                                $this->loadModel('RecurringProfile');
                                                $array['RecurringProfile'] = array('recurring_profile_id' => $data['recurring_payment_id'], 'paypal_transaction_id' => $data['txn_id'],
                                                    'message' => 'Recurring Payment Recieved<br>
																													Amount Recieved: ' . $data['amount'] . '<br>
																													Creation Date (Paypal Server): ' . $data['time_created'] . '<br>
																													Next Payment Date : ' . $data['next_payment_date']
                                                );
                                                $this->RecurringProfile->create();
                                                $this->RecurringProfile->save($array);
                                                $passName = $this->Pass->givePassName($passDetails['Pass']['id']);
                                                CakeLog::write('recurringPayment', $data['recurring_payment_id'] . " : Recurring Payment Done ");
                                                CakeLog::write('passRenewedRecurring', $data['recurring_payment_id'] . ' : Pass Activated with Pass ID: ' . $passDetails['Pass']['id'] . ' and Pass Name: ' . $passName . ', and Customer Pass ID: <a href="/admin/CustomerPasses/edit/' . $customerPassData['CustomerPass']['id'] . '">' . $customerPassData['CustomerPass']['id'] . '</a> renewed by Recurring Payment, Amount Paid: $' . $response['AMT'] . '');
                                            }
                                        } else {
                                            CakeLog::write('transactionNotSaved', $data['recurring_payment_id'] . " : Transaction Data Not Saved For this Recurring ID ");
                                        }
                                    } else {
                                        CakeLog::write('noPassFoundForRecurringProfile', $data['recurring_payment_id'] . " : No Pass Found for this Recurring ID ");
                                    }
                                } else {
                                    CakeLog::write('noRecurringProfileFound', $data['recurring_payment_id'] . " : No Customer Pass Found for this Recurring ID ");
                                }
                            } else {
                                CakeLog::write('noRecurringProfileFound', $data['recurring_payment_id'] . " : No Customer Pass Found for this Recurring ID ");
                            }
                        } elseif ($data['payment_status'] == 'Denied' || $data['payment_status'] == 'Pending') {
                            CakeLog::write('recurringPaymentNotCompleted', $data['recurring_payment_id'] . " : Recurring Payment was not completed Status:" . $data['payment_status'] . "Reason : " . $data['pending_reason']);
                        } else {
                            CakeLog::write('recurringPaymentNotCompleted', $data['recurring_payment_id'] . " : Recurring Payment was not completed Status : " . $data['payment_status']);
                        }
                    } elseif ($data['txn_type'] == 'recurring_payment_skipped') {
                        CakeLog::write('recurringPaymentSkippedResponse', $data['recurring_payment_id'] . " :" . $response);
                        CakeLog::write('recurringPaymentSkipped', $data['recurring_payment_id'] . " : Recurring Payment Skipped ");
                    } elseif ($data['txn_type'] == 'recurring_payment_failed') {
                        CakeLog::write('recurringPaymentFailedResponse', $data['recurring_payment_id'] . " :" . $response);
                        CakeLog::write('recurringPaymentFailed', $data['recurring_payment_id'] . " : Recurring Payment Failed ");
                    } elseif ($data['txn_type'] == 'recurring_payment_suspended_due_to_max_failed_payment') {
                        CakeLog::write('recurringPaymentSuspendedResponse', $data['recurring_payment_id'] . " :" . $response);
                        CakeLog::write('recurringPaymentSuspended', $data['recurring_payment_id'] . " : recurring_payment_suspended_due_to_max_failed_payment");
                    }
                } else {
                    CakeLog::write('paymentResponse', $response);
                }
            } else {
                CakeLog::write('recurringNotVerified', "recurring Not Verified");
            }
        } else {
            CakeLog::write('TestRecurring', "TEST ONLY");
        }
    }

    /*     * **********************************************
     * Function to cancel Recurring Profiles using CRONJOB Added on 10th Feb 2015
     */

    public function admin_cancel_recurring_profile() {
		die;
       /* $this->layout = $this->autoRender = false;
        $this->loadModel('CustomerPass');
        $this->CustomerPass->recursive = -1;
        $dt = new DateTime();
        $currentDateTime = $dt->format('Y-m-d H:i:s');
        $toDate = date('Y-m-d H:i:s', strtotime($currentDateTime . "+2 hours"));
        //debug($currentDateTime);
        //debug($toDate);
        $result = $this->CustomerPass->find('all', array('conditions' => array('recurring_profile_status = \'ActiveProfile\' AND pass_valid_upto BETWEEN \'' . $currentDateTime . '\' AND \'' . $toDate . '\'')));
        //$result=$this->CustomerPass->find('all',array('conditions'=>array('recurring_profile_status = \'ActiveProfile\' AND DATEDIFF(hour,pass_valid_uotp,\''.$currentDateTime.'\')=2')));
        //debug($result);die;
        $msg = ' Cron Job Working internetparkingPass';
        $this->Email->from = 'Online Parking Pass<noreply@netparkingpass.com>';
        $this->Email->to = 'shalender@netgen.in';
        $this->Email->subject = 'CronJob Information';
        try {
            if ($this->Email->send($msg)) {
                CakeLog::write('cronJobVerification', ' Cronjob Mail Sent');
            }
        } catch (Exception $e) {
            CakeLog::write('cronJobVerificationError', ' Cronjob Mail Sent Exception Generated');
        }
        for ($i = 0; $i < count($result); $i++) {

            $recurringPayment = array('ACTION' => 'cancel',
                'PROFILEID' => $result[$i]['CustomerPass']['recurring_profile_id']
            );
            $errorMessage = null;
            try {
                $recurringResponse = $this->Paypal->cancelRecurringProfile($recurringPayment);
                $response = "";
                foreach ($recurringResponse as $key => $value) {
                    $response = $response . "<br>" . $key . " :-" . $value;
                }
                CakeLog::write('cronRecuringProfileCancelAttempt', $response);
                if ($recurringResponse['ACK'] == 'Success') {
                    if ($this->CustomerPass->updateAll(array('recurring_profile_status' => "'Cancelled'"), array('recurring_profile_id' => $recurringResponse['PROFILEID']))) {
                        CakeLog::write('cronRecuringProfileCancelled', $recurringResponse['PROFILEID'] . " : Recurring Profile Cancelled ");
                    } else {
                        CakeLog::write('cronRecuringProfileNotCancelled', $recurringResponse['PROFILEID'] . " : Recurring Profile Not Cancelled ");
                    }
                } else {
                    CakeLog::write('cronRecuringProfileCancelAttemptFailed', $result[$i]['CustomerPass']['recurring_profile_id'] . " : Recurring Profile Cancellation Attempt Failed ");
                }
            } catch (Exception $e) {
                $errorMessage = $e->getMessage();
                CakeLog::write('cronRecuringProfileCancelAttemptException', $result[$i]['CustomerPass']['recurring_profile_id'] . ' : ' . $errorMessage);
            }
        }*/
    }
     /********************************************
  * All properties pass cost in time frame
  */
  public function admin_pass_costs(){
  
  }
   public function admin_pass_cost_listing(){
			$this->autoRender=false;
			$toDate=date('Y-m-d',strtotime($this->request->query['toDate']));
			$fromDate=date('Y-m-d',strtotime($this->request->query['fromDate']));
			$conditions='';
			$conditionsCount='';
			if($toDate==$fromDate){
				$toDate=$fromDate.' 23:59:59';
			}else{
				$toDate=$toDate.' 23:59:59';
			}
			$conditions= " Transaction.date_time BETWEEN '".$fromDate." 00:00:00' AND '".$toDate."' ";
			$this->loadModel('Property');
			$this->loadModel('CustomerPass');
			$this->Property->recursive=-1;
			$allProperty=$this->Property->find('all',array('fields'=>array('id','name'),'order'=>array('id'=>'ASC')));
			$propertyIds=array_keys($allProperty);
			//SELECT pu.property_id,p.name,SUM(t.amount) FROM `transactions` t LEFT JOIN property_users pu ON pu.user_id=t.user_id LEFT JOIN properties p ON p.id=pu.property_id GROUP BY pu.property_id
			$this->Transaction->recursive=-1;
			$cost=$this->Transaction->find('all',array(
														'fields'=>array('pu.property_id','p.name','SUM(Transaction.amount) as cost'),
														'joins'=>array(
																		array(
																				'table'=>'property_users',
																				'alias'=>'pu',
																				'type'=>'LEFT',
																				'conditions'=>array(
																					'pu.user_id=Transaction.user_id'
																				)
																		),
																		array(
																			'table'=>'properties',
																			'alias'=>'p',
																			'type'=>'LEFT',
																			'conditions'=>array(
																					'p.id=pu.property_id'
																			)
																		)
																
														),
														'group'=>array('pu.property_id'),
														'conditions'=>$conditions,
														'order'=>array('pu.property_id'=>'ASC')
			
			
			));
			$this->CustomerPass->recursive=-1;
			$passCount=$this->CustomerPass->find('all',array('fields'=>array('property_id','count(*) as passcount'),
																'conditions'=>array('RFID_tag_number IS NOT NULL and created BETWEEN "'.$fromDate.' 00:00:00" AND "'.$toDate.'"'),
																'group'=>array('CustomerPass.property_id'),
																'order'=>array('CustomerPass.property_id'=>'ASC')
																)
												);
			$totalPasses=$totalAmount=0;
						for($i=0;$i<count($allProperty);$i++){
							$allProperty[$i]['Property']['cost']=0;
							$allProperty[$i]['Property']['passCount']=0;
							for($j=0;$j<count($cost);$j++){
								if($cost[$j]['pu']['property_id']==$allProperty[$i]['Property']['id']){
									$allProperty[$i]['Property']['cost']=$cost[$j][0]['cost'];
									$totalAmount=$totalAmount+$cost[$j][0]['cost'];
								}
							}
							for($k=0;$k<count($passCount);$k++){
								if($passCount[$k]['CustomerPass']['property_id']==$allProperty[$i]['Property']['id']){
									$allProperty[$i]['Property']['passCount']=$passCount[$k][0]['passcount'];
									$totalPasses=$totalPasses+$passCount[$k][0]['passcount'];
								}
							}
						} 
			$allProperty[]['TotalAmount']=$totalAmount;
			$allProperty[]['TotalPasses']=$totalPasses;
			echo json_encode($allProperty);  
  }
  public function admin_renewal_pass_costs(){
  
  }
   public function admin_renewal_pass_cost_listing(){
			$this->autoRender=false;
			$toDate=date('Y-m-d',strtotime($this->request->query['toDate']));
			$fromDate=date('Y-m-d',strtotime($this->request->query['fromDate']));
			$conditions='';
			$conditionsCount='';
			if($toDate==$fromDate){
				$toDate=$fromDate.' 23:59:59';
			}else{
				$toDate=$toDate.' 23:59:59';
			}
			$conditions= " date_time BETWEEN '".$fromDate." 00:00:00' AND '".$toDate."' AND Transaction.comments='passRenewed'";
			$this->loadModel('Property');
			$this->loadModel('CustomerPass');
			$this->Property->recursive=-1;
			$allProperty=$this->Property->find('all',array('fields'=>array('id','name')));
			$propertyIds=array_keys($allProperty);
			for($i=0;$i<count($allProperty);$i++){
				$this->Transaction->recursive=-1;
				$allProperty[$i]['Property']['cost']=$this->Transaction->field('SUM(amount)',array(' user_id IN (SELECT user_id from property_users where property_id='.$allProperty[$i]['Property']['id'].' ) and '.$conditions));
			}
			
			$totalAmount=$this->Transaction->field('SUM(amount)',array($conditions));
			$this->CustomerPass->recursive=-1;
			$arr['PropertyData']=$allProperty;
			$arr['TotalAmount']=$totalAmount;
			echo json_encode($arr);  
  }
  /********************************************
  * All properties guest pass cost in time frame
  */
  public function admin_guest_pass_costs(){
  
  }
   public function admin_guest_pass_cost_listing(){
			$this->autoRender=false;
			$toDate=date('Y-m-d',strtotime($this->request->query['toDate']));
			$fromDate=date('Y-m-d',strtotime($this->request->query['fromDate']));
			$conditions='';
			$conditionsCount='';
			if($toDate==$fromDate){
				$toDate=$fromDate.' 23:59:59';
			}else{
				$toDate=$toDate.' 23:59:59';
			}
			$conditions= " created BETWEEN '".$fromDate." 00:00:00' AND '".$toDate."' ";
			$this->loadModel('Property');
			$this->loadModel('UserGuestPass');
			$this->Property->recursive=-1;
			$allProperty=$this->Property->find('all',array('fields'=>array('id','name')));
			for($i=0;$i<count($allProperty);$i++){
				$allProperty[$i]['Property']['freeDays']=$this->UserGuestPass->field('SUM(days)',array('amount=0 and property_id='.$allProperty[$i]['Property']['id'].' and '.$conditions));
				$allProperty[$i]['Property']['paidDays']=$this->UserGuestPass->field('SUM(days)',array('amount!=0 and property_id='.$allProperty[$i]['Property']['id'].' and '.$conditions));
				$allProperty[$i]['Property']['paidAmount']=$this->UserGuestPass->field('SUM(amount)',array('amount!=0 and property_id='.$allProperty[$i]['Property']['id'].' and '.$conditions));
			}
			$totalAmount=$this->UserGuestPass->field('SUM(amount)',array($conditions));
			$allProperty[]['TotalAmount']=$totalAmount;
			$allProperty[]['totalFreeDays']=$this->UserGuestPass->field('SUM(days)',array('amount=0 and '.$conditions));
			$allProperty[]['totalPaidDays']=$this->UserGuestPass->field('SUM(days)',array('amount!=0 and '.$conditions));
			echo json_encode($allProperty);  
  }
   public function admin_search(){
	  $this->Transaction->unbindModel(array('hasMany'=>array('CustomerPass'),'belongsTo'=>array('Pass')));
	 $results= $this->Transaction->find('all',array('conditions'=>array('paypal_tranc_id LIKE "%'.$this->request->data['Transaction']['search_key'].'%"')));
	 $this->loadModel('Pass');
	 for($i=0;$i<count($results);$i++){
		$passIds=explode(",",trim($results[$i]['Transaction']['pass_id']));
		$passName='';
		for($j=0;$j<count($passIds);$j++){
			if(!empty($passIds[$j])){
				$passName=$passName.$this->Pass->givePassName($passIds[$j]);
				if($j<count($passIds)-1){
					$passName=$passName.',';
				}
			}
			
		}
		$results[$i]['Transaction']['pass_id']=$passName;
	 }
	 $this->set(compact('results'));
  }
  /********************************************************************
   * 1st September 2015
   * Buy Passes Using Coupons
   */
   public function buy_passes(){
		$couponPackageId=CakeSession::read('CurrentPackage');
		$this->loadModel('CouponPackage');
		$this->layout = 'customer'; 
		$this->loadModel('Pass');
		$this->loadModel('Package'); 
		
		$dt = new DateTime();
        $currentDateTime = $dt->format('Y-m-d H:i:s');
		$allPasses=$allGuestPasses=$allOtherPasses=$otherRequiredPasses=$otherPasses=$returnArray=array();
		
		$this->CouponPackage->recursive=-1;
		$couponPackage=$this->CouponPackage->find('first',array('conditions'=>array('id'=>$couponPackageId)));
		
		if(!$couponPackageId){
			throw new NotFoundException(__('Invalid Coupon Package'));
		}
		if(!$couponPackage){
			throw new NotFoundException(__('No Coupon Package'));
		}
		$couponPasses=explode(',',$couponPackage['CouponPackage']['passes']);
		for($hg=0;$hg<count($couponPasses);$hg++){
			$this->Pass->unbindModel(array('belongsTo'=>array('Property')));
			$this->Pass->unbindModel(array('hasMany'=>array('Transaction','CustomerPass')));
			$couponPass=$this->Pass->find('first',array('conditions'=>array('Pass.id'=>$couponPasses[$hg])));
			if($couponPass){
				if($couponPass['Pass']['expiration_date']){
					if($couponPass['Pass']['expiration_date']>$currentDateTime){
						if(isset($couponPass['Package']['expiration_date'])){
							if($couponPass['Package']['expiration_date']){
								if($couponPass['Package']['expiration_date']>$currentDateTime){
									$allPasses[]=$couponPass;
								}
							}else{
								$allPasses[]=$couponPass;
							}
						}
					}
				}else{
					$allPasses[]=$couponPass;
				}
				
			}
		}
		if(empty($allPasses)){
			$this->Session->setFlash('Please Select The Passes','notice');
			return $this->redirect(array('action'=>'select_passes'));
		}		
		$totalAmount=0;
		$returnArray=array();
		for ($i = 0; $i < count($allPasses); $i++) {
			$passTotalCost=0;
			$returnArray[$i]['Pass']['passName']=$allPasses[$i]['Pass']['name'];
			if(isset($allPasses[$i]['Package'])){
				$dt = new DateTime();
                $currentDateTime = $dt->format('Y-m-d H:i:s');
				if ($allPasses[$i]['Package']['is_fixed_duration'] == 0) {
                        switch ($allPasses[$i]['Package']['duration_type']) {
                            case "Hour":
                                $date = new DateTime($currentDateTime);
                                $k = (int) $allPasses[$i]['Package']['duration'];
                                $date->add(new DateInterval('PT' . $k . 'H'));
                                $allPasses[$i]['Package']['expiration_date'] = date_format($date, 'Y-m-d H:i:s');
                                break;
                            case "Day":
                                $date = new DateTime($currentDateTime);
                                $k = $allPasses[$i]['Package']['duration'];
                                $date->add(new DateInterval('P' . $k . 'D'));
                                $allPasses[$i]['Package']['expiration_date'] = date_format($date, 'Y-m-d H:i:s');
                                break;
                            case "Week":
                                $date = new DateTime($currentDateTime);
                                $k = (int) $allPasses[$i]['Package']['duration'] * 7;
                                $date->add(new DateInterval('P' . $k . 'D'));
                                $allPasses[$i]['Package']['expiration_date'] = date_format($date, 'Y-m-d H:i:s');
                                break;
                            case "Month":
                                $date = new DateTime($currentDateTime);
                                $k = (int) $allPasses[$i]['Package']['duration'];
                                $date->add(new DateInterval('P' . $k . 'M'));
                                $allPasses[$i]['Package']['expiration_date'] = date_format($date, 'Y-m-d H:i:s');
                                break;
                            case "Year":
                                $date = new DateTime($currentDateTime);
                                $k = (int) $allPasses[$i]['Package']['duration'];
                                $date->add(new DateInterval('P' . $k . 'Y'));
                                $allPasses[$i]['Package']['expiration_date'] = date_format($date, 'Y-m-d H:i:s');
                                break;
                        }
                }
                $date3 = new DateTime($allPasses[$i]['Package']['expiration_date']);
				$datePack=$date3->format('Y-m-d');
                $allPasses[$i]['Package']['expiration_date']= $datePack.' 23:59:59';
             
				$returnArray[$i]['Pass']['packageName']=$allPasses[$i]['Package']['name'];
				if($allPasses[$i]['Package']['is_required']==1){
							 $returnArray[$i]['Pass']['required']="Yes";
				}else{
						    $returnArray[$i]['Pass']['required']="No";
				}
				if ($allPasses[$i]['Package']['is_guest'] == 1) {
						$returnArray[$i]['Pass']['isGuest']="Yes";
						$returnArray[$i]['Pass']['packageCost']="NA";
						$passTotalCost=$allPasses[$i]['Pass']['deposit'] + $allPasses[$i]['Pass']['cost_1st_year'];
						$totalAmount = $totalAmount + $passTotalCost;
				}else{
					 $returnArray[$i]['Pass']['isGuest']="No";
					 $packageNameDetail='$ '.$allPasses[$i]['Package']['cost'];
						  if($allPasses[$i]['Package']['is_fixed_duration']==0){
							 if($allPasses[$i]['Package']['is_recurring'] == 1){
								if($allPasses[$i]['Package']['is_guest'] == 0){
									if($allPasses[$i]['Package']['cost']>0){
										$packageNameDetail=$packageNameDetail.'<br> <font color="red">Recurring Payment*</font><br>
																				Interval : '.$allPasses[$i]['Package']['duration'].' '.$allPasses[$i]['Package']['duration_type'];
									}
								}
							 }
						  }
					 $returnArray[$i]['Pass']['packageCost']=$packageNameDetail;
					 $passTotalCost=$allPasses[$i]['Pass']['deposit'] + $allPasses[$i]['Pass']['cost_1st_year'] + $allPasses[$i]['Package']['cost'];
					 $totalAmount = $totalAmount + $passTotalCost;
				}
			}else{
					$returnArray[$i]['Pass']['packageName']= "No Package";
					$returnArray[$i]['Pass']['packageCost']="NA";
					$returnArray[$i]['Pass']['required']="No";
					$returnArray[$i]['Pass']['isGuest']="No";
					$passTotalCost=$allPasses[$i]['Pass']['deposit'] + $allPasses[$i]['Pass']['cost_1st_year'];
					$totalAmount = $totalAmount +$passTotalCost;
			}
			$returnArray[$i]['Pass']['passDeposit']=$allPasses[$i]['Pass']['deposit'];
			$returnArray[$i]['Pass']['cost']=$allPasses[$i]['Pass']['cost_1st_year'];
			$dt = new DateTime();
            $currentDateTime = $dt->format('Y-m-d H:i:s');
			if ($allPasses[$i]['Pass']['is_fixed_duration'] == 0) {
                        switch ($allPasses[$i]['Pass']['duration_type']) {
                            case "Hour":
                                $date = new DateTime($currentDateTime);
                                $k = (int) $allPasses[$i]['Pass']['duration'];
                                $date->add(new DateInterval('PT' . $k . 'H'));
                                $allPasses[$i]['Pass']['expiration_date'] = date_format($date, 'Y-m-d H:i:s');
                                break;
                            case "Day":
                                $date = new DateTime($currentDateTime);
                                $k = $allPasses[$i]['Pass']['duration'];
                                $date->add(new DateInterval('P' . $k . 'D'));
                                $allPasses[$i]['Pass']['expiration_date'] = date_format($date, 'Y-m-d H:i:s');
                                break;
                            case "Week":
                                $date = new DateTime($currentDateTime);
                                $k = (int) $allPasses[$i]['Pass']['duration'] * 7;
                                $date->add(new DateInterval('P' . $k . 'D'));
                                $allPasses[$i]['Pass']['expiration_date'] = date_format($date, 'Y-m-d H:i:s');
                                break;
                            case "Month":
                                $date = new DateTime($currentDateTime);
                                $k = (int) $allPasses[$i]['Pass']['duration'];
                                $date->add(new DateInterval('P' . $k . 'M'));
                                $allPasses[$i]['Pass']['expiration_date'] = date_format($date, 'Y-m-d H:i:s');
                                break;
                            case "Year":
                                $date = new DateTime($currentDateTime);
                                $k = (int) $allPasses[$i]['Pass']['duration'];
                                $date->add(new DateInterval('P' . $k . 'Y'));
                                $allPasses[$i]['Pass']['expiration_date'] = date_format($date, 'Y-m-d H:i:s');
                                break;
                        }
              }
              $date1 = new DateTime($allPasses[$i]['Pass']['expiration_date']);
              $datePass=$date1->format('Y-m-d');
              $allPasses[$i]['Pass']['expiration_date']= $datePass.' 23:59:59';
              $expiryDate= date("m/d/Y",strtotime( $allPasses[$i]['Pass']['expiration_date']));
              $returnArray[$i]['Pass']['expiryDate']=$expiryDate;
              $returnArray[$i]['Pass']['passCost']=$passTotalCost;
              
        }
        if($this->request->is('Post')){
			$this->loadModel('CustomerPass');
            if ($this->CustomerPass->hasAny(array('user_id' => $this->Auth->user('id')))) {
				  $this->Session->setFlash('You have already purchased passes', 'warning');
				  $this->Session->delete('other_passes');
				  $this->Session->delete('guest_pass');
				  $this->Session->write('Allowed', true);
				  return $this->redirect(array('controller' => 'CustomerPasses', 'action' => 'view'));
            }
            $netAmountPayable=$totalAmount;
            if(isset($this->request->data['Transaction']['coupon'])){
				if(!empty($this->request->data['Transaction']['coupon'])){
					$this->loadModel('TableCoupon');
					$coupon=$this->TableCoupon->find('first',array('conditions'=>array('coupon_code'=>$this->request->data['Transaction']['coupon'],'property_id'=>$_SESSION['PropertyId'])));
					if($coupon){
						if($coupon['TableCoupon']['valid']==1){
							$dt = new DateTime();
							$currentDateTime= $dt->format('Y-m-d H:i:s');
							if($coupon['TableCoupon']['start_date']){
								if($coupon['TableCoupon']['start_date']<$currentDateTime){
									if($coupon['TableCoupon']['expiration_date']){
										if($coupon['TableCoupon']['expiration_date']>$currentDateTime){
											$newtotalAmount=$totalAmount-(($coupon['TableCoupon']['discount']/100)*$totalAmount);
										}
									}else{
										$newtotalAmount=$totalAmount-(($coupon['TableCoupon']['discount']/100)*$totalAmount);
									}
								}
							}else{
								if($coupon['TableCoupon']['expiration_date']){
									if($coupon['TableCoupon']['expiration_date']>$currentDateTime){
										$newtotalAmount=$totalAmount-(($coupon['TableCoupon']['discount']/100)*$totalAmount);
									}
								}else{	
									$newtotalAmount=$totalAmount-(($coupon['TableCoupon']['discount']/100)*$totalAmount);
								}
							}
							CakeLog::write('firstCouponApplied', AuthComponent::user('username').' : First Coupon Applied, COUPON CODE: '.$this->request->data['Transaction']['coupon'].', TOTAL AMOUNT: '.$totalAmount.', DISCOUNT: '.(($coupon['TableCoupon']['discount']/100)*$totalAmount).', NEW AMOUNT: '.$newtotalAmount);
							$newtotalAmount=number_format($newtotalAmount,2);
							$totalAmount=$newtotalAmount;
						}
					}
				}
			}
			//debug($totalAmount);die;
            if ($totalAmount == 0) {
                $this->Transaction->updateValidations();
            }
            $this->Transaction->set($this->request->data);
            if ($this->Transaction->validates()) {
				 $errorMessage = null;
                $response = null;
                if ($totalAmount > 0) {
                    $payment = array(
                        'amount' => $totalAmount,
                        'card' => $this->request->data['Transaction']['card_number'], 
                        'expiry' => array(
                            'M' => $this->request->data['Transaction']['month'],
                            'Y' => $this->request->data['Transaction']['year'],
                        ),
                        'cvv' => $this->request->data['Transaction']['cvv'],
                        'currency' => 'USD' 
                    );

                    try {
                         $response = $this->Paypal->doDirectPayment($payment);
                    } catch (Exception $e) {
                        $errorMessage = $e->getMessage();
                    }
                } else {
                    $response['TIMESTAMP'] = date('Y-m-d H:i:s');
                    $response['TRANSACTIONID'] = 'No Payment';
                    $response['ACK'] = 'Success';
                    $response['AMT'] = '0';
                    $response['PassDetail'] = $allPasses;

                    $this->request->data['Transaction']['first_name_cc'] = $this->Auth->user('first_name');
                    $this->request->data['Transaction']['last_name_cc'] = $this->Auth->user('last_name');
                }
                 if ($errorMessage == null) {
					if ($response['ACK'] == 'Success') {
						$passArray = ' ';
						$passName = ' ';
						
                        for ($i = 0; $i < count($allPasses); $i++) {
                            $passArray = $passArray . $allPasses[$i]['Pass']['id'];
                            $passName = $passName . $allPasses[$i]['Pass']['name'];
                            if ($i < (count($allPasses) - 1)) {
								$passArray = $passArray .',';
								$passName = $passName .',';
							}
                        }
                        $passArray=trim($passArray);
                        $passName=trim($passName);
                        $dateTransaction = date('Y-m-d H:i:s', strtotime($response['TIMESTAMP']));
                        $arraySerialize = serialize($response);
                        $transactionArray['Transaction']['paypal_tranc_id'] = $response['TRANSACTIONID'];
                        $transactionArray['Transaction']['user_id'] = $this->Auth->user('id');
                        $transactionArray['Transaction']['date_time'] = $dateTransaction;
                        $transactionArray['Transaction']['amount'] = $response['AMT'];
                        $transactionArray['Transaction']['result'] = $response['ACK'];
                        $transactionArray['Transaction']['pass_id'] = $passArray;
                        $transactionArray['Transaction']['transaction_array'] = $arraySerialize;
                        $transactionArray['Transaction']['first_name'] = $this->request->data['Transaction']['first_name_cc'];
                        $transactionArray['Transaction']['last_name'] = $this->request->data['Transaction']['last_name_cc'];
                       //$transactionArray['Transaction']['comments'] = "passPurchased";		
                        $this->Transaction->create();          	
                        if ($this->Transaction->save($transactionArray, false)) {
							 $transaction = $this->Transaction->getLastInsertId();
							 $this->loadModel('CustomerPass');
                             $cstPass = new CustomerPass;
                             for ($i = 0; $i < count($allPasses); $i++) {
                                $customerPassArray[$i]['user_id'] = $this->Auth->user('id');
                                $customerPassArray[$i]['property_id'] = $this->Session->read('PropertyId');
                                $customerPassArray[$i]['transaction_id'] = $transaction;
                                $customerPassArray[$i]['pass_id'] = $allPasses[$i]['Pass']['id'];
                                $customerPassArray[$i]['pass_cost'] = $allPasses[$i]['Pass']['cost_1st_year'];
                                $customerPassArray[$i]['pass_deposit'] = $allPasses[$i]['Pass']['deposit'];
                                if(isset($allPasses[$i]['Package'])){
										 if ($allPasses[$i]['Package']['is_guest'] == 0) {
													$customerPassArray[$i]['package_id'] = $allPasses[$i]['Package']['id'];
													$customerPassArray[$i]['package_cost'] = $allPasses[$i]['Package']['cost'];
													$customerPassArray[$i]['membership_vaild_upto'] = $allPasses[$i]['Package']['expiration_date'];
										 }elseif($allPasses[$i]['Package']['is_guest']==1){
												$customerPassArray[$i]['is_guest_pass']=1;
										 }
								}
								//RECURRING CHECK START
								if(Configure::read('RECURRING')){
									if($allPasses[$i]['Pass']['is_fixed_duration'] == 0){
										  if(isset($allPasses[$i]['Package'])){
											  if($allPasses[$i]['Package']['is_fixed_duration']==0){
												  if($allPasses[$i]['Package']['is_recurring'] == 1){
													 if($allPasses[$i]['Package']['is_guest'] == 0){
														if($allPasses[$i]['Package']['cost']>0){
															$this->loadModel('RecurringProfile');
															$recurAmount=$allPasses[$i]['Package']['cost'];
															$recurringPayment = array(
																'amount' => $recurAmount,
																'card' => $this->request->data['Transaction']['card_number'], 
																'expiry' => array(
																		'M' => $this->request->data['Transaction']['month'],
																		'Y' => $this->request->data['Transaction']['year'],
																	),
																'cvv' => $this->request->data['Transaction']['cvv'],
																'currency' => 'USD' ,
																'BILLINGPERIOD'=>$allPasses[$i]['Package']['duration_type'],
																'BILLINGFREQUENCY'=>$allPasses[$i]['Package']['duration'],
																'PROFILESTARTDATE'=>$allPasses[$i]['Package']['expiration_date'],
																'FIRSTNAME' => AuthComponent::user('first_name'),
																'LASTNAME' => AuthComponent::user('last_name'), 
																'EMAIL'=>AuthComponent::user('email'),
																'STREET' => AuthComponent::user('address_line_1'),			
																'CITY' => AuthComponent::user('city'),				
																'STATE' => AuthComponent::user('state'),						
																'ZIP' => AuthComponent::user('zip')
															);
															$errorMessage=null;
															try {
																$recurringResponse = $this->PayFlow->createRecurringProfilePayFlow($recurringPayment);
															}catch (Exception $e) {
																$errorMessage=$e->getMessage();
															}
															if($errorMessage==null){
																//debug($recurringResponse);die;
																if(isset($recurringResponse['RESULT']) && $recurringResponse['RESULT'] == 0) {
																	if($recurringResponse['RESPMSG']=='Approved'){
																		$recurringResponse['PROFILESTATUS']='Active';
																		$array['RecurringProfile']=array(
																									 'recurring_profile_id'=>$recurringResponse['PROFILEID'],
																									 'message'=>'Recurring Profile created',
																									 'status'=>$recurringResponse['PROFILESTATUS']
																		);
																		$this->RecurringProfile->create();
																		if($this->RecurringProfile->save($array)){
																				CakeLog::write('pofileDataSaved','Profile Data Saved for ID: '.$recurringResponse['PROFILEID']);
																		}else{
																				CakeLog::write('pofileDataNotSaved','Profile Data Not Saved for ID: '.$recurringResponse['PROFILEID']);
																		}
																		CakeLog::write('recurringProfileCreated', ''.AuthComponent::user('username').' : Recurring Profile created by User: '.AuthComponent::user('first_name').' '.AuthComponent::user('last_name').
																			' In Property: '.CakeSession::read('PropertyName').', for Pass Name : '.$allPasses[$i]['Pass']['name'].
																			' Profile Id : '.$recurringResponse['PROFILEID'].
																			' Profile Status : '.$recurringResponse['PROFILESTATUS'].''); 
																		$customerPassArray[$i]['recurring_profile_id']=$recurringResponse['PROFILEID'];
																		$customerPassArray[$i]['recurring_profile_status']=$recurringResponse['PROFILESTATUS'];
																	}
																}else{
																	CakeLog::write('recurringProfileCreationFailed', ''.AuthComponent::user('username').' : Recurring Profile creation failed User: '.AuthComponent::user('first_name').' '.AuthComponent::user('last_name').
																		' In Property: '.CakeSession::read('PropertyName').', for Pass Name : '.$allPasses[$i]['Pass']['name'].''); 
																}
															}else{
																	CakeLog::write('recurringProfileCreationException', ''.AuthComponent::user('username').' : Recurring Profile creation failed User: '.AuthComponent::user('first_name').' '.AuthComponent::user('last_name').
																			' In Property: '.CakeSession::read('PropertyName').', for Pass Name : '.$allPasses[$i]['Pass']['name'].' Exception: '.$errorMessage.''); 
															}
														}
													 }
											
													}
												}
											
											 }
										}
								}
								//RECURRING CHECK END
                                $customerPassArray[$i]['pass_valid_upto'] = $allPasses[$i]['Pass']['expiration_date'];
                                
                             }
                            // debug($customerPassArray);die;										
                            $cstPass->create();
                            if ($cstPass->saveAll($customerPassArray, array('validate' => false))) {
								    CakeLog::write('selectedPassesPurchased', ''.AuthComponent::user('username').' : Passes Purchased by User: '.AuthComponent::user('first_name').' '.AuthComponent::user('last_name').' In Property: '.CakeSession::read('PropertyName').', Passes Id : '.$passArray.' Passes Name : '.$passName.' Amount Paid: $'.$response['AMT'].', Package Coupon Used : '.CakeSession::read('CouponCode').'');
									$billAddressArray['BillingAddress']['user_id'] = $this->Auth->user('id');
									$billAddressArray['BillingAddress']['email'] = $this->request->data['Transaction']['email'];
									$billAddressArray['BillingAddress']['first_name'] = $this->request->data['Transaction']['first_name'];
									$billAddressArray['BillingAddress']['last_name'] = $this->request->data['Transaction']['last_name'];
									$billAddressArray['BillingAddress']['address_line_1'] = $this->request->data['Transaction']['address_line_1'];
									$billAddressArray['BillingAddress']['address_line_2'] = $this->request->data['Transaction']['address_line_2'];
									$billAddressArray['BillingAddress']['city'] = $this->request->data['Transaction']['city'];
									$billAddressArray['BillingAddress']['state'] = $this->request->data['Transaction']['state'];
									$billAddressArray['BillingAddress']['zip'] = $this->request->data['Transaction']['zip'];
									$billAddressArray['BillingAddress']['phone'] = $this->request->data['Transaction']['phone'];

									$this->loadModel('BillingAddress');
									$billAddress = new BillingAddress();
									$billAddress->create();
									if ($billAddress->save($billAddressArray, false)) {
										$this->Session->setFlash('Your transaction has been completed succesfully, Add vehicles now', 'success');
										$this->Session->delete('Allowed');
										$this->Session->delete('other_passes');
										$this->Session->delete('guest_pass');
										$this->Session->write('Allowed', true);
										$this->redirect(array('controller'=>'Vehicles','action'=>'add_vehicles'));	
										//return $this->redirect(array('controller' => 'CustomerPasses', 'action' => 'view'));
									}else {
										$this->Session->setFlash('Billing Address Not Saved', 'error');
										return $this->redirect(array('controller' => 'CustomerPasses', 'action' => 'view'));
									}
						    }else {
                                $this->Session->setFlash('Pass not saved', 'error');
                            }
					    }else {
                            $this->Session->setFlash('The transaction could not be saved. Contact Admin', 'error');
                        }
					}else {
                        $this->Session->setFlash('Transaction Failed', 'error');
                    }
					 
				 }else {
                    $this->Session->setFlash($errorMessage, 'error');
                }
		    }else {
				$totalAmount=$netAmountPayable;
                $this->Session->setFlash('Validation Failure', 'error');
            }
		}
        $this->loadModel('State');
        $state = new State();
        $states = $state->find('list');
        $this->set(compact('totalAmount','returnArray','states'));
   }
    /********************************************
  * New All properties guest pass cost in time frame valid from 5th December,2015 
  */
  public function admin_new_guest_pass_costs(){
  
  }
   public function admin_new_guest_pass_cost_listing(){
			$this->autoRender=false;
			
			$toDate=date('Y-m-d',strtotime($this->request->query['toDate']));
			$fromDate=date('Y-m-d',strtotime($this->request->query['fromDate']));
			$conditions='';
			$conditionsCount='';
			if($toDate==$fromDate){
				$toDate=$fromDate.' 23:59:59';
			}else{
				$toDate=$toDate.' 23:59:59';
			}
			$conditions= " created BETWEEN '".$fromDate." 00:00:00' AND '".$toDate."' ";
			$this->loadModel('Property');
			$this->loadModel('UserGuestPass');
			$this->Property->recursive=-1;
			$allProperty=$this->Property->find('all',array('fields'=>array('id','name'),'order'=>array('id'=>'ASC')));
			$totalPaid=$totalDays=$totalFreedays=0;
			$this->UserGuestPass->recursive=-1;
			$data=$this->UserGuestPass->find('all',array('conditions'=>array($conditions),'fields'=>array('sum(days) AS days','sum(paid) AS paid','sum(free) AS free','sum(amount) AS amount','property_id'),'group'=>'property_id','order'=>array('property_id'=>'ASC')));
			
			for($i=0;$i<count($allProperty);$i++){
				for($j=0;$j<count($data);$j++){
					if($data[$j]['UserGuestPass']['property_id']==$allProperty[$i]['Property']['id']){
						$allProperty[$i]['Property']['days']=$data[$j][0]['days'];
						$totalDays=$totalDays+$data[$j][0]['days'];
						$allProperty[$i]['Property']['paidDays']=$data[$j][0]['paid'];
						$totalPaid=$totalPaid+$data[$j][0]['paid'];
						$allProperty[$i]['Property']['freeDays']=$data[$j][0]['days']-$data[$j][0]['paid'];
						$totalFreedays=$totalFreedays+($data[$j][0]['days']-$data[$j][0]['paid']);
						$allProperty[$i]['Property']['paidAmount']=$data[$j][0]['amount'];
					}
				}
				if(!isset($allProperty[$i]['Property']['days'])){
					$allProperty[$i]['Property']['days']=NULL;
					$allProperty[$i]['Property']['paidDays']=NULL;
					$allProperty[$i]['Property']['freeDays']=NULL;
					$allProperty[$i]['Property']['paidAmount']=NULL;
				}
			}
			
			$totalAmount=$this->UserGuestPass->field('SUM(amount)',array($conditions));
			$allProperty[]['TotalAmount']=$totalAmount;
			$allProperty[]['totalFreeDays']=$totalFreedays;
			$allProperty[]['totalPaidDays']=$totalPaid;
			$allProperty[]['totalDays']=$totalDays;
			
			echo json_encode($allProperty);  
  }
  public function admin_renewal_passes(){
	
  }
    public function admin_get_renewal_passes(){
		$this->layout=false;
		$toDate=date('Y-m-d',strtotime($this->request->query['toDate']));
		$fromDate=date('Y-m-d',strtotime($this->request->query['fromDate']));
		$conditions='';
		$conditionsCount='';
		if($toDate==$fromDate){
			$toDate=$fromDate.' 23:59:59';
		}else{
			$toDate=$toDate.' 23:59:59';
		}
		$conditions= " Transaction.date_time BETWEEN '".$fromDate." 00:00:00' AND '".$toDate."' ";
		$this->loadModel('Property');
		$properties=$this->Property->find('list');
		$propertyList=array_values($properties);
		$propertyIds=array_keys($properties);
		$array=array();
		for($i=0;$i<count($propertyList);$i++){
			$array[]=array(
					 'PropertyName'=>$propertyList[$i],
			);	
		}
		
		$this->Transaction->unbindModel(array('hasMany'=>'CustomerPass'));
		$freeRenewedPass=$this->Transaction->find('all',array('conditions'=>array('Transaction.comments'=>'passRenewed','Transaction.amount'=>0,$conditions),
													'fields'=>array('SUM(Transaction.amount)' ,'count(*) AS TotalPasses','Property.name as PropertyName'),  
													'joins'=>array(
																	array(
																			'table'=>'properties',
																			'alias'=>'Property',
																			'type'=>'LEFT',
																			'conditions'=>array(
																					'Property.id=Pass.property_id'
																			)
																		)
																	
																	),
													'group'=>array('Property.id'),
													'order'=>array('Property.id'=>'ASC')
		));
		
		$this->Transaction->unbindModel(array('hasMany'=>'CustomerPass')); 
		$paidRenewedPass=$this->Transaction->find('all',array('conditions'=>array('Transaction.comments'=>'passRenewed','Transaction.amount!=0',$conditions),
													'fields'=>array('SUM(Transaction.amount)','count(*) AS TotalPasses','Property.name as PropertyName'),  
													'joins'=>array(
																	array(
																			'table'=>'properties',
																			'alias'=>'Property',
																			'type'=>'LEFT',
																			'conditions'=>array(
																					'Property.id=Pass.property_id'
																			)
																		)
																	
																	),
													'group'=>array('Property.id'),
													'order'=>array('Property.id'=>'ASC')
		));
		//debug($paidRenewedPass);die;
		$finalArr=array();
		for($j=0;$j<count($array);$j++){
			$array[$j]['PropertyID']=$propertyIds[$j];
			$array[$j]['FreePassAmount']=0;
			$array[$j]['FreePassCount']=0;
			$array[$j]['PaidPassAmount']=0;
			$array[$j]['PaidPassCount']=0;
			$var=0;
			$counter=0;
			foreach($freeRenewedPass as $freePass){
				 if($freePass['Property']['PropertyName']==$array[$j]['PropertyName']){
					$array[$j]['FreePassAmount']=$freePass[0]['SUM(`Transaction`.`amount`)'];
					$array[$j]['FreePassCount']=$freePass[0]['TotalPasses'];
					//unset($freeRenewedPass[$var]);
				 }
				$var++;
			}
			foreach($paidRenewedPass as $paidPass){
				 if($paidPass['Property']['PropertyName']==$array[$j]['PropertyName']){
					$array[$j]['PaidPassAmount']=$paidPass[0]['SUM(`Transaction`.`amount`)'];
					$array[$j]['PaidPassCount']=$paidPass[0]['TotalPasses'];
					//unset($paidRenewedPass[$counter]);
				 }
				$counter++;
			}
		}
		//debug($array);die;
		$this->set(compact('array','toDate','fromDate'));
	}
	public function admin_get_renewal_passwise(){
		$this->layout=false;
		$toDate=$this->request->query['toDate'];
		$fromDate=$this->request->query['fromDate'];
		$this->loadModel('Pass');
		$conditions= " Transaction.date_time BETWEEN '".$fromDate." 00:00:00' AND '".$toDate."' and Property.id=".$this->request->query['propertyId'];
		$this->Transaction->unbindModel(array('hasMany'=>'CustomerPass')); 
		$paidRenewedPass=$this->Transaction->find('all',array('conditions'=>array('Transaction.comments'=>'passRenewed','Transaction.amount!=0',$conditions),
													'fields'=>array('SUM(Transaction.amount)','count(*) AS TotalPasses','Property.name as PropertyName','Pass.name AS PassName'),  
													'joins'=>array(
																	array(
																			'table'=>'properties',
																			'alias'=>'Property',
																			'type'=>'LEFT',
																			'conditions'=>array(
																					'Property.id=Pass.property_id'
																			)
																		)
																	
																	),
													'group'=>array('Transaction.pass_id'),
													'order'=>array('Property.id'=>'ASC')
		));
		
		$this->Transaction->unbindModel(array('hasMany'=>'CustomerPass'));
		$freeRenewedPass=$this->Transaction->find('all',array('conditions'=>array('Transaction.comments'=>'passRenewed','Transaction.amount'=>0,$conditions),
													'fields'=>array('SUM(Transaction.amount)' ,'count(*) AS TotalPasses','Property.name as PropertyName','Pass.name AS PassName'),  
													'joins'=>array(
																	array(
																			'table'=>'properties',
																			'alias'=>'Property',
																			'type'=>'LEFT',
																			'conditions'=>array(
																					'Property.id=Pass.property_id'
																			)
																		)
																	
																	),
													'group'=>array('Transaction.pass_id'),
													'order'=>array('Property.id'=>'ASC')
		));
		$this->loadModel('Pass');
		$this->Pass->recursive=-1;
		$allPasses=$this->Pass->find('all',array('conditions'=>array('property_id'=>$this->request->query['propertyId'])));
		$finalArr=array();
		for($i=0;$i<count($allPasses);$i++){
			$arr['PassName']=$allPasses[$i]['Pass']['name'];
			$arr['FreePassAmount']=0;
			$arr['FreePassCount']=0;
			$arr['PaidPassAmount']=0;
			$arr['PaidPassCount']=0;
			foreach($freeRenewedPass as $freePass){
					if($freePass['Pass']['PassName']==$allPasses[$i]['Pass']['name']){
						$arr['FreePassAmount']=$freePass[0]['SUM(`Transaction`.`amount`)'];
						$arr['FreePassCount']=$freePass[0]['TotalPasses'];
					}
			}
			foreach($paidRenewedPass as $paidPass){
				 if($paidPass['Pass']['PassName']==$allPasses[$i]['Pass']['name']){
					$arr['PaidPassAmount']=$paidPass[0]['SUM(`Transaction`.`amount`)'];
					$arr['PaidPassCount']=$paidPass[0]['TotalPasses'];
				 }
			}
			$finalArr[]=$arr;
		}
		$this->set(compact('finalArr'));
		
	}
	public function guestCheckOutAjax($customerPassId = null, $passId = null, $permitId = null) {
		$this->layout=false;
        $this->loadModel('CustomerPass');
        if (!$this->CustomerPass->exists($customerPassId)) {
            throw new NotFoundException(__('Invalid Customer Pass'));
        }
        $this->loadModel('Package');
        if (!$this->Package->exists($permitId)) {
            throw new NotFoundException(__('Invalid package'));
        }
        $this->loadModel('Pass');
        if (!$this->Pass->exists($passId)) {
            throw new NotFoundException(__('Invalid pass'));
        }

        $this->loadModel('Property');
        $freeGuestCredits = $this->Property->field('free_guest_credits', array('id' => $this->Session->read('PropertyId')));
        $permitCost = $this->Package->field('cost', array('id' => $permitId));
        $previousUsedCredits = $this->Auth->user('guest_credits');
        $toDate = $this->Session->read('toDate');
        //debug($toDate);die;
        $toDate = date("Y-m-d", strtotime($toDate));
        $dt = new DateTime();
        $currentDateTime = $dt->format('Y-m-d');
        $datetime1 = date_create($toDate);
        $datetime2 = date_create($currentDateTime);
        $interval = date_diff($datetime1, $datetime2);
        $currentCreditUsed = $interval->days;
        $creditPayable=$newCredits = 0;
        $amount = 0;
        if ((int) $previousUsedCredits >= $interval->days) {
            $newCredits = $previousUsedCredits - $interval->days;
            $amount = 0;
        } else {
            $newCredits = 0;
            $creditPayable = $interval->days - $previousUsedCredits;
            $amount = $creditPayable * $permitCost;
        }
        $this->loadModel('BillingAddress');
        $this->BillingAddress->recursive = -1;
        $billingAddress = $this->BillingAddress->find('first', array('conditions' => array('user_id' => $this->Auth->user('id'))));
        $this->loadModel('State');
        $state = new State();
        $states = $state->find('list');
        $this->set(compact('customerPassId','passId','permitId','freeGuestCredits', 'previousUsedCredits', 'billingAddress', 'states', 'permitCost', 'newCredits', 'amount', 'currentCreditUsed', 'creditPayable'));
		//$this->render('guestCheckOut');
    }
    public function processPayment($customerPassId = null, $passId = null, $permitId = null){
		$this->layout=$this->autoRender=false;
		$this->loadModel('CustomerPass');
        if (!$this->CustomerPass->exists($customerPassId)) {
            throw new NotFoundException(__('Invalid Customer Pass'));
        }
        $this->loadModel('Package');
        if (!$this->Package->exists($permitId)) {
            throw new NotFoundException(__('Invalid package'));
        }
        $this->loadModel('Pass');
        if (!$this->Pass->exists($passId)) {
            throw new NotFoundException(__('Invalid pass'));
        }

        $this->loadModel('Property');
        $freeGuestCredits = $this->Property->field('free_guest_credits', array('id' => $this->Session->read('PropertyId')));
        $permitCost = $this->Package->field('cost', array('id' => $permitId));
        $previousUsedCredits = $this->Auth->user('guest_credits');
        $toDate = $this->Session->read('toDate');
        $toDate = date("Y-m-d", strtotime($toDate));
        $dt = new DateTime();
        $currentDateTime = $dt->format('Y-m-d');
        $datetime1 = date_create($toDate);
        $datetime2 = date_create($currentDateTime);
        $interval = date_diff($datetime1, $datetime2);
        $currentCreditUsed = $interval->days;
        $creditPayable=$newCredits = 0;
        $amount = 0;
        if ((int) $previousUsedCredits >= $interval->days) {
            $newCredits = $previousUsedCredits - $interval->days;
            $amount = 0;
        } else {
            $newCredits = 0;
            $creditPayable = $interval->days - $previousUsedCredits;
            $amount = $creditPayable * $permitCost;
        }
		$response=array(
						'success'=>true,
						'message'=>'',
				);	
        if ($this->request->is('post')|| $this->request->is('put')) {
			//debug($this->request);die;
            $this->Transaction->set($this->request->data);
            if ($this->Transaction->validates()) {
				$vehicleToSet=$vehicleID='';
				$isVehicleOk=false;
				$addNewVehicle=false;
				$vehicleValidation=false;
				$assignedLocation=$this->request->query['location'];
				$this->loadModel('Vehicle');
				if(!$this->request->query['vehicle_id']){
					 $vehicleToSet['Vehicle']=array('owner'=>$this->request->query['owner'],
										  'make'=>$this->request->query['make'],
										  'model'=>$this->request->query['model'],
										  'color'=>$this->request->query['color'],
										  'license_plate_number'=>$this->request->query['platenum'],
										  'license_plate_state'=>$this->request->query['state'],
										  'last_4_digital_of_vin'=>$this->request->query['vin'],
										  'user_id'=>$this->Auth->user('id'),
										  'property_id'=>$this->Session->read('PropertyId')
											);
			
					$this->Vehicle->set($vehicleToSet);
					if($this->Vehicle->validates()){
						$isVehicleOk=true;
						$addNewVehicle=true;
						$vehicleValidation=true;
					}else{
						$errors=$this->Vehicle->invalidFields();
						$finalError=array();
						foreach($errors as $field=>$error){
							$finalError[$field]=implode(", ",array_unique($error));
						}
						$finalError=array_unique(array_values($finalError));
						$finalError=implode(",<br> ",$finalError);
						$response['success'] = false;
						$response['message'] = $finalError;
					}
				}else{
					$vehicleID=$this->request->query['vehicle_id'];
					$isVehicleOk=true;
					$vehicleValidation=true;
				}	
				if($vehicleValidation){
						$date = new DateTime();
						$date->add(new DateInterval('P' . $currentCreditUsed . 'D'));
						$permitExpiryDate = date_format($date, 'Y-m-d H:i:s');


						$payment = array(
							'amount' => (int) $amount,
							'card' => $this->request->data['Transaction']['card_number'], // This is a sandbox CC
							'expiry' => array(
								'M' => $this->request->data['Transaction']['month'],
								'Y' => $this->request->data['Transaction']['year'],
							),
							'cvv' => $this->request->data['Transaction']['cvv'],
							'currency' => 'USD' // Defaults to GBP if not provided
						);

						$errorMessage = null;
						$response = null;
						try {
							$response = $this->Paypal->doDirectPayment($payment);
							// Check $response[ACK]='Success' OR 'OK'
						} catch (Exception $e) {
							$errorMessage = $e->getMessage();
						}
						if ($errorMessage == null) {
							if ($response['ACK'] == 'Success') {
								$dateTransaction = date('Y-m-d H:i:s', strtotime($response['TIMESTAMP']));
								$arraySerialize = serialize($response);
								$transactionArray['Transaction']['paypal_tranc_id'] = $response['TRANSACTIONID'];
								$transactionArray['Transaction']['user_id'] = $this->Auth->user('id');
								$transactionArray['Transaction']['date_time'] = $dateTransaction;
								$transactionArray['Transaction']['amount'] = $response['AMT'];
								$transactionArray['Transaction']['result'] = $response['ACK'];
								$transactionArray['Transaction']['pass_id'] = $passId;
								$transactionArray['Transaction']['transaction_array'] = $arraySerialize;
								$transactionArray['Transaction']['first_name'] = $this->request->data['Transaction']['first_name_cc'];
								$transactionArray['Transaction']['last_name'] = $this->request->data['Transaction']['last_name_cc'];
								$transactionArray['Transaction']['comments'] = "guestPassActivated";
								$this->Transaction->create();
								if ($this->Transaction->save($transactionArray, false)) {
									$this->loadModel('CustomerPass');
									$this->loadModel('Vehicle');
									if(!$vehicleID){
										$vehicleToSet['customer_pass_id']=$customerPassId;
										if($this->Vehicle->save($vehicleToSet,false)){
											$vehicleID=$this->Vehicle->getLastInsertId();
											CakeLog::write('NewGuestVehicle', ''.AuthComponent::user('username').' : New Guest Vehicle added for Customer Pass ID: <a href="/admin/CustomerPasses/edit/'.$customerPassId.'">'.$customerPassId.'</a> by User : '.AuthComponent::user('first_name').' '.AuthComponent::user('last_name').' In Property '.CakeSession::read('PropertyName').' AND NEW VEHICLE ID : '.$vehicleID);
										}
										
									}
									$this->Vehicle->updateAll(array('Vehicle.customer_pass_id'=>NULL),array('Vehicle.customer_pass_id'=>$customerPassId));
									$this->CustomerPass->updateAll(array('CustomerPass.vehicle_id'=>NULL),array('CustomerPass.vehicle_id'=>$vehicleID));
									$customerPassArray['CustomerPass']['transaction_id'] = $this->Transaction->getLastInsertId();
									$customerPassArray['CustomerPass']['membership_vaild_upto'] = $permitExpiryDate;
									$customerPassArray['CustomerPass']['id'] = $customerPassId;
									$customerPassArray['CustomerPass']['package_id'] = $permitId;
									$customerPassArray['CustomerPass']['is_guest_pass'] = 1;
									$customerPassArray['CustomerPass']['vehicle_id']=$vehicleID;
									if($assignedLocation){
										$customerPassArray['CustomerPass']['assigned_location']=$assignedLocation;
									}
									$this->CustomerPass->create();
									if ($this->CustomerPass->save($customerPassArray, false)) {
											$this->Vehicle->updateAll(array('Vehicle.customer_pass_id'=>$customerPassId),array('Vehicle.id'=>$vehicleID));
											$vehicleInfo=$this->Vehicle->field('vehicle_info',array('id'=>$vehicleID));
											$this->loadModel('UserGuestPass');
											$userPassArray = array('UserGuestPass' => array('user_id' => AuthComponent::user('id'),
													'customer_pass_id' => $customerPassId,
													'vehicle_id'=>$vehicleID,
													'vehicle_details'=>$vehicleInfo,
													'days' => $currentCreditUsed,
													'free' => $currentCreditUsed-$creditPayable, 
													'paid' => $creditPayable,
													'amount' => $amount,
													'to_date' => $permitExpiryDate,
													'property_id' => $this->Session->read('PropertyId')
											));
											$this->UserGuestPass->save($userPassArray, false);
											$userGuestPassId=	$this->UserGuestPass->getLastInsertID();
											$billAddressArray['BillingAddress']['email'] = $this->request->data['Transaction']['email'];
											$billAddressArray['BillingAddress']['first_name'] = $this->request->data['Transaction']['first_name'];
											$billAddressArray['BillingAddress']['last_name'] = $this->request->data['Transaction']['last_name'];
											$billAddressArray['BillingAddress']['address_line_1'] = $this->request->data['Transaction']['address_line_1'];
											$billAddressArray['BillingAddress']['address_line_2'] = $this->request->data['Transaction']['address_line_2'];
											$billAddressArray['BillingAddress']['city'] = $this->request->data['Transaction']['city'];
											$billAddressArray['BillingAddress']['state'] = $this->request->data['Transaction']['state'];
											$billAddressArray['BillingAddress']['zip'] = $this->request->data['Transaction']['zip'];
											$billAddressArray['BillingAddress']['phone'] = $this->request->data['Transaction']['phone'];
											$this->loadModel('BillingAddress');
											$billAddress = new BillingAddress();
											$billAddress->create();
											$rslt = $billAddress->hasAny(array('user_id' => $this->Auth->user('id')));
											if ($rslt == true) {
												$billAddressId = $billAddress->find('first', array('fields' => array('id'), 'conditions' => array('user_id' => $this->Auth->user('id'))));
												$billAddressArray['BillingAddress']['id'] = $billAddressId['BillingAddress']['id'];
											}
											if ($billAddress->save($billAddressArray, false)) {
												$this->loadModel('User');
												$user['User']['id'] = $this->Auth->user('id');
												$user['User']['guest_credits'] = $newCredits;
												$this->User->create();
												if ($this->User->save($user, false)) {
													$_SESSION['Auth']['User']['guest_credits']=$newCredits;
													CakeLog::write('guestPassUpdatedPaid', ''.AuthComponent::user('username').' : Guest Pass Updated, Customer Pass ID: <a href="/admin/CustomerPasses/edit/'.$customerPassId.'">'.$customerPassId.'</a> by User : '.AuthComponent::user('first_name').' '.AuthComponent::user('last_name').' In Property '.CakeSession::read('PropertyName').'For '.$currentCreditUsed.' Days, Amount Paid: $'.$amount.' AND VEHICLE ID : '.$vehicleID. " AND VEHICLE INFO : ".$vehicleInfo);
													$this->Session->setFlash('Your transaction has been completed succesfully', 'success');
													$this->Session->delete('Allowed');
													$this->Session->delete('vehicleID');
													$this->Session->write('Allowed', true);
													$response=array(
														'success'=>true,
														'message'=>'PaymentSuccess.',
														'userguestpass'=>$userGuestPassId
													);
												} else {
													$response=array(
														'success'=>false,
														'message'=>'User Table not updated.',
													);
												}
											} else {
												$response=array(
													'success'=>false,
													'message'=>'Billing Address Not Saved.',
												);
											}
									   
									} else {
										$response=array(
											'success'=>false,
											'message'=>'Pass not saved',
										);
									}
								} else {
									$response=array(
										'success'=>false,
										'message'=>'The transaction could not be saved. Contact Admin',
									);
								}
							} else {
								$response=array(
									'success'=>false,
									'message'=>'Transaction Failed',
								);
							}
						} else {
							$response=array(
								'success'=>false,
								'message'=>$errorMessage,
							);	
						}
				}
            }else{
				$errors=$this->Transaction->invalidFields();
				$finalError=array();
				foreach($errors as $field=>$error){
					$finalError[$field]=implode(", ",array_unique($error));
				}
				$finalError=array_values($finalError);
				$finalError=implode(",<br> ",$finalError);
				$response['success'] = false;
				$response['message'] = $finalError;
			}
        }else {
			$response=array(
				'success'=>false,
				'message'=>'Invalid Request',
			);	
		}
		echo json_encode($response);
	}
	public function admin_renewed(){
		$propertyId=$this->request->query['propertyId'];
		$fromDate=date('Y-m-d',strtotime($this->request->query['from']));
		$toDate=date('Y-m-d',strtotime($this->request->query['to']));
		$toDate=$toDate." 23:59:59"; 
		$aColumns = array(
							'user.first_name', 
							'pass.name',
							'vehicle.owner', 
							'vehicle.license_plate_number', 
							'cp.pass_valid_upto', 
							'cp.membership_vaild_upto',
							'cp.RFID_tag_number',
							'cp.id',
							'cp.user_id',
							'user.last_name',
							'user.username',
							'user.email',
							'user.address_line_1',
							'user.address_line_2',
							'user.city',
							'user.state',
							'user.zip',
							'user.phone' 
						);
        $sIndexColumn = " transactions.id ";
        $sTable = " transactions ";
        $sJoinTable=' INNER JOIN customer_passes cp ON (cp.user_id=transactions.user_id AND cp.pass_id=transactions.pass_id)
					  LEFT JOIN vehicles vehicle ON cp.vehicle_id=vehicle.id
					  INNER JOIN users user ON user.id=cp.user_id
					  INNER JOIN passes pass ON pass.id=transactions.pass_id
					  ';
        $sConditions=' cp.property_id='.$propertyId.' AND transactions.comments="passRenewed" '." AND transactions.date_time BETWEEN '" . $fromDate . "' AND '" . $toDate . "' ";;
       
        $returnArr=$this->NewDatatable->getData(array('columns'=>$aColumns,'index_column'=>$sIndexColumn,'table'=>$sTable,'join'=>$sJoinTable,'conditions'=>$sConditions));
        echo json_encode($returnArr);
        die;  
	}
}
