<?php
App::uses('AppController', 'Controller');
class TableCouponsController extends AppController { 
	public $components = array('Paginator','RequestHandler');
	public function beforeFilter()
    { 
        parent::beforeFilter();
        $this->Security->unlockedActions = array('getCouponDetails');
        //$this->Auth->allow();
    }
	public function getCouponDetails(){
		$this->layout=$this->autoRender=false;
		$couponCode=$this->request->data['code'];
		if($couponCode){
			$coupon=$this->TableCoupon->find('first',array('conditions'=>array('coupon_code'=>$couponCode,'property_id'=>$_SESSION['PropertyId'])));
			if($coupon){
				if($coupon['TableCoupon']['valid']==1){
					$dt = new DateTime();
					$currentDateTime= $dt->format('Y-m-d H:i:s');
					//debug($currentDateTime);die;
					if($coupon['TableCoupon']['start_date']){
						if($coupon['TableCoupon']['start_date']<$currentDateTime){
							if($coupon['TableCoupon']['expiration_date']){
								if($coupon['TableCoupon']['expiration_date']>$currentDateTime){
									echo $coupon['TableCoupon']['discount'];
								}else{
									echo "Coupon Expired 1";
								}
							}else{
								echo $coupon['TableCoupon']['discount'];
							}
						}else{
							echo "Coupon Expired 2";
						}
					}else{
						if($coupon['TableCoupon']['expiration_date']){
							if($coupon['TableCoupon']['expiration_date']>$currentDateTime){
								echo $coupon['TableCoupon']['discount'];
							}else{
								echo "Coupon Expired 3";
							}
						 }else{	
							echo $coupon['TableCoupon']['discount'];
						}
					}
				}else{
					echo "Invalid Coupon";
				}
			}else{
				echo "No Coupon Exist";
			}
		}else{
			echo "false";
		}
	}
	
	function admin_add(){
		if($this->request->is('POST')){
			$this->TableCoupon->create();
			if($this->TableCoupon->save($this->request->data)){
				$this->Session->setFlash('Coupon added successfully.','success');
			}	
		}
		$this->loadModel('Property');
		$property=$this->Property->find('list');
		 $this->paginate = array(
            'limit' => 10,
            'fields'=>array('TableCoupon.*,Property.name')
        );
        $coupons = $this->paginate('TableCoupon');
		$this->set(compact('property','coupons'));
	}
	function admin_edit($id){
		if (!$this->TableCoupon->exists($id)) {
			throw new NotFoundException(__('Invalid coupon'));
		}
		if($this->request->is('PUT')){
			if($this->TableCoupon->save($this->request->data)){
				$this->Session->setFlash('Coupon added successfully.','success');
				return $this->redirect(array('action' => 'add'));
			}
		}
		$this->TableCoupon->recursive=-1;
		$this->request->data=$this->TableCoupon->find('first',array('conditions'=>array('id'=>$id)));
		if($this->request->data['TableCoupon']['start_date']){
			$this->request->data['TableCoupon']['start_date']=date("m/d/Y", strtotime($this->request->data['TableCoupon']['start_date']));
		}
		if($this->request->data['TableCoupon']['expiration_date']){
			$this->request->data['TableCoupon']['expiration_date']=date("m/d/Y", strtotime($this->request->data['TableCoupon']['expiration_date']));
		}
		$this->loadModel('Property');
		$property=$this->Property->find('list');
		$this->set(compact('property'));
	}
	function admin_delete($id){
		$this->TableCoupon->id = $id;
		if (!$this->TableCoupon->exists()) {
			throw new NotFoundException(__('Invalid Coupon'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->TableCoupon->delete()) {
			$this->Session->setFlash('Coupon has been deleted.','success');
		} else {
			$this->Session->setFlash('The coupon could not be deleted. Please, try again.','error');
		}
		return $this->redirect(array('action' => 'add'));
	}
	function admin_activate($id){
		$this->TableCoupon->id = $id;
		if (!$this->TableCoupon->exists()) {
			throw new NotFoundException(__('Invalid Coupon'));
		}
		$this->request->allowMethod('post', 'delete');
		$arr['TableCoupon']=array('id'=>$id,'valid'=>'1');
		if ($this->TableCoupon->save($arr,false)) {
			$this->Session->setFlash('Coupon has been activated successfully.','success');
		} else {
			$this->Session->setFlash('The coupon could not be activated. Please, try again.','error');
		}
		return $this->redirect(array('action' => 'add'));
	}
	function admin_deactivate($id){
		$this->TableCoupon->id = $id;
		if (!$this->TableCoupon->exists()) {
			throw new NotFoundException(__('Invalid Coupon'));
		}
		$arr['TableCoupon']=array('id'=>$id,'valid'=>'0');
		if ($this->TableCoupon->save($arr,false)) {
			$this->Session->setFlash('Coupon has been de-activated successfully.','success');
		} else {
			$this->Session->setFlash('The coupon could not be de-activated. Please, try again.','error');
		}
		return $this->redirect(array('action' => 'add'));
	}
}
