<?php //d5c40513a1789c19eee430b9fff8d1f406187e8e
App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');

/**
 * Users Controller
 */
class UsersController extends AppController { 
/**
 * Components
 */
	public $components = array('Paginator','Email','Acl','DataTable','NewDatatable');
	var $resetUser=null;
	public $helpers=array('Html','Form','Session');
    public function beforeFilter()
    { 
        parent::beforeFilter();
        //$this->Security->validatePost = false;
		//$this->Security->requireSecure();
    }
    function admin_rebuildARO() {
		ini_set('max_execution_time', 0);
		set_time_limit(0);
		 $aro = new Aro();
		// debug($aro);die;
        // Build the groups.
        $groups = $this->User->Role->find('all');
       
        foreach($groups as $group) {
            $aro->create();
            $aro->save(array(
                // 'alias'=>$group['Group']['name'],
                'foreign_key' => $group['Role']['id'],
                'model'=>'Role',
                'parent_id' => null
            ));
        }
		 $aroList=array();
        // Build the users.
        $this->User->recursive=-1;
        $users = $this->User->find('all',array('limit' => 100,'fields'=>array('id','role_id')));
        $i=0;
        foreach($users as $user) {
            $aroList= array(
                // 'alias' => $user['User']['email'],
                'foreign_key' => $user['User']['id'],
                'model' => 'User',
                'parent_id' => $user['User']['role_id']
            );
            $aro->create();
            $aro->save($aroList);
        }
         //$aro->saveAll($aroList);
       
       /* foreach($aroList as $data) {
            $aro->create();
            if($aro->save($data)){
				echo "saved";
			}else{
				echo "not saved";
			}
        }*/
		 debug($aroList);die; 
        echo "AROs rebuilt!";
        exit;
    } 
/*************************************************************
 * ACL 
 */
	public function admin_initDB()
    {
		/*try{
		  curl_init();   
		}catch (Exception $e) {
		  debug($e->getMessage());
		}
		 echo phpinfo ();*/
        $role = $this->User->Role;
      
        //SUPER ADMIN ACCESSIBLE ACTIONS ACL
        $role->id = $role->ROLES['superAdmin'];
        $this->Acl->allow($role, 'controllers');
		//Property Admin Action
		$this->Acl->deny($role, 'controllers/Users/admin_pa_home_page');
		$this->Acl->deny($role, 'controllers/Users/admin_pa_user_details');
		$this->Acl->deny($role, 'controllers/Users/admin_pa_customer_details');
		$this->Acl->deny($role, 'controllers/Users/admin_pa_view_user_details');
		$this->Acl->deny($role, 'controllers/PropertyUsers/admin_pa_reset_property');
		$this->Acl->deny($role, 'controllers/Properties/admin_pa_property_view');
		//Manager Actions
		$this->Acl->deny($role, 'controllers/Users/manager_home_page');
		$this->Acl->deny($role, 'controllers/Users/manager_customer_details');
		$this->Acl->deny($role, 'controllers/Users/manager_view_user_details');
		$this->Acl->deny($role, 'controllers/Users/manager_user_details');
		$this->Acl->deny($role, 'controllers/CustomerPasses/manager_inactive_vehicles');
		$this->Acl->deny($role, 'controllers/CustomerPasses/manager_get_inactive_vehicles');
		$this->Acl->deny($role, 'controllers/CustomerPasses/manager_guest_vehicles');
		$this->Acl->deny($role, 'controllers/CustomerPasses/manager_get_guest_vehicles');
		$this->Acl->deny($role, 'controllers/CustomerPasses/manager_active_guest_vehicles');
		$this->Acl->deny($role, 'controllers/CustomerPasses/manager_get_active_guest_vehicles');
		$this->Acl->deny($role, 'controllers/CustomerPasses/manager_view_expirations_today');
		$this->Acl->deny($role, 'controllers/CustomerPasses/manager_view_expirations_last');
		$this->Acl->deny($role, 'controllers/CustomerPasses/manager_view_expirations_next');
		$this->Acl->deny($role, 'controllers/Vehicles/manager_all_vehicles');
		$this->Acl->deny($role, 'controllers/Vehicles/manager_get_all_vehicle_details');
		$this->Acl->deny($role, 'controllers/Vehicles/manager_other_vehicles');
		$this->Acl->deny($role, 'controllers/Vehicles/manager_get_other_vehicle_details');
		$this->Acl->deny($role, 'controllers/Properties/manager_property_view');
        //Customer Actions
        $this->Acl->deny($role, 'controllers/Users/customerHomePage');
		$this->Acl->deny($role, 'controllers/Users/myAccount');
		$this->Acl->deny($role, 'controllers/Users/getSubDomain');
		$this->Acl->deny($role, 'controllers/CustomerPasses/view');
		$this->Acl->deny($role, 'controllers/CustomerPasses/renewPass');
		$this->Acl->deny($role, 'controllers/CustomerPasses/getCustomerPassDetails');
		$this->Acl->deny($role, 'controllers/CustomerPasses/guestPass');
		$this->Acl->deny($role, 'controllers/Packages/index');
		$this->Acl->deny($role, 'controllers/Packages/getCurrentUserPassStatus');
		$this->Acl->deny($role, 'controllers/Packages/getCorrespondingPackage');
		$this->Acl->deny($role, 'controllers/Packages/getPackageNamePassBought');
		$this->Acl->deny($role, 'controllers/Packages/remainingPassPackageName');
		$this->Acl->deny($role, 'controllers/Packages/getPackageName');
		$this->Acl->deny($role, 'controllers/Passes/customer_buy_pass');
		$this->Acl->deny($role, 'controllers/Passes/getDeposit');
		$this->Acl->deny($role, 'controllers/Passes/getCost');
		$this->Acl->deny($role, 'controllers/Passes/getDuration');
		$this->Acl->deny($role, 'controllers/Passes/getPassPermit');
		$this->Acl->deny($role, 'controllers/Passes/getPassName');
		$this->Acl->deny($role, 'controllers/Transactions/check_out');
		$this->Acl->deny($role, 'controllers/Transactions/compulsoryPasses');
		$this->Acl->deny($role, 'controllers/Transactions/buyRemainingPass');
		$this->Acl->deny($role, 'controllers/Transactions/guestCheckOut');
		$this->Acl->deny($role, 'controllers/Transactions/passRenew');
		$this->Acl->deny($role, 'controllers/Transactions/activatePass');
		$this->Acl->deny($role, 'controllers/Vehicles/add');
		$this->Acl->deny($role, 'controllers/Vehicles/getVehiclePlateNumber');
		$this->Acl->deny($role, 'controllers/Vehicles/setGuestPackageOptions');
		$this->Acl->deny($role, 'controllers/Vehicles/addSelectedVehicle');
		$this->Acl->deny($role, 'controllers/Vehicles/edit');
		$this->Acl->deny($role, 'controllers/Vehicles/index');
		$this->Acl->deny($role, 'controllers/Chats/contact_us');
		$this->Acl->deny($role, 'controllers/Chats/add');
       
        //PROPERTY ADMIN ACCESSIBLE ACTIONS ACL
        $role->id = $role->ROLES['propertyAdmin'];
        $this->Acl->deny($role, 'controllers');
        $this->Acl->allow($role, 'controllers/Users/admin_pa_home_page');
        $this->Acl->allow($role, 'controllers/Users/admin_logout');
        $this->Acl->allow($role, 'controllers/Users/admin_myAccount');
        $this->Acl->allow($role, 'controllers/Users/admin_pa_user_details');
        $this->Acl->allow($role, 'controllers/Users/admin_pa_customer_details');
        $this->Acl->allow($role, 'controllers/Users/admin_pa_view_user_details');
        $this->Acl->allow($role, 'controllers/Users/admin_print_details');
        $this->Acl->allow($role, 'controllers/PropertyUsers/admin_pa_reset_property');
        $this->Acl->allow($role, 'controllers/Properties/admin_pa_property_view');
        $this->Acl->allow($role, 'controllers/Passes/admin_getPassName');
        $this->Acl->allow($role, 'controllers/Passes/admin_getPName');
        $this->Acl->allow($role, 'controllers/Packages/admin_getPackageNamePassBought');
        $this->Acl->allow($role, 'controllers/Packages/admin_getPackageName');
        $this->Acl->allow($role, 'controllers/Vehicles/admin_getVehiclePlateNumber');
        $this->Acl->allow($role, 'controllers/Vehicles/admin_all_vehicles');
        $this->Acl->allow($role, 'controllers/Vehicles/admin_get_all_vehicle_details');
        $this->Acl->allow($role, 'controllers/Vehicles/admin_other_vehicles');
        $this->Acl->allow($role, 'controllers/Vehicles/admin_get_other_vehicle_details');
        $this->Acl->allow($role, 'controllers/Vehicles/admin_pa_search');
        $this->Acl->allow($role, 'controllers/Transactions/admin_get_transactions');
        $this->Acl->allow($role, 'controllers/CustomerPasses/admin_active_vehicles');
        $this->Acl->allow($role, 'controllers/CustomerPasses/admin_get_active_vehicles');
        $this->Acl->allow($role, 'controllers/CustomerPasses/admin_inactive_vehicles');
        $this->Acl->allow($role, 'controllers/CustomerPasses/admin_get_inactive_vehicles');
        $this->Acl->allow($role, 'controllers/CustomerPasses/admin_guest_vehicles');
        $this->Acl->allow($role, 'controllers/CustomerPasses/admin_get_guest_vehicles');
        $this->Acl->allow($role, 'controllers/CustomerPasses/admin_active_guest_vehicles');
        $this->Acl->allow($role, 'controllers/CustomerPasses/admin_get_active_guest_vehicles');
        $this->Acl->allow($role, 'controllers/CustomerPasses/admin_view_expirations_today');
		$this->Acl->allow($role, 'controllers/CustomerPasses/admin_view_expirations_last');
		$this->Acl->allow($role, 'controllers/CustomerPasses/admin_view_expirations_next');
		$this->Acl->allow($role, 'controllers/CustomerPasses/admin_view_passes_created');
		$this->Acl->allow($role, 'controllers/Tickets/admin_add');
		$this->Acl->allow($role, 'controllers/Tickets/admin_contact_super_admin');
		$this->Acl->allow($role, 'controllers/Tickets/admin_contact');
        $this->Acl->allow($role, 'controllers/Tickets/admin_view');
        $this->Acl->allow($role, 'controllers/TicketReplies/admin_view_single');
        $this->Acl->allow($role, 'controllers/TicketReplies/admin_add');
        $this->Acl->allow($role, 'controllers/ChangeVehicleDetails/admin_change');
        $this->Acl->allow($role, 'controllers/ChangeVehicleDetails/admin_done');
        $this->Acl->allow($role, 'controllers/ChangeVehicleDetails/admin_view_change');
        $this->Acl->allow($role, 'controllers/Vehicles/admin_list_vehicles_pa');
        $this->Acl->allow($role, 'controllers/Vehicles/admin_get_property_vehicle_details');
        $this->Acl->allow($role, 'controllers/Users/admin_property_wise_users_pa');
        $this->Acl->allow($role, 'controllers/Users/admin_get_customers');
        $this->Acl->allow($role, 'controllers/CustomerPasses/admin_property_wise_pass_details_pa');
        $this->Acl->allow($role, 'controllers/Transactions/admin_pass_costs_pa');
        $this->Acl->allow($role, 'controllers/CustomerPasses/admin_last_seen');
        $this->Acl->allow($role, 'controllers/Tickets/admin_get_tickets_user');
        $this->Acl->allow($role, 'controllers/Tickets/admin_send_email_bulk');
        $this->Acl->allow($role, 'controllers/CustomerPasses/admin_no_vehicle');
        $this->Acl->allow($role, 'controllers/CustomerPasses/admin_get_no_vehicle');
        $this->Acl->allow($role, 'controllers/PropertyUsers/admin_create_csv');
		
        //PROPERTY MANAGER ACCESSIBLE ACTIONS ACL
        $role->id = $role->ROLES['propertyManager'];
        $this->Acl->deny($role, 'controllers');
		$this->Acl->allow($role, 'controllers/Users/manager_home_page');
		$this->Acl->allow($role, 'controllers/Users/myAccount');
		$this->Acl->allow($role, 'controllers/Users/getSubDomain');
		$this->Acl->allow($role, 'controllers/Users/admin_logout');
		$this->Acl->allow($role, 'controllers/Users/manager_customer_details');
		$this->Acl->allow($role, 'controllers/Users/manager_view_user_details');
		$this->Acl->allow($role, 'controllers/Users/manager_user_details');
		$this->Acl->allow($role, 'controllers/Users/manager_print_details');
		$this->Acl->allow($role, 'controllers/CustomerPasses/manager_active_vehicles');
		$this->Acl->allow($role, 'controllers/CustomerPasses/manager_get_active_vehicles');
		$this->Acl->allow($role, 'controllers/CustomerPasses/manager_inactive_vehicles');
		$this->Acl->allow($role, 'controllers/CustomerPasses/manager_get_inactive_vehicles');
		$this->Acl->allow($role, 'controllers/CustomerPasses/manager_guest_vehicles');
		$this->Acl->allow($role, 'controllers/CustomerPasses/manager_get_guest_vehicles');
		$this->Acl->allow($role, 'controllers/CustomerPasses/manager_active_guest_vehicles');
		$this->Acl->allow($role, 'controllers/CustomerPasses/manager_get_active_guest_vehicles');
		$this->Acl->allow($role, 'controllers/Vehicles/manager_all_vehicles');
		$this->Acl->allow($role, 'controllers/Vehicles/manager_get_all_vehicle_details');
		$this->Acl->allow($role, 'controllers/Vehicles/manager_other_vehicles');
		$this->Acl->allow($role, 'controllers/Vehicles/manager_get_other_vehicle_details');
		$this->Acl->allow($role, 'controllers/Vehicles/getVehiclePlateNumber');
		$this->Acl->allow($role, 'controllers/Vehicles/manager_search');
		$this->Acl->allow($role, 'controllers/Properties/manager_property_view');
		$this->Acl->allow($role, 'controllers/Passes/getPassName');
		$this->Acl->allow($role, 'controllers/Packages/getPackageNamePassBought');
		$this->Acl->allow($role, 'controllers/Transactions/get_transactions');
		$this->Acl->allow($role, 'controllers/CustomerPasses/manager_view_expirations_today');
		$this->Acl->allow($role, 'controllers/CustomerPasses/manager_view_expirations_last');
		$this->Acl->allow($role, 'controllers/CustomerPasses/manager_view_expirations_next');
		$this->Acl->allow($role, 'controllers/CustomerPasses/manager_property_wise_passes');
		$this->Acl->allow($role, 'controllers/Tickets/manager_view');
		$this->Acl->allow($role, 'controllers/Tickets/manager_contact');
		$this->Acl->allow($role, 'controllers/Tickets/manager_contact_super_admin');
        $this->Acl->allow($role, 'controllers/TicketReplies/manager_view_single');
        $this->Acl->allow($role, 'controllers/TicketReplies/manager_add');
        $this->Acl->allow($role, 'controllers/ChangeVehicleDetails/manager_change');
        $this->Acl->allow($role, 'controllers/ChangeVehicleDetails/manager_done');
        $this->Acl->allow($role, 'controllers/ChangeVehicleDetails/manager_view_change');
        $this->Acl->allow($role, 'controllers/Tickets/admin_get_tickets_user');
        $this->Acl->allow($role, 'controllers/Tickets/admin_send_email_bulk');
		$this->Acl->allow($role, 'controllers/CustomerPasses/no_vehicle');
		$this->Acl->allow($role, 'controllers/CustomerPasses/get_no_vehicle');
		$this->Acl->allow($role, 'controllers/CustomerPasses/admin_get_no_vehicle');
		$this->Acl->allow($role, 'controllers/PropertyUsers/admin_create_csv');
		$this->Acl->allow($role, 'controllers/CustomerPasses/unapproved_users');
		$this->Acl->allow($role, 'controllers/Users/approve_user');
		$this->Acl->allow($role, 'controllers/CustomerPasses/admin_pass_approval');
		$this->Acl->allow($role, 'controllers/CustomerPasses/approve');
		
        //CUSTOMER ACCESSIBLE ACTIONS ACL
        $role->id = 4;
        $this->Acl->deny($role, 'controllers');
		$this->Acl->allow($role, 'controllers/Users/customerHomePage');
		$this->Acl->allow($role, 'controllers/Users/myAccount');
		$this->Acl->allow($role, 'controllers/Users/admin_logout');
		$this->Acl->allow($role, 'controllers/Users/getSubDomain');
		$this->Acl->allow($role, 'controllers/CustomerPasses/view');
		$this->Acl->allow($role, 'controllers/CustomerPasses/renewPass');
		$this->Acl->allow($role, 'controllers/CustomerPasses/getCustomerPassDetails');
		$this->Acl->allow($role, 'controllers/CustomerPasses/guestPass');
		$this->Acl->allow($role, 'controllers/Packages/index');
		$this->Acl->allow($role, 'controllers/Packages/getCurrentUserPassStatus');
		$this->Acl->allow($role, 'controllers/Packages/getCorrespondingPackage');
		$this->Acl->allow($role, 'controllers/Packages/getPackageNamePassBought');
		$this->Acl->allow($role, 'controllers/Packages/remainingPassPackageName');
		$this->Acl->allow($role, 'controllers/Packages/getPackageName');
		$this->Acl->allow($role, 'controllers/Passes/customer_buy_pass');
		$this->Acl->allow($role, 'controllers/Passes/getDeposit');
		$this->Acl->allow($role, 'controllers/Passes/getCost');
		$this->Acl->allow($role, 'controllers/Passes/getDuration');
		$this->Acl->allow($role, 'controllers/Passes/getPassPermit');
		$this->Acl->allow($role, 'controllers/Passes/getPassName');
		$this->Acl->allow($role, 'controllers/Transactions/check_out');
		$this->Acl->allow($role, 'controllers/Transactions/compulsoryPasses');
		$this->Acl->allow($role, 'controllers/Transactions/buyRemainingPass');
		$this->Acl->allow($role, 'controllers/Transactions/guestCheckOut');
		$this->Acl->allow($role, 'controllers/Transactions/passRenew');
		$this->Acl->allow($role, 'controllers/Transactions/activatePass');
		$this->Acl->allow($role, 'controllers/Transactions/select_passes');
		$this->Acl->allow($role, 'controllers/Transactions/buy_selected_passes');
		$this->Acl->allow($role, 'controllers/Transactions/buy_passes');
		$this->Acl->allow($role, 'controllers/Vehicles/add');
		$this->Acl->allow($role, 'controllers/Vehicles/getVehiclePlateNumber');
		$this->Acl->allow($role, 'controllers/Vehicles/setGuestPackageOptions');
		$this->Acl->allow($role, 'controllers/Vehicles/addSelectedVehicle');
		$this->Acl->allow($role, 'controllers/Vehicles/edit');
		$this->Acl->allow($role, 'controllers/Vehicles/index');
		$this->Acl->allow($role, 'controllers/Vehicles/add_vehicles');
		$this->Acl->allow($role, 'controllers/Tickets/contact_us');
        $this->Acl->allow($role, 'controllers/Tickets/add');
        $this->Acl->allow($role, 'controllers/Tickets/view');
        $this->Acl->allow($role, 'controllers/Tickets/ticket_status');
        $this->Acl->allow($role, 'controllers/TicketReplies/view_single');
        $this->Acl->allow($role, 'controllers/TicketReplies/add');
        $this->Acl->allow($role, 'controllers/TicketReplies/message_ajax');
        $this->Acl->allow($role, 'controllers/ChangeVehicleDetails/change');
        $this->Acl->allow($role, 'controllers/ChangeVehicleDetails/view_notifications');
        $this->Acl->allow($role, 'controllers/CustomerPasses/extendGuestPass');
        $this->Acl->allow($role, 'controllers/CustomerPasses/update_guest_pass');
        $this->Acl->allow($role, 'controllers/ChangeVehicleDetails/all_requests');
		$this->Acl->allow($role, 'controllers/ChangeVehicleDetails/delete');      
		$this->Acl->allow($role, 'controllers/Vehicles/add_guest_vehicle');
		$this->Acl->allow($role, 'controllers/CustomerPasses/activateGuestPass');
		$this->Acl->allow($role, 'controllers/CustomerPasses/checkForPayment');
		$this->Acl->allow($role, 'controllers/Vehicles/get_guest_vehicle_ajax');
		$this->Acl->allow($role, 'controllers/Vehicles/add_guest_vehicle_ajax');
		$this->Acl->allow($role, 'controllers/Transactions/guestCheckOutAjax');
		$this->Acl->allow($role, 'controllers/Transactions/processPayment');
		$this->Acl->allow($role, 'controllers/Notifications/sendOpt');
		$this->Acl->allow($role, 'controllers/Notifications/verifyOtp');
		$this->Acl->allow($role, 'controllers/Users/smsSubscription');
		$this->Acl->allow($role, 'controllers/Documents/admin_get_documents');
        // allow basic users to log out
        
        $this->Acl->allow($role, 'controllers/Users/logout');

        // we add an exit to avoid an ugly "missing views" error message
        echo "all done";
        exit;
    }
/*********************************************************************
 * ACL END
 */ 

	public function index() {
		$this->User->recursive = 0;
		$this->set('users', $this->Paginator->paginate());
	}

/**********************************************************************
 * view method
 */
	public function view($id = null) {
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}
		$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
		$this->set('user', $this->User->find('first', $options));
	}

/**********************************************************************
 * admin_index method
 */
	public function admin_index() {
	}

/*********************************************************************
 * admin_view method
 */
	public function admin_view($id = null) {
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}
		$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
		$this->set('user', $this->User->find('first', $options));
	}

/*********************************************************************
 * admin_add User method
 */
    public function admin_add()
    {
        $this->layout='default';
        if ($this->request->is('post')){
            $this->User->set($this->request->data);
            $this->User->validate=$this->User->userAdd;
            if($this->User->validates()){
                $this->User->create();
                if ($this->User->save($this->request->data,false)){
					CakeLog::write('newUserAddedAdmin', ''.AuthComponent::user('username').' : New users added with User ID: <a href="/admin/users/view_user_details/'.$this->User->getLastInsertID().'">'.$this->User->getLastInsertID().'</a>, by Name: '.AuthComponent::user('first_name').' '.AuthComponent::user('last_name').' ');  
                    $this->Session->setFlash('The user has been saved.','success');
                    return $this->redirect(array('action' => 'unassigned_user'));
                } else{
                    $this->Session->setFlash('The user could not be saved. Please, try again.','error');
                }
            }
            else{
                $this->Session->setFlash(__('Validation Failed'));
            }
        }
        $roles = $this->User->Role->find('list',array('fields'=>array('role_name'),'conditions' => array('NOT' => array('Role.id' => array(4)))));
        $this->set(compact('roles'));

    }

/*******************************************************************
 * admin_edit method
 */
	public function admin_edit($id = null) {
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if($this->request->data['User']['password_changed']==0){
				$this->request->data['User']['password']="passwordnotset";
				$this->request->data['User']['password_confirmation']="passwordnotset";
			}
			$this->request->data['User']['first_name']=trim($this->request->data['User']['first_name']);
			$this->request->data['User']['last_name']=trim($this->request->data['User']['last_name']);
			$this->User->set($this->request->data);
			$this->User->validate=$this->User->userAdd;
			if($this->User->validates()){
				if($this->request->data['User']['password_changed']==0){
					unset($this->request->data['User']['password']);
					unset($this->request->data['User']['password_confirmation']);
					unset($this->request->data['User']['password_changed']);
				}
				if ($this->User->save($this->request->data)) {
					CakeLog::write('userEditedAdmin', ''.AuthComponent::user('username').' : User details edited with User ID: <a href="/admin/users/view_user_details/'.$id.'">'.$id.'</a>, by Name: '.AuthComponent::user('first_name').' '.AuthComponent::user('last_name').' ');  
					$this->Session->setFlash('The user has been saved.','success');
					return $this->redirect(array('action' => 'index'));
				} else {
					$this->Session->setFlash('The user could not be saved.','error');
				}
			}else{
				$this->Session->setFlash('Validation Failed','error');
			}
		} else {
			$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
			$this->request->data = $this->User->find('first', $options);
		}
		$roles = $this->User->Role->find('list');
		$this->set(compact('roles'));
	}


/*********************************************************************
 * admin_delete method
 */
	public function admin_delete($id = null) {
		$this->User->id = $id;
		if (!$this->User->exists()) {
			throw new NotFoundException(__('Invalid user'));
		}
		//$this->request->allowMethod('post', 'delete');
		if ($this->User->delete()) {
			$this->loadModel('PropertyUser');
			$this->PropertyUser->deleteAll(array('user_id'=>$id));
			$this->Session->setFlash(__('The user has been deleted.'),'success');
		} else {
			$this->Session->setFlash(__('The user could not be deleted. Please, try again.'),'error');
		}
		return $this->redirect(array('action' => 'index'));
	}


/**********************************************************************
* admin_login method
*/
    public function admin_login()
    {
        $this->layout='login';
        $this->loadModel('Role');
        $redirectURL=null;
        if ($this->Auth->loggedIn()) {
			if($this->Auth->user('role_id')==1||$this->Auth->user('role_id')==2){
			  if($this->Auth->redirect()=="/"){
					$user = $this->Auth->user();
					if($user['role_id']==$this->Role->ROLES['superAdmin']){
						$this->redirect(array('action'=>'admin_superAdminHome'));
					}
					elseif($user['role_id']==$this->Role->ROLES['propertyAdmin']){
						$this->redirect(array('action'=>'admin_pa_home_page'));
					}
			   }else{
					$this->redirect($this->Auth->redirect());
			   }
			}else{
				$this->redirect(array('action'=>'logout'));
			}
		}
        if ($this->request->is('post')){
           if($this->Auth->login()){
			   //debug(AuthComponent::user('role_id'));die;
			   if($this->Auth->user('role_id')==1||$this->Auth->user('role_id')==2){
				   if($this->Auth->user('role_id')==2){
				    $this->loadModel('Property');
					$adminAllProperty=$this->Property->find('list',array(
															 'joins'=>array(
																			array(
																				  'table'=>'property_users',
																				  'alias'=>'PropertyUser',
																				  'type'=>'LEFT',
																				  'conditions'=>array(
																						'Property.id = PropertyUser.property_id'
																				   )
																			)
															  ),
															  'conditions'=>array('PropertyUser.user_id'=>$this->Auth->user('id'),'PropertyUser.role_id'=>$this->Auth->user('role_id'))
													
													));
					$this->Session->write('AdminAllProperty',$adminAllProperty);
					}
					if($this->request->data['User']['requestedUrl']=="/"){
						$user = $this->Auth->user();
						if($user['role_id']==$this->Role->ROLES['superAdmin']){
							$this->redirect(array('action'=>'admin_superAdminHome'));
						}
						elseif($user['role_id']==$this->Role->ROLES['propertyAdmin']){
							$this->redirect(array('action'=>'admin_pa_home_page'));
						}
					}else{
						$this->redirect($this->request->data['User']['requestedUrl']);
					}
				}else{ 
					$this->redirect(array('action'=>'logout'));
				}
            
        }else{
                $this->Session->setFlash('Username and password does not match.','error');
            }
        }
        $requestedURL=$this->Auth->redirect();
        $this->set(compact('requestedURL'));

    }


/**********************************************************************************************************************
* admin_logout method
*/
    public function admin_logout()
    {
        $this->Session->destroy();
        return $this->redirect('login');
    }

/**********************************************************************************************************************
* Method to get the current sub domain
*/
    public function getSubDomain(){
        $this->layout=$this->autoRender=false;
        return $this->request->params['sub_domain'];
    }

/**********************************************************************************************************************
* Login Other Than ADMIN
*/
    public function login()
    {       
		$this->loadModel('Role');
		$this->loadModel('CouponCode');
		if ($this->Auth->loggedIn()) {
			  if($this->Auth->redirect()=="/"){
					$user = $this->Auth->user();
					if($user['role_id']==$this->Role->ROLES['customer']){
									$this->loadModel('CustomerPass');
									$result=$this->CustomerPass->hasAny(array('user_id'=>$this->Auth->user('id')));
									if($result){
										$this->Session->write('Allowed',true);
										//$this->redirect(array('action'=>'customerHomePage'));
										$this->redirect(array('controller'=>'Vehicles','action'=>'add_vehicles'));											
									}else{
										$this->Session->write('Allowed',false);
										if(AuthComponent::user('coupon_used')){
												$AllcouponPackage=$this->CouponCode->find('all',array('conditions'=>array('code'=>$this->Auth->user('coupon_used'))));
												$couponPackage=array();
												foreach($AllcouponPackage as $coupon){
													if(isset($coupon['CouponPackage']['property_id'])){
														if($coupon['CouponPackage']['property_id']==CakeSession::read('PropertyId')){
															$couponPackage=$coupon;
														}
													}
												}			
												if(isset($couponPackage['CouponPackage']['id'])){
													if($couponPackage['CouponPackage']['id']){
														$this->Session->write('CouponCode', $this->Auth->user('coupon_used'));
														$this->Session->write('CurrentPackage', $couponPackage['CouponPackage']['id']);
														$this->redirect(array('controller' => 'Transactions', 'action' => 'buy_passes'));
													}else{
														$this->Session->setFlash('Coupon Not Found');
														$this->redirect(array('controller'=>'Transactions','action'=>'compulsoryPasses'));
													}
												}else{
													$this->Session->setFlash('Coupon Not Found');
													$this->redirect(array('controller'=>'Transactions','action'=>'compulsoryPasses'));
												}
										}else{
											$this->redirect(array('controller'=>'Transactions','action'=>'compulsoryPasses'));
										}
									}
					}
					elseif($user['role_id']==$this->Role->ROLES['propertyManager']){
						$this->redirect(array('action'=>'manager_home_page'));
					}
			   }else{
					$this->redirect($this->Auth->redirect());
			   }
		}
	
		$this->layout='login';
       
        if ($this->request->is('post')){
            $this->User->recursive=-1;
            $userExists=$this->User->hasAny(array('username'=>$this->request->data['User']['username'],'archived'=>0));
			if($userExists==true){
				$userId=$this->User->field('id',array('username'=>$this->request->data['User']['username']));
				$this->loadModel('PropertyUser');
				$userExists=$this->PropertyUser->hasAny(array('user_id'=>$userId,'property_id'=>$this->request->params['Property']['id']));
				if($userExists){
					//$approved=$this->User->field('approved',array('username'=>$this->request->data['User']['username']));
					//if($approved){
						if($this->Auth->login()){
							if($this->Auth->user('role_id')==3||$this->Auth->user('role_id')==4){
								 if($this->request->data['User']['requestedUrl']=="/"){
										if($this->Auth->user('role_id')==$this->Role->ROLES['customer']){
											$this->Session->write('PropertyId',$this->request->params['Property']['id']);
											$this->Session->write('PropertyName',$this->request->params['Property']['name']);
											$this->Session->write('PropertyLogo',$this->request->params['Property']['logo']);
											$this->loadModel('CustomerPass');
											$result=$this->CustomerPass->hasAny(array('user_id'=>$this->Auth->user('id')));
											if($result){
												$this->Session->write('Allowed',true);
												$this->redirect(array('controller'=>'Vehicles','action'=>'add_vehicles'));												
											}else{
												$this->Session->write('Allowed',false); 
												if($this->Auth->user('coupon_used')){
													$AllcouponPackage=$this->CouponCode->find('all',array('conditions'=>array('code'=>$this->Auth->user('coupon_used'))));
													$couponPackage=array();
													foreach($AllcouponPackage as $coupon){
														if(isset($coupon['CouponPackage']['property_id'])){
															if($coupon['CouponPackage']['property_id']==CakeSession::read('PropertyId')){
																$couponPackage=$coupon;
															}
														}
													}			
													if(isset($couponPackage['CouponPackage']['id'])){
														if($couponPackage['CouponPackage']['id']){
															$this->Session->write('CouponCode', $this->Auth->user('coupon_used'));
															$this->Session->write('CurrentPackage', $couponPackage['CouponPackage']['id']);
															$this->redirect(array('controller' => 'Transactions', 'action' => 'buy_passes'));
														}else{
															$this->Session->setFlash('Coupon Not Found');
															$this->redirect(array('controller'=>'Transactions','action'=>'compulsoryPasses'));
														}
													}else{
														$this->Session->setFlash('Coupon Not Found');
														$this->redirect(array('controller'=>'Transactions','action'=>'compulsoryPasses'));
													}
												}else{
													$this->redirect(array('controller'=>'Transactions','action'=>'compulsoryPasses'));
												}
											}
										}
										if($this->Auth->user('role_id')==$this->Role->ROLES['propertyManager']){
											$this->Session->write('PropertyName',$this->request->params['Property']['name']);
											$this->Session->write('PropertyId',$this->request->params['Property']['id']);
											$this->Session->write('PropertyLogo',$this->request->params['Property']['logo']);
											$this->redirect(array('action'=>'manager_home_page'));	
										}
								}else{
									   $this->loadModel('CustomerPass');
									   $result=$this->CustomerPass->hasAny(array('user_id'=>$this->Auth->user('id')));
									   if($result){
											$this->Session->write('PropertyName',$this->request->params['Property']['name']);
											 $this->Session->write('PropertyId',$this->request->params['Property']['id']);
											 $this->Session->write('PropertyLogo',$this->request->params['Property']['logo']);

											$this->Session->write('Allowed',true);	 
											$this->redirect(array('controller'=>'Vehicles','action'=>'add_vehicles'));										
									   }else{
											$this->Session->write('Allowed',false);
									   }
									   $this->Session->write('PropertyName',$this->request->params['Property']['name']);
									   $this->Session->write('PropertyId',$this->request->params['Property']['id']);
									   $this->Session->write('PropertyLogo',$this->request->params['Property']['logo']);
									   $this->redirect($this->request->data['User']['requestedUrl']);
								}
							}else{
								$this->redirect(array('action'=>'logout'));
							}	
						}else{
							$this->Session->setFlash('Username and password does not match.','error');
						}	
					/*}else{
						$key = Security::hash(String::uuid(),'sha512',true);
						$arr['User']=array(
											'id'=>$userId,
											'aproval_code'=>$key
						
						);
						$this->User->clear();
						if($this->User->save($arr,array('validate'=>false))){
							$this->redirect('/Users/resend_approval_link/'.$key);
						}else{
							$this->Session->setFlash('We have found a problem, Please contact admin.','error');
						}
					}*/			
				}else{
					$this->Session->setFlash('You are not authorised user','error');
				}
			}else{
				$this->Session->setFlash('User does not exist','error');
			}	
		}
		$requestedURL=$this->Auth->redirect();
		$this->loadModel('Property');
        $property=$this->request->params['Property'];
		$this->set(compact('property','requestedURL'));
    }
/**********************************************************************************************************************
* Method to get the current sub domain
*/
    public function logout()
    {
        $this->Session->destroy();
        return $this->redirect('login');
    }
 /**********************************************************************************************************************
  * Register action to register new customer 
  * Changes Made On : 4th Sepmeber,2015
  * Coupon Functionality Added\
  * 
  * Changed Done On: 14th March,2016
  */
 public function register() {
        $this->layout = 'login';
        $this->loadModel('Property');
        $property = $this->request->params['Property'];
        //debug($property);
        $this->loadModel('State');
        $state = new State();
        $states = $state->find('list');
        $this->set(compact('property', 'states'));
		$this->loadModel('CouponCode');
        if ($this->request->is('post')) {
			$this->request->data['User']['coupon']=trim($this->request->data['User']['coupon']);
			if($this->request->data['User']['coupon']){
				$this->User->validate['coupon']=array(
													   'Incorrrect Coupon'=>array(
																				  'rule'=>'validateCoupon'
													   )
				);
			}
            $this->User->set($this->request->data);
            if ($this->User->validates()) {
					$this->loadModel('Property');
					$isCouponRequired=$this->Property->field('coupon_selection',array('id'=>$this->request->data['User']['property_id']));
					if($isCouponRequired){
						if(empty($this->request->data['User']['coupon'])){
							$this->Session->setFlash(__('Coupon Is Required. Please, try again.'),'error');
							return $this->redirect(array('action' => 'register'));
						}
					} 
					$this->User->create();
					if ($this->User->beforeSave()) {
						$this->request->data['User']['guest_credits'] = $property['free_guest_credits'];
						if(!empty($this->request->data['User']['coupon'])){
							$this->request->data['User']['coupon_used']=$this->request->data['User']['coupon'];
						}
						// $key = Security::hash(String::uuid(),'sha512',true);
						$key = Security::hash(CakeText::uuid(),'sha512',true); 
						$this->request->data['User']['aproval_code']=$key;
						if ($this->User->save($this->request->data, false)) {
							
							$lastInsertID = $this->User->getLastInsertId();
							$propertyID = $this->request->data['User']['property_id'];

							//Enter new record in Property User Table
							$this->loadModel('PropertyUser');
							$propertyUser = new PropertyUser();
							$propertyUser->set(array('user_id' => $lastInsertID, 'property_id' => $propertyID));
							if ($propertyUser->save()) {
								
								$url = Router::url( array('controller'=>'users','action'=>'verify_email'), true ).'/'.$key;
								$this->Email->from = $property['name'].'<noreply@' . $_SERVER['HTTP_HOST'] . '>';
								$this->Email->to = $this->request->data['User']['email'];
								$this->Email->sendAs = 'html';
								$this->Email->subject = 'Email Verification Link';
								$this->Email->smtpOptions=Configure::read('SMTP_SETTING');
								$this->Email->delivery='smtp';
								$message='Dear Customer,<br>Please <a href="'.$url.'">Click Here</a> to verify your email.<br>
										  OR<br>
										  Copy the following link and open in new tab.
										  <br>
										  '.$url.'
										  <br>
										 Thanks,
										  <br>
										  <b>Administrator</b><br>
										  '.$_SERVER['HTTP_HOST'].'
											';
								if($this->Email->send($message)){
									//CakeLog::write('newUserRegistered', '' . AuthComponent::user('username') . ' : User registered with User ID: <a href="/admin/users/view_user_details/' . AuthComponent::user('id') . '">' . AuthComponent::user('id') . '</a>, Name: ' . AuthComponent::user('first_name') . ' ' . AuthComponent::user('last_name') . ', in Property: ' . CakeSession::read('PropertyName') . '');

									$this->Session->setFlash(__('Please check your email to verify your email address.'),'success');
									
								}else{
									$this->Session->setFlash(__('Account approval email cannot be sent. Please contact administrator.'),'error'); 
								}
								
								//$this->redirect(array('action'=>'login'));
								if ($this->Auth->login()) {

									$this->Session->write('PropertyId', $propertyID);
									$this->Session->write('PropertyName', $this->request->params['Property']['name']);
									CakeLog::write('newUserRegistered', '' . AuthComponent::user('username') . ' : User registered with User ID: <a href="/admin/users/view_user_details/' . AuthComponent::user('id') . '">' . AuthComponent::user('id') . '</a>, Name: ' . AuthComponent::user('first_name') . ' ' . AuthComponent::user('last_name') . ', in Property: ' . CakeSession::read('PropertyName') . '');
									if(empty($this->request->data['User']['coupon'])){
										$this->redirect(array('controller' => 'Transactions', 'action' => 'compulsoryPasses'));
									}else{
										$this->Session->write('CouponCode', $this->request->data['User']['coupon']);
										$AllcouponPackage=$this->CouponCode->find('all',array('conditions'=>array('code'=>$this->Auth->user('coupon_used'))));
										$couponPackage=array();
										foreach($AllcouponPackage as $coupon){
											if(isset($coupon['CouponPackage']['property_id'])){
												if($coupon['CouponPackage']['property_id']==CakeSession::read('PropertyId')){
													$couponPackage=$coupon;
												}
											}
										}	
										if(isset($couponPackage['CouponPackage']['id'])){
											if($couponPackage['CouponPackage']['id']){
												$this->Session->write('CurrentPackage', $couponPackage['CouponPackage']['id']);
												$this->Session->write('CouponCode', $this->request->data['User']['coupon']);
												$this->redirect(array('controller' => 'Transactions', 'action' => 'buy_passes'));
											}else{
												$this->Session->setFlash('Coupon Not Found');
												$this->redirect(array('controller'=>'Transactions','action'=>'compulsoryPasses'));
											}
										}else{
											$this->redirect(array('controller'=>'Transactions','action'=>'compulsoryPasses'));
										}
									}
								}
							} else {
								echo "Property user error";
							}
						} else {
							$this->Session->setFlash(__('The user could not be saved. Please, try again.'));
						}
					}
				
            }
        }
    }

/**********************************************************************************************************************
 * home page of the customer
 */

    public function customerHomePage()
    {
        $this->layout='customer';
       
    }
    function admin_get_customers($prop_id=null){
		$this->autoRender=$this->layout=false;

        $aColumns = array('User.first_name', 'User.last_name', 'User.username', 'User.email', 'User.phone','User.address_line_1','User.address_line_2','properties.name','User.id','User.role_id','Role.role_name');
        $sIndexColumn = " property_users.id ";
        $sTable = " property_users ";
        $sJoinTable=' INNER JOIN users User ON property_users.user_id=User.id INNER JOIN properties ON property_users.property_id=properties.id INNER JOIN roles Role ON property_users.role_id=Role.id';
        $sConditions=' User.id !='.AuthComponent::user('id').' AND property_users.role_id=4 AND User.archived=0 AND property_users.property_id = '.$prop_id;
       
        $returnArr=$this->NewDatatable->getData(array('columns'=>$aColumns,'index_column'=>$sIndexColumn,'table'=>$sTable,'join'=>$sJoinTable,'conditions'=>$sConditions));
        echo json_encode($returnArr);
        die;    
	}
/**********************************************************************************************************************
 * To view account details all users other than admin
*/
   public function myAccount()
    {
		
        if($this->Auth->user('role_id')==4){
			$this->layout='customer';
		}elseif($this->Auth->user('role_id')==3){
			$this->layout='manager';
		}
		  
        $id=$this->Session->read('Auth.User.id');
        if (!$this->User->exists($id)) {
            throw new NotFoundException(__('Invalid user'));
        }
        $currentPhone=$this->User->field('phone',array('User.id'=>$this->Auth->user('id')));
        if ($this->request->is(array('post', 'put')))
        { 
			if(isset($this->request->data['User']['phone'])){
				if($currentPhone!=$this->request->data['User']['phone']){
					$this->request->data['User']['phone_verified']=0;
				}
			}
			$this->User->set($this->request->data);
			if(isset($this->request->data['User']['new_password'])){
				if($this->User->validates(array('fieldList'=>array('current_password','new_password','confirm_password')))){
					$data['User']['id']=$this->Auth->user('id');
					$data['User']['password']=$this->request->data['User']['new_password'];
					$data['User']['password_confirmation']=$this->request->data['User']['confirm_password'];
					if ($this->User->save($data,false))
					{
					$this->Session->setFlash('Your profile has been updated successfully.','success');
					return $this->redirect(array('action' => 'myAccount'));
					}
					else {
						$this->Session->setFlash(' Please, try again.','error');
					}
				}else{
					 $this->Session->setFlash('Check Password Details','error');    
				}
			}else{
				if($this->User->validates(array('fieldList'=>array('first_name','last_name','email','address_line_1','address_line_2','street','city','state','zip','phone')))){
					if ($this->User->save($this->request->data,false))
					{
				    CakeLog::write('accountDetailUpdated', ''.AuthComponent::user('username').' : Account details updated by Name: '.AuthComponent::user('first_name').' '.AuthComponent::user('last_name').' ');  
					$this->Session->setFlash('Your profile has been updated successfully.','success');
					return $this->redirect(array('action' => 'myAccount'));
					}
					else {
						$this->Session->setFlash(' Please, try again.','error');
					}
				}
				else {
                $this->Session->setFlash(' Validation Failure','error');
				}
			}
		}
      
        $this->loadModel('State');
        $state=new State();
        $states=$state->find('list');
        $this->User->recursive = -1;
        $currentUser=$this->User->find('first',array('conditions'=>array('User.id'=>$this->Session->read('Auth.User.id'))));
        $this->set(array('users'=>$currentUser,'states'=>$states,'currentUser'=>$currentUser));
}
/**********************************************************************************************************************
 * To view admin account
*/
    public function admin_myAccount()
    {
		if($this->Auth->user('role_id')==1){
			$this->layout='default';
		}elseif($this->Auth->user('role_id')==2){
			$this->layout='administrator';
			$this->Session->write('PropertySelection',true);
		}
         
        $id=$this->Session->read('Auth.User.id');
        if (!$this->User->exists($id)) {
            throw new NotFoundException(__('Invalid user'));
        }
        if ($this->request->is(array('post', 'put')))
        { 
			$this->User->set($this->request->data);
			if(isset($this->request->data['User']['new_password'])){
				if($this->User->validates(array('fieldList'=>array('current_password','new_password','confirm_password')))){
					$data['User']['id']=$this->Auth->user('id');
					$data['User']['password']=$this->request->data['User']['new_password'];
					$data['User']['password_confirmation']=$this->request->data['User']['confirm_password'];
					if ($this->User->save($data,false))
					{
					$this->Session->setFlash('Your profile has been updated successfully.','success');
					return $this->redirect(array('action' => 'myAccount'));
					}
					else {
						$this->Session->setFlash(' Please, try again.','error');
					}
				}else{
					// $this->redirect(array('action' => 'myAccount#tab_3-3'));
					 $this->Session->setFlash('Check Password Details','error');    
				}
			}else{
				if($this->User->validates(array('fieldList'=>array('first_name','last_name','email','address_line_1','address_line_2','street','city','state','zip','phone')))){
					if ($this->User->save($this->request->data,false))
					{
					CakeLog::write('accountDetailUpdatedAdmin', ''.AuthComponent::user('username').' : Account details updated by Name: '.AuthComponent::user('first_name').' '.AuthComponent::user('last_name').' ');  
					$this->Session->setFlash('Your profile has been updated successfully.','success');
					return $this->redirect(array('action' => 'myAccount'));
					}
					else {
						$this->Session->setFlash(' Please, try again.','error');
					}
				}
				else {
                $this->Session->setFlash(' Validation Failure','error');
				}
			}
		}
      
        $this->loadModel('State');
        $state=new State();
        $states=$state->find('list');
        $this->User->recursive = -1;
        $currentUser=$this->User->find('first',array('conditions'=>array('User.id'=>$this->Session->read('Auth.User.id'))));
        $this->set(array('users'=>$currentUser,'states'=>$states));


    }
/**********************************************************************************************************************
 * To view user details
*/
    public function userEditDetails($id=null)
    {
        if (!$this->User->exists($id)) {
            throw new NotFoundException(__('Invalid user'));
        }
        if ($this->request->is(array('post', 'put')))
        {
            if ($this->User->save($this->request->data,false,array(
                'first_name','last_name','email','address_line_1','address_line_2','street','city','state','zip','phone')))
            {
                $this->Session->setFlash(__('The record has been updated.'));
                return $this->redirect(array('action' => 'myAccount'));
            } else {
                $this->Session->setFlash(__('The user could not be saved. Please, try again.'));
            }
        }
        else
        {
            $options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
            $this->request->data = $this->User->find('first', $options);
        }


    }
/**********************************************************************************************************************
 * To view admin home page
*/
    public function admin_superAdminHome()
    {
		$this->loadModel('Property');
		$propertyCount=$this->Property->find('count');
		$this->loadModel('CustomerPass');
		$dt = new DateTime();
		$currentDateTime= $dt->format('Y-m-d');
		$vehicleCount=$this->CustomerPass->find('count',array('conditions'=>array('pass_valid_upto >='=>$currentDateTime,'vehicle_id IS NOT NULL')));
		$userCount=$this->User->find('count',array('conditions'=>array('role_id'=>4)));
		$this->loadModel('Transaction');
		$this->Transaction->virtualFields['amountTransaction']='SUM(Transaction.amount)';
		$this->Transaction->recursive=-1;
		$conditions= " date_time BETWEEN '".date('Y-m-d', strtotime("-1 month"))."' AND '".$currentDateTime."' ";
		$transactionAmount = $this->Transaction->field('amountTransaction',array($conditions));
		
		$this->set(compact('propertyCount','vehicleCount','userCount','transactionAmount'));
		/*$this->loadModel('ChangeVehicleDetail');
		$vehicleData=$this->ChangeVehicleDetail->find('all',array('conditions'=>array('changed=0'),'order'=>array('ChangeVehicleDetail.created DESC' )));
		$this->loadModel('Ticket');
		$newMessages=$this->Ticket->find('all',array('conditions'=>array('reply_admin !=0'),'order'=>array('Ticket.created DESC' )));
		
		$this->set(compact('vehicleData','newMessages'));   */   
    }

/**********************************************************************************************************************
 * To view admin details
*/
    public function admin_user_details($id)
    {
        $this->layout='default';
        if(!empty($id))
        {
            $this->User->unbindModel(array('belongsTo'=>'Role','hasMany'=>array('Pass','Transaction','Vehicle','PropertyUser')));
            $userDetails=$this->User->find('first',array('fields'=>array('first_name','email','phone'),'conditions'=>array('User.id'=>$id)));
            $this->User->recursive = 0;
            return $userDetails;
        }

    }
   public function home()
    {
        $this->layout='home';		
    }
/*********************************************************************************************
* Change Password
*/
	public function changePassword(){
	if ($this->request->is(array('post', 'put')))
        {	
			$this->User->set($this->request->data);			
        }   
	}
/*********************************************************************************************
* forget Password
*/
	public function forgetPwd(){
		$this->User->recursive=-1;
		if(!empty($this->request->data))
		{
			
			if(empty($this->request->data['email']))
			{
				$this->Session->setFlash('Please Provide Your Email Address that You used to Register with Us');
			}
			else
			{
				$email=$this->request->data['email'];
				$userDetails=$this->User->find('first',array('conditions'=>array('User.email'=>$email)));
				if($userDetails)
				{
					if($userDetails['User']['archived']==0){	
						
						// $key = Security::hash(String::uuid(),'sha512',true);
						$key = Security::hash(CakeText::uuid(),'sha512',true); 
						$hash=sha1($userDetails['User']['username'].rand(0,60));
						$url = Router::url( array('controller'=>'users','action'=>'reset'), true ).'/'.$key.'#'.$hash;
						$ms=$url;
						$ms=wordwrap($ms,1000);
						$dt = new DateTime();
						$currentDateTime= $dt->format('Y-m-d H:i:s');
						$saveUser['User']['id']=$userDetails['User']['id'];
						$saveUser['User']['tokenhash']=$key;
						$saveUser['User']['tokentime']=$currentDateTime;
						if($this->User->save($saveUser,false)){
								$this->Email->template = 'resetpw';
								$this->Email->from    = 'Online Parking Pass Password Reset <alerts@netparkingpass.com>';
								$this->Email->to      = $userDetails['User']['first_name'].'<'.$userDetails['User']['email'].'>';
								$this->Email->subject = 'Reset Your onlineparkingpass.com Password';
								$this->Email->sendAs = 'both';
								$this->Email->smtpOptions=Configure::read('SMTP_SETTING');
								$this->Email->delivery='smtp';
								$this->set('ms', $ms);
								$this->set('username', $userDetails['User']['username']);
								try {
									if ( $this->Email->send() ) {
										$this->Session->setFlash('Check Your Email To Reset your password','success');
									} 
								}	 catch ( Exception $e ) {
									$this->Session->setFlash('Email sending failed please try later','error');
								}
						}
						else{
							$this->Session->setFlash("Error Generating Reset link");
						}
					}else{
						$this->Session->setFlash("This account has been archived. Please contact admin.");
					}
				}
				else
				{
					$this->Session->setFlash('Email does Not Exist');
				}
			}
		}
		 $this->redirect(array('controller'=>'users','action'=>'login'));
	}
/*********************************************************************************************
* reset Password
*/
	public function reset($token=null){
        $this->layout="login";
        $this->User->recursive=-1;
        $resetUser=$this->User->findBytokenhash($token);
		 if($resetUser){
			$dt = new DateTime();
			$currentDateTime= $dt->format('Y-m-d H:i:s');
			$diff = strtotime($currentDateTime) - strtotime($resetUser['User']['tokentime']);
			$diff_in_hrs = $diff/3600;
			
			if($diff_in_hrs<=6){
				$this->Session->write('userId',$resetUser['User']['id']);
				$this->redirect(array('action'=>'resetPassword'));
			}else{
				$this->Session->setFlash('Link Expired Please Retry','error');
			}	 
         }else{
                $this->Session->setFlash('Link Expired Please Retry','error');
         }
         
    }
    public function resetPassword(){
        $this->layout="login";
        $userId=$this->Session->read('userId');
		if ($this->request->is(array('post'))){
					$this->request->data['User']['id']= $userId;
					$this->request->data['User']['tokenhash']=null;
                    $this->User->set($this->request->data);
                    if($this->User->validates(array('fieldList'=>array('password','password_confirmation')))){
                        if($this->User->save($this->request->data,false))
                        {
                            $this->Session->delete('userId');
                            $this->Session->setFlash('Password Has been Updated','success');
                            $this->redirect('/');
                        }
                    }
                    else{
                       $this->Session->setFlash('Validation Error','error');
                    }       
		}
		$this->set(compact('userId'));
    }
     /**********************************************************
     * MANAGER HOME PAGE  
     * 
     */
     public function manager_home_page(){
		$this->layout='manager';
		$dt = new DateTime();
		$currentDateTime= $dt->format('Y-m-d H:i:s');
		$this->loadModel('CustomerPass');
		$this->CustomerPass->recursive=-1;
		$activeGuest=$this->CustomerPass->find('count',array('conditions'=>array('is_guest_pass'=>1,'membership_vaild_upto >'=>$currentDateTime,'pass_valid_upto >'=>$currentDateTime,'property_id'=>$this->Session->read('PropertyId'))));
		$this->CustomerPass->recursive=-1;
		$activeVehicles=$this->CustomerPass->find('count',array('conditions'=>array('OR'=>array(array('is_guest_pass'=>0),array('is_guest_pass'=>null)),'membership_vaild_upto >'=>$currentDateTime,'pass_valid_upto >'=>$currentDateTime,'property_id'=>$this->Session->read('PropertyId'),'vehicle_id !='=>null)));
		
        for($i=0;$i<=3;$i++){
            $month=date('m',strtotime(' -'.$i.' month'));
            $year=date('Y',strtotime(' -'.$i.' month'));
             $count[]=$this->CustomerPass->find('count',array('conditions'=>array('AND'=>array(array('month(created)'=>$month,'year(created)'=>$year)),'property_id'=>$this->Session->read('PropertyId'))));
        }
        
        $monthname = date('M(Y)', strtotime('+0 month'));
        $lastMonthName = date('M(Y)', strtotime('-1 month'));
        $thirdLastMonthName = date('M(Y)', strtotime('-2 month'));
        $fourthLastMonthName = date('M(Y)', strtotime('-3 month'));
        $this->set(array('counter1' => $count[0], 'counter2' =>  $count[1], 'counter3' => $count[2], 'counter4' =>  $count[3], 'monthname' => $monthname, 'lastMonthName' => $lastMonthName, 'thirdLastMonthName' => $thirdLastMonthName, 'fourthLastMonthName' => $fourthLastMonthName));
    	
		
		
		$this->loadModel('PropertyUser');
		$this->PropertyUser->recursive=-1;
		$registeredUser=$this->PropertyUser->find('count',array('conditions'=>array('property_id'=>$this->Session->read('PropertyId'),'role_id'=>4)));
		$activePasses=$this->CustomerPass->find('count',array('conditions'=>array('membership_vaild_upto >'=>$currentDateTime,'pass_valid_upto >'=>$currentDateTime,'property_id'=>$this->Session->read('PropertyId'))));
		$inActivePasses=$this->CustomerPass->find('count',array('conditions'=>array('OR'=>array(array('membership_vaild_upto <'=>$currentDateTime),array('pass_valid_upto <'=>$currentDateTime)),'property_id'=>$this->Session->read('PropertyId'))));
		 $expiredPasses = $this->CustomerPass->find('count', array('conditions' => array('pass_valid_upto <' => $currentDateTime, 'property_id' => $this->Session->read('PropertyId'))));

		$result[]=array('label'=>'Active','data'=>$activePasses, 'color'=> '#53dc9e');
		$result[]=array('label'=>'Inactive','data'=>$inActivePasses,'color'=> '#eb7575');
		$result[]=array('label'=>'Expired','data'=>$expiredPasses,'color'=> '#cfb72f');
		
		$result2[]=array('label'=>'Active vehicles','data'=>$activeVehicles,'color'=> '#53dc9e');
		$result2[]=array('label'=>'Active Guest Vehicles','data'=>$activeGuest,'color'=> '#eb7575');
		$result2[]=array('label'=>'InActive Vehicles','data'=>$inActivePasses,'color'=> '#cfb72f');
		
		$this->set(compact('activeGuest','activeVehicles','registeredUser','activePasses','result','result2','createdPasses','inActivePasses','expiredPasses'));
	 }
	public function manager_my_account(){
		$this->layout='manager';
	 }
	 public function manager_user_details(){
		$this->layout='manager';
	 }
	 public function manager_customer_details(){
		$this->autoRender=$this->layout=false;
		$output=array();
		$output = $this->User->managergetCustomerData();
        echo json_encode($output);
	 }
	  public function manager_view_user_details($id){
		 $this->layout='manager';
		 if (!$this->User->exists($id)) {
            throw new NotFoundException(__('Invalid user'));
        }  
		$this->User->unbindModel(array('belongsTo'=>'Role','hasMany'=>array('Transaction')));
		$userDetails=$this->User->find('first',array('conditions'=>array('id'=>$id)));
		$this->set(compact('userDetails'));
	 }
/************************************
 * Admin View User Details
 */
	 public function admin_view_user_details($id){
		
		 if (!$this->User->exists($id)) {
            throw new NotFoundException(__('Invalid user'));
        }  
		$this->User->unbindModel(array('belongsTo'=>'Role','hasMany'=>array('Transaction')));
		$userDetails=$this->User->find('first',array('conditions'=>array('id'=>$id)));
		$this->set(compact('userDetails'));
	 } 
	 public function saveUser($arr=array()){
		if($this->User->save($arr,false)){
			return $this->User->getLastInsertId();
		}else{
			return false;
		}
	}
/***************************
 * Property Administrator
 */
 public function admin_pa_home_page()
 {
  $this->layout='administrator';
  $this->Session->write('PropertySelection',true);
  $this->loadModel('PropertyUser');
  $adminProperty=$this->PropertyUser->find('list',array('fields'=>array('p.name'),
											 'joins'=>array(
															array(
																  'table'=>'properties',
																  'alias'=>'p',
																  'type'=>'LEFT',
																  'conditions'=>array(
																		'p.id = PropertyUser.property_id'
																   )
															)
											  ),
											  'conditions'=>array('user_id'=>$this->Auth->user('id'),'role_id'=>$this->Auth->user('role_id'))
									
									));
   if(!CakeSession::check('PropertyId'))
   {
		$keys=array_keys($adminProperty);
		$propertyNames=array_values($adminProperty);
		$propertyId=$this->PropertyUser->field('property_id',array('id'=>$keys[0]));
		$this->Session->write('CurrentProperty',$keys[0]);
		$this->Session->write('PropertyName',$propertyNames[0]);
		$this->Session->write('PropertyId',$propertyId);
		$this->Session->write('AdminProperty',$adminProperty);
	}
   $dt = new DateTime();
   $currentDateTime= $dt->format('Y-m-d H:i:s');
   $this->loadModel('CustomerPass');
   $this->CustomerPass->recursive=-1;
   $activeGuest=$this->CustomerPass->find('count',array('conditions'=>array('is_guest_pass'=>1,'membership_vaild_upto >'=>$currentDateTime,'pass_valid_upto >'=>$currentDateTime,'property_id'=>$this->Session->read('PropertyId'))));
   $this->CustomerPass->recursive=-1;
   $activeVehicles=$this->CustomerPass->find('count',array('conditions'=>array('OR'=>array(array('is_guest_pass'=>0),array('is_guest_pass'=>null)),'membership_vaild_upto >'=>$currentDateTime,'pass_valid_upto >'=>$currentDateTime,'property_id'=>$this->Session->read('PropertyId'),'vehicle_id !='=>null)));
   $createdPasses=$this->CustomerPass->find('all');
		
		$this->PropertyUser->recursive=-1;
		$registeredUser=$this->PropertyUser->find('count',array('conditions'=>array('property_id'=>$this->Session->read('PropertyId'),'role_id'=>4)));
		$activePasses=$this->CustomerPass->find('count',array('conditions'=>array('membership_vaild_upto >'=>$currentDateTime,'pass_valid_upto >'=>$currentDateTime,'property_id'=>$this->Session->read('PropertyId'))));
		$inActivePasses=$this->CustomerPass->find('count',array('conditions'=>array('OR'=>array(array('membership_vaild_upto <'=>$currentDateTime),array('pass_valid_upto <'=>$currentDateTime)),'property_id'=>$this->Session->read('PropertyId'))));
		$expiredPasses=$this->CustomerPass->find('count',array('conditions'=>array('pass_valid_upto <'=>$currentDateTime)));
		$result[]=array('label'=>'Active','data'=>$activePasses,'color'=> '#53dc9e');
		$result[]=array('label'=>'Inactive','data'=>$inActivePasses,'color'=> '#eb7575');
		$result[]=array('label'=>'Expired','data'=>$expiredPasses, 'color'=> '#cfb72f');
		
		$result2[]=array('label'=>'Active vehicles','data'=>$activeVehicles,'color'=> '#53dc9e');
		$result2[]=array('label'=>'Active Guest Vehicles','data'=>$activeGuest,'color'=> '#eb7575');
		$result2[]=array('label'=>'InActive Vehicles','data'=>$inActivePasses,'color'=> '#cfb72f');
		
		$this->set(compact('activeGuest','activeVehicles','registeredUser','activePasses','result','result2','createdPasses','inActivePasses','expiredPasses'));
		
		for($i=0;$i<=3;$i++){
            $month=date('m',strtotime(' -'.$i.' month'));
            $year=date('Y',strtotime(' -'.$i.' month'));
            $count[]=$this->CustomerPass->find('count',array('conditions'=>array('AND'=>array(array('month(created)'=>$month,'year(created)'=>$year)),'property_id'=>$this->Session->read('PropertyId'))));
        }
        
        $monthname = date('M(Y)', strtotime('+0 month'));
        $lastMonthName = date('M(Y)', strtotime('-1 month'));
        $thirdLastMonthName = date('M(Y)', strtotime('-2 month'));
        $fourthLastMonthName = date('M(Y)', strtotime('-3 month'));
        $this->set(array('counter1' => $count[0], 'counter2' =>  $count[1], 'counter3' => $count[2], 'counter4' =>  $count[3], 'monthname' => $monthname, 'lastMonthName' => $lastMonthName, 'thirdLastMonthName' => $thirdLastMonthName, 'fourthLastMonthName' => $fourthLastMonthName));
        $sessionId = $this->Session->read('PropertyId');
		$this->loadModel('Pass');
		$this->Pass->recursive = -1;
		$passStatus = $this->Pass->find('all', array('fields' => array('name','id'), 'conditions' => array('Pass.property_id' => $sessionId)));
		$this->set(compact('passStatus'));            
 }
	public function admin_pa_user_details(){
		$this->layout='administrator';
		$this->Session->write('PropertySelection',true);
	 }
	public function admin_pa_customer_details(){
		$this->autoRender=$this->layout=false;
		$output=array();
		$output = $this->User->adminGetCustomerData();
        echo json_encode($output);
	 }
	public function admin_pa_view_user_details($id){
		 $this->layout='administrator';
		 $this->Session->write('PropertySelection',false);
		 if (!$this->User->exists($id)) {
            throw new NotFoundException(__('Invalid user'));
        }  
		$this->User->unbindModel(array('belongsTo'=>'Role','hasMany'=>array('Transaction')));
		$userDetails=$this->User->find('first',array('conditions'=>array('id'=>$id)));
		$this->set(compact('userDetails'));
	 }
/***********************************************************************
 * Admin Print User Details
 */
	public function admin_print_details($userId){
		$this->layout='print';
		if(!$this->User->exists($userId)){
			throw new NotFoundException('User Not Found');
		}
		$this->User->unbindModel(array('belongsTo'=>'Role','hasMany'=>array('Transaction')));
		$userDetails=$this->User->find('first',array('conditions'=>array('id'=>$userId)));
		$this->loadModel('Property');
		for($i=0;$i<count($userDetails['PropertyUser']);$i++){
			$userDetails['User']['assigned_Property'][]=$this->Property->givePropertyName($userDetails['PropertyUser'][$i]['property_id']);
			$userDetails['User']['property_logo'][]=$this->Property->field('logo',array('id'=>$userDetails['PropertyUser'][$i]['property_id']));
		}
		$this->set(compact('userDetails'));
		
	} 
/***********************************************************************
 * Manager Print User Details
 */ 
	public function manager_print_details($userId){
		$this->layout='print';
		if(!$this->User->exists($userId)){ 
			throw new NotFoundException('User Not Found');
		}
		$this->User->unbindModel(array('belongsTo'=>'Role','hasMany'=>array('Transaction')));
		$userDetails=$this->User->find('first',array('conditions'=>array('id'=>$userId)));
		$this->loadModel('Property');
		for($i=0;$i<count($userDetails['PropertyUser']);$i++){
			$userDetails['User']['assigned_Property'][]=$this->Property->givePropertyName($userDetails['PropertyUser'][$i]['property_id']);
			$userDetails['User']['property_logo'][]=$this->Property->field('logo',array('id'=>$userDetails['PropertyUser'][$i]['property_id']));
		}
		$this->set(compact('userDetails'));
		
	}
/***********************************************************************
 * Admin Property Wise User Details
 */
	public function admin_property_wise_users(){ 
		$this->loadModel('Property');    
		$property_list=$this->Property->find('list',array('fields'=>'name'));
		$id=array_keys($property_list); 
		$selectedId=isset($id[0])?$id[0]:'';
		$this->set(compact('property_list','selectedId'));
	}
/***********************************************************************
 * Admin Property Wise User Details
 */
	public function admin_property_wise_users_pa(){ 
		$this->layout='administrator';
		$this->loadModel('Property');    
		$property_list=CakeSession::read('AdminAllProperty');
		$id=array_keys($property_list); 
		$selectedId=$id[0];
		$this->set(compact('property_list','selectedId'));
		$this->render('admin_property_wise_users');
	}
/**************************************************************
 * Admin Access To user Account
 */
 public function admin_userAccount()
    {
		if(CakeSession::check('UserIdAdmin')){
		$user_id=CakeSession::read('UserIdAdmin');
		$this->layout='admin_customer';
		$this->view='admin_myAccount';
        $id=$this->Session->read('Auth.User.id');
        if (!$this->User->exists($user_id)) {
            throw new NotFoundException(__('Invalid user'));
        }
        if ($this->request->is(array('post', 'put')))
        { 
			$this->User->set($this->request->data);
			if(isset($this->request->data['User']['new_password'])){
				if($this->User->validates(array('fieldList'=>array('current_password','new_password','confirm_password')))){
					$data['User']['id']=$user_id;
					$data['User']['password']=$this->request->data['User']['new_password'];
					$data['User']['password_confirmation']=$this->request->data['User']['confirm_password'];
					if ($this->User->save($data,false))
					{
					$this->Session->setFlash('Your profile has been updated successfully.','success');
					return $this->redirect(array('action' => 'myAccount'));
					}
					else {
						$this->Session->setFlash(' Please, try again.','error');
					}
				}else{
					 $this->Session->setFlash('Check Password Details','error');    
				}
			}else{
				if($this->User->validates(array('fieldList'=>array('first_name','last_name','email','address_line_1','address_line_2','street','city','state','zip','phone')))){
					if ($this->User->save($this->request->data,false))
					{
				    CakeLog::write('accountDetailUpdatedAdmin', ''.AuthComponent::user('username').' : Account details updated by Name: '.AuthComponent::user('first_name').' '.AuthComponent::user('last_name').' ');  
					$this->Session->setFlash('Your profile has been updated successfully.','success');
					return $this->redirect(array('action' => 'userAccount'));
					}
					else {
						$this->Session->setFlash(' Please, try again.','error');
					}
				}
				else {
                $this->Session->setFlash(' Validation Failure','error');
				}
			}
		}
      
        $this->loadModel('State');
        $state=new State();
        $states=$state->find('list');
        $this->User->recursive = -1;
        $currentUser=$this->User->find('first',array('conditions'=>array('User.id'=>$user_id)));
        $this->set(array('users'=>$currentUser,'states'=>$states));
       }else{
			return $this->redirect(array('controller'=>'Users','action' => 'index'));
	   }
}
//Update on 30th April 2015
	public function admin_guest_credits_cron(){
		$this->layout=$this->autoRender=false;
		$month=date("m");
		$year=date("Y");
		$this->loadModel('Cron');
		$dt = new DateTime();
		$currentDateTime = $dt->format('m/d/Y H:i:s');
		$current_cron=$this->Cron->find('first',array('conditions'=>array('month'=>$month,'year'=>$year)));
		if(empty($current_cron)){ 
		 CakeLog::write('NoCronFound',$currentDateTime.' : Cron Creation Attempt On : '.$currentDateTime );
		 $array['Cron']=array('month'=>$month,'year'=>$year);
		 $this->Cron->create();
		 if($this->Cron->save($array)){
			CakeLog::write('NoCronFound',$currentDateTime.' : Cron Creation Attempt On : '.$currentDateTime.' was successful' );
			$cronId=$this->Cron->getLastInsertId();
			$this->loadModel('Property');
			$this->Property->recursive=-1;
			$allProperties=$this->Property->find('all',array('fields'=>array('id','name','free_guest_credits')));  
			$this->loadModel('CronJob');
			for($i=0;$i<count($allProperties);$i++){
				$arr['CronJob']=array('cron_id'=>$cronId,'property_id'=>$allProperties[$i]['Property']['id'],'credits_given'=>$allProperties[$i]['Property']['free_guest_credits']);
				$this->CronJob->create();
				$this->CronJob->save($arr);
			}
			for($i=0;$i<count($allProperties);$i++){
				if($this->User->query('UPDATE users set guest_credits='.$allProperties[$i]['Property']['free_guest_credits'].' where id in (select user_id from property_users where property_id='.$allProperties[$i]['Property']['id'].') and role_id=4')){
					$this->CronJob->updateAll(array('completed'=>1),array('cron_id'=>$cronId,'property_id'=>$allProperties[$i]['Property']['id']));
					CakeLog::write('guestCreditsUpdated','guestCreditsUpdated : All guest credits updated for property : '.$allProperties[$i]['Property']['name'].' credits assigned :'.$allProperties[$i]['Property']['free_guest_credits'].'' );
				}else{
					CakeLog::write('guestCreditsUpdateFailed','guestCreditsUpdateFailed : All guest credits updation for property : '.$allProperties[$i]['Property']['name'].' credits assigned :'.$allProperties[$i]['Property']['free_guest_credits'].' failed' );
				}
			}
			if(!($this->CronJob->hasAny(array('completed'=>0,'cron_id'=>$cronId)))){
				$this->Cron->updateAll(array('completed'=>1),array('id'=>$cronId));
				CakeLog::write('NoCronFound',$currentDateTime.' : All guest credits updated On : '.$currentDateTime.' was successful' );
			}else{
			
			}
		 }else{
			CakeLog::write('NoCronFound',$currentDateTime.' : Cron Creation Attempt On : '.$currentDateTime.' was un-successful' );
		 }
		}else{
			if(!$current_cron['Cron']['completed']){
				CakeLog::write('CronFound',$currentDateTime.' : Cron Found On : '.$currentDateTime.' but was not completed' );
				$this->update_cron($current_cron['Cron']['id']);
			}else{
				CakeLog::write('CronFound',$currentDateTime.' : Cron Found On : '.$currentDateTime.' but was completed' );
			}
		}
	}
	//Update on 30th April 2015
	public function update_cron($cron_id){
		$this->layout=$this->autoRender=false;
		$cronId=$cron_id;
		$this->loadModel('CronJob');
		$cronJobs=$this->CronJob->find('all',array('conditions'=>array('cron_id'=>$cronId,'completed'=>0)));
		$dt = new DateTime();
		$currentDateTime = $dt->format('m/d/Y H:i:s');
		if($cronJobs){
			CakeLog::write('CronFound',$currentDateTime.' : Cron Found On : '.$currentDateTime.' Attempted to update cron jobs for Cron Id : '.$cronId );
			$this->LoadModel('Property');
			for($i=0;$i<count($cronJobs);$i++){
				$credits=$this->Property->field('free_guest_credits',array('id'=>$cronJobs[$i]['CronJob']['property_id']));
				$property_name=$this->Property->field('name',array('id'=>$cronJobs[$i]['CronJob']['property_id']));
				if($this->User->query('UPDATE users set guest_credits='.$credits.' where id in (select user_id from property_users where property_id='.$cronJobs[$i]['CronJob']['property_id'].') and role_id=4')){
					$this->CronJob->updateAll(array('completed'=>1),array('cron_id'=>$cronId,'property_id'=>$cronJobs[$i]['CronJob']['property_id']));
					CakeLog::write('guestCreditsUpdated','guestCreditsUpdated : All guest credits updated for property name : '.$property_name.' credits assigned :'.$credits.' under cron ID: '.$cronId );
				}else{
					CakeLog::write('guestCreditsUpdateFailed','guestCreditsUpdateFailed : All guest credits updation for property name : '.$property_name.' credits assigned :'.$credits.' failed under cron ID:'.$cronId );
				}
			}
			if(!($this->CronJob->hasAny(array('completed'=>0,'cron_id'=>$cronId)))){
				if($this->Cron->updateAll(array('completed'=>1),array('id'=>$cronId))){
					CakeLog::write('CronFound',$currentDateTime.' : All guest credits updation attempt On : '.$currentDateTime.' was successful for cron ID: '.$cronId );
				}else{
					CakeLog::write('CronFound',$currentDateTime.' : All guest credits updation attempt On : '.$currentDateTime.' was un-successful for Cron ID: '.$cronId );
				}
			}
		}else{
			if($this->Cron->updateAll(array('completed'=>1),array('id'=>$cronId))){
				CakeLog::write('CronFound',$currentDateTime.' : All guest credits updation attempt On : '.$currentDateTime.' was already completed for cron ID : '.$cronId );
			}else{
				CakeLog::write('CronFound',$currentDateTime.' : All guest credits updation attempt On : '.$currentDateTime.' was already completed for cron ID : '.$cronId.' .But Cron Has not been been marked completed ');
			}
		}
	}
	public function admin_user_details_corrected($archived=false){
		$conditions=array('User.id !='=>AuthComponent::user('id'));
		if($archived){
			$conditions=' User.archived=1 ';
		}else{
			$conditions=' User.archived=0 ';
		}
		if($this->request->query['key']){
			$conditions.=' AND User.'.$this->request->query['search_option'].' LIKE "%'.trim($this->request->query['key']).'%"';
			//$conditions.=' AND User.'.$this->request->query['search_option'].' = "'.trim($this->request->query['key']).'"';
		}
		$aColumns = array('User.first_name', 'User.last_name', 'User.username', 'User.email', 'User.phone','User.address_line_1','User.address_line_2','Role.role_name','properties.name','User.id','property_users.role_id','User.archived');
        $sIndexColumn = " User.id ";
        $sTable = " users User ";
        $sJoinTable=' INNER JOIN property_users on User.id=property_users.user_id
					  INNER JOIN roles Role on Role.id=User.role_id
					  INNER JOIN properties on properties.id=property_users.property_id
         ';
        $sConditions=$conditions;
       
        $returnArr=$this->NewDatatable->getData(array('columns'=>$aColumns,'index_column'=>$sIndexColumn,'table'=>$sTable,'join'=>$sJoinTable,'conditions'=>$sConditions));
        echo json_encode($returnArr);
        die;
	 }
	 public function admin_unassigned_user(){
		 $this->User->recursive=-1;
		$unassignedUsers=$this->User->find('all',array(
				'fields' => array('User.first_name', 'User.last_name', 'User.username', 'User.email', 'User.phone','User.address_line_1','User.address_line_2','Role.role_name','User.id','User.role_id'),
				'joins' => array(
								array(
									'alias' => 'Role',
									'table' => 'roles',
									'type' => 'INNER',
									'conditions' => 'Role.id=User.role_id'
								)
							),
				'conditions'=>array('User.id NOT IN (SELECT DISTINCT user_id from property_users) AND User.id  != '.AuthComponent::user('id').' AND role_id != 4')
			)			
		);
		$result = Hash::map($unassignedUsers, "{n}", function($newArr){
						 $str = "<a class='btn green btn-xs green-stripe' href='/admin/users/edit/".$newArr['User']['id']."'>Edit</a>&nbsp;
										  &nbsp;<a class='btn default btn-xs ' href='/admin/users/view_user_details/".$newArr['User']['id']."'>View</a>&nbsp;
										  &nbsp;<a class='btn yellow btn-xs yellow-stripe' href='/admin/users/print_details/".$newArr['User']['id']."'target='_blank'>Print</a>&nbsp;
										  ";
						 if($newArr['Role']['role_name']!='customer'){
							 $uniqID=uniqid();
							 $str= $str.'<form action="/admin/Users/delete/'.$newArr['User']['id'].'" name="post_'.$uniqID.'" id="post_'.$uniqID.'" style="display:none;" method="post"><input type="hidden" name="_method" value="POST"></form><a href="#" onclick="if (confirm(&quot;Are you sure?&quot;)) { document.post_'.$uniqID.'.submit(); } event.returnValue = false; return false;" class="btn red btn-xs red-stripe">Delete</a>';
							 
						   }
						 if($newArr['Role']['role_name']=='customer'){
							 $str .= "<a class='btn red btn-xs red-stripe' href='/admin/CustomerPasses/customer_view/".$newArr['User']['id']."'>Manage</a>&nbsp;
									  &nbsp;<a class='btn default btn-xs green-stripe' href='/admin/tickets/contact_user/".$newArr['User']['id']."'>Contact</a>";
							}
						$newArr['User']['id']=$str;
						return $newArr;
				});		
		$this->set(compact('result'));
		//debug($result);die;
	}
	public function resend_approval_link($key){
		$this->layout='login';
		if($key){
			$user=$this->User->find('first',array('conditions'=>array('aproval_code'=>$key)));
			if($user){
				if($this->request->is('POST')){
					$sendMail=false;
					$email=$user['User']['email'];
					if($this->request->data['User']['email']){
						 $this->User->set($this->request->data);
						 if ($this->User->validates(array('fieldList' => array('email')))) {	
							$email=$this->request->data['User']['email'];
							$arr['User']=array(
												"id"=>$user['User']['id'],
												"email"=>$email
							);
							if($this->User->save($arr,false)){
								$sendMail=true;
							}else{
								$this->Session->setFlash(__('Internal Error, Please try later.'),'error');
							}
						 }
					}
					if($sendMail){
						$property = $this->request->params['Property'];
						$url = Router::url( array('controller'=>'users','action'=>'verify_email'), true ).'/'.$key;
						$this->Email->from = $property['name'].'<noreply@' . $_SERVER['HTTP_HOST'] . '>';
						$this->Email->to = $email;
						$this->Email->sendAs = 'html';
						$this->Email->subject = 'Email Verification Link';
						$this->Email->smtpOptions=Configure::read('SMTP_SETTING');
						$this->Email->delivery='smtp';
						$message='Dear Customer,<br>Please <a href="'.$url.'">Click Here</a> to verify your email.<br>
								  OR<br>
								  Copy the following link and open in new tab.
								  <br>
								  '.$url.'
								  <br>
								 Thanks,
								  <br>
								  <b>Administrator</b><br>
								  '.$_SERVER['HTTP_HOST'].'
									';
						if($this->Email->send($message)){
							$this->Session->setFlash(__('Please check your email to verify your email address.'),'success');
							
						}else{
							$this->Session->setFlash(__('Account approval email cannot be sent. Please contact administrator.'),'error');
						}
						$this->redirect(array('action'=>'login'));
					}
				}
				$this->set(compact('key','user'));
			}else{
				$this->Session->setFlash('Error Occured. Try To Login Again','error');
				$this->redirect(array('action'=>'login'));
			}
		}else{
			$this->Session->setFlash(__('Account approval email cannot be sent. Please contact administrator.'),'error');
			$this->redirect(array('action'=>login));
		}
	}
	public function verify_email($approvalkey){
		if($approvalkey){
			$user=$this->User->find('first',array('conditions'=>array('aproval_code'=>$approvalkey)));
			//debug($user);die;
			if($user){
					$arr['User']=array(
										'id'=>$user['User']['id'],
										'approved'=>1,
										'aproval_code'=>NULL
										);
					$this->User->clear();
					if($this->User->save($arr,array('validate'=>false))){
						$this->Session->setFlash('Email Verified Successfully, Please Login Now','success');
						$this->redirect(array('action'=>'login'));
					}else{
						$this->Session->setFlash('Email Cannot Be Verified','error');
						$this->redirect(array('action'=>'login'));
					}
			}else{
				$this->Session->setFlash('Link Has Been Expired. Try To Login.','error');
				$this->redirect(array('action'=>'login'));
			}
		}else{
			throw new NotFoundException(__('No Approval Link'));
		}
	}
	/*******************************************************************
	 * function to archive a user
	 */
	 public function admin_archive_user($userID){
		$result=$this->getUserArchived($userID);
		if($result){
			$this->Session->setFlash('User marked archived','success');
		}else{
			$this->Session->setFlash('User is already archived. Try Again','error');
		}
		$this->redirect($this->referer());
	 }
	 public function admin_archive_list(){
	 
	 }
	 public function admin_create_csv($userID){
		$this->User->recursive=-1;
		$user=$this->User->find('first',array('conditions'=>array('User.id'=>$userID)));
		$this->loadModel('CustomerPass');
		$customerPasses=$this->CustomerPass->find('all',array('conditions'=>array('CustomerPass.user_id'=>$userID)));
		header("Content-type: application/csv");
		header("Content-Disposition: attachment; filename=".$user['User']['username'].".csv");
		header("Pragma: no-cache");
		header("Expires: 0");
		header("Content-Type: application/octet-stream"); 
		$fp = fopen('php://output', 'w');
		$row=array(
					'Full Name',
					'User Name',
					'Email',
					'Phone',
					'Address',
					'Vehicle Name',
					'Make',
					'Model',
					'Color',
					'Plate Number',
					'State',
					'VIN',
					'Parking Location',
					'Pass Name',
					'Status',
					'Pass Expiration Date',
					'RFID Tag'
		);
		fputcsv($fp, $row); 
		$dt = new DateTime();
		$currentDateTime = $dt->format('Y-m-d H:i:s');
		foreach($customerPasses as $pass){
			$status_msg='';
			if ($pass['CustomerPass']['pass_valid_upto'] < $currentDateTime) {
				$status = "PassExpired";
				$status_msg = "Pass Expired";
				if (is_null($pass['CustomerPass']['package_id'])) {
					$status_msg = $status_msg . ", Package Was Not Bought";
				}
			} else {
				if (is_null($pass['CustomerPass']['package_id'])) {
					$status = "PassValid";
					$status_msg = "Pass Is Valid But Package Not Bought";
				} else {
					if ($pass['CustomerPass']['membership_vaild_upto'] < $currentDateTime) {
						$status = "PassExpired";
						$status_msg = "Pass Is Valid But Package Expired";
					} else {
						$status = "PassActive";
						$status_msg = "Pass Active";
					}
				}
			}
			
			$data=array(
						$pass['User']['first_name'].' '.$pass['User']['last_name'],
						$pass['User']['username'],
						$pass['User']['email'],
						$pass['User']['phone'],
						$pass['User']['address_line_1'].' '.$pass['User']['address_line_2'].' '.$pass['User']['city'].' '.$pass['User']['state'].' '.$pass['User']['zip'],
						$pass['Vehicle']['owner']?$pass['Vehicle']['owner']:'',
						$pass['Vehicle']['make']?$pass['Vehicle']['make']:'',
						$pass['Vehicle']['model']?$pass['Vehicle']['model']:'',
						$pass['Vehicle']['color']?$pass['Vehicle']['color']:'',
						$pass['Vehicle']['license_plate_number']?$pass['Vehicle']['license_plate_number']:'',
						$pass['Vehicle']['license_plate_state']?$pass['Vehicle']['license_plate_state']:'',
						$pass['Vehicle']['last_4_digital_of_vin']?$pass['Vehicle']['last_4_digital_of_vin']:'',
						$pass['Package']['name']?$pass['Package']['name']:'',
						$pass['Pass']['name']?$pass['Pass']['name']:'',
						$status_msg,
						$pass['CustomerPass']['pass_valid_upto']?date('m-d-Y H:i:s',strtotime($pass['CustomerPass']['pass_valid_upto'])):'',
						$pass['CustomerPass']['RFID_tag_number']?$pass['CustomerPass']['RFID_tag_number']:''
			);
			fputcsv($fp, $data); 
		}	
		stream_get_contents($fp);
		die;
	 }
	 /*******************************************************************
	 * function to restore a user
	 */
	 public function admin_restore_user($userID){
		$result=$this->restoreUser($userID);
		if($result){
			$this->Session->setFlash('Request processed successfully','success');	
		}else{
			$this->Session->setFlash('Request cannot be processed. Try Again.','error');
		}
		return $this->redirect($this->referer());
	 }
	 public function approve_user($passID,$status){
		 $this->loadModel('CustomerPass');
		 $this->CustomerPass->id=$passID;
		 if($this->CustomerPass->saveField('approved',$status)){
			 if($status==0){
				$customerPass=$this->CustomerPass->find('first',array(
																	   'conditions'=>array(
																							'CustomerPass.id'=>$passID
																	   )
																	 ));
				if($customerPass){
				   $this->getPassArchived($passID);
				}
			 }
			 $this->Session->setFlash('Request processed successfully','success');
		 }else{
			$this->Session->setFlash('Request cannot be processed. Try Again.','error');
		 }		
		 return $this->redirect($this->referer());
	 }
	 public function smsSubscription(){
		$this->User->id=$this->Auth->user('id');
		$this->User->saveField('un_subscribed',$this->request->data['User']['un_subscribed']);
		if($this->request->data['User']['un_subscribed']){
			$this->Session->setFlash('Sms notification un-subscribed successfully.','success');
		}else{
			$this->Session->setFlash('Sms notification subscribed successfully.','success');
		}
		return $this->redirect($this->referer());
	 }
	 /***************************************************************
	  * getUserArchived function to archive a single user
	  * 
	  * 
	  */
	 private function getUserArchived($userID){
		$ifArchived=$this->User->field('archived',array('User.id'=>$userID)); 			
		if($ifArchived==0){				
			if($this->User->updateAll(array('archived'=>1),array('User.id'=>$userID))){
				if($this->User->Vehicle->updateAll(array('vehicle_archived'=>1),array('Vehicle.user_id'=>$userID))){
						if($this->User->CustomerPass->updateAll(array('pass_archived'=>1),array('CustomerPass.user_id'=>$userID))){
							 $this->User->CustomerPass->clear();
							 $allPasses=$this->User->CustomerPass->find('all',array(
																	'conditions'=>array('CustomerPass.user_id'=>$userID)
														));
							 foreach($allPasses as $customerPass){
								$arr['CustomerPass']=array(
															'id'=>$customerPass['CustomerPass']['id'],
															'pass_before_archive'=>$customerPass['CustomerPass']['pass_valid_upto'],
															'package_before_archive'=>$customerPass['CustomerPass']['membership_vaild_upto'],
															'pass_valid_upto'=>$customerPass['CustomerPass']['pass_valid_upto']?date('Y-m-d', strtotime(' -1 day')):$customerPass['CustomerPass']['pass_valid_upto'],
															'membership_vaild_upto'=>$customerPass['CustomerPass']['membership_vaild_upto']?date('Y-m-d', strtotime(' -1 day')):$customerPass['CustomerPass']['membership_vaild_upto']
								);
								$this->User->CustomerPass->clear();
								$this->User->CustomerPass->save($arr, array('validate' => false));
							 }
							CakeLog::write('UserArchived','User archived with user id : '.$userID.' Archived by  : '.AuthComponent::user('username').' NAME : '. AuthComponent::user('first_name') . ' ' . AuthComponent::user('last_name'));
							return true;
						}else{
							$this->User->updateAll(array('archived'=>0),array('User.id'=>$userID));
							$this->User->Vehicle->updateAll(array('vehicle_archived'=>0),array('Vehicle.user_id'=>$userID));
							return false;
						}
				}else{
					$this->User->updateAll(array('archived'=>0),array('User.id'=>$userID));
					return false;
				}
			}else{
				return false;
			}
		}else{
			return true;
		}
	 }
	 /***************************************************************
	  * getPassArchived function to archive a single pass
	  * 
	  * 
	  */
	  private function getPassArchived($id){
		$this->loadModel('CustomerPass');
		$this->CustomerPass->id = $id;
		if (!$this->CustomerPass->exists()) {
			throw new NotFoundException(__('Invalid customer pass'));
		}
		$this->CustomerPass->unbindModel(array('belongsTo'=>'Transaction'));
		$customerPassData=$this->CustomerPass->find('first',array('conditions'=>array('CustomerPass.id'=>$id)));
		$this->loadModel('DeletedCustomerPass');
		$arr=array(
					'by_user'=>AuthComponent::user('id'),
					'of_user'=>$customerPassData['User']['id'],
					'pass_data'=>json_encode($customerPassData)
		);
		if($this->DeletedCustomerPass->save($arr)){
			//Add Refund Entry
			$this->loadModel('Pass');
			$passName=$this->Pass->givePassName($customerPassData['CustomerPass']['pass_id']);
			$resfundArr['Refund']=[
									'user_id'=>$customerPassData['CustomerPass']['user_id'],
									'property_id'=>$customerPassData['CustomerPass']['property_id'],
									'transaction_id'=>$customerPassData['CustomerPass']['transaction_id'],
									'pass'=>$passName,
									'permit'=>$customerPassData['Package']['name'],
									'pass_cost'=>$customerPassData['CustomerPass']['pass_cost'],
									'pass_deposit'=>$customerPassData['CustomerPass']['pass_deposit'],
									'package_cost'=>$customerPassData['CustomerPass']['package_cost'],
									'total_cost'=>$customerPassData['CustomerPass']['pass_cost']+$customerPassData['CustomerPass']['pass_deposit']+$customerPassData['CustomerPass']['package_cost'],
									'deleted_customer_pass_id'=>$this->DeletedCustomerPass->getLastInsertID()
								  ];
			
			$this->loadModel('Refund');
			if($this->Refund->save($resfundArr)){
				CakeLog::write('deletePassRefund', ''.AuthComponent::user('username').' BACKUP DONE CUSTOMER PASS ID : '.$id.' Pass Name : '.$passName.' OF USER ID : '.$customerPassData['User']['id'].' WITH REFUND ID : '.$this->Refund->getLastInsertID());
			}
			CakeLog::write('deletePassInitiated', ''.AuthComponent::user('username').' BACKUP DONE CUSTOMER PASS ID : '.$id.' Pass Name : '.$passName.' OF USER ID : '.$customerPassData['User']['id']);
			if($customerPassData['CustomerPass']['vehicle_id']){
				$this->loadModel('Vehicle');
				$this->CustomerPass->updateAll(array('CustomerPass.vehicle_id'=>NULL),array('CustomerPass.id'=>$id));
				if($this->Vehicle->updateAll(array('Vehicle.customer_pass_id'=>NULL),array('Vehicle.customer_pass_id'=>$id))){
					CakeLog::write('vehicleDetached', ''.AuthComponent::user('username').' Vehicle Removed VEHICLE ID : '.$customerPassData['CustomerPass']['vehicle_id'].' CUSTOMER PASS ID : '.$id);
				}
			}
			//DELETE ChangeVehicleDetail Requests
			$this->loadModel('ChangeVehicleDetail');
			$this->ChangeVehicleDetail->deleteAll(array('ChangeVehicleDetail.customer_pass_id' =>$id), false);
			//DELETE BACKUPS
			$this->loadModel('CustomerPassBackup');
			$this->CustomerPassBackup->deleteAll(array('CustomerPassBackup.previous_id' =>$id), false);
			//DELETE BACKUPS
			$this->loadModel('LastSeenVehicle');
			$this->LastSeenVehicle->deleteAll(array('LastSeenVehicle.customer_pass_id' =>$id), false);
			
			$this->loadModel('RecurringProfile');
			$recurringProfile=$this->RecurringProfile->find('all',array('conditions'=>array('RecurringProfile.customer_pass_id'=>$id)));
			if($recurringProfile){
				foreach($recurringProfile as $recProfile){
					$result=$this->PayFlow->deactivate_profile($recProfile['RecurringProfile']['recurring_profile_id']);
					if($result){
						if($result['RESULT']==0 && $result['RESPMSG']=='Approved'){
								$arr1['RecurringProfile']=array('id'=>$recProfile['RecurringProfile']['id'],
																'status'=>'Cancelled'
															);
								if($this->RecurringProfile->save($arr1,false)){
									CakeLog::write('ProfileDeactivation',$recProfile['RecurringProfile']['id'].' Recurring Profile deactivation COMPLETED... RECURRING PROFILE ID : '.$recProfile['RecurringProfile']['id'].'  CUSTOMER PASS ID : '.$id);
								}else{
									CakeLog::write('ProfileDeactivation',$recProfile['RecurringProfile']['id'].' Recurring Profile deactivation DONE BUT NOT SAVED... RECURRING PROFILE ID : '.$recProfile['RecurringProfile']['id'].'  CUSTOMER PASS ID : '.$id);
								}
						}else{
							CakeLog::write('ProfileDeactivation',$recProfile['RecurringProfile']['id'].' Recurring Profile deactivation failed... RECURRING PROFILE ID : '.$recProfile['RecurringProfile']['id'].'  CUSTOMER PASS ID : '.$id);
						}
					}else{
						CakeLog::write('DeactivateRecurring', ''.AuthComponent::user('username').' NO RESULT....RECURRING PROFILE ID : '.$recProfile['RecurringProfile']['id'].' CUSTOMER PASS ID : '.$id);
					}
				}
			}
			//DELETE UserGuestPass Data
			$this->loadModel('UserGuestPass');
			$this->UserGuestPass->deleteAll(array('UserGuestPass.customer_pass_id' =>$id), false);
			
			$this->request->allowMethod('post', 'delete');
			if ($this->CustomerPass->delete()) {
				$this->Session->setFlash(__('The customer pass has been deleted.'));
				CakeLog::write('deletePassCompleted', ''.AuthComponent::user('username').' DELETED CUSTOMER PASS ID : '.$id.' Pass Name : '.$passName.' OF USER ID : '.$customerPassData['User']['id']);
			} else {
				$this->Session->setFlash(__('The customer pass could not be deleted. Please, try again.'));
			}
		}else{
			$this->Session->setFlash(__('The customer pass could not be deleted. Please, try again.'),'error');
		}
		
	 }
	 /***************************************************************
	  * admin_archive_all function to archive all the users ina particular listing
	  * 
	  * 
	  */
	 public function admin_archive_all($propertyID){
		$this->loadModel('PropertyUser');
		$this->loadModel('Property');
		$this->PropertyUser->recursive=-1;
		$propertyUsers=$this->PropertyUser->find('all',array(
											  'conditions'=>array(
																'property_id'=>$propertyID,
																'role_id'=>4
											  )
										));
		$IsError=false;
		foreach($propertyUsers as $propertyUser){
			$returnResult=$this->getUserArchived($propertyUser['PropertyUser']['user_id']);
			if($returnResult==false){	
				$IsError=true;
			}
		}
		
		if($IsError==false){
			$this->Property->updateAll(array('archived'=>1),array('Property.id'=>$propertyID));
			$this->Session->setFlash('Property archived successfully.','success');
		}else{  
			foreach($propertyUsers as $propertyUser){
				$this->restoreUser($propertyUser['PropertyUser']['user_id']);
			}
			$this->Session->setFlash('Property cannot be archived','error');
		}
		return $this->redirect($this->referer());
	}
	/***************************************************************
	  * admin_restore_all function to archive all the users ina particular listing
	  * 
	  * 
	  */
	public function admin_restore_all($propertyID){
		$this->loadModel('PropertyUser');
		$this->loadModel('Property');
		$this->PropertyUser->recursive=-1;
		$propertyUsers=$this->PropertyUser->find('all',array(
											  'conditions'=>array(
																'property_id'=>$propertyID,
																'role_id'=>4
											  )
										));
		$IsError=false;
		foreach($propertyUsers as $propertyUser){
			$returnResult=$this->restoreUser($propertyUser['PropertyUser']['user_id']);
			if(!$returnResult){
				$IsError=true;
			}
		}
		
		if($IsError==false){
			$this->Property->updateAll(array('archived'=>0),array('Property.id'=>$propertyID));
			$this->Session->setFlash('Property restored successfully.','success');
		}else{
			foreach($propertyUsers as $propertyUser){
				$this->getUserArchived($propertyUser['PropertyUser']['user_id']);
			}
			$this->Session->setFlash('Property cannot be restored','error');
		}
		return $this->redirect($this->referer());
	}
	 /***************************************************************
	  * restoreUser function to archive restore a single user
	  * 
	  * 
	  */
	private function restoreUser($userID){
		    $this->loadModel('User');
			if($this->User->updateAll(array('archived'=>0),array('User.id'=>$userID))){
				if($this->User->Vehicle->updateAll(array('vehicle_archived'=>0),array('Vehicle.user_id'=>$userID))){
					if($this->User->CustomerPass->updateAll(array('pass_archived'=>0),array('CustomerPass.user_id'=>$userID))){
						$this->User->CustomerPass->clear();
						$allPasses=$this->User->CustomerPass->find('all',array(
																		'conditions'=>array('CustomerPass.user_id'=>$userID)
															));
						 foreach($allPasses as $customerPass){
							$arr['CustomerPass']=array(
														'id'=>$customerPass['CustomerPass']['id'],
														'pass_valid_upto'=>$customerPass['CustomerPass']['pass_before_archive'],
														'membership_vaild_upto'=>$customerPass['CustomerPass']['package_before_archive']
							);
							$this->User->CustomerPass->clear();
							$this->User->CustomerPass->save($arr, array('validate' => false));
						 }
						
						
						CakeLog::write('UserArchived','User archived with user id : '.$userID.' Archived by  : '.AuthComponent::user('username').' NAME : '. AuthComponent::user('first_name') . ' ' . AuthComponent::user('last_name'));
						return true;
					}else{
						$this->User->updateAll(array('archived'=>0),array('User.id'=>$userID));
						$this->User->Vehicle->updateAll(array('vehicle_archived'=>0),array('Vehicle.user_id'=>$userID));
						return false;
					}
				}else{
					$this->User->updateAll(array('archived'=>0),array('User.id'=>$userID));
					return false;
				}
			}else{
				return false;
			}
		return false;
	 }
}
