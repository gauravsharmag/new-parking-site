<?php
App::uses('CakeEmail', 'Network/Email');
class TicketsController extends AppController {

    public $components = array('Email','DataTable');
	 public function beforeFilter() {
		parent::beforeFilter();
            $this->Security->unlockedActions = array('admin_send_email_bulk','admin_get_tickets','admin_get_tickets_user');
        }
	
     public function contact_us() {
        $this->layout = 'customer';
        if ($this->request->is('post')) {
			$this->loadModel('PropertyUser');
			$propertyManager=$this->PropertyUser->find('first',array('conditions'=>array('property_id'=>$_SESSION['PropertyId'],'PropertyUser.role_id'=>3),'fields'=>array('User.email')));
			
            $this->request->data['Ticket']['user_id'] = AuthComponent::user('id');
            $this->request->data['Ticket']['recipient_id'] = AuthComponent::user('id');
            $this->request->data['Ticket']['property_name'] = $this->Session->read('PropertyName');
            $this->request->data['Ticket']['reply_admin'] = 0;
            $this->Ticket->set($this->request->data);
            if($this->Ticket->validates(array('fieldList'=>array('subject','message','email')))){
				$this->Ticket->create();
				if ($this->Ticket->save($this->request->data,false)) {
					$url = Router::url(array('controller' => 'ticket_replies', 'action' => 'view_single', $this->Ticket->getLastInsertId()), true);
					$ms = $url;
					$ms = wordwrap($ms, 1000);
					$this->Email->template = 'contact';
					$this->Email->from = 'New Query from ' . $this->Auth->user('username') . ' <alerts@netparkingpass.com>';
					$this->Email->to = $propertyManager['User']['email'];
					$this->Email->subject = $this->request->data['Ticket']['subject'];
					$this->Email->sendAs = 'both';
					$userName=$this->Auth->user('username');
					$emailId=  $this->Auth->user('email');   
					$propertyName= $this->Session->read('PropertyName'); 
					$this->set(compact('ms','userName','emailId','propertyName'));
					$this->Email->smtpOptions=Configure::read('SMTP_SETTING');
					$this->Email->delivery='smtp';
					try {
						if ($this->Email->send()) {
							$this->Session->setFlash('Your message has been delivered, we will contact you soon.', 'success');
						}
					} catch (Exception $e) {
						$this->Session->setFlash('Message not sent.', 'error');
					}
					return $this->redirect(array('controller' => 'ticket_replies', 'action' => 'view_single', $this->Ticket->getLastInsertId()));
				}else{
					$this->Session->setFlash('Message not sent.', 'error');
				} 
            
              } else {
                $this->Session->setFlash('Validation Error', 'error');
            }
        }
    }
    
     public function admin_super_admin_contact() {
        $this->loadModel('User');
        $this->User->recursive = -1;
        //$usernameVal=$this->User->find('list',array('fields'=>array('User.username')));
         $usernameVal = $this->User->find("all", array(
            'fields' => array('User.username','User.id','p.name'),
            'joins' => array(
                array(
                    'table' => 'property_users',
                    'alias' => 'pu',
                    'type' => 'LEFT',
                    'conditions' => array(
                        'pu.user_id = User.id'
                    )),
                array(
                    'table' => 'properties',
                    'alias' => 'p',
                    'type' => 'LEFT',
                    'conditions' => array(
                        'p.id = pu.property_id'
                    )),
            )
        )); //debug($usernameVal); die;
        $this->set(compact('usernameVal'));
        if ($this->request->is('post')) {
            // debug($this->request->data);
            $this->loadModel('User');
        $this->User->recursive = -1;
            $id=$this->request->data['Ticket']['recipient_id'];
            $ticket = $this->User->find("first", array(
            'fields' => array('User.email','User.phone', 'User.username', 'p.sub_domain','p.name'),
            'joins' => array(
                array(
                    'table' => 'property_users',
                    'alias' => 'pu',
                    'type' => 'LEFT',
                    'conditions' => array(
                        'pu.user_id = User.id'
                    )),
                array(
                    'table' => 'properties',
                    'alias' => 'p',
                    'type' => 'LEFT',
                    'conditions' => array(
                        'p.id = pu.property_id'
                    )),
            ),
            'conditions' => array('User.id' => $id),
        ));

            // debug( $ticket);die;   

  
           $this->Ticket->set($this->request->data);
            if($this->Ticket->validates(array('fieldList'=>array('recipient_id','subject','message')))){
            $this->request->data['Ticket']['reply_admin'] = 1;
            $this->request->data['Ticket']['user_reply'] = 0;
            $this->request->data['Ticket']['user_id'] = $this->Auth->user('id');
            $this->request->data['Ticket']['email'] = $ticket['User']['email'];
            $this->request->data['Ticket']['phone'] = $ticket['User']['phone'];
            $this->request->data['Ticket']['property_name'] =  $ticket['p']['name'];
            $this->request->data['Ticket']['recipient_id']=$id;
           // debug($this->request->data); die;
            $this->Ticket->create();
            if ($this->Ticket->save($this->request->data)) {
                $url = "http://" . $ticket['p']['sub_domain'] .".". Configure::read('SITE_URL') . "/ticket_replies/view_single/" . $this->Ticket->getLastInsertId();
                $url2 = "http://" . $ticket['p']['sub_domain'] .".". Configure::read('SITE_URL') . "/users/login";
                $ms = $url;
                $ms = wordwrap($ms, 1000);
                $ms2 = $url2;
                $ms2 = wordwrap($ms2, 1000);
                $this->Email->template = 'contact_user';
                $this->Email->from = 'Message from ' . $this->Auth->user('username') . ' (Admin)' . ' <alerts@netparkingpass.com>';
                $this->Email->to = $ticket['User']['email'];
                $this->Email->subject = $this->request->data['Ticket']['subject'];
                $this->Email->sendAs = 'both';
                $emailId=$ticket['User']['email'];
                $userName=$ticket['User']['username'];
                $propertyName=$ticket['p']['name'];
                $this->set(compact('ms','ms2','propertyName','userName','emailId'));
                $this->Email->smtpOptions=Configure::read('SMTP_SETTING');
				$this->Email->delivery='smtp';
                $this->Email->send();
                $this->Session->setFlash('Your message has been delivered', 'success');
                return $this->redirect(array('controller' => 'ticket_replies', 'action' => 'view_single', $this->Ticket->getLastInsertId()));
            }
            
             else {
                $this->Session->setFlash('Cant contact', 'error');
            }
            }
        }
    }

    
    
    
    

    public function view() {
        $this->layout = 'customer';
        $this->Ticket->recursive = 1;
        $this->paginate = array(
            'limit' => 20,
            'conditions' => array('OR'=>array('Ticket.recipient_id' => $this->Auth->user('id'),'Ticket.user_id' => $this->Auth->user('id'))), 'order' => array('Ticket.modified DESC')
        );
        $viewQueries = $this->paginate('Ticket');
        $this->set(compact('viewQueries'));
    }

    public function admin_view() {
       
            $this->Ticket->recursive = 1;
            $this->paginate = array(
                'limit' => 15,
                'order' => array('Ticket.created DESC')
            );
            $viewQueries = $this->paginate('Ticket');
        
        $this->set(compact('viewQueries'));
    }
    
    public function manager_view() {
        $this->layout = "manager";
        $this->Ticket->recursive = 1;
        $this->paginate = array(
            'limit' => 20,
            'order' => array('Ticket.created DESC'),
            'conditions' => array('Ticket.property_id' => $this->Session->read('PropertyId'), 'Ticket.sender!=2')
        );
        $viewQueries = $this->paginate('Ticket');
        $this->set(compact('viewQueries'));
    }

    public function admin_contact($id = null) {
        $this->layout = 'administrator';
        $this->Session->write('PropertySelection', true);
        $this->loadModel('User');
        $this->User->recursive = -1;
        $ticket = $this->User->find("first", array(
            'fields' => array('User.email', 'p.sub_domain'),
            'joins' => array(
                array(
                    'table' => 'property_users',
                    'alias' => 'pu',
                    'type' => 'LEFT',
                    'conditions' => array(
                        'pu.user_id = User.id'
                    )),
                array(
                    'table' => 'properties',
                    'alias' => 'p',
                    'type' => 'LEFT',
                    'conditions' => array(
                        'p.id = pu.property_id'
                    )),
            ),
            'conditions' => array('User.id' => $id),
        ));
        if ($this->request->is('post')) {
            $this->request->data['Ticket']['user_id'] = $this->Auth->user('id');
            $this->request->data['Ticket']['reply_admin'] = 1;
            $this->request->data['Ticket']['reply_manager'] = 1;
            $this->request->data['Ticket']['my_reply'] = 0;
            $this->request->data['Ticket']['sender'] = 2;
            $this->request->data['Ticket']['sender_id'] = $this->Auth->user('id');
            $this->request->data['Ticket']['email'] = $this->Auth->user('email');
            $this->request->data['Ticket']['phone'] = $this->Auth->user('phone');
            $this->request->data['Ticket']['property_id'] = $this->Session->read('PropertyId');
            $this->request->data['Ticket']['property_name'] = $this->Session->read('PropertyName');
            $this->request->data['Ticket']['recipient_id']=$id;
            $this->Ticket->create();
            if ($this->Ticket->save($this->request->data)) {
                $url = "//" . $ticket['p']['sub_domain'] .".". Configure::read('SITE_URL') . "/ticket_replies/view_single/" . $this->Ticket->getLastInsertId();
                $ms = $url;
                $ms = wordwrap($ms, 1000);
                $this->Email->template = 'contact_user';
                $this->Email->from = 'Message from ' . $this->Auth->user('username') . ' (Admin)' . ' <alerts@netparkingpass.com>';
                $this->Email->to = $ticket['User']['email'];
                $this->Email->subject = $this->request->data['Ticket']['subject'];
                $this->Email->sendAs = 'both';
                $this->set('ms', $ms);
                 $this->Email->smtpOptions=Configure::read('SMTP_SETTING');
				$this->Email->delivery='smtp';
                $this->Email->send();
                $this->Session->setFlash('Your message has been delivered', 'success');
                return $this->redirect(array('controller' => 'ticket_replies', 'action' => 'view_single', $this->Ticket->getLastInsertId()));
            } else {
                $this->Session->setFlash('Cant contact', 'error');
            }
        }
    }

    public function manager_contact($id = null) {
        $this->layout = 'manager';
        $this->loadModel('User');
        $ticket = $this->User->findById($id);
        if ($this->request->is('post')) {
            $this->request->data['Ticket']['user_id'] = $this->Auth->user('id');
            $this->request->data['Ticket']['reply_admin'] = 1;
            $this->request->data['Ticket']['reply_manager'] = 1;
            $this->request->data['Ticket']['my_reply'] = 0;
            $this->request->data['Ticket']['sender'] = 3;
            $this->request->data['Ticket']['sender_id'] = $this->Auth->user('id');
            $this->request->data['Ticket']['email'] = $this->Auth->user('email');
            $this->request->data['Ticket']['phone'] = $this->Auth->user('phone');
            $this->request->data['Ticket']['property_id'] = $this->Session->read('PropertyId');
            $this->request->data['Ticket']['property_name'] = $this->Session->read('PropertyName');
            $this->request->data['Ticket']['recipient_id']=$id;
            $this->Ticket->create();
            if ($this->Ticket->save($this->request->data)) {
                $url = "//" . $_SERVER['HTTP_HOST'] . "/ticket_replies/view_single/" . $this->Ticket->getLastInsertId();
                $ms = $url;
                $ms = wordwrap($ms, 1000);
                $this->Email->template = 'contact_user';
                $this->Email->from = 'Message from ' . $this->Auth->user('username') . ' (Manager)' . ' <alerts@netparkingpass.com>';
                $this->Email->to = $ticket['User']['email'];
                $this->Email->subject = $this->request->data['Ticket']['subject'];
                $this->Email->sendAs = 'both';
                $this->set('ms', $ms);
                 $this->Email->smtpOptions=Configure::read('SMTP_SETTING');
				$this->Email->delivery='smtp';
                $this->Email->send();
                $this->Session->setFlash('Your message has been delivered', 'success');
                return $this->redirect(array('controller' => 'ticket_replies', 'action' => 'manager_view_single', $this->Ticket->getLastInsertId()));
            } else {
                $this->Session->setFlash('Cant contact', 'error');
            }
        }
    }

    public function admin_contact_super_admin() {

        $this->layout = 'administrator';
        $this->Session->write('PropertySelection', true);
        if ($this->request->is('post')) {
            $this->request->data['Ticket']['user_id'] = 0;
            $this->request->data['Ticket']['reply_admin'] = 5;
            $this->request->data['Ticket']['reply_manager'] = 5;
            $this->request->data['Ticket']['reply_super_admin'] = 0;
            $this->request->data['Ticket']['sender'] = 2;
            $this->request->data['Ticket']['sender_id'] = $this->Auth->user('id');
            $this->request->data['Ticket']['email'] = $this->Auth->user('email');
            $this->request->data['Ticket']['phone'] = $this->Auth->user('phone');
            $this->request->data['Ticket']['property_id'] = $this->Session->read('PropertyId');
            $this->request->data['Ticket']['property_name'] = $this->Session->read('PropertyName');
            $this->Ticket->create();
            if ($this->Ticket->save($this->request->data)) {
                $this->Session->setFlash('Message sent to Super Admin', 'success');
                return $this->redirect(array('controller' => 'tickets', 'action' => 'view'));
            } else {
                $this->Session->setFlash('Cant contact', 'error');
            }
        }
    }

    public function manager_contact_super_admin($id = null) {
        $this->layout = 'manager';
        if ($this->request->is('post')) {
            $this->request->data['Ticket']['user_id'] = 0;
            $this->request->data['Ticket']['reply_admin'] = 5;
            $this->request->data['Ticket']['reply_manager'] = 5;
            $this->request->data['Ticket']['reply_super_admin'] = 0;
            $this->request->data['Ticket']['sender'] = 3;
            $this->request->data['Ticket']['sender_id'] = $this->Auth->user('id');
            $this->request->data['Ticket']['email'] = $this->Auth->user('email');
            $this->request->data['Ticket']['phone'] = $this->Auth->user('phone');
            $this->request->data['Ticket']['property_id'] = $this->Session->read('PropertyId');
            $this->request->data['Ticket']['property_name'] = $this->Session->read('PropertyName');
            $this->Ticket->create();
            if ($this->Ticket->save($this->request->data)) {
                $this->Session->setFlash('Message sent to Super Admin', 'success');
                return $this->redirect(array('controller' => 'tickets', 'action' => 'manager_view'));
            } else {
                $this->Session->setFlash('Cant contact', 'error');
            }
        }
    }
    
    public function admin_contact_user($user_id) {
	$this->loadModel('User');
	$this->User->recursive=-1;
	if(!$this->User->exists($user_id)){
		throw new NotFoundException('User not found');
		}
        $userName=$this->User->field('username', array('User.id'=>$user_id));
       if ($this->request->is('post')) {
           // debug($this->request->data); die;
            $this->loadModel('User');
        $this->User->recursive = -1;
            $ticket = $this->User->find("first", array(
            'fields' => array('User.email','User.phone', 'User.username', 'p.sub_domain','p.name'),
            'joins' => array(
                array(
                    'table' => 'property_users',
                    'alias' => 'pu',
                    'type' => 'LEFT',
                    'conditions' => array(
                        'pu.user_id = User.id'
                    )),
                array(
                    'table' => 'properties',
                    'alias' => 'p',
                    'type' => 'LEFT',
                    'conditions' => array(
                        'p.id = pu.property_id'
                    )),
            ),
            'conditions' => array('User.id' => $user_id),
        ));
        $this->request->data['Ticket']['recipient_id']=$user_id;
           $this->Ticket->set($this->request->data);
            if($this->Ticket->validates(array('fieldList'=>array('recipient_id','subject','message')))){
            $this->request->data['Ticket']['reply_admin'] = 1;
            $this->request->data['Ticket']['user_reply'] = 0;
            $this->request->data['Ticket']['user_id'] = $this->Auth->user('id');
            $this->request->data['Ticket']['email'] = $ticket['User']['email'];
            $this->request->data['Ticket']['phone'] = $ticket['User']['phone'];
            $this->request->data['Ticket']['property_name'] =  $ticket['p']['name'];
           // debug($this->request->data); die;
            $this->Ticket->create();
            if ($this->Ticket->save($this->request->data)) {
                $url = "http://" . $ticket['p']['sub_domain'] .".". Configure::read('SITE_URL') . "/ticket_replies/view_single/" . $this->Ticket->getLastInsertId();
                $url2 = "http://" . $ticket['p']['sub_domain'] .".". Configure::read('SITE_URL') . "/users/login";
                $ms = $url;
                $ms = wordwrap($ms, 1000);
                $ms2 = $url2;
                $ms2 = wordwrap($ms2, 1000);
                $this->Email->template = 'contact_user';
                $this->Email->from = 'Message from ' . $this->Auth->user('username') . ' (Admin)' . ' <alerts@netparkingpass.com>';
                $this->Email->to = $ticket['User']['email'];
                $this->Email->subject = $this->request->data['Ticket']['subject'];
                $this->Email->sendAs = 'both';
                $emailId=$ticket['User']['email'];
                $userName=$ticket['User']['username'];
                $propertyName=$ticket['p']['name'];
                $this->set(compact('ms','ms2','propertyName','userName','emailId'));
                 $this->Email->smtpOptions=Configure::read('SMTP_SETTING');
				$this->Email->delivery='smtp';
                $this->Email->send();
                $this->Session->setFlash('Your message has been delivered', 'success');
                return $this->redirect(array('controller' => 'ticket_replies', 'action' => 'view_single', $this->Ticket->getLastInsertId()));
            }
            
             else {
                $this->Session->setFlash('Cant contact', 'error');
            }
            }
        }
        $this->set(compact('user_id','userName'));
    }

	public function admin_bulkmessages(){
		$this->loadModel('PropertyUser');
		$this->PropertyUser->recursive=-1;
		$this->PropertyUser->virtualFields = array(
			'UserFullName' => "CONCAT(User.first_name,' ',User.last_name, '--', Property.name)"
		);
         $usernameVal = $this->PropertyUser->find("list", array(
            'fields' => array('User.email','PropertyUser.UserFullName'),
            'joins' => array(
                array(
                    'table' => 'users',
                    'alias' => 'User',
                    'type' => 'INNER',
                    'conditions' => array(
                        'PropertyUser.user_id = User.id'
                    )),
                array(
                    'table' => 'properties',
                    'alias' => 'Property',
                    'type' => 'INNER',
                    'conditions' => array(
                        'Property.id = PropertyUser.property_id'
                    )),
            ),
            'conditions'=>array('PropertyUser.role_id'=>4)
        )); 
		if($this->request->is('POST')){
			if($this->request->data['Ticket']['message']){
				$recipients=array();
				if(!is_array($this->request->data['Ticket']['recipient'])){
					$recipients=array_keys($usernameVal);
				}else{
					$recipients=$this->request->data['Ticket']['recipient'];
				}
				$Email = new CakeEmail();
				$Email->config('smtp');
				$Email->from(array(AuthComponent::user('email') => $_SERVER['HTTP_HOST']));
				$Email->emailFormat('html');
				$Email->bcc($recipients);
				$Email->subject($this->request->data['Ticket']['subject']);
				if($Email->send($this->request->data['Ticket']['message'])){
					$this->Session->setFlash('Email Sent Successfully','success');
				}else{
					$this->Session->setFlash('Email Cannot be Sent','error');
				}
			}else{
				$this->Session->setFlash('Message Cannot be Empty','error');
			}
		}
		$this->set(compact('usernameVal'));
	} 
    public function admin_send_email_bulk(){
		$this->autoRender=false;
		if($this->request->is('POST')){
			$subject=$this->request->data['Ticket']['subject'];
			$message=$this->request->data['Ticket']['message'];
			$usersData=explode(',',$this->request->data['Ticket']['to_users']);
			$emails=$ticket=array();
			//debug($this->request->data);die;
			for($i=0;$i<count($usersData);$i++){
				list($id,$property,$email,$phone)=explode('*',$usersData[$i]);
				if($this->request->data['Ticket']['generate_ticket']){
					$ticket[]=array( 
							 'user_id' => $this->Auth->user('id'),
							 'recipient_id'=>$id,
							 'property_name' => $property,
							 'subject'=>$subject,
							 'email' => $email,
							 'phone' => $phone,
							 'message'=>$message,
							 'reply_admin' => 1,
							 'user_reply' => 0					 
							);
				}
				$emails[]=$email;
			}
			$this->Email->from = 'parking<noreply@' . $_SERVER['HTTP_HOST'] . '>';
			$this->Email->bcc = $emails;
			$this->Email->sendAs = 'html';
			$this->Email->subject = $subject;
			$this->Email->smtpOptions=Configure::read('SMTP_SETTING');
			$this->Email->delivery='smtp';
			$message='Dear Customer,<br>'.$message.'
					  <br>
					  <b>Administrator</b><br>
					  '.$_SERVER['HTTP_HOST'].'
						';
			if($this->Email->send($message)){
				if($this->request->data['Ticket']['generate_ticket']){
					if($this->Ticket->saveMany($ticket,array('validate'=>false))){
						echo "true";
					}else{
						echo "false";
					}
				}else{
					echo "true";
				}	
			}else{
						echo "false";
			}
		}
	}
	public function admin_get_tickets($conditions=NULL){
		$this->autoRender = $this->layout = false;
	    $finalConditions= array(
								'fields' => array('Ticket.id',
												  'Ticket.subject',
												  'Ticket.created',
												  'Ticket.status',
												  'User.id',
												  'User.first_name',
												  'User.last_name',
												  'Recipient.id',
												  'Recipient.first_name',
												  'Recipient.last_name',
												  'Ticket.user_id',
												  'Ticket.recipient_id'
												 ),
								'joins' => array(
									array( 
											'table'=>'users',
											'alias'=>'User',
											'type'=>'INNER',
											'conditions'=>array( 
												'User.id=Ticket.user_id'
											)
										),
									array(
											'table'=>'users',
											'alias'=>'Recipient',
											'type'=>'LEFT',
											'conditions'=>array(
												'Recipient.id=Ticket.recipient_id'
											)
										)
							),
						'order'=>array('Ticket.id'=>'DESC')
			
	    );
	    if($conditions){
			$finalConditions['conditions']=array('Ticket.reply_admin'=>0);
			//$finalConditions['conditions']['Ticket.recipient_id']=NULL;
			$finalConditions['conditions']['Ticket.status']=0;
		}
		$this->paginate =$finalConditions;
	    $this->DataTable->mDataProp = true;
	    $data1 = $this->DataTable->getResponse();
	    $finalData = array();
	     $result = Hash::map($data1['aaData'], "{n}", function($ticket) {
					
					if($ticket['User']['first_name']!=AuthComponent::user('first_name')){
						$ticket['User']['first_name']="<a target='_blank;' href='/admin/users/view_user_details/".$ticket['User']['id']."'>".$ticket['User']['first_name'].' '.$ticket['User']['last_name']."</a>";
					}else{
						$ticket['User']['first_name']=$ticket['User']['first_name'].' '.$ticket['User']['last_name'];
					}
					if($ticket['Recipient']['first_name']!=AuthComponent::user('first_name')){
						$ticket['Recipient']['first_name']="<a target='_blank;' href='/admin/users/view_user_details/".$ticket['Recipient']['id']."'>".$ticket['Recipient']['first_name'].' '.$ticket['Recipient']['last_name']."</a>";
					}else{
						$ticket['Recipient']['first_name']=$ticket['Recipient']['first_name'].' '.$ticket['Recipient']['last_name'];
					}
					$ticket['Ticket']['created']=date('m/d/Y H:i:s',strtotime($ticket['Ticket']['created']));
					$ticket['Ticket']['id']="<a href='/admin/TicketReplies/view_single/".$ticket['Ticket']['id']."' class='label label-sm label-danger'>View Ticket</a>";
					
					if ($ticket['Ticket']['status'] == 0) { 
                      $ticket['Ticket']['status']='<span class="label label-warning">open</span>';
				    }else { 
						$ticket['Ticket']['status']='<span class="label label-default">closed</span>';
				    } 
				    if(!$ticket['Recipient']['id']){
						 $ticket['Ticket']['status']=$ticket['Ticket']['status'].'&nbsp;<span class="label label-success">Not-Replied</span>';
					}  
					if($ticket['Ticket']['user_id']==$ticket['Ticket']['recipient_id']){
							$ticket['Recipient']['first_name']='Admin';
						}     
					return $ticket;
		});
		$data1['aaData'] = $result;
	    echo json_encode($data1);
	 }
	 public function admin_get_tickets_user($userID){
		$this->autoRender = $this->layout = false;  
		$conditions=array('PackageInstance.member_id'=>AuthComponent::user('id'));
	    $this->paginate = array(
								'fields' => array('Ticket.id',
												  'Ticket.subject',
												  'Ticket.created',
												  'Ticket.status',
												  'User.id',
												  'User.first_name',
												  'User.last_name',
												  'Recipient.id',
												  'Recipient.first_name',
												  'Recipient.last_name',
												  'Ticket.user_id',
												  'Ticket.recipient_id',
												 ),
								'joins' => array(
									array( 
											'table'=>'users',
											'alias'=>'User',
											'type'=>'INNER',
											'conditions'=>array( 
												'User.id=Ticket.user_id'
											)
										),
									array(
											'table'=>'users',
											'alias'=>'Recipient',
											'type'=>'LEFT',
											'conditions'=>array(
												'Recipient.id=Ticket.recipient_id'
											)
										)
							),
						'conditions'=>array(
											'OR'=>array(
														array('User.id'=>$userID),
														array('Recipient.id'=>$userID),
											)
						),
						'order'=>array('Ticket.id'=>'DESC')
			
	    );
	    $this->DataTable->mDataProp = true;
	    $data1 = $this->DataTable->getResponse();
	   
	    $finalData = array();
	     $result = Hash::map($data1['aaData'], "{n}", function($ticket) {
					if(AuthComponent::user('role_id')==1){
						if($ticket['User']['first_name']!=AuthComponent::user('first_name')){
							$ticket['User']['first_name']="<a target='_blank;' href='/admin/users/view_user_details/".$ticket['User']['id']."'>".$ticket['User']['first_name'].' '.$ticket['User']['last_name']."</a>";
						}else{
							$ticket['User']['first_name']=$ticket['User']['first_name'].' '.$ticket['User']['last_name'];
						}
						if($ticket['Recipient']['first_name']!=AuthComponent::user('first_name')){
							$ticket['Recipient']['first_name']="<a target='_blank;' href='/admin/users/view_user_details/".$ticket['Recipient']['id']."'>".$ticket['Recipient']['first_name'].' '.$ticket['Recipient']['last_name']."</a>";
						}else{
							$ticket['Recipient']['first_name']=$ticket['Recipient']['first_name'].' '.$ticket['Recipient']['last_name'];
						}
						if($ticket['Ticket']['user_id']==$ticket['Ticket']['recipient_id']){
							$ticket['Recipient']['first_name']='Admin';
						}
					}else{
						$ticket['User']['first_name']=$ticket['User']['first_name'].' '.$ticket['User']['last_name'];
						$ticket['Recipient']['first_name']=$ticket['Recipient']['first_name'].' '.$ticket['Recipient']['last_name']; 	
						if($ticket['Ticket']['user_id']==$ticket['Ticket']['recipient_id']){
							$ticket['Recipient']['first_name']='Admin';
						}
					}
					$ticket['Ticket']['created']=date('m/d/Y H:i:s',strtotime($ticket['Ticket']['created']));
					if(AuthComponent::user('role_id')==1 || AuthComponent::user('role_id')==2){
						$ticket['Ticket']['id']="<a href='/admin/TicketReplies/view_single/".$ticket['Ticket']['id']."' class='label label-sm label-danger'>View Ticket</a>";
					}else{
						$ticket['Ticket']['id']="<a href='/TicketReplies/manager_view_single/".$ticket['Ticket']['id']."' class='label label-sm label-danger'>View Ticket</a>";
					}
					if ($ticket['Ticket']['status'] == 0) { 
                      $ticket['Ticket']['status']='<span class="label label-warning">open</span>';
				    }else { 
						$ticket['Ticket']['status']='<span class="label label-default">closed</span>';
				    } 
				    if(!$ticket['Recipient']['id']){
						 $ticket['Ticket']['status']=$ticket['Ticket']['status'].'&nbsp;<span class="label label-success">Not-Replied</span>';
					}       
					return $ticket;
		});
		$data1['aaData'] = $result;
	    echo json_encode($data1);
	 }
}

?>
