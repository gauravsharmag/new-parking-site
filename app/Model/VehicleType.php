<?php
App::uses('AppModel', 'Model');
/**
 * VehicleType Model
 *
 * @property Property $Property
 */
class VehicleType extends AppModel {


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Property' => array(
			'className' => 'Property',
			'foreignKey' => 'property_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
    var $validate=array(



        //'property_id'=>array('required'=>true,'notEmpty'=>true,'messsage'=>'Required'),
       // 'deposit'=>array('required'=>true,'notEmpty'=>true,'messsage'=>'Required'),
        /*'deposit'=>array(
            'required'=>'true',
            'rule'=>array('minLength',2),
            'message'=>'Min 1 character required'

        ),*/
        'deposit' => array(

            'Only numeric data allowed' => array('rule' => 'numeric','required' => true, 'message' => 'Only numbers are allowed')

        ),
        'pass_cost' => array(
            'Only numeric data allowed' => array('rule' => 'numeric','required' => true, 'message' => 'Only numbers are allowed')

        ),
        'pass_cost_after_1_year' => array(
            'Only numeric data allowed' => array('rule' => 'numeric','required' => true, 'message' => 'Only numbers are allowed')

        ),
        'recurring_cost_duration' => array(
            'Only numeric data allowed' => array('rule' => 'numeric','required' => true, 'message' => 'Only numbers are allowed')

        ),
        'recurring_cost' => array(
            'Only numeric data allowed' => array('rule' => 'numeric','required' => true, 'message' => 'Only numbers are allowed')

        ),
        'vehicle_password'=>array(
            'required'=>'true',
            'rule'=>array('minLength',3),
            'message'=>'Min 3 character required'
        ),
        'vehicle_type_name'=>array(
            'Not Empty'=>array('rule'=>'notBlank','required'=>true),
            'checkVehicleTypeAndProperty'=>array('rule'=>'checkVehicleTypeAndProperty','message'=>'This vehicle type exists')),
    );
    public function checkVehicleTypeAndProperty($data)
    {

        debug($this->data);
       // die();
        if(isset($this->data['VehicleType']['id']))
        {
            return true;
        }
        else
        {
             $var=$this->find('all',array('conditions'=>array('vehicle_type_name '=>$this->data['VehicleType']['vehicle_type_name'],'AND'=>array('property_id'=>$this->data['VehicleType']['property_id']))));
             if($var)
            {
                return false;
            }
            else
            {
                return true;
            }
        }


    }
}
