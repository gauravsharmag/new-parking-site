<?php
App::uses('AppModel', 'Model');
/**
 * Transaction Model
 *
 * @property User $User
 * @property Pass $Pass
 * @property CustomerPass $CustomerPass
 */
class Transaction extends AppModel {


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Pass' => array(
			'className' => 'Pass',
			'foreignKey' => 'pass_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'CustomerPass' => array(
			'className' => 'CustomerPass',
			'foreignKey' => 'transaction_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);
    var $validate=array(
        'email'=>array(
            'Email Required'=>array('rule'=>'email','allowEmpty'=>false,'required'=>true,'message'=>'Email is not correct',)
        ),
        'first_name'=>array('minimum length'=>array(
            'rule'    => array('minLength', '2'),
            'message' => 'Minimum 2 characters required',
            'allowEmpty'=>false,
            'required'=>true),
            'real name' => array('rule' => '/^[a-zA-Z ]*$/', 'message' => 'Only letters and spaces.')

        ),
        'last_name'=>array('minimum length'=>array(
            'rule'    => array('minLength', '2'),
            'message' => 'Minimum 2 characters required',
            'allowEmpty'=>false,
            'required'=>true),
            'real name' => array('rule' => '/^[a-zA-Z ]*$/', 'message' => 'Only letters and spaces.')

        ),
        'address_line_1'=>array(
            'rule'    => array('minLength', '2'),
            'message' => 'Minimum 2 characters required',
            'allowEmpty'=>false,
            'required'=>true
        ),
        'address_line_2'=>array(
            'rule'    => array('minLength', '2'),
            'message' => 'Minimum 2 characters required',
            'required'=>true,
            'allowEmpty'=>true
        ),
        'zip' => array(
            'rule' => array('postal', null, 'us'),
            'message'=>'Please check your zip code',
            'required'=>true,
            'allowEmpty'=>false
        ),
        'phone' => array(
            'rule' => array('phone', null, 'us'),
            'message'=>'Please check your phone number',
            'required'=>true,
            'allowEmpty'=>false
        ),
		'city'=>array('minimum length'=>array(
            'rule'    => array('minLength', '2'),
            'message' => 'Minimum 2 characters required',
            'allowEmpty'=>false,
            'required'=>true),
            'real name' => array('rule' => '/^[a-zA-Z ]*$/', 'message' => 'Only letters and spaces allowed.')

        ),
        'state'=>array(
            'rule'    => array('minLength', '2'),
            'message' => 'Minimum 2 characters required',
            'required'=>true,
            'allowEmpty'=>false
        ),
        'card_number' => array(
            'numeric' => array(
                'rule' => 'numeric',
                'message' => 'Only numeric data allowed',
                'allowEmpty' => false,
                'required' => true
                ),
            'minimum length'=>array('rule'=>array('minLength','15'),'message'=>'Min 15 numbers'),
            'maximum length'=>array('rule'=>array('maxLength','16'),'message'=>'Max 16 numbers'),

        ),
        'cvv' => array(
            'numeric' => array(
                'rule' => 'numeric',
                'message' => 'Only numeric data allowed',
                'allowEmpty' => false,
                'required' => true
            ),
            'minimum length'=>array('rule'=>array('minLength','3'),'message'=>'Min. 3 numbers'),
            'maximum length'=>array('rule'=>array('maxLength','4'),'message'=>'Max. 4 numbers')
        ),
        'month'=>array(
            'rule'    => array('minLength', '1'),
            'message' => 'Select Month',
            'required'=>true,
            'allowEmpty'=>false
        ),
        'year'=>array(
            'rule'    => array('minLength', '1'),
            'message' => 'Select Month',
            'required'=>true,
            'allowEmpty'=>false
        ),
      'first_name_cc'=>array('minimum length'=>array(
            'rule'    => array('minLength', '2'),
            'message' => 'Minimum 2 characters required',
            'allowEmpty'=>false,
            'required'=>true),
            'real name' => array('rule' => '/^[a-zA-Z ]*$/', 'message' => 'Only letters and spaces.')

        ),
        'last_name_cc'=>array('minimum length'=>array(
            'rule'    => array('minLength', '2'),
            'message' => 'Minimum 2 characters required',
            'allowEmpty'=>false,
            'required'=>true),
            'real name' => array('rule' => '/^[a-zA-Z ]*$/', 'message' => 'Only letters and spaces.')
        )

    );

	public function updateValidations()
	{
				$this->validator()->remove('card_number');
				$this->validator()->remove('cvv');
				$this->validator()->remove('month');
				$this->validator()->remove('year');
				$this->validator()->remove('last_name_cc');
				$this->validator()->remove('first_name_cc');
	}
									  

  /*  public function uniqueEmail()
    {
        $uid = CakeSession::read("Auth.User.id");
        App::import('model','BillingAddress');
        $billAdd=new BillingAddress();
        $billAdd->recursive=-1;
        $result=$billAdd->hasAny(array('user_id !='=>$uid,'email'=>$this->data['Transaction']['email']));
        If($result==true){
            return false;
        }
        else{
            return true;
        }


    }*/
	 public function getTransactionData($userId){
		$aColumns = array('paypal_tranc_id','date_time','amount','result','pass_id','first_name','last_name','id');
        $sIndexColumn = "id";
        $sTable = "transactions";
        $sJoinTable=' ';
         $sConditions=' transactions.user_id='.$userId.' ';
        $outs=$this->managerCustomerTransactionDetails(array('columns'=>$aColumns,'index_column'=>$sIndexColumn,'table'=>$sTable,'join'=>$sJoinTable,'conditions'=>$sConditions));
        return $outs;
	}
/***********************************************
 * For Manager Customer Details 
 */

	public function managerCustomerTransactionDetails($config=array()){
		if(!isset($config['columns']) || !isset($config['index_column']) || !isset($config['table'])){
				return array();
		}
		$aColumns=$config['columns'];
		$sIndexColumn=$config['index_column'];
		$sTable=$config['table'];
		$sJoinTable=isset($config['join'])? $config['join'] : '';
		$sCondition=isset($config['conditions'])? $config['conditions'] : '';
        App::uses('ConnectionManager', 'Model');
        $dataSource = ConnectionManager::getDataSource('default');
        $gaSql['user']       = $dataSource->config['login'];
        $gaSql['password']   = $dataSource->config['password'];
        $gaSql['db']         = $dataSource->config['database'];
        $gaSql['server']     = $dataSource->config['host'];
    
        function fatal_error ( $sErrorMessage = '' ){
            header( $_SERVER['SERVER_PROTOCOL'] .' 500 Internal Server Error' );
            die( $sErrorMessage );
        }
        /*
         * MySQL connection
        */
        if ( ! $gaSql['link'] = mysqli_connect( $gaSql['server'], $gaSql['user'], $gaSql['password'],$gaSql['db']) ){
            fatal_error( 'Could not open connection to server' );
        }
        
        if ( ! mysqli_select_db($gaSql['link'],$gaSql['db']) ){
            fatal_error( 'Could not select database ' );
        }
         
         
        /*
         * Paging
        */
        $sLimit = "";
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' ){
            $sLimit = "LIMIT ".intval( $_GET['iDisplayStart'] ).", ".
            intval( $_GET['iDisplayLength'] );
        }
        /*
         * Ordering
        */
        $sOrder = "";
        if ( isset( $_GET['iSortCol_0'] ) ){
            $sOrder = "ORDER BY  ";
            for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ ){
                if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" ){
                    $sOrder .= $aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
                        ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
                }
            }
            $sOrder = substr_replace( $sOrder, "", -2 );
            if ( $sOrder == "ORDER BY" ){
                $sOrder = "";
            }
        }
        /*
         * Filtering
        * NOTE this does not match the built-in DataTables filtering which does it
        * word by word on any field. It's possible to do here, but concerned about efficiency
        * on very large tables, and MySQL's regex functionality is very limited
        */
        $sWhere = "";
        if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" ){
            $sWhere = "WHERE (";
            for ( $i=0 ; $i<count($aColumns) ; $i++ ){
                $sWhere .= $aColumns[$i]." LIKE '%".mysqli_real_escape_string($gaSql['link'], $_GET['sSearch'] )."%' OR ";
            }
            $sWhere = substr_replace( $sWhere, "", -3 );
            $sWhere .= ')';
        }
        for ( $i=0 ; $i<count($aColumns) ; $i++ ){
            if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' ){
                if ( $sWhere == "" ){
                    $sWhere = "WHERE ";
                } else{
                    $sWhere .= " AND ";
                }
                $sWhere .= $aColumns[$i]." LIKE '%".mysqli_real_escape_string($gaSql['link'],$_GET['sSearch_'.$i])."%' ";
            }
        }
        if($sCondition!=''){
            if ( $sWhere == "" ){
                $sWhere="WHERE ".$sCondition." ";
            }else{
                $sWhere .=" AND ".$sCondition." ";
            }
        }
        /*
         * SQL queries
        * Get data to display 
        */
        $sQuery = "
    SELECT SQL_CALC_FOUND_ROWS ".str_replace(" , ", " ", implode(", ", $aColumns))."
            FROM   $sTable $sJoinTable 
            $sWhere
            $sOrder
            $sLimit
            ";
       //debug($sQuery);die;
        $rResult = mysqli_query( $gaSql['link'],$sQuery) or fatal_error( 'MySQL Error Here 1: ' . mysqli_errno($gaSql['link']) );
         
        /* Data set length after filtering */
        $sQuery = "
    SELECT FOUND_ROWS()
";
        $rResultFilterTotal =mysqli_query( $gaSql['link'],$sQuery)  or fatal_error( 'MySQL Error Here 2: ' . mysqli_errno($gaSql['link']) );
        $aResultFilterTotal = mysqli_fetch_array($rResultFilterTotal);
        $iFilteredTotal = $aResultFilterTotal[0];
         
        /* Total data set length */
        $sQuery = "
    SELECT COUNT(".$sIndexColumn.")
            FROM   $sTable $sJoinTable
            ";
        $rResultTotal = mysqli_query( $gaSql['link'],$sQuery) or fatal_error( 'MySQL Error Here 3: ' . mysqli_errno($gaSql['link']) );
        $aResultTotal = mysqli_fetch_array($rResultTotal);
        $iTotal = $aResultTotal[0];
         
        /*
         * Output
        */
        $output = array(
                "sEcho" => intval($_GET['sEcho']),
                "iTotalRecords" => $iTotal,
                "iTotalDisplayRecords" => $iFilteredTotal,
                "aaData" => array()
        );
        App::import('Model', 'Pass');
        $pass = new Pass();
        while ( $aRow = mysqli_fetch_array( $rResult ) )
        {
            $row = array();
            for ( $i=0 ; $i<count($aColumns); $i++ )
            {   
					if ( $aColumns[$i] != ' ' ){
						if($sTable=='transactions'){
							if($aColumns[$i]=="date_time") {
								$row[] = date("m/d/Y H:i:s", strtotime($aRow[ $aColumns[$i] ]));
							}
							elseif($aColumns[$i]=="pass_id") {
								$passList=explode(",",$aRow["pass_id"]);
								if(count($passList)>1){
									//$row[]=$aRow[ $aColumns[$i] ];
									$list='';
									for($j=0;$j<(count($passList));$j++){
										if($passList[$j]){
											$passName=$pass->givePassName($passList[$j]);									
											$list=$list.$passName.'<br>';
										}
									}
									trim($list);
									$row[]=$list;
								}
								else{
									$row[]=$pass->givePassName((int)$aRow[ $aColumns[$i] ]);
								} 
									
							}else{	
							$row[] = $aRow[ $aColumns[$i] ];
							}
						}
					}
			}
         $output['aaData'][] = $row;
         }
        return $output;
	}

 
}
