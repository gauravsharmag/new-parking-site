<?php
App::uses('AppModel', 'Model');
/**
 * Package Model
 *
 * @property Property $Property
 * @property CustomerPass $CustomerPass
 * @property Vehicle $Vehicle
 */
class Package extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		
		'name' => array(
			'alphaNumeric' => array(
                'rule' => array('custom','/^[a-zA-Z 0-9]*$/'),
				'message' => 'Name should be alphanumeric',
                'allowEmpty' => false,
                'required' => true,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
			'minLength' => array(
				'rule' => array('minLength',2),
				'message' => 'Minimum two character required',
			),
            'checkPackageAndProperty'=>array('rule'=>'checkPackageAndProperty','message'=>'This type of already exists in this property')
		),
		'cost' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                'message' => 'Only numeric data allowed',
                'allowEmpty' => false,
                'required' => true
            ),
		),
        'pass_id'=>array( 'rule'=> array('numeric'),'required'=>true,'allowEmpty'=>true,'message'=>'Option not valid'),
		/*'start_date' => array(
			'datetime' => array(
				'rule' => array('date','dmy'),
                'message' => 'Date format is wrong',
				'allowEmpty' => true,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'expiration_date' => array(
			'datetime' => array(
                'rule' => array('date','dmy'),
                'message' => 'Date format is wrong',
                'allowEmpty' => true,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),*/
		'password' => array(
			'minLength' => array(
				'rule' => array('minLength',2),
				'message' => 'Minimum two characters required',
				'allowEmpty' => true,
				//'required' => true,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'total_spaces_each_package' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'Only numeric data allowed',
				'allowEmpty' => true,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
        'free_guest_credits' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                'message' => 'Only numeric data allowed',
                'allowEmpty' => true,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),


	);
    public $validateDate = array(

        'start_date' => array(
            'datetime' => array(
                'rule' => array('date','mdy'),
                'message' => 'Date is either empty or format is incorrect',
                'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'expiration_date' => array(
            'datetime' => array(
                'rule' => array('date','mdy'),
                'message' => 'Date is either empty or format is incorrect',
                'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),



    );
    public $validateDuration=array(

        'duration_type'=> array(
            'rule' => array('notBlank'),
            'message' => 'Please select a duration type '),
      'duration' => array(
          'numeric' => array(
              'rule' => array('numeric'),
              'message' => 'Duration is either empty or contain non numeric data',
              'allowEmpty' => false
          ))

    ); 
	//The Associations below have been created with all possible keys, those that are not needed can be removed
    public $hasOne = array(
        'Pass' => array(
            'className' => 'Pass',
            'foreignKey' => 'package_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );
/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Property' => array(
			'className' => 'Property',
			'foreignKey' => 'property_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'CustomerPass' => array(
			'className' => 'CustomerPass',
			'foreignKey' => 'package_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Vehicle' => array(
			'className' => 'Vehicle',
			'foreignKey' => 'package_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);
    public function beforeValidate($options=null)
    {
        if($this->data['Package']['is_fixed_duration']==0)
        {
            $this->validate=array_merge($this->validate,$this->validateDuration);
            //debug($this->validate);
           // die();
        }
        else
        {
            $this->validate=array_merge($this->validate,$this->validateDate);
           // debug($this->validate);
           // die();
        }


    }
    public function beforeSave($options=null)
    {
       if(isset($this->data['Package']['start_date']))

       {
        $this->data['Package']['start_date'] = date("Y-m-d", strtotime($this->data['Package']['start_date']));
        $this->data['Package']['expiration_date'] = date("Y-m-d", strtotime($this->data['Package']['expiration_date']));
		
       }
        if(empty($this->data['Package']['pass_id'])){
            unset($this->data['Package']['pass_id']);
        }

    }
    public function checkPackageAndProperty($data)
    {


        if(isset($this->data['Package']['id']))
        {
            $this->recursive=-1;
            $var=$this->find('all',array('fields'=>array('name'),'conditions'=>array('Package.property_id'=>$this->data['Package']['property_id'],'Package.name'=>$this->data['Package']['name'],'NOT'=>array('Package.id'=>$this->data['Package']['id']))));
            if($var)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        else
        {
            $var=$this->find('all',array('conditions'=>array('Package.name '=>$this->data['Package']['name'],'AND'=>array('Package.property_id'=>$this->data['Package']['property_id']))));
            if($var)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        debug($this->data);
        die();

    }
	 public function givePackageName($id){
		if (!$this->exists($id)) {
            return "No Package Found";
        }else{
			return $this->field('name',array('id'=>$id));
		}
	}
}
