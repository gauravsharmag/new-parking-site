<?php

class TicketReply extends AppModel {

    var $name = 'TicketReply';
    public $belongsTo = array(
       		'Ticket' => array(
            'className' => 'Ticket',
            'foreignKey' => 'ticket_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
			),
			'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );
}

?>
