<?php
App::uses('AppModel', 'Model');
/**
 * State Model
 *
 */
class State extends AppModel {

/**
 * Primary key field
 *
 * @var string
 */
	public $primaryKey = 'abbrev';

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';

}
