<?php
App::uses('AppModel', 'Model');
/**
 * Role Model
 *
 * @property PropertyUser $PropertyUser
 * @property User $User
 */
class Role extends AppModel {
	public $ROLES=array('superAdmin'=>1,'propertyAdmin'=>2,'propertyManager'=>3,'customer'=>4);
	public $actsAs = array('Acl' => array('type' => 'requester'));
    public function parentNode()
    {
        return null;
    }
public $hasMany = array(
		'PropertyUser' => array(
			'className' => 'PropertyUser', 
			'foreignKey' => 'role_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'role_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);
	

}
