<?php
App::uses('AppModel', 'Model');
/**
 * CustomerPass Model
 *
 * @property User $User
 * @property Vehicle $Vehicle
 * @property Property $Property
 * @property Transaction $Transaction
 * @property Package $Package
 */
class CustomerPass extends AppModel {


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Vehicle' => array(
			'className' => 'Vehicle',
			'foreignKey' => 'vehicle_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Property' => array(
			'className' => 'Property',
			'foreignKey' => 'property_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Transaction' => array(
			'className' => 'Transaction',
			'foreignKey' => 'transaction_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Package' => array(
			'className' => 'Package',
			'foreignKey' => 'package_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
        'Pass' => array(
			'className' => 'Pass',
			'foreignKey' => 'pass_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
	var $validate=array(
		'RFID_tag_number'=>array('Numeric'=>array(  'rule' => array('custom','/^[a-zA-Z0-9]*$/'),'required'=>true,'message'=>'Only AlphaNumeric data allowed'),
            'Minimum Length'=>array('rule'=>array('minLength',2),'message'=>'Minimum 2 characters required'),
            'is Unique'=>array('rule'=>'isUnique','message'=>'This RFID Tag Number has already been used')
            ),
		'owner'=>array(
            'rule' => array('custom','/^[a-zA-Z ]*$/'),
            'message' => 'Only alphabets allowed',
            'required'=>true,
			'allowEmpty'=>true

        ),
        'assigned_location' => array('Numeric' => array('rule' => array('custom', '/^[a-zA-Z0-9 ]*$/'), 'required' => false,'allowEmpty'=>true, 'message' => 'Only AlphaNumeric data allowed'),
            'Minimum Length' => array('rule' => array('minLength', 1), 'message' => 'Minimum 1 characters required'),
            'is Unique' => array('rule' => 'checkAssignedLocation', 'message' => 'This Location Has Already Assigned')
        ),
        'make'=>array('Alphanumeric'=>array('rule' => array('custom','/^[a-zA-Z 0-9]*$/'),'allowEmpty'=>true,'required'=>true,'message'=>'Only Alpha Numeric data allowed'),
                      'Minimum Length'=>array('rule'=>array('minLength',2),'message'=>'Minimum 2 character required')
                      ),
        'model'=>array('Alphanumeric'=>array('rule' => array('custom','/^[a-zA-Z 0-9]*$/'),'allowEmpty'=>true,'required'=>true,'message'=>'Only Alpha Numeric data allowed'),
                      'Minimum Length'=>array('rule'=>array('minLength',1),'message'=>'Minimum 1 character required')
                      ),
        'color'=>array('Alphanumeric'=>array('rule' => array('custom','/^[a-zA-Z 0-9]*$/'),'allowEmpty'=>true,'required'=>true,'message'=>'Only Alpha Numeric data allowed'),
                        'Minimum Length'=>array('rule'=>array('minLength',2),'message'=>'Minimum 2 character required')
                      ),
        'license_plate_number'=>array('Alphanumeric'=>array('rule'=>'alphaNumeric','allowEmpty'=>true,'required'=>false,'message'=>'Only numeric data'),
                         'Minimum Length'=>array('rule'=>array('minLength',2),'message'=>'Minimum 2 character required'),
                         'Already Registered'=>array('rule'=>'isRegistered','message'=>'This vehicle is already registered in this property')
                      ),
        'last_4_digital_of_vin'=>array('Numeric'=>array('rule'=>'numeric','allowEmpty'=>true,'required'=>true,'message'=>'Only numeric data'),
            'Minimum Length'=>array('rule'=>array('minLength',2),'message'=>'Minimum 2 characters required'),
            'Maximum Length'=>array('rule'=>array('maxLength',4),'message'=>'Maximum 4 characters required')
                      ),
        'license_plate_state'=>array(
            'rule'    => array('minLength', '2'),
            'message' => 'Minimum 2 characters required',
            'required'=>false,
            'allowEmpty'=>true
        ),
        'pass_valid_upto'=> array(
            'date and time' => array(
               'rule' => 'checkdate',
                'message' => 'Incorrect Date Time',
                'allowEmpty' => false,
                'required' => true, 
            )
        ),
        'membership_vaild_upto'=> array(
            'date and time' => array(
                'rule' => 'checkdate',
                'message' => 'Incorrect Date Time',
                'allowEmpty' => true,
                'required' => false, 
            )
        )
	);
	var $validateApprove=array(
		 'id'=>array(
				'allowEmpty' => false,
                'required' => true 
          ),
		 'pass_valid_upto'=> array(
            'date and time' => array(
               'rule' => 'checkdate',
                'message' => 'Incorrect Date Time',
                'allowEmpty' => false,
                'required' => true, 
            )
        ),
        'membership_vaild_upto'=> array(
            'date and time' => array(
                'rule' => 'checkdate',
                'message' => 'Incorrect Date Time',
                'allowEmpty' => true,
                'required' => false, 
            )
        )
	
	);
	public function updateValidations()
	{
				$this->validator()->remove('owner');
				$this->validator()->remove('make');
				$this->validator()->remove('model');
				$this->validator()->remove('color');
				$this->validator()->remove('license_plate_number');
				$this->validator()->remove('license_plate_number');
	}	
	public function checkAssignedLocation($options=array()){ 
				$propertyid=$this->field('property_id',array('id'=>$this->data['CustomerPass']['id']));
				if($this->hasAny(array('assigned_location'=>$this->data['CustomerPass']['assigned_location'],
										'property_id'=>$this->data['CustomerPass']['property_id'],
										'id !='=>$this->data['CustomerPass']['id']
										))){
					return false;
				}else{
					return true;
				}	
				//debug($propertyid);
				//debug($this->data);die;
	}
	public function checkdate($options=array()){
				$value=array_values($options);
				if(!is_null($value[0])){
					if($result= strtotime($value[0])){
						return true;
					}else{
						return false;
					}
				}else{
					return true;
				}
	}
    public function getCurrentPassStatus($userId=null,$propertyId=null)
    {
        $dt = new DateTime();
        $currentDateTime= $dt->format('Y-m-d H:m:s');
            $this->recursive=-1;
            $passesValidPackagesValid= $this->find('count',array('conditions'=>array('pass_valid_upto >'=>$currentDateTime,'AND'=>array('user_id'=>$userId,'property_id'=>$propertyId,'membership_vaild_upto >'=>$currentDateTime))));
            $this->recursive=-1;
            $passesValidPackageExpired= $this->find('count',array('conditions'=>array('pass_valid_upto >'=>$currentDateTime,'AND'=>array('user_id'=>$userId,'property_id'=>$propertyId,'membership_vaild_upto <'=>$currentDateTime))));
            $this->recursive=-1;
            $expiredPasses= $this->find('count',array('conditions'=>array('pass_valid_upto <'=>$currentDateTime)));
         $returnArr = array('passesValidPackagesValid'=>$passesValidPackagesValid,'passesValidPackageExpired'=>$passesValidPackageExpired,'expiredPasses'=>$expiredPasses);

        return  $returnArr ;

    }
	 public function getTableData($conditions=false){
		$aColumns = array('users.username',
						  'users.manager_approved',
						  'properties.name',
						  'count(*)',
						  'user_id',
						  'users.first_name',
						  'users.last_name',
						  'users.address_line_1',
						  'users.address_line_2',
						  'users.city',
						  'users.state',
						  'users.zip',
						  'users.phone',
						  'users.email');
        $sIndexColumn = "customer_passes.id";
        $sTable = "customer_passes";
        $sJoinTable=' INNER JOIN users ON customer_passes.user_id = users.id INNER JOIN properties ON  properties.id = customer_passes.property_id ';
        $sConditions=' RFID_tag_number IS NULL ';
        if($conditions){
			$sConditions=$sConditions." AND ".$conditions;
		}
		 $sConditions= $sConditions.' GROUP BY(user_id) ';
		
        $outs=$this->simplifyDataTableRFID(array('columns'=>$aColumns,'index_column'=>$sIndexColumn,'table'=>$sTable,'join'=>$sJoinTable,'conditions'=>$sConditions));
        return $outs;
	}
	
	public function getRFIDData(){
		$aColumns = array('users.username','properties.name','vehicles.owner','vehicles.license_plate_number','pass_valid_upto','membership_vaild_upto','cp.RFID_tag_number','cp.id');
         
        /* Indexed column (used for fast and accurate table cardinality) */
        $sIndexColumn = "cp.id";
         
        /* DB table to use */ 
        $sTable = "customer_passes cp";
        $sJoinTable=' LEFT JOIN users ON cp.user_id = users.id LEFT JOIN properties ON cp.property_id = properties.id LEFT JOIN vehicles ON cp.vehicle_id = vehicles.id ';
        $sConditions=' cp.RFID_tag_number IS NOT NULL ';
        $outs=$this->simplifyDataTableRFID(array('columns'=>$aColumns,'index_column'=>$sIndexColumn,'table'=>$sTable,'join'=>$sJoinTable,'conditions'=>$sConditions));
        return $outs;
	}
		public function getRFIDDataPropertyWise($property_id){
		$aColumns = array('users.username','properties.name','vehicles.owner','vehicles.license_plate_number','pass_valid_upto','membership_vaild_upto','cp.RFID_tag_number','cp.id');
        $sIndexColumn = "cp.id";
        $sTable = "customer_passes cp";
        $sJoinTable=' LEFT JOIN users ON cp.user_id = users.id LEFT JOIN properties ON cp.property_id = properties.id LEFT JOIN vehicles ON cp.vehicle_id = vehicles.id ';
        $sConditions=' cp.RFID_tag_number IS NOT NULL AND cp.property_id = '.$property_id.' ';
        $outs=$this->simplifyDataTableRFID(array('columns'=>$aColumns,'index_column'=>$sIndexColumn,'table'=>$sTable,'join'=>$sJoinTable,'conditions'=>$sConditions));
        return $outs;
	}
/********************************************************************
 * Manager Get Active Vehicles
 */
	public function getActiveVehicle(){
		$dt = new DateTime();
		$currentDateTime= $dt->format('Y-m-d H:i:s');
		$aColumns = array('users.first_name','users.address_line_1','vehicles.make','vehicles.model','vehicles.color','vehicles.license_plate_number','vehicles.license_plate_state','vehicles.last_4_digital_of_vin','pass_valid_upto','membership_vaild_upto','cp.RFID_tag_number','users.last_name','users.address_line_2','users.city','users.state','users.email','users.phone');
         
        /* Indexed column (used for fast and accurate table cardinality) */
        $sIndexColumn = "cp.id";
         
        /* DB table to use */ 
        $sTable = "customer_passes cp";
        $sJoinTable=' LEFT JOIN users ON cp.user_id = users.id LEFT JOIN vehicles ON cp.vehicle_id = vehicles.id ';
        $sConditions= " (cp.is_guest_pass = 0 OR cp.is_guest_pass IS NULL) AND cp.pass_valid_upto >'".$currentDateTime."' AND cp.membership_vaild_upto >'".$currentDateTime."' AND cp.property_id =".CakeSession::read('PropertyId')." AND cp.vehicle_id IS NOT NULL AND cp.pass_archived=0";
        $outs=$this->simplifyDataTableRFID(array('columns'=>$aColumns,'index_column'=>$sIndexColumn,'table'=>$sTable,'join'=>$sJoinTable,'conditions'=>$sConditions));
        return $outs;
	}
/********************************************************************
 * Manager Get In-Active Vehicles
 */
	public function getInActiveVehicle(){
		$dt = new DateTime();
		$currentDateTime= $dt->format('Y-m-d H:i:s');
		$aColumns = array('users.first_name','users.address_line_1','vehicles.make','vehicles.model','vehicles.color','vehicles.license_plate_number','vehicles.license_plate_state','vehicles.last_4_digital_of_vin','pass_valid_upto','membership_vaild_upto','cp.RFID_tag_number','users.last_name','users.address_line_2','users.city','users.state','users.email','users.phone');
         
        /* Indexed column (used for fast and accurate table cardinality) */
        $sIndexColumn = "cp.id";
         
        /* DB table to use */ 
        $sTable = "customer_passes cp";
        $sJoinTable=' LEFT JOIN users ON cp.user_id = users.id LEFT JOIN vehicles ON cp.vehicle_id = vehicles.id ';
        $sConditions= " (cp.is_guest_pass = 0 OR cp.is_guest_pass IS NULL) AND (cp.pass_valid_upto <'".$currentDateTime."' OR cp.membership_vaild_upto <'".$currentDateTime."') AND cp.property_id =".CakeSession::read('PropertyId')." AND cp.vehicle_id IS NOT NULL AND cp.pass_archived=0 ";
        $outs=$this->simplifyDataTableRFID(array('columns'=>$aColumns,'index_column'=>$sIndexColumn,'table'=>$sTable,'join'=>$sJoinTable,'conditions'=>$sConditions));
        return $outs;
	}
/********************************************************************
 * Manager Get Guest Vehicles
 */
	public function getGuestVehicle(){
		$aColumns = array('users.first_name','users.address_line_1','vehicles.make','vehicles.model','vehicles.color','vehicles.license_plate_number','vehicles.license_plate_state','vehicles.last_4_digital_of_vin','pass_valid_upto','membership_vaild_upto','cp.RFID_tag_number','users.last_name','users.address_line_2','users.city','users.state','users.email','users.phone');
         
        /* Indexed column (used for fast and accurate table cardinality) */
        $sIndexColumn = "cp.id";
         
        /* DB table to use */ 
        $sTable = "customer_passes cp";
        $sJoinTable=' LEFT JOIN users ON cp.user_id = users.id LEFT JOIN vehicles ON cp.vehicle_id = vehicles.id ';
        $sConditions= " cp.is_guest_pass = 1 AND cp.property_id=".CakeSession::read('PropertyId')." AND cp.vehicle_id IS NOT NULL AND cp.pass_archived=0";
        $outs=$this->simplifyDataTableRFID(array('columns'=>$aColumns,'index_column'=>$sIndexColumn,'table'=>$sTable,'join'=>$sJoinTable,'conditions'=>$sConditions));
        return $outs;
	}
/********************************************************************
 * Manager Get Active Guest Vehicles
 */
	public function getActiveGuestVehicle(){
		$dt = new DateTime();
		$currentDateTime= $dt->format('Y-m-d H:i:s');
		$aColumns = array('users.first_name','users.address_line_1','vehicles.make','vehicles.model','vehicles.color','vehicles.license_plate_number','vehicles.license_plate_state','vehicles.last_4_digital_of_vin','pass_valid_upto','membership_vaild_upto','cp.RFID_tag_number','users.last_name','users.address_line_2','users.city','users.state','users.email','users.phone');
        $sIndexColumn = "cp.id";
        $sTable = "customer_passes cp";
        $sJoinTable=' LEFT JOIN users ON cp.user_id = users.id LEFT JOIN vehicles ON cp.vehicle_id = vehicles.id ';
        $sConditions= " cp.is_guest_pass = 1 AND cp.pass_valid_upto >'".$currentDateTime."' AND cp.membership_vaild_upto >'".$currentDateTime."' AND cp.property_id =".CakeSession::read('PropertyId')." AND cp.vehicle_id IS NOT NULL AND cp.pass_archived=0 ";
        $outs=$this->simplifyDataTableRFID(array('columns'=>$aColumns,'index_column'=>$sIndexColumn,'table'=>$sTable,'join'=>$sJoinTable,'conditions'=>$sConditions));
        return $outs;
	}
	function isRegistered(){
		App::import('model','Vehicle');
		$vehicle = new Vehicle();
		if(isset($this->data['CustomerPass']['vehicle_id'])){
			$i=$vehicle->hasAny(array('license_plate_number'=>$this->data['CustomerPass']['license_plate_number'],'license_plate_state'=>$this->data['CustomerPass']['license_plate_state'],'property_id'=>$this->data['CustomerPass']['property_id'],'id !='=>$this->data['CustomerPass']['vehicle_id']));
			if($i){
				$this->invalidate('license_plate_state','Vehicle with this plate number and state is already present');
				return false;
			}else{
				return true;
			}
		}else{
				return true;
		}
	}
	public function get_data_passwise($pass_id){
		$aColumns = array('users.username','properties.name','vehicles.owner','vehicles.license_plate_number','pass_valid_upto','membership_vaild_upto','cp.RFID_tag_number','cp.id','cp.user_id');
        $sIndexColumn = "cp.id";
        $sTable = "customer_passes cp";
        $sJoinTable=' LEFT JOIN users ON cp.user_id = users.id LEFT JOIN properties ON cp.property_id = properties.id LEFT JOIN vehicles ON cp.vehicle_id = vehicles.id ';
        $sConditions=' cp.pass_id= '.$pass_id.' AND cp.pass_archived=0';
        $outs=$this->propertyWisesPasses(array('columns'=>$aColumns,'index_column'=>$sIndexColumn,'table'=>$sTable,'join'=>$sJoinTable,'conditions'=>$sConditions));
        return $outs;
	}
	public function get_data_passwise_interval($condition){
		$aColumns = array('users.username','properties.name','vehicles.owner','vehicles.license_plate_number','pass_valid_upto','membership_vaild_upto','cp.RFID_tag_number','cp.id','cp.user_id','users.first_name','users.last_name','users.address_line_1','users.address_line_2','users.city','users.state','users.zip','users.phone','users.email','cp.user_id','cp.created');
         
        /* Indexed column (used for fast and accurate table cardinality) */
        $sIndexColumn = "cp.id";
         
        /* DB table to use */ 
        $sTable = "customer_passes cp";
        $sJoinTable=' LEFT JOIN users ON cp.user_id = users.id LEFT JOIN properties ON cp.property_id = properties.id LEFT JOIN vehicles ON cp.vehicle_id = vehicles.id ';
        $sConditions=$condition.' AND cp.pass_archived=0';
       // debug($sConditions);die;
        $outs=$this->propertyWisesPassesExpired(array('columns'=>$aColumns,'index_column'=>$sIndexColumn,'table'=>$sTable,'join'=>$sJoinTable,'conditions'=>$sConditions));
        return $outs;
	}	
/***********************************************
 * For Property Wise Passes 
 */

	public function propertyWisesPasses($config=array()){
		if(!isset($config['columns']) || !isset($config['index_column']) || !isset($config['table'])){
				return array();
		}
		$aColumns=$config['columns'];
		$sIndexColumn=$config['index_column'];
		$sTable=$config['table'];
		$sJoinTable=isset($config['join'])? $config['join'] : '';
		$sCondition=isset($config['conditions'])? $config['conditions'] : '';
        App::uses('ConnectionManager', 'Model');
        $dataSource = ConnectionManager::getDataSource('default');
        $gaSql['user']       = $dataSource->config['login'];
        $gaSql['password']   = $dataSource->config['password'];
        $gaSql['db']         = $dataSource->config['database'];
        $gaSql['server']     = $dataSource->config['host'];
       
        /* Local functions
        */
        function fatal_error ( $sErrorMessage = '' ){
            header( $_SERVER['SERVER_PROTOCOL'] .' 500 Internal Server Error' );
            die( $sErrorMessage );
        }
        /*
         * MySQL connection
        */
        if ( ! $gaSql['link'] = mysqli_connect( $gaSql['server'], $gaSql['user'], $gaSql['password'],$gaSql['db']) ){
            fatal_error( 'Could not open connection to server' );
        }
		
        if ( ! mysqli_select_db($gaSql['link'],$gaSql['db']) ){
            fatal_error( 'Could not select database ' );
        }
         
         
        /*
         * Paging
        */
        $sLimit = "";
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' ){
            $sLimit = "LIMIT ".intval( $_GET['iDisplayStart'] ).", ".
            intval( $_GET['iDisplayLength'] );
        }
        /*
         * Ordering
        */
        $sOrder = "";
        if ( isset( $_GET['iSortCol_0'] ) ){
            $sOrder = "ORDER BY  ";
            for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ ){
                if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" ){
                    $sOrder .= $aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
                        ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
                }
            }
            $sOrder = substr_replace( $sOrder, "", -2 );
            if ( $sOrder == "ORDER BY" ){
                $sOrder = "";
            }
        }
        /*
         * Filtering
        * NOTE this does not match the built-in DataTables filtering which does it
        * word by word on any field. It's possible to do here, but concerned about efficiency
        * on very large tables, and MySQL's regex functionality is very limited
        */
        $sWhere = "";
        if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" ){
            $sWhere = "WHERE (";
            for ( $i=0 ; $i<count($aColumns) ; $i++ ){
                $sWhere .= $aColumns[$i]." LIKE '%".mysqli_real_escape_string($gaSql['link'], $_GET['sSearch'] )."%' OR ";
            }
            $sWhere = substr_replace( $sWhere, "", -3 );
            $sWhere .= ')';
        }
        for ( $i=0 ; $i<count($aColumns) ; $i++ ){
            if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' ){
                if ( $sWhere == "" ){
                    $sWhere = "WHERE ";
                } else{
                    $sWhere .= " AND ";
                }
                $sWhere .= $aColumns[$i]." LIKE '%".mysqli_real_escape_string($gaSql['link'],$_GET['sSearch_'.$i])."%' ";
            }
        }
        if($sCondition!=''){
			if ( $sWhere == "" ){
				$sWhere="WHERE ".$sCondition." ";
			}else{
				$sWhere .=" AND ".$sCondition." ";
			}
		}
        /*
         * SQL queries
        * Get data to display 
        */
        $sQuery = "
    SELECT SQL_CALC_FOUND_ROWS ".str_replace(" , ", " ", implode(", ", $aColumns))."
            FROM   $sTable $sJoinTable 
            $sWhere
            $sOrder
            $sLimit
            ";
       //debug($sQuery);die;
        $rResult = mysqli_query( $gaSql['link'],$sQuery) or fatal_error( 'MySQL Error Here 1: ' . mysqli_errno($gaSql['link']) );
         
        /* Data set length after filtering */
        $sQuery = "
    SELECT FOUND_ROWS()
";
        $rResultFilterTotal =mysqli_query( $gaSql['link'],$sQuery)  or fatal_error( 'MySQL Error Here 2: ' . mysqli_errno($gaSql['link']) );
        $aResultFilterTotal = mysqli_fetch_array($rResultFilterTotal);
        $iFilteredTotal = $aResultFilterTotal[0];
         
        /* Total data set length */
        $sQuery = "
    SELECT COUNT(".$sIndexColumn.")
            FROM   $sTable $sJoinTable
            ";
        $rResultTotal = mysqli_query( $gaSql['link'],$sQuery) or fatal_error( 'MySQL Error Here 3: ' . mysqli_errno($gaSql['link']) );
        $aResultTotal = mysqli_fetch_array($rResultTotal);
        $iTotal = $aResultTotal[0];
         
        /*
         * Output
        */
        $output = array(
                "sEcho" => intval($_GET['sEcho']),
                "iTotalRecords" => $iTotal,
                "iTotalDisplayRecords" => $iFilteredTotal,
                "aaData" => array()
        );
        
        while ( $aRow = mysqli_fetch_array( $rResult ) )
        {
            $row = array();
            for ( $i=0 ; $i<count($aColumns); $i++ )
            {  
					if(count(explode('.',$aColumns[$i]))==2){
						$aColumns[$i]=explode('.',$aColumns[$i]);
						$aColumns[$i]=$aColumns[$i][1];
					}  
                   if ( $aColumns[$i] != ' ' ){
						if($aColumns[$i]=="id") {
								$row[] = "<a class='btn green btn-xs green-stripe' href='/admin/CustomerPasses/edit/".$aRow[ $aColumns[$i] ]."'>Add/Edit RFID Tag</a>";
						}elseif($aColumns[$i]=="user_id"){
								$row[] = "<button class='btn yellow btn-xs' onclick='showModal(this);' id='".$aRow[ 'id' ]."' type='button'>Guest Pass Detail</button>";
						}elseif($aColumns[$i]=="pass_valid_upto"||$aColumns[$i]=="membership_vaild_upto"){
							if(is_null($aRow[ $aColumns[$i] ])){
								$row[] = $aRow[ $aColumns[$i] ];
							}else{
								  $dt = new DateTime();
								  $currentDateTime= $dt->format('Y-m-d H:i:s');
								  if($aRow[ $aColumns[$i] ]>$currentDateTime){	
									$row[] = date("m/d/Y H:i:s", strtotime($aRow[ $aColumns[$i] ])).' '.'<span class="label label-sm label-danger">Active</span>';
								  }elseif($aRow[ $aColumns[$i] ]<$currentDateTime){
									$row[] = date("m/d/Y H:i:s", strtotime($aRow[ $aColumns[$i] ])).' '.'<span class="label label-sm label-warning">Expired</span>';
								  }else{
									$row[] = $aRow[ $aColumns[$i] ];
								  }
							}
						}
						else {
							$row[] = $aRow[ $aColumns[$i] ];
						}
					}
            }
            $output['aaData'][] = $row;
        }
        return $output;
	}	
public function propertyWisesPassesExpired($config=array()){
		if(!isset($config['columns']) || !isset($config['index_column']) || !isset($config['table'])){
				return array();
		}
		$aColumns=$config['columns'];
		$sIndexColumn=$config['index_column'];
		$sTable=$config['table'];
		$sJoinTable=isset($config['join'])? $config['join'] : '';
		$sCondition=isset($config['conditions'])? $config['conditions'] : '';
        App::uses('ConnectionManager', 'Model');
        $dataSource = ConnectionManager::getDataSource('default');
        $gaSql['user']       = $dataSource->config['login'];
        $gaSql['password']   = $dataSource->config['password'];
        $gaSql['db']         = $dataSource->config['database'];
        $gaSql['server']     = $dataSource->config['host'];
       
        function fatal_error ( $sErrorMessage = '' ){
            header( $_SERVER['SERVER_PROTOCOL'] .' 500 Internal Server Error' );
            die( $sErrorMessage );
        }
        /*
         * MySQL connection
        */
        if ( ! $gaSql['link'] = mysqli_connect( $gaSql['server'], $gaSql['user'], $gaSql['password'],$gaSql['db']) ){
            fatal_error( 'Could not open connection to server' );
        }
		
        if ( ! mysqli_select_db($gaSql['link'],$gaSql['db']) ){
            fatal_error( 'Could not select database ' );
        }
         
         
        /*
         * Paging
        */
        $sLimit = "";
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' ){
            $sLimit = "LIMIT ".intval( $_GET['iDisplayStart'] ).", ".
            intval( $_GET['iDisplayLength'] );
        }
        /*
         * Ordering
        */
        $sOrder = "";
        if ( isset( $_GET['iSortCol_0'] ) ){
            $sOrder = "ORDER BY  ";
            for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ ){
                if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" ){
                    $sOrder .= $aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
                        ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
                }
            }
            $sOrder = substr_replace( $sOrder, "", -2 );
            if ( $sOrder == "ORDER BY" ){
                $sOrder = "";
            }
        }
        /*
         * Filtering
        * NOTE this does not match the built-in DataTables filtering which does it
        * word by word on any field. It's possible to do here, but concerned about efficiency
        * on very large tables, and MySQL's regex functionality is very limited
        */
        $sWhere = "";
        if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" ){
            $sWhere = "WHERE (";
            for ( $i=0 ; $i<count($aColumns) ; $i++ ){
                $sWhere .= $aColumns[$i]." LIKE '%".mysqli_real_escape_string($gaSql['link'], $_GET['sSearch'] )."%' OR ";
            }
            $sWhere = substr_replace( $sWhere, "", -3 );
            $sWhere .= ')';
        }
        for ( $i=0 ; $i<count($aColumns) ; $i++ ){
            if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' ){
                if ( $sWhere == "" ){
                    $sWhere = "WHERE ";
                } else{
                    $sWhere .= " AND ";
                }
                $sWhere .= $aColumns[$i]." LIKE '%".mysqli_real_escape_string($gaSql['link'],$_GET['sSearch_'.$i])."%' ";
            }
        }
        if($sCondition!=''){
			if ( $sWhere == "" ){
				$sWhere="WHERE ".$sCondition." ";
			}else{
				$sWhere .=" AND ".$sCondition." ";
			}
		}
        /*
         * SQL queries
        * Get data to display 
        */
        $sQuery = "
    SELECT SQL_CALC_FOUND_ROWS ".str_replace(" , ", " ", implode(", ", $aColumns))."
            FROM   $sTable $sJoinTable 
            $sWhere
            $sOrder
            $sLimit
            ";
       //debug($sQuery);die;
        $rResult = mysqli_query( $gaSql['link'],$sQuery) or fatal_error( 'MySQL Error Here 1: ' . mysqli_errno($gaSql['link']) );
         
        /* Data set length after filtering */
        $sQuery = "
    SELECT FOUND_ROWS()
";
        $rResultFilterTotal =mysqli_query( $gaSql['link'],$sQuery)  or fatal_error( 'MySQL Error Here 2: ' . mysqli_errno($gaSql['link']) );
        $aResultFilterTotal = mysqli_fetch_array($rResultFilterTotal);
        $iFilteredTotal = $aResultFilterTotal[0];
         
        /* Total data set length */
        $sQuery = "
    SELECT COUNT(".$sIndexColumn.")
            FROM   $sTable $sJoinTable
            ";
        $rResultTotal = mysqli_query( $gaSql['link'],$sQuery) or fatal_error( 'MySQL Error Here 3: ' . mysqli_errno($gaSql['link']) );
        $aResultTotal = mysqli_fetch_array($rResultTotal);
        $iTotal = $aResultTotal[0];
         
        /*
         * Output
        */
        $output = array(
                "sEcho" => intval($_GET['sEcho']),
                "iTotalRecords" => $iTotal,
                "iTotalDisplayRecords" => $iFilteredTotal,
                "aaData" => array()
        );
        
        while ( $aRow = mysqli_fetch_array( $rResult ) )
        {
            $row = array();
            for ( $i=0 ; $i<count($aColumns); $i++ )
            {  
					if(count(explode('.',$aColumns[$i]))==2){
						$aColumns[$i]=explode('.',$aColumns[$i]);
						$aColumns[$i]=$aColumns[$i][1];
					}  
                   if ( $aColumns[$i] != ' ' ){
						if($aColumns[$i]=="id") {
								$row[] = "<a class='btn green btn-xs green-stripe' href='/admin/CustomerPasses/edit/".$aRow[ $aColumns[$i] ]."'>Add/Edit RFID Tag</a>";
						}elseif($aColumns[$i]=="pass_valid_upto"||$aColumns[$i]=="membership_vaild_upto"){
							$newStr='';
							if(is_null($aRow[ $aColumns[$i] ])){
								$newStr = $aRow[ $aColumns[$i] ];
							}else{
								  $dt = new DateTime();
								  $currentDateTime= $dt->format('Y-m-d H:i:s');
								  if($aRow[ $aColumns[$i] ]>$currentDateTime){	
									$newStr = date("m/d/Y H:i:s", strtotime($aRow[ $aColumns[$i] ])).' '.'<span class="label label-sm label-danger">Active</span>';
								  }elseif($aRow[ $aColumns[$i] ]<$currentDateTime){
									$newStr = date("m/d/Y H:i:s", strtotime($aRow[ $aColumns[$i] ])).' '.'<span class="label label-sm label-warning">Expired</span>';
								  }else{
									$newStr = $aRow[ $aColumns[$i] ];
								  }
								  
							}
							if(isset($aRow['created'])){
								if($aColumns[$i]=="pass_valid_upto"){
									$newStr.='<br><b>Purchased On :</b> '.date("m/d/Y",strtotime($aRow['created']));
								}
							}
							$row[]=$newStr;
						}
						else {
							$row[] = $aRow[ $aColumns[$i] ];
						}
					} 
            }  
            $output['aaData'][] = $row;
        }
        return $output;
	}
}
